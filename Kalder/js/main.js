/*-----------------------------------------------------------------------------------*/
/* 		Mian Js Start 
/*-----------------------------------------------------------------------------------*/



var start;
var oturumBaslangic;


var tur = $("#dropCins");
var sehir = $("#dropPlaces");
var adres = $("#txtAdressContent");

kurumtipi = new Array("GENEL MERKEZ", "ŞUBE", "TEMSİLCİLİK");
sehirler = new Array("İstanbul", "Ankara", "Bursa", "Eskişehir", "İzmir", "Kayseri", "Trakya", "Doğu Marmara", "Batı Akdeniz");

genelMerkez = new Array("İstanbul");
subeler = new Array("Ankara", "Bursa", "Eskişehir", "İzmir");
temsilcilikler = new Array("Kayseri", "Trakya", "Doğu Marmara", "Batı Akdeniz");


function getCities() {
    tur = $("#dropCins").val();
    $(sehir).html('');

    if (tur == 'sube') {

        subeler.forEach(function (t) {
            $(sehir).append('<option value='+t+'>' + t + '</option>');
        });
    }
    else if (tur == 'temsilcilik') {
        temsilcilikler.forEach(function (t) {
            $(sehir).append('<option value=' + t + '>' + t + '</option>');
        });
    }
    else {
        genelMerkez.forEach(function (t) {
            $(sehir).append('<option value=' + t + '>' + t + '</option>');
        });
    }

    getAdresses();

}


function Adresler(sehir) {

    switch (sehir) {
        case "İstanbul":
            İstanbulAdres = " <strong> KalDer GENEL MERKEZ</strong><br>" +
                           " Cevizli Mah. Tugay Yolu Cad. Ofisim İstanbul Plazaları No:20/B K:11 N0:65 Maltepe PK:34846<br>" +
                           "T: 0(216) 518 42 84 - F: 0(216) 518 42 86";
            return (İstanbulAdres);
            break;
        case "Ankara":

            AnkaraAdres = " <strong>KalDer ANKARA ŞUBESİ</strong><br>" +
                           " Uğur Mumcu Caddesi Kız Kulesi Sokak 21/6 GOP Ankara<br>" +
                           " T: 0312 447 48 68 - F: 0312 447 48 32";

            return (AnkaraAdres);
            break;
        case "Bursa":

            BursaAdres = "<strong> KalDer BURSA ŞUBESİ</strong><br>" +
                          "Fethiye Mah. Sanayi Cad. No.317 Kat.2 16140 Nilüfer / BURSA<br>" +
                          "T: 0(224)241 60 10 - F: 0(224) 241 58 70";
            return (BursaAdres);
            break;

        case "Eskişehir":

            EskişehirAdres = "<strong>KalDer ESKİŞEHİR ŞUBESİ</strong><br>" +
                             " Hoşnudiye Mah. Nayman Sokak Selka Apt. D:2 ESKİŞEHİR<br>" +
                             "T: 0(222)221 65 52 - F: 0(222)234 34 69";
            return (EskişehirAdres);
            break;

        case "İzmir":

            İzmirAdres = "<strong>KalDer İZMİR ŞUBESİ</strong><br>" +
                           "HALİT ZİYA BULVARI NO:52 K:1 D:102 UZ İŞ MERKEZİ PASAPORT/İZMİR<br>" +
                           "T: 0(232) 482 30 70 - F: 0(232) 482 08 40 ";
            return (İzmirAdres);
            break;

        case "Trakya":

            TrakyaAdres = "<strong>KalDer TRAKYA TEMSİLCİLİĞİ</strong><br>" +
                            "ÇOSB İsmetpaşa Mah. Fatih Blv. No:6 Kapaklı – Çerkezköy, Tekirdağ PK:59510<br>" +
                            "T: 0(282) 758 11 56 - F: 0(282) 518 42 84 <br>M: <a href='mailto:cerkezkoy@kalder.org'>cerkezkoy@kalder.org</a>";

            return (TrakyaAdres);
            break;

        case "Kayseri":

            KayseriAdres = "<strong>KalDer KAYSERİ TEMSİLCİLİĞİ</strong><br>" +
                            "KAYSO- Kocasinan Bulvarı No:161 PK: 38110 Kocasinan / Kayseri<br> " +
                            "T: 0(352) 245 10 50 - F: 0(352) 245 10 40 <br>M: <a href='mailto:kayseri@kalder.org'>kayseri@kalder.org</a>";
            return (KayseriAdres);
            break;

        case "Doğu Marmara":

            MarmaraAdres = "<strong>KalDer DOĞU MARMARA GÖNÜLLÜ TEMSİLCİLİĞİ</strong><br>" +
                             "Kocaeli Sanayi Odası Fuariçi 41040  İzmit / Kocaeli<br>" +
                             "T: 0(262) 315 80 00 - F: 0(262) 321 90 70 <br>M: <a href='mailto:kocaeli@kalder.org'>kocaeli@kalder.org</a>";
            return (MarmaraAdres);
            break;

        case "Batı Akdeniz":

            MarmaraAdres = "<strong>KalDer BATI AKDENİZ GÖNÜLLÜ TEMSİLCİLİĞİ</strong><br>" +
                             "Akdeniz Üniversitesi Kampüsü, Dumlupınar Bulvarı, Enstitüler Binası A Blok Kat:4 07058 Konyaaltı/Antalya<br>" +
                             "T: 0(242) 310 66 44 - F: 0(242) 310 67 86 <br>M: <a href='mailto:antalya@kalder.org'>antalya@kalder.org</a>";
            return (MarmaraAdres);
            break;
    }

    return sehir;
}


function getAdresses() {
    sehiradi = $("#dropPlaces").val();
    $("#txtAdressContent").html('');

    $("#txtAdressContent").html(Adresler(sehiradi));

}


$("#dropCins").change(function () {
    getCities();
});


$("#dropPlaces").change(function () {
    getAdresses();
});


function AverageTime() {
    var end = new Date();
    var OturumBitis = end.getTime();
    var data = (Math.round((OturumBitis - oturumBaslangic) / 1000));

    $.ajax({
        type: "POST",
        url: 'default.aspx/AverageTime',
        //data:  '{time:' + (OturumBitis - oturumBaslangic) +'"}',
        data: '{"time":"' + data + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //success: OnSuccess,
        failure: function (response) {
            alert(response.d);
        }
    })

    function OnSuccess(response) {
        alert(response.d);
    }
}





$(window).on('unload', function () {
    AverageTime();
});


$(window).on('beforeunload', function () {
    AverageTime();


});





$(document).ready(function ($) {
        

    getCities();

    //$('#dropCins option[value=sube]').prop('selected', true);
    //$('#dropPlaces option[value=ankara]').prop('selected', true);


    jQuery('#dropCins').val(1).trigger("chosen:updated");
    


    //Bu arkadaş sağ tuş yasağı getiriyor
    // ***********************************************

    // ***********************************************
    //var isNS = (navigator.appName == "Netscape") ? 1 : 0;
    //var EnableRightClick = 0;
    //if (isNS)
    //    document.captureEvents(Event.MOUSEDOWN || Event.MOUSEUP);
    //function mischandler() {
    //    if (EnableRightClick == 1) { return true; }
    //    else { return false; }
    //}
    //function mousehandler(e) {
    //    if (EnableRightClick == 1) { return true; }
    //    var myevent = (isNS) ? e : event;
    //    var eventbutton = (isNS) ? myevent.which : myevent.button;
    //    if ((eventbutton == 2) || (eventbutton == 3)) return false;
    //}
    //function keyhandler(e) {
    //    var myevent = (isNS) ? e : window.event;
    //    if (myevent.keyCode == 96)
    //        EnableRightClick = 1;
    //    return;
    //}
    //document.oncontextmenu = mischandler;
    //document.onkeypress = keyhandler;
    //document.onmousedown = mousehandler;
    //document.onmouseup = mousehandler;
    //-->



    start = new Date();
    oturumBaslangic = start.getTime();
    //console.log(oturumBaslangic);

    $('#menu').slicknav({
        label: '',
        duration: 1000,
        easingOpen: "easeOutBounce",
        prependTo: '#show_menu'
    });

    //var sharer = new SelectionSharer('p');
    //var sharer = new SelectionSharer('h2');

    $('#cssmenu > ul > li > a').click(function () {
        $('#cssmenu li').removeClass('active');
        $(this).closest('li').addClass('active');
        var checkElement = $(this).next();
        if ((checkElement.is('ul')) && (checkElement.is(':visible'))) {
            $(this).closest('li').removeClass('active');
            checkElement.slideUp('normal');
        }
        if ((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
            $('#cssmenu ul ul:visible').slideUp('normal');
            checkElement.slideDown('normal');
        }
        if ($(this).closest('li').find('ul').children().length == 0) {
            return true;
        } else {
            return false;
        }
    });



    //$.supersized({

    //    //Functionality
    //    slideshow: 1,		//Slideshow on/off
    //    autoplay: 0,		//Slideshow starts playing automatically
    //    start_slide: 1,		//Start slide (0 is random)
    //    random: 0,		//Randomize slide order (Ignores start slide)
    //    slide_interval: 10000,	//Length between transitions
    //    transition: 0, 		//0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
    //    transition_speed: 900,	//Speed of transition
    //    new_window: 1,		//Image links open in new window/tab
    //    pause_hover: 0,		//Pause slideshow on hover
    //    keyboard_nav: 1,		//Keyboard navigation on/off
    //    performance: 1,		//0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
    //    image_protect: 0,		//Disables image dragging and right click with Javascript
    //    image_path: 'img/', //Default image path

    //    //Size & Position
    //    min_width: 0,		//Min width allowed (in pixels)
    //    min_height: 0,		//Min height allowed (in pixels)
    //    vertical_center: 1,		//Vertically center background
    //    horizontal_center: 1,		//Horizontally center background
    //    fit_portrait: 1,		//Portrait images will not exceed browser height
    //    fit_landscape: 0,		//Landscape images will not exceed browser width

    //    //Components
    //    navigation: 1,		//Slideshow controls on/off
    //    thumbnail_navigation: 1,		//Thumbnail navigation
    //    slide_counter: 1,		//Display slide numbers
    //    slide_captions: 1,		//Slide caption (Pull from "title" in slides array)
    //    slides: [		//Slideshow Images
    //                    {

    //                        image: '<%= ResolveUrl("~/") %>upload/bg.jpg', title: '...'
    //                    }
    //    ]

    //});



    $(".carousel-logo").carousel({

        autoplay: true,
        infinite: true,
        nav: true,
        pager: true,
        circular: true,

        delay: 4000,
        speed: 1000,
        direction: 'right',
        easing: 'linear',

        prevText: "",
        nextText: "",

        mousewheel: false,
        touch: true,
        keyboard: false,
        responsive: true

    });


    var carousel = jQuery(".carousel-normal").carousel({
        autoplay: true,
        infinite: true,
        circular: true,
        delay: 4000,
        speed: 1000,
        direction: 'right', //top, right, bottom, left
        easing: 'linear',

        pager: true,

        nav: false,
        navItems: 1,

        prevText: "Geri",
        nextText: "İleri",

        render: function (item) {
            return '<li><img src="' + item.img + '" alt="" /></li>';
        },
        load: null,
        /* function(callback){},*/

        mousewheel: false,
        touch: true,
        keyboard: false,
        responsive: true
    });




    $('.carousel_nav').each(function () {

        $('.carousel_nav_prev').click(function () {
            carousel.carousel('geri');
            return false;
        });


        $('.carousel_nav_next').click(function () {

            carousel.carousel('ileri');
            return false;
        });
    });




    //

    var site = function () {
        this.navLi = $('#menu li').children('ul').hide().end();
        this.init();
    };

    site.prototype = {

        init: function () {
            this.setMenu();
        },

        // Enables the slidedown menu, and adds support for IE6

        setMenu: function () {

            $.each(this.navLi, function () {
                if ($(this).children('ul')[0]) {
                    $(this)
                        .append('<span />')
                        .children('span')
                            .addClass('hasChildren')
                }
            });

            this.navLi.hover(function () {
                // mouseover
                $(this).find('> ul').stop(true, true).slideDown('slow', 'easeOutBounce');
            }, function () {
                // mouseout
                $(this).find('> ul').stop(true, true).hide();
            });

        }

    }

    new site();



    var isOnIOS = navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPhone/i);
    var eventName = isOnIOS ? "pagehide" : "beforeunload";

    window.addEventListener(eventName, function (event) {
        window.event.cancelBubble = true; // Don't know if this works on iOS but it might!

    });




	
	/*$(".nav-tabs li a").click(function(){
		$(".nav-tabs li a img").attr("data-id")
        $(this).find("img").attr("src","olay.png");
		var sonuc =  $(this).attr("data-id");
		alert(sonuc)
		});
	*/
	
"use strict"
/*-----------------------------------------------------------------------------------*/
/* 	FULL SCREEN
/*-----------------------------------------------------------------------------------*/
$('.full-screen').superslides({
});
/*-----------------------------------------------------------------------------------*/
/* 	TESTIMONIAL SLIDER
/*-----------------------------------------------------------------------------------*/
$(".texti-slide").owlCarousel({ 
    items : 1,
	autoplay:true,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	singleItem	: true,
    navigation : true,
	navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
	pagination : true,
	animateOut: 'fadeOut'	
});
/*-----------------------------------------------------------------------------------*/
/*    TESTI SLIDER 2
/*-----------------------------------------------------------------------------------*/
$('.testi-slide').owlCarousel({
    loop:true,
	autoPlay:6000, //Set AutoPlay to 6 seconds 
    items:1,
    margin:10,	
	navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
    responsiveClass:true,
	nav:true,
	animateOut: 'fadeOut'
});
/*-----------------------------------------------------------------------------------*/
/* 	FLEX SLIDER
/*-----------------------------------------------------------------------------------
$('.flex-banner').flexslider({
    animation: "slide",
	directionNav:true,
	controlNav:true,
	slideshow: true,                //Boolean: Animate slider automatically
    slideshowSpeed: 6000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
    animationSpeed: 500,            //Integer: Set the speed of animations, in milliseconds
	autoPlay : true,
    pauseOnHover: true
});*/
/*-----------------------------------------------------------------------------------*/
/* 	FEATURED SLIDER 
/*-----------------------------------------------------------------------------------*/
$('.founder-slide').owlCarousel({
    margin:30,
    nav:true,
	loop: true,
	autoplay:true,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	singleItem	: true,
	navText: ["<i class='ion-ios-arrow-thin-left'></i>","<i class='ion-ios-arrow-thin-right'></i>"],
	pagination : true,
    responsive:{
        300:{
            items:1
        },		
		600:{
            items:2
        }}
});
/*-----------------------------------------------------------------------------------*/
/* 	FEATURED SLIDER 
/*-----------------------------------------------------------------------------------*/

function showSlider() {
    $(".doct-list-style").css('visibility','visible');
	$(".kalDer-magazine").css('visibility','visible');
}


$('.doct-list-style').owlCarousel({
    margin:10,
    nav:true,
	loop: true,
	autoplay:true,
	dots:false,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	singleItem	: false,
	navText: ["<i class='ion-chevron-left'></i>","<i class='ion-chevron-right'></i>"],
	pagination : false,
	onInitialized: showSlider,
    responsive:{
        300:{
            items:1,
			dots:false,
			nav:true

        },		
		600:{
            items:2
        },
		800:{
            items:2
        },
		1000:{
            items:3
        },
		1200:{
            items:4
        }}
});

$('.kalDer-magazine').owlCarousel({
    margin: 10,
    nav:true,
	loop: true,
	autoplay:true,
	dots:false,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	singleItem	: true,
	navText: ["<i class='ion-chevron-left'></i>","<i class='ion-chevron-right'></i>"],
	pagination : true,
	onInitialized: showSlider,
	responsiveClass: true,
	lazyLoad: true,
    responsive:{
        0: {
            merge: true,
            center: true,
            autoWidth:true,
            margin: 2,
            items: 1,
            
        },		
		600:{
            items:2
        },
		800:{
            items:2
        },
		1000:{
            items:3
        },
		1200:{
            items:5
        }}
});


/*
setTimeout(function() {
    $(".doct-list-style").css('visibility','visible');
}, 3000);
*/
/*-----------------------------------------------------------------------------------*/
/* 	FOOTER REFERENCE SLIDER 
/*-----------------------------------------------------------------------------------*/
$('.brand-list-style').owlCarousel({
    margin:40,
    nav:false,
	loop: true,
	autoplay:true,
	dots:false,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	singleItem	: true,
	navText: ["<i class='ion-chevron-left'></i>","<i class='ion-chevron-right'></i>"],
	pagination : false,
    responsive:{
        300:{
            items:1
        },		
		600:{
            items:3
        },
		800:{
            items:3
        },
		1000:{
            items:4
        },
		1200:{
            items:4
        }}
});



/*-----------------------------------------------------------------------------------*/
/* 	SERVICES SLIDER 
/*-----------------------------------------------------------------------------------*/
$('.services-slide').owlCarousel({
    margin:30,
    nav:true,
	loop: true,
	autoplay:true,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	singleItem	: true,
	navText: ["<i class='ion-ios-arrow-thin-left'></i>","<i class='ion-ios-arrow-thin-right'></i>"],
	pagination : true,
    responsive:{
        300:{
            items:1
        },	
		600:{
            items:3
        }}
});
/*-----------------------------------------------------------------------------------*/
/* 	REFERENCES SLIDER 
/*-----------------------------------------------------------------------------------*/
$('.reference-list-style').owlCarousel({
    margin:30,
    nav:false,
	loop: false,
	autoplay:false,
	dots:false,
	//autoplayTimeout:5000,
	//autoplayHoverPause:true,
	singleItem	: true,
	//navText: ["<i class='ion-chevron-left'></i>","<i class='ion-chevron-right'></i>"],
	//pagination : false,
    responsive:{
        300:{
            items:1
        },		
		600:{
            items:2
        },
		800:{
            items:3
        },
		1000:{
            items:4
        },
		1200:{
            items:4
        }}
});

/*-----------------------------------------------------------------------------------*/
/* 	CAMPAIGN SLIDER 
/*-----------------------------------------------------------------------------------*/
$('.campaign-list-style').owlCarousel({
    margin:0,
    nav:true,
	loop: true,
	autoplay:true,
	animateOut: 'slideOutDown',
    animateIn: 'flipInX',
	dots:false,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	singleItem	: true,
	navText: ["<i class='ion-chevron-left'></i>","<i class='ion-chevron-right'></i>"],
	pagination : false,
    responsive:{
        300:{
			items:1,
			nav:false,
			dots:true
        },		
		600:{
            items:1
        },
		800:{
            items:1
        },
		1000:{
            items:1
        },
		1200:{
            items:1
        }}
});

/*-----------------------------------------------------------------------------------*/
/* 	INNER PAGE SLIDER 
/*-----------------------------------------------------------------------------------*/
/*
$("#slide-cont").owlCarousel({
    items: 1,
});

        // 1) ASSIGN EACH 'DOT' A NUMBER
			var dotcount = 1;
	 
			$('.owl-dot').each(function() {
			  $( this ).addClass( 'dotnumber' + dotcount);
			  $( this ).attr('data-info', dotcount);
			  dotcount=dotcount+1;
			});
			
			 // 2) ASSIGN EACH 'SLIDE' A NUMBER
			var slidecount = 1;
	 
			$('.owl-item').not('.cloned').each(function() {
			  $( this ).addClass( 'slidenumber' + slidecount);
			  slidecount=slidecount+1;
			});
			
			// SYNC THE SLIDE NUMBER IMG TO ITS DOT COUNTERPART (E.G SLIDE 1 IMG TO DOT 1 BACKGROUND-IMAGE)
			$('.owl-dot').each(function() {
			
          var grab = $(this).data('info');

          var slidegrab = $('.slidenumber'+ grab +' img').attr('src');
          console.log(slidegrab);

          $(this).css("background-image", "url("+slidegrab+")");  

      });
			
			// THIS FINAL BIT CAN BE REMOVED AND OVERRIDEN WITH YOUR OWN CSS OR FUNCTION, I JUST HAVE IT
        // TO MAKE IT ALL NEAT 
			var amount = $('.owl-dot').length;
			var gotowidth = 100/amount;
			
			$('.owl-dot').css("width", gotowidth+"%");
			var newwidth = $('.owl-dot').width();
			$('.owl-dot').css("height", newwidth+"px");

*/


/*-----------------------------------------------------------------------------------*/
/* 		Active Menu Item on Page Scroll
/*-----------------------------------------------------------------------------------*/
$('.scroll a').click(function() {  
	$('html, body').animate({scrollTop: $(this.hash).offset().top -80}, 1500);
return false;
});

/*-----------------------------------------------------------------------------------*/
/* 		Why-Choose Scroll for Mobile
/*-----------------------------------------------------------------------------------*/
if($(window).width() <= 440){
	$('#why-choose ul.nav li a').click(function() {  
		$('html, body').animate({scrollTop: $("#why-choose").offset().top +215}, 800);
		return true;
	});
}

/*-----------------------------------------------------------------------------------*/
/* 		Teklif Formu Show/Hide for Mobile
/*-----------------------------------------------------------------------------------*/
$('.menu-toggle').click(function() {  
	if($(this).find("i").hasClass("fa-times")){
		$(this).find("i").removeClass("fa-times");
		$(this).find("i").addClass("fa-bars");
	}else{
		$(this).find("i").removeClass("fa-bars");
		$(this).find("i").addClass("fa-times");
	}
});

/*-----------------------------------------------------------------------------------*/
/* 		Teklif Formu Show/Hide for Mobile
/*-----------------------------------------------------------------------------------*/
$('#formbtn').click(function() {  
	if($(this).hasClass("fa-arrow-up")){
		$(this).removeClass("fa-arrow-up");
		$(this).addClass("fa-arrow-down");
	}else{
		$(this).removeClass("fa-arrow-down");
		$(this).addClass("fa-arrow-up");
	}
	$('#teklifPart2').toggleClass("showthis");
	$('#teklifPart3').toggleClass("showthis");
});

/*-----------------------------------------------------------------------------------*/
/*    DATE PICKER
/*-----------------------------------------------------------------------------------*/
$("#datepicker").datepicker({
	inline: true,
	dateFormat: "dd-mm-yy",//tarih formatı yy=yıl mm=ay dd=gün
			changeMonth: true,//ayı elle seçmeyi aktif eder
			changeYear: true,//yılı elee seçime izin verir
			dayNames:[ "Pazar", "Pazartesi", "Salı", "Çarşamba", "perşembe", "Cuma", "Cumartesi" ],
			monthNamesShort: [ "Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık" ],
			dayNamesMin: [ "Pa", "Pzt", "Sa", "Çar", "Per", "Cum", "Cmt" ],//kısaltmalar
});
/*-----------------------------------------------------------------------------------*/
/* 		Parallax
/*-----------------------------------------------------------------------------------*/
jQuery.stellar({
   horizontalScrolling: false,
   scrollProperty: 'scroll',
   positionProperty: 'position'
});
/*-----------------------------------------------------------------------------------*/
/*	ISOTOPE PORTFOLIO
/*-----------------------------------------------------------------------------------*/
var $container = $('.time-table .doc-avai');
    $container.imagesLoaded(function () {
        $container.isotope({
            itemSelector: '.item-iso',
            layoutMode: 'masonry'
        });
	});
    $('.filter li a').click(function () {
        $('.filter li a').removeClass('active');
        $(this).addClass('active');
        var selector = $(this).attr('data-filter');
        $container.isotope({
            filter: selector
        });
        return false;
    });



   
		
});

/*-----------------------------------------------------------------------------------*/
/*    SEARCH BUTTON
/*-----------------------------------------------------------------------------------*/
$('#search-btn').click(function(){
  if($("#search-container input").hasClass("focus")){
	  $("#search-container input").removeClass("focus").fadeOut();
  }else{
	  $("#search-container input").addClass("focus").fadeIn();
  }
});

/*-----------------------------------------------------------------------------------*/
/*    STICKY SOCIAL OPEN/HIDE
/*-----------------------------------------------------------------------------------*/
$('#sticky-social span').click(function(){
  if($(this).hasClass("opened")){
	  $(this).removeClass("opened");
	  $("#sticky-social").animate({"right":"-55px"}, "slow");
  }else{
	 $(this).addClass("opened");
	  $("#sticky-social").animate({"right":"0px"}, "slow");
  }
});


/*-----------------------------------------------------------------------------------*/
/*    SIDE MENU SHOW/HIDE FOR MOBILE/*-----------------------------------------------------------------------------------*/
$('i#mobile-sidemenu-btn').click(function(){
  if($(this).hasClass("opened")){
	  $(this).removeClass("opened");
	  $(".sidemenus").slideUp();
  }else{
	 $(this).addClass("opened");
	  $(".sidemenus").slideDown();
  }
});

/*
document.addEventListener("click",function(e){
	var clickedID = e.target.id;
	alert(clickedId);
	if (clickedID != "search") {
		if($("#search-container input").hasClass("focus")){
			  $("#search-container input").removeClass("focus").fadeOut();
		}
  }
});
*/

/*-----------------------------------------------------------------------------------*/
/*    POPUP VIDEO
/*-----------------------------------------------------------------------------------*/
$('.popup-vedio').magnificPopup({
	type: 'inline',
	fixedContentPos: false,
	fixedBgPos: true,
	overflowY: 'auto',
	closeBtnInside: true,
	preloader: true,
	midClick: true,
	removalDelay: 300,
	mainClass: 'my-mfp-slide-bottom'
});
$('.gallery-pop').magnificPopup({
	delegate: 'a',
	type: 'image',
	tLoading: 'Loading image #%curr%...',
	mainClass: 'mfp-img-mobile',
	gallery: {
		enabled: true,
		navigateByImgClick: true,
		preload: [0,1] // Will preload 0 - before current, and 1 after the current image
	},
	image: {
		tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
		titleSrc: function(item) {
			return item.el.attr('title') + '';}}
});
/*-----------------------------------------------------------------------------------*/
/* 	POPUP IMAGE GALLERY
/*-----------------------------------------------------------------------------------*/
$('.popup-gallery').magnificPopup({
	delegate: 'a',
	type: 'image',
	tLoading: 'Loading image #%curr%...',
	mainClass: 'mfp-img-mobile',
	gallery: {
	enabled: true,
	navigateByImgClick: true,
	preload: [0,1]},
	image: {
	tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
	titleSrc: function(item) {
	return item.el.attr('title') + '';}}
});
/*-----------------------------------------------------------------------------------*/
/*  ISOTOPE PORTFOLIO
/*-----------------------------------------------------------------------------------*/
var $container = $('.portfolio-wrapper .items');
	$container.imagesLoaded(function () {
	   $container.isotope({
	     itemSelector: '.item',
	     layoutMode: 'masonry'
 });
});
$('.filter li a').on("click",function() {
    $('.filter li a').removeClass('active');
    $(this).addClass('active');
    var selector = $(this).attr('data-filter');
    $container.isotope({
    filter: selector
	   });
    return false;
});

/*-----------------------------------------------------------------------------------
    Animated progress bars
/*-----------------------------------------------------------------------------------
$('.progress-bars').waypoint(function() {
  $('.progress').each(function(){
        $(this).find('.progress-bar').animate({
                width:$(this).attr('data-percent')
            },1500);
        });
        }, { offset: '100%',
             triggerOnce: true 
});*/
 

//Start after window load so not messy jumping effect.
// else menu appear top 0 left 0 than jump calculated position
// TODO : If page loads when scrolled way down , menu placed wrong position.So check position for it.



  jQuery(document).ready(function($) {
 
 
 
$( "ul.ownmenu li" ).mouseover(function() {
//  $(".menu_effect").css("display", "block")
    $(".menu_effect").css({"visibility": "visible", "opacity": "0.8", "transition":"visibility 0s, opacity 0.5s linear;"});
	
	$(".menu_effect img").css("width","40%");
});

$( "ul.ownmenu li" ).mouseout(function() {
  
  $(".menu_effect").css({"visibility": "hidden", "opacity": "0", "transition":"visibility 0s, opacity 0.5s linear;"});
  	$(".menu_effect img").css("width","10%");
});
 
 
        $('#contentCarousel').carousel({
                interval: 5000
        });
 
        $('#carousel-text').html($('#slide-content-0').html());
 
        //Handles the carousel thumbnails
       $('[id^=carousel-selector-]').click( function(){
            var id = this.id.substr(this.id.lastIndexOf("-") + 1);
            var id = parseInt(id);
            $('#contentCarousel').carousel(id);
        });
 
 
        // When the carousel slides, auto update the text
        $('#contentCarousel').on('slid.bs.carousel', function (e) {
                 var id = $('.item.active').data('slide-number');
                $('#carousel-text').html($('#slide-content-'+id).html());
        });

		
		//Accordion	
		$(function() {
			var Accordion = function(el, multiple) {
				this.el = el || {};
				this.multiple = multiple || false;

				// Variables privadas
				var links = this.el.find('.link');
				// Evento
				links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
			}

			Accordion.prototype.dropdown = function(e) {
				var $el = e.data.el;
					$this = $(this),
					$next = $this.next();

				$next.slideToggle();
				$this.parent().toggleClass('open');

				if (!e.data.multiple) {
					$el.find('.submenu').not($next).slideUp().parent().removeClass('open');
				};
			}	

			var accordion = new Accordion($('#accordion'), false);
		});

		
		});



//Bootstrap Table Extension for Kalder

  function fnExcelReport() {
      var tab_text = '<table border="1px" style="font-size:20px" ">';
      var textRange;
      var j = 0;
      var tab = document.getElementById('registerForm'); // id of table
      var lines = tab.rows.length;

      // the first headline of the table
      if (lines > 0) {
          tab_text = tab_text + '<tr bgcolor="#DFDFDF">' + tab.rows[0].innerHTML + '</tr>';
      }

      // table data lines, loop starting from 1
      for (j = 1 ; j < lines; j++) {
          tab_text = tab_text + "<tr>" + tab.rows[j].innerHTML + "</tr>";
      }

      tab_text = tab_text + "</table>";
      tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");             //remove if u want links in your table
      tab_text = tab_text.replace(/<img[^>]*>/gi, "");                 // remove if u want images in your table
      tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, "");    // reomves input params
      // console.log(tab_text); // aktivate so see the result (press F12 in browser)

      var ua = window.navigator.userAgent;
      var msie = ua.indexOf("MSIE ");

      // if Internet Explorer
      if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
          txtArea1.document.open("txt/html", "replace");
          txtArea1.document.write(tab_text);
          txtArea1.document.close();
          txtArea1.focus();
          sa = txtArea1.document.execCommand("SaveAs", true, "DataTableExport.xls");
      }
      else // other browser not tested on IE 11
          sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

      return (sa);
  }



 


  $(document).ready(function () {

      $('.social-feed-facebook').socialfeed({
          // FACEBOOK
          facebook: {
              accounts: ['@TurkiyeKaliteDernegi', '!TurkiyeKaliteDernegi'],
              limit: 3,
              access_token: '117775548885281|d032eb272149a5fcf8ea9f3de0927458'
          },

          // GENERAL SETTINGS
          template: "/template.html",

          length: 220, //Integer: For posts with text longer than this length, show an ellipsis.
          show_media: false,
          date_locale: "tr"

      });

      $('.social-feed-Instagram').socialfeed({
          // INSTAGRAM
          instagram: {
              accounts: ['@turkiyekalitedernegi', ''],
              limit: 2,
              client_id: 'ed2d2e41d2e04ee1a49303b7aaa4e608',
              access_token: '438234801.ed2d2e4.e6565d43e3724de38c7fde44b67042ea'
          },

          // GENERAL SETTINGS
          template: "/template.html",

          length: 220, //Integer: For posts with text longer than this length, show an ellipsis.
          show_media: false,
          date_locale: "tr"
      });

      $('.social-feed-twitter').socialfeed({
          twitter: {
              accounts: ['@KaliteDernegi', '!KaliteDernegi'],
              limit: 3,
              consumer_key: 'QuHuprYlBlXwgExjdRyPetpBr', // make sure to have your app read-only
              consumer_secret: 'pdLEjR8QqbwsUZapWkGRdLitVwcTyignqmpHlROVl5KIIYy14u', // make sure to have your app read-only
          },

          // GENERAL SETTINGS
          template: "/template.html",

          length: 220, //Integer: For posts with text longer than this length, show an ellipsis.
          show_media: false,
          date_locale: "tr"
      });


  });