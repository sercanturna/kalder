﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder
{
    public partial class references : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();

        protected void Page_Load(object sender, EventArgs e)
        {
            GetReference();


           // ltrlTel.Text = db.GetDataCell("Select Tel from GenelAyarlar");

            /* Facebook Likebox 
            string fblink = db.GetDataCell("Select Facebook from GenelAyarlar");

            if (fblink == "" || fblink == null)
                pnl_facebook_like.Visible = false;
            else
                LtrlFbox.Text = "<div class=\"fb-like-box\" data-href=\"" + fblink + "\" data-width=\"218\" data-colorscheme=\"light\" data-show-faces=\"true\" data-header=\"true\" data-stream=\"false\" data-show-border=\"true\"></div>";
            */

            string Baslik = db.GetDataCell("Select refHeader from GenelAyarlar");
            ltrlBaslik.Text = Baslik;
            ltrlBaslik2.Text = ltrlBaslik.Text;

            

            GetSidebarModules();
            GetContentModules();
            GetFooterModules();

        }

        public void GetSidebarModules()
        {
            DataTable dt = db.GetDataTable("select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=5 and mss.IsActive=1 and m.SectionId=2 order by mss.OrderNo");
            rptSidebarModules.DataSource = dt;
            rptSidebarModules.DataBind();

            if (dt.Rows.Count == 0)
            {
                sideBar.Visible = false;
                pageContent.Style.Add("width", "100%");
            }

        }

        public void GetContentModules()
        {
            DataTable dt = db.GetDataTable("select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=5 and mss.IsActive=1 and m.SectionId=1 order by mss.OrderNo");
            rptContentModules.DataSource = dt;
            rptContentModules.DataBind();
        }

        public void GetFooterModules()
        {
            DataTable dt = db.GetDataTable("select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=5 and mss.IsActive=1 and m.SectionId=5 order by mss.OrderNo");
            rptFooterModules.DataSource = dt;
            rptFooterModules.DataBind();
        }

        public void GetReference()
        {
            DataTable dt = db.GetDataTable("Select * from Reference where Status=1 order by OrderNo");
            CollectionPager1.DataSource = dt.DefaultView;
            CollectionPager1.BindToControl = rptRef;
            rptRef.DataSource = CollectionPager1.DataSourcePaged;
            rptRef.DataBind();
        }


        public void Page_PreInit()
        {
            // Set the Theme for the page. Post-data is not currently loaded
            // Use trace="true" to see the Form collection
            //this.Theme = db.GetDataCell("Select Tema from GenelAyarlar");
            //if (Request.Form != null && Request.Form.Count > 0)
            //this.Theme = this.Request.Form[4].Trim();
        }



        protected void rptSidebarModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);


                    //if (drv["ModuleId"].ToString() == "11")
                    //{
                    //    PropertyInfo[] info = uc.GetType().GetProperties();


                    //    foreach (PropertyInfo item in info)
                    //    {
                    //        if (item.CanWrite)
                    //        {
                    //            if (item.Name == "PageIDwithURL")
                    //                item.SetValue(uc, PageURL, null);

                    //        }
                    //    }

                    //}
                }
            }




        }

        protected void rptContentModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);


                    //PropertyInfo[] info = uc.GetType().GetProperties();


                    //foreach (PropertyInfo item in info)
                    //{
                    //    if (item.CanWrite)
                    //    {
                    //        if (item.Name == "PageIDwithURL")
                    //            item.SetValue(uc, PageURL, null);

                    //    }
                    //}

                }
            }




        }

        protected void rptFooterModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);
                }
            }




        }
    }
}