﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Temalar
/// </summary>
public class Temalar
{
    public static ArrayList TumTemalariCek()
    {
        ArrayList tmp = new ArrayList();

        // klasorBilgi değişkeni, App_Themes klasöre girmemi sağlıyor.
        // GetDirectories fonksiyonu ile App_Themes klasörü içindeki
        // klasörleri çekiyoruz. Bu çektiğimiz klasörler temalarımızın
        // adı oluyor.
        DirectoryInfo klasorBilgi = new DirectoryInfo(HttpContext.Current.Server.MapPath("App_Themes"));
        DirectoryInfo[] klasorListesi = klasorBilgi.GetDirectories();

        foreach (var directoryInfo in klasorListesi)
        {
            tmp.Add(directoryInfo.Name);
        }

        return tmp;
    }
}