﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

/// <summary>
/// Summary description for FileUploadWithoutResize
/// </summary>
/// 
namespace Kalder
{
    public class FileUploadWithoutResize
    {
        public FileUploadWithoutResize()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static string SaveFile(FileUpload FuName, string Path, string FileGroup)
        {
            string FileName = "";
            HttpPostedFile postedFile = FuName.PostedFile;

            if (postedFile != null && postedFile.ContentLength > 0)
            {
                string filePath = postedFile.FileName;
                FileInfo clientFileInfo = new FileInfo(filePath);
                FileName = clientFileInfo.Name;

                //Ana Dizin
                string ServerPath = HttpContext.Current.Server.MapPath("~/");
                //Yeni Yol
                string newPath = ServerPath + "/" + Path + "/" + FileName;
                postedFile.SaveAs(Ayarlar.FileName(newPath));
            }
            return FileName;
        }
    }
}