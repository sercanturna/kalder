﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Routing;
using System.Data;
using System.IO;
using System.Text;
using System.Net;


namespace Kalder
{
    public class Global : System.Web.HttpApplication
    {
        DbConnection db = new DbConnection();
        string ip;
  
        protected void Application_Start(object sender, EventArgs e)
        {
            RoutingAyarlari(RouteTable.Routes);
            Application["OnlineUsers"] = 0;

           

        }


        protected void Application_BeginRequest(object sender, EventArgs e)
        {



            if (HttpContext.Current.Request.Url.ToString().ToLower().Contains(
  "http://kalder.org"))
            {
                HttpContext.Current.Response.Status =
                    "301 Moved Permanently";
                HttpContext.Current.Response.AddHeader("Location",
                    Request.Url.ToString().ToLower().Replace(
                        "http://kalder.org",
                        "http://www.kalder.org"));
            }

           



            // Her seferinde kontrol etmesin ve sonsuz döngüye girmesin ;)
            ///// ************  LİSANSLAMA İŞLEMİ   **************////

            // Lisans Kontrolü yapmak için cplus hosting hesabımız altında yüklü olan domain listesine bağlanıyorum.
            WebClient client = new WebClient();
            Stream stream = client.OpenRead("http://www.cplus.com.tr/cportal_allowed_domains.txt");
            StreamReader reader = new StreamReader(stream);
            String content = reader.ReadToEnd();

       
            // Lisans Kontrolü yapmak için mevcut sitenin domain adını otomatik olarak alarak başındaki www. ibaresini kaldırıyoruz.
            string DomainName = HttpContext.Current.Request.Url.Host;
            if (DomainName.Contains("www."))
            {
                DomainName = DomainName.Remove(0, 4);
            }

            // Burada 301 Redirect kodunu işliyoruz.
            //if (HttpContext.Current.Request.Url.ToString().ToLower().Contains("http:// "))
            //{
            //    HttpContext.Current.Response.Status = "301 Moved Permanently";
            //    HttpContext.Current.Response.AddHeader("Location", Request.Url.ToString().ToLower().Replace("http://website.net", "http://www.website.net"));
            //}



            // Lisanslı domainler listesinde mevcut domain yer almıyorsa lisans sayfasına gidecek.
            if (!content.Contains(DomainName))
            {
                Response.Redirect("http://www.cplus.com.tr/lisans.html");
            }

            ///// ************  LİSANSLAMA İŞLEMİ   **************////
          
        }


        public static void RoutingAyarlari(RouteCollection routes)
        {
            //string[] allowedMethods = { "GET", "POST" };
            //HttpMethodConstraint methodConstraints = new HttpMethodConstraint(allowedMethods);

            // PDF veya herhangi bir fiziksel dosyaya link verilebilmesi için bu ignore kodlarını yazdık.
            routes.Ignore("{resource}.axd/{*pathInfo}");
            routes.Ignore("{upload}/{*pathInfo}", new { upload = "content" });
            routes.Ignore("upload/{*pathInfo}");
            routes.RouteExistingFiles = false;

            //routes.Ignore("{url}/{*pathInfo}", new { url = "https:/" });

            routes.MapPageRoute("Urunler", "urunler", "~/product_list.aspx");
            routes.MapPageRoute("UrunlerKategori", "urunler/{ProductCat}", "~/product_list.aspx");
            routes.MapPageRoute("UrunDetay", "urunler/{ProductCat}/{ProductSeoUrl}", "~/product_details.aspx");
            routes.MapPageRoute("AnaSayfa", "anasayfa", "~/default.aspx");
            routes.MapPageRoute("BizeUlasin", "kalder_iletisim", "~/contact_us.aspx");
            routes.MapPageRoute("Referanslar", "referanslar", "~/references.aspx");

            DbConnection db = new DbConnection();

            string BlogLink = db.GetDataCell("Select newsHeader from GenelAyarlar");

            routes.MapPageRoute("Haberler", BlogLink, "~/news.aspx");
            routes.MapPageRoute("KaliteDergisi", "once_kalite_dergisi", "~/dergi.aspx");

            routes.MapPageRoute("Ebultenler", "e_bultenler", "~/bultenler.aspx");
            

            routes.MapPageRoute("YönetimKurulu", "yonetim_kurulu", "~/yonetim_kurulu.aspx");

            routes.MapPageRoute("EtkinlikTakvimi", "etkinlik_takvimi", "~/etkinlik_takvimi.aspx");
            routes.MapPageRoute("EtkinlikDetay", "etkinlikler/{SeoUrl}", "~/event_details.aspx");

            routes.MapPageRoute("Kutuphane", "sanal_kutuphane", "~/sanal_kutuphane.aspx");
            
            
            routes.MapPageRoute("HaberKategori", BlogLink+"/{NewsCat}", "~/news.aspx");
            routes.MapPageRoute("Galeri", "fotogaleri", "~/photo_gallery.aspx");
            routes.MapPageRoute("GaleriKategori", "fotogaleri/{GalleryCat}", "~/photo_gallery.aspx");
            routes.MapPageRoute("haberDetay", "{NewsCat}/{HaberId}/{HaberBaslik}", "~/news_details.aspx");
            routes.MapPageRoute("StaticSayfa", "{PageURL}", "~/static_pages.aspx");
            routes.MapPageRoute("aramasayfasi", "{Kategori}-results/{ArananKelime}", "~/search.aspx");
            //  routes.MapPageRoute("kategori", "kategori_{KategoriAdi}_{KategoriId}.html", "~/Kategori.aspx");
            //  routes.MapPageRoute("yazidetay", "{yazi}_yazisi_{YaziId}.html", "~/YaziDetay.aspx");
            //  routes.MapPageRoute("fotogaleri", "fotogaleri.html", "~/FotoGaleri.aspx");
            routes.MapPageRoute("Admin", "AdminV2/default.aspx", "~/AdminV2/default.aspx");
            routes.MapPageRoute("Markalar", "AdminV2/brands.cplus", "~/AdminV2/brands.aspx");
            routes.MapPageRoute("UrunDuzenle", "AdminV2/edit_product_{pId}", "~/AdminV2/edit_product.aspx");

            routes.MapPageRoute("YKDuzenle", "AdminV2/edit_yk_{pId}", "~/AdminV2/edit_yk.aspx");




            
        }
        protected void Session_Start(object sender, EventArgs e)
        {
            
            Application.Lock();
            Application["OnlineUsers"] = Convert.ToInt32(Application["OnlineUsers"]) + 1;

                        //string IpAdres = Application["OturumIP"].ToString();
                        //string Tarih = DateTime.Now.ToShortDateString();

                        //DataRow dr = db.GetDataRow("Select * from Stats where Ip='" + IpAdres + "' and Date='" + Tarih + "'");
            
                        //if(dr !=null)

            
            Application.UnLock();
        }
        //public void Init(HttpApplication context)
        //{
        //    context.BeginRequest += (Application_BeginRequest);
        //}

        //protected void Application_BeginRequest(object source, EventArgs e)
        //{
        //    var context = ((HttpApplication)source).Context;
        //    ip = context.Request.UserHostAddress;
        //}

        
       

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {
            Application.Lock();
            Application["OnlineUsers"] = Convert.ToInt32(Application["OnlineUsers"]) - 1;
            Application.UnLock();
        }

        protected void Application_End(object sender, EventArgs e)
        {
            

        }
    }
}