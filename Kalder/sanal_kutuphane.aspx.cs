﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder
{
    public partial class sanal_kutuphane : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string PageId;
        protected void Page_Load(object sender, EventArgs e)
        {

            getBooks();

            GetSidebarModules();
            PageId = "4119";

            hdnPid.Value = PageId;

            GetPage(PageId);
        }


        public void GetPage(string PageId)
        {
            //rptBreadcrumb.DataSource = db.GetDataTable("select * from Pages where PageId=" + PageId +" and PageIsActive=1 and ShowMenu=1 order by OrderNumber"); bu şekilde breadcumb da sayfa gözükmüyor
            rptBreadcrumb.DataSource = db.GetDataTable("select * from Pages where PageId=" + PageId + " order by OrderNumber");
            rptBreadcrumb.DataBind();
        }

        public void GetSidebarModules()
        {
            DataTable dt = db.GetDataTable("select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=2 and mss.IsActive=1 and m.SectionId=2 order by mss.OrderNo");
            rptSidebarModules.DataSource = dt;
            rptSidebarModules.DataBind();

            if (dt.Rows.Count == 0)
            {
                sideBar.Visible = false;
                //  pageContent.Style.Add("width", "100%");
            }

        }

        protected void rptBreadcrumb_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;

                PlaceHolder treeMenu = (PlaceHolder)e.Item.FindControl("treeMenu");
                treeMenu.Controls.Clear();

                //DataSet ds = db.GetDataSet("Select * From Pages where PageId=" + PageId + "and PageIsActive=1 and ShowMenu=1"); bu şekilde breadcumb da sayfa gözükmüyor
                DataSet ds = db.GetDataSet("Select * From Pages where PageId=" + PageId);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if ((int)dr["ParentPageId"] != 0)
                    {
                        int Id = (int)dr["PageId"];
                        getParent(Convert.ToInt32(PageId), treeMenu);
                    }

                    treeMenu.Controls.Add(new LiteralControl("<li>" + dr["PageName"].ToString() + "</li>"));
                }
            }
        }

        private int getParent(int PageId, PlaceHolder treeMenu)
        {
            string parentPageId = db.GetDataCell("select ParentPageId from Pages where PageId=" + PageId + "order by PageId asc");

            // DataSet ds = db.GetDataSet("select * from Pages where PageId=" + parentPageId + "and PageIsActive=1 and ShowMenu=1");bu şekilde breadcumb da sayfa gözükmüyor
            DataSet ds = db.GetDataSet("select * from Pages where PageId=" + parentPageId);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string ParentPageName = db.GetDataCell("Select PageName from Pages where PageId=" + parentPageId);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (parentPageId != "0")
                    {
                        int id = Convert.ToInt32(parentPageId);
                        getParent(id, treeMenu);
                    }
                    treeMenu.Controls.Add(new LiteralControl("<li><a href=" + Page.ResolveUrl("~") + dr["PageURL"].ToString() + ">" + dr["PageName"].ToString() + "</a></li>"));
                }
                return 0;
            }
            return 0;
        }

        protected void rptSidebarModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);


                    if (drv["ModuleId"].ToString() == "11")
                    {
                        PropertyInfo[] info = uc.GetType().GetProperties();


                        foreach (PropertyInfo item in info)
                        {
                            if (item.CanWrite)
                            {
                                if (item.Name == "PageIDwithURL")
                                    item.SetValue(uc, "sanal_kutuphane", null);

                            }
                        }

                    }
                }
            }




        }

        public void getBooks()
        {
            DataTable dt = db.GetDataTable("Select * from SanalKutuphane where Durum=1");

            rptKutuphane.DataSource = dt;
            rptKutuphane.DataBind();
        }

    

        protected void rptKutuphane_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "show")
            {
                string Id = Convert.ToString(e.CommandArgument);

                DataRow dr = db.GetDataRow("Select * from SanalKutuphane where Id="+Id);

                lblModalTitle.Text = Ayarlar.OzetCek(dr["KitapAdi"].ToString(),77);

                string KitapNo, KitapAdi, Yazar, Yayinci, Aciklama, Telif, Sayfa, ISBNno, Dolapno, Rafno, AnahtarKelimeler, Resim;

KitapNo = dr["KitapNo"].ToString();
KitapAdi = dr["KitapAdi"].ToString();
Yazar = dr["Yazar"].ToString();
Yayinci = dr["Yayinci"].ToString();
Aciklama = dr["Aciklama"].ToString();
Telif = dr["TelifYil"].ToString();
Sayfa = dr["SayfaSayi"].ToString();
ISBNno = dr["ISBNNo"].ToString();
Dolapno = dr["DolapNo"].ToString();
Rafno = dr["RafNo"].ToString();
AnahtarKelimeler = dr["AnahtarKelime"].ToString();
Resim = dr["Resim"].ToString();

lblModalBody.Text = "<table class=\"table table-striped\">"+
"<tr><td style=\"width:120px\">Kitap No</td><td>" + KitapNo + "</td></tr>" +
"<tr><td style=\"width:120px\">Kitap Adı</td><td>"+KitapAdi+"</td></tr>"+
"<tr><td>Yazar</td><td>"+Yazar+"</td></tr>"+
"<tr><td>Yayıncı</td><td>"+Yayinci+"</td></tr>"+
"<tr><td>Açıklama</td><td>"+Aciklama+"</td></tr>"+
"<tr><td>Telif Hakkı Yılı</td><td>"+Telif+"</td></tr>"+
"<tr><td>Sayfa Sayısı</td><td>"+Sayfa+"</td></tr>"+
"<tr><td>ISBN Numarası</td><td>"+ISBNno+"</td></tr>"+
"<tr><td>Dolap Numarası</td><td>"+Dolapno+"</td></tr>"+
"<tr><td>Raf Numarası</td><td>"+Rafno+"</td></tr>"+
"<tr><td>Anahtar Kelimeler</td><td>"+AnahtarKelimeler+"</td></tr>" +
"</table>";
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#myModal').modal();", true);
                upModal.Update();


               
            }
        }

    }
}