

$(document).ready(function () {

    $('.swtch-opener').click(function (e) {
        if ($('#style-switcher').hasClass('swclose')) {
            $('#style-switcher').removeClass('swclose');
            $('#style-switcher').addClass('swopen');


        }
        else {
            $('#style-switcher').removeClass('swopen');
            $('#style-switcher').addClass('swclose');
        }
        e.preventDefault();
    });

 
    //if ($.cookie("pattern")) {
    //    //$.supersized().remove();
    //    //$('#supersized').empty().remove();
    //    //$('#supersized-loader').empty().remove();
    //    $('body').addClass($.cookie("pattern"));
    //}

  
    $('.ul-pattern li').click(function () {
      $('#supersized').empty().remove();
      changePattern = ($(this).attr('id'));
      $('body').removeAttr('class').attr('class', '');
      $('body').addClass(changePattern);
    
      $.cookie('pattern', $(this).attr('id'), { expires: 1 });
    });



    $('.ul-background li').click(function () {
        $('#supersized').empty().remove();
        changeBackground = ($(this).attr('id'));
        $('body').removeAttr('class').attr('class', '');
        $('body').addClass(changeBackground);

        $.cookie('pattern', $(this).attr('id'), { expires: 1 });
    });


    $('.stylereset').click(function (e) {
        $.cookie('css', '', { expires: 1 });
        //$('link[id="skin"]').attr('href', 'css/main.css');
        $.cookie('pattern', '', { expires: 1 });
        $('body').removeClass();
        $('body').addClass("pattern2");
        e.preventDefault();
    });


   
});
