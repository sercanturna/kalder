﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CportalV2
{
    public class MenuNode
    {

        public int Id { get; set; }
        public int ParentId { get; set; }
        public string KatAdi { get; set; }
        public bool MenudeGoster { get; set; }
        public string Meta_Desc { get; set; }
        public string Meta_Title { get; set; }
        public string Meta_Keyword { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public bool YayinDurumu { get; set; }
        public string Sutun { get; set; }
        public string SiraNo { get; set; }
        public string Resim { get; set; }
        public DateTime EklenmeTarihi { get; set; }


        public List<MenuNode> Children { get; set; }
    }
}