﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder
{
    public partial class event_detail : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string EventId, EventSeoUrl;

        protected void Page_Load(object sender, EventArgs e)
        {
            EventSeoUrl = RouteData.Values["SeoUrl"].ToString();
            EventId = db.GetDataCell("Select EventId from Events where SeoUrl='" + EventSeoUrl + "'");
            DataRow dr = db.GetDataRow("Select * from Events where EventId=" + EventId + "order by BeginDate desc");

            Call_Seo(dr);
            GetImages(EventId);
            hplLink.Text = dr["EventName"].ToString();

            ltrlOrganizer.Text = dr["Organizator"].ToString();
            ltrlPlace.Text = dr["EventPlace"].ToString();

            DateTime BalangicTarihi = Convert.ToDateTime(dr["BeginDate"].ToString());
            ltrlBaslangicTarih.Text = BalangicTarihi.ToString("dd") + " " + BalangicTarihi.ToString("MMMM") + " " + BalangicTarihi.ToString("yyyy");

            DateTime BitisTarihi = Convert.ToDateTime(dr["EndDate"].ToString());
            ltrlBitisTarih.Text = BitisTarihi.ToString("dd") + " " + BitisTarihi.ToString("MMMM") + " " + BitisTarihi.ToString("yyyy");

            string Broc = Page.ResolveUrl("~/upload/Etkinlikler/") + dr["BrochureImg"].ToString();

            if (dr["BrochureImg"].ToString() == "NoBrochure.jpg")
            {
                imgPageHeader.Visible=false;
            }
            else
            { 
                imgPageHeader.ImageUrl = Broc;
            }

            if (dr["GalleryId"].ToString() == "0")
                fotos.Style.Add("display", "none");
           


            string video = dr["Video"].ToString();

            if (video == "Null" || video == "" || video == null)
                pnlVideo.Visible = false;
            else
                ltrlVideo.Text = video;

            ltrlText.Text = dr["EventDetail"].ToString();

            GetPolulerEvents();

        }

        public void Call_Seo(DataRow dr)
        {
            //SEO
            string metaTitle = dr["Meta_Title"].ToString();
            string metaDesc = dr["Meta_Desc"].ToString();
            string metaKey = dr["Meta_Keywords"].ToString();
            string SiteAdi = db.GetDataCell("Select FirmaUnvanKisa from GenelAyarlar");


            if (metaTitle != null && metaTitle != "")
                this.Page.Title = metaTitle;
            else
                this.Page.Title = dr["EventName"].ToString() + " | " + SiteAdi; ;

            if (metaDesc != null && metaDesc != "")
                this.Page.MetaDescription = metaDesc;
            else
                this.Page.MetaDescription = Ayarlar.OzetCek(Ayarlar.HtmlTagTemizle(dr["EventDetail"].ToString()), 150);


            if (metaKey != null && metaKey != "")
                this.Page.MetaKeywords = metaKey;
            else
                this.Page.MetaKeywords = metaTitle;
        }

        public void GetImages(string EventId)
        {
            DataTable dt = db.GetDataTable("Select p.GalleryId,gk.*,gr.* from Events as p inner join GaleriKategori as gk on p.GalleryId=gk.GaleriId inner join GaleriResim as gr on gk.GaleriId=gr.GaleriId where gr.YayinDurumu=1 and p.EventId=" + EventId);
            rptImageGallery.DataSource = dt;
            rptImageGallery.DataBind();
        }

        public void GetPolulerEvents()
        {
            //rptPopulerEvents.DataSource = db.GetDataTable("Select * from Events where IsActive=1 and GalleryId>0 order by BeginDate desc");
            rptPopulerEvents.DataSource = db.GetDataTable("Select * from Events order by BeginDate desc");
            rptPopulerEvents.DataBind();
        }

        protected void rptBreadcrumb_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        protected void rptFooterModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }




        protected void rptImages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {


                string resimAdi = DataBinder.Eval(e.Item.DataItem, "ResimAdi").ToString();
                string KategoriAdi = DataBinder.Eval(e.Item.DataItem, "KategoriAdi").ToString();
                string ResimAciklama = DataBinder.Eval(e.Item.DataItem, "Aciklama").ToString();
                //string ResimAciklama = "KalDer";

                string resimYolu = Server.MapPath("~/upload/Galeri/"+ KategoriAdi + "/" + resimAdi);


                Literal ltrlFigure = (Literal)e.Item.FindControl("ltrlFigure");

                Bitmap bitmap = new Bitmap(resimYolu);

                int iWidth = bitmap.Width;
                int iHeight = bitmap.Height;


                //  System.Drawing.Image img = System.Drawing.Image.FromFile(resimYolu);
                string dataSize = iWidth.ToString() + "x" + iHeight.ToString();

                string display = "";

                if (ResimAciklama == "")
                {
                    display = "display:none !important;";
                }
                else
                    display = "display:visible !important;";

               string figure = "<figure class=\"col-md-3 col-sm-3 form-group\" itemprop=\"associatedMedia\" itemscope itemtype=\"http://schema.org/ImageObject\">" +
                      "<a href=\"" + Page.ResolveClientUrl("~") + "/upload/Galeri/" + KategoriAdi + "/" + resimAdi + " \" itemprop=\"contentUrl\" data-size=\"" + dataSize + "\">" +
                          "<span class=\"helper\"></span><img class=\"img-responsive\" src=\"" + Page.ResolveClientUrl("~") + "/upload/Galeri/" + KategoriAdi + "/" + resimAdi + " \" itemprop=\"thumbnail\" alt=\"" + ResimAciklama + "\" />" +
                      "</a>" +
                     "<figcaption style=\"" + display + "\" itemprop=\"caption description\">" + ResimAciklama + "</figcaption>" +
                    "</figure>";


                ltrlFigure.Text = figure;

                 

            }
        }



        //<a href = "<%#Page.ResolveUrl("~/") %>upload/Galeri/<%#Eval("KategoriAdi") %>/<%#Eval("ResimAdi") %>" rel="prettyPhoto[pp_gal]"  data-gallery="multiimages" class="col-xs-6 col-sm-3 img-thumbnail" style="display:block; overflow:hidden; max-height:100px;   padding:3px;">
        //                                                                  <img src = "<%#Page.ResolveUrl("~/") %>upload/Galeri/<%#Eval("KategoriAdi") %>/<%#Eval("ResimAdi") %>" class="img-responsive " alt="<%#Eval("Aciklama") %>" />
        //                                                              </a>
    }
}
