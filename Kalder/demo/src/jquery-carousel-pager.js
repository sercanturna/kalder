(function(window, document, $, undefined) {
    var Pager = function(api) {
        return {
            setup: function() {
                // init
                this.current = 0;

                // build dom
                this.$pager = $('<ul class="' + api.options.namespace + '-pager" />');

                this.build();

                this.$pager.appendTo(api.$wrap);

                // bind events
                this.bind();

                // active current pager
                this.active(this.current);
            },
            build: function() {
                var itemMarkup = [],
                    i = Math.ceil(api.total / api.visibleNumber);
                for (var n = 1; n <= i; n++) {
                    itemMarkup += "<li>" + "<a href='#'>" + n + "</a>" + "</li>";
                }
                this.$pager.html(itemMarkup);
                this.$items = this.$pager.children();
            },
            update: function() {
                // rebuild pager items
                this.build();

                // get new current pager
                var pager = Math.floor(api.current / api.visibleNumber);
                this.active(pager);
            },
            bind: function() {
                var self = this;
                this.$pager.on('click', 'li', function() {
                    var to = $(this).index() * api.visibleNumber;

                    api.goTo(to);
                    return false;
                });

                api.$element.on('moveEnd', function(e) {
                    var pager = Math.floor(api.current / api.visibleNumber);
                    if (api.current === api.total - api.visibleNumber && typeof api.prevNumber === 'undefined') {
                        pager = Math.ceil(api.total / api.visibleNumber) - 1;
                    }
                    if (pager !== this.current) {
                        self.active(pager);
                    }
                });
            },
            active: function(i) {
                this.current = i;
                this.$items.removeClass(api.options.namespace + '-pager-active').eq(i).addClass(api.options.namespace + '-pager-active');
            }
        };
    };
    $(document).on('carousel::ready', function(event, instance) {
        if (instance.options.pager === true) {
            var pager = Pager(instance);

            pager.setup();

            instance.$element.on('carousel::resize', function(e) {
                pager.update();
            });
        }
    });
})(window, document, jQuery);
