(function(window, document, $, undefined) {
    var $doc = $(document);
    var keyboard = {
        keys: {
            'UP': 38,
            'DOWN': 40,
            'LEFT': 37,
            'RIGHT': 39
        },
        map: {},
        bound: false,
        press: function(e) {
            var key = e.keyCode || e.which;
            if (key in keyboard.map && typeof keyboard.map[key] === 'function') {
                keyboard.map[key](e);
            }
            return false;
        },
        attach: function(map) {
            var key, up;
            for (key in map) {
                if (map.hasOwnProperty(key)) {
                    up = key.toUpperCase();
                    if (up in keyboard.keys) {
                        keyboard.map[keyboard.keys[up]] = map[key];
                    } else {
                        keyboard.map[up] = map[key];
                    }
                }
            }
            if (!keyboard.bound) {
                keyboard.bound = true;
                $doc.bind('keydown', keyboard.press);
            }
        },
        detach: function() {
            keyboard.bound = false;
            keyboard.map = {};
            $doc.unbind('keydown', keyboard.press);
        }
    };

    $doc.on('carousel::ready', function(event, instance) {
        if (instance.options.keyboard !== true) {
            return;
        }
        instance.$element.on('focus', function() {
            keyboard.attach({
                left: function() {
                    instance.prev();
                },
                right: function() {
                    instance.next();
                },
                up: function() {
                    instance.prev();
                },
                down: function() {
                    instance.next();
                }
            });
            return false;
        }).on('blur', function() {
            keyboard.detach();
        });
    });
})(window, document, jQuery);
