﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder
{
    public partial class contact_us : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string SubDomainSubeAdi = Ayarlar.GetSubDomain();
        protected void Page_Load(object sender, EventArgs e)
        {

            DataRow dr = db.GetDataRow("Select * from GenelAyarlar");
            string Map = dr["GoogleMap"].ToString();

            ltrlMap.Text = Map;

           // ltrlTel.Text = dr["Tel"].ToString();
            ltrlTelefon.Text = dr["Tel"].ToString();
            ltrlFax.Text = dr["Fax"].ToString();
            ltrlEmail.Text = dr["Eposta"].ToString();
            ltrlAdres.Text = dr["Adres"].ToString();


            ///* Facebook Likebox*/
            //string fblink = dr["Facebook"].ToString();

            //if (fblink == "" || fblink == null)
            //   // pnl_facebook_like.Visible = false;
            //else
            //    LtrlFbox.Text = "<div class=\"fb-like-box\" data-href=\"" + fblink + "\" data-width=\"218\" data-colorscheme=\"light\" data-show-faces=\"true\" data-header=\"true\" data-stream=\"false\" data-show-border=\"true\"></div>";


        }



        protected void contact_submit1_Click(object sender, EventArgs e)
        {
            string Name, Company, Phone, mail, subject, message, responseType, dept;

            Name = Ayarlar.Temizle(txtName.Text);
            Company = Ayarlar.Temizle(txtCompany.Text);
            Phone = Ayarlar.Temizle(txtPhone.Text);
            mail = Ayarlar.Temizle(txtEmail.Text);
            subject = Ayarlar.Temizle(txtSubject.Text);
            message = Ayarlar.Temizle(txtMessage.Text);
            responseType = dropTur.SelectedItem.Text;
            dept = dropDepartment.SelectedItem.Text;

            SqlConnection baglanti = db.baglan();
            SqlCommand cmdKaydet = new SqlCommand("SET language turkish; insert into ContactForm(" +
                //  NameSurname, Email, CompanyName, Subject, [Content],PhoneNumber,SentDate,Status,IsRead
            "NameSurname," +
            "Email," +
            "CompanyName," +
            "Subject," +
            "Content," +
            "PhoneNumber," +
            "Status," +
            "IsRead," +
            "Department," +
            "ResponseType," +
            "SentDate) values (" +

            "@NameSurname," +
            "@Email," +
            "@CompanyName," +
            "@Subject," +
            "@Content," +
            "@PhoneNumber," +
            "@Status," +
            "@IsRead," +
            "@Department," +
            "@ResponseType," +
            "@SentDate)",
            baglanti);

            cmdKaydet.Parameters.AddWithValue("NameSurname", Name);
            cmdKaydet.Parameters.AddWithValue("Email", mail);
            cmdKaydet.Parameters.AddWithValue("CompanyName", Company);
            cmdKaydet.Parameters.AddWithValue("Subject", subject);
            cmdKaydet.Parameters.AddWithValue("Content", message);
            cmdKaydet.Parameters.AddWithValue("PhoneNumber", Phone);
            cmdKaydet.Parameters.AddWithValue("Status", 0);
            cmdKaydet.Parameters.AddWithValue("IsRead", 0);
            cmdKaydet.Parameters.AddWithValue("Department", dept);
            cmdKaydet.Parameters.AddWithValue("ResponseType", responseType);
            cmdKaydet.Parameters.AddWithValue("SentDate", DateTime.Now.ToString());



            int sonuc = cmdKaydet.ExecuteNonQuery();

            if (sonuc > 0)
            {
              //  Ayarlar.Alert.Show("Mesajınız Gönderilmiştir. En kısa sürede tarafınıza geri dönüş yapılacaktır.");
             
                MailYolla(Company, Name, mail, Phone, message, dept, responseType);
            }
            else
            {
             //   Ayarlar.Alert.Show("Üzgünüz, Mesajınız gönderilemedi. Lütfen daha sonra tekrar deneyin.");
             //   ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
            }
            
        }


        


        public void MailYolla(string companyName, string name, string email, string phone, string message, string dept, string responseType)
        {
            //MailAddress gonderen = new MailAddress("bilgi@seyahatterzisi.com.tr", "Seyahat Terzisi WebForm");
            //MailAddress alan = new MailAddress("bilgi@seyahatterzisi.com.tr", "StudyExpo");
            //MailMessage eposta = new MailMessage(gonderen, alan);
            //eposta.IsBodyHtml = true;
            //eposta.Subject = "Seyahat Terzisi WebForm";
            //eposta.Body = "<strong>Firma Adı: </strong>" + companyName + "<br/><strong>Adı Soyadı: </strong>" + name + "<br/><strong>E-mail Adresi: </strong>" + email + "<br/><strong>Telefon: </strong>" + phone + "<br/><strong>Mesaj: </strong>" + message + "<br/><br/><br/> " + "Yönetim Panelinden görüntülemek için; <br/> <a href='http://www.seyahatterzisi.com.tr/adminv2'>Site Yönetimine Git!</a> ";

            //System.Net.NetworkCredential auth = new System.Net.NetworkCredential("bilgi@seyahatterzisi.com.tr", "Blg0123");
            //SmtpClient SMTP = new SmtpClient();
            //SMTP.Host = "smtp.seyahatterzisi.com.tr";
            //SMTP.UseDefaultCredentials = false;
            //SMTP.Credentials = auth;
            //SMTP.DeliveryMethod = SmtpDeliveryMethod.Network;

            DataRow dr = db.GetDataRow("Select * from MailSettings where FormId=1 and IsActive = 1");

            if (!dr.IsNull("Id"))
            {
                string GonderenEposta = dr["Gonderen"].ToString();
                string AlanEposta = dr["Alan"].ToString();
                string Konu = dr["FormKonusu"].ToString();
                string SmtpSunucu = dr["SmtpSunucu"].ToString();
                string SmtpKullaniciAdi = dr["SmtpKullaniciAdi"].ToString();
                string SmtpParola = dr["SmtpParola"].ToString();


                MailAddress gonderen = new MailAddress(GonderenEposta, Konu);
                // MailAddress alan = new MailAddress(AlanEposta, Konu);
                //MailMessage eposta = new MailMessage(gonderen, alan);
                MailMessage eposta = new MailMessage();

                eposta.From = gonderen;

               

                if (SubDomainSubeAdi == "kalder")
                {
                    if (dept == "Üyelik")
                    {
                        //eposta.To.Add(new MailAddress("sanems@kalder.org"));
                        //eposta.CC.Add(new MailAddress("gorkem.erkus@kalder.org"));
                        eposta.To.Add(new MailAddress("kalderuyelik@kalder.org"));
                        eposta.CC.Add(new MailAddress("kalder@kalder.org"));
                    }
                    else if (dept == "Eğitimler")
                    {
                        eposta.To.Add(new MailAddress("kalderegitim@kalder.org"));
                        //eposta.To.Add(new MailAddress("sennur.akin@kalder.org"));
                        //eposta.CC.Add(new MailAddress("orhana@kalder.org"));
                        eposta.CC.Add(new MailAddress("kalder@kalder.org"));
                    }
                    else if (dept == "Rehberlik")
                    {
                        eposta.To.Add(new MailAddress("orhana@kalder.org"));
                        eposta.CC.Add(new MailAddress("kalder@kalder.org"));
                    }
                    else if (dept == "Yayınlar")
                    {
                        eposta.To.Add(new MailAddress("lale.evliyazade@kalder.org"));
                        eposta.CC.Add(new MailAddress("kalder@kalder.org"));
                    }
                    else if (dept == "UKH")
                    {
                        eposta.To.Add(new MailAddress("orhana@kalder.org"));
                        eposta.CC.Add(new MailAddress("kalder@kalder.org"));
                    }
                    else if (dept == "Ödül")
                    {
                        eposta.To.Add(new MailAddress("kalderodul@kalder.org"));
                        //eposta.To.Add(new MailAddress("bercin.gun@kalder.org"));
                        //eposta.CC.Add(new MailAddress("orhana@kalder.org"));
                        eposta.CC.Add(new MailAddress("kalder@kalder.org"));
                    }
                    else if (dept == "Etkinlikler")
                    {
                        eposta.To.Add(new MailAddress("kalderkongre@kalder.org"));
                        //eposta.To.Add(new MailAddress("lale.evliyazade@kalder.org"));
                        eposta.CC.Add(new MailAddress("kalder@kalder.org"));
                    }
                    else if (dept == "TMS")
                    {
                        eposta.CC.Add(new MailAddress("gorkem.erkus@kalder.org"));
                        eposta.CC.Add(new MailAddress("kalder@kalder.org"));
                    }
                    else if (dept == "Dış Değerlendirme")
                    {
                        eposta.To.Add(new MailAddress("bercin.gun@kalder.org"));
                        eposta.CC.Add(new MailAddress("orhana@kalder.org"));
                        eposta.CC.Add(new MailAddress("kalder@kalder.org"));
                    }
                    else if (dept == "Muhasebe")
                    {
                        eposta.To.Add(new MailAddress("kaldermuhasebe@kalder.org"));
                        //eposta.To.Add(new MailAddress("tugba.tasar@kalder.org"));
                        //eposta.CC.Add(new MailAddress("orhana@kalder.org"));
                        eposta.CC.Add(new MailAddress("kalder@kalder.org"));
                    }
                    else if (dept == "Diğer")
                    {
                        eposta.To.Add(new MailAddress("dincer.ozberber@kalder.org"));
                        eposta.CC.Add(new MailAddress("kalder@kalder.org"));
                    }
                }
                else
                {

                    dept = Ayarlar.BasHarfBuyuk(SubDomainSubeAdi) + " Bilgi Edinme Formu";

                    eposta.To.Add(new MailAddress(SubDomainSubeAdi+"@kalder.org"));
                    eposta.CC.Add(new MailAddress("kalder@kalder.org"));

                 
                }

              
                eposta.IsBodyHtml = true;
                eposta.Subject = name + " form gönderdi";
                eposta.Body = "<strong>Başvuru Konusu: </strong>" + dept + "<br/><strong>Başvuru Cevap Türü: </strong>" + responseType + "<br/><strong>Firma Adı: </strong>" + companyName + "<br/><strong>Adı Soyadı: </strong>" + name + "<br/><strong>E-mail Adresi: </strong>" + email + "<br/><strong>Telefon: </strong>" + phone + "<br/><strong>Mesaj: </strong>" + message + "<br/><br/><br/> " + "Yönetim Panelinden görüntülemek için; <br/> <a href=\"http://" + HttpContext.Current.Request.Url.Host + "/adminv2 \">Site Yönetimine Git!</a> ";


                System.Net.NetworkCredential auth = new System.Net.NetworkCredential(SmtpKullaniciAdi, SmtpParola);
                SmtpClient SMTP = new SmtpClient();
                SMTP.Host = SmtpSunucu;
                SMTP.UseDefaultCredentials = false;
                SMTP.Credentials = auth;
                SMTP.DeliveryMethod = SmtpDeliveryMethod.Network;
                SMTP.Port = 587;
                SMTP.EnableSsl = true;
                
                try
                {
                    SMTP.Send(eposta);
                    // Response.Write("Mail Gönderildi!");

                    lblModalTitle.Text = "Teşekkürler";
                    lblModalBody.Text = "Formunuz başarıyla gönderildi, ilgili birimimiz tarafından en kısa sürede tarafınıza geri dönüş sağlanacaktır.";

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "MailingModal", "$('#MailingModal').modal();", true);
                    upModal.Update();


                }
                catch (Exception ex)
                {
                    //Response.Write("Mail Gönderilemedi, Sebebi: " + ex.Message);


                    lblModalTitle.Text = "Üzgünüm";
                    lblModalBody.Text = "Mail Gönderilemedi : "+ ex.Message;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "MailingModal", "$('#MailingModal').modal();", true);
                    upModal.Update();
                }


                
            }
            else
            {
                Ayarlar.Alert.Show("Lütfen Yönetim Paneli Mesajlar sekmesi altında SMTP ayarlarını giriniz.");
            }
           
        }


        public void Page_PreInit()
        {

            //this.Theme = db.GetDataCell("Select Tema from GenelAyarlar");
            //if (Request.Form != null && Request.Form.Count > 0)
            //    this.Theme = this.Request.Form[4].Trim();
        }

        protected void rptSidebarModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }
    }
}