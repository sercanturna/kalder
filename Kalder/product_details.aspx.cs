﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder
{
    public partial class product_details : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string ProductCat, ProductCatId, ParentProductCatId, ProductSeoUrl, ProductId;
        protected void Page_Load(object sender, EventArgs e)
        {
            ProductCat = RouteData.Values["ProductCat"].ToString();

            ProductSeoUrl = RouteData.Values["ProductSeoUrl"].ToString();
            ProductId = db.GetDataCell("Select ProductId from Products where SeoUrl='" + ProductSeoUrl + "'");


            //Usercontrol olan breadcumb dosyama route value gönderiyorum.
            breadcumb1.getKategoriID = RouteData.Values["ProductCat"].ToString();
            getImages();


            ProductCatId = db.GetDataCell("Select UrunKatId from UrunKategori where Seo_url='" + ProductCat + "'");
            ParentProductCatId = db.GetDataCell("Select UstKatId from UrunKategori where UrunKatId='" + ProductCatId + "'");
            //  GetPageleftMenu(ParentProductCatId, ProductCatId);
            GetProducts(ProductCatId);

            ltrlBaslik.Text = db.GetDataCell("Select ProductName from Products where ProductId=" + ProductId);


            /* Facebook Likebox*/
            string fblink = db.GetDataCell("Select Facebook from GenelAyarlar");

            if (fblink != "" || fblink != null)
                ltrlFbComments.Text = "<div class=\"fb-comments\"  data-href=\"" + Request.Url.Authority + Request.Url.AbsolutePath + "\" data-numposts=\"5\" data-colorscheme=\"light\"></div>";


            DataRow dr = db.GetDataRow("Select * from Products as p inner join Brands as b on p.BrandId=b.BrandId inner join Currency as c on c.CurrencyId=p.CurrencyId where p.ProductId=" + ProductId);

            try
            {
                ltrlBrand.Text = dr["BrandName"].ToString();

                ltrlProductName.Text = dr["ProductName"].ToString();
                ltrlProductCode.Text = dr["ProductCode"].ToString();
                ltrlTags.Text = dr["Meta_Keywords"].ToString().Replace(",", ", ");


                string Currency = dr["Currency"].ToString();



                double Price = (double)dr["UnitPrice"];




                double tax = (double)dr["Tax"];

                if (tax == 0)
                    ltrlPrice.Text = Price.ToString("#.00") + " " + Currency;
                else
                    ltrlPrice.Text = Price.ToString("#.00") + " " + Currency + " +KDV";





                double TotalPrice = Price + (tax * Price / 100);

                ltrlTotalPrice.Text = TotalPrice.ToString("#.00") + " " + Currency;


                ltrlproductDesc.Text = dr["Description"].ToString();

                DataSet ds = db.GetDataSet("Select kui.UrunKatId,KatAdi from KatUrunIliski as kui inner join UrunKategori as uk on kui.UrunKatId=uk.UrunKatId where ProductId=" + ProductId);

                foreach (DataRow drcat in ds.Tables[0].Rows)
                {
                    ltrlProductCat.Text += "<a style='text-decoration:none;' href=\"" + Page.ResolveUrl("~") + "urunler/" + Ayarlar.UrlSeo(drcat["KatAdi"].ToString()) + " \">" + drcat["KatAdi"].ToString() + ", </a>";
                }


                //<img class="default_image" id="zoom" src="<%=Page.ResolveUrl("~") %>upload/urunler/asus_x550cc_xo387h_intel_core_i3_2365m_14ghz_4gb_500gb_156_tasinabilir_bilgisayar785.jpg" data-zoom-image="<%=Page.ResolveUrl("~") %>upload/urunler/asus_x550cc_xo387h_intel_core_i3_2365m_14ghz_4gb_500gb_156_tasinabilir_bilgisayar785.jpg" style="border:1px solid #808080;"/>

                string DefaultProductImage = db.GetDataCell("Select ImageURL from ProductImages where ProductId=" + ProductId + "and IsPrimary=1");

                ltrlDefaultImage.Text = "<img id=\"zoom\" class=\"default_image\" src=" + Page.ResolveUrl("~") + "upload/urunler/" + DefaultProductImage + " data-zoom-image=" + Page.ResolveUrl("~") + "upload/urunler/" + DefaultProductImage + " style=\"border:1px solid #808080;\"/>";

            }
            catch (Exception)
            {

            }

            GetSidebarModules();
            GetFooterModules();


            Page.Title = dr["Meta_Title"].ToString();
            Page.MetaDescription = dr["Meta_Desc"].ToString();
            Page.MetaKeywords = dr["Meta_Keywords"].ToString();
        }

        public void GetFooterModules()
        {
            DataTable dt = db.GetDataTable("select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=4 and mss.IsActive=1 and m.SectionId=5 order by mss.OrderNo");
            rptFooterModules.DataSource = dt;
            rptFooterModules.DataBind();
        }

        public void GetSidebarModules()
        {
            DataTable dt = db.GetDataTable("select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=4 and mss.IsActive=1 and m.SectionId=2 order by mss.OrderNo");
            rptSidebarModules.DataSource = dt;
            rptSidebarModules.DataBind();

            if (dt.Rows.Count == 0)
            {
                sideBar.Visible = false;


                pageContent.Style.Add("width", "100%");
            }
        }

        protected void rptSidebarModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);


                    PropertyInfo[] info = uc.GetType().GetProperties();


                    foreach (PropertyInfo item in info)
                    {
                        if (item.CanWrite)
                        {
                            if (item.Name == "ProductCatProp")
                                item.SetValue(uc, ProductCat, null);

                        }
                    }


                }
            }




        }

        protected void rptFooterModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);

                }
            }




        }


        public void GetProducts(string ProductCatId)
        {

            DataTable dtProducts = db.GetDataTable("SELECT p.*,kui.*, uk.*,c.Currency ,(SELECT top 1 ImageURL FROM ProductImages WHERE ProductImages.ProductId = p.ProductId and IsPrimary=1) as ImageURL from Products as p inner join KatUrunIliski as kui on p.ProductId=kui.ProductId inner join UrunKategori as uk on uk.UrunKatId=kui.UrunKatId inner join Currency as c on c.CurrencyId= p.CurrencyId where kui.UrunKatId=" + ProductCatId);
            rpt_Products.DataSource = dtProducts;
            rpt_Products.DataBind();
        }

        public void getImages()
        {
            DataTable dt = db.GetDataTable("Select * from ProductImages where ProductId=" + ProductId);
            rptProductImages.DataSource = dt;
            rptProductImages.DataBind();
        }

        //public void GetPageleftMenu(string ParentPageId, string ProductCatId)
        //{
        //    DataSet ds = db.GetDataSet("SELECT aa.UrunKatId, aa.UstKatId, aa.Seo_url as Seo_url, bb.KatAdi AS ParentCat, aa.KatAdi FROM UrunKategori aa LEFT OUTER JOIN UrunKategori bb ON aa.UstKatId=bb.UrunKatId where aa.UstKatId=" + ProductCatId + " and aa.YayinDurumu=1 order by aa.SiraNo");

        //    if (ds.Tables[0].Rows.Count == 0)
        //    {
        //        ds = db.GetDataSet("SELECT aa.UrunKatId, aa.UstKatId, aa.Seo_url as Seo_url, bb.KatAdi AS ParentCat, aa.KatAdi FROM UrunKategori aa LEFT OUTER JOIN UrunKategori bb ON aa.UstKatId=bb.UrunKatId where aa.UstKatId=" + ParentProductCatId + " and aa.YayinDurumu=1 order by aa.SiraNo");
        //    }

        //    rpt_leftmenu.DataSource = ds;
        //    rpt_leftmenu.DataBind();
        //}

        //protected void rpt_leftmenu_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        //{
        //    DataRowView drv = e.Item.DataItem as DataRowView;
        //    Literal ltrlLeftMenuHeader = (Literal)e.Item.FindControl("ltrlLeftMenuHeader");

        //    if (e.Item.ItemType == ListItemType.Header)
        //    {
        //        DataSet ds = db.GetDataSet("SELECT aa.UrunKatId, aa.UstKatId, aa.Seo_url as Seo_url, bb.KatAdi AS ParentCatName, aa.KatAdi FROM UrunKategori aa LEFT OUTER JOIN UrunKategori bb ON aa.UstKatId=bb.UrunKatId where aa.UstKatId=" + ProductCatId);


        //        if (ds.Tables[0].Rows.Count == 0)
        //        {
        //            ds = db.GetDataSet("SELECT aa.UrunKatId, aa.UstKatId, aa.Seo_url as Seo_url, bb.KatAdi AS ParentCatName, aa.KatAdi FROM UrunKategori aa LEFT OUTER JOIN UrunKategori bb ON aa.UstKatId=bb.UrunKatId where aa.UstKatId=" + ParentProductCatId);
        //        }

        //        foreach (DataRow dr in ds.Tables[0].Rows)
        //        {
        //            ltrlLeftMenuHeader.Text = "<li class='has-sub active'><a href='" +Page.ResolveUrl("~/urunler/") + Ayarlar.UrlSeo(dr["ParentCatName"].ToString()) + "'><span>" + dr["ParentCatName"].ToString() + "</span></a><ul>";
        //        }
        //    }




        //    //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    //{
        //    //    DataRowView drv = e.Item.DataItem as DataRowView;

        //    //    PlaceHolder treeMenu = (PlaceHolder)e.Item.FindControl("leftMenu");
        //    //    treeMenu.Controls.Clear();
        //    //    DataSet ds = db.GetDataSet("Select distinct ParentPageId,PageName,PageURL,OrderNumber From Pages where PageId=" + drv.Row["ParentPageId"].ToString() + " and PageIsActive=1 and ShowMenu=1  order by OrderNumber");
        //    // //   treeMenu.Controls.Add(new LiteralControl("<ul id='treeMenu'>"));
        //    //    foreach (DataRow dr in ds.Tables[0].Rows)
        //    //    {
        //    //        if ((int)dr["ParentPageId"] == 0)
        //    //        {
        //    //            treeMenu.Controls.Add(new LiteralControl("<li class='has-sub'><a href=" + dr["PageURL"].ToString() + "><span>" + dr["PageName"].ToString() + "</span></a>"));
        //    //            int Id = (int)dr["ParentPageId"];
        //    //          //  altDoldur(Id, treeMenu);
        //    //        }
        //    //    }
        //    //   //  treeMenu.Controls.Add(new LiteralControl("</ul>"));
        //    //}
        //}

        public void Page_PreInit()
        {
            //this.Theme = db.GetDataCell("Select Tema from GenelAyarlar");
            //if (Request.Form != null && Request.Form.Count > 0)
            //    this.Theme = this.Request.Form[4].Trim();
        }

        protected void rpt_Products_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            DataRowView drv = e.Item.DataItem as DataRowView;

            Literal KDV = (Literal)e.Item.FindControl("ltrlKDV");

            if ((double)drv.Row["Tax"] == 0)
                KDV.Visible = false;
            else
                KDV.Visible = true;
            KDV.Text = " + KDV";
        }
    }
}