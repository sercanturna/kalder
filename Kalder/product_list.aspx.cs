﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace Kalder
{
    public partial class product_list : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string ProductCat, ProductCatId, ParentProductCatId;
        protected void Page_Load(object sender, EventArgs e)
        {

            ProductCat = RouteData.Values["ProductCat"].ToString();

            breadcumb1.getKategoriID = ProductCat;

            ProductCatId = db.GetDataCell("Select UrunKatId from UrunKategori where Seo_url='" + ProductCat + "'");
            ParentProductCatId = db.GetDataCell("Select UstKatId from UrunKategori where UrunKatId='" + ProductCatId + "'");

            if (ParentProductCatId == "0")
            {
                GetPopulerProducts();
            }
            else
            {
                GetProducts(ProductCatId);
            }

            GetSidebarModules();
            GetFooterModules();


            DataRow dr = db.GetDataRow("Select * from UrunKategori where UrunKatId=" + ProductCatId);

            string ImageURL = dr["Resim"].ToString();
            if (ImageURL == "resim_yok.jpg" || ImageURL == "")
                imgPageHeader.Visible = false;
            else
                imgPageHeader.ImageUrl = Page.ResolveUrl("~") + "upload/UrunKategoriResimleri/770/" + dr["Resim"].ToString();

            ltrlBaslik.Text = dr["KatAdi"].ToString();


            int hit = (int)dr["Hit"] + 1;
            db.cmd("update UrunKategori set Hit=" + hit.ToString() + " where UrunKatId=" + ProductCatId);

            Page.Title = dr["Meta_Title"].ToString();
            Page.MetaDescription = dr["Meta_Desc"].ToString();
            Page.MetaKeywords = dr["Meta_Keyword"].ToString();

        }


        public void GetFooterModules()
        {
            DataTable dt = db.GetDataTable("select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=4 and mss.IsActive=1 and m.SectionId=5 order by mss.OrderNo");
            rptFooterModules.DataSource = dt;
            rptFooterModules.DataBind();
        }

        public void GetSidebarModules()
        {
            DataTable dt = db.GetDataTable("select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=4 and mss.IsActive=1 and m.SectionId=2 order by mss.OrderNo");
            rptSidebarModules.DataSource = dt;
            rptSidebarModules.DataBind();

            if (dt.Rows.Count == 0)
            {
                sideBar.Visible = false;


                pageContent.Style.Add("width", "100%");
            }
        }

        protected void rptSidebarModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);


                    PropertyInfo[] info = uc.GetType().GetProperties();


                    foreach (PropertyInfo item in info)
                    {
                        if (item.CanWrite)
                        {
                            if (item.Name == "ProductCatProp")
                                item.SetValue(uc, ProductCat, null);

                        }
                    }


                }
            }




        }

        protected void rptFooterModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);

                }
            }




        }

        public void GetPopulerProducts()
        {
            DataTable dtProducts = db.GetDataTable("SELECT p.*,c.*,(SELECT top 1 ImageURL FROM ProductImages WHERE ProductImages.ProductId = p.ProductId and IsPrimary=1) as ImageURL, (select top 1 k.KatAdi from UrunKategori k inner join KatUrunIliski i on k.UrunKatId = i.UrunKatId  where i.ProductId = p.ProductId) as KatAdi from Products as p inner join Currency as c on c.CurrencyId=p.CurrencyId inner join Brands as b on p.BrandId=b.BrandId where p.IsPopuler=1 and b.Status=1");
            rpt_Products.DataSource = dtProducts;
            rpt_Products.DataBind();
        }

        public void GetProducts(string ProductCatId)
        {

            DataTable dtProducts = db.GetDataTable("SELECT p.*,kui.*, uk.*,c.*,(SELECT top 1 ImageURL FROM ProductImages WHERE ProductImages.ProductId = p.ProductId and IsPrimary=1) as ImageURL from Products as p inner join KatUrunIliski as kui on p.ProductId=kui.ProductId inner join UrunKategori as uk on uk.UrunKatId=kui.UrunKatId inner join Currency as c on c.CurrencyId=p.CurrencyId where kui.UrunKatId=" + ProductCatId);
            rpt_Products.DataSource = dtProducts;
            rpt_Products.DataBind();
        }






        public void Page_PreInit()
        {
            // Set the Theme for the page. Post-data is not currently loaded
            // Use trace="true" to see the Form collection
            //this.Theme = db.GetDataCell("Select Tema from GenelAyarlar");
            //if (Request.Form != null && Request.Form.Count > 0)
            //    this.Theme = this.Request.Form[4].Trim();
        }

        protected void rpt_Products_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                Image yeni = (Image)e.Item.FindControl("imgNew");

                Literal KDV = (Literal)e.Item.FindControl("ltrlKDV");

                if ((double)drv.Row["Tax"] == 0)
                    KDV.Visible = false;
                else
                    KDV.Visible = true;
                KDV.Text = "+ KDV";

                string IsNew = db.GetDataCell("Select IsNew from Products where ProductId=" + drv.Row["ProductId"].ToString());

                if (IsNew != "True")
                {
                    yeni.Visible = false;
                }

            }

        }
    }
}