﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="etik_bildirim_formu.aspx.cs" Inherits="Kalder.etik_bildirim_formu" validateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <style>
        .modal-header {
	padding-bottom: 5px;
}

.modal-footer {
    	padding: 0;
	}
    
.modal-footer .btn-group button {
	height:40px;
	border-top-left-radius : 0;
	border-top-right-radius : 0;
	border: none;
	border-right: 1px solid #ddd;
}
	
.modal-footer .btn-group:last-child > button {
	border-right: 0;
}

        #etikForm input, #etikForm textarea {
        border:1px solid #ccc;
        padding:10px;
        }


        .form-group .btn {
        width:50%; display:inline;
        }

        .form-group .btn-primary {
        background-color:#ddd;
        }

    </style>

    <script type="text/javascript" >


        $(document).ready(function()
        {
    
            $('.modal').on('hidden.bs.modal', function(e)
            { 
                $(this).removeData();
            }) ;
 

            if (typeof (Sys) !== 'undefined') {
                Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function (sender, args) {
                    { { scopeName } } _init();
                });
            }


            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                if (args.get_error() != undefined) {
                    console.log(args.get_error().message.substr(args.get_error().name.length + 2));
                    args.set_errorHandled(true);
                }
            }



        });
</script>
</head>

    
<body>
    <form id="etikForm" runat="server" style="padding:30px;">
    <asp:ScriptManager ID="sc1" runat="server" EnablePartialRendering="true" AsyncPostBackTimeOut="36000"></asp:ScriptManager>



           <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
            <ContentTemplate>
            

			
            <!-- content goes here -->
              <div class="form-group">
              <asp:Literal ID="ltrlSonuc" runat="server"></asp:Literal>

                  <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="up1">
<ProgressTemplate>
    <div class="modal">
        <div class="center">
            <img alt="" src="images/kalder_loading.gif" />
        </div>
    </div>
</ProgressTemplate>
</asp:UpdateProgress>
                  
              </div>




			<div class="form-group">
                <label for="txtName">Adı Soyadı</label>
                <asp:TextBox AutoCompleteType="FirstName" CssClass="form-control" id="txtName" runat="server"></asp:TextBox>
                <p class="help-block">Bildirimi yapanın adı soyadı (Zorunlu değildir).</p>
              </div>
              <div class="form-group">
                <label for="txtPhone">Telefon</label>
                <asp:TextBox AutoCompleteType="BusinessPhone" CssClass="form-control" id="txtPhone" runat="server"></asp:TextBox>
                  <p class="help-block">Bildirimi yapanın telefon numarası (Zorunlu değildir).</p>
              </div>
            <div class="form-group">
                <label for="txtEmail">E-posta Adresi</label>
                <asp:TextBox AutoCompleteType="Email" CssClass="form-control" id="txtEmail" runat="server"></asp:TextBox>
                  <p class="help-block">Bildirimi yapanın e-posta adresi (Zorunlu değildir).</p>
              </div>
              <div class="form-group">
                <label for="txtMsg">Bildirim Açıklama</label>
                <asp:TextBox class="form-control" id="txtMsg" TextMode="MultiLine" runat="server"></asp:TextBox>
              </div>


                <div class="form-group">
                    <asp:Button ID="btnGonder" type="submit" class="btn btn-default" runat="server" Text="Gönder" OnClick="btnGonder_Click" UseSubmitBehavior="false"/>  
                    <asp:Button ID="btnTemizle" type="submit" class="btn btn-primary" runat="server" Text="Temizle" OnClick="btnTemizle_Click"  />
              </div>
             
                 
              
                
                
                   </ContentTemplate>
               <Triggers>
                   <asp:AsyncPostBackTrigger ControlID="txtName" />
                   <asp:AsyncPostBackTrigger ControlID="txtPhone" />
                   <asp:AsyncPostBackTrigger ControlID="txtEmail" />
                   <asp:AsyncPostBackTrigger ControlID="txtMsg" />

                   <asp:AsyncPostBackTrigger ControlID="btnGonder" EventName="Click" />
                   <asp:AsyncPostBackTrigger ControlID="btnTemizle" EventName="Click"/>
               </Triggers>
        </asp:UpdatePanel>


	<%--	</div>
		<div class="modal-footer">
			<div class="btn-group btn-group-justified" role="group" aria-label="group button">
				<div class="btn-group" role="group">
					<button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
				</div>
				<div class="btn-group btn-delete hidden" role="group">
					<button type="button" id="delImage" class="btn btn-default btn-hover-red" data-dismiss="modal"  role="button">Delete</button>
				</div>
				<div class="btn-group" role="group">
					<button type="button" id="saveImage" class="btn btn-default btn-hover-green" data-action="save" role="button">Save</button>
				</div>
			</div>
		</div>
	</div>
  </div>
</div>--%>
        

     </form>
</body>
</html>
