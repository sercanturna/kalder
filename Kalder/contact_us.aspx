﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="contact_us.aspx.cs" Inherits="Kalder.contact_us" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href='http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz' rel='stylesheet' type='text/css' />
    <script src="<%= Page.ResolveUrl("~")%>js/form.js"></script>
    <style>
        #contact-form input[type="text"],
        #contact-form input[type="email"],
        #contact-form input[type="tel"],
        #contact-form input[type="url"],
        #contact-form textarea,
        #contact-form select,
        #contact-form input[type="submit"] {
            /*font:400 12px/12px "Helvetica Neue", Helvetica, Arial, sans-serif;*/
            font-size: 14px !important;
        }

        #contact-form {
            text-shadow: 0 1px 0 #FFF;
            border-radius: 4px;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            background: #F9F9F9;
            padding: 25px;
        }

            #contact-form h3 {
                color: #2a4a77;
                display: block;
                font-size: 28px;
            }

            #contact-form h4 {
                margin: 5px 0 15px;
                display: block;
                font-size: 13px;
            }

            #contact-form label {
                width: 100%;
            }

                #contact-form label span {
                    cursor: pointer;
                    color: #00aca8;
                    display: block;
                    margin: 5px 0;
                    font-weight: 900;
                }

            #contact-form input[type="text"],
            #contact-form input[type="email"],
            #contact-form input[type="tel"],
            #contact-form input[type="url"],
            #contact-form textarea {
                width: 100%;
                box-shadow: inset 0 1px 2px #DDD, 0 1px 0 #FFF;
                -webkit-box-shadow: inset 0 1px 2px #DDD, 0 1px 0 #FFF;
                -moz-box-shadow: inset 0 1px 2px #DDD, 0 1px 0 #FFF;
                border: 1px solid #CCC;
                background: #FFF;
                margin: 0 0 5px;
                padding: 10px;
                border-radius: 5px;
            }

                #contact-form input[type="text"]:hover,
                #contact-form input[type="email"]:hover,
                #contact-form input[type="tel"]:hover,
                #contact-form input[type="url"]:hover,
                #contact-form textarea:hover {
                    -webkit-transition: border-color 0.3s ease-in-out;
                    -moz-transition: border-color 0.3s ease-in-out;
                    transition: border-color 0.3s ease-in-out;
                    border: 1px solid #AAA;
                }

            #contact-form textarea {
                height: 100px;
                max-width: 100%;
            }

            #contact-form input[type="submit"] {
                cursor: pointer;
                width: 100%;
                border: none;
                background: #00aca8; /* Old browsers */
                color: #FFF;
                margin: 0 0 5px;
                padding: 10px;
                border-radius: 5px;
            }

                #contact-form input[type="submit"]:hover {
                    background: #356aa0; /* Old browsers */
                    -webkit-transition: background 0.3s ease-in-out;
                    -moz-transition: background 0.3s ease-in-out;
                    transition: background-color 0.3s ease-in-out;
                }

                #contact-form input[type="submit"]:active {
                    box-shadow: inset 0 1px 3px rgba(0,0,0,0.5);
                }

            #contact-form input:focus,
            #contact-form textarea:focus {
                outline: 0;
                border: 1px solid #999;
            }

        ::-webkit-input-placeholder {
            color: #888;
        }

        :-moz-placeholder {
            color: #888;
        }

        ::-moz-placeholder {
            color: #888;
        }

        :-ms-input-placeholder {
            color: #888;
        }

        ul.iletisim {
            width: 100% !important;
            background-color: #eeeeee;
            overflow: hidden;
        }

            ul.iletisim li {
                /*float:left;
		width:220px;*/
                display: inline-block;
                border-right: 2px solid #fff;
                line-height: 1.5;
                color: #727272;
                text-align: left;
                padding: 10px;
            }

        @media only screen and (max-width: 375px) {
            ul.iletisim {
                padding: 5px;
            }

                ul.iletisim li {
                    width: 100% !important;
                    border-right: none;
                }

            .wrap {
                white-space: normal;
                width: 100px;
            }
        }


        div.contactform * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-font-smoothing: antialiased;
            -moz-font-smoothing: antialiased;
            -o-font-smoothing: antialiased;
            font-smoothing: antialiased;
            text-rendering: optimizeLegibility;
        }

        div.contactform {
            font: 400 12px/1.625 "Helvetica Neue", Helvetica, Arial, sans-serif;
            color: #444;
            background: #eeeeee; /* Old browsers */
            /* IE9 SVG, needs conditional override of 'filter' to 'none' */
            background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2VlZWVlZSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNjY2NjY2MiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
            background: -moz-linear-gradient(top, #eeeeee 0%, #cccccc 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#eeeeee), color-stop(100%,#cccccc)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, #eeeeee 0%,#cccccc 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, #eeeeee 0%,#cccccc 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top, #eeeeee 0%,#cccccc 100%); /* IE10+ */
            background: linear-gradient(to bottom, #eeeeee 0%,#cccccc 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#cccccc',GradientType=0 ); /* IE6-8 */
            width: 100%;
        }

        .wrapper {
            max-width: 1100px;
            width: 100%;
            margin: 0 auto;
            position: relative;
        }


        .contentbox {
            overflow: hidden;
        }

        .overlay {
            background: rgba(0, 172, 168, .7) !important;
        }

        .breadcrumb {
            margin: 63px 0px !important;
        }
    </style>


        <script type="text/javascript" >


        $(document).ready(function()
        {
    
            $('.modal').on('hidden.bs.modal', function(e)
            { 
                $(this).removeData();
            }) ;
 

            if (typeof (Sys) !== 'undefined') {
                Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(function (sender, args) {
                    { { scopeName } } _init();
                });
            }


            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);

            function EndRequestHandler(sender, args) {
                if (args.get_error() != undefined) {
                    console.log(args.get_error().message.substr(args.get_error().name.length + 2));
                    args.set_errorHandled(true);
                }
            }



        });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
              <!-- Bootstrap Modal Dialog -->
<div class="modal fade" id="MailingModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <asp:UpdatePanel ID="upModal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" style="color:#00aca8; padding:5px;"><asp:Label ID="lblModalTitle" runat="server" Text=""></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblModalBody" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Kapat</button>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>

    <div id="screeno"></div>

    <!--======= CONTENT =========-->
    <div class="content">
        <section class="sub-banner" style="margin-top: 178px;">
            <div class="overlay">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="<%= Page.ResolveUrl("~")%>Anasayfa" title="Ana Sayfa">
                            <%--<img src="<%= Page.ResolveUrl("~/App_Themes/")%><%= Page.Theme %>/images/home-icon.png" alt="Home" class="home" /></a></li>--%>
                            <%--<img src="<%= Page.ResolveUrl("~/")%>images/home-icon.png" alt="Home" class="home" />--%>Anasayfa</a></li>
                        <li><a href="<%= Page.ResolveUrl("~")%>iletisim" title="İletişim">Bize Ulaşın</a></li>
                    </ol>
                </div>
            </div>
        </section>



        <section class="innerpages">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">


                        <div class="contentbox">


                            <asp:Image ID="imgPageHeader" runat="server" CssClass="pageImage" />

                            <div class="tittle tittle-2">

                                <h3>
                                    <asp:Literal ID="ltrlBaslik" runat="server" Text="Bize Ulaşın"></asp:Literal></h3>
                                <hr />
                            </div>
                            <div class="text-conntainer">


                                <div class="map">
                                    <asp:Literal ID="ltrlMap" runat="server"></asp:Literal>
                                </div>

                                <div>
                                    <ul class="iletisim">
                                        <li><strong>Adres:</strong><br />
                                            <asp:Literal ID="ltrlAdres" runat="server"></asp:Literal></li>
                                        <li><strong>Telefon:</strong><br />
                                            <asp:Literal ID="ltrlTelefon" runat="server"></asp:Literal></li>
                                        <li><strong>Fax:</strong><br />
                                            <asp:Literal ID="ltrlFax" runat="server"></asp:Literal></li>
                                        <li><strong>E-Mail:</strong><br />
                                            <asp:Literal ID="ltrlEmail" runat="server"></asp:Literal></li>
                                    </ul>


                                </div>

                                <div class="contactform left">

                                    <div class="wrapper">
                                        <div id="main" style="padding: 10px;">
                                            <!-- Form -->
                                            <div id="contact-form">

                                                <div style="display:none;">
                                                    <label>
                                                        <span>Başvuru Konusu (zorunlu)</span>
                                                        <asp:DropDownList ID="dropDepartment" runat="server" TabIndex="0" Visible="false" required>
                                                            <asp:ListItem>Lütfen Seçiniz</asp:ListItem>
                                                            <asp:ListItem>Üyelik</asp:ListItem>
                                                            <asp:ListItem>Eğitimler</asp:ListItem>
                                                            <asp:ListItem>Rehberlik</asp:ListItem>
                                                            <asp:ListItem>Yayınlar</asp:ListItem>
                                                            <asp:ListItem>UKH</asp:ListItem>
                                                            <asp:ListItem>Ödül</asp:ListItem>
                                                            <asp:ListItem>Etkinlikler</asp:ListItem>
                                                            <asp:ListItem>TMS</asp:ListItem>
                                                            <asp:ListItem>Dış Değerlendirme</asp:ListItem>
                                                            <asp:ListItem>Muhasebe</asp:ListItem>
                                                            <asp:ListItem>Diğer</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </label>
                                                </div>

                                                <div>
                                                    <label>
                                                        <span>Başvuruya Cevap Türü</span>
                                                        <asp:DropDownList ID="dropTur" runat="server" TabIndex="0" required autofocus>
                                                            <asp:ListItem>Lütfen Seçiniz</asp:ListItem>
                                                            <asp:ListItem>Telefon</asp:ListItem>
                                                            <asp:ListItem>E-Posta</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </label>
                                                </div>

                                                <div>
                                                    <label>
                                                        <span>Adı Soyadı: (zorunlu)</span>
                                                        <asp:TextBox ID="txtName" runat="server" placeholder="Adınız ve Soyadınızı yazın" TabIndex="0" required autofocus></asp:TextBox>
                                                    </label>
                                                </div>
                                                <div>
                                                    <label>
                                                        <span>Firma Adı:</span>
                                                        <asp:TextBox ID="txtCompany" runat="server" placeholder="Firma iseniz lütfen ünvan belirtin" TabIndex="1" autofocus></asp:TextBox>
                                                    </label>
                                                </div>
                                                <div>
                                                    <label>
                                                        <span>Eposta: (zorunlu)</span>
                                                        <asp:TextBox ID="txtEmail" runat="server" placeholder="Eposta adresinizi doğru biçimde yazın" TabIndex="2" required></asp:TextBox>
                                                    </label>
                                                </div>
                                                <div>
                                                    <label>
                                                        <span>Telefon:</span>
                                                        <asp:TextBox ID="txtPhone" runat="server" placeholder="Telefon numaranızı alan konu ile birlikte yazın" TabIndex="3"></asp:TextBox>

                                                    </label>
                                                </div>
                                                <div>
                                                    <label>
                                                        <span>Konu: (zorunlu)</span>
                                                        <asp:TextBox ID="txtSubject" runat="server" placeholder="Belirtmek istediğiniz Konu" TabIndex="4" required></asp:TextBox>
                                                    </label>
                                                </div>
                                                <div>
                                                    <label>
                                                        <span>Mesajınız: (zorunlu)</span>
                                                        <asp:TextBox ID="txtMessage" runat="server" TextMode="MultiLine" placeholder="Mesajınızı yazın" TabIndex="5" required></asp:TextBox>
                                                    </label>
                                                </div>
                                                <div>
                                                    <asp:Button ID="contact_submit1" Text="Mesajı Gönder" runat="server" OnClick="contact_submit1_Click" />

                                                </div>
                                            </div>
                                            <!-- /Form -->

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>



                </div>
            </div>
        </section>



    </div>







</asp:Content>

