﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder
{
    public partial class news_details : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string NewsCat, NewsCatId, NewsId;
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                string HaberKatAdi = RouteData.Values["NewsCat"].ToString().Replace("_", " ");
                //ltrlnewsCat.Text = Ayarlar.BasHarfBuyuk(NewsCat);

                NewsId = RouteData.Values["HaberId"].ToString();

                if (NewsId == null)
                {
                    HaberKatAdi = RouteData.Values["NewsCat"].ToString().Replace("_", " ");
                    NewsId = db.GetDataCell("Select KategoriId from Haberler where KategoriAdi like '%" + HaberKatAdi + "%'");

                    NewsCatId = db.GetDataCell("Select KategoriId from Haberler where HaberId=" + NewsId);


                }

                string haberlerBaslik = db.GetDataCell("Select newsHeader from GenelAyarlar");

                ltrlnewsCat.Text = "<a href=\"" + Page.ResolveUrl("~/") + haberlerBaslik +"/" +RouteData.Values["NewsCat"].ToString() + "\">" + Ayarlar.BasHarfBuyuk(HaberKatAdi) + "</a>";
            }
            catch (Exception)
            {

            }




            GetSidebarModules();
            GetFooterModules();

            NewsCatId = db.GetDataCell("Select KategoriId from Haberler where HaberId=" + NewsId);
            GetNewsbyId(NewsCatId);
             



            DataRow dr = db.GetDataRow("Select * from Haberler where HaberId=" + NewsId);
            ltrlBaslik.Text = dr["Baslik"].ToString();
            ltrlNewsTitle.Text = dr["Baslik"].ToString();

            string video = dr["Video"].ToString();

            if (video == "Null" || video == "" || video == null)
                pnlVideo.Visible = false;
            else
                ltrlVideo.Text = video;


            /* Facebook Likebox*/
            string fblink = db.GetDataCell("Select Facebook from GenelAyarlar");

            if (fblink != "" || fblink != null)
               ltrlFbComments.Text = "<div class=\"fb-comments yorum\"  data-href=\"" + Request.Url.Authority + Request.Url.AbsolutePath + "\" data-numposts=\"5\" data-colorscheme=\"light\"></div>";
            


            int hit = (int)dr["Hit"] + 1;
            db.cmd("update Haberler set Hit=" + hit.ToString() + " where HaberId=" + NewsId);

            string Resim;


            if (dr["Resim"] != null)
            {
              //  Resim = dr["Resim"].ToString();
                Resim = db.GetDataCell("Select Resim from HaberResimleri where IsPrimary=1 and HaberId=" + NewsId);
                ltrlContent.Text = "<img src=\"" + Page.ResolveUrl("~") + "upload/HaberResimleri/FotoGaleri/big/" + Resim + "\" align=\"left\" class=\"news_img img-responsive \" />" + "<h5 class='news_summary'>" + dr["Ozet"].ToString() + "</h5>" + dr["Detay"].ToString();
            }
            else
            {
                Resim = "";
                ltrlContent.Text = dr["Detay"].ToString();
            }

            string metaDesc = dr["MetaDesc"].ToString();
            string metaTitle = dr["MetaTitle"].ToString();
            string MetaKeyword = dr["MetaKeywords"].ToString();


            if (metaDesc == "" || metaDesc == null)
                metaDesc = Ayarlar.OzetCek(dr["Ozet"].ToString(), 150);


            if (metaTitle == "" || metaTitle == null)
                metaTitle = Ayarlar.OzetCek(dr["Baslik"].ToString(), 70);
            

             this.Page.Title = metaTitle;
             this.Page.MetaDescription = metaDesc;
             this.Page.MetaKeywords = MetaKeyword;
             GetPhotos(NewsId);


             ltrlBlog.Text = db.GetDataCell("Select newsHeader from GenelAyarlar");
        }

        public void GetPhotos(string id) {

            DataTable dt = db.GetDataTable("Select Resim from HaberResimleri where IsPrimary=0 and HaberId="+id);
            rptPhotoGallery.DataSource = dt;
            rptPhotoGallery.DataBind();

            if (dt.Rows.Count == 0)
            {
                PnlGallery.Visible = false;
                PnlGallery.Style.Add("display","none");
                
            }
        }



        protected void rptImages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {


                string resimAdi = DataBinder.Eval(e.Item.DataItem, "Resim").ToString();
                //string KategoriAdi = DataBinder.Eval(e.Item.DataItem, "KategoriAdi").ToString();
                //string ResimAciklama = DataBinder.Eval(e.Item.DataItem, "Aciklama").ToString();
                string ResimAciklama = "KalDer";

                string resimYolu = Server.MapPath("~/upload/HaberResimleri/FotoGaleri/big" + "/" + resimAdi);


                Literal ltrlFigure = (Literal)e.Item.FindControl("ltrlFigure");

                Bitmap bitmap = new Bitmap(resimYolu);

                int iWidth = bitmap.Width;
                int iHeight = bitmap.Height;


                //  System.Drawing.Image img = System.Drawing.Image.FromFile(resimYolu);
                string dataSize = iWidth.ToString() + "x" + iHeight.ToString();

                string display = "";

                if (ResimAciklama == "")
                {
                    display = "display:none !important;";
                }
                else
                    display = "display:visible !important;";

                string figure = "<figure class=\"col-md-3 col-sm-3 form-group\" itemprop=\"associatedMedia\" itemscope itemtype=\"http://schema.org/ImageObject\">" +
                      "<a href=\"" + Page.ResolveClientUrl("~") + "/upload/HaberResimleri/FotoGaleri/big" + "/" + resimAdi + " \" itemprop=\"contentUrl\" data-size=\"" + dataSize + "\">" +
                          "<span class=\"helper\"></span><img class=\"img-responsive\" src=\"" + Page.ResolveClientUrl("~") + "/upload/HaberResimleri/FotoGaleri/thumb" + "/" + resimAdi + " \" itemprop=\"thumbnail\" alt=\"" + ResimAciklama + "\" />" +
                      "</a>" +
                       // "<figcaption style=\"" + display + "\" itemprop=\"caption description\">" + ResimAciklama + "</figcaption>" +
                    "</figure>";


                ltrlFigure.Text = figure;

            }
        }




        public void GetSidebarModules()
        {
            DataTable dt = db.GetDataTable("select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=3 and mss.IsActive=1 and m.SectionId=2 order by mss.OrderNo");
            rptSidebarModules.DataSource = dt;
            rptSidebarModules.DataBind();

          
        }

        protected void rptSidebarModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);

                }
            }
        }


        public void GetNewsbyId(string Id)
        {
            rptOtherNews.DataSource = db.GetDataTable("Select * from Haberler as h inner join HaberKategori as hk on h.KategoriId=hk.KategoriId where h.KategoriId=" + Id + " and h.Durum=1 order by h.SiraNo");
            rptOtherNews.DataBind();
        }

        public void GetFooterModules()
        {
            DataTable dt = db.GetDataTable("select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=3 and mss.IsActive=1 and m.SectionId=5 order by m.OrderNo");
            rptFooterModules.DataSource = dt;
            rptFooterModules.DataBind();
        }

        protected void rptFooterModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);

                }
            }
        }

        public void Page_PreInit()
        {
            // Set the Theme for the page. Post-data is not currently loaded
            // Use trace="true" to see the Form collection
            //this.Theme = db.GetDataCell("Select Tema from GenelAyarlar");
            // if (Request.Form != null && Request.Form.Count > 0)
            //     this.Theme = this.Request.Form[4].Trim();
        }

        protected void rptOtherNews_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string id = DataBinder.Eval(e.Item.DataItem, "HaberId").ToString();
                string baslik = DataBinder.Eval(e.Item.DataItem, "Baslik").ToString();
                string ozet = DataBinder.Eval(e.Item.DataItem, "Ozet").ToString();

                string imageURL = db.GetDataCell("Select Resim from HaberResimleri where IsPrimary=1 and HaberId=" + id);

                System.Web.UI.WebControls.Image img = (System.Web.UI.WebControls.Image)e.Item.FindControl("img");
                img.ImageUrl = Page.ResolveUrl("~/upload/HaberResimleri/FotoGaleri/thumb/") + imageURL;
                img.AlternateText = baslik;
               // Literal ltrlImage = (Literal)e.Item.FindControl("ltrlImage");
               // ltrlImage.Text = "<img src=\"" + Page.ResolveUrl("~/upload/HaberResimleri/FotoGaleri/big/") + imageURL + "\" alt=\"" + baslik + "\" data-description=\"" + ozet + "\" />";

            }
        }

        protected void rptContentModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);


                    PropertyInfo[] info = uc.GetType().GetProperties();


                    foreach (PropertyInfo item in info)
                    {
                        if (item.CanWrite)
                        {
                            if (item.Name == "PageIDwithURL")
                                item.SetValue(uc, ltrlnewsCat, null);

                        }
                    }

                }
            }




        }
    }
}