﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Kalder
{
    class Takvim
    {

        public Takvim()
        {

        }
        //public static string connetionstring = ConfigurationManager.ConnectionStrings["CportalConnectionString"].ConnectionString;

        //SqlConnection bgl = new SqlConnection(connetionstring);

        // public string connetionstring = "Data Source=(local);Initial Catalog=CS_Akatlar;Integrated Security=SSPI;";
        SqlConnection bgl = new SqlConnection(ConfigurationManager.ConnectionStrings["KalderConnectionString"].ConnectionString);


        SqlCommand cmd;
        DataTable dt;

        public DataTable NotesDate()
        {
            cmd = new SqlCommand("select * from Events WHERE IsActive=1 and ISNULL(BeginDate,'')<>''", bgl);
            BglState();
            SqlDataReader dr = cmd.ExecuteReader();
            dt = new DataTable();
            dt.Load(dr);
            bgl.Close();
            dr.Close();
            cmd.Dispose();
            return dt;
        }

        void BglState()
        {
            if (bgl.State == ConnectionState.Closed)
                bgl.Open();
        }

    }
}
