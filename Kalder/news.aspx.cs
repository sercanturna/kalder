﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder
{
    public partial class news : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string NewsCat, NewsCatId;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                NewsCat = RouteData.Values["NewsCat"].ToString();
                ltrlnewsCat.Text = Ayarlar.BasHarfBuyuk(NewsCat);

                ltrlBaslik.Text = ltrlnewsCat.Text;
            }
            catch (Exception)
            {

            }

            GetSidebarModules();
            GetFooterModules();


            if (NewsCat == null || NewsCat == "")
            {
                GetNews();
            }
            else
            {
                NewsCatId = db.GetDataCell("Select KategoriId from HaberKategori where SeoURL like'" + NewsCat + "'");
                GetNewsbyId(NewsCatId);
            }

            ltrlBlog.Text = db.GetDataCell("Select newsHeader from GenelAyarlar");
        }


        public void GetSidebarModules()
        {
            DataTable dt = db.GetDataTable("select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=3 and mss.IsActive=1 and m.SectionId=2 order by mss.OrderNo");
            rptSidebarModules.DataSource = dt;
            rptSidebarModules.DataBind();

            if (dt.Rows.Count == 0)
            {
               // sideBar.Visible = false;


               // pageContent.Style.Add("width", "100%");
            }
        }

        protected void rptSidebarModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);

                }
            }
        }

        public void GetFooterModules()
        {
            DataTable dt = db.GetDataTable("select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=3 and mss.IsActive=1 and m.SectionId=5 order by m.OrderNo");
            rptFooterModules.DataSource = dt;
            rptFooterModules.DataBind();
        }

        protected void rptFooterModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);

                }
            }
        }

        public void GetNewsbyId(string Id)
        {
            DataTable dt = db.GetDataTable("Select * from Haberler as h inner join HaberKategori as hk on h.KategoriId=hk.KategoriId where h.KategoriId=" + Id + " and h.Durum=1 order by h.SiraNo");
            CollectionPager1.DataSource = dt.DefaultView;
            CollectionPager1.BindToControl = rptNews;
            rptNews.DataSource = CollectionPager1.DataSourcePaged;
            rptNews.DataBind();

            DataTable dtMansetHaberleri = db.GetDataTable("Select * from Haberler as h inner join HaberKategori as hk on h.KategoriId=hk.KategoriId where h.KategoriId=" + Id + " and Durum=1 and Manset=1 order by h.SiraNo");
            if (dtMansetHaberleri.Rows.Count < 1)
            { }
            //    pnlSlider.Visible = false;
            //rptNewsSlider.DataSource = dtMansetHaberleri;
            //rptNewsSlider.DataBind();


            //rptNewsSliderThumb.DataSource = dtMansetHaberleri;
            //rptNewsSliderThumb.DataBind();
        }

        public void GetNews()
        {
            DataTable dt = db.GetDataTable("Select * from Haberler as h inner join HaberKategori as hk on h.KategoriId=hk.KategoriId where Durum=1 order by h.SiraNo");
            CollectionPager1.DataSource = dt.DefaultView;
            CollectionPager1.BindToControl = rptNews;
            rptNews.DataSource = CollectionPager1.DataSourcePaged;
            rptNews.DataBind();


            DataTable dtMansetHaberleri = db.GetDataTable("Select * from Haberler as h inner join HaberKategori as hk on h.KategoriId=hk.KategoriId where Durum=1 and Manset=1 order by h.SiraNo");
            if (dtMansetHaberleri.Rows.Count < 1)
            {}
            //    pnlSlider.Visible = false;
            ////DataTable dtMansetHaberleri = db.GetDataTable("Select * from HaberKategori where YayinDurumu=1 order by KategoriSiraNo");
            //rptNewsSlider.DataSource = dtMansetHaberleri;
            //rptNewsSlider.DataBind();


            //rptNewsSliderThumb.DataSource = dtMansetHaberleri;
            //rptNewsSliderThumb.DataBind();

        }

        protected void rpt_News_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string id = DataBinder.Eval(e.Item.DataItem, "HaberId").ToString();
                string imageURL = db.GetDataCell("Select Resim from HaberResimleri where IsPrimary=1 and HaberId=" + id);

                System.Web.UI.WebControls.Image img = (System.Web.UI.WebControls.Image)e.Item.FindControl("img");
                img.ImageUrl = Page.ResolveUrl("~/upload/HaberResimleri/FotoGaleri/thumb/") + imageURL;

            }

        }

        public void Page_PreInit()
        {
            // Set the Theme for the page. Post-data is not currently loaded
            // Use trace="true" to see the Form collection
            // this.Theme = db.GetDataCell("Select Tema from GenelAyarlar");
            // if (Request.Form != null && Request.Form.Count > 0)
            //     this.Theme = this.Request.Form[4].Trim();
        }

        protected void rptNewsSlider_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string id = DataBinder.Eval(e.Item.DataItem, "HaberId").ToString();
                string baslik = DataBinder.Eval(e.Item.DataItem, "Baslik").ToString();
                string ozet = DataBinder.Eval(e.Item.DataItem, "Ozet").ToString();

                string imageURL = db.GetDataCell("Select Resim from HaberResimleri where IsPrimary=1 and HaberId=" + id);

                //System.Web.UI.WebControls.Image img = (System.Web.UI.WebControls.Image)e.Item.FindControl("img");
                //img.ImageUrl = Page.ResolveUrl("~/upload/HaberResimleri/FotoGaleri/thumb/") + imageURL;
                Literal ltrlImage = (Literal)e.Item.FindControl("ltrlImage");
                ltrlImage.Text = "<img src=\"" + Page.ResolveUrl("~/upload/HaberResimleri/FotoGaleri/big/") + imageURL + "\" alt=\"" + baslik + "\" data-description=\"" + ozet + "\" />";

            }
             
        }
    }
}