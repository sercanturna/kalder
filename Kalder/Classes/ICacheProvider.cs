﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kalder
{
    public interface ICacheProvider
    {
        void Set(string key, object value, int expireAsMinute);

        T Get<T>(string key);

        void Remove(string key);

        Dictionary<string, object> RemoveAll();

        bool IsExist(string key);
    }
}