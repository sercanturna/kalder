﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
 

namespace Kalder
{
    #region MenuItemOptions
    public class MenuItemOptions
    {
        private string _active = string.Empty;
        public string active
        {
            get { return _active; }
            set { _active = value; }
        }

        private bool _selected = false;
        public bool selected
        {
            get { return _selected; }
            set { _selected = value; }
        }

        private string _open = string.Empty;
        public string open
        {
            get { return _open; }
            set { _open = value; }
        }
    }
    #endregion

    #region MenuItem
    public class MenuItem
    {
        public int? PageId { get; set; }
        public int? ParentPageId { get; set; }
        public string PageName { get; set; }
        public string PageUrl { get; set; }
        public string PageImage { get; set; }
        public string Meta_Title { get; set; }
        public string Meta_Keywords { get; set; }
        public string Meta_Desc { get; set; }
        public string PageContent { get; set; }
        public bool? PageIsActive { get; set; }
        public bool? ShowMenu { get; set; }
        public int? Cols { get; set; }
        public int? Hit { get; set; }
        public int? OrderNumber { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? GalleryId { get; set; }

        public List<MenuItem> MenuItems = new List<MenuItem>();
    }
    #endregion

    public class Menu
    {
        DbConnection db = new DbConnection();

        private StringBuilder menuStringBuilder = new StringBuilder();

        #region GetAll
        public List<MenuItem> GetAll()
        {
            CacheProvider cp = new CacheProvider();

            if (cp.IsExist("menu"))
            {
                return cp.Get<List<MenuItem>>("menu");
            }

            DataTable ds = db.GetDataTable("select * from Pages where PageIsActive=1 and ShowMenu=1 order by OrderNumber");

            List<MenuItem> menus = ds.AsEnumerable().Select(m => new MenuItem()
            {
                PageId = m.Field<int?>("PageId"),
                ParentPageId = m.Field<int?>("ParentPageId"),
                PageName = m.Field<string>("PageName"),
                PageUrl = m.Field<string>("PageUrl"),
                PageImage = m.Field<string>("PageImage"),
                Meta_Title = m.Field<string>("Meta_Title"),
                Meta_Keywords = m.Field<string>("Meta_Keywords"),
                Meta_Desc = m.Field<string>("Meta_Desc"),
                PageContent = m.Field<string>("PageContent"),
                PageIsActive = m.Field<bool?>("PageIsActive"),
                ShowMenu = m.Field<bool?>("ShowMenu"),
                Cols = m.Field<int?>("Cols"),
                Hit = m.Field<int?>("Hit"),
                OrderNumber = m.Field<int?>("OrderNumber"),
                CreatedDate = m.Field<DateTime?>("CreatedDate"),
                GalleryId = m.Field<int?>("GalleryId"),


            }).ToList();

            foreach (MenuItem mi in menus)
            {
                MenuItem _mi = menus.Where(w => w.PageId == mi.ParentPageId).FirstOrDefault();
                if (_mi != null) _mi.MenuItems.Add(mi);
            }

            menus = menus.Where(w => w.ParentPageId == 0).ToList();

            cp.Remove("menu");
            cp.Set("menu", menus, 60 * 24 * 10);

            return menus;
        }
        #endregion

        #region CreateMenuString
        private void CreateMenuString(List<MenuItem> mil, int i)
        {
            string ul_attributes = string.Empty;
            if (i == 0)
            {
               // ul_attributes = " class=\" ownmenu\" data-keep-expanded=\"false\" data-auto-scroll=\"true\" data-slide-speed=\"200\"";
                i++;
            }
            else
            {
                ul_attributes = " class=\"dropdown animated-3s fadeIn\"";
                menuStringBuilder.Append("<ul" + ul_attributes + ">");
            }

            

            foreach (MenuItem mi in mil)
            {
                MenuItemOptions mio = new MenuItemOptions();

                /*if (mi.area == ViewBag.area && mi.controller == ViewBag.controller && mi.action == ViewBag.action) { mio.selected = true; mio.open = "open"; mio.active = "active"; }
                else if (mi.area == ViewBag.area && mi.controller == ViewBag.controller) { mio.selected = true; mio.open = "open"; mio.active = "active"; }
                else if (mi.area == ViewBag.area && mi.area != null && mi.topmenuid == 0) { mio.selected = true; mio.open = "open"; mio.active = "active"; }
                */

                menuStringBuilder.Append("<li class=\"" + mio.active + " " + mio.open + "\">");

                if (mi.PageUrl.Contains("http") || mi.PageUrl.Contains("pdf"))
                {
                    menuStringBuilder.Append("<a href=" + mi.PageUrl + " target=\"_blank\"><span>" + mi.PageName + "</span></a>");
                }
                else
                menuStringBuilder.Append("<a href=" +(HttpContext.Current.Handler as Page).ResolveUrl("~/") + mi.PageUrl + "><span>" + mi.PageName + "</span></a>");

                //menusb.Append("<i class=\"\"></i>");

              //  menuStringBuilder.Append("<span class=\"title\">" + mi.PageName + "</span>");
                if (mio.selected) menuStringBuilder.Append("<span class=\"selected\"></span>");

                //menuStringBuilder.Append("<span class=\"arrow " + mio.open + "\"></span>");

                menuStringBuilder.Append("</a>");

                if (mi.MenuItems.Count > 0)
                {
                    CreateMenuString(mi.MenuItems, i);
                    menuStringBuilder.Append("</li>");
                }
                else
                {
                    menuStringBuilder.Append("</li>");
                }
            }
             menuStringBuilder.Append("</ul>");
        }
        #endregion

        #region WriteMenuItem
        public string WriteMenuItem(List<MenuItem> mil, int i)
        {
            CreateMenuString(mil, i);
            return menuStringBuilder.ToString();
        }
        #endregion
    }
}