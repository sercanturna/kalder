﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Kalder._default" %>

<%@ Register src="include/references.ascx" tagname="references" tagprefix="uc1" %>
<%--<%@ Register src="include/TextSlider.ascx" tagname="TextSlider" tagprefix="uc2" %>--%>
 
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     
<script src="<%= ResolveUrl("~/") %>include/sliderengine/amazingslider.js"></script>
<script src="<%= ResolveUrl("~/") %>include/sliderengine/initslider-1.js"></script>
<!-- End of head section HTML codes -->

    <style>
        table.yasal tbody tr td {
        padding-bottom:10px; 
        }

            table.yasal tbody tr td h2 {
            font-family: 'Open Sans', sans-serif; font-size:12px; letter-spacing:0px;
            }

                table.yasal tbody tr td.sep {
                padding:0 5px;
                }

            table.yasal tbody tr td:first-child {
            width:26%;
            }
                             

        table.yasal td  {
        font-size:13px;
        }
   
    </style>



    <asp:Literal ID="ltrlCustomStyle" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
  <%--  <uc2:TextSlider ID="TextSlider1" runat="server" />--%>
    
    <asp:Repeater ID="rptBodyModules" runat="server" OnItemDataBound="rptBodyModules_ItemDataBound">
        <ItemTemplate>
            <asp:PlaceHolder ID="phBodyModules" runat="server"></asp:PlaceHolder>
        </ItemTemplate>
    </asp:Repeater>
           
    <div class="clear"></div>
           
    <div class="content_wrapper">
        <asp:Repeater ID="rptContentModules" runat="server" OnItemDataBound="rptContentModules_ItemDataBound">
            <ItemTemplate>
                <asp:PlaceHolder ID="phContentModules" runat="server"></asp:PlaceHolder>
            </ItemTemplate>
        </asp:Repeater>
    </div>
            <div class="clear"></div>

     <asp:Repeater ID="rptFooterModules" runat="server" OnItemDataBound="rptFooterModules_ItemDataBound">
        <ItemTemplate>
            <asp:PlaceHolder ID="phFooterModules" runat="server"></asp:PlaceHolder>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>

