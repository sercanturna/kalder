﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using System.Linq;
using System.Security.Cryptography; 

/// <summary>
/// Summary description for Ayarlar
/// </summary>
public class Ayarlar
{
	public Ayarlar()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static string md5(string sPassword)
    {
        System.Security.Cryptography.MD5CryptoServiceProvider x = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] bs = System.Text.Encoding.UTF8.GetBytes(sPassword);
        bs = x.ComputeHash(bs);
        System.Text.StringBuilder s = new System.Text.StringBuilder();
        foreach (byte b in bs)
        {
            s.Append(b.ToString("x2").ToLower());
        }
        return s.ToString();
    }

    public static string GetSubDomain()
    {
        System.Uri url = new System.Uri(HttpContext.Current.Request.Url.AbsoluteUri);

        if (url.HostNameType == UriHostNameType.Dns)
        {

            string host = url.Host;

            var nodes = host.Split('.');
            int startNode = 0;
            if (nodes[0] == "www") startNode = 1;

            return string.Format("{0}", nodes[startNode]);

        }

        return null;
    }
    

    public static string FileName(string Metin)
    {
        string deger = Metin;

        deger = deger.Replace(" ", "_");
        deger = deger.Replace("'", "");
        deger = deger.Replace("<", "");
        deger = deger.Replace(">", "");
        deger = deger.Replace("&", "");
        deger = deger.Replace("[", "");
        deger = deger.Replace("]", "");


        return deger;
    }

    public static int RandomNumber(int min, int max)
    {
        Random random = new Random();
        return random.Next(min, max);
    }

    public static string Temizle(string Metin)
    {
        string deger = Metin;  

        deger = deger.Replace("'", "");
        deger = deger.Replace("<", "");
        deger = deger.Replace(">", "");
        deger = deger.Replace("&", "");
        deger = deger.Replace("[", "");
        deger = deger.Replace("]", "");
        deger = deger.Replace("-", "");
        deger = deger.Replace(";", "");
        deger = deger.Replace(":", "");
     
        return deger;
    }

    public static string SQLInjectionControl(string Metin)
    {
        string deger = Metin;

        deger = deger.Replace("-", "");
        deger = deger.Replace("/*", "");
        deger = deger.Replace("*/", "");

        return deger;
    }

  

    public static string sayikontrol(string Text)
    {
        try
        {
            int x = Convert.ToInt32(Text);
        }
        catch
        {
            Text = "0";
        }
        
        return Text;
    }

    public static string UnicodeConvert(string Metin)
    {
    string  deger = Metin.ToString();
            deger = deger.Replace("%u011f","ğ");
            deger = deger.Replace("%u011e","Ğ");
            deger = deger.Replace("%u0131","ı");
            deger = deger.Replace("%u0130","İ");
            deger = deger.Replace("%u00f6","ö");
            deger = deger.Replace("%u00d6","Ö");
            deger = deger.Replace("%u00fc","ü");
            deger = deger.Replace("%u00dc","Ü");
            deger = deger.Replace("%u015f","ş");
            deger = deger.Replace("%u015e","Ş");
            deger = deger.Replace("%u00e7","ç");
            deger = deger.Replace("%u00c7", "Ç");
            deger = deger.Replace("+", "");
            deger = deger.Replace(" ", "");
            deger = deger.Replace("%0a", "");
         deger = deger.Replace("%09%2c", ",");
            deger = deger.Replace("%09", "");
        
        

        return deger;
    }

    public static string UrlSeo(string Metin)
    {
        string deger = Metin.ToString().ToLower();
        deger = deger.Replace("&nbsp;", "_");
        deger = deger.Replace("-", "_");
        deger = deger.Replace(".", null);
        deger = deger.Replace("'", null);
        deger = deger.Replace(",", null);
        deger = deger.Replace(":", null);
        deger = deger.Replace("+", null);
        deger = deger.Replace("\"", null);
        deger = deger.Replace("?", null);
        deger = deger.Replace("(", null);
        deger = deger.Replace(")", null);
        deger = deger.Replace("!", null);
        deger = deger.Replace("%", null);
        deger = deger.Replace(" ", "_");
        deger = deger.Replace("ç", "c");
        deger = deger.Replace("ö", "o");
        deger = deger.Replace("&#199;", "c");
        deger = deger.Replace("&#246;", "o");
        deger = deger.Replace("&#214;", "O");
        deger = deger.Replace("&#252;", "U");
        deger = deger.Replace("&#220;", "u");
        deger = deger.Replace("&#231;", "C");
        deger = deger.Replace("&#174;", "®");
        deger = deger.Replace("&amp;", "-");
        deger = deger.Replace("é", "e");
        deger = deger.Replace("è", "e");
        deger = deger.Replace("ë", "e");
        deger = deger.Replace("ê", "e");
        deger = deger.Replace("É", "e");
        deger = deger.Replace("È", "e");
        deger = deger.Replace("Ë", "e");
        deger = deger.Replace("Ê", "e");
        deger = deger.Replace("í", "i");
        deger = deger.Replace("&#253;", "i");
        deger = deger.Replace("ì", "i");
        deger = deger.Replace("î", "i");
        deger = deger.Replace("ï", "i");
        deger = deger.Replace("I", "i");
        deger = deger.Replace("&#221;", "i");
        deger = deger.Replace("Í", "i");
        deger = deger.Replace("Ì", "i");
        deger = deger.Replace("Î", "i");
        deger = deger.Replace("Ï", "i");
        deger = deger.Replace("ó", "o");
        deger = deger.Replace("ò", "o");
        deger = deger.Replace("ô", "o");
        deger = deger.Replace("Ó", "o");
        deger = deger.Replace("Ò", "o");
        deger = deger.Replace("Ô", "o");
        deger = deger.Replace("á", "a");
        deger = deger.Replace("ä", "a");
        deger = deger.Replace("â", "a");
        deger = deger.Replace("à", "a");
        deger = deger.Replace("â", "a");
        deger = deger.Replace("Ä", "a");
        deger = deger.Replace("Â", "a");
        deger = deger.Replace("Á", "a");
        deger = deger.Replace("À", "a");
        deger = deger.Replace("Â", "a");
        deger = deger.Replace("ú", "u");
        deger = deger.Replace("ù", "u");
        deger = deger.Replace("û", "u");
        deger = deger.Replace("Ú", "u");
        deger = deger.Replace("Ù", "u");
        deger = deger.Replace("Û", "u");
        deger = deger.Replace("&", "_");
        deger = deger.Replace("ı", "i");
        deger = deger.Replace("ü", "u");
        deger = deger.Replace("ş", "s");
        deger = deger.Replace("ğ", "g");
        deger = deger.Replace("İ", "I");
        deger = deger.Replace("Ö", "O");
        deger = deger.Replace("Ç", "C");
        deger = deger.Replace("Ğ", "G");
        deger = deger.Replace("Ş", "S");
        deger = deger.Replace("Ü", "U");
        deger = deger.Replace(">", null);
        deger = deger.Replace("<", null);
        deger = deger.Replace("/", null);
        deger = deger.Replace("~£", "-");
        deger = deger.Replace("£", null);
        deger = deger.Replace("~~", "/");
        deger = deger.Replace("`", "'");
        deger = deger.Replace("__", "_");
        deger = deger.Replace("__", "_");
        deger = deger.Replace("__", "_");

        return deger;
    }

    public static string ResimAdi(string Metin)
    {
        string deger = Metin.ToString().ToLower();
        deger = deger.Replace("&nbsp;", "_");
        deger = deger.Replace("-", "_");
        deger = deger.Replace("'", null);
        deger = deger.Replace(",", null);
        deger = deger.Replace(":", null);
        deger = deger.Replace("+", null);
        deger = deger.Replace("\"", null);
        deger = deger.Replace("?", null);
        deger = deger.Replace("%", null);
        deger = deger.Replace(" ", "_");
        deger = deger.Replace("ç", "c");
        deger = deger.Replace("ö", "o");
        deger = deger.Replace("&#199;", "c");
        deger = deger.Replace("&#246;", "o");
        deger = deger.Replace("&#214;", "O");
        deger = deger.Replace("&#252;", "U");
        deger = deger.Replace("&#220;", "u");
        deger = deger.Replace("&#231;", "C");
        deger = deger.Replace("&#174;", "®");
        deger = deger.Replace("&amp;", "-");
        deger = deger.Replace("é", "e");
        deger = deger.Replace("è", "e");
        deger = deger.Replace("ë", "e");
        deger = deger.Replace("ê", "e");
        deger = deger.Replace("É", "e");
        deger = deger.Replace("È", "e");
        deger = deger.Replace("Ë", "e");
        deger = deger.Replace("Ê", "e");
        deger = deger.Replace("í", "i");
        deger = deger.Replace("&#253;", "i");
        deger = deger.Replace("ì", "i");
        deger = deger.Replace("î", "i");
        deger = deger.Replace("ï", "i");
        deger = deger.Replace("I", "i");
        deger = deger.Replace("&#221;", "i");
        deger = deger.Replace("Í", "i");
        deger = deger.Replace("Ì", "i");
        deger = deger.Replace("Î", "i");
        deger = deger.Replace("Ï", "i");
        deger = deger.Replace("ó", "o");
        deger = deger.Replace("ò", "o");
        deger = deger.Replace("ô", "o");
        deger = deger.Replace("Ó", "o");
        deger = deger.Replace("Ò", "o");
        deger = deger.Replace("Ô", "o");
        deger = deger.Replace("á", "a");
        deger = deger.Replace("ä", "a");
        deger = deger.Replace("â", "a");
        deger = deger.Replace("à", "a");
        deger = deger.Replace("â", "a");
        deger = deger.Replace("Ä", "a");
        deger = deger.Replace("Â", "a");
        deger = deger.Replace("Á", "a");
        deger = deger.Replace("À", "a");
        deger = deger.Replace("Â", "a");
        deger = deger.Replace("ú", "u");
        deger = deger.Replace("ù", "u");
        deger = deger.Replace("û", "u");
        deger = deger.Replace("Ú", "u");
        deger = deger.Replace("Ù", "u");
        deger = deger.Replace("Û", "u");
        deger = deger.Replace("&", "_");
        deger = deger.Replace("ı", "i");
        deger = deger.Replace("ü", "u");
        deger = deger.Replace("ş", "s");
        deger = deger.Replace("ğ", "g");
        deger = deger.Replace("İ", "I");
        deger = deger.Replace("Ö", "O");
        deger = deger.Replace("Ç", "C");
        deger = deger.Replace("Ğ", "G");
        deger = deger.Replace("Ş", "S");
        deger = deger.Replace("Ü", "U");
        deger = deger.Replace(">", null);
        deger = deger.Replace("<", null);
        deger = deger.Replace("/", null);
        deger = deger.Replace("~£", "-");
        deger = deger.Replace("£", null);
        deger = deger.Replace("~~", "/");
        deger = deger.Replace("`", "'");
        deger = deger.Replace("__", "_");
        deger = deger.Replace("__", "_");
        deger = deger.Replace("__", "_");

        return deger;
    }

    public static string OzetCek(string Metin, int Karakter)
    {
        if (Metin.Length >= Karakter)
            Metin = Metin.Substring(0, Karakter) + "...";

        return Metin;
    }

    public static Bitmap ResimBoyutlandir(Bitmap resim, int boyut)
    {
        Bitmap sresim = resim;

        using (Bitmap OrjinalResim = resim)
        {
            double yukseklik = OrjinalResim.Height;
            double genislik = OrjinalResim.Width;
            double oran = 0;

            if (genislik > yukseklik && genislik > boyut)
            {
                oran = genislik / yukseklik;
                genislik = boyut;
                yukseklik = boyut / oran;
            }
            else if (yukseklik > genislik || yukseklik > boyut)
            {
                oran = yukseklik / genislik;
                yukseklik = boyut;
                genislik = boyut / oran;
            }

            Size ydeger = new Size(Convert.ToInt32(genislik), Convert.ToInt32(yukseklik));
            Bitmap yresim = new Bitmap(OrjinalResim, ydeger);
            sresim = yresim;
            OrjinalResim.Dispose();
        }

        return sresim;
    }


    public static Bitmap ResimBoyutlandirYukseklik(Bitmap resim, int boyut)
    {
        Bitmap sresim = resim;

        using (Bitmap OrjinalResim = resim)
        {
            double yukseklik = OrjinalResim.Height;
            double genislik = OrjinalResim.Width;
            double oran = 0;

            if (genislik > yukseklik && yukseklik > boyut)
            {
                oran = yukseklik / boyut;
                yukseklik = boyut;
                genislik = genislik / oran;
            }
            //else if (yukseklik > genislik || yukseklik > boyut)
            //{
            //    oran = yukseklik / genislik;
            //    yukseklik = boyut;
            //    genislik = boyut / oran;
            //}

            Size ydeger = new Size(Convert.ToInt32(genislik), Convert.ToInt32(yukseklik));
            Bitmap yresim = new Bitmap(OrjinalResim, ydeger);
            sresim = yresim;
            OrjinalResim.Dispose();
        }

        return sresim;
    }


    public static string BasHarfBuyuk(string str)
    {
        str = str.ToLower();
        char[] stra = str.ToCharArray();
        for (int i = 0; i < stra.Length; i++)
        {
            if (i == 0)
            {
                str = string.Empty;
                str += stra[i].ToString().ToUpper();
            }
            else
            {
                str += stra[i].ToString();
            }
        }
        return str;
    }

    public static class Alert
    {

        /// <summary> 
        /// Shows a client-side JavaScript alert in the browser. 
        /// </summary> 
        /// <param name="message">The message to appear in the alert.</param> 
        public static void Show(string message)
        {
            // Cleans the message to allow single quotation marks 
            string cleanMessage = message.Replace("'", "\\'");
            string script = "<script type=\"text/javascript\">alert('" + cleanMessage + "');</script>";

            // Gets the executing web page 
            Page page = HttpContext.Current.CurrentHandler as Page;

            // Checks if the handler is a Page and that the script isn't allready on the Page 
            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
            {
                page.ClientScript.RegisterClientScriptBlock(typeof(Alert), "alert", script);
            }
        }


        public static void result()
        {
            string script = "<script type=\"text/javascript\">Javascript:Success();</script>";

            Page page = HttpContext.Current.CurrentHandler as Page;

            // Checks if the handler is a Page and that the script isn't allready on the Page 
            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("result"))
            {
                page.ClientScript.RegisterClientScriptBlock(typeof(Page), "result", script);

            }
        }

         
    }


    public static string HtmlveCssTemizle(string html)
    {
        if (String.IsNullOrEmpty(html)) return "";
        HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
        doc.LoadHtml(html);
        doc.DocumentNode.Descendants()
                .Where(n => n.Name == "script" || n.Name == "style")
                .ToList()
                .ForEach(n => n.Remove());

        return HttpUtility.HtmlDecode(doc.DocumentNode.InnerText);


    }


    public static string HtmlTagTemizle(string source)
    {
        return Regex.Replace(source, "<.*?>", string.Empty);
    }

    /// <summary>
    /// Compiled regular expression for performance.
    /// </summary>
    static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

    /// <summary>
    /// Remove HTML from string with compiled Regex.
    /// </summary>
    public static string StripTagsRegexCompiled(string source)
    {
        return _htmlRegex.Replace(source, string.Empty);
    }

    /// <summary>
    /// Remove HTML tags from string using char array.
    /// </summary>
    public static string StripTagsCharArray(string source)
    {
        char[] array = new char[source.Length];
        int arrayIndex = 0;
        bool inside = false;

        for (int i = 0; i < source.Length; i++)
        {
            char let = source[i];
            if (let == '<')
            {
                inside = true;
                continue;
            }
            if (let == '>')
            {
                inside = false;
                continue;
            }
            if (!inside)
            {
                array[arrayIndex] = let;
                arrayIndex++;
            }
        }
        return new string(array, 0, arrayIndex);
    }
}