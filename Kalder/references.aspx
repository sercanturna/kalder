﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="references.aspx.cs" Inherits="Kalder.references" %>
 
<%@ Register Assembly="CollectionPager" Namespace="SiteUtils" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   <style>
  
        /*div.gallery-container img{
            height:auto;
        min-height:80px !important;
        max-width:120px !important;
        padding-top:10% !important;
        }*/

       .gallery-container figure, .categorycontainer li {
           width: 140px;
           height: 104px;
       }

       .gallery-container .img_wrapper
{
     width:100%;  height:100%; overflow:hidden; white-space: nowrap; text-align:center
}

.gallery-container .helper {
    display: inline-block;
    height: 100%;
    vertical-align: middle;
}

       .gallery-container img, .categorycontainer img {
       height:auto !important;
       }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  
    <div class="clear page_container">
           <div class="breadcumb">
            <ul id="breadcrumb" class="breadcumb">
                <li><a href="<%= Page.ResolveUrl("~")%>Anasayfa" title="Ana Sayfa">
                    <img src="<%= Page.ResolveUrl("~/")%>images/home-icon.png" alt="Home" class="home" /></a></li>

                <li><a href="<%= Page.ResolveUrl("~")%>referanslar" title="Referanslar"><asp:Literal ID="ltrlBaslik" runat="server"></asp:Literal></a></li>
            </ul>
            
            <div class="social_share">
        <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style " style="float:right; width:150px;">
        <a class="addthis_button_preferred_1"></a>
        <a class="addthis_button_preferred_2"></a>
        <a class="addthis_button_preferred_3"></a>
        <a class="addthis_button_preferred_4"></a>
        <a class="addthis_button_compact"></a>
        <a class="addthis_counter addthis_bubble_style"></a>
        </div>
        <script type="text/javascript">var addthis_config = { "data_track_addressbar": false };</script>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f34f2f752ee3f86"></script>
        <!-- AddThis Button END -->
            </div>

        </div>



       
        <div id="pageContent" class="page_content references" runat="server">
         

             <asp:Repeater ID="rptContentModules" runat="server" OnItemDataBound="rptContentModules_ItemDataBound">
                <ItemTemplate>
                    <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:Repeater>


             <h1 class="title"><span><asp:Literal ID="ltrlBaslik2" runat="server"></asp:Literal></span></h1>
             <asp:UpdatePanel ID="upRef" runat="server">
                 <ContentTemplate>
                     <div class="gallery-container">
                 <asp:Repeater ID="rptRef" runat="server">
                     <ItemTemplate>
                         <figure><a href="http://<%#Eval("Link").ToString().Replace("http://","") %>" target="_blank">
                             <div class="img_wrapper"><span class="helper"></span><img src="<%=Page.ResolveUrl("~/upload/Referans/") %><%#Eval("BrandLogo") %>" alt="<%#Eval("BrandName") %>" /></div></a></figure>
                     </ItemTemplate>
                 </asp:Repeater>
             </div>
               
             

             
                    <section id="paginations" style="border-top:1px dashed #d3d3d3 !important; float:left; width:100% !important; padding-top:10px;">
                                <div class="sayfalama">
                                  <cc1:CollectionPager ID="CollectionPager1" runat="server" BackText=" « Önceki" 
                                    FirstText="İlk" LabelText="" LastText="Son" NextText="Sonraki »" 
                                    PageNumbersDisplay="Numbers" ResultsFormat="Resimler {0}-{1} (Toplam:{2})" ResultsStyle="font-weight:normal; font-size:12px !important; float:left; line-height:2.4; padding-right:10px;"
                                    PageSize="20" SectionPadding="5" BackNextDisplay="Buttons" BackNextLocation="Split" 
                                    MaxPages="5" PageNumbersSeparator=""></cc1:CollectionPager>
                         </div>
                                  </section>

                       </ContentTemplate>
             </asp:UpdatePanel>
        </div>

         <div id="sideBar" class="sidebar" runat="server">

           <asp:Repeater ID="rptSidebarModules" runat="server" OnItemDataBound="rptSidebarModules_ItemDataBound">
                <ItemTemplate>
                    <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:Repeater>

           

        </div>
        <!-- / Sidebar-->

        </div>

     <asp:Repeater ID="rptFooterModules" runat="server" OnItemDataBound="rptFooterModules_ItemDataBound">
            <ItemTemplate>
                <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
            </ItemTemplate>
    </asp:Repeater>
</asp:Content>

