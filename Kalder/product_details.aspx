﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="product_details.aspx.cs" Inherits="Kalder.product_details" %>
 <%@ Register src="include/references.ascx" tagname="references" tagprefix="uc1" %>
<%@ Register src="include/breadcumb.ascx" tagname="breadcumb" tagprefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="<%=Page.ResolveUrl("~") %>js/font-size.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script> 
    <script src="<%=Page.ResolveUrl("~") %>js/jquery.elevatezoom.js"></script>
    <link rel="stylesheet" href="<%=Page.ResolveUrl("~") %>js/jquery.fancybox.css" />
    <script src="<%=Page.ResolveUrl("~") %>js/jquery.fancybox.pack.js"></script>
      
    

    <link href="<%=Page.ResolveUrl("~") %>js/Responsive-Tabs/responsive-tabs.css" rel="stylesheet" />
     <script>
         $('document').ready(function () {
             
             //  $('.fb-comments').attr('data-width', $('#commentboxcontainer').width());
               
             $(".fb-comments").attr("data-width", $(".fb-comments").parent().width()-30);
            
             $(window).on('resize', function () {
                 resizeFacebookComments();
             });

             function resizeFacebookComments() {
                 var src = $('.fb-comments iframe').attr('src').split('width='),
                     width = $(".fb-comments").parent().width();

                 $('.fb-comments iframe').attr('src', src[0] + 'width=' + width);
             }

               
          
         });


    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="clear page_container">
  <uc2:breadcumb ID="breadcumb1" runat="server" />

    
 

         <div id="pageContent" class="page_content" runat="server">
            <h1 class="title"><span class="left"><asp:Literal ID="ltrlBaslik" runat="server"></asp:Literal></span> 
                <div id="controls" class="right" style="display:none;">
                    <a href="#" id="small">A</a>
                    <a href="#" id="medium" class="selected">A</a>
                    <a href="#" id="large">A</a>
                </div>
            </h1>

            <div class="clear"></div>
            <div class="product-gallery-container left">
                <asp:Literal ID="ltrlDefaultImage" runat="server"></asp:Literal>
                

                <div id="gal1">

                    <asp:Repeater ID="rptProductImages" runat="server">
                        <ItemTemplate>
                            <a href="#" data-image="<%=Page.ResolveUrl("~") %>upload/urunler/<%#Eval("ImageURL") %>" data-zoom-image="<%=Page.ResolveUrl("~") %>upload/urunler/<%#Eval("ImageURL") %>">
                               <span><img id="<%#Eval("ImgId") %>" class="thumb" src="<%=Page.ResolveUrl("~") %>upload/urunler/<%#Eval("ImageURL") %>" /></span> 
                            </a>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div class="product_detail">
 
               
                
    <table class="details">
    <tbody>
         <tr>
            <td colspan="4" style="font-weight:400; font-size:14px; ">Ürün detayları</td>
         </tr>
         <tr >
             <td class="baslik">Marka</td>
           
             <td class="content"><asp:Literal ID="ltrlBrand" runat="server"></asp:Literal></td>
         </tr>
         <tr >
             <td class="baslik">Ürün Adı</td>
          
             <td class="content"><asp:Literal ID="ltrlProductName" runat="server"></asp:Literal> </td>
         </tr>
         <tr >
             <td class="baslik">Kategori</td>
       
             <td class="content"><asp:Literal ID="ltrlProductCat" runat="server"></asp:Literal></td>
         </tr>
         <tr >
             <td class="baslik">Ürün Kodu</td>
           
             <td class="content"><asp:Literal ID="ltrlProductCode" runat="server"></asp:Literal></td>
         </tr>
         <tr >
             <td class="baslik">Etiketler</td>
          
             <td class="content"><asp:Literal ID="ltrlTags" runat="server"></asp:Literal></td>
         </tr>
         <tr >
             <td class="baslik">Fiyat</td>
         
             <td class="content price"><asp:Literal ID="ltrlPrice" runat="server"></asp:Literal></td>
         </tr>
          <tr id="KDV" runat="server">
             <td class="baslik">KDV Dahil</td>
           
             <td class="content price"><asp:Literal ID="ltrlTotalPrice" runat="server"></asp:Literal></td>
         </tr>
    </tbody>
    </table>
 
            </div>

            <div class="clear"></div>
            <div class="tabview">
                <!--Horizontal Tab-->
                                    <div id="horizontalTab">
                                        <ul>
                                            <li><a href="#tab-1">Ürün Detay</a></li>
                                            <li><a href="#tab-2">Ürün Yorumları</a></li>
                                            <li><a href="#tab-3">Diğer Ürünler</a></li>
                                           
                                        </ul>

                                        <div id="tab-1" class="text_content detail">
                                            <asp:Literal ID="ltrlproductDesc" runat="server"></asp:Literal>
                                        </div>
                                        <div id="tab-2">
                                            <div id="commentboxcontainer" style="width:100%; margin-top:10px;">
                                                <asp:Literal ID="ltrlFbComments" runat="server"></asp:Literal>
                                            </div>
                                        </div>
                                        <div id="tab-3">
                                             <div class="clear"></div>
                                            <div class="text_content other_products">
                                                <ul class="other_products">
                                                    <asp:Repeater ID="rpt_Products" runat="server" OnItemDataBound="rpt_Products_ItemDataBound">
                                                        <ItemTemplate>
                                                            <li><a href="<%#Page.ResolveUrl("~/urunler/")+Ayarlar.UrlSeo(Eval("KatAdi").ToString()) %>/<%#Eval("SeoUrl") %>"><img src="<%=Page.ResolveUrl("~") %>upload/urunler/<%#Eval("ImageURL") %>" /><p><%#Ayarlar.OzetCek(Eval("ProductName").ToString(),95) %></p><div><%# String.Format("{0:00.00}", Eval("UnitPrice"))%> <%#Eval("Currency")%><asp:Literal ID="ltrlKDV" runat="server"></asp:Literal></div></a>
                                                                 
                                                            </li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </ul>
                                            </div>
                                        </div>
                                     

                                    </div>
            </div>
        </div>

        <div class="sidebar" id="sideBar" runat="server">
              
            <asp:Repeater ID="rptSidebarModules" runat="server" OnItemDataBound="rptSidebarModules_ItemDataBound">
                <ItemTemplate>
                    <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:Repeater>
           
        </div>
        <!-- / Sidebar-->

        </div>
        <asp:Repeater ID="rptFooterModules" runat="server" OnItemDataBound="rptFooterModules_ItemDataBound">
            <ItemTemplate>
                <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
            </ItemTemplate>
    </asp:Repeater>

    <style>
         table.details tbody{
                        font-family: 'Open Sans', Helvetica, Arial, sans-serif;
                    }
                    table.details tbody tr:nth-child(2n+1) {
                    background-color:#fbfbfb;
                    }

                    table.details tbody tr td.baslik {
                    width:25%; padding-right:0; background-color:#f0f0f0; border:1px solid #fff; font-weight:600; vertical-align:middle;
                    }
                    table.details tbody tr td.ayrac {
                    padding:0; margin:0; width:10px;  
                    }
                    .zoomContainer {
                    border:1px solid #ccc !important;
                    }

        div.detail div{
        width:100% !important; padding:0 !important; margin:0 !important; display:block !important; 
        }
      
        
         ul.other_products li a {
        display:block; width:100%; clear:both; overflow:hidden;  margin:10px;  text-decoration:none;
        }
        ul.other_products li {
        border:1px solid #ececec; margin-bottom:5px;
        }
     
        
        ul.other_products li a img {
        float:left; width:10%; height:10%;  margin-right:10px;  border:1px solid #fff; padding:2px;
        }
            ul.other_products li a div {
                float:left; font-size:14px; font-weight:bold;
            }
            ul.other_products li:hover a > img{
        border:1px solid #808080; padding:2px;
        }
               ul.other_products li:hover {
        background-color:#ececec;
        }
               .fb-comments, .fb-comments span[style], .fb-comments iframe[style] {width: 100% !important;}
    </style>


    <!-- Responsive Tabs JS -->
    <script src="<%=Page.ResolveUrl("~") %>js/Responsive-Tabs/jquery.responsiveTabs.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#horizontalTab').responsiveTabs({
                rotate: false,
                startCollapsed: 'accordion',
                collapsible: 'accordion',
                setHash: true,
              //  disabled: [3, 4]
            });

            $('#start-rotation').on('click', function () {
                $('#horizontalTab').responsiveTabs('active');
            });
            $('#stop-rotation').on('click', function () {
                $('#horizontalTab').responsiveTabs('stopRotation');
            });
            $('#start-rotation').on('click', function () {
                $('#horizontalTab').responsiveTabs('active');
            });
            $('.select-tab').on('click', function () {
                $('#horizontalTab').responsiveTabs('activate', $(this).val());
            });

        });

        $("#zoom").elevateZoom({ gallery: 'gal1', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: false, loadingIcon: '<%=Page.ResolveUrl("~") %>images/progress.gif' });
        //pass the images to Fancybox
        $("#zoom").bind("click", function (e) {
            var ez = $('#zoom').data('elevateZoom');
            $.fancybox(ez.getGalleryList());
            return false;
        });

 



    </script>
     
  
</asp:Content>

