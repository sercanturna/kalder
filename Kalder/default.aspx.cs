﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace Kalder
{
    public partial class _default : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {

            GetBodyModules();
            GetContentModules();
            GetFooterModules();

            try
            {
                DataRow dr = db.GetDataRow("Select * from GenelAyarlar");
                Page.Title = dr["MetaTitle"].ToString();
                Page.MetaDescription = dr["MetaDescription"].ToString();
                Page.MetaKeywords = dr["MetaKeywords"].ToString();
            }
            catch (Exception ex)
            {
               // Ayarlar.Alert.Show(ex.Message);
            }

           // IncludeCSS(this, "style/color.aspx");

        }

        [System.Web.Services.WebMethod]
        public static void AverageTime(string time)
        {
           // string IpAdres = Request.ServerVariables["REMOTE_ADDR"].ToString(); //Ip Adresini alıyoruz.
            string IpAdres = System.Web.HttpContext.Current.Request.UserHostAddress;
            DbConnection db = new DbConnection();
            int sonuc = db.cmd("update Stats set AvarageTime=AvarageTime+" + time + " where Ip ='" + IpAdres + "'");

          //  db.cmd("update Stats set AvarageTime=5");
        }



        public static void IncludeCSS(Page page, string cssfile)
        {
            HtmlGenericControl child = new HtmlGenericControl("link");
            child.Attributes.Add("rel", "stylesheet");
            child.Attributes.Add("href", cssfile);
            child.Attributes.Add("type", "text/css");
            page.Header.Controls.AddAt(0, child);
        }


        public void GetBodyModules()
        {
            //DataTable dt = db.GetDataTable("select * from modules as m inner join ModuleSections as ms on m.SectionId=ms.SectionId where IsActive=1 and m.SectionId=1 order by OrderNo");
            DataTable dt = db.GetDataTable("select m.ModuleId,m.ModuleName,m.ModuleContent,m.IsActive, mss.IsActive, ms.SectionName, mss.OrderNo, mss.SectionId, mss.SegmentId, msg.SegmentName from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId inner join ModuleSections as ms on mss.SectionId=ms.SectionId inner join ModuleSegments as msg on mss.SegmentId=msg.SegmentId where mss.SegmentId=1 and mss.SectionId=1 and mss.IsActive=1 order by mss.OrderNo");
            rptBodyModules.DataSource = dt;
            rptBodyModules.DataBind();

        }

        public void GetContentModules()
        {
            DataTable dt = db.GetDataTable("Select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=1 and mss.IsActive=1 and m.SectionId=4 order by mss.OrderNo");
            rptContentModules.DataSource = dt;
            rptContentModules.DataBind();

            if (dt.Rows.Count > 0)
            {
                double x, oran, p, w, new_width;
                x = dt.Rows.Count;

                double bordersize = x * 0.203;

                if (x > 1)
                {
                    oran = 1;
                    p = (x - 1) * oran;  // 2

                    // double w = 97.600;
                    w = 100 - bordersize;

                    new_width = (w - p) / x;

                    ltrlCustomStyle.Text = "<style>div.content_wrapper div.colums{ width:" + new_width.ToString().Replace(",", ".") + "% !important; margin-left: " + oran.ToString().Replace(",", ".") + "% ; height:410px; overflow:hidden;}</style>";


                    //Response.Write(
                    //    x.ToString() + "<br>" +
                    //    p.ToString() + "<br>" +
                    //    new_width.ToString()
                    //    );

                }
                 

                //pageContent.Style.Add("width", "100%");
            }
        }

        public void GetFooterModules()
        {
            DataTable dt = db.GetDataTable("Select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=1 and mss.IsActive=1 and m.SectionId=5 order by mss.OrderNo");
            rptFooterModules.DataSource = dt;
            rptFooterModules.DataBind();

        }



        protected void rptBodyModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phBodyModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);

                    //Bu kod bloğu eğer  ekleyeceğim modülden gelen bir parametreyi set ediyorsam gerekli
                    //if (drv["ModuleId"].ToString() == "11")
                    //{
                    //    PropertyInfo[] info = uc.GetType().GetProperties();


                    //    foreach (PropertyInfo item in info)
                    //    {
                    //        if (item.CanWrite)
                    //        {
                    //            if(item.Name=="PageIDwithURL")
                    //                item.SetValue(uc, PageURL,null);

                    //        }
                    //    }

                    //}
                }
            }

        }


        protected void rptContentModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phContentModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);

                    //Bu kod bloğu eğer  ekleyeceğim modülden gelen bir parametreyi set ediyorsam gerekli
                    //if (drv["ModuleId"].ToString() == "11")
                    //{
                    //    PropertyInfo[] info = uc.GetType().GetProperties();


                    //    foreach (PropertyInfo item in info)
                    //    {
                    //        if (item.CanWrite)
                    //        {
                    //            if(item.Name=="PageIDwithURL")
                    //                item.SetValue(uc, PageURL,null);

                    //        }
                    //    }

                    //}
                }
            }

        }

        protected void rptFooterModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phFooterModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);

                    //Bu kod bloğu eğer  ekleyeceğim modülden gelen bir parametreyi set ediyorsam gerekli
                    //if (drv["ModuleId"].ToString() == "11")
                    //{
                    //    PropertyInfo[] info = uc.GetType().GetProperties();


                    //    foreach (PropertyInfo item in info)
                    //    {
                    //        if (item.CanWrite)
                    //        {
                    //            if(item.Name=="PageIDwithURL")
                    //                item.SetValue(uc, PageURL,null);

                    //        }
                    //    }

                    //}
                }
            }

        }

        public void Page_PreInit()
        {
            // Set the Theme for the page. Post-data is not currently loaded
            // Use trace="true" to see the Form collection
            //this.Theme = db.GetDataCell("Select Tema from GenelAyarlar");
            //if (Request.Form != null && Request.Form.Count > 0)
            //    this.Theme = this.Request.Form[4].Trim();
        }
    }
}