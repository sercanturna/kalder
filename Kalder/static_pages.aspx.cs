﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Kalder
{
    public partial class static_pages : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string PageURL, PageId, ParentPageId;
        protected void Page_Load(object sender, EventArgs e)
        {
            PageURL = RouteData.Values["PageURL"].ToString();
            

            PageId = db.GetDataCell("Select top 1 PageId from Pages where PageURL='" + PageURL + "' order by PageId desc");

            hdnPid.Value = PageId;
           

            ParentPageId = db.GetDataCell("Select ParentPageId from Pages where PageId='" + PageId + "'");


           int resimGalerisi = GetImages(PageId);
           if (resimGalerisi > 0)
           {
               PnlGallery.Visible = true;
               galleryRow.Visible = true;
           }
           else
           {
               PnlGallery.Visible = false;
               galleryRow.Visible = false;
           }
               

            GetPage(PageId);
            GetSidebarModules();
            GetContentModules();
            GetFooterModules();


            DataRow dr = db.GetDataRow("Select top 1 * from Pages where PageId=" + PageId);

            string ImageURL = dr["PageImage"].ToString();
            if (ImageURL == "resim_yok.jpg" || ImageURL == "")
                imgPageHeader.Visible = false;
            else
                imgPageHeader.ImageUrl = Page.ResolveUrl("~") + "upload/SayfaResimleri/big/" + dr["PageImage"].ToString();

            ltrlBaslik.Text = dr["PageName"].ToString();
            ltrlText.Text = dr["PageContent"].ToString();


            this.Page.Title = dr["Meta_Title"].ToString();
            this.Page.MetaDescription = dr["Meta_Desc"].ToString();
            this.Page.MetaKeywords = dr["Meta_Keywords"].ToString();


            int hit = (int)dr["Hit"] + 1;

            db.cmd("update Pages set Hit=" + hit.ToString() + " where PageId=" + PageId);

        }



        public int GetImages(string PageId)
        {
            DataTable dt = db.GetDataTable("Select p.GalleryId,gk.*,gr.* from Pages as p inner join GaleriKategori as gk on p.GalleryId=gk.GaleriId inner join GaleriResim as gr on gk.GaleriId=gr.GaleriId where gr.YayinDurumu=1 and p.PageId=" + PageId);
            rptImageGallery.DataSource = dt;
            rptImageGallery.DataBind();

            return dt.Rows.Count;

        }



        private int getParent(int PageId, PlaceHolder treeMenu)
        {
            string parentPageId = db.GetDataCell("select ParentPageId from Pages where PageId=" + PageId + "order by PageId asc");

            // DataSet ds = db.GetDataSet("select * from Pages where PageId=" + parentPageId + "and PageIsActive=1 and ShowMenu=1");bu şekilde breadcumb da sayfa gözükmüyor
            DataSet ds = db.GetDataSet("select * from Pages where PageId=" + parentPageId);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string ParentPageName = db.GetDataCell("Select PageName from Pages where PageId=" + parentPageId);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (parentPageId != "0")
                    {
                        int id = Convert.ToInt32(parentPageId);
                        getParent(id, treeMenu);
                    }
                    treeMenu.Controls.Add(new LiteralControl("<li><a href=" + Page.ResolveUrl("~") + dr["PageURL"].ToString() + ">" + dr["PageName"].ToString() + "</a></li>"));
                }
                return 0;
            }
            return 0;
        }

        protected void rptBreadcrumb_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;

                PlaceHolder treeMenu = (PlaceHolder)e.Item.FindControl("treeMenu");
                treeMenu.Controls.Clear();

                //DataSet ds = db.GetDataSet("Select * From Pages where PageId=" + PageId + "and PageIsActive=1 and ShowMenu=1"); bu şekilde breadcumb da sayfa gözükmüyor
                DataSet ds = db.GetDataSet("Select * From Pages where PageId=" + PageId);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if ((int)dr["ParentPageId"] != 0)
                    {
                        int Id = (int)dr["PageId"];
                        getParent(Convert.ToInt32(PageId), treeMenu);
                    }

                    treeMenu.Controls.Add(new LiteralControl("<li>" + dr["PageName"].ToString() + "</li>"));
                }
            }
        }

        public void GetPage(string PageId)
        {
            //rptBreadcrumb.DataSource = db.GetDataTable("select * from Pages where PageId=" + PageId +" and PageIsActive=1 and ShowMenu=1 order by OrderNumber"); bu şekilde breadcumb da sayfa gözükmüyor
            rptBreadcrumb.DataSource = db.GetDataTable("select * from Pages where PageId=" + PageId + " order by OrderNumber");
            rptBreadcrumb.DataBind();
        }





        public void Page_PreInit()
        {
            // this.Theme = db.GetDataCell("Select Tema from GenelAyarlar");
            // if (Request.Form != null && Request.Form.Count > 0)
            //     this.Theme = this.Request.Form[4].Trim();
        }



        public void GetSidebarModules()
        {
            DataTable dt = db.GetDataTable("select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=2 and mss.IsActive=1 and m.SectionId=2 order by mss.OrderNo");
            rptSidebarModules.DataSource = dt;
            rptSidebarModules.DataBind();

            if (dt.Rows.Count == 0)
            {
                sideBar.Visible = false;
              //  pageContent.Style.Add("width", "100%");
            }

        }

        public void GetContentModules()
        {
            DataTable dt = db.GetDataTable("select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=2 and mss.IsActive=1 and m.SectionId=1 order by mss.OrderNo");
            rptContentModules.DataSource = dt;
            rptContentModules.DataBind();
        }

        public void GetFooterModules()
        {
            DataTable dt = db.GetDataTable("select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=2 and mss.IsActive=1 and m.SectionId=5 order by mss.OrderNo");
            rptFooterModules.DataSource = dt;
            rptFooterModules.DataBind();
        }








        protected void rptImages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {


                string resimAdi = DataBinder.Eval(e.Item.DataItem, "ResimAdi").ToString();
                string KategoriAdi = DataBinder.Eval(e.Item.DataItem, "KategoriAdi").ToString();
                string ResimAciklama = DataBinder.Eval(e.Item.DataItem, "Aciklama").ToString();


                string resimYolu = Server.MapPath("~/upload/Galeri/" + KategoriAdi + "/" + resimAdi);


                Literal ltrlFigure = (Literal)e.Item.FindControl("ltrlFigure");

                Bitmap bitmap = new Bitmap(resimYolu);

                int iWidth = bitmap.Width;
                int iHeight = bitmap.Height;


                //  System.Drawing.Image img = System.Drawing.Image.FromFile(resimYolu);
                string dataSize = iWidth.ToString() + "x" + iHeight.ToString();

                string display = "";

                if (ResimAciklama == "")
                {
                    display = "display:none !important;";
                }
                else
                    display = "display:visible !important;";

                //string figure = "<figure itemprop=\"associatedMedia\" itemscope itemtype=\"http://schema.org/ImageObject\">" +
                //      "<a href=\"" + Page.ResolveClientUrl("~") + "/upload/Galeri/" + KategoriAdi + "/" + resimAdi + " \" itemprop=\"contentUrl\" data-size=\"" + dataSize + "\">" +
                //          "<img src=\"" + Page.ResolveClientUrl("~") + "/upload/Galeri/" + KategoriAdi + "/" + resimAdi + " \" itemprop=\"thumbnail\" alt=\"" + ResimAciklama + "\" />" +
                //      "</a>" +
                //        "<figcaption style=\"" + display + "\" itemprop=\"caption description\">" + ResimAciklama + "</figcaption>" +
                //    "</figure>";


                //ltrlFigure.Text = figure;

                string figure = "<figure class=\"col-md-3 col-sm-3 form-group\" itemprop=\"associatedMedia\" itemscope itemtype=\"http://schema.org/ImageObject\">" +
                      "<a href=\"" + Page.ResolveClientUrl("~") + "/upload/Galeri/" + KategoriAdi + "/" + resimAdi + " \" itemprop=\"contentUrl\" data-size=\"" + dataSize + "\">" +
                          "<span class=\"helper\"></span><img class=\"img-responsive\" src=\"" + Page.ResolveClientUrl("~") + "/upload/Galeri/" + KategoriAdi + "/" + resimAdi + " \" itemprop=\"thumbnail\" alt=\"" + ResimAciklama + "\" />" +
                      "</a>" +
                     "<figcaption style=\"" + display + "\" itemprop=\"caption description\">" + ResimAciklama + "</figcaption>" +
                    "</figure>";


                ltrlFigure.Text = figure;



            }
        }



        protected void rptSidebarModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);


                    if (drv["ModuleId"].ToString() == "11")
                    {
                        PropertyInfo[] info = uc.GetType().GetProperties();


                        foreach (PropertyInfo item in info)
                        {
                            if (item.CanWrite)
                            {
                                if (item.Name == "PageIDwithURL")
                                    item.SetValue(uc, PageURL, null);

                               

                            }
                        }

                    }
                }
            }




        }

        protected void rptContentModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);


                    PropertyInfo[] info = uc.GetType().GetProperties();


                    foreach (PropertyInfo item in info)
                    {
                        if (item.CanWrite)
                        {
                            if (item.Name == "PageIDwithURL")
                                item.SetValue(uc, PageURL, null);

                        }
                    }

                }
            }




        }

        protected void rptFooterModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);
                }
            }




        }
    }
}