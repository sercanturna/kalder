﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LatestNews_Side.ascx.cs" Inherits="Kalder.include.LatestNews_Side" %>


<style>

    ul.list-group li {
    padding:10px 0;
    }
    ul.list-group li h2 {
    font-size:13px; padding:0 5px; margin:0 !important; font-weight:400;
    }

    ul.list-group li img {
    width:80px; float:left; padding-right:10px;
    }

    .more {
    color:#f68a1e !important;
    }

</style>

<aside class="blog" style="margin-top:4%; background-color:#f2f2f2;">
   

        <ul class="list-group">
                <a><h5>İlgili Haberler</h5></a>
            <asp:Repeater ID="rptNews" runat="server" OnItemDataBound="rptNews_ItemDataBound">
                <ItemTemplate>
                    <li>
                        <div class="">
                           <%-- <div class="overlay">--%>
                                <asp:Image ID="img" runat="server" /><h2><%#Eval("Baslik") %></h2>
                                <p>
                                    
                                    <a href="<%= ResolveUrl("~/") %><%#Ayarlar.UrlSeo(Eval("KategoriAdi").ToString()) %>/<%#Eval("HaberId") %>/<%#Ayarlar.UrlSeo(Eval("Baslik").ToString()) %>" class="more">Devamını oku »</a>
                                </p>
                           <%-- </div>--%>
                          
                        </div>

                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
   
</aside>
