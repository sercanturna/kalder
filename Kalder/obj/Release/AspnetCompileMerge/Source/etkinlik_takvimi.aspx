﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="etkinlik_takvimi.aspx.cs" Inherits="Kalder.etkinlik_takvimi" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<title>KalDer | Etkinlik Takvimi</title>
<meta name="Keywords" content="KalDer etkinlikleri, KalDer etkinlikler, KalDer etkinlik takvimi, etkinlik, aktivite, toplu aktivite, kongre, kalite kongresi " />
<meta name="Description" content="KalDer | Etkinlik Takvimi " />
    

         <script src="<%=Page.ResolveUrl("~/") %>js/jquery-qtip/jquery-1.3.2.min.js"></script>
    <script src="<%=Page.ResolveUrl("~/") %>js/jquery-qtip/jquery.qtip-1.0.0-rc3.min.js" type="text/javascript"></script>

      <script type="text/javascript" charset="utf-8">
          $(document).ready(function () {
              $(function () {

                  $("div.carousel-inner div:first-child").addClass("active");


                  $('#myCarousel2').carousel({
                      interval: 4000
                  });

                  $('.carousel').carousel({
                      interval: 4000
                  });


                  $('#takvim-list').perfectScrollbar();
                     


              });
          });
          </script>
       <script src="<%=Page.ResolveUrl("~/") %>js/jquery-qtip/jquery-1.3.2.min.js"></script>
    <script src="<%=Page.ResolveUrl("~/") %>js/jquery-qtip/jquery.qtip-1.0.0-rc3.min.js" type="text/javascript"></script>


    <script class="qtip" type="text/javascript">
        var $j = jQuery.noConflict();


         $j(document).ready(function () {
        
 
            $j('td.qtip[alt]').click(function () {
                $(this).css('backgroundColor', '#5e5e5e');
            });

            $j('td.qtip[alt]').qtip({

                content: {

                    // text: '<img class="throbber" src="/projects/qtip/images/throbber.gif" alt="Loading..." />',
                    //  url: $(this).attr('rel'), // Use the rel attribute of each element for the url to load
                    title: {
                        // text: $(this).qtip(), // Give the tooltip a title using each elements text
                        button: 'Close' // Show a close link in the title
                    }
                },
                //position: {
                //    corner: {
                //        tooltip: "bottomLeft",
                //        target: "topRight"
                //    }
                //},
                position: {
                    corner: {
                        target: 'bottomMiddle', // Position the tooltip above the link
                        tooltip: 'topMiddle'
                    },
                    adjust: {
                        screen: true // Keep the tooltip on-screen at all times
                    }
                },
                show: {
                    when: 'click',
                    solo: true // Only show one tooltip at a time
                },
                hide: {
                    when: { event: 'unfocus' }
                },



                style: {
                    tip: true, // Apply a speech bubble tip to the tooltip at the designated tooltip corner
                    border: {
                        width: 4,
                        radius: 7,
                        color: '#000'
                    },
                    name: 'light',
                    width: { min: 280 },
                    padding: 5,
                    textAlign: 'left',
                    tip: true,
                    name: 'light'


                },
                // The magic
                api: {
                    onHide: function () {
                        $j('td.qtip[title]').css('backgroundColor', '#eea404'); // <img src="images/smilies/wink.gif" style="vertical-align: middle;" border="0" alt="Wink" title="Wink" />
                    }
                }
            });



        });

       
        $(window).load(function () {
            $('#takvim-list').perfectScrollbar();
            $('#lesson-list').perfectScrollbar();
        });



        function BindEvents() {
            $(document).ready(function () {
                $('#takvim-list').perfectScrollbar();
                $('#lesson-list').perfectScrollbar();


                $j('td.qtip[alt]').click(function () {
                    $(this).css('backgroundColor', '#5e5e5e');
                });

                $j('td.qtip[alt]').qtip({

                    content: {

                        // text: '<img class="throbber" src="/projects/qtip/images/throbber.gif" alt="Loading..." />',
                        //  url: $(this).attr('rel'), // Use the rel attribute of each element for the url to load
                        title: {
                            // text: $(this).qtip(), // Give the tooltip a title using each elements text
                            button: 'Close' // Show a close link in the title
                        }
                    },
                    //position: {
                    //    corner: {
                    //        tooltip: "bottomLeft",
                    //        target: "topRight"
                    //    }
                    //},
                    position: {
                        corner: {
                            target: 'bottomMiddle', // Position the tooltip above the link
                            tooltip: 'topMiddle'
                        },
                        adjust: {
                            screen: true // Keep the tooltip on-screen at all times
                        }
                    },
                    show: {
                        when: 'click',
                        solo: true // Only show one tooltip at a time
                    },
                    hide: {
                        when: { event: 'unfocus' }
                    },



                    style: {
                        tip: true, // Apply a speech bubble tip to the tooltip at the designated tooltip corner
                        border: {
                            width: 4,
                            radius: 7,
                            color: '#000'
                        },
                        name: 'light',
                        width: { min: 280 },
                        padding: 5,
                        textAlign: 'left',
                        tip: true,
                        name: 'light'


                    },
                    // The magic
                    api: {
                        onHide: function () {
                            $j('td.qtip[title]').css('backgroundColor', '#eea404'); // <img src="images/smilies/wink.gif" style="vertical-align: middle;" border="0" alt="Wink" title="Wink" />
                        }
                    }
                });
            });
        }
      
          
           

       

</script>


     






    <link href="<%#Page.ResolveUrl("~") %>css/Takvim.css" rel="stylesheet" type="text/css" /> 
   
  
    <style>
        /*.Calender {
        width:100%;
        }

        ul.eventlist li img {
        width:100% !important;
        height:auto !important;
        }*/
  
 
 

#myCarousel2 .thumbnail {
	margin-bottom: 0;
}
.carousel-control.left, .carousel-control.right {
	background-image:none !important;
}
.carousel-control {
	color:#fff;
	top:45%;
	color:#1c1c1c;
	bottom:auto;
	padding-top:4px;
	width:30px;
	height:30px;
	text-shadow:none;
	opacity:1;
}
.carousel-control:hover {
	color: #ffa10b;
}
.carousel-control.left, .carousel-control.right {
	background-image:none !important;
}
.carousel-control.right {
	left:auto;
	right:-42px;
}
.carousel-control.left {
	right:auto;
	left:-32px;
}
 

        .well {
        background:none; border:none; box-shadow:none; margin:0 !important; padding: 0 40px !important;
        }

        .carousel-nav, carousel-pager {
        
        display:none;}


        ul.eventlist li a {
        color:#ffa10b;
        }


         .contentbox {
            overflow: hidden;
        }

        .overlay {
            background: rgba(0, 172, 168, .7) !important;
        }

        h5.bold {
        padding-left:10px;
        }

        i.margin-right {
        margin-right:10px;
        }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     
    

     <div id="screeno"></div>

    <!--======= CONTENT =========-->
		<div class="content" > 
			<section class="sub-banner" style="margin-top:178px;">
				  <div class="overlay">
					<div class="container">
					  <ol class="breadcrumb">
						<li><a href="<%= Page.ResolveUrl("~")%>Anasayfa" title="Ana Sayfa">
                    <%--<img src="<%= Page.ResolveUrl("~/App_Themes/")%><%= Page.Theme %>/images/home-icon.png" alt="Home" class="home" /></a></li>--%>
                <%--<img src="<%= Page.ResolveUrl("~/")%>images/home-icon.png" alt="Home" class="home" />--%>Anasayfa</a></li>
                <asp:Repeater ID="rptBreadcrumb" runat="server" OnItemDataBound="rptBreadcrumb_ItemDataBound">
                    <ItemTemplate>
                        <asp:PlaceHolder ID="treeMenu" runat="server"></asp:PlaceHolder>
                    </ItemTemplate>
                </asp:Repeater>
					  </ol>
					</div>
				  </div>
			</section>


            	<section class="innerpages">
				<div class="container"> 

                                                        <div class="tittle tittle-2">
										
										<h3><asp:Literal ID="ltrlBaslik" runat="server" Text="KalDer Etkinlik Takvimi"></asp:Literal></h3>
										<hr />
									</div>

					<div class="row">
					<div class="col-md-12">

                         <div class="contentbox">



                              <div class="well sponsor-logos">
                        <div class="text-center">
                            <!-- COROUSEL -->
                            <div id="eventBrochure" class="carousel slide">

                                <!-- Carousel items -->
                                <div class="carousel-inner">
                                    <asp:Repeater ID="rptEventsItems" runat="server" OnItemDataBound="rptEventsItems_ItemDataBound">
                                        <ItemTemplate>
                                            <%# (Container.ItemIndex + 4) % 4 == 0 ? "<div class=\"item row-fluid\">" : string.Empty %>
                                                          <div class="col-md-3 text-center">
                                                              <div class="thumbnail">
                                                                    <a href="etkinlikler/<%#Ayarlar.UrlSeo(Eval("SeoUrl").ToString())%>"><img src="upload/Etkinlikler/<%#Eval("BrochureImg") %>" alt="<%#Eval("EventName") %>" title="<%#Eval("EventName") %>" style="height:335px;" /></a>
                                                              </div>
                                                          </div>
                                            <%# (Container.ItemIndex + 4) % 4 == 3 ? "</div>" : string.Empty %>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </div>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                                <!-- /INNER -->

                            
                                <!-- CONTROLS -->
                                    <a class="left carousel-control" href="#eventBrochure" data-slide="prev"><i class="glyphicon glyphicon-chevron-left fa-2x"></i></a>
                                    <a class="right carousel-control" href="#eventBrochure" data-slide="next"><i class="glyphicon glyphicon-chevron-right fa-2x"></i></a>
                                <!-- /CONTROLS -->
                            </div>
                            <!-- /COROUSEL -->
                        </div>
                    </div>

                             </div>


                        </div>
                        </div>

                    <div class="row">
                        <div class="col-md-12">

                         <div class="contentbox">
  <!--Jquery Reference-->


      <div style="padding-bottom: 0px; margin-top: -10px;" class="space-divider"></div>
       
  
            <div class="row">
                <div class="col-md-2">
                    <div class="sd-services-carousel-desc">
                        <h3 class="sd-styled-title">etkinlik <strong>takvimi</strong></h3>
                        <p>tüm KalDer etkinlikleri hakkında detaylı bilgiye ulaşın..</p>
                    </div>
                </div>
                <div class="col-md-6 takvim">
                    

                    <asp:UpdatePanel ID="up3" runat="server" RenderMode="Inline" ChildrenAsTriggers="true">
                        <ContentTemplate>
                            <script type="text/javascript">
                                Sys.Application.add_load(BindEvents);
                            </script>
                        
                    <asp:Calendar  ID="Calendar1" runat="server" CssClass="Calender" NextPrevFormat="FullMonth" NextPrevStyle-Wrap="true" NextPrevStyle-Font-Underline="false" NextPrevStyle-HorizontalAlign="Left" NextPrevStyle-ForeColor="Orange"
                        OnDayRender="Calendar1_DayRender" BorderWidth="0">
                        <DayHeaderStyle Font-Bold="True" Font-Size="100%" Font-Underline="False" ForeColor="#eea404" Height="30" />
                        <NextPrevStyle CssClass="Ay" VerticalAlign="Bottom" />
                        <OtherMonthDayStyle ForeColor="Gray" />
                        <SelectedDayStyle BackColor="#cccccc" Font-Bold="True" ForeColor="white" />
                        <TitleStyle BackColor="Transparent" Font-Bold="True" ForeColor="white" Font-Size="110%" />
                        <TodayDayStyle CssClass="Bugun" />
                        <DayStyle CssClass="Gunler" />
                    </asp:Calendar>
                      
                            </ContentTemplate>
                    </asp:UpdatePanel>
                </div>

               
                <div class="col-md-4" id="takvim-list">

               

                    <ul class="list-unstyled events" style="padding-top:19px; padding-bottom:0px; ">
                        <asp:Repeater ID="rptPopulerEvents" runat="server">
                            <ItemTemplate>
                                <li><a href="etkinlikler/<%#Ayarlar.UrlSeo(Eval("SeoUrl").ToString())%>"><i class="glyphicon glyphicon-calendar pull-left margin-right"></i>
                                    <h5 class="bold"><%#Eval("EventName") %><br />
                                        <small><span style="color:#ffa10b; margin-left:13px;">Tarih :</span> <%# String.Format("{0:yyyy-MMMM-dd}", Convert.ToDateTime(Eval("BeginDate")).ToString("dd MMMM yyyy")) %> - <%# String.Format("{0:yyyy-MMMM-dd}", Convert.ToDateTime(Eval("EndDate")).ToString("dd MMMM yyyy")) %></small></h5>
                                </a></li>
                            </ItemTemplate>
                        </asp:Repeater>

 
                    </ul>
                 
                </div>
            </div>
      
 </div>
                            </div>
                    </div>

                    </div>
                    </section>
                    </div>
    



   
	 


      

</asp:Content>
