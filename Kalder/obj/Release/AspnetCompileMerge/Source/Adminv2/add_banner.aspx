﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="add_banner.aspx.cs" Inherits="Kalder.Adminv2.add_banners" %>

<%@ Register Assembly="CKFinder" Namespace="CKFinder" TagPrefix="CKFinder" %>


<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">

    <script src="js/multi.js" type="text/javascript"></script>
     <link href="./js/tags/jquery.tagsinput.css" rel="stylesheet" />
    <script src="./js/tags/jquery.tagsinput.js"></script>
   
      <script>
          
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 183px;
            font-family:Verdana;
            font-size:13px;
            font-weight:bold;
            color:#646464;
        }
          ul.resimler
        {
          
        }

            ul.resimler li
            {
                height:280px;
            }
    </style> 

    
    <script src="../ckeditor/ckeditor.js" type="text/javascript"></script>
 
    <script src="../ckfinder/ckfinder.js" type="text/javascript"></script>
    <script type="text/javascript">
        

        function SumChar(val) {
            var len = val.value.length;
            if (len >= 201) {
                val.value = val.value.substring(0, 200);
            } else {
                $('#desc').text(200 - len + " Karakter");
            }
        };

</script>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate></ContentTemplate>
    </asp:UpdatePanel>
    <div class="row">
        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-globe"></i>
                    <h3>Banner Ekle</h3>
                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                    <div id="edit-profile" class="form-horizontal">

    <div class="control-group">
        <label class="control-label" for="dropTarget">Link Hedef</label>
        <div class="controls">
            <asp:DropDownList ID="dropTarget" runat="server">
                <asp:ListItem Text="Link verecekseniz Seçin!" Value="_blank"></asp:ListItem>
                <asp:ListItem Text="Aynı Pencerede Aç" Value="_self"></asp:ListItem>
                <asp:ListItem Text="Yeni Pencerede Aç" Value="_blank"></asp:ListItem>
            </asp:DropDownList>
        </div>
        
        <!-- /controls -->
    </div>
    <!-- /control-group -->

    <div class="control-group">
        <label class="control-label" for="txtLink">Link</label>
        <div class="controls">
            <asp:TextBox ID="txtLink" runat="server" Width="300px" Text="#"></asp:TextBox>
        </div>
        
        <!-- /controls -->
    </div>
    <!-- /control-group -->


   <div class="control-group">
        <label class="control-label" for="fuResim">Banner Resmi</label>
        <div class="controls">
            <asp:FileUpload ID="fuResim" runat="server" />
            <p class="help-block">* Resim ölçüsü 468px x 60px boyutlarında olmalı!</p>
        </div>
        
        <!-- /controls -->
    </div>
    <!-- /control-group -->



    <div class="control-group">
        <label class="control-label" for="txtBaslik">Başlık</label>
        <div class="controls">
            <asp:TextBox ID="txtTitle" runat="server" Width="300px"></asp:TextBox>
        </div>
        
        <!-- /controls -->
    </div>
    <!-- /control-group -->
 

    <div class="control-group">
        <label class="control-label" for="chkOnay">Durum</label>
        <div class="controls">
             <asp:DropDownList ID="drpDurum" runat="server">
                 <asp:ListItem Value="">-Seçiniz-</asp:ListItem>
                 <asp:ListItem Value="1">Aktif</asp:ListItem>
                 <asp:ListItem Value="0">Pasif</asp:ListItem>
             </asp:DropDownList>
            <p class="help-block">* Durumu Pasif yaparsanız haber sitede gözükmez.</p>
        </div>
        
        <!-- /controls -->
    </div>
    <!-- /control-group -->

    

    <div class="control-group">
        <label class="control-label" for="chkManset">Sıra No</label>
        <div class="controls">
        <asp:TextBox ID="txtSiraNo" runat="server"></asp:TextBox>
            <p class="help-block">* Sırasını belirtin. Eğer istemiyorsanız boş bırakın</p>
        </div>
        
        <!-- /controls -->
    </div>
    <!-- /control-group -->

    <div class="form-actions">
           <asp:Button ID="btn_SlaytEkle" runat="server" CssClass="btn btn-primary" onclick="btn_SlaytEkle_Click" Text="Banner Ekle" />
            <asp:Button ID="btn_Temizle" runat="server" CssClass="btn"  Text="Temizle" />
        </div>
    <!-- /form-actions -->
   
                        </div>
                </div>
            </div>
         </div>
    </div>
</asp:Content>