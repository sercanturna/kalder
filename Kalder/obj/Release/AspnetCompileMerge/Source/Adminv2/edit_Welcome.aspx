﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="edit_Welcome.aspx.cs" Inherits="Kalder.Adminv2.edit_Welcome" %>


<%--<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit.HTMLEditor" tagprefix="cc1" %>--%>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="CKFinder" Namespace="CKFinder" TagPrefix="CKFinder" %>


<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script src="js/libs/jquery-1.8.3.min.js"></script>
    <script src="js/libs/jquery-ui-1.10.0.custom.min.js"></script>
    <script src="js/multi.js" type="text/javascript"></script>
    <link href="./js/tags/jquery.tagsinput.css" rel="stylesheet" />
    <script src="./js/tags/jquery.tagsinput.js"></script>




    <script src="js/validation/jquery.validationEngine-tr.js"></script>
    <script src="js/validation/jquery.validationEngine.js"></script>
    <link href="js/validation/validationEngine.jquery.css" rel="stylesheet" />

    <script>
        $(document).ready(function () {

            $("#eklentiler").addClass("active");

            $("#form1").validationEngine('attach', { promptPosition: "topRight", scroll: true });
        });
    </script>

    <style type="text/css">
        .style1 {
            width: 100%;
        }

        .style2 {
            width: 183px;
            font-family: Verdana;
            font-size: 13px;
            font-weight: bold;
            color: #646464;
        }

        ul.resimler {
        }

            ul.resimler li {
                height: 280px;
            }
    </style>


    <script src="../ckeditor/ckeditor.js" type="text/javascript"></script>

    <script src="../ckfinder/ckfinder.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.onload = function () {
            var editor = CKEDITOR.replace('<% = txtDetay.ClientID %>');
            CKFinder.setupCKEditor(editor, '../ckfinder');
        };

        function DescChar(val) {
            var len = val.value.length;
            if (len >= 201) {
                val.value = val.value.substring(0, 200);
            } else {
                $('#desc').text(200 - len + " Karakter");
            }
        };

        function TitleChar(val) {
            var len = val.value.length;
            if (len >= 70) {
                val.value = val.value.substring(0, 70);
            } else {
                $('#title').text(70 - len + " Karakter");
            }
        };




        $(function () {
            $('#content_tags').tagsInput({
                width: 'auto',
            });
        });

    </script>

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

   
    <div class="row">
        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-globe"></i>
                    <h3>Hoşgeldiniz Modülü Düzenle</h3>
                </div>
                <!-- /widget-header -->


                <div class="widget-content">
                        <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>


                    <div class="tab-content">


                        <div class="tab-pane active" id="genel">
                            <div id="edit-genel" class="form-horizontal">

                                    <div class="control-group">
                                    <label class="control-label" for="txtBaslik">Başlık</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtBaslik" TextMode="SingleLine" CssClass="validate[required] input-xlarge" runat="server"></asp:TextBox><div class="char" id="Div1"></div>
                                        <p class="help-block">Modül için görüntülenecek bir başlık yazın (Örn. Hoşgeldiniz, Hakkımda vb.)</p>
                                    </div>
                                    <!-- /controls -->
                                </div>
                                <!-- /control-group -->

                                <div class="control-group">
                                    <label class="control-label" for="txtDetay">İçerik</label>
                                    <div class="controls">
                                        <CKEditor:CKEditorControl ID="txtDetay" runat="server">
                                        </CKEditor:CKEditorControl>
                                        <p class="help-block">* Sayfa içeriğinin tamamını bu alanda yapılandırın.</p>
                                    </div>

                                    <!-- /controls -->
                                </div>
                                <!-- /control-group -->

                                <div class="control-group">
                                    <label class="control-label" for="fuResim">Sayfa Resmi</label>
                                    <div class="controls">
                                        <asp:Image ID="imgResim" runat="server" /><asp:FileUpload ID="fuResim" runat="server" />
                                        <span style="border: 0px solid #f00; position: absolute; z-index: 1000; right: 80px; margin-top: 20px;"><asp:Literal ID="ltrlSil" runat="server" Text="Resmi Sil"></asp:Literal>
                                            <asp:ImageButton ID="btnResimTemizle" runat="server" OnClick="btnResimTemizle_Click" ImageUrl="~/images/icon_close.png" /></span>
                                            <p class="help-block">* Burada seçilecek resmin 4x3 formatında olması gerekli.</p>
                                            
                                        
                                        
                                    </div>

                                    <!-- /controls -->
                                </div>
                                <!-- /control-group -->




                                <div class="control-group">
                                    <label class="control-label" for="vergidaire">Seo URL</label>
                                    <div class="controls">
                                        <asp:TextBox ID="txtSeoUrl" TextMode="SingleLine" CssClass="validate[required] input-xlarge" runat="server"></asp:TextBox><div class="char" id="Div1"></div>
                                        <p class="help-block">* Devam butonuna tıklanınca gidilecek adresi girin.</p>
                                    </div>
                                    <!-- /controls -->
                                </div>
                                <!-- /control-group -->

                            </div>
                        </div>


                    </div>
                  </ContentTemplate>
    </asp:UpdatePanel>
                    <div class="form-actions">
                        <asp:Button ID="btn_SayfaGuncelle" runat="server" CssClass="btn btn-primary" OnClick="btn_SayfaGuncelle_Click" Text="Güncelle" />                        
                        <asp:Button ID="btn_Temizle" runat="server" CssClass="btn" Text="İptal" />
                    </div>
                    <!-- /form-actions -->


                </div>
            </div>
        </div>
    </div>

   
    <script src="./js/demo/validation.js"></script>
</asp:Content>
