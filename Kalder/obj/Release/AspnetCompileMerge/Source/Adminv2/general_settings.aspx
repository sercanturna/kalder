﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="general_settings.aspx.cs" Inherits="Kalder.Adminv2.general_settings" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     
      <!-- jQuery/jQueryUI (hosted) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.js"></script>
		<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css"/>


        <script src="./js/colorpicker/jquery.colorpicker.js"></script>
		<link href="./js/colorpicker/jquery.colorpicker.css" rel="stylesheet" type="text/css"/>

        <link href="./js/tags/jquery.tagsinput.css" rel="stylesheet" />
        <script src="./js/tags/jquery.tagsinput.js"></script>


   
      <script>
          function TitleChar(val) {
              var len = val.value.length;
              if (len >= 70) {
                  val.value = val.value.substring(0, 70);
              } else {
                  $('#title').text(70 - len);
              }
          };

          function SumChar(val) {
              var len = val.value.length;
              if (len >= 200) {
                  val.value = val.value.substring(0, 200);
              } else {
                  $('#desc').text(200 - len);
              }
          };

          function DescChar(val) {
              var len = val.value.length;
              if (len >= 155) {
                  val.value = val.value.substring(0, 155);
              } else {
                  $('#desc').text(155 - len);
              }
          };

          $(function () {
              $('#content_tags').tagsInput({
                  width: 'auto',
              });
          });

         
          $("#ayarlar").addClass("active");



          $(function () {
              $('#content_Renk1ColorPicker').colorpicker({
                  parts: 'full',
                  alpha: true,
                  showOn: 'both',
                  buttonColorize: true,
                  showNoneButton: true
              });
          });

          $(function () {
              $('#content_Renk2ColorPicker').colorpicker({
                  parts: 'full',
                  alpha: true,
                  showOn: 'both',
                  buttonColorize: true,
                  showNoneButton: true
              });
          });

          //$(function () {
          //    $('#content_colorpicker').colorpicker();
          //});
           
    </script>

   

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" Runat="Server">

    
    <div class="row">

        <!--
      	<div class="span8" style="border:1px solid #ccebac; background-color:#e0f2cb; color:#6da827; font-weight:bold;line-height:40px; margin-bottom:10px; border-radius:5px;">
             <span class="span7 left"><img src="img/notifications/success-32.png" style="height:16px;" /> İşlem Başarı ile tamamlandı.</span>
             <asp:HyperLink ID="hplClose" runat="server" OnClick="hplClose_Click"><img src="img/notifications/success-32.png" /></asp:HyperLink>
      	</div>
        -->
      	<div class="span8">      		
      		
      		<div class="widget stacked ">
      			
      			<div class="widget-header">
      				<i class="icon-cog"></i>
      				<h3>Genel Ayarlar</h3>
  				</div> <!-- /widget-header -->

                  
				
				<div class="widget-content">

					<div class="tabbable">
					<ul class="nav nav-tabs">
					  
					  <li class="active"><a href="#yasal" data-toggle="tab">Yasal Bilgiler</a></li>  
                      <li class=""><a href="#settings" data-toggle="tab">Site Ayarları</a></li>
                      <li class=""><a href="#adres" data-toggle="tab">Adres Tanımları</a></li>
                      <li class=""><a href="#sosyal" data-toggle="tab">Sosyal Medya</a></li>
                      <li class=""><a href="#seo" data-toggle="tab">SEO</a></li>
					</ul>
					
					<br/>
					
						<div class="tab-content">
						
							
                            <div class="tab-pane active" id="yasal">
							<div id="edit_yasal" class="form-horizontal">
								 
									
									<div class="control-group">											
										<label class="control-label" for="unvan">Firma Ünvanı</label>
										<div class="controls">
                                            <asp:TextBox ID="txtcompanyshort" TextMode="SingleLine" CssClass="input-medium"  runat="server"></asp:TextBox>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                <div class="control-group">											
										<label class="control-label" for="unvanuzun">Firma Ünvanı Uzun</label>
										<div class="controls">
                                            <asp:TextBox ID="txtcompanylong" TextMode="SingleLine" CssClass="input-long"  runat="server"></asp:TextBox>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->


                                <div class="control-group">											
										<label class="control-label" for="vergidaire">Finansal Tablolar</label>
										<div class="controls">
									 <asp:FileUpload ID="fuFinansalTablolar" runat="server" />
                                           
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->
									
									
									<div class="control-group">											
										<label class="control-label" for="vergidaire">Vergi Dairesi</label>
										<div class="controls">
									 
                                            <asp:TextBox ID="txtVergiDairesi" TextMode="SingleLine" CssClass="input-medium" runat="server"></asp:TextBox>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->
									
									
									<div class="control-group">											
										<label class="control-label" for="vergino">Vergi No</label>
										<div class="controls">
											
                                            <asp:TextBox ID="txtVergino" CssClass="input-medium" runat="server"></asp:TextBox>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->
									
									
									<div class="control-group">											
										<label class="control-label" for="nace">Nace Kodu</label>
										<div class="controls">
											 
                                            <asp:TextBox ID="txtNace" TextMode="MultiLine" ToolTip="Nace kodunuzu yandaki linke tıklayarak sorgulayabilirsiniz." CssClass="input-large" runat="server"></asp:TextBox>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                <div class="control-group">											
										<label class="control-label" for="nace">Mersis No</label>
										<div class="controls">
											 
                                            <asp:TextBox ID="txtMersis" ToolTip="Mersis numaranızı yazın." CssClass="input-large" runat="server"></asp:TextBox>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                <div class="control-group">											
										<label class="control-label" for="ortak">Ortaklar</label>
										<div class="controls">
											 <ul class="ortaklar">
                                             <li><asp:TextBox ID="txtortak1" ToolTip="Ad ve Soyad şeklinde yazınız" CssClass="input-medium" runat="server"></asp:TextBox></li>
                                             <li><asp:TextBox ID="txtortak2" ToolTip="Ad ve Soyad şeklinde yazınız" CssClass="input-medium" runat="server"></asp:TextBox></li>
                                             <li><asp:TextBox ID="txtortak3" ToolTip="Ad ve Soyad şeklinde yazınız" CssClass="input-medium" runat="server"></asp:TextBox></li>
                                             <li><asp:TextBox ID="txtortak4" ToolTip="Ad ve Soyad şeklinde yazınız" CssClass="input-medium" runat="server"></asp:TextBox></li>
                                             <li><asp:TextBox ID="txtortak5" ToolTip="Ad ve Soyad şeklinde yazınız" CssClass="input-medium" runat="server"></asp:TextBox></li>
                                             </ul>
                                            <label class="control-label">Sermaye Tutarı </label>
                                             <ul class="sermaye">
                                             <li><asp:TextBox ID="txtSermaye1" ToolTip="Örn; 125.000" CssClass="input-medium" runat="server"></asp:TextBox></li>
                                             <li><asp:TextBox ID="txtSermaye2" ToolTip="Örn; 125.000" CssClass="input-medium" runat="server"></asp:TextBox></li>
                                             <li><asp:TextBox ID="txtSermaye3" ToolTip="Örn; 125.000" CssClass="input-medium" runat="server"></asp:TextBox></li>
                                             <li><asp:TextBox ID="txtSermaye4" ToolTip="Örn; 125.000" CssClass="input-medium" runat="server"></asp:TextBox></li>
                                             <li><asp:TextBox ID="txtSermaye5" ToolTip="Örn; 125.000" CssClass="input-medium" runat="server"></asp:TextBox></li>
                                             </ul>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->
									
									
								 
									
								<div class="form-actions">
                                        <asp:Button ID="btnKaydet1" runat="server" Text="Kaydet" CssClass="btn btn-primary" OnClick="btnKaydet_Click" />
                                        <asp:Button ID="btnKaydet2" runat="server" Text="İptal" CssClass="btn" OnClick="btnIptal_Click" />
									</div> <!-- /form-actions -->
								 
							</div>
							</div>
                             

							<div class="tab-pane" id="settings">
								<div id="edit-profile2" class="form-horizontal">	

                                    <div class="control-group">											
										<label class="control-label" for="vergidaire">Firma Logo</label>
										<div class="controls">
									 
                                            <asp:FileUpload ID="fuLogo" CssClass="input-xlarge" runat="server"></asp:FileUpload> <asp:Image runat="server" ID="imgLogo" Height="70" />
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                     <div class="control-group">											
										<label class="control-label" for="dropSehir">Tema</label>
										<div class="controls">
									 <asp:DropDownList ID="dropRenk" runat="server" CssClass="dropdown" Enabled="false">
                                         <asp:ListItem Value="dark_blue">Lacivert</asp:ListItem>
                                         <asp:ListItem Value="red">Bordo</asp:ListItem>
                                         <asp:ListItem Value="green">Yeşil</asp:ListItem>
                                         <asp:ListItem Value="orange">Turuncu</asp:ListItem>
                                         <asp:ListItem Value="gray">Gri</asp:ListItem>
                                         <asp:ListItem Value="SteelGray">Demir</asp:ListItem>
                                         <asp:ListItem Value="pink">Pembe</asp:ListItem>
                                         <asp:ListItem Value="purple">Mor</asp:ListItem>
                                         <asp:ListItem Value="white">Beyaz</asp:ListItem>
                                         <asp:ListItem Value="yellow">Sarı</asp:ListItem>
                                         <asp:ListItem Value="blue">Mavi</asp:ListItem>
									 </asp:DropDownList>
                                            
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    <div class="control-group">											
										<label class="control-label" for="dropSehir">Birincil Renk </label>
										<div class="controls">
									        <asp:TextBox ID="Renk1ColorPicker" runat="server"></asp:TextBox>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    <div class="control-group">											
										<label class="control-label" for="dropSehir">İkincil Renk </label>
										<div class="controls">
									        <asp:TextBox ID="Renk2ColorPicker" runat="server"></asp:TextBox>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    
                                    <div class="control-group">											
										<label class="control-label" for="vergidaire">ArkaPlan Resmi</label>
										<div class="controls">
                                           <span style="border:0px solid #f00; position:absolute; z-index:1000; right:80px; margin-top:20px;"><asp:ImageButton ID="btnBgTemizle" runat="server" OnClick="btnBgTemizle_Click" ImageUrl="~/images/icon_close.png" /></span>
                                            <asp:FileUpload ID="fuBg" CssClass="input-xlarge" runat="server"></asp:FileUpload> <asp:Image runat="server" ID="imgBg" style="width:480px; height:240px; margin-top:10px; border:1px solid #eaeaea; padding:4px;" />
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

									



										<br/>
										
											<div class="form-actions">
                                        <asp:Button ID="Button3" runat="server" Text="Kaydet" CssClass="btn btn-primary" OnClick="btnKaydet_Click" />
                                        <asp:Button ID="Button4" runat="server" Text="İptal" CssClass="btn" OnClick="btnIptal_Click" />
									</div> <!-- /form-actions -->
								 
								</div>
							</div>

                            <div class="tab-pane" id="adres">

                               <div id="edit-adress" class="form-horizontal">

                                   	<div class="control-group">											
										<label class="control-label" for="vergidaire">Açık Adres</label>
										<div class="controls">
                                            <asp:TextBox ID="txtAdres" TextMode="MultiLine" CssClass="input-xlarge" runat="server"></asp:TextBox>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->


                                    <div class="control-group">											
										<label class="control-label" for="dropSehir">Şehir</label>
										<div class="controls">
									 <asp:DropDownList ID="dropSehir" runat="server" CssClass="dropdown"></asp:DropDownList>
                                            
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    <div class="control-group">											
										<label class="control-label" for="txtTel">Tel Pbx</label>
										<div class="controls">
                                            <asp:TextBox ID="txtTelPbx" TextMode="SingleLine" CssClass="input-xlarge" runat="server"></asp:TextBox>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->


                                      	<div class="control-group">											
										<label class="control-label" for="txtTel">Tel</label>
										<div class="controls">
                                            <asp:TextBox ID="txtTel" TextMode="SingleLine" CssClass="input-xlarge" runat="server"></asp:TextBox>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                   
                                      	<div class="control-group">											
										<label class="control-label" for="txtFax">Fax</label>
										<div class="controls">
                                            <asp:TextBox ID="txtFax" TextMode="SingleLine" CssClass="input-xlarge" runat="server"></asp:TextBox>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                   
                                      	<div class="control-group">											
										<label class="control-label" for="txtMail">E-posta</label>
										<div class="controls">
                                            <asp:TextBox ID="txtMail" TextMode="SingleLine" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                            <p class="help-block">* Örn; info@firmaisminiz.com </p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                      	<div class="control-group">											
										<label class="control-label" for="txtMail">Çalışma Saatleri</label>
										<div class="controls">
                                            
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    	<div class="control-group">											
										<label class="control-label" for="txtMail">Haftaiçi</label>
										<div class="controls">
                                            <asp:TextBox ID="txtHaftaici" TextMode="SingleLine" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                            <p class="help-block">* Örn; 09:00 - 22:00 </p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->
                                    	<div class="control-group">											
										<label class="control-label" for="txtMail">Haftasonu</label>
										<div class="controls">
                                            <asp:TextBox ID="txtHaftasonu" TextMode="SingleLine" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                            <p class="help-block">* Örn; 09:00 - 22:00 </p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                <br/>
										
											<div class="form-actions">
                                        <asp:Button ID="Button1" runat="server" Text="Kaydet" CssClass="btn btn-primary" OnClick="btnKaydet_Click" />
                                        <asp:Button ID="Button2" runat="server" Text="İptal" CssClass="btn" OnClick="btnIptal_Click" />
									</div> <!-- /form-actions -->
                                
                               </div><!-- /tab-pane -->
                            </div>


                            <div class="tab-pane" id="sosyal">
                                <div id="edit-social" class="form-horizontal">
                                      	<div class="control-group">											
										<label class="control-label" for="txtFacebook">Facebook</label>
										<div class="controls">
                                            <asp:TextBox ID="txtFacebook" TextMode="SingleLine" CssClass="input-xlarge" runat="server"></asp:TextBox><br />
                                            <p class="help-block">* Örn; www.facebook.com/CplusMedia</p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                   
                                      	<div class="control-group">											
										<label class="control-label" for="txtTwitter">Twitter</label>
										<div class="controls">
                                            <asp:TextBox ID="txtTwitter" TextMode="SingleLine" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                            <p class="help-block">* Örn; www.twitter.com/CplusMedia</p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                   
                                      	<div class="control-group">											
										<label class="control-label" for="txtInstagram">Instagram</label>
										<div class="controls">
                                            <asp:TextBox ID="txtInstagram" TextMode="SingleLine" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                            <p class="help-block">* Örn; www.instagram.com/CplusMedia</p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                   <div class="control-group">											
										<label class="control-label" for="txtGoogle">GooglePlus</label>
										<div class="controls">
                                            <asp:TextBox ID="txtGoogle" TextMode="SingleLine" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                            <p class="help-block">* Örn; plus.google.com/1234567890</p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    <div class="control-group">											
										<label class="control-label" for="txtYoutube">Youtube</label>
										<div class="controls">
                                            <asp:TextBox ID="txtYoutube" TextMode="SingleLine" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                            <p class="help-block">* Örn; www.youtube.com/CplusMedia</p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    <div class="control-group">											
										<label class="control-label" for="txtPinterest">Pinterest</label>
										<div class="controls">
                                            <asp:TextBox ID="txtPinterest" TextMode="SingleLine" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                            <p class="help-block">* Örn; www.pinterest.com/CplusMedia</p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    <div class="control-group">											
										<label class="control-label" for="txtfoursquare">FourSquare</label>
										<div class="controls">
                                            <asp:TextBox ID="txtfoursquare" TextMode="SingleLine" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                            <p class="help-block">* Örn; www.foursquare.com/CplusMedia</p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    
                                <br/>
										
											<div class="form-actions">
                                        <asp:Button ID="Button5" runat="server" Text="Kaydet" CssClass="btn btn-primary" OnClick="btnKaydet_Click" />
                                        <asp:Button ID="Button6" runat="server" Text="İptal" CssClass="btn" OnClick="btnIptal_Click" />
									</div> <!-- /form-actions -->
                                </div>
                            </div>


                            <div class="tab-pane" id="seo">
                                <div id="edit-seo" class="form-horizontal">
                                    <div class="control-group">											
										<label class="control-label" for="vergidaire">Meta Title</label>
										<div class="controls">
									 
                                            <asp:TextBox ID="txtTitle" TextMode="SingleLine" CssClass="input-xlarge" onkeyup="TitleChar(this)" runat="server"></asp:TextBox><div class="char" id="title"></div>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    <div class="control-group">											
										<label class="control-label" for="vergidaire">Meta Description</label>
										<div class="controls">
									 
                                            <asp:TextBox ID="txtDescription" TextMode="MultiLine" CssClass="input-xlarge" onkeyup="DescChar(this)" runat="server"></asp:TextBox><div class="char" id="desc"></div>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    <div class="control-group">											
										<label class="control-label" for="vergidaire">Meta Keywords</label>
										<div class="controls">
                                            <asp:TextBox ID="tags"  CssClass="input-medium" runat="server"></asp:TextBox>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->
									
                                    
			                        <div class="control-group">											
										<label class="control-label" for="vergidaire">Analytics Kodu</label>
										<div class="controls">
                                            <asp:TextBox ID="txtAnalytics" TextMode="MultiLine"  CssClass="input-xlarge" runat="server"></asp:TextBox>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->
				

                                      <div class="control-group">											
										<label class="control-label" for="vergidaire">Google Harita Kodu</label>
										<div class="controls">
                                            <asp:TextBox ID="txtHarita" TextMode="MultiLine"  CssClass="input-xlarge" runat="server"></asp:TextBox>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->
                                    <div class="form-actions">
                                        <asp:Button ID="Button7" runat="server" Text="Kaydet" CssClass="btn btn-primary" OnClick="btnKaydet_Click" />
                                        <asp:Button ID="Button8" runat="server" Text="İptal" CssClass="btn" OnClick="btnIptal_Click" />
									</div> <!-- /form-actions -->
                                </div>
                                
                            </div>
							 
						</div>
					  
					  
					</div>
					
					
					
					
					
				</div> <!-- /widget-content -->
					
			</div> <!-- /widget -->
      		
	    </div> <!-- /span8 -->
      	
      	
      	<div class="span4">
			
			
			<div class="widget stacked widget-box">
				
				<div class="widget-header">
                    <i class="icon-info-sign"></i>
      				<h3>Genel Bilgi</h3>
  				</div> <!-- /widget-header -->
				
				<div class="widget-content">
					<p>Bu bölümden siteniz ve hesabınız ile ilgili genel ayarları yapabilirsiniz. Ticaret Kanununa göre sitenizde bulunması gereken bilgileri "YASAL" menü başlığı altından girmeniz gerekir.</p>
					<p>Ayrıca Google SEO açısından önemli olan sitenizin META taglarını, Google Analitics, Haritalar gibi izleme kodlarını da "Site Ayarları" menü başlığı altından ekleyebilirsiniz.</p>
				</div> <!-- /widget-content -->
				
			</div> <!-- /widget-box -->
								
	      </div> <!-- /span4 -->

        <div class="span4">
			
			
			<div class="widget stacked widget-box">
				
				<div class="widget-header">
                    <i class="icon-info-sign"></i>
      				<h3>Önemli Bilgi</h3>
  				</div> <!-- /widget-header -->
				
				<div class="widget-content">
					<p>Nace kodu, faaliyet konusu, Resmi Ortaklar ve Sermaye Oranlarını <a target="_blank" href="http://www.ito.org.tr/wps/portal/bilgi-bankasi/detay/?page=fb/sk/tug&prmPageId=BM1.1.3&initView=true"> bu adresten</a> sadece şirket ünvanınızın bir kısmını yazarak sorgulayabilirsiniz.</p>
				</div> <!-- /widget-content -->
				
			</div> <!-- /widget-box -->
								
	      </div>
      	
      </div>
</asp:Content>

