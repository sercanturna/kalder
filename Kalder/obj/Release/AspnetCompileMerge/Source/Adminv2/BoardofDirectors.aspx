﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="BoardofDirectors.aspx.cs" Inherits="Kalder.Adminv2.BoardofDirectors" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    
    <%@ Register Assembly="CollectionPager" Namespace="SiteUtils" TagPrefix="cc1" %>

    <script type="text/javascript">
        $(function () {
            var $allCheckbox = $('.allCheckbox :checkbox');
            var $checkboxes = $('.singleCheckbox :checkbox');
            $allCheckbox.change(function () {
                if ($allCheckbox.is(':checked')) {
                    $checkboxes.attr('checked', 'checked');
                }
                else {
                    $checkboxes.removeAttr('checked');
                }
            });
            $checkboxes.change(function () {
                if ($checkboxes.not(':checked').length) {
                    $allCheckbox.removeAttr('checked');
                }
                else {
                    $allCheckbox.attr('checked', 'checked');
                }
            });
        });
       // $("#urunler").addClass("active");
    </script>
    <style>
        th.allCheckbox {
            width: 20px;
        }

            td.singleCheckbox input, th.allCheckbox input {
                width: 20px !important;
            }
    </style>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <div class="row">

        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-user"></i>
                    <h3>Yönetim Kurulu Üyeleri</h3>
                    <asp:LinkButton ID="btnDeleteAll" OnClick="btnDeleteAll_Onclick" runat="server" CssClass="btn right" BorderStyle="Solid" BorderColor="#606060" BorderWidth="1" ToolTip="Sil" CausesValidation="False" Text="x Sil" OnClientClick="return confirm('Eğitmeni silmek istediğinize emin misiniz?');"></asp:LinkButton>
                    <a href="add_yk.aspx" class="btn btn-success btn-primary right"> + Ekle </a>
                    
                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                    <div class="EU_TableScroll">
                        <asp:UpdatePanel ID="upGrid" runat="server">
                            <ContentTemplate>

                                <table id="ProductCat" class="table table-bordered table-striped blank"  border="1" style="border-collapse: collapse;">
                                   <asp:Repeater ID="rpt_Products" runat="server" OnItemDataBound="rpt_Products_OnItemDataBound">
                                       <HeaderTemplate>
                                     <tr >
                                        <th scope="col" class="allCheckbox"><asp:CheckBox ID="allCheckbox1"  runat="server" /></th>
                                        <th scope="col">ID</th>
                                        <th scope="col"></th>
                                        <th scope="col">Adı Soyadı</th>
                                         <th scope="col">Ünvanı</th>
                                        
                                         <th scope="col">Sıra No</th>
                                        <th scope="col">Durum</th>
                                        <th scope="col">İşlem</th>
                                    </tr>
                                    </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="singleCheckbox">
                                                    <asp:CheckBox ID="chkContainer" runat="server"  />
                                                    <asp:HiddenField ID="hbItem" runat="server" Value='<%# Eval("YkId") %>' />
                                                </td>
                                                <td><%#Eval ("YkId") %></td> 
                                                <td><img src="../upload/YonetimKurulu/<%#Eval ("Image") %>" style="height:50px" /></td>
                                                <td><%#Eval ("Name") %></td>
                                                <td><%# Eval("Title") %></td>
                                               
                                                <td><%# Eval("OrderNo") %></td>
                                                <td class="center"><%# Eval("IsActive") == "True" ? Eval("IsActive") : Eval("IsActive").ToString().Replace("True", "<img src=\"img/play.png\" />").Replace("False", "<img src=\"img/pause.png\" />")%></td>
                                                <td><a href="edit_yk_<%#Eval("YkId") %>" class="btn btn-small"><i class="btn-icon-only icon-pencil"></i></a></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                  
                                    <!--
                                    <tr>
                                        <td colspan="9" style="text-align:center; padding:20px;" ><asp:Label CssClass="alert" ID="ltrlSonuc" runat="server" Text="Henüz bir ürün kategorisi oluşturulmamış."></asp:Label></td>
                                    </tr>
                                    -->
                                     
                                </table>
                              <section id="paginations">
                                <div class="sayfalama">
                                  <cc1:CollectionPager ID="CollectionPager1" runat="server" BackText=" « Önceki" 
                                    FirstText="İlk" LabelText="" LastText="Son" NextText="Sonraki »" 
                                    PageNumbersDisplay="Numbers" ResultsFormat="Sayfalar {0} {1} (Toplam:{2})"
                                    PageSize="50" SectionPadding="50" BackNextDisplay="Buttons" BackNextLocation="Split" 
                                    MaxPages="500" PageNumbersSeparator=""></cc1:CollectionPager>
                         </div>
                                  </section>
                                        </ContentTemplate>
                        </asp:UpdatePanel>

                        
                        
                    </div>
                </div>
            </div>
        </div>


    </div>


</asp:Content>
