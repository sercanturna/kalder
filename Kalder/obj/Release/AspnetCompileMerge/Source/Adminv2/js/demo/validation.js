function pageLoad() {
    var rules = {
        rules: {
            ctl00$content$txtusername: {
                minlength: 2,
                required: true
            },
            ctl00$content$txtName: {
                minlength: 2,
                required: true
            },

            ctl00$content$txtpassword1: {
                required: false,
                minlength: 5
            },
            ctl00$content$txtpassword2: {
                required: false,
                minlength: 5,
                equalTo: "#content_txtpassword1"
            },

            ctl00$content$txtemail: {
                required: true,
                email: true
            },



            ctl00$content$txtKategoriAdi: {
                minlength: 2,
                required: true
            },

            ctl00$content$drpDurum :{
                required: true
            },
            

            subject: {
                minlength: 2,
                required: true
            },
            message: {
                minlength: 2,
                required: true
            },
            validateSelect: {
                required: true
            },
            validateCheckbox: {
                required: true,
                minlength: 2
            },
            validateRadio: {
                required: true
            }
        }
    };


    var validationObj = $.extend(rules, Application.validationRules);

    var s = $('#form1').validate(validationObj);

    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_initializeRequest(onEachRequest);

}
function onEachRequest(sender, args) {
    if ($("#form1").valid() == false) {
        args.set_cancel(true);
    }
}