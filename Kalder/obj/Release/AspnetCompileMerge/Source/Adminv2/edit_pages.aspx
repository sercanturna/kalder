﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="edit_pages.aspx.cs" Inherits="Kalder.Adminv2.edit_pages" %>
 
<%--<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit.HTMLEditor" tagprefix="cc1" %>--%>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="CKFinder" Namespace="CKFinder" TagPrefix="CKFinder" %>


<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
      
    <script src="js/multi.js" type="text/javascript"></script>
     <link href="./js/tags/jquery.tagsinput.css" rel="stylesheet" />
    <script src="./js/tags/jquery.tagsinput.js"></script>

  

    
    <script src="js/validation/jquery.validationEngine-tr.js"></script>
    <script src="js/validation/jquery.validationEngine.js"></script>
   <link href="js/validation/validationEngine.jquery.css" rel="stylesheet" />  
   
      <script>
          $(document).ready(function () {

              $("#sayfalar").addClass("active");

              $("#form1").validationEngine('attach', { promptPosition: "topRight", scroll: true });
          });
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 183px;
            font-family:Verdana;
            font-size:13px;
            font-weight:bold;
            color:#646464;
        }
          ul.resimler
        {
          
        }

            ul.resimler li
            {
                height:280px;
            }
    </style> 

    
    <script src="../ckeditor/ckeditor.js" type="text/javascript"></script>
 
    <script src="../ckfinder/ckfinder.js" type="text/javascript"></script>
    <script type="text/javascript">
      //  window.onload = function () {
            var editor = CKEDITOR.replace('<% = txtDetay.ClientID %>');
        CKFinder.setupCKEditor(editor, '../ckfinder');

        var content_editor = editor;

        content_editor.on('key', function () {
            change_page = true;
        });
      //  };

        function DescChar(val) {
            var len = val.value.length;
            if (len >= 201) {
                val.value = val.value.substring(0, 200);
            } else {
                $('#desc').text(200 - len + " Karakter");
            }
        };

        function TitleChar(val) {
            var len = val.value.length;
            if (len >= 70) {
                val.value = val.value.substring(0, 70);
            } else {
                $('#title').text(70 - len + " Karakter");
            }
        };
        
     
      

        $(function () {
            $('#content_tags').tagsInput({
                width: 'auto',
            });
        });

</script>
     
       
 
 

</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
             </ContentTemplate>
    </asp:UpdatePanel>
    <div class="row">
        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-globe"></i>
                    <h3>Sayfa Düzenle</h3>
                </div>
                <!-- /widget-header -->


                <div class="widget-content">
                    <div class="tabbable">
					<ul class="nav nav-tabs">
					  <li class="active"><a href="#genel" data-toggle="tab">Genel Bilgiler</a></li>
                      <li><a href="#galeri" data-toggle="tab">Foto Galeri</a></li>  
                      <li class=""><a href="#veriler" data-toggle="tab">SEO</a></li>
                     
					</ul>
					
					<br/>
					
						<div class="tab-content">
						
							
                            <div class="tab-pane active" id="genel">
                    <div id="edit-genel" class="form-horizontal">

                                       <div class="control-group">
                            <label class="control-label" for="dropKat">Ana Sayfa</label>
                            <div class="controls">
                                <asp:DropDownList ID="dropKat" runat="server" AutoPostBack="true"  OnSelectedIndexChanged="dropKat_SelectedIndexChanged" ></asp:DropDownList>
                                <p class="help-block">* Ekleyeceğiniz sayfa bir alt sayfa ise lütfen ana sayfayı seçiniz.</p>
                            </div>
                            <!-- /controls -->
                        </div>
                        <!-- /control-group -->
                        

                        <div class="control-group">
                            <label class="control-label" for="txtKategoriAdi">Sayfa Adı</label>
                            <div class="controls">
                                <asp:TextBox ID="txtKategoriAdi" runat="server" Width="300px" CssClass="validate[required]"></asp:TextBox>
                            </div>
        
                            <!-- /controls -->
                        </div>
                        <!-- /control-group -->

                     


                     

                        <div class="control-group">
                            <label class="control-label" for="txtDetay">İçerik</label>
                            <div class="controls">
                                <CKEditor:CKEditorControl ID="txtDetay" runat="server" >
                                </CKEditor:CKEditorControl>
                                <p class="help-block">* Sayfa içeriğinin tamamını bu alanda yapılandırın.</p>
                            </div>
        
                            <!-- /controls -->
                        </div>
                        <!-- /control-group -->

                                              <div class="control-group">
                            <label class="control-label" for="fuResim">Sayfa Resmi</label>
                            <div class="controls">
                                <asp:Image ID="imgResim" runat="server" />
                                <span style="border:0px solid #f00; position:absolute; z-index:1000; right:80px; margin-top:20px;">Resmi Sil <asp:ImageButton ID="btnResimTemizle" runat="server" OnClick="btnResimTemizle_Click" ImageUrl="~/images/icon_close.png" /></span>
                                <asp:FileUpload ID="fuResim" runat="server" />
                                <p class="help-block">* Burada seçilecek resmin 4x3 formatında olması gerekli.</p>
                            </div>
        
                            <!-- /controls -->
                        </div>
                        <!-- /control-group -->

                               

                                <div class="control-group">
                                    <label class="control-label" for="chkOnay">Durum</label>
                                    <div class="controls">
                                         <asp:DropDownList ID="drpDurum" runat="server">
                                             <asp:ListItem Value="NULL">-Seçiniz-</asp:ListItem>
                                             <asp:ListItem Value="1">Aktif</asp:ListItem>
                                             <asp:ListItem Value="0">Pasif</asp:ListItem>
                                         </asp:DropDownList>
                                        <p class="help-block">* Durumu Pasif yaparsanız haber sitede gözükmez.</p>
                                    </div>
        
                                    <!-- /controls -->
                                </div>
                                <!-- /control-group -->
 

                                <div class="control-group">
                                    <label class="control-label" for="txtSiraNo">Sıra No</label>
                                    <div class="controls">
                                    <asp:TextBox ID="txtSiraNo" runat="server"></asp:TextBox>
                                        <p class="help-block">* Haberin sitedeki sırasını belirtin. Eğer istemiyorsanız boş bırakın</p>
                                    </div>
        
                                    <!-- /controls -->
                                </div>
                                <!-- /control-group -->

                              <%--Sütunları devre dışı bıraktım. 
                                   <div class="control-group">
                                    <label class="control-label" for="txtSutun">Sütunlar</label>
                                    <div class="controls">
                                    <asp:TextBox ID="txtSutun" runat="server"></asp:TextBox>
                                        <p class="help-block">* 3 Alt Sayfa için kullanılacak sütun sayısı. Sadece ana sayfalarda geçerlidir.</p>
                                    </div>
        
                                    <!-- /controls -->
                                </div>
                                <!-- /control-group -->--%>

                              <div class="control-group">
                                    <label class="control-label" for="chkManset">Menüde Göster</label>
                                    <div class="controls">
                                    <asp:CheckBox ID="chkMenudeGoster" runat="server"></asp:CheckBox>
                                        <p class="help-block">* Sayfayı üst Menude gösterir ancak sadece Ana Sayfalar için geçerlidir.</p>
                                    </div>
        
                                    <!-- /controls -->
                                </div>
                                <!-- /control-group -->

                        		 
                                </div>
                             </div>


                            <div class="tab-pane" id="galeri">
                                <div id="Div2" class="form-horizontal">	
                                <div class="control-group">
                                    <label class="control-label" for="chkOnay">Foto Galeri</label>
                                    <div class="controls">
                                         <asp:DropDownList ID="drpGaleri" runat="server"></asp:DropDownList>
                                        <p class="help-block">* Sayfaya ait fotoğraf albümü seçin. Yeni bir tane oluşturmak için lütfen <a href="gallery_categories.aspx" target="_blank"> Albüm Yönetimi </a>sayfasına gidin.</p>
                                    </div>
        
                                    <!-- /controls -->
                                </div>
                                <!-- /control-group -->
                            </div>
                            </div>

							<div class="tab-pane" id="veriler">
                                <div id="edit-profile2" class="form-horizontal">	
                                  
                                      <div class="control-group">											
										<label class="control-label" for="vergidaire">Seo URL</label>
										<div class="controls">
                                            <asp:TextBox ID="txtSeoUrl" TextMode="SingleLine" CssClass="validate[required] input-xlarge" runat="server" ></asp:TextBox><div class="char" id="Div1"></div>
										 <p class="help-block">* Google ve diğer arama motorları için adres çubuğunda gözükecek kategoriniz için bir URL belirleyin.</p>
                                        </div> <!-- /controls -->				
									</div> <!-- /control-group -->


                                       <div class="control-group">											
										<label class="control-label" for="vergidaire">Meta Title</label>
										<div class="controls">
									 
                                            <asp:TextBox ID="txtTitle" TextMode="SingleLine" CssClass="input-xlarge" onkeyup="TitleChar(this)" runat="server"></asp:TextBox><div class="char" id="title"></div>
										 <p class="help-block">* Google ve diğer arama motorları için bir başlık yazın.</p>
                                        </div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    <div class="control-group">											
										<label class="control-label" for="vergidaire">Meta Description</label>
										<div class="controls">
                                            <asp:TextBox ID="txtDescription" TextMode="MultiLine" CssClass="input-xlarge" onkeyup="DescChar(this)" runat="server"></asp:TextBox><div class="char" id="desc"></div>
                                            <p class="help-block">* Google ve diğer arama motorları için bir açıklama yazın.</p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    <div class="control-group">											
										<label class="control-label" for="vergidaire">Meta Keywords</label>
										<div class="controls">                                            
                                            <asp:TextBox ID="tags"  CssClass="input-medium" runat="server"></asp:TextBox>
                                            <p class="help-block">* Google ve diğer arama motorları için anahtar kelimeler yazın. <br />Kelimeler arasına virgül koyarak yada Enter tuşuna basarak geçiş yapabilirsiniz.</p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->


                                     <div class="control-group">											
										<label class="control-label" for="vergidaire">Okunma Sayısı</label>
										<div class="controls">                                            
                                            <asp:TextBox ID="txtHit"  CssClass="input-medium" runat="server"></asp:TextBox>
                                            <p class="help-block">* Sayfanın sitenizde kaç kere okunduğunu gösteren değerdir.</p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->


                                    </div>
							 </div>
						</div>
    <div class="form-actions">
           <asp:Button ID="btn_SayfaGuncelle" runat="server" CssClass="btn btn-primary" onclick="btn_SayfaGuncelle_Click" Text="Güncelle" />
            <asp:Button ID="btn_Temizle" runat="server" CssClass="btn"  Text="İptal" />
        </div>
    <!-- /form-actions -->
   
                        </div>
                </div>
            </div>
         </div>
    </div>
           

     <script src="./js/demo/validation.js"></script>
</asp:Content>