﻿<%@ WebHandler Language="C#" Class="FileUpHandler" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.IO;
using Microsoft.SqlServer;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.Web.Routing;


    public class FileUpHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            CportalV2.DbConnection db = new CportalV2.DbConnection();

            string pId = context.Request.QueryString["PageId"];

            try
            {

                context.Response.Write(pId);
            }
            catch (Exception ex)
            {
                context.Response.Write(ex.Message);
            }

          //  DbConnection db = new DbConnection();

            string sayfa = HttpContext.Current.Request.Url.AbsolutePath;

            // context.Response.ContentType = "text/plain";
            // context.Response.Write(sayfa + " d" + pId);

            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;




                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];



                    string fname = context.Server.MapPath("~/upload/urunler/" + file.FileName);
                    file.SaveAs(fname);

                    //  ****************** Veri tabanına kaydet ***************  //

                   SqlConnection resimbaglanti = db.baglan();
                    SqlCommand resimcmd = new SqlCommand("SET language turkish; Insert into ProductImages(" +
                    "ImageURL," +
                    "ProductId) values (" +
                    "@ImageURL," +
                    "@ProductId)", resimbaglanti);

                    resimcmd.Parameters.AddWithValue("ImageURL", file.FileName);
                    resimcmd.Parameters.AddWithValue("ProductId", pId);

                    int resimsonuc = resimcmd.ExecuteNonQuery();

                }
               
                //context.Response.ContentType = "text/javascript";
                //string script = String.Format("Javascript:SuccessWithMsg('Tebrikler');");
                //context.Response.Write(script);
            }
        }
 

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
