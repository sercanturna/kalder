﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="add_product_categories.aspx.cs" Inherits="Kalder.Adminv2.add_product_categories" %>

<%@ Register Assembly="CKFinder" Namespace="CKFinder" TagPrefix="CKFinder" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">

    <script src="js/multi.js" type="text/javascript"></script>
     <link href="./js/tags/jquery.tagsinput.css" rel="stylesheet" />
    <script src="./js/tags/jquery.tagsinput.js"></script>


   
 <script type="text/javascript">
     $("#urunler").addClass("active");
</script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 183px;
            font-family:Verdana;
            font-size:13px;
            font-weight:bold;
            color:#646464;
        }
          ul.resimler
        {
          
        }

            ul.resimler li
            {
                height:280px;
            }
    </style> 

    
    <script src="../ckeditor/ckeditor.js" type="text/javascript"></script>
 
    <script src="../ckfinder/ckfinder.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.onload = function () {
            var editor = CKEDITOR.replace('<% = txtDetay.ClientID %>');
            CKFinder.setupCKEditor(editor, '../ckfinder');
        };

        function DescChar(val) {
            var len = val.value.length;
            if (len >= 201) {
                val.value = val.value.substring(0, 200);
            } else {
                $('#desc').text(200 - len + " Karakter");
            }
        };

        function TitleChar(val) {
            var len = val.value.length;
            if (len >= 70) {
                val.value = val.value.substring(0, 70);
            } else {
                $('#title').text(70 - len + " Karakter");
            }
        };


      

        $(function () {
            $('#content_tags').tagsInput({
                width: 'auto',
            });
        });

</script>
     
       
 
 

</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>
             </ContentTemplate>
    </asp:UpdatePanel>
    <div class="row">
        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-globe"></i>
                    <h3>Ürün Kategorisi Ekle</h3>
                </div>
                <!-- /widget-header -->


                <div class="widget-content">
                    <div class="tabbable">
					<ul class="nav nav-tabs">
					  <li class="active"><a href="#genel" data-toggle="tab">Genel Bilgiler</a></li>  
                      <li class=""><a href="#veriler" data-toggle="tab">SEO</a></li>
                     
					</ul>
					
					<br/>
					
						<div class="tab-content">
						
							
                            <div class="tab-pane active" id="genel">
                    <div id="edit-genel" class="form-horizontal">

                                       <div class="control-group">
                            <label class="control-label" for="dropKat">Ana Kategori</label>
                            <div class="controls">
                                <asp:DropDownList ID="dropKat" runat="server" AutoPostBack="true"  OnSelectedIndexChanged="dropKat_SelectedIndexChanged" ></asp:DropDownList>
                                <p class="help-block">* Ekleyeceğiniz kategori bir alt kategori ise lütfen ana kategoriyi seçin.</p>
                            </div>
                            <!-- /controls -->
                        </div>
                        <!-- /control-group -->
                        

                        <div class="control-group">
                            <label class="control-label" for="txtKategoriAdi">Kategori Adı</label>
                            <div class="controls">
                                <asp:TextBox ID="txtKategoriAdi" runat="server" Width="300px"></asp:TextBox>
                            </div>
        
                            <!-- /controls -->
                        </div>
                        <!-- /control-group -->

                     


                     

                        <div class="control-group">
                            <label class="control-label" for="txtDetay">Açıklama</label>
                            <div class="controls">
                                <CKEditor:CKEditorControl ID="txtDetay" runat="server"></CKEditor:CKEditorControl>
                                <%--<CKEditor:CKEditorControl ID="txtDetay" runat="server" Width="630px" Height="300px">
                                </CKEditor:CKEditorControl>--%>
                                <p class="help-block">* Haber içeriğinin tamamını bu alanda yapılandırın.</p>
                            </div>
        
                            <!-- /controls -->
                        </div>
                        <!-- /control-group -->

                        <div class="control-group">
                            <label class="control-label" for="fuResim">Kategori Resmi</label>
                            <div class="controls">
                                <asp:FileUpload ID="fuResim" runat="server" />
                                <p class="help-block">* Burada seçilecek resmin 4x3 formatında olması gerekli.</p>
                            </div>
        
                            <!-- /controls -->
                        </div>
                        <!-- /control-group -->

                               

                                <div class="control-group">
                                    <label class="control-label" for="chkOnay">Durum</label>
                                    <div class="controls">
                                         <asp:DropDownList ID="drpDurum" runat="server">
                                             <asp:ListItem Value="NULL">-Seçiniz-</asp:ListItem>
                                             <asp:ListItem Value="1">Aktif</asp:ListItem>
                                             <asp:ListItem Value="0">Pasif</asp:ListItem>
                                         </asp:DropDownList>
                                        <p class="help-block">* Durumu Pasif yaparsanız haber sitede gözükmez.</p>
                                    </div>
        
                                    <!-- /controls -->
                                </div>
                                <!-- /control-group -->
 

                                <div class="control-group">
                                    <label class="control-label" for="txtSiraNo">Sıra No</label>
                                    <div class="controls">
                                    <asp:TextBox ID="txtSiraNo" runat="server"></asp:TextBox>
                                        <p class="help-block">* Kategorinin sırasını belirtin. Otomatik olmasını istiyorsanız boş bırakın</p>
                                    </div>
        
                                    <!-- /controls -->
                                </div>
                                <!-- /control-group -->

                                <div class="control-group">
                                    <label class="control-label" for="txtSutun">Sütunlar</label>
                                    <div class="controls">
                                    <asp:TextBox ID="txtSutun" runat="server"></asp:TextBox>
                                        <p class="help-block">* 3 Alt kategori için kullanılacak sütun sayısı. Sadece ana kategoride geçerlidir.</p>
                                    </div>
        
                                    <!-- /controls -->
                                </div>
                                <!-- /control-group -->

                              <div class="control-group">
                                    <label class="control-label" for="chkManset">Menüde Göster</label>
                                    <div class="controls">
                                    <asp:CheckBox ID="chkMenudeGoster" runat="server"></asp:CheckBox>
                                        <p class="help-block">* Kategoriyi üst Menude gösterir ancak sadece Ana Kategoriler için geçerlidir.</p>
                                    </div>
        
                                    <!-- /controls -->
                                </div>
                                <!-- /control-group -->

                        		 
                                </div>
                             </div>

							<div class="tab-pane" id="veriler">
                                <div id="edit-profile2" class="form-horizontal">	
                                  
                                      <div class="control-group">											
										<label class="control-label" for="vergidaire">Seo URL</label>
										<div class="controls">
                                            <asp:TextBox ID="txtSeoUrl" TextMode="SingleLine" CssClass="input-xlarge" onkeyup="TitleChar(this)" runat="server"></asp:TextBox><div class="char" id="Div1"></div>
										 <p class="help-block">* Google ve diğer arama motorları için adres çubuğunda gözükecek kategoriniz için bir URL belirleyin.</p>
                                        </div> <!-- /controls -->				
									</div> <!-- /control-group -->


                                       <div class="control-group">											
										<label class="control-label" for="vergidaire">Meta Title</label>
										<div class="controls">
									 
                                            <asp:TextBox ID="txtTitle" TextMode="SingleLine" CssClass="input-xlarge" onkeyup="TitleChar(this)" runat="server"></asp:TextBox><div class="char" id="title"></div>
										 <p class="help-block">* Google ve diğer arama motorları için bir başlık yazın.</p>
                                        </div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    <div class="control-group">											
										<label class="control-label" for="vergidaire">Meta Description</label>
										<div class="controls">
                                            <asp:TextBox ID="txtDescription" TextMode="MultiLine" CssClass="input-xlarge" onkeyup="DescChar(this)" runat="server"></asp:TextBox><div class="char" id="desc"></div>
                                            <p class="help-block">* Google ve diğer arama motorları için bir açıklama yazın.</p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    <div class="control-group">											
										<label class="control-label" for="vergidaire">Meta Keywords</label>
										<div class="controls">                                            
                                            <asp:TextBox ID="tags"  CssClass="input-medium" runat="server"></asp:TextBox>
                                            <p class="help-block">* Google ve diğer arama motorları için anahtar kelimeler yazın. <br />Kelimeler arasına virgül koyarak yada Enter tuşuna basarak geçiş yapabilirsiniz.</p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->


                                    </div>
							 </div>
						</div>
    <div class="form-actions">
           <asp:Button ID="btn_HaberEkle" runat="server" CssClass="btn btn-primary" onclick="btn_HaberEkle_Click" Text="Kategori Ekle" />
            <asp:Button ID="btn_Temizle" runat="server" CssClass="btn"  Text="İptal" />
        </div>
    <!-- /form-actions -->
   
                        </div>
                </div>
            </div>
         </div>
    </div>
           

     <script src="./js/demo/validation.js"></script>
</asp:Content>