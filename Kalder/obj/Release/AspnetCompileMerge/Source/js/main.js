/*-----------------------------------------------------------------------------------*/
/* 		Mian Js Start 
/*-----------------------------------------------------------------------------------*/
$(document).ready(function($) {
	
	/*$(".nav-tabs li a").click(function(){
		$(".nav-tabs li a img").attr("data-id")
        $(this).find("img").attr("src","olay.png");
		var sonuc =  $(this).attr("data-id");
		alert(sonuc)
		});
	*/
	
"use strict"
/*-----------------------------------------------------------------------------------*/
/* 	FULL SCREEN
/*-----------------------------------------------------------------------------------*/
$('.full-screen').superslides({
});
/*-----------------------------------------------------------------------------------*/
/* 	TESTIMONIAL SLIDER
/*-----------------------------------------------------------------------------------*/
$(".texti-slide").owlCarousel({ 
    items : 1,
	autoplay:true,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	singleItem	: true,
    navigation : true,
	navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
	pagination : true,
	animateOut: 'fadeOut'	
});
/*-----------------------------------------------------------------------------------*/
/*    TESTI SLIDER 2
/*-----------------------------------------------------------------------------------*/
$('.testi-slide').owlCarousel({
    loop:true,
	autoPlay:6000, //Set AutoPlay to 6 seconds 
    items:1,
    margin:10,	
	navText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
    responsiveClass:true,
	nav:true,
	animateOut: 'fadeOut'
});
/*-----------------------------------------------------------------------------------*/
/* 	FLEX SLIDER
/*-----------------------------------------------------------------------------------*/
$('.flex-banner').flexslider({
    animation: "slide",
	directionNav:true,
	controlNav:true,
	slideshow: true,                //Boolean: Animate slider automatically
    slideshowSpeed: 6000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
    animationSpeed: 500,            //Integer: Set the speed of animations, in milliseconds
	autoPlay : true,
    pauseOnHover: true
});
/*-----------------------------------------------------------------------------------*/
/* 	FEATURED SLIDER 
/*-----------------------------------------------------------------------------------*/
$('.founder-slide').owlCarousel({
    margin:30,
    nav:true,
	loop: true,
	autoplay:true,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	singleItem	: true,
	navText: ["<i class='ion-ios-arrow-thin-left'></i>","<i class='ion-ios-arrow-thin-right'></i>"],
	pagination : true,
    responsive:{
        300:{
            items:1
        },		
		600:{
            items:2
        }}
});
/*-----------------------------------------------------------------------------------*/
/* 	FEATURED SLIDER 
/*-----------------------------------------------------------------------------------*/

function showSlider() {
    $(".doct-list-style").css('visibility','visible');
	$(".kalDer-magazine").css('visibility','visible');
}


$('.doct-list-style').owlCarousel({
    margin:10,
    nav:true,
	loop: true,
	autoplay:true,
	dots:false,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	singleItem	: false,
	navText: ["<i class='ion-chevron-left'></i>","<i class='ion-chevron-right'></i>"],
	pagination : false,
	onInitialized: showSlider,
    responsive:{
        300:{
            items:1,
			dots:false,
			nav:true

        },		
		600:{
            items:2
        },
		800:{
            items:2
        },
		1000:{
            items:3
        },
		1200:{
            items:4
        }}
});

$('.kalDer-magazine').owlCarousel({
    margin:10,
    nav:true,
	loop: true,
	autoplay:true,
	dots:false,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	singleItem	: false,
	navText: ["<i class='ion-chevron-left'></i>","<i class='ion-chevron-right'></i>"],
	pagination : true,
	onInitialized: showSlider,
    responsive:{
        300:{
            items:2,
			dots:false,
			nav:true

        },		
		600:{
            items:2
        },
		800:{
            items:2
        },
		1000:{
            items:3
        },
		1200:{
            items:5
        }}
});


/*
setTimeout(function() {
    $(".doct-list-style").css('visibility','visible');
}, 3000);
*/
/*-----------------------------------------------------------------------------------*/
/* 	FOOTER REFERENCE SLIDER 
/*-----------------------------------------------------------------------------------*/
$('.brand-list-style').owlCarousel({
    margin:40,
    nav:false,
	loop: true,
	autoplay:true,
	dots:false,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	singleItem	: true,
	navText: ["<i class='ion-chevron-left'></i>","<i class='ion-chevron-right'></i>"],
	pagination : false,
    responsive:{
        300:{
            items:1
        },		
		600:{
            items:3
        },
		800:{
            items:3
        },
		1000:{
            items:4
        },
		1200:{
            items:4
        }}
});



/*-----------------------------------------------------------------------------------*/
/* 	SERVICES SLIDER 
/*-----------------------------------------------------------------------------------*/
$('.services-slide').owlCarousel({
    margin:30,
    nav:true,
	loop: true,
	autoplay:true,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	singleItem	: true,
	navText: ["<i class='ion-ios-arrow-thin-left'></i>","<i class='ion-ios-arrow-thin-right'></i>"],
	pagination : true,
    responsive:{
        300:{
            items:1
        },	
		600:{
            items:3
        }}
});
/*-----------------------------------------------------------------------------------*/
/* 	REFERENCES SLIDER 
/*-----------------------------------------------------------------------------------*/
$('.reference-list-style').owlCarousel({
    margin:30,
    nav:false,
	loop: false,
	autoplay:false,
	dots:false,
	//autoplayTimeout:5000,
	//autoplayHoverPause:true,
	singleItem	: true,
	//navText: ["<i class='ion-chevron-left'></i>","<i class='ion-chevron-right'></i>"],
	//pagination : false,
    responsive:{
        300:{
            items:1
        },		
		600:{
            items:2
        },
		800:{
            items:3
        },
		1000:{
            items:4
        },
		1200:{
            items:4
        }}
});

/*-----------------------------------------------------------------------------------*/
/* 	CAMPAIGN SLIDER 
/*-----------------------------------------------------------------------------------*/
$('.campaign-list-style').owlCarousel({
    margin:0,
    nav:true,
	loop: true,
	autoplay:true,
	animateOut: 'slideOutDown',
    animateIn: 'flipInX',
	dots:false,
	autoplayTimeout:5000,
	autoplayHoverPause:true,
	singleItem	: true,
	navText: ["<i class='ion-chevron-left'></i>","<i class='ion-chevron-right'></i>"],
	pagination : false,
    responsive:{
        300:{
			items:1,
			nav:false,
			dots:true
        },		
		600:{
            items:1
        },
		800:{
            items:1
        },
		1000:{
            items:1
        },
		1200:{
            items:1
        }}
});

/*-----------------------------------------------------------------------------------*/
/* 	INNER PAGE SLIDER 
/*-----------------------------------------------------------------------------------*/
/*
$("#slide-cont").owlCarousel({
    items: 1,
});

        // 1) ASSIGN EACH 'DOT' A NUMBER
			var dotcount = 1;
	 
			$('.owl-dot').each(function() {
			  $( this ).addClass( 'dotnumber' + dotcount);
			  $( this ).attr('data-info', dotcount);
			  dotcount=dotcount+1;
			});
			
			 // 2) ASSIGN EACH 'SLIDE' A NUMBER
			var slidecount = 1;
	 
			$('.owl-item').not('.cloned').each(function() {
			  $( this ).addClass( 'slidenumber' + slidecount);
			  slidecount=slidecount+1;
			});
			
			// SYNC THE SLIDE NUMBER IMG TO ITS DOT COUNTERPART (E.G SLIDE 1 IMG TO DOT 1 BACKGROUND-IMAGE)
			$('.owl-dot').each(function() {
			
          var grab = $(this).data('info');

          var slidegrab = $('.slidenumber'+ grab +' img').attr('src');
          console.log(slidegrab);

          $(this).css("background-image", "url("+slidegrab+")");  

      });
			
			// THIS FINAL BIT CAN BE REMOVED AND OVERRIDEN WITH YOUR OWN CSS OR FUNCTION, I JUST HAVE IT
        // TO MAKE IT ALL NEAT 
			var amount = $('.owl-dot').length;
			var gotowidth = 100/amount;
			
			$('.owl-dot').css("width", gotowidth+"%");
			var newwidth = $('.owl-dot').width();
			$('.owl-dot').css("height", newwidth+"px");

*/


/*-----------------------------------------------------------------------------------*/
/* 		Active Menu Item on Page Scroll
/*-----------------------------------------------------------------------------------*/
$('.scroll a').click(function() {  
	$('html, body').animate({scrollTop: $(this.hash).offset().top -80}, 1500);
return false;
});

/*-----------------------------------------------------------------------------------*/
/* 		Why-Choose Scroll for Mobile
/*-----------------------------------------------------------------------------------*/
if($(window).width() <= 440){
	$('#why-choose ul.nav li a').click(function() {  
		$('html, body').animate({scrollTop: $("#why-choose").offset().top +215}, 800);
		return true;
	});
}

/*-----------------------------------------------------------------------------------*/
/* 		Teklif Formu Show/Hide for Mobile
/*-----------------------------------------------------------------------------------*/
$('.menu-toggle').click(function() {  
	if($(this).find("i").hasClass("fa-times")){
		$(this).find("i").removeClass("fa-times");
		$(this).find("i").addClass("fa-bars");
	}else{
		$(this).find("i").removeClass("fa-bars");
		$(this).find("i").addClass("fa-times");
	}
});

/*-----------------------------------------------------------------------------------*/
/* 		Teklif Formu Show/Hide for Mobile
/*-----------------------------------------------------------------------------------*/
$('#formbtn').click(function() {  
	if($(this).hasClass("fa-arrow-up")){
		$(this).removeClass("fa-arrow-up");
		$(this).addClass("fa-arrow-down");
	}else{
		$(this).removeClass("fa-arrow-down");
		$(this).addClass("fa-arrow-up");
	}
	$('#teklifPart2').toggleClass("showthis");
	$('#teklifPart3').toggleClass("showthis");
});

/*-----------------------------------------------------------------------------------*/
/*    DATE PICKER
/*-----------------------------------------------------------------------------------*/
$("#datepicker").datepicker({
	inline: true,
	dateFormat: "dd-mm-yy",//tarih formatı yy=yıl mm=ay dd=gün
			changeMonth: true,//ayı elle seçmeyi aktif eder
			changeYear: true,//yılı elee seçime izin verir
			dayNames:[ "Pazar", "Pazartesi", "Salı", "Çarşamba", "perşembe", "Cuma", "Cumartesi" ],
			monthNamesShort: [ "Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık" ],
			dayNamesMin: [ "Pa", "Pzt", "Sa", "Çar", "Per", "Cum", "Cmt" ],//kısaltmalar
});
/*-----------------------------------------------------------------------------------*/
/* 		Parallax
/*-----------------------------------------------------------------------------------*/
jQuery.stellar({
   horizontalScrolling: false,
   scrollProperty: 'scroll',
   positionProperty: 'position'
});
/*-----------------------------------------------------------------------------------*/
/*	ISOTOPE PORTFOLIO
/*-----------------------------------------------------------------------------------*/
var $container = $('.time-table .doc-avai');
    $container.imagesLoaded(function () {
        $container.isotope({
            itemSelector: '.item-iso',
            layoutMode: 'masonry'
        });
	});
    $('.filter li a').click(function () {
        $('.filter li a').removeClass('active');
        $(this).addClass('active');
        var selector = $(this).attr('data-filter');
        $container.isotope({
            filter: selector
        });
        return false;
    });



   
		
});

/*-----------------------------------------------------------------------------------*/
/*    SEARCH BUTTON
/*-----------------------------------------------------------------------------------*/
$('#search-btn').click(function(){
  if($("#search-container input").hasClass("focus")){
	  $("#search-container input").removeClass("focus").fadeOut();
  }else{
	  $("#search-container input").addClass("focus").fadeIn();
  }
});

/*-----------------------------------------------------------------------------------*/
/*    STICKY SOCIAL OPEN/HIDE
/*-----------------------------------------------------------------------------------*/
$('#sticky-social span').click(function(){
  if($(this).hasClass("opened")){
	  $(this).removeClass("opened");
	  $("#sticky-social").animate({"right":"-55px"}, "slow");
  }else{
	 $(this).addClass("opened");
	  $("#sticky-social").animate({"right":"0px"}, "slow");
  }
});


/*-----------------------------------------------------------------------------------*/
/*    SIDE MENU SHOW/HIDE FOR MOBILE/*-----------------------------------------------------------------------------------*/
$('i#mobile-sidemenu-btn').click(function(){
  if($(this).hasClass("opened")){
	  $(this).removeClass("opened");
	  $(".sidemenus").slideUp();
  }else{
	 $(this).addClass("opened");
	  $(".sidemenus").slideDown();
  }
});

/*
document.addEventListener("click",function(e){
	var clickedID = e.target.id;
	alert(clickedId);
	if (clickedID != "search") {
		if($("#search-container input").hasClass("focus")){
			  $("#search-container input").removeClass("focus").fadeOut();
		}
  }
});
*/

/*-----------------------------------------------------------------------------------*/
/*    POPUP VIDEO
/*-----------------------------------------------------------------------------------*/
$('.popup-vedio').magnificPopup({
	type: 'inline',
	fixedContentPos: false,
	fixedBgPos: true,
	overflowY: 'auto',
	closeBtnInside: true,
	preloader: true,
	midClick: true,
	removalDelay: 300,
	mainClass: 'my-mfp-slide-bottom'
});
$('.gallery-pop').magnificPopup({
	delegate: 'a',
	type: 'image',
	tLoading: 'Loading image #%curr%...',
	mainClass: 'mfp-img-mobile',
	gallery: {
		enabled: true,
		navigateByImgClick: true,
		preload: [0,1] // Will preload 0 - before current, and 1 after the current image
	},
	image: {
		tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
		titleSrc: function(item) {
			return item.el.attr('title') + '';}}
});
/*-----------------------------------------------------------------------------------*/
/* 	POPUP IMAGE GALLERY
/*-----------------------------------------------------------------------------------*/
$('.popup-gallery').magnificPopup({
	delegate: 'a',
	type: 'image',
	tLoading: 'Loading image #%curr%...',
	mainClass: 'mfp-img-mobile',
	gallery: {
	enabled: true,
	navigateByImgClick: true,
	preload: [0,1]},
	image: {
	tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
	titleSrc: function(item) {
	return item.el.attr('title') + '';}}
});
/*-----------------------------------------------------------------------------------*/
/*  ISOTOPE PORTFOLIO
/*-----------------------------------------------------------------------------------*/
var $container = $('.portfolio-wrapper .items');
	$container.imagesLoaded(function () {
	   $container.isotope({
	     itemSelector: '.item',
	     layoutMode: 'masonry'
 });
});
$('.filter li a').on("click",function() {
    $('.filter li a').removeClass('active');
    $(this).addClass('active');
    var selector = $(this).attr('data-filter');
    $container.isotope({
    filter: selector
	   });
    return false;
});

/*-----------------------------------------------------------------------------------
    Animated progress bars
/*-----------------------------------------------------------------------------------*/
$('.progress-bars').waypoint(function() {
  $('.progress').each(function(){
        $(this).find('.progress-bar').animate({
                width:$(this).attr('data-percent')
            },1500);
        });
        }, { offset: '100%',
             triggerOnce: true 
});
 

//Start after window load so not messy jumping effect.
// else menu appear top 0 left 0 than jump calculated position
// TODO : If page loads when scrolled way down , menu placed wrong position.So check position for it.



  jQuery(document).ready(function($) {
 
 
 
$( "ul.ownmenu li" ).mouseover(function() {
//  $(".menu_effect").css("display", "block")
    $(".menu_effect").css({"visibility": "visible", "opacity": "0.8", "transition":"visibility 0s, opacity 0.5s linear;"});
	
	$(".menu_effect img").css("width","40%");
});

$( "ul.ownmenu li" ).mouseout(function() {
  
  $(".menu_effect").css({"visibility": "hidden", "opacity": "0", "transition":"visibility 0s, opacity 0.5s linear;"});
  	$(".menu_effect img").css("width","10%");
});
 
 
        $('#contentCarousel').carousel({
                interval: 5000
        });
 
        $('#carousel-text').html($('#slide-content-0').html());
 
        //Handles the carousel thumbnails
       $('[id^=carousel-selector-]').click( function(){
            var id = this.id.substr(this.id.lastIndexOf("-") + 1);
            var id = parseInt(id);
            $('#contentCarousel').carousel(id);
        });
 
 
        // When the carousel slides, auto update the text
        $('#contentCarousel').on('slid.bs.carousel', function (e) {
                 var id = $('.item.active').data('slide-number');
                $('#carousel-text').html($('#slide-content-'+id).html());
        });

		
		//Accordion	
		$(function() {
			var Accordion = function(el, multiple) {
				this.el = el || {};
				this.multiple = multiple || false;

				// Variables privadas
				var links = this.el.find('.link');
				// Evento
				links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
			}

			Accordion.prototype.dropdown = function(e) {
				var $el = e.data.el;
					$this = $(this),
					$next = $this.next();

				$next.slideToggle();
				$this.parent().toggleClass('open');

				if (!e.data.multiple) {
					$el.find('.submenu').not($next).slideUp().parent().removeClass('open');
				};
			}	

			var accordion = new Accordion($('#accordion'), false);
		});

		
		});



//Bootstrap Table Extension for Kalder

  function fnExcelReport() {
      var tab_text = '<table border="1px" style="font-size:20px" ">';
      var textRange;
      var j = 0;
      var tab = document.getElementById('registerForm'); // id of table
      var lines = tab.rows.length;

      // the first headline of the table
      if (lines > 0) {
          tab_text = tab_text + '<tr bgcolor="#DFDFDF">' + tab.rows[0].innerHTML + '</tr>';
      }

      // table data lines, loop starting from 1
      for (j = 1 ; j < lines; j++) {
          tab_text = tab_text + "<tr>" + tab.rows[j].innerHTML + "</tr>";
      }

      tab_text = tab_text + "</table>";
      tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");             //remove if u want links in your table
      tab_text = tab_text.replace(/<img[^>]*>/gi, "");                 // remove if u want images in your table
      tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, "");    // reomves input params
      // console.log(tab_text); // aktivate so see the result (press F12 in browser)

      var ua = window.navigator.userAgent;
      var msie = ua.indexOf("MSIE ");

      // if Internet Explorer
      if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
          txtArea1.document.open("txt/html", "replace");
          txtArea1.document.write(tab_text);
          txtArea1.document.close();
          txtArea1.focus();
          sa = txtArea1.document.execCommand("SaveAs", true, "DataTableExport.xls");
      }
      else // other browser not tested on IE 11
          sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

      return (sa);
  }



 


