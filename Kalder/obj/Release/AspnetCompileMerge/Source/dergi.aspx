﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="dergi.aspx.cs" Inherits="Kalder.dergi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        

       .grid-item {   margin:5px; border:0px solid #ccc;} 

        .grid-item a img {
            /* padding:5px;  height:263px;*/ width: 188px;
        }

        .grid-item a h6 {
            font-weight:bold;
        }

        .contentbox {
            overflow: hidden;
        }

        .overlay {
            background: rgba(0, 172, 168, .7) !important;
        }

        .filter-button-group {
        margin-bottom:20px;
        }

        .filter-button-group button {
            background-color: rgba(0, 172, 168, .9);
            color: rgb(255, 255, 255);
            padding: 10px;
            box-shadow: none;
            border:none;
            font-weight:bold;
        }


        .filter-button-group button:hover {
            background-color: #f68a1e;
        }



    </style>

    <script src="js/isotope.pkgd.min.js"></script>
    <script>


        $(document).ready(function () {
            $('.grid').isotope({
                // options
                itemSelector: '.grid-item',
                layoutMode: 'fitRows' 
            });



            // init Isotope
            var $grid = $('.grid').isotope({
                // options
            });
            // filter items on button click
            $('.filter-button-group').on('click', 'button', function () {
                var filterValue = $(this).attr('data-filter');
                $grid.isotope({ filter: filterValue });
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">




    <div id="screeno"></div>

    <!--======= CONTENT =========-->
    <div class="content">
        <section class="sub-banner" style="margin-top: 178px;">
            <div class="overlay">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="<%= Page.ResolveUrl("~")%>Anasayfa" title="Ana Sayfa">
                            <%--<img src="<%= Page.ResolveUrl("~/App_Themes/")%><%= Page.Theme %>/images/home-icon.png" alt="Home" class="home" /></a></li>--%>
                            <%--<img src="<%= Page.ResolveUrl("~/")%>images/home-icon.png" alt="Home" class="home" />--%>Anasayfa</a></li>
                        
                        <asp:Repeater ID="rptBreadcrumb" runat="server" OnItemDataBound="rptBreadcrumb_OnItemDataBound">
                    <ItemTemplate>
                        <asp:PlaceHolder ID="treeMenu" runat="server"></asp:PlaceHolder>
                    </ItemTemplate>
                </asp:Repeater>
                    </ol>
                </div>
            </div>
        </section>





        <section class="innerpages">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <i id="mobile-sidemenu-btn" class="ion-chevron-down"></i>
                            <div class="col-md-3 col-sm-3 sidemenus" id="sideBar" runat="server">

                                <asp:Repeater ID="rptSidebarModules" runat="server" OnItemDataBound="rptSidebarModules_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
                                    </ItemTemplate>
                                </asp:Repeater>

                            </div>


                            <div class="col-md-9 col-sm-9">
                                <div class="contentbox">

                                    <div class="tittle tittle-2">
										
										<h3><asp:Literal ID="ltrlBaslik" runat="server" Text="KalDer Önce Kalite Dergisi"></asp:Literal></h3>
										<hr />
									</div>

                                    <div class="button-group filter-button-group">

                                        <asp:UpdatePanel ID="up1" runat="server">
                                            <ContentTemplate>

                                                <button data-filter="*">Tümü</button>
                                                <button data-filter=".2015">2015</button>
                                                <button data-filter=".2016">2016</button>
                                                <button data-filter=".2017">2017</button>

                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>


                                        <div class="grid">


                                            <asp:Repeater ID="rptDergi" runat="server">
                                                <ItemTemplate>
                                                    <div class="grid-item <%#Eval("Yil") %>"><a href="<%#Eval("Link") %>">
                                                        <img src="upload/Dergi/<%#Eval("Resim") %>"><h6><%#Eval("Aciklama") %></h6>
                                                    </a></div>
                                                </ItemTemplate>
                                            </asp:Repeater>

                                        </div>







                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>


    </div>
</asp:Content>
