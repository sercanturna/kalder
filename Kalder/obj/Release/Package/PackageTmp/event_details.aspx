﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="event_details.aspx.cs" Inherits="Kalder.event_detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
            <script src="<%=Page.ResolveUrl("~/") %>js/jquery-qtip/jquery-1.3.2.min.js"></script>
    <script src="<%=Page.ResolveUrl("~/") %>js/jquery-qtip/jquery.qtip-1.0.0-rc3.min.js" type="text/javascript"></script>

     <script type="text/javascript" charset="utf-8">
          $(document).ready(function () {
              $(function () {

                  $("div.carousel-inner div:first-child").addClass("active");


                  $('#myCarousel2').carousel({
                      interval: 4000
                  });

                  $('.carousel').carousel({
                      interval: 4000
                  });


                //  $('#takvim-list').perfectScrollbar();
                     


              });
          });
          </script>

                <style>


                .sd-sidebar .widget_archive li:before,
.sd-sidebar .widget_categories li:before,
.sd-sidebar .widget_pages li:before,
.sd-sidebar .widget_meta li:before,
.sd-sidebar .widget_recent_comments li:before,
.sd-sidebar .widget_recent_entries li:before,
.sd-sidebar .widget_rss li:before,
.sd-sidebar .widget_nav_menu li:before,
.sd-sidebar .widget_links li:before {
	border: 0px solid #feb94a;
	border-radius: 100%;
	content: "";
	height: 6px;
	left: 0;
	position: absolute;
	top: 5px;
	width: 6px;
}

ul.events {
margin:0; 

padding: 0;
border-width: 0 0 1px;
box-shadow: 0 0 0 -3px rgba(0,0,0,0);
-webkit-transition: box-shadow .3s ease-out;
-moz-transition: box-shadow .3s ease-out;
-o-transition: box-shadow .3s ease-out;
transition: box-shadow .3s ease-out;

border-radius:6px;
}

               
                  

     

        .stButton {
        overflow:visible; height:23px;
        }
            .stButton span {
            height:100% !important;
            }

            .metro .sidebarmenu.light {
            border: none !important;
}

        .metro .sidebarmenu > ul ul li a, .metro .sidebarmenu > ul ul a, .metro .sidebarmenu > ul li a  {
        font-size:12px !important;
        }

        .metro .sidebarmenu > ul ul li a:before{
        border: 2px solid #feb94a;
border-radius: 100%;
content: "";
height: 6px;
left: 5px;
position: absolute;
top: 10px;
width: 6px;
}


        .metro .sidebarmenu ul li ul li ul li:last-child {
        border-bottom:none !important; 
        }

        #takvim-list {
        width:100% !important;
        }

        div.entry-thumb {
        border:0px solid #000000; width:650px !important;
        }

            div.entry-thumb img.pageImage {
            width:100%;
            }

        #footer .events {
        border:none;
        }

        #myCarousel2 .thumbnail {
	margin-bottom: 0;
}
.carousel-control.left, .carousel-control.right {
	background-image:none !important;
}
.carousel-control {
	color:#fff;
	top:45%;
	color:#1c1c1c;
	bottom:auto;
	padding-top:4px;
	width:30px;
	height:30px;
	text-shadow:none;
	opacity:1;
}
.carousel-control:hover {
	color: #ffa10b;
}
.carousel-control.left, .carousel-control.right {
	background-image:none !important;
}
.carousel-control.right {
	left:auto;
	right:-42px;
}
.carousel-control.left {
	right:auto;
	left:-32px;
}
 

        .well {
        background:none; border:none; box-shadow:none; margin:0 !important; padding: 0 40px !important;
        }

        .carousel-nav, carousel-pager {
        
        display:none;}

        ul.events {
        padding-top:0 !important;
        }

        ul.events li {
        margin-top:20px !important;
        }
        ul.events li:first-child {
        margin-top:0px !important;
        }

        ul.eventlist li a {
        color:#ffa10b;
        }


         .contentbox {
            overflow: hidden;
        }

        .overlay {
            background: rgba(0, 172, 168, .7) !important;
        }

        h5.bold {
        padding-left:10px; font-size:14px; margin-left:12px; line-height:1.4;
        }

        i.margin-right {
        margin-right:10px;
        }

        .posterthumb {
        height:335px; color:#fff; display:table-cell; vertical-align:middle;
        }

        .tittle-2 h3 {
    font-weight: bold;
    text-transform: inherit;
    text-align: center;
}
    </style>
 
   

    <link href="<%= Page.ResolveUrl("~/")%>js/perfect-scrollbar/css/perfect-scrollbar.min.css" rel="stylesheet" />
    <script src="<%= Page.ResolveUrl("~/")%>js/perfect-scrollbar/js/min/perfect-scrollbar.min.js"></script>
    <script src="<%= Page.ResolveUrl("~/")%>js/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
   
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function () {

                $('#takvim-list').perfectScrollbar();
            });

        
			</script>

        <link href="<%#Page.ResolveUrl("~") %>css/Takvim.css" rel="stylesheet" type="text/css" /> 
     
   
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    
    <div id="screeno"></div>

    <!--======= CONTENT =========-->
    <div class="content" > 
			<section class="sub-banner" style="margin-top:178px;">
				  <div class="overlay">
					<div class="container">
					  <ol class="breadcrumb">
						<li><a href="<%= Page.ResolveUrl("~")%>Anasayfa" title="Ana Sayfa">
                    <%--<img src="<%= Page.ResolveUrl("~/App_Themes/")%><%= Page.Theme %>/images/home-icon.png" alt="Home" class="home" /></a></li>--%>
                <%--<img src="<%= Page.ResolveUrl("~/")%>images/home-icon.png" alt="Home" class="home" />--%>Anasayfa</a></li>
                          <li><a href="<%= Page.ResolveUrl("~")%>Etkinlik_Takvimi" title="Etkinlik Takvimi">Etkinlik Takvimi</a></li>
                <asp:Repeater ID="rptBreadcrumb" runat="server" OnItemDataBound="rptBreadcrumb_ItemDataBound">
                    <ItemTemplate>
                        <asp:PlaceHolder ID="treeMenu" runat="server"></asp:PlaceHolder>
                    </ItemTemplate>
                </asp:Repeater>
					  </ol>
					</div>
				  </div>
			</section>


        <section class="innerpages">
       	<div class="container"> 
               <div class="row">
                    <div class="col-md-12">
                      
                       
                        <div class="row">
                            <i id="mobile-sidemenu-btn" class="ion-chevron-down"></i>
                            <div class="col-md-4 col-sm-12 sidemenus" id="sideBar" runat="server" style="background-color:#f2f2f2; ">
                                <div class="contentbox" style="margin-bottom:0;">
                                <div class="tittle tittle-2">
                                  <h3>Diğer Etkinlikler</h3> <hr />
                                </div>
                                  
                <div class="sd-sidebar blog-sidebar" id="Div1" runat="server">
                   <div class="col-md-4" id="takvim-list">
                    <aside  class="sidebar-widget widget_categories " >
                         <div class="sd-title-wrapper">
                          


                         <ul class="list-unstyled events" style="padding-top:10px; padding-bottom:0px; ">
                        <asp:Repeater ID="rptPopulerEvents" runat="server">
                            <ItemTemplate>
                                <li><a href="<%= Page.ResolveUrl("~")%>etkinlikler/<%#Ayarlar.UrlSeo(Eval("SeoUrl").ToString())%>"><i class="glyphicon glyphicon-calendar pull-left margin-right"></i>
                                    <h5 class="bold"><%#Eval("EventName") %><br />
                                        <small><span style="color:#ffa10b;">Tarih :</span> <%# String.Format("{0:yyyy-MMMM-dd}", Convert.ToDateTime(Eval("BeginDate")).ToString("dd MMMM yyyy")) %> - <%# String.Format("{0:yyyy-MMMM-dd}", Convert.ToDateTime(Eval("EndDate")).ToString("dd MMMM yyyy")) %></small></h5>
                                </a></li>
                            </ItemTemplate>
                        </asp:Repeater>

 
                    </ul>

                       </div>
                            
                    </aside>
                    </div>
                </div>
                <!-- / Sidebar-->
            </div>
            
 </div>

                <div class="col-md-8 col-sm-12">

                         <div class="contentbox">

                               <div class="tittle tittle-2">
                <h3><asp:HyperLink ID="hplLink" runat="server"></asp:HyperLink></h3><hr />
            </div>

                                        <aside class="entry-meta clearfix ">
                                            <ul class="list-unstyled">
                                                <li class="meta-date">BAŞLANGIÇ TARİHİ: <span class="meta-gray"> <asp:Literal ID="ltrlBaslangicTarih" runat="server"></asp:Literal></span></li>
                                                <li class="meta-category">BİTİŞ TARİHİ: <asp:Literal ID="ltrlBitisTarih" runat="server"></asp:Literal></li>
                                                <li class="meta-category">DÜZENLEYEN: <asp:Literal ID="ltrlOrganizer" runat="server"></asp:Literal></li>
                                                <li class="meta-tag">YER: <asp:Literal ID="ltrlPlace" runat="server"></asp:Literal></li>
                                            </ul>
                                        </aside>
                           

                            <asp:Panel ID="pnlVideo" runat="server">
                                <div class="flex-video widescreen">
                                    <asp:Literal ID="ltrlVideo" runat="server"></asp:Literal>
                                </div>
                            </asp:Panel>

                            <div class="entry-thumb" runat="server" id="DivThumb">
                                <figure>
                                    <a class="">
                                        <asp:Image ID="imgPageHeader" runat="server" CssClass="pageImage margin-top margin-bottom img-thumbnail img-responsive pull-left" />
                                    </a>
                                </figure>
                            </div>

                             <div class="entry-content" style="padding-top: 10px;">
                                        <asp:Literal ID="ltrlText" runat="server"></asp:Literal>
                                    </div> 

                           

                        </div>

  


                        <div class="col-md-12 col-sm-12" style="margin:0; padding:0;" id="fotos" runat="server">
                                    <div class="contentbox">
                                        <div id="PnlGallery" runat="server">
                                            <div class="tittle tittle-2">
                                         <h3>Etkinliğe ait fotoğraflar</h3>
                                        <hr />
                                    </div>
                                           

                                        <div class="gallery-container">
                                            <asp:Repeater ID="rptImageGallery" runat="server" OnItemDataBound="rptImages_ItemDataBound">
                                                <ItemTemplate>
                                                    <asp:Literal ID="ltrlFigure" runat="server"></asp:Literal>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>

                                        <!-- Root element of PhotoSwipe. Must have class pswp. -->
                                        <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

                                            <!-- Background of PhotoSwipe. 
                                 It's a separate element, as animating opacity is faster than rgba(). -->
                                            <div class="pswp__bg"></div>

                                            <!-- Slides wrapper with overflow:hidden. -->
                                            <div class="pswp__scroll-wrap">

                                                <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
                                                <!-- don't modify these 3 pswp__item elements, data is added later on. -->
                                                <div class="pswp__container">
                                                    <div class="pswp__item"></div>
                                                    <div class="pswp__item"></div>
                                                    <div class="pswp__item"></div>
                                                </div>

                                                <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                                                <div class="pswp__ui pswp__ui--hidden">

                                                    <div class="pswp__top-bar">

                                                        <!--  Controls are self-explanatory. Order can be changed. -->

                                                        <div class="pswp__counter"></div>

                                                        <button type="button" class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                                                        <button type="button" class="pswp__button pswp__button--share" title="Share"></button>

                                                        <button type="button" class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                                                        <button type="button" class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                                                        <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                                                        <!-- element will get class pswp__preloader--active when preloader is running -->
                                                        <div class="pswp__preloader">
                                                            <div class="pswp__preloader__icn">
                                                                <div class="pswp__preloader__cut">
                                                                    <div class="pswp__preloader__donut"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                                        <div class="pswp__share-tooltip"></div>
                                                    </div>

                                                    <button type="button" class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                                                    </button>

                                                    <button type="button" class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                                                    </button>

                                                    <div class="pswp__caption">
                                                        <div class="pswp__caption__center"></div>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                        <link href="<%= ResolveUrl("~/") %>js/photoswipe/dist/photoswipe.css" rel="stylesheet" />
                                        <link href="<%= ResolveUrl("~/") %>js/photoswipe/dist/default-skin/default-skin.css" rel="stylesheet" />
                                        <script src="<%= ResolveUrl("~/") %>js/photoswipe/dist/photoswipe.min.js"></script>
                                        <script src="<%= ResolveUrl("~/") %>js/photoswipe/dist/photoswipe-ui-default.min.js"></script>

                                        <script>
                                            var initPhotoSwipeFromDOM = function (gallerySelector) {

                                                // parse slide data (url, title, size ...) from DOM elements 
                                                // (children of gallerySelector)
                                                var parseThumbnailElements = function (el) {
                                                    var thumbElements = el.childNodes,
                                                        numNodes = thumbElements.length,
                                                        items = [],
                                                        figureEl,
                                                        linkEl,
                                                        size,
                                                        item;

                                                    for (var i = 0; i < numNodes; i++) {

                                                        figureEl = thumbElements[i]; // <figure> element

                                                        // include only element nodes 
                                                        if (figureEl.nodeType !== 1) {
                                                            continue;
                                                        }

                                                        linkEl = figureEl.children[0]; // <a> element

                                                        size = linkEl.getAttribute('data-size').split('x');

                                                        // create slide object
                                                        item = {
                                                            src: linkEl.getAttribute('href'),
                                                            w: parseInt(size[0], 10),
                                                            h: parseInt(size[1], 10)
                                                        };



                                                        if (figureEl.children.length > 1) {
                                                            // <figcaption> content
                                                            item.title = figureEl.children[1].innerHTML;
                                                        }

                                                        if (linkEl.children.length > 0) {
                                                            // <img> thumbnail element, retrieving thumbnail url
                                                            item.msrc = linkEl.children[0].getAttribute('src');
                                                        }

                                                        item.el = figureEl; // save link to element for getThumbBoundsFn
                                                        items.push(item);
                                                    }

                                                    return items;
                                                };

                                                // find nearest parent element
                                                var closest = function closest(el, fn) {
                                                    return el && (fn(el) ? el : closest(el.parentNode, fn));
                                                };

                                                // triggers when user clicks on thumbnail
                                                var onThumbnailsClick = function (e) {
                                                    e = e || window.event;
                                                    e.preventDefault ? e.preventDefault() : e.returnValue = false;

                                                    var eTarget = e.target || e.srcElement;

                                                    // find root element of slide
                                                    var clickedListItem = closest(eTarget, function (el) {
                                                        return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
                                                    });

                                                    if (!clickedListItem) {
                                                        return;
                                                    }

                                                    // find index of clicked item by looping through all child nodes
                                                    // alternatively, you may define index via data- attribute
                                                    var clickedGallery = clickedListItem.parentNode,
                                                        childNodes = clickedListItem.parentNode.childNodes,
                                                        numChildNodes = childNodes.length,
                                                        nodeIndex = 0,
                                                        index;

                                                    for (var i = 0; i < numChildNodes; i++) {
                                                        if (childNodes[i].nodeType !== 1) {
                                                            continue;
                                                        }

                                                        if (childNodes[i] === clickedListItem) {
                                                            index = nodeIndex;
                                                            break;
                                                        }
                                                        nodeIndex++;
                                                    }



                                                    if (index >= 0) {
                                                        // open PhotoSwipe if valid index found
                                                        openPhotoSwipe(index, clickedGallery);
                                                    }
                                                    return false;
                                                };

                                                // parse picture index and gallery index from URL (#&pid=1&gid=2)
                                                var photoswipeParseHash = function () {
                                                    var hash = window.location.hash.substring(1),
                                                    params = {};

                                                    if (hash.length < 5) {
                                                        return params;
                                                    }

                                                    var vars = hash.split('&');
                                                    for (var i = 0; i < vars.length; i++) {
                                                        if (!vars[i]) {
                                                            continue;
                                                        }
                                                        var pair = vars[i].split('=');
                                                        if (pair.length < 2) {
                                                            continue;
                                                        }
                                                        params[pair[0]] = pair[1];
                                                    }

                                                    if (params.gid) {
                                                        params.gid = parseInt(params.gid, 10);
                                                    }

                                                    return params;
                                                };

                                                var openPhotoSwipe = function (index, galleryElement, disableAnimation, fromURL) {
                                                    var pswpElement = document.querySelectorAll('.pswp')[0],
                                                        gallery,
                                                        options,
                                                        items;

                                                    items = parseThumbnailElements(galleryElement);

                                                    // define options (if needed)
                                                    options = {

                                                        // define gallery index (for URL)
                                                        galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                                                        getThumbBoundsFn: function (index) {
                                                            // See Options -> getThumbBoundsFn section of documentation for more info
                                                            var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                                                                pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                                                                rect = thumbnail.getBoundingClientRect();

                                                            return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
                                                        }

                                                    };

                                                    // PhotoSwipe opened from URL
                                                    if (fromURL) {
                                                        if (options.galleryPIDs) {
                                                            // parse real index when custom PIDs are used 
                                                            // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                                                            for (var j = 0; j < items.length; j++) {
                                                                if (items[j].pid == index) {
                                                                    options.index = j;
                                                                    break;
                                                                }
                                                            }
                                                        } else {
                                                            // in URL indexes start from 1
                                                            options.index = parseInt(index, 10) - 1;
                                                        }
                                                    } else {
                                                        options.index = parseInt(index, 10);
                                                    }

                                                    // exit if index not found
                                                    if (isNaN(options.index)) {
                                                        return;
                                                    }

                                                    if (disableAnimation) {
                                                        options.showAnimationDuration = 0;
                                                    }

                                                    // Pass data to PhotoSwipe and initialize it
                                                    gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
                                                    gallery.init();
                                                };

                                                // loop through all gallery elements and bind events
                                                var galleryElements = document.querySelectorAll(gallerySelector);

                                                for (var i = 0, l = galleryElements.length; i < l; i++) {
                                                    galleryElements[i].setAttribute('data-pswp-uid', i + 1);
                                                    galleryElements[i].onclick = onThumbnailsClick;
                                                }

                                                // Parse URL and open gallery if it contains #&pid=3&gid=1
                                                var hashData = photoswipeParseHash();
                                                if (hashData.pid && hashData.gid) {
                                                    openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
                                                }
                                            };

                                            // execute above function
                                            initPhotoSwipeFromDOM('.gallery-container');

                                        </script>
                                        <script>
                                            if (document.location.search.match(/type=embed/gi)) {
                                                window.parent.postMessage("resize", "*");
                                            }
                                        </script>

                                    </div>
                                        </div>

                                </div>
     
                </div>

                            </div>
                        </div>

                   </div>
               </div>
          </section>
      

        <asp:Repeater ID="rptFooterModules" runat="server" OnItemDataBound="rptFooterModules_ItemDataBound">
            <ItemTemplate>
                <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
            </ItemTemplate>
        </asp:Repeater>
    </div>
          
    
</asp:Content>

