﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="sanal_kutuphane.aspx.cs" Inherits="Kalder.sanal_kutuphane" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script>
        $(function () {
            var v = document.getElementById('<%= hdnPid.ClientID %>');
            var val = v.value;

            $("#cssmenu a#" + val).addClass("active");


            $('#library').DataTable({
                dom: 'Bfrtip',
                buttons: [

                ],
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Turkish.json"
                }
            });
        });
    </script>
    <style>
        ul.bookList li label {
        width:70px !important;
      
        border:0px solid #ff0000;
        }

        a.bookDetail:hover {
color:#ff6a00;
        }

            .contentbox {
            overflow: hidden;
        }

        .overlay {
            background: rgba(0, 172, 168, .7) !important;
        }


    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:HiddenField ID="hdnPid" runat="server" />

    <div id="screeno"></div>

    <!--======= CONTENT =========-->
    <div class="content">

          <!-- Bootstrap Modal Dialog -->
<div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <asp:UpdatePanel ID="upModal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" style="color:#00aca8; padding:5px;"><asp:Label ID="lblModalTitle" runat="server" Text=""></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblModalBody" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Kapat</button>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>


        <section class="sub-banner" style="margin-top: 178px;">
            <div class="overlay">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="<%= Page.ResolveUrl("~")%>Anasayfa" title="Ana Sayfa">
                            <%--<img src="<%= Page.ResolveUrl("~/App_Themes/")%><%= Page.Theme %>/images/home-icon.png" alt="Home" class="home" /></a></li>--%>
                            <%--<img src="<%= Page.ResolveUrl("~/")%>images/home-icon.png" alt="Home" class="home" />--%>Anasayfa</a></li>

                        <asp:Repeater ID="rptBreadcrumb" runat="server" OnItemDataBound="rptBreadcrumb_OnItemDataBound">
                            <ItemTemplate>
                                <asp:PlaceHolder ID="treeMenu" runat="server"></asp:PlaceHolder>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ol>
                </div>
            </div>
        </section>





        <section class="innerpages">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <i id="mobile-sidemenu-btn" class="ion-chevron-down"></i>
                            <div class="col-md-3 col-sm-3 sidemenus" id="sideBar" runat="server">
                                <asp:Repeater ID="rptSidebarModules" runat="server" OnItemDataBound="rptSidebarModules_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>


                            <div class="col-md-9 col-sm-9">
                                <div class="contentbox">

                                    <div class="tittle tittle-2">

                                        <h3>
                                            <asp:Literal ID="ltrlBaslik" runat="server" Text="Sanal Kütüphane"></asp:Literal></h3>
                                        <hr />
                                        <i>Aşağıdaki tabloda yer alan arama robotu ile Kitap Adı, Yazar Adı, Yayıncı Kurum gibi sonuçları canlı olarak filtreleyebilirsiniz.</i>
                                    </div>

                                    <table class="table table-condensed table-striped" id="library">
                                        <thead>
                                            <tr>
                                                <td><h3 style="margin-top: 10px;">Sanal Kütüphane</h3></td>
                                              <%--  <td><label>Kitap Adı </label></td>
                                                <td><label>Yazar </label></td>
                                                <td><label>Yayıncı </label></td>--%>
                                            </tr>
                                        </thead>
                                        <tbody>        
                                            <asp:Repeater ID="rptKutuphane" runat="server" OnItemCommand="rptKutuphane_ItemCommand">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                          
                                                            <asp:UpdatePanel ID="up5" runat="server">
                                                                <ContentTemplate>
                                                            <asp:LinkButton ID="lnkBookDetail" runat="server" CssClass="bookDetail" CommandName="show" CommandArgument='<%# Eval("Id") %>'>
                                                           <%-- <a class="bookDetail" data-toggle="modal" data-target="#BookModal">--%>
                                                            <ul class="bookList">
                                                                <li><label>Kitap Adı </label>: <%# Ayarlar.OzetCek(Eval("KitapAdi").ToString(),100) %></li>
                                                                <li><label>Yazar </label>: <%# Ayarlar.OzetCek(Eval("Yazar").ToString(),100) %></li>
                                                                <li><label>Yayıncı </label>: <%# Ayarlar.OzetCek(Eval("Yayinci").ToString(),100) %></li>
                                                            </ul>
                                                                </asp:LinkButton>
                                                                    </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div>



  
</asp:Content>
