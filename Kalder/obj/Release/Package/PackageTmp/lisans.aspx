﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="lisans.aspx.cs" Inherits="Kalder.lisans" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

            /* delay function */
            jQuery.fn.delay = function (time, func) {
                return this.each(function () {
                    setTimeout(func, time);
                });
            };


            jQuery.fn.countDown = function (settings, to) {
                settings = jQuery.extend({
                    startFontSize: '36px',
                    endFontSize: '12px',
                    duration: 1000,
                    startNumber: 10,
                    endNumber: 0,
                    callBack: function () { }
                }, settings);
                return this.each(function () {

                    if (!to && to != settings.endNumber) { to = settings.startNumber; }

                    //set the countdown to the starting number
                    $(this).text(to).css('fontSize', settings.startFontSize);

                    //loopage
                    $(this).animate({
                        'fontSize': settings.endFontSize
                    }, settings.duration, '', function () {
                        if (to > settings.endNumber + 1) {
                            $(this).css('fontSize', settings.startFontSize).text(to - 1).countDown(settings, to - 1);
                        }
                        else {
                            settings.callBack(this);
                        }
                    });

                });
            };

            //$('#countdown').countDown({
            //    startNumber: 10,
            //    callBack: function (me) {
            //        window.location = "default.aspx";
            //    }
            //});

        });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
        (function () {
            var bsa = document.createElement('script');
            bsa.type = 'text/javascript';
            bsa.async = true;
            bsa.src = '//s3.buysellads.com/ac/bsa.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(bsa);
        })();
</script>
    <div class="clear page_container" style="background:url(images/error.png) right top no-repeat;">
        <h1 style="width:100%; text-align:center; font-size:25px; margin-top:20px; margin-bottom:20px;">
            Lisans Hatası!
        </h1>
          <p style="min-height: 350px; text-align: center; font-size:14px; line-height:2; color:#808080; font-family: 'Open Sans', sans-serif;">
            Alan adınızın bu yazılımını kullanma yetkisi yok.
            <br />
            Yıllık kullanım süreniz dolmuş olabilir. Ayrıca site dosyalarını farklı bir alan adına kurduysanız da bu hatayı alırsınız. 
            <br />
                Lütfen Cplus Interactive yetkilileriyle iletişime geçin..<br />
		<%--<span id="countdown"></span>--%>
       
      
        </p>
    </div>
</asp:Content>
