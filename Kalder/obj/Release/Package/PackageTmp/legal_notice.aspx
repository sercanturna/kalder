﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="legal_notice.aspx.cs" Inherits="Kalder.legal_notice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        table.Legal {
        width:80%; margin-top:5%;
        }
     
            table.Legal tbody tr td:first-child {
            width:20%; font-weight:600; line-height:2;
            }
            table.Legal tbody tr td {
            line-height:1.5;
            }
        table.Legal tbody tr td.sep {
        padding:5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
      <div class="clear page_container">
           <div class="breadcumb">
            <ul id="breadcrumb" class="breadcumb">
                <li><a href="<%= Page.ResolveUrl("~")%>Anasayfa" title="Ana Sayfa">
                    <%--<img src="<%= Page.ResolveUrl("~/App_Themes/")%><%= Page.Theme %>/images/home-icon.png" alt="Home" class="home" /></a></li>--%>
                    <img src="<%= Page.ResolveUrl("~/")%>images/home-icon.png" alt="Home" class="home" /></a></li>
                    

                <li><a href="<%= Page.ResolveUrl("~")%>haberler" title="Yasal Bilgiler">Yasal Bilgiler</a></li>
                <li><asp:Literal ID="ltrlnewsCat" runat="server"></asp:Literal></li>
            </ul>
            
            <div class="social_share">
                <!-- AddThis Button BEGIN -->
                <div class="addthis_toolbox addthis_default_style " style="float:right; width:150px;">
                <a class="addthis_button_preferred_1"></a>
                <a class="addthis_button_preferred_2"></a>
                <a class="addthis_button_preferred_3"></a>
                <a class="addthis_button_preferred_4"></a>
                <a class="addthis_button_compact"></a>
                <a class="addthis_counter addthis_bubble_style"></a>
                </div>
                <script type="text/javascript">var addthis_config = { "data_track_addressbar": true };</script>
                <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f34f2f752ee3f86"></script>
                <!-- AddThis Button END -->
            </div>
        </div>
        <table class="Legal">
        <tbody>
            <tr>
                <td>Firma Ünvanı</td>
                <td class="sep">:</td>
                <td colspan="4"><asp:Literal ID="ltrlCompany" runat="server"></asp:Literal></td>
            </tr>
             <tr>
                <td>Vergi Dairesi</td>
               <td class="sep">:</td>
                <td colspan="4"><asp:Literal ID="ltrlVergiDairesi" runat="server"></asp:Literal></td>
            </tr>
             <tr>
                <td>Vergi Numarası</td>
                <td class="sep">:</td>
                <td colspan="4"><asp:Literal ID="ltrlVergiNo" runat="server"></asp:Literal></td>
            </tr>
             <tr>
                <td>Mersis Numarası</td>
                <td class="sep">:</td>
                <td colspan="4"><asp:Literal ID="ltrlMersis" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td>Nace Kodu</td>
                <td class="sep">:</td>
                <td colspan="4"><asp:Literal ID="ltrlNaceKodu" runat="server"></asp:Literal></td>
            </tr>

           
             <tr >
                <td style="padding-top:20px;">Ortak 1 </td>
                <td class="sep">:</td>
                <td><asp:Literal ID="ltrlOrtak1" runat="server" Text="-"></asp:Literal></td>
                <td colspan="2">Sermaye Tutarı :</td>
                <td><asp:Literal ID="ltrlOrtak1Sermaye" runat="server"></asp:Literal></td>
            </tr>
             <tr>
                <td>Ortak 2 </td>
                <td class="sep">:</td>
                <td><asp:Literal ID="ltrlOrtak2" runat="server" Text="-"></asp:Literal></td>
                <td>Sermaye Tutarı :</td>
                <td colspan="2"><asp:Literal ID="ltrlOrtak2Sermaye" runat="server"></asp:Literal></td>
            </tr>
             <tr>
                <td>Ortak 3</td>
                <td class="sep">:</td>
                <td><asp:Literal ID="ltrlOrtak3" runat="server" Text="-"></asp:Literal>
                <td>Sermaye Tutarı :</td>
                <td colspan="2"><asp:Literal ID="ltrlOrtak3Sermaye" runat="server"></asp:Literal></td>
            </tr>
             <tr>
                <td>Ortak 4</td>
                <td class="sep">:</td>
                <td><asp:Literal ID="ltrlOrtak4" runat="server" Text="-"></asp:Literal>
                 <td>Sermaye Tutarı :</td>
                <td colspan="2"><asp:Literal ID="ltrlOrtak4Sermaye" runat="server"></asp:Literal></td>
            </tr>
            <tr>
                <td>Finansal Tablolar</td>
                <td class="sep">:</td>
                <td><asp:HyperLink ID="hplFinancialFiles" runat="server"></asp:HyperLink></td>
            </tr>
        </tbody>
    </table>
        </div>
</asp:Content>

