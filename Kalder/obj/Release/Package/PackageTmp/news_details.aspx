﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="news_details.aspx.cs" Inherits="Kalder.news_details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="<%= Page.ResolveUrl("~")%>js/jquery.mobile.vmouse.js"></script>

    <script src="<%= Page.ResolveUrl("~")%>js/font-size.js"></script>
    <script src="<%= Page.ResolveUrl("~")%>js/jquery.jcarousel.min.js"></script>
    <script src="<%= Page.ResolveUrl("~")%>js/jquery.jcarousel.swipe.js"></script>
    <link href="<%= Page.ResolveUrl("~")%>js/jcarousel.responsive.css" rel="stylesheet" />

    <script src="<%= Page.ResolveUrl("~")%>js/jcarousel.responsive.js"></script>


    <script>
        $(function () {
            $('.fb-comments').attr('data-width', $('#commentboxcontainer').width());

         


        });

         
        //Youtube Plugin


    </script>

    <style>
        .video-container {
            position: relative;
            padding-bottom: 56.25%;
            height: 0;
            overflow: hidden;
            padding-top: 30px;
            margin-bottom: 10px;
        }

            .video-container iframe,
            .video-container object,
            .video-container embed {
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
            }

        .gallery-container li {
            width: 178px;
        }

        .gallery-container figure {
         border:0px solid #22282f; height:160px; white-space:nowrap; padding:5px; text-align:center; overflow:hidden;
          

     background-color: #22282f;
   border-left:3px solid white;
   border-right:3px solid white;
        }
            .gallery-container figure:last-child {
             padding:5px;
            }
            .gallery-container figure img {
            vertical-align:middle;
            max-height:145px;
            width:100%;
            }


            .helper {
    display: inline-block;
    height: 100%;
    vertical-align: middle;
    
}

        .contentbox {
        overflow:hidden;
        }

        .overlay {
    background: rgba(0, 172, 168, .7) !important;
}

        .breadcrumb {
        margin: 63px 0px !important;
}

        img.news_img {
    border: 1px solid #cacaca;
    padding: 2px;
    margin-right: 10px;
    max-width:420px !important;
}


h5.news_summary {
    font-weight: bold;
    margin-bottom: 10px;
    font-size: 14px;
    line-height:1.4;
}

        div.text-conntainer p {
        text-align:left;
        }

        .text-conntainer li {
        list-style:inherit;
        }

    </style>
    <script type="text/javascript" src="<%= ResolveUrl("~/Adminv2") %>/js/plugins/hoverIntent/jquery.hoverIntent.minified.js"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/Adminv2") %>/js/demo/gallery.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div id="screeno"></div>

    <!--======= CONTENT =========-->
    <div class="content">
        <section class="sub-banner" style="margin-top: 178px;">
            <div class="overlay">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="<%= Page.ResolveUrl("~")%>Anasayfa" title="Ana Sayfa">Ana Sayfa</a></li>
                        <li><a href="<%= Page.ResolveUrl("~")%>Haberler">
                            <asp:Literal ID="ltrlBlog" runat="server"></asp:Literal></a></li>
                        <li>
                            <asp:Literal ID="ltrlnewsCat" runat="server"></asp:Literal></li>
                        <li>
                            <asp:Literal ID="ltrlNewsTitle" runat="server"></asp:Literal></li>
                    </ol>
                </div>
            </div>
        </section>


        <section class="innerpages">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <i id="mobile-sidemenu-btn" class="ion-chevron-down"></i>
                            <div class="col-md-3 col-sm-3 sidemenus" id="sideBar" runat="server">

                                <asp:Repeater ID="rptSidebarModules" runat="server" OnItemDataBound="rptSidebarModules_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
                                    </ItemTemplate>
                                </asp:Repeater>



                                <ul>
                                    <asp:Repeater ID="rptOtherNews" runat="server" OnItemDataBound="rptOtherNews_ItemDataBound" Visible="false">
                                        <ItemTemplate>
                                            <li style="border: 0px solid #ff0000; position: relative; display: block;">
                                                <a href="<%= ResolveUrl("~/") %><%#Ayarlar.UrlSeo(Eval("KategoriAdi").ToString()) %>/<%#Eval("HaberId") %>/<%#Ayarlar.UrlSeo(Eval("Baslik").ToString()) %>">
                                                    <asp:Image ID="img" runat="server" Style="height: 130px !important; width: 100%" />
                                    
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>





                            <div class="col-md-9 col-sm-9">
                                <div class="contentbox">

                                    <asp:Repeater ID="rptContentModules" runat="server" OnItemDataBound="rptContentModules_ItemDataBound">
                                        <ItemTemplate>
                                            <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
                                        </ItemTemplate>
                                    </asp:Repeater>

                                    <asp:Image ID="imgPageHeader" runat="server" CssClass="pageImage" />

                                    <div class="tittle tittle-2" >

                                        <h3>
                                            <asp:Literal ID="ltrlBaslik" runat="server"></asp:Literal></h3>
                                        <hr />
                                    </div>
                                    <div class="text-conntainer">
                                        <asp:Panel ID="pnlVideo" runat="server">
                                            <div class="video-container">
                                                <asp:Literal ID="ltrlVideo" runat="server"></asp:Literal>
                                            </div>
                                        </asp:Panel>

                                        <asp:Literal ID="ltrlContent" runat="server"></asp:Literal>
                                    </div>
                                    <div id="commentboxcontainer" style="max-width: 100%; margin-top: 15px;">
                                        <asp:Literal ID="ltrlFbComments" runat="server" Visible="false"></asp:Literal>
                                    </div>

                                </div>

                                
                        </div>

                            <div class="col-md-9 col-sm-9 col-md-offset-3">
                                    <div class="contentbox">
                                        <div id="PnlGallery" runat="server">

                                            <div class="tittle tittle-2">

                                         <h3>Habere ait diğer görseller</h3>
                                        <hr />
                                    </div>
                                           

                                            <div class="gallery-container">



                                            <asp:Repeater ID="rptPhotoGallery" runat="server" OnItemDataBound="rptImages_ItemDataBound">
                                                <ItemTemplate>
                                                    <asp:Literal ID="ltrlFigure" runat="server"></asp:Literal>
                                                </ItemTemplate>
                                            </asp:Repeater>

                                        </div>

                                        <!-- Root element of PhotoSwipe. Must have class pswp. -->
                                        <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

                                            <!-- Background of PhotoSwipe. 
                                 It's a separate element, as animating opacity is faster than rgba(). -->
                                            <div class="pswp__bg"></div>

                                            <!-- Slides wrapper with overflow:hidden. -->
                                            <div class="pswp__scroll-wrap">

                                                <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
                                                <!-- don't modify these 3 pswp__item elements, data is added later on. -->
                                                <div class="pswp__container">
                                                    <div class="pswp__item"></div>
                                                    <div class="pswp__item"></div>
                                                    <div class="pswp__item"></div>
                                                </div>

                                                <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
                                                <div class="pswp__ui pswp__ui--hidden">

                                                    <div class="pswp__top-bar">

                                                        <!--  Controls are self-explanatory. Order can be changed. -->

                                                        <div class="pswp__counter"></div>

                                                        <button type="button" class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                                                        <button type="button" class="pswp__button pswp__button--share" title="Share"></button>

                                                        <button type="button" class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                                                        <button type="button" class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                                                        <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                                                        <!-- element will get class pswp__preloader--active when preloader is running -->
                                                        <div class="pswp__preloader">
                                                            <div class="pswp__preloader__icn">
                                                                <div class="pswp__preloader__cut">
                                                                    <div class="pswp__preloader__donut"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                                                        <div class="pswp__share-tooltip"></div>
                                                    </div>

                                                    <button type="button" class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                                                    </button>

                                                    <button type="button" class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                                                    </button>

                                                    <div class="pswp__caption">
                                                        <div class="pswp__caption__center"></div>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>
                                        <link href="<%= ResolveUrl("~/") %>js/photoswipe/dist/photoswipe.css" rel="stylesheet" />
                                        <link href="<%= ResolveUrl("~/") %>js/photoswipe/dist/default-skin/default-skin.css" rel="stylesheet" />
                                        <script src="<%= ResolveUrl("~/") %>js/photoswipe/dist/photoswipe.min.js"></script>
                                        <script src="<%= ResolveUrl("~/") %>js/photoswipe/dist/photoswipe-ui-default.min.js"></script>


                                        <script>
                                            var initPhotoSwipeFromDOM = function (gallerySelector) {

                                                // parse slide data (url, title, size ...) from DOM elements 
                                                // (children of gallerySelector)
                                                var parseThumbnailElements = function (el) {
                                                    var thumbElements = el.childNodes,
                                                        numNodes = thumbElements.length,
                                                        items = [],
                                                        figureEl,
                                                        linkEl,
                                                        size,
                                                        item;

                                                    for (var i = 0; i < numNodes; i++) {

                                                        figureEl = thumbElements[i]; // <figure> element

                                                        // include only element nodes 
                                                        if (figureEl.nodeType !== 1) {
                                                            continue;
                                                        }

                                                        linkEl = figureEl.children[0]; // <a> element

                                                        size = linkEl.getAttribute('data-size').split('x');

                                                        // create slide object
                                                        item = {
                                                            src: linkEl.getAttribute('href'),
                                                            w: parseInt(size[0], 10),
                                                            h: parseInt(size[1], 10)
                                                        };



                                                        if (figureEl.children.length > 1) {
                                                            // <figcaption> content
                                                            item.title = figureEl.children[1].innerHTML;
                                                        }

                                                        if (linkEl.children.length > 0) {
                                                            // <img> thumbnail element, retrieving thumbnail url
                                                            item.msrc = linkEl.children[0].getAttribute('src');
                                                        }

                                                        item.el = figureEl; // save link to element for getThumbBoundsFn
                                                        items.push(item);
                                                    }

                                                    return items;
                                                };

                                                // find nearest parent element
                                                var closest = function closest(el, fn) {
                                                    return el && (fn(el) ? el : closest(el.parentNode, fn));
                                                };

                                                // triggers when user clicks on thumbnail
                                                var onThumbnailsClick = function (e) {
                                                    e = e || window.event;
                                                    e.preventDefault ? e.preventDefault() : e.returnValue = false;

                                                    var eTarget = e.target || e.srcElement;

                                                    // find root element of slide
                                                    var clickedListItem = closest(eTarget, function (el) {
                                                        return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
                                                    });

                                                    if (!clickedListItem) {
                                                        return;
                                                    }

                                                    // find index of clicked item by looping through all child nodes
                                                    // alternatively, you may define index via data- attribute
                                                    var clickedGallery = clickedListItem.parentNode,
                                                        childNodes = clickedListItem.parentNode.childNodes,
                                                        numChildNodes = childNodes.length,
                                                        nodeIndex = 0,
                                                        index;

                                                    for (var i = 0; i < numChildNodes; i++) {
                                                        if (childNodes[i].nodeType !== 1) {
                                                            continue;
                                                        }

                                                        if (childNodes[i] === clickedListItem) {
                                                            index = nodeIndex;
                                                            break;
                                                        }
                                                        nodeIndex++;
                                                    }



                                                    if (index >= 0) {
                                                        // open PhotoSwipe if valid index found
                                                        openPhotoSwipe(index, clickedGallery);
                                                    }
                                                    return false;
                                                };

                                                // parse picture index and gallery index from URL (#&pid=1&gid=2)
                                                var photoswipeParseHash = function () {
                                                    var hash = window.location.hash.substring(1),
                                                    params = {};

                                                    if (hash.length < 5) {
                                                        return params;
                                                    }

                                                    var vars = hash.split('&');
                                                    for (var i = 0; i < vars.length; i++) {
                                                        if (!vars[i]) {
                                                            continue;
                                                        }
                                                        var pair = vars[i].split('=');
                                                        if (pair.length < 2) {
                                                            continue;
                                                        }
                                                        params[pair[0]] = pair[1];
                                                    }

                                                    if (params.gid) {
                                                        params.gid = parseInt(params.gid, 10);
                                                    }

                                                    return params;
                                                };

                                                var openPhotoSwipe = function (index, galleryElement, disableAnimation, fromURL) {
                                                    var pswpElement = document.querySelectorAll('.pswp')[0],
                                                        gallery,
                                                        options,
                                                        items;

                                                    items = parseThumbnailElements(galleryElement);

                                                    // define options (if needed)
                                                    options = {

                                                        // define gallery index (for URL)
                                                        galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                                                        getThumbBoundsFn: function (index) {
                                                            // See Options -> getThumbBoundsFn section of documentation for more info
                                                            var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                                                                pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                                                                rect = thumbnail.getBoundingClientRect();

                                                            return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
                                                        }

                                                    };

                                                    // PhotoSwipe opened from URL
                                                    if (fromURL) {
                                                        if (options.galleryPIDs) {
                                                            // parse real index when custom PIDs are used 
                                                            // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                                                            for (var j = 0; j < items.length; j++) {
                                                                if (items[j].pid == index) {
                                                                    options.index = j;
                                                                    break;
                                                                }
                                                            }
                                                        } else {
                                                            // in URL indexes start from 1
                                                            options.index = parseInt(index, 10) - 1;
                                                        }
                                                    } else {
                                                        options.index = parseInt(index, 10);
                                                    }

                                                    // exit if index not found
                                                    if (isNaN(options.index)) {
                                                        return;
                                                    }

                                                    if (disableAnimation) {
                                                        options.showAnimationDuration = 0;
                                                    }

                                                    // Pass data to PhotoSwipe and initialize it
                                                    gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
                                                    gallery.init();
                                                };

                                                // loop through all gallery elements and bind events
                                                var galleryElements = document.querySelectorAll(gallerySelector);

                                                for (var i = 0, l = galleryElements.length; i < l; i++) {
                                                    galleryElements[i].setAttribute('data-pswp-uid', i + 1);
                                                    galleryElements[i].onclick = onThumbnailsClick;
                                                }

                                                // Parse URL and open gallery if it contains #&pid=3&gid=1
                                                var hashData = photoswipeParseHash();
                                                if (hashData.pid && hashData.gid) {
                                                    openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
                                                }
                                            };

                                            // execute above function
                                            initPhotoSwipeFromDOM('.gallery-container');

                                        </script>
                                        <script>
                                            if (document.location.search.match(/type=embed/gi)) {
                                                window.parent.postMessage("resize", "*");
                                            }
                                        </script>

                                    </div>
                                        </div>

                                </div>
                    </div>
                </div>
            </div>
    </div>




    </section>

  

    <asp:Repeater ID="rptFooterModules" runat="server" OnItemDataBound="rptFooterModules_ItemDataBound">
        <ItemTemplate>
            <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
        </ItemTemplate>
    </asp:Repeater>

    </div>

</asp:Content>

