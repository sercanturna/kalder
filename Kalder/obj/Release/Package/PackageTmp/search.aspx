﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="search.aspx.cs" Inherits="Kalder.search" %>
 
<%@ Register Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<%@ Register Assembly="CollectionPager" Namespace="SiteUtils" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
  <%--<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>--%>
     <script>
    

         $(function () {

             __doPostBack('<%=UpSearchResults.ClientID %>', null); 
            <%--   $("#tabs").tabs();

            $('.linkHaber').click(function () {
                 $('#<%=lnkNews.ClientID%>').trigger('click');
                }); --%>
          
             
       
         });
         

       

         Sys.Application.add_load(function() {
            
             getCities();
         }
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  
 
    <div id="screeno"></div>

    <!--======= CONTENT =========-->
		<div class="content search" > 
			<section class="sub-banner" style="margin-top:178px;">
				  <div class="overlay">
					<div class="container">

            <ol id="breadcrumb" class="breadcrumb">
                <li><a href="<%= Page.ResolveUrl("~")%>Anasayfa" title="Ana Sayfa">
                    <%--<img src="<%= Page.ResolveUrl("~/App_Themes/")%><%= Page.Theme %>/images/home-icon.png" alt="Home" class="home" /></a></li>--%>
                <%--<img src="<%= Page.ResolveUrl("~/")%>images/home-icon.png" alt="Home" class="home" />--%>Anasayfa</a></li>
                <li><a href="<%= Page.ResolveUrl("~")%>search" title="Sitede Ara">Sitede Ara</a></li>
                <li>
                    <asp:Literal ID="ltrlnewsCat" runat="server"></asp:Literal></li>
            </ol>
 

         
                        </div>
                      </div>
                </section>
 
            	<section class="innerpages">
				<div class="container"> 
					<div class="row">
					<div class="col-md-12">


        <div id="pageContent" class="page_content" runat="server" style="width:100%">
            <div class="search_bar" style="border-bottom:1px solid #ccc;">
            
            <div class="detail_search_box" >
                <div class="left">
                    <asp:DropDownList ID="dropCat" runat="server">
                        <asp:ListItem>Tüm Kategorilerde</asp:ListItem>
                        <asp:ListItem>Sayfalarda</asp:ListItem>
                        <asp:ListItem>Haberlerde</asp:ListItem>
                        <%-- <asp:ListItem>Ürünlerde</asp:ListItem>--%>
                        <%--<asp:ListItem>Resimlerde</asp:ListItem>--%>
                    </asp:DropDownList>
                </div>
                <div class="left ayrac" style="font-size: 21px;">| </div>
                <div class="left">
                    <asp:TextBox runat="server" ID="txtDetailSearch" CssClass="TopSearch" value="Ne aradınız?" onfocus="if(this.value=='Ne aradınız?'){this.value=''};" onblur="if(this.value==''){this.value='Ne aradınız?'};"></asp:TextBox>
                </div>
                 <div class="left">
                    <asp:LinkButton runat="server" id="ImgBtnSearch" CssClass="btnSearch form-control" OnClick="ImgBtnSearch_Click1"><img src="<%= Page.ResolveUrl("~/")%>images/search.jpg" /></asp:LinkButton>
                </div>
            </div>
            </div>
         
            
  
    <asp:UpdatePanel ID="UpSearchResults" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
        <ContentTemplate>
            <div class="result" id="tabs">
                <div class="filter">
                    <ul class="categories" style="border-bottom:1px solid #ccc; overflow:hidden">
                        <%-- <li class="active"><a href="#tab1">Sayfalar</a></li>
                        <li><a href="#tab2">Haberler</a></li>
                       <li class="active"><a href="#tab1">Sayfalar</a>--%>
                        
                        <li><asp:LinkButton ID="lnkPages" runat="server" Text="Sayfalar"></asp:LinkButton></li>
                        <li><asp:LinkButton ID="lnkNews" runat="server" Text="Haberler" ClientIDMode="Static"></asp:LinkButton></li>
                        <li><asp:LinkButton ID="lnkProduct" runat="server" Text="Ürünler" Visible="false"></asp:LinkButton></li>
                        <li><asp:LinkButton ID="lnkImages" runat="server" Text="Resimler" Visible="false"></asp:LinkButton></li>
                    </ul>
                    <div class="clear"></div>
                    <div class="result_qty"><asp:Literal ID="ltrlKeyword" runat="server"></asp:Literal> </div>
                </div>
                
                
            <div id="tab1">
                <ul class="result">    
                     <asp:Repeater ID="rptPages" runat="server">
                  <ItemTemplate>
                      <li><a href="<%# "http://" + Request.ServerVariables["SERVER_NAME"] +"/"+ Request.ServerVariables["SCRIPT_NAME"].Split(new char[]{ '/' })[0].ToString() + Eval("PageUrl") %>" class="r_title">
                         
                          <%#(String.IsNullOrEmpty(Eval("Meta_Title").ToString()) ? ""+Eval("PageName")+"" : Eval("Meta_Title"))%>
                          </a>
                          <p>  <%#(String.IsNullOrEmpty(Eval("Meta_Desc").ToString()) ? ""+Ayarlar.OzetCek(Ayarlar.HtmlTagTemizle(Eval("PageContent").ToString()),200)+"" : Eval("Meta_Desc"))%></p>
                          <a href="<%# "http://" + Request.ServerVariables["SERVER_NAME"] +"/"+ Request.ServerVariables["SCRIPT_NAME"].Split(new char[]{ '/' })[0].ToString() + Eval("PageUrl") %>" class="link">
                              <%# "http://" + Request.ServerVariables["SERVER_NAME"] +"/"+ Request.ServerVariables["SCRIPT_NAME"].Split(new char[]{ '/' })[0].ToString()  + Eval("PageUrl") %></a>

                      </li>
                  </ItemTemplate>
                </asp:Repeater>
                </ul>
            </div>
            <div id="tab2">
                <ul class="result">
                <asp:Repeater ID="rptNews" runat="server">
                  <ItemTemplate>
                      <%--<li><a href="<%# "http://" + Request.ServerVariables["SERVER_NAME"] +"/"+ Request.ServerVariables["SCRIPT_NAME"].Split(new char[]{ '/' })[0].ToString() + Eval("PageUrl") %>" class="r_title"><%#Eval("Meta_Title") %></a><p><%#Eval("Meta_Desc") %></p><a href="<%# "http://" + Request.ServerVariables["SERVER_NAME"] +"/"+ Request.ServerVariables["SCRIPT_NAME"].Split(new char[]{ '/' })[0].ToString() + Eval("PageUrl") %>" class="link"><%# "http://" + Request.ServerVariables["SERVER_NAME"] +"/"+ Request.ServerVariables["SCRIPT_NAME"].Split(new char[]{ '/' })[0].ToString()  + Eval("PageUrl") %></a></li>--%>
                    <li>
                      <a class="r_title" href="<%= ResolveUrl("~/") %><%#Ayarlar.UrlSeo(Eval("KategoriAdi").ToString()) %>/<%#Eval("HaberId") %>/<%#Ayarlar.UrlSeo(Eval("Baslik").ToString()) %>"><%#Ayarlar.OzetCek(Eval("Baslik").ToString(), 150) %></a>
                      <p><%#Ayarlar.OzetCek(Eval("Ozet").ToString(), 130) %></p>
                      <a class="link" href="<%= ResolveUrl("~/") %><%#Ayarlar.UrlSeo(Eval("KategoriAdi").ToString()) %>/<%#Eval("HaberId") %>/<%#Ayarlar.UrlSeo(Eval("Baslik").ToString()) %>">
                          <%= ResolveUrl("~/") %><%#Ayarlar.UrlSeo(Eval("KategoriAdi").ToString()) %>/<%#Eval("HaberId") %>/<%#Ayarlar.UrlSeo(Eval("Baslik").ToString()) %>
                      </a>
                    </li>
                  </ItemTemplate>
                </asp:Repeater>
                </ul>
            </div>
            <div id="tab3">
               
                                <div class="text_content product_list">
                <ul class="products">
                               <asp:Repeater ID="rptProducts" runat="server" OnItemDataBound="rpt_Products_ItemDataBound">
                        <ItemTemplate>
                            <li><asp:Image ID="imgNew" runat="server" ImageUrl="~/images/yeni_urun_icon.png" CssClass="new_icon" /><a href="<%= ResolveUrl("~/urunler/") %><%#Ayarlar.UrlSeo(Eval("KatAdi").ToString()) %>/<%#Eval("SeoUrl") %>">
                                <img src="<%=Page.ResolveUrl("~") %>upload/urunler/<%#Eval("ImageURL") %>" />
                                <p class="product_Name"><span><%#Ayarlar.OzetCek(Eval("ProductName").ToString(),40) %></span></p></a>
                                <div class="price"><%# String.Format("{0:0.00}", Eval("UnitPrice")) %> <%#Eval("Currency") %> <span style="font-size:10px;"><asp:Literal ID="ltrlKDV" runat="server"></asp:Literal></span></div>
                               <!-- <a href="<%#Ayarlar.UrlSeo(Eval("KatAdi").ToString()) %>/<%#Eval("SeoUrl") %>" class="button">Ürünü İncele</a> -->
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
                                    </div>
            </div>

                                 <section id="paginations" style="border-top:1px dashed #d3d3d3 !important; float:left; width:100% !important; padding-top:10px; margin-top:20px;">
                                <div class="sayfalama">
                                  <cc1:CollectionPager ID="CollectionPager1" runat="server" BackText=" « Önceki" 
                                    FirstText="İlk" LabelText="" LastText="Son" NextText="Sonraki »" 
                                    PageNumbersDisplay="Numbers" ResultsFormat="Sonuçlar {0}-{1} (Toplam:{2})" ResultsStyle="font-weight:normal; font-size:12px !important; float:left; line-height:2.4; padding-right:10px;"
                                    PageSize="5" SectionPadding="5" BackNextDisplay="Buttons" BackNextLocation="Split" PagingMode="PostBack" 
                                    MaxPages="100000" PageNumbersSeparator=""></cc1:CollectionPager>

                                    
                         </div>
                                  </section>


            </div>
           
             </ContentTemplate>
         <Triggers>
             <asp:AsyncPostBackTrigger ControlID="CollectionPager1" EventName="Click" /> 
             <asp:PostBackTrigger ControlID="lnkNews" />
             <asp:PostBackTrigger ControlID="lnkPages" />
             <asp:PostBackTrigger ControlID="ImgBtnSearch"/>
         </Triggers>
    </asp:UpdatePanel>
            
        </div>
    </div>
                        </div>
                    </div></section>
             
     </div>
    <style>
               .breadcrumb {
        margin: 63px 0px !important;
}
          .contentbox {
        overflow:hidden;
        }
        .overlay {
    background: rgba(0, 172, 168, .7) !important;
}


        div.result ul.result {margin-left:20px;}
        div.result div.filter ul.categories li {
        float:left; border:0px solid #888; margin:10px 20px 0 0; padding:10px; padding-bottom:0; padding-left:0;
        }
            div.result div.filter ul.categories li a.active { border-bottom:3px solid #dd4b39;  display:block; 
            
            }
                div.result div.filter ul.categories li a {
                text-decoration:none; color:#808080; padding:0 10px 10px 10px;
                }
                div.result div.filter ul.categories li a.active  {
                color:#dd4b39; font-weight:bold;
                }

        div.result_qty {
        padding-top:20px; padding-left:10px; padding-bottom:20px;
        }

        div.result ul.result li {
        margin-top:15px;
        }
        div.result ul.result li a.r_title{
        margin-top:20px;
        font-weight:600;
        font-size:16px;
        color:#1a0dab;
        text-decoration:none;
        line-height:1.6;
        }
            div.result ul.result li a.r_title:hover {
            text-decoration:underline;
            }
         div.result ul.result li a.link{
        margin-top:20px;
        color:#006621;
        line-height:1.8;
        text-decoration:none;
        }
        

        div.search_bar {
        background-color:#f7f7f7;  height:75px; padding-top:20px; 
        }

        div.detail_search_box {
            margin-top: 4px;
            background-color: #FFF;
         margin:0px auto;
            width: 450px;
            border-radius: 4px;
            border-top-right-radius: 0;
            border-bottom-right-radius: 0;
            padding: 5px;
            box-shadow: inset 0px 0px 5px #888;
            height:40px;
        }

            div.detail_search_box input, div.detail_search_box select {
                padding: 4px;
                border: 0px solid #f7f7f7;
            }

div.detail_search_box input:focus,
div.detail_search_box select:focus,
div.detail_search_box textarea:focus,
div.detail_search_box button:focus {
    outline: none !important;
}

     [contenteditable="true"]:focus {
    outline: none;
}

        input.TopSearch {
     
        min-width:100px;
        width:295px !important;
        }
        .btnSearch {
         
         position:absolute;
         margin-top:-9px;
        }


        div.search input, div.search select {
        width:inherit;
        }

        .left {
        float:left;
        }


        .sayfalama div > span > a, .sayfalama div > span > b, .sayfalama div > span > input {
    float: left;
    padding: 4px 12px;
    line-height: 20px;
    text-decoration: none;
    background-color: #00aba8;
    border: 1px solid #ddd;
    border-left-width: 0;
}
    </style>

     
</asp:Content>

