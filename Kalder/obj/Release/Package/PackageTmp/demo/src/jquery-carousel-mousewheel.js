(function(window, document, $, undefined) {
    var Mousewheel = function(api) {
        return {
            setup: function() {
                var element = api.$element.get(0);
                var callback = function(element) {
                    var roll = 0;
                    if (element.preventDefault) {
                        element.preventDefault();
                    } else {
                        element.returnValue = !1;
                        element.cancelBubble = !0;
                    }
                    if (element.wheelDelta) {
                        roll = element.wheelDelta / 120;
                    } else if (element.detail) {
                        roll = -element.detail / 3;
                    }
                    if (roll > 0) {
                        api.prev();
                    } else if (roll < 0) {
                        api.next();
                    }
                };
                api.$element.on('focus', function() {
                    if (element.addEventListener) {
                        element.addEventListener('mousewheel', callback, false);
                        element.addEventListener("DOMMouseScroll", callback, false);
                    } else if (element.attachEvent) {
                        element.attachEvent('onmousewheel', callback);
                    }
                }).on('blur', function() {
                    if (element.addEventListener) {
                        element.removeEventListener('mousewheel', callback, false);
                        element.removeEventListener("DOMMouseScroll", callback, false);
                    } else if (element.attachEvent) {
                        element.detachEvent('onmousewheel', callback);
                    }
                });
            }
        };
    };
    $(document).on('carousel::ready', function(event, instance) {
        if (instance.options.mousewheel === true) {
            var mousewheel = Mousewheel(instance);

            mousewheel.setup();
        }
    });
})(window, document, jQuery);
