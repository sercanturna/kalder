﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="error.aspx.cs" Inherits="Kalder.error" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

            /* delay function */
            jQuery.fn.delay = function (time, func) {
                return this.each(function () {
                    setTimeout(func, time);
                });
            };


            jQuery.fn.countDown = function (settings, to) {
                settings = jQuery.extend({
                    startFontSize: '36px',
                    endFontSize: '12px',
                    duration: 1000,
                    startNumber: 10,
                    endNumber: 0,
                    callBack: function () { }
                }, settings);
                return this.each(function () {

                    if (!to && to != settings.endNumber) { to = settings.startNumber; }

                    //set the countdown to the starting number
                    $(this).text(to).css('fontSize', settings.startFontSize);

                    //loopage
                    $(this).animate({
                        'fontSize': settings.endFontSize
                    }, settings.duration, '', function () {
                        if (to > settings.endNumber + 1) {
                            $(this).css('fontSize', settings.startFontSize).text(to - 1).countDown(settings, to - 1);
                        }
                        else {
                            settings.callBack(this);
                        }
                    });

                });
            };

            $('#countdown').countDown({
                startNumber: 10,
                callBack: function (me) {
                    window.location = "default.aspx";
                }
            });

        });
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script type="text/javascript">
        (function () {
            var bsa = document.createElement('script');
            bsa.type = 'text/javascript';
            bsa.async = true;
            bsa.src = '//s3.buysellads.com/ac/bsa.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(bsa);
        })();
</script>

     <div id="screeno"></div>

    <!--======= CONTENT =========-->
		<div class="content search" > 
			<section class="sub-banner" style="margin-top:178px;">
				  <div class="overlay">
					<div class="container">

            <ol id="breadcrumb" class="breadcrumb">
                <li><a href="<%= Page.ResolveUrl("~")%>Anasayfa" title="Ana Sayfa">
                    <%--<img src="<%= Page.ResolveUrl("~/App_Themes/")%><%= Page.Theme %>/images/home-icon.png" alt="Home" class="home" /></a></li>--%>
                <%--<img src="<%= Page.ResolveUrl("~/")%>images/home-icon.png" alt="Home" class="home" />--%>Anasayfa</a></li>
                <li><a href="<%= Page.ResolveUrl("~")%>search" title="Sitede Ara">404 Hata Sayfası</a></li>
                <li>
                    <asp:Literal ID="ltrlnewsCat" runat="server"></asp:Literal></li>
            </ol>
 

         
                        </div>
                      </div>
                </section>
 
            	<section class="innerpages">
				<div class="container"> 
					<div class="row">
					<div class="col-md-12">


    <div class="clear page_container" style="background:url(images/error.png) right top no-repeat;">
        <h1 style="width:100%; text-align:center; font-size:25px; margin-top:20px; margin-bottom:20px;">
            Bu bir 404 hata sayfasıdır!
        </h1>
          <p style="min-height: 350px; text-align: center; font-size:14px; line-height:2; color:#808080; font-family: 'Open Sans', sans-serif;">
            Aradığınız URL sitemizde bulunamadı.
            <br />
            Lütfen ulaşmak istediğiniz sayfayı menü başlıklarına tıklayarak yeniden seçin.
            <br />
                Ana sayfaya yönlendiriliyorsunuz.<br />
		<span id="countdown"></span>
       
      
        </p>
    </div>



                        </div>
                        </div>
                    </div>
                    </section>
            </div>
</asp:Content>
