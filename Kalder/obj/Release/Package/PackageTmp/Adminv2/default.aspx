﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Kalder.Adminv2._default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link href="css/pages/dashboard.css" rel="stylesheet" />


    <%--<script src="./js/charts/donut.js"></script>--%>
    <script src="./js/plugins/flot/jquery.flot.js"></script>
    <script src="./js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="./js/plugins/flot/jquery.flot.resize.js"></script>

    <script type="text/javascript">
        $("#anasayfa").addClass("active");
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="content" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="row">

        <div class="span12">
            <div class="widget stacked">

                <div class="widget-header">
                    <i class="icon-bookmark"></i>
                    <h3>Hızlı Ulaşım</h3>
                </div>
                <!-- /widget-header -->

                <div class="widget-content">
                 
                    <div class="shortcuts">
                        <a href="general_settings.aspx" class="shortcut">
                            <i class="shortcut-icon icon-wrench"></i>
                            <span class="shortcut-label">Ayarlar</span>
                        </a>

                        <a href="E_Bulten.aspx" class="shortcut">
                            <i class="shortcut-icon icon-envelope"></i>
                            <span class="shortcut-label">E-Bültenler</span>
                        </a>

                        <a href="stats.aspx" class="shortcut">
                            <i class="shortcut-icon icon-signal"></i>
                            <span class="shortcut-label">İstatistik</span>
                        </a>

                        <a href="BoardofDirectors.aspx" class="shortcut">
                            <i class="shortcut-icon icon-user"></i>
                            <span class="shortcut-label">Yönetim Kurulu</span>
                        </a>

                        <a href="maillist.aspx" class="shortcut">
                            <i class="shortcut-icon icon-list"></i>
                            <span class="shortcut-label">Maillist</span>
                        </a>

                        <a href="boxes.aspx" class="shortcut">
                            <i class="shortcut-icon icon-link"></i>
                            <span class="shortcut-label">Kısayollar</span>
                        </a>

                        <a href="gallery_categories.aspx" class="shortcut">
                            <i class="shortcut-icon icon-camera"></i>
                            <span class="shortcut-label">Foto Galeri</span>
                        </a>

                        <a href="banners.aspx" class="shortcut">
                            <i class="shortcut-icon icon-bookmark"></i>
                            <span class="shortcut-label">Bannerlar</span>
                        </a>
                        <a href="sliders.aspx" class="shortcut">
                            <i class="shortcut-icon icon-picture"></i>
                            <span class="shortcut-label">Slayt Show</span>
                        </a>
                    </div>
                    <!-- /shortcuts -->

                </div>
                <!-- /widget-content -->

            </div>
            <!-- /widget -->
        </div>

    </div>
    <!-- /row -->

    <div class="row">


        <div class="span6">
            <div class="widget stacked">

                <div class="widget-header">
                    <i class="icon-star"></i>
                    <h3>Site İstatistikleri</h3>
                </div>
                <!-- /widget-header -->

                <div class="widget-content">

                    <div class="stats">
                        <div class="stat">
                            <span class="stat-value">
                                <asp:Literal ID="ltrlOnline" runat="server"></asp:Literal></span>
                            Online Ziyaretçi
                        </div>
                        <!-- /stat -->

                        <div class="stat">
                            <span class="stat-value">
                                <asp:Literal ID="ltrlTotalVisit" runat="server"></asp:Literal></span>
                            Toplam Ziyaretçi
                        </div>
                        <!-- /stat -->

                        <div class="stat">
                            <span class="stat-value">
                                <asp:Literal ID="ltrlUniqeVisit" runat="server"></asp:Literal></span>
                            Tekil Ziyaretçi
                        </div>
                        <!-- /stat -->



                    </div>
                    <!-- /stats -->


                    <div id="chart-stats" class="stats">

                        <div class="stat stat-chart">
                            <!--<div id="donut-chart" class="chart-holder"></div>  #donut -->

                            <asp:Chart ID="Chart1" runat="server" Height="150" Width="300" Palette="None" PaletteCustomColors="Gainsboro; Orange; Crimson; DodgerBlue">

                                <Legends>
                                    <asp:Legend Alignment="Center" Docking="Right" IsTextAutoFit="true" Name="Default" LegendStyle="Table" />
                                </Legends>
                                <Series>
                                    <asp:Series Name="Series1" ChartType="Doughnut"></asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>

                        </div>
                        <!-- /substat -->

                        <div class="stat stat-time">
                            <span class="stat-value">
                                <asp:Literal ID="ltrlOrtalamaSure" runat="server"></asp:Literal></span>
                            Sitede Geçirilen Ortalama Süre
                        </div>
                        <!-- /substat -->

                    </div>
                    <!-- /substats -->

                </div>
                <!-- /widget-content -->

            </div>

            <div class="widget widget-nopad stacked">

                <div class="widget-header">
                    <i class="icon-list-alt"></i>
                    <h3>Son Haberler</h3>
                </div>
                <!-- /widget-header -->

                <div class="widget-content">

                    <ul class="news-items">
                        <asp:Repeater ID="rpt_News" runat="server" OnItemDataBound="rpt_News_OnItemDataBound">
                            <ItemTemplate>
                                <li>
                                    <div class="news-item-detail">
                                        <a href="javascript:;" class="news-item-title"><%#Eval("Baslik") %></a>
                                        <p class="news-item-preview"><%#Ayarlar.OzetCek(Eval("Ozet").ToString(),110) %></p>
                                    </div>

                                    <div class="news-item-date">
                                        <span class="news-item-day">
                                            <asp:Literal ID="ltrlGun" runat="server"></asp:Literal></span>
                                        <span class="news-item-month">
                                            <asp:Literal ID="ltrlAy" runat="server"></asp:Literal></span>
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>

                </div>
                <!-- /widget-content -->

            </div>
            <!-- /widget -->




        </div>
        <!-- /span6 -->


        <div class="span6">


            <div class="widget stacked widget-table action-table">
                <div class="widget-header">
                    <i class="icon-retweet"></i>
                    <h3>Mesajlar</h3>


                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                    <div class="EU_TableScroll">
                        <asp:UpdatePanel ID="upGrid" runat="server">
                            <ContentTemplate>

                                <table id="ProductCat" class="table table-bordered table-striped blank" cellspacing="0" border="0" style="border-collapse: collapse;">
                                    <asp:Repeater ID="rpt_Message" runat="server">
                                        <HeaderTemplate>
                                            <tr>

                                                <th scope="col">Durum</th>
                                                <th scope="col">Adı Soyadı</th>
                                                <th scope="col">Firma Adı</th>


                                                <th scope="col">Konu</th>


                                                <th scope="col">İşlem</th>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>

                                            <tr class="<%#Eval("IsRead") %>">

                                                <td class="center">
                                                    <asp:HiddenField ID="hbItem" runat="server" Value='<%# Eval("MessageId") %>' />
                                                    <%# Eval("Status") == "True" ? Eval("Status") : Eval("Status").ToString().Replace("True", "<img src=\"img/msg_read.png\" />").Replace("False", "<img src=\"img/msg_noread.png\" />")%></td>
                                                <td><%#Eval ("NameSurname") %></td>
                                                <td><%#Ayarlar.OzetCek(Eval ("CompanyName").ToString(),24) %></td>



                                                <td><%#Ayarlar.OzetCek(Eval ("Subject").ToString(),20) %></td>




                                                <td><a href="viewMessage.aspx?mId=<%#Eval("MessageId") %>" class="btn btn-small" data-toggle="modal" data-target="#myModal"><i class="btn-icon-only icon-bell"></i></a></td>
                                            </tr>

                                        </ItemTemplate>
                                    </asp:Repeater>

                                       
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" style="color:#ffffff">Mesaj içeriği</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
              
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



                                    <!--
                                    <tr>
                                        <td colspan="9" style="text-align:center; padding:20px;" ><asp:Label CssClass="alert" ID="ltrlSonuc" runat="server" Text="Henüz bir ürün kategorisi oluşturulmamış."></asp:Label></td>
                                    </tr>
                                    -->

                                </table>

                            </ContentTemplate>
                        </asp:UpdatePanel>



                    </div>
                </div>
            </div>


        </div>
        <!-- /span6 -->


        <div class="span6" style="display:none;">

            <!-- /widget -->

            <div class="widget stacked widget-table action-table">

                <div class="widget-header">
                    <i class="icon-th-list"></i>
                    <h3>Modüller</h3>
                </div>
                <!-- /widget-header -->

                <div class="widget-content">

                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Durum</th>
                                <th>Modül Adı</th>
                                <th class="td-actions"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Aktif</td>
                                <td>Anasayfa Kayan Kutular</td>
                                <td class="td-actions">
                                    <asp:LinkButton ID="lnkAktifEt" runat="server" CssClass="btn btn-small btn-warning"><i class="btn-icon-only icon-ok"></i>	</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-small"><i class="btn-icon-only icon-pause"></i>	</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td>Pasif</td>
                                <td>Anasayfa Kayan Logolar</td>
                                <td class="td-actions">
                                    <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-small btn-warning"><i class="btn-icon-only icon-ok"></i>	</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton3" runat="server" CssClass="btn btn-small"><i class="btn-icon-only icon-pause"></i>	</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td>Aktif</td>
                                <td>Sidebar Hava Durumu</td>
                                <td class="td-actions">
                                    <asp:LinkButton ID="LinkButton4" runat="server" CssClass="btn btn-small btn-warning"><i class="btn-icon-only icon-ok"></i>	</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton5" runat="server" CssClass="btn btn-small"><i class="btn-icon-only icon-pause"></i>	</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td>Pasif</td>
                                <td>Sidebar Döviz Kuru</td>
                                <td class="td-actions">
                                    <asp:LinkButton ID="LinkButton6" runat="server" CssClass="btn btn-small btn-warning"><i class="btn-icon-only icon-ok"></i>	</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton7" runat="server" CssClass="btn btn-small"><i class="btn-icon-only icon-pause"></i>	</asp:LinkButton>
                                </td>
                            </tr>

                            <tr>
                                <td>Pasif</td>
                                <td>E-Bülten</td>
                                <td class="td-actions">
                                    <asp:LinkButton ID="LinkButton10" runat="server" CssClass="btn btn-small btn-warning"><i class="btn-icon-only icon-ok"></i>	</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton11" runat="server" CssClass="btn btn-small"><i class="btn-icon-only icon-pause"></i>	</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td>Pasif</td>
                                <td>Facebook LikeBox</td>
                                <td class="td-actions">
                                    <asp:LinkButton ID="LinkButton8" runat="server" CssClass="btn btn-small btn-warning"><i class="btn-icon-only icon-ok"></i>	</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton9" runat="server" CssClass="btn btn-small"><i class="btn-icon-only icon-pause"></i>	</asp:LinkButton>
                                </td>
                            </tr>
                            <tr>
                                <td>Pasif</td>
                                <td>Twitter Feeds</td>
                                <td class="td-actions">
                                    <asp:LinkButton ID="LinkButton12" runat="server" CssClass="btn btn-small btn-warning"><i class="btn-icon-only icon-ok"></i>	</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton13" runat="server" CssClass="btn btn-small"><i class="btn-icon-only icon-pause"></i>	</asp:LinkButton>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
                <!-- /widget-content -->

            </div>
        </div>
        <!-- /.span6 -->

    </div>
    <!-- /.row -->


    <div class="widget stacked" style="display: none;">

        <div class="widget-header">
            <i class="icon-file"></i>
            <h3>Modüller</h3>
        </div>
        <!-- /widget-header -->

        <div class="widget-content">

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>


            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

        </div>
        <!-- /widget-content -->







    </div>
</asp:Content>
