﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Kalder.Adminv2.login" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" /> 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="./css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="./css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
	<link href="./css/font-awesome.min.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
    <link href="./css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet" />    
    <link href="./css/base-admin-2.css" rel="stylesheet" />
    <link href="./css/base-admin-2-responsive.css" rel="stylesheet" />
    <link href="./css/pages/signin.css" rel="stylesheet" type="text/css" />
    <link href="./css/custom.css" rel="stylesheet" />
    
</head>
<body>
    <form id="form1" runat="server">
   <div class="navbar navbar-inverse navbar-fixed-top">
	
	<div class="navbar-inner">
		
		<div class="container">
			
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<i class="icon-cog"></i>
			</a>
			
			<a class="brand" href="./Default.aspx">
				<asp:Literal ID="LtrlCompanyName" runat="server"></asp:Literal> <sup>v.1.0</sup>				
			</a>		
			
			<div class="nav-collapse">
				<ul class="nav pull-right">
					
					<li class="">						
						<a href="./signup.html" class="">
							Sistemi Satın Al
						</a>
						
					</li>
					
					<li class="">						
						<a href="../" class="">
							<i class="icon-chevron-left"></i>
							Siteye geri dön
						</a>
						
					</li>
				</ul>
				
			</div><!--/.nav-collapse -->	
	
		</div> <!-- /container -->
		
	</div> <!-- /navbar-inner -->
	
</div> <!-- /navbar -->



<div class="account-container stacked">
	
	<div class="content clearfix">
		
		 
		
			<h1>Giriş Yap</h1>		
			
			<div class="login-fields">
				
				<p>Lütfen kullanıcı bilgilerinizi girin:</p>
				
				<div class="field">
					<label for="username">Kullanıcı Adı:</label>
                    <asp:TextBox ID="txtUsername" runat="server" CssClass="login username-field" placeholder="Kullanıcı Adı"></asp:TextBox>
				
				</div> <!-- /field -->
				
				<div class="field">
					<label for="password">Parola:</label>
                     <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" CssClass="login password-field" placeholder="Parola"></asp:TextBox>
				</div> <!-- /password -->
				
			</div> <!-- /login-fields -->
			
			<div class="login-actions">
				
				<span class="login-checkbox">
                    <asp:CheckBox ID="chkRemember" CssClass="field login-checkbox" Text="Beni Hatırla" runat="server" />
					<input id="Field" name="Field" type="checkbox" class="field login-checkbox" value="First Choice" tabindex="4" />
					 
				</span>
									
                <asp:Button ID="btnLogin" runat="server" CssClass="button btn btn-warning btn-large" Text="Giriş Yap" OnClick="btnLogin_Click" />
				
			</div> <!-- .actions -->
			
		 <div class="login-social">
				<asp:Literal ID="ltrlAlert" runat="server"></asp:Literal>
			</div>
			
		 
		
	</div> <!-- /content -->
	
</div> <!-- /account-container -->


<!-- Text Under Box -->
<div class="login-extra">
	Henüz bir hesabınız yok mu? <a href="http://www.cportal.web.tr">Satın Al</a><br />
	Şifremi <a href="#">Unuttum</a>
</div> <!-- /login-extra -->



<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="./js/libs/jquery-1.8.3.min.js"></script>
<script src="./js/libs/jquery-ui-1.10.0.custom.min.js"></script>
<script src="./js/libs/bootstrap.min.js"></script>

<script src="./js/Application.js"></script>

<!--<script src="./js/signin.js"></script>-->
    </form>
</body>
</html>
