﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="maillist.aspx.cs" Inherits="Kalder.Adminv2.maillist" EnableEventValidation="false"  %>
 <%@ Register Assembly="CollectionPager" Namespace="SiteUtils" TagPrefix="cc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
       

    <script type="text/javascript">
        $(function () {
            var $allCheckbox = $('.allCheckbox :checkbox');
            var $checkboxes = $('.singleCheckbox :checkbox');
            $allCheckbox.change(function () {
                if ($allCheckbox.is(':checked')) {
                    $checkboxes.attr('checked', 'checked');
                }
                else {
                    $checkboxes.removeAttr('checked');
                }
            });
            $checkboxes.change(function () {
                if ($checkboxes.not(':checked').length) {
                    $allCheckbox.removeAttr('checked');
                }
                else {
                    $allCheckbox.attr('checked', 'checked');
                }
            });
        });
         
        $("#mesajlar").addClass("active");

        $('#myModal').on('hidden', function () {
           // __doPostBack('up1', '')
              location.reload();
        });
 


        $(function () {
            $('#ProductCat').DataTable({
                dom: 'Bfrtip',
                buttons: [
                      'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                ],

                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Turkish.json"
                }
            });
        });


    </script>
    <style>
        th.allCheckbox {
            width: 20px;
        }

            td.singleCheckbox input, th.allCheckbox input {
                width: 20px !important;
            }
    </style>
     
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <asp:UpdatePanel runat="server" ID="up1">
        <ContentTemplate>
<div class="row">

        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-retweet"></i>
                    <h3>E-Bülten Üyelikleri</h3>
                    <asp:LinkButton ID="btnDeleteAll" OnClick="btnDeleteAll_Onclick" runat="server" CssClass="btn right" BorderStyle="Solid" BorderColor="#606060" BorderWidth="1" ToolTip="Sil" CausesValidation="False" Text="x Sil" OnClientClick="return confirm('Bu Mesajları silmek istediğinize emin misiniz?');"></asp:LinkButton><asp:Button ID="btnExportExel" runat="server" CssClass="btn btn-success right" OnClick="btnExportExel_Click" Text="Excel'e Aktar" />
                   
                    
                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                    <div class="EU_TableScroll">
                        <asp:UpdatePanel ID="upGrid" runat="server">
                            <ContentTemplate>

                                <table id="ProductCat" class="table table-bordered table-striped blank" cellspacing="0" border="1" style="border-collapse: collapse;">
                                   <asp:Repeater ID="rpt_maillist" runat="server">
                                       <HeaderTemplate>
                                           <thead>
                                     <tr>
                                        <th scope="col" class="allCheckbox"><asp:CheckBox ID="allCheckbox1"  runat="server" /></th>
                                        <th scope="col">Adı Soyadı</th>
                                        <th scope="col">E-Posta</th>
                                        <th scope="col">Gönderilme Tarihi</th>
                                    </tr>
                                    </thead>
                                           <tbody>
                                    </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="singleCheckbox">
                                                    <asp:CheckBox ID="chkContainer" runat="server"  />
                                                    <asp:HiddenField ID="hbItem" runat="server" Value='<%# Eval("MaillistId") %>' />
                                                </td>
                                                 <td><%#Eval ("AdSoyad") %></td>
                                                 <td><a href="mailto:<%#Eval ("Email") %>"><%#Eval ("Email") %></a></td>
                                                 <td><%#Eval ("SentDate") %></td>
                                             </tr>
                                            
                                        </ItemTemplate>
                                    </asp:Repeater>
                                        </tbody>
                                </table>

                              <section id="paginations" style="display:none;">
                                <div class="sayfalama">
                                  <cc1:CollectionPager ID="CollectionPager1" runat="server" BackText=" « Önceki" 
                                    FirstText="İlk" LabelText="" LastText="Son" NextText="Sonraki »" 
                                    PageNumbersDisplay="Numbers" ResultsFormat="Sayfalar {0} {1} (Toplam:{2})"
                                    PageSize="10" SectionPadding="5" BackNextDisplay="Buttons" BackNextLocation="Split" 
                                    MaxPages="50000" PageNumbersSeparator=""></cc1:CollectionPager>
                         </div>
                                  </section>
                                        </ContentTemplate>
                        </asp:UpdatePanel>

                        
                        
                    </div>
                </div>
            </div>
        </div>


    </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    


</asp:Content>