﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="Messages.aspx.cs" Inherits="Kalder.Adminv2.Messages" %>
 <%@ Register Assembly="CollectionPager" Namespace="SiteUtils" TagPrefix="cc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
       
        <link href="js/bootstrap-switch.min.css" rel="stylesheet" />
    <script src="js/bootstrap-switch.min.js"></script>
 

    <script type="text/javascript">
        $(function () {
            var $allCheckbox = $('.allCheckbox :checkbox');
            var $checkboxes = $('.singleCheckbox :checkbox');
            $allCheckbox.change(function () {
                if ($allCheckbox.is(':checked')) {
                    $checkboxes.attr('checked', 'checked');
                }
                else {
                    $checkboxes.removeAttr('checked');
                }
            });
            $checkboxes.change(function () {
                if ($checkboxes.not(':checked').length) {
                    $allCheckbox.removeAttr('checked');
                }
                else {
                    $allCheckbox.attr('checked', 'checked');
                }
            });

           


        });
         
        $("#mesajlar").addClass("active");

        $('#myModal').on('hidden', function () {
           // __doPostBack('up1', '')
              location.reload();
        });
 

        var mySwicthControl = "[id='content_IsActive']";
        $("[id = 'content_IsActive']").attr('data-on-text', 'On');
        $("[id = 'content_IsActive']").attr('data-off-text', 'Off');

       $(mySwicthControl).bootstrapSwitch();


        $(mySwicthControl).on('switchChange.bootstrapSwitch', function (event, state) {
         
            if (state == false) {
                    $("#fields :input").each(function () {
                        $(this).prop("disabled", true);
                    })
            } else {
                $("#fields :input").each(function () {
                    $(this).prop("disabled", false);
                })
            }

        });

        function CheckFormState(state)
        {
         
            if (state == "False") {
                $("#fields :input").each(function () {
                    $(this).prop("disabled", true);
                })
            } else {
                $("#fields :input").each(function () {
                    $(this).prop("disabled", false);
                })
            }
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        var mySwicthControl = "[id='content_IsActive']";
        $("[id = 'content_IsActive']").attr('data-on-text', 'On');
        $("[id = 'content_IsActive']").attr('data-off-text', 'Off');


        prm.add_endRequest(function () {

            var $allCheckbox = $('.allCheckbox :checkbox');
            var $checkboxes = $('.singleCheckbox :checkbox');
            $allCheckbox.change(function () {
                if ($allCheckbox.is(':checked')) {
                    $checkboxes.attr('checked', 'checked');
                }
                else {
                    $checkboxes.removeAttr('checked');
                }
            });
            $checkboxes.change(function () {
                if ($checkboxes.not(':checked').length) {
                    $allCheckbox.removeAttr('checked');
                }
                else {
                    $allCheckbox.attr('checked', 'checked');
                }
            });

           
            $(mySwicthControl).bootstrapSwitch();

            $(mySwicthControl).on('switchChange.bootstrapSwitch', function (event, state) {

                if (state == false) {
                    $("#fields :input").each(function () {
                        $(this).prop("disabled", true);
                    })
                } else {
                    $("#fields :input").each(function () {
                        $(this).prop("disabled", false);
                    })
                }

            });


            $('#myModal').on('hidden', function () {
                __doPostBack('up1', '')
                // location.reload();
            });



        });

        $(function () {
            $('#ProductCat').DataTable({
                dom: 'Bfrtip',
                buttons: [
                      'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                ],

                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Turkish.json"
                }
            });
        });


    </script>
    <style>

       

        th.allCheckbox {
            width: 20px;
        }

            td.singleCheckbox input, th.allCheckbox input {
                width: 20px !important;
            }
    </style>
     
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <asp:UpdatePanel runat="server" ID="up1"  ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
<div class="row">

        <div class="span8">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-retweet"></i>
                    <h3>Mesajlar</h3>
                    <asp:LinkButton ID="btnDeleteAll" OnClick="btnDeleteAll_Onclick" runat="server" CssClass="btn right" BorderStyle="Solid" BorderColor="#606060" BorderWidth="1" ToolTip="Sil" CausesValidation="False" Text="x Sil" OnClientClick="return confirm('Bu Mesajları silmek istediğinize emin misiniz?');"></asp:LinkButton>
                   
                    
                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                    <div class="EU_TableScroll">
                        <asp:UpdatePanel ID="upGrid" runat="server">
                            <ContentTemplate>

                                <table id="ProductCat" class="table table-bordered table-striped blank" cellspacing="0" border="1" style="border-collapse: collapse;">
                                   <asp:Repeater ID="rpt_News" runat="server">
                                       <HeaderTemplate>
                                           <thead>
                                     <tr>
                                        <th scope="col" class="allCheckbox"><asp:CheckBox ID="allCheckbox1"  runat="server" /></th>
                                       <th scope="col">Durum</th>
                                        <th scope="col">Adı Soyadı</th>
                                       <%-- <th scope="col">Firma Adı</th>--%>
                                    <%--    <th scope="col">E-Posta</th>--%>
                                        <%--<th scope="col">Telefon</th>--%>
                                        <th scope="col">Konu</th>
                                        <%--<th scope="col">Mesaj</th>--%>
                                        <th scope="col">Gönderilme Tarihi</th>
                                        <th scope="col">İşlem</th>
                                    </tr>
                                                </thead>
                                           <tbody>
                                    </HeaderTemplate>
                                        <ItemTemplate>
                                             
                                            <tr class="<%#Eval("IsRead") %>">
                                                <td class="singleCheckbox">
                                                    <asp:CheckBox ID="chkContainer" runat="server"  />
                                                    <asp:HiddenField ID="hbItem" runat="server" Value='<%# Eval("MessageId") %>' />
                                                </td>
                                               <td class="center"><%# Eval("Status") == "True" ? Eval("Status") : Eval("Status").ToString().Replace("True", "<img src=\"img/msg_read.png\" />").Replace("False", "<img src=\"img/msg_noread.png\" />")%></td> 
                                                 <td><%#Eval ("NameSurname") %></td>
                                                 <%--<td><%#Eval ("CompanyName") %></td>--%>
                                               <%--  <td><a href="mailto:<%#Eval ("Email") %>&amp;Subject=<%#Eval("Subject") %>&amp;body=<%#Eval("Content") %>"><%#Eval ("Email") %></a></td>--%>
                                               <%-- <td><%#Eval ("PhoneNumber") %></td>--%>
                                                <td><%#Ayarlar.OzetCek(Eval ("Subject").ToString(),20) %></td>
                                               <%-- <td><%# Ayarlar.OzetCek(Eval("Content").ToString(),20) %></td>--%>
                                              
                                                 <td><%#Eval ("SentDate") %></td>
                                               
                                                 <td><a href="viewMessage.aspx?mId=<%#Eval("MessageId") %>" class="btn btn-small" data-toggle="modal" data-target="#myModal"><i class="btn-icon-only icon-bell"></i></a></td>
                                             </tr>
                                            
                                        </ItemTemplate>
                                    </asp:Repeater>

                                    <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title" style="color:#ffffff">Mesaj içeriği</h4>

            </div>
            <div class="modal-body"><div class="te"></div></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
              
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
                                  
                                    <!--
                                    <tr>
                                        <td colspan="9" style="text-align:center; padding:20px;" ><asp:Label CssClass="alert" ID="ltrlSonuc" runat="server" Text="Henüz bir ürün kategorisi oluşturulmamış."></asp:Label></td>
                                    </tr>
                                    -->
                                        </tbody>
                                </table>
                              <section id="paginations">
                                <div class="sayfalama">
                                  <cc1:CollectionPager ID="CollectionPager1" runat="server" BackText=" « Önceki" 
                                    FirstText="İlk" LabelText="" LastText="Son" NextText="Sonraki »" 
                                    PageNumbersDisplay="Numbers" ResultsFormat="Sayfalar {0} {1} (Toplam:{2})"
                                    PageSize="10" SectionPadding="5" BackNextDisplay="Buttons" BackNextLocation="Split" 
                                    MaxPages="50000" PageNumbersSeparator=""></cc1:CollectionPager>
                         </div>
                                  </section>
                                        </ContentTemplate>
                        </asp:UpdatePanel>

                        
                        
                    </div>
                </div>
            </div>
             
        </div>



    	<div class="span4">
			
			
			<div class="widget stacked widget-box">
				
				<div class="widget-header">
                    <i class="icon-envelope"></i>
      				<h3>Mail Ayarları</h3>
                   <div class="pull-right" style="width:70px;"> 
                       	<asp:UpdatePanel ID="UpdatePanel1" runat="server" >
                                            <ContentTemplate>

                                           
									 <input type="checkbox" data-off-color="warning" data-size="normal" runat="server" id="IsActive" />
                                                 </ContentTemplate>
                          <Triggers>
          
            <asp:AsyncPostBackTrigger ControlID="IsActive" />
        </Triggers>
										</asp:UpdatePanel>
                      

                   </div>
  				</div> <!-- /widget-header -->
				
				<div class="widget-content">
					<p class="help-block">Bu mesajları aynı zamanda e-posta yoluyla almak için lütfen aşağıdaki bilgileri tanımlayın.</p><br />
					
                    <div id="edit-profile2" class="form-inline" />
									<fieldset id="fields">

                                        <div class="control-group">
                                        <label class="control-label" for="emailserver">Alıcı Adres</label>
											<div class="controls">
												<asp:TextBox CssClass="input-large" id="TxtAlici" placeholder="ali@alanadı.com" runat="server"></asp:TextBox>
                                                <p class="help-block">E-posta hangi adrese gönderilsin?</p>
											</div>
										</div>
                                        

                                        <div class="control-group">
                                        <label class="control-label">Smtp Server</label>
											<div class="controls">
												<asp:TextBox CssClass="input-large" id="TxtEmailServer" placeholder="mail.alanadı.com" runat="server"></asp:TextBox>
											</div>
										</div>
										<div class="control-group">
											<label class="control-label" >Smtp Kullanıcı Adı</label>
											<div class="controls">
												<asp:TextBox CssClass="input-large" id="TxtSmtpUser" placeholder="info@alanadınız.com" runat="server"></asp:TextBox>
											</div>
										</div>
									
										<div class="control-group">
											<label class="control-label">SMTP Parola</label>
											<div class="controls">
												<asp:TextBox CssClass="input-large" id="txtSmtpPass" TextMode="Password" runat="server"></asp:TextBox>
                                                <asp:HiddenField ID="hdnPass" runat="server" />
											</div>
										</div>
										
														
										<asp:UpdatePanel ID="up2" runat="server">
                                            <ContentTemplate>

                                           
										<div class="form-actions">
											<asp:Button id="btnKaydet" runat="server" CssClass="btn btn-primary" Text="Kaydet" OnClick="btnKaydet_Click"></asp:Button>
										</div>
                                                 </ContentTemplate>
                         
										</asp:UpdatePanel>
									</fieldset>
								</form>

				</div> <!-- /widget-content -->
				
			</div> <!-- /widget-box -->
								
	      </div> <!-- /span4 -->

    </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnKaydet" />
            <asp:AsyncPostBackTrigger ControlID="IsActive" />
        </Triggers>
    </asp:UpdatePanel>
    

    


</asp:Content>