﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="edit_currency.aspx.cs" Inherits="Kalder.Adminv2.edit_currency" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="server">
    <asp:ScriptManager ID="sm1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>


    <div class="row">
        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-money"></i>
                    <h3>Döviz Modülü Düzenle</h3>
                </div>
                <!-- /widget-header -->


                <div class="widget-content">
                    <table class="table table-bordered table-striped table-highlight">
                        <thead>
                        <tr>
                            <th ><strong>Seçimler</strong></th>
                            <th ><strong>Döviz Kodu</strong></th>
                            <th ><strong>Birim</strong></th>
                            <th ><strong>Döviz Cinsi</strong></th>
                        </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="rptCurrency" runat="server" OnItemDataBound="rptCurrency_ItemDataBound">
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                            
                                                    <asp:CheckBox ID="chk" runat="server" OnCheckedChanged="chk_CheckedChanged" AutoPostBack="true" />
                                            
                                        </td>
                                        <td class="para kurkodu" ><span class="kurgorsel"><asp:Literal ID="ltrlImage" runat="server"></asp:Literal></span><%#Eval("Currency") %>/TRY</td>
                                        <td class="para birim"><%#Eval("Unit") %></td>
                                        <td class="para"><%#Eval("CurrencyName") %></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>

                   
                            </tbody>
                    </table>
                     
                    </div>
                </div>
            </div>
        </div>
            </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress AssociatedUpdatePanelID="up1" runat="server">
        <ProgressTemplate>
            <div class="progress_template">
                <img src="img/loader.gif" /><br /><h1 class="help-block">İŞLEM GERÇEKLEŞTİRİLİYOR!</h1>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="css/checkbox.css" rel="stylesheet" />
</asp:Content>
