﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="edit_news.aspx.cs" Inherits="Kalder.Adminv2.edit_news" %>


<%--<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit.HTMLEditor" tagprefix="cc1" %>--%>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="CKFinder" Namespace="CKFinder" TagPrefix="CKFinder" %>


<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">

     <link href="css/custom.css" rel="stylesheet" />


    <script src="js/multi.js" type="text/javascript"></script>
    <link href="./js/tags/jquery.tagsinput.css" rel="stylesheet" />
    <script src="./js/tags/jquery.tagsinput.js"></script>
 

        <script src="js/validation/jquery.validationEngine-tr.js"></script>
    <script src="js/validation/jquery.validationEngine.js"></script>
   <link href="js/validation/validationEngine.jquery.css" rel="stylesheet" /> 


    <style type="text/css">
        .style1 {
            width: 100%;
        }

        .style2 {
            width: 183px;
            font-family: Verdana;
            font-size: 13px;
            font-weight: bold;
            color: #646464;
        }

        ul.resimler {
        }

            ul.resimler li {
                height: 280px;
            }
    </style>


    <script src="../ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="../ckfinder/ckfinder.js" type="text/javascript"></script>

<%--<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>--%>

<link href="css/bootstrap.fd.css" rel="stylesheet" />
<script src="js/bootstrap.fd.js"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            $('#tags').tagsInput({
                width: 'auto',
            });



            var editor = CKEDITOR.replace('<% = txtDetay.ClientID %>');
            CKFinder.setupCKEditor(editor, '../ckfinder');

            var content_editor = editor;

            content_editor.on('key', function () {
                change_page = true;
            });


            $("#haberler").addClass("active");

              $("#form1").validationEngine('attach', { promptPosition: "topRight", scroll: true });
        });
        function fn_init() {

            $("#form1").validationEngine('attach', { promptPosition: "topRight", scroll: true });


            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_initializeRequest(onEachRequest);


            function onEachRequest(sender, args) {
                if ($("#form1").validationEngine() == false) {
                    args.set_cancel(true);
                }
            }

        }
        function SumChar(val) {
            var len = val.value.length;
            if (len >= 201) {
                val.value = val.value.substring(0, 200);
            } else {
                $('#sumdesc').text(200 - len + " Karakter");
            }
        };

        function DescChar(val) {
            var len = val.value.length;
            if (len >= 201) {
                val.value = val.value.substring(0, 200);
            } else {
                $('#desc').text(200 - len + " Karakter");
            }
        };
        function TitleChar(val) {
            var len = val.value.length;
            if (len >= 70) {
                val.value = val.value.substring(0, 70);
            } else {
                $('#title').text(70 - len + " Karakter");
            }
        };
        $(function () {
            $('#content_tags').tagsInput({
                width: 'auto',
            });
        });


    </script>

    <script>

        $(function pageLoad() {
            $("#open_btn").click(function () {

                $.FileDialog({ multiple: true }).on('files.bs.filedialog', function (ev) {
                    var files = ev.files;

                    var data = new FormData();
                    for (var i = 0; i < files.length; i++) {
                        data.append(files[i].name, files[i]);
                    }

                    $.ajax({
                        url: "FileUpHandlerNews.ashx?HaberId=<%= Request.QueryString["hId"] %>",
                        type: "POST",
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (result) {
                            // alert(result);

                            var UpdatePanel1 = '<%=upResim.ClientID%>';

                            function ShowItems() {
                                if (UpdatePanel1 != null) {
                                    __doPostBack(UpdatePanel1, '');
                                }
                            }
                            SuccessWithMsg("Resim(ler) başarıyla yüklendi. Şayet görüntülenmiyorsa sayfayı yenileyin.")
                            ShowItems();
                        },
                        error: function (err) {
                            alert(err.statusText)
                        }
                    });
                }).on('cancel.bs.filedialog', function (ev) {
                    CancelWithMsg("Yükleme işlemini iptal ettiniz! \r Haber resimlerinde herhangi bir değişiklik yapılmadı.");
                });
            });

        });

        //Sayfa Postback olduğunda Update panel içerisindeki jquery çalışm ıyor bu nedenle prm adında bir request oluşturup sayfayı yeniden tetikletiyorum.
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            $("#open_btn").click(function () {

                $.FileDialog({ multiple: true }).on('files.bs.filedialog', function (ev) {
                    var files = ev.files;

                    var data = new FormData();
                    for (var i = 0; i < files.length; i++) {
                        data.append(files[i].name, files[i]);
                    }

                    $.ajax({
                        url: "FileUpHandlerNews.ashx?HaberId=<%= Request.QueryString["hId"] %>",
                        type: "POST",
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (result) {
                            // alert(result);

                            var UpdatePanel1 = '<%=upResim.ClientID%>';

                            function ShowItems() {
                                if (UpdatePanel1 != null) {
                                    __doPostBack(UpdatePanel1, '');
                                }
                            }
                            SuccessWithMsg("Resim(ler) başarıyla yüklendi. Şayet görüntülenmiyorsa sayfayı yenileyin.")
                            ShowItems();
                        },
                        error: function (err) {
                            alert(err.statusText)
                        }
                    });
                }).on('cancel.bs.filedialog', function (ev) {
                    CancelWithMsg("Yükleme işlemini iptal ettiniz! \r Haber resimlerinde herhangi bir değişiklik yapılmadı.");
                });
            });

        });
    </script>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>


    <asp:Literal ID="hdScript" runat="server"></asp:Literal>
    <div class="row">
        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-globe"></i>
                    <h3>Haber Güncelle</h3>
                </div>
                <!-- /widget-header -->


                <div class="widget-content">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#genel" data-toggle="tab">Genel Bilgiler</a></li>
                            <li class=""><a href="#resimler" data-toggle="tab">Resim Galerisi</a></li>
                            <li class=""><a href="#seo" data-toggle="tab">SEO</a></li>

                        </ul>

                        <br />

                        <div class="tab-content">
                            <div class="tab-pane active" id="genel">
                                <asp:UpdatePanel ID="upGenel" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>

                                        <div id="edit-genel" class="form-horizontal">
                                            <div class="control-group">
                                                <label class="control-label" for="dropKat">Haber Kategorisi</label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="dropKat" runat="server"></asp:DropDownList>
                                                    <%--<p class="help-block">* Haberi ekleyeceğiniz kategoriyi seçin.</p>--%>
                                                </div>

                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->

                                            <div class="control-group">
                                                <label class="control-label" for="txtBaslik">Başlık</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtBaslik" runat="server" Width="300px"></asp:TextBox>
                                                </div>

                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->

                                            <div class="control-group">
                                                <label class="control-label" for="txtOzet">Özet</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtOzet" runat="server" Height="60px" TextMode="MultiLine" Width="400px" onkeyup="SumChar(this)"></asp:TextBox><div class="char" id="sumdesc"></div>
                                                    <p class="help-block">* Haber içeriğinin bir kısmını anlatın..</p>
                                                </div>

                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->

                                            <div class="control-group">
                                                <label class="control-label" for="txtDetay">Detay</label>
                                                <div class="controls">
                                                    <CKEditor:CKEditorControl ID="txtDetay" runat="server">
                                                    </CKEditor:CKEditorControl>
                                                    <p class="help-block">* Haber içeriğinin tamamını bu alanda yapılandırın.</p>
                                                </div>

                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->

                                            <%--       <div class="control-group">
                                                <label class="control-label" for="fuResim">Manşet Resmi</label>
                                                <div class="controls">
                                                    <asp:FileUpload ID="fuResim" runat="server" />
                                                    <p class="help-block">* Burada seçilecek resmin 4x3 formatında olması gerekli.</p>
                                                </div>
                                                <div class="controls">
                                                    <li>
                                                        <asp:Image ID="imgManset" runat="server" />
                                                        <span>
                                                            <asp:LinkButton ID="lnkDeleteCuffImage" OnClick="lnkDeleteCuffImage_Click" runat="server" CssClass="btn btn-small" Text="Sil" OnClientClick="return confirm('Bu resmi silmek istediğinize emin misiniz?');"></asp:LinkButton></span>
                                                    </li>

                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->--%>

                                            <%--                                            <div class="control-group">
                                                <label class="control-label" for="fuDetayResim">Resim galerisi</label>
                                                <div class="controls">
                                                    <asp:FileUpload ID="fuDetayResim" runat="server" CssClass="multi" />
                                                    <p class="help-block">* Habere ait fotoğraf galerisi oluşturabilirsiniz.</p>
                                                </div>
                                                <div class="controls">
                                                    <span style="color: #ff6a00;">
                                                        <asp:Literal ID="ltrlAlert" runat="server" Visible="false"></asp:Literal></span>
                                                    <ul class="gallery-container news-gallery">
                                                        <asp:Repeater ID="rptResimler" runat="server" OnItemCommand="rptResimler_ItemCommand">
                                                            <ItemTemplate>
                                                                <li>
                                                                    <img src="../upload/HaberResimleri/FotoGaleri/thumb/<%#Eval("Resim") %>" />
                                                                    <span>
                                                                        <asp:LinkButton ID="lnkDelete" CommandName="del" CommandArgument='<%#Eval ("ResimId") %>' runat="server" CssClass="btn btn-small" Text="Sil" OnClientClick="return confirm('Bu resmi silmek istediğinize emin misiniz?');"></asp:LinkButton></span>
                                                                </li>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </ul>
                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->--%>

                                            <div class="control-group">
                                                <label class="control-label" for="fuResim">Video</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtVideo" runat="server" Height="50px" TextMode="MultiLine" Width="400px"></asp:TextBox>
                                                    <p class="help-block">* Youtube dan alınan embed kodunu yapıştırın eğer yoksa boş bırakın.</p>
                                                </div>

                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->

                                            <div class="control-group">
                                                <label class="control-label" for="chkOnay">Durum</label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="drpDurum" runat="server">
                                                        <asp:ListItem Value="">-Seçiniz-</asp:ListItem>
                                                        <asp:ListItem Value="1">Aktif</asp:ListItem>
                                                        <asp:ListItem Value="0">Pasif</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <p class="help-block">* Durumu Pasif yaparsanız haber sitede gözükmez.</p>
                                                </div>

                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->

                                            <div class="control-group">
                                                <label class="control-label" for="chkManset">Manşet Haberi</label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="dropManset" runat="server">
                                                        <asp:ListItem Value="">-Seçiniz-</asp:ListItem>
                                                        <asp:ListItem Value="1">Evet</asp:ListItem>
                                                        <asp:ListItem Value="0">Hayır</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <p class="help-block">* Seçili olursa haber manşete taşınacak.</p>
                                                </div>

                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->

                                            <div class="control-group">
                                                <label class="control-label" for="chkManset">Sıra No</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtSiraNo" runat="server"></asp:TextBox>
                                                    <p class="help-block">* Haberin sitedeki sırasını belirtin. Eğer istemiyorsanız boş bırakın</p>
                                                </div>

                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->
                                            </div>

                                            <div class="form-actions">
                                                <asp:Button ID="btn_HaberGuncelle" runat="server" CssClass="btn btn-primary" OnClick="btn_HaberGuncelle_Click" Text="Güncelle" />
                                                <asp:Button ID="btn_Temizle" runat="server" CssClass="btn" Text="Temizle" />
                                            </div>
                                            <!-- /form-actions -->
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btn_HaberGuncelle" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            
                                </div><!-- / genel -->

                              <div class="tab-pane" id="resimler">
                                   <asp:UpdatePanel ID="upResim" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>  

                        <div id="edit-resimler" class="form-horizontal">	
                                  
                        <div class="control-group">
                            <label class="control-label" for="fuResim">Haber Resimleri</label>
                            <div class="controls">
                                 
                                    <input type="button" id="open_btn" class="btn btn-primary" value="upload files" />
                                
                                <%--<asp:AsyncFileUpload ID="fuResim" runat="server" />--%>
                                <p class="help-block">* Habere ait resim galerisi oluşturun. Aynı anda birden fazla resim yükleyebilirsiniz.</p>
                            </div>
        
                            <!-- /controls -->
                        </div>
                        <!-- /control-group -->
                            
                           

                                    <ul class="gallery-container">
                                    <asp:Repeater ID="rptResimler" runat="server" OnItemCommand="rptResimler_ItemCommand" OnItemDataBound="rptResimler_ItemDataBound">

                                    <ItemTemplate>
                                        <li>
                                            <a href="../upload/HaberResimleri/FotoGaleri/big/<%#Eval("Resim") %>" class="ui-lightbox">
                                                <img src="../upload/HaberResimleri/FotoGaleri/thumb/<%#Eval("Resim") %>" />
                                            </a>
                                            <a href="../upload/HaberResimleri/FotoGaleri/big/<%#Eval("Resim") %>" class="preview"></a>

                                             <asp:UpdatePanel ID="uplnkUpdate" runat="server" >
                                                <ContentTemplate> 
                                                        <asp:LinkButton ID="lnkUpdate" CommandName="setPrimary" CommandArgument='<%#Eval ("ResimId") %>' runat="server" CssClass="btn btn-small btn-primary" Text="Varsayılan Yap"></asp:LinkButton></span>
                                                        <asp:LinkButton ID="lnkDelete" CommandName="del" CommandArgument='<%#Eval ("ResimId") %>' runat="server" CssClass="btn btn-small" Text="Sil" OnClientClick="return confirm('Bu resmi silmek istediğinize emin misiniz?');"></asp:LinkButton></span>
                                               </ContentTemplate> 
                                               <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="lnkUpdate" EventName="Click" />
                                                    <asp:AsyncPostBackTrigger ControlID="lnkDelete" EventName="Click" />
                                                </Triggers>
                                            </asp:UpdatePanel> 
                                                 

                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                                        </ul>
                                </div>
                                 </ContentTemplate>
                                </asp:UpdatePanel> 
                                     
                                
                                
                                </div>

                            <div class="tab-pane" id="seo">
                                <div id="edit-profile2" class="form-horizontal">	
                                  
                             <%--         <div class="control-group">											
										<label class="control-label" for="vergidaire">Seo URL</label>
										<div class="controls">
                                            <asp:TextBox ID="txtSeoUrl" TextMode="SingleLine" CssClass="input-xlarge" runat="server" ></asp:TextBox><div class="char" id="Div1"></div>
										 <p class="help-block">* Google ve diğer arama motorları için adres çubuğunda gözükecek kategoriniz için bir URL belirleyin.</p>
                                        </div> <!-- /controls -->				
									</div> <!-- /control-group -->--%>


                                       <div class="control-group">											
										<label class="control-label" for="vergidaire">Meta Title</label>
										<div class="controls">
									 
                                            <asp:TextBox ID="txtTitle" TextMode="SingleLine" CssClass="input-xlarge" onkeyup="TitleChar(this)" runat="server"></asp:TextBox><div class="char" id="title"></div>
										 <p class="help-block">* Google ve diğer arama motorları için bir başlık yazın.</p>
                                        </div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    <div class="control-group">											
										<label class="control-label" for="vergidaire">Meta Description</label>
										<div class="controls">
                                            <asp:TextBox ID="txtDescription" TextMode="MultiLine" CssClass="input-xlarge" onkeyup="DescChar(this)" runat="server"></asp:TextBox><div class="char" id="desc"></div>
                                            <p class="help-block">* Google ve diğer arama motorları için bir açıklama yazın.</p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    <div class="control-group">											
										<label class="control-label" for="vergidaire">Meta Keywords</label>
										<div class="controls">                                            
                                            <asp:TextBox ID="tags"  CssClass="input-medium" runat="server"></asp:TextBox>
                                            <p class="help-block">* Google ve diğer arama motorları için anahtar kelimeler yazın. <br />Kelimeler arasına virgül koyarak yada Enter tuşuna basarak geçiş yapabilirsiniz.</p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->


                                     <div class="control-group">											
										<label class="control-label" for="vergidaire">Okunma Sayısı</label>
										<div class="controls">                                            
                                            <asp:TextBox ID="txtHit"  CssClass="input-medium" runat="server"></asp:TextBox>
                                            <p class="help-block">* Haberin sitenizde kaç kere okunduğunu gösteren değerdir.</p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->


                                    </div>
							 </div>
							
                         </div>

                    </div><!-- /tabbable -->
                </div><!-- /widget-content -->
            </div><!-- /widget stacked -->
        </div><!-- /span12 -->
    </div><!-- /row -->


</asp:Content>
