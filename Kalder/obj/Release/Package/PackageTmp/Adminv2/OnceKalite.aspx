﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="OnceKalite.aspx.cs" Inherits="Kalder.Adminv2.OnceKalite" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    

    <script type="text/javascript">
        function toogleSelectionUsingHeaderCheckbox(source) {
            $("#content_GridView1 input[name$='cbDelete']").each(function () {
                $(this).attr('checked', source.checked);
            });
        }

        function toogleSelectionOfHeaderCheckbox() {
            var Checkboxs = $("#content_GridView1 input[name$='cbDelete']").length;
            var CheckSelected = $("#content_GridView1 input[name$='cbDelete']:checked").length;
            if (Checkboxs == CheckSelected) {
                $("#content_GridView1 input[name$='cbDeleteHeader']").attr('checked', true);
            }
            else {
                $("#content_GridView1 input[name$='cbDeleteHeader']").attr('checked', false);
            }
        }
        
                      $(document).load(function () {
                          $("#content_GridView1_btnDeleteAll").click(function () {
                              var RowSelected = $("#content_GridView1 input[name$='cbDelete']:checked").length;
                              if (RowSelected == 0) {
                                  alert('Hiçbir kayıt seçmediniz');
                                  return false;
                              }
                              else
                                  return confirm("Seçtiğiniz " + RowSelected + ' kayıt silinecek, onaylıyor musunuz?');
                          });
                      });

                   
                  $("#dergi").addClass("active");
 

    </script>
    <style>
          .sercan {
        vertical-align:top; margin-top:0px;
        }

        .showmenu label, .showmenu input{
        float:left; margin-right:5px;
        }
    </style>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <div class="row">

        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-book"></i>
                    <h3>Önce Kalite Dergisi Yönetimi</h3>
                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                <div style="border-bottom:1px dashed #808080; margin-bottom:10px; display:none;">
                        <label style="float:left; line-height:2; padding-right:5px;">Başlık :</label><asp:TextBox ID="txtKutuBaslik" runat="server" CssClass="left"></asp:TextBox> <asp:Button ID="btnBaslik" runat="server" Text="Güncelle" CssClass="btn btn-success btn-primary left sercan" OnClick="btnBaslik_Click" />
                  
                    </div>
                    <div class="EU_TableScroll">
                        <asp:UpdatePanel ID="upGrid" runat="server" UpdateMode="Always">
                            <ContentTemplate>

                    
                        <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-striped" AllowPaging="True" PageSize="5"
                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="DergiId" DataSourceID="OnceKaliteDergisiDataSource" ShowFooter="True"
                            OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowUpdating="GridView1_RowUpdating" FooterStyle-CssClass="Ekle" EditRowStyle-BackColor="SlateGray">

                            <Columns>
                                <asp:TemplateField HeaderText="Link">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="cbDeleteHeader" onclick="toogleSelectionUsingHeaderCheckbox(this);" runat="server" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbDelete" onclick="toogleSelectionOfHeaderCheckbox();" runat="server" />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:LinkButton ID="btnDeleteAll" OnClick="btnDeleteAll_Onclick" ToolTip="Seçilen Kayıtları Sil" runat="server" Text="Sil"></asp:LinkButton>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="DergiId" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="DergiId" />



                                
                                <asp:TemplateField HeaderText="Yayın Tarihi" SortExpression="Aciklama">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtYil" runat="server" Text='<%# Bind("Yil") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqYilName" ValidationGroup="Edit" ControlToValidate="txtYil" runat="server" Text="*" ForeColor="Red" ErrorMessage="Yayın yılı yazmalısınız (Örn; 2016)!"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="YilLabel" runat="server" Text='<%# Bind("Yil") %>'></asp:Label>
                                    </ItemTemplate>
                                     <FooterTemplate>
                                        <asp:TextBox ID="txtYilEkle" runat="server" ToolTip="Yayın yılını yazın."></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqYilEkleName" ValidationGroup="Insert" ControlToValidate="txtYilEkle" runat="server" Text="*" ForeColor="Red" ErrorMessage="Yayın yılı yazmalısınız (Örn; 2016)!"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>




                                <asp:TemplateField HeaderText="Açıklama" SortExpression="Aciklama">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtKatAdi" runat="server" Text='<%# Bind("Aciklama") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqCatName" ValidationGroup="Edit" ControlToValidate="txtKatAdi" runat="server" Text="*" ForeColor="Red" ErrorMessage="Bir Firma Adı Yazmalısınız!"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Aciklama") %>'></asp:Label>
                                    </ItemTemplate>
                                     <FooterTemplate>
                                        <asp:TextBox ID="txtKatAdiEkle" runat="server" ToolTip="Referans firmanın adını yazınız."></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqName" ValidationGroup="Insert" ControlToValidate="txtKatAdiEkle" runat="server" Text="*" ForeColor="Red" ErrorMessage="Bir Firma Adı Yazmalısınız!"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>




                                <asp:TemplateField HeaderText="Resim" SortExpression="Resim">
                                    <EditItemTemplate>
                                        <asp:FileUpload ID="fuKatResim" runat="server" />
                                        <%--<asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("Resim") %>'></asp:TextBox>--%>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Image ID="img1" runat="server" CssClass="img-polaroid img-small" ImageUrl='<%# "~/upload/Dergi/" + Eval("Resim") %>' Height="80" Width="60" />
                                        <%--<asp:Label ID="Label2" runat="server" Text='<%# Bind("Resim") %>'></asp:Label>--%>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:FileUpload ID="fuKatResimEkle" runat="server" ToolTip="Referansınız için bir logo yükleyebilirsiniz." />
                                        <asp:RequiredFieldValidator ID="reqImage" ValidationGroup="Insert" ControlToValidate="fuKatResimEkle" runat="server" Text="*" ForeColor="Red" ErrorMessage="Referans için bir logo yüklemelisiniz!"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Durum" SortExpression="Status">
                                    <EditItemTemplate>
                                        <asp:CheckBox ID="chkYayin" runat="server" Checked='<%# Bind("Status") %>' />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkYayin" runat="server" Checked='<%# Bind("Status") %>' Enabled="false" />
                                    </ItemTemplate>
                                     <FooterTemplate>
                                        <asp:CheckBox ID="chkDurumEkle" runat="server" Enabled="true" ToolTip="Referansı yayından kaldırır, istediğiniz zaman tekrar yayınlayabilirsiniz." />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sıra No" SortExpression="OrderNo">
                                    <EditItemTemplate>
                                         <asp:TextBox ID="txtSiraNo" runat="server" Text='<%# Bind("OrderNo") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqOrderNo" ValidationGroup="Edit" ControlToValidate="txtSiraNo" runat="server" Text="*" ForeColor="Red" ErrorMessage="Bir Sıra numarası Girmelisiniz!"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("OrderNo") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtSiraNoEkle" runat="server" ToolTip="Sitedeki sıralamayı belirtir. Bu sayede menü yada sayfa içeriğinde belirttiğiniz sırada gözükür."></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqInsertOrderNo" ValidationGroup="Insert" ControlToValidate="txtSiraNoEkle" runat="server" Text="*" ForeColor="Red" ErrorMessage="Bir Sıra numarası Girmelisiniz!"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Link" SortExpression="Link">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtRefURL" runat="server" Text='<%# Bind("Link") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("Link") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtRefURL" runat="server" ToolTip="Referans siteye link verin!. Örn: http://google.com"></asp:TextBox>
                                    </FooterTemplate>
                                </asp:TemplateField>

                               <asp:TemplateField HeaderText="İşlem" ConvertEmptyStringToNull="False">
                                    <EditItemTemplate>
                                        <asp:UpdatePanel ID="upOnayla" runat="server">
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="LinkButton1" />
                                            </Triggers>
                                            <ContentTemplate>
                                                <asp:LinkButton ID="LinkButton1" runat="server" ValidationGroup="Edit" CssClass="btn btn-small btn-warning" ToolTip="Onayla" CausesValidation="True" CommandName="Update" Text="">
                                            <i class="btn-icon-only icon-ok"></i>	
                                                </asp:LinkButton>
                                                &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-small" ToolTip="İptal" CausesValidation="False" CommandName="Cancel" Text="İptal">
                                             <i class="btn-icon-only icon-circle-arrow-left"></i>
                                                </asp:LinkButton>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-small" ToolTip="Düzenle" CausesValidation="true" CommandName="Edit" Text="">
                                             <i class="btn-icon-only icon-pencil"></i>	
                                        </asp:LinkButton>
                                        &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-small" ToolTip="Sil" CausesValidation="False" CommandName="Delete" Text="" OnClientClick="return confirm('Bu referansı silmek istediğinize emin misiniz?');">
                                            <i class="btn-icon-only icon-remove"></i>
                                        </asp:LinkButton>
                                        &nbsp;<!--
                                        <asp:LinkButton ID="HyperLink1" runat="server" CssClass="btn btn-small btn-warning" PostBackUrl='<%# Eval("DergiId", "images.aspx?GId={0}") %>' ToolTip="Resim Ekle">
                                            <i class="btn-icon-only icon-plus"></i>
                                        </asp:LinkButton>-->
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:UpdatePanel ID="upEkle" runat="server">
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkEkle" />
                                            </Triggers>
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lnkEkle" runat="server" OnClick="lnkEkle_Click" ValidationGroup="Insert" CausesValidation="true" Text="Ekle" CssClass="btn btn-success btn-primary"></asp:LinkButton>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </FooterTemplate>
                                </asp:TemplateField>



                            </Columns>

                            <EmptyDataTemplate>
                                
                                <table class="table table-bordered table-striped blank" cellspacing="0" border="1" style="border-collapse: collapse;">
                                    <tr>
                                        <th scope="col"> - </th>
                                        <th scope="col">ID</th>
                                        <th scope="col">Yayin Tarihi</th>
                                        <th scope="col">Aciklama</th>
                                        <th scope="col">Resim</th>
                                        <th scope="col">Durum</th>
                                        <th scope="col">Sıra No</th>
                                        <th scope="col">Link</th>
                                        <th scope="col">İşlem</th>

                                    </tr>
                                    <tr>
                                        <td colspan="8" style="text-align:center; padding:20px;" ><asp:Label CssClass="alert" ID="ltrlSonuc" runat="server" Text="Henüz bir Dergi eklenmemiş. Lütfen aşağıdaki formu kullanarak bir tane ekleyin."></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                         <td>&nbsp;</td>
                                        <td><asp:TextBox ID="txtYilEkle" runat="server" ToolTip="Yayın yılı yazmalısınız (Örn; 2016)" MaxLength="4" TextMode="Number"></asp:TextBox>
                                             <asp:RequiredFieldValidator ID="reqYear" ValidationGroup="NewInsert" ControlToValidate="txtYilEkle" runat="server" Text="*" ForeColor="Red" ErrorMessage="Yayın yılı yazmalısınız (Örn; 2016)!"></asp:RequiredFieldValidator></td>
                                         <td><asp:TextBox ID="txtKatAdiEkle" runat="server" ToolTip="Referans firmanın adını yazınız." ></asp:TextBox>
                                             <asp:RequiredFieldValidator ID="reqName" ValidationGroup="NewInsert" ControlToValidate="txtKatAdiEkle" runat="server" Text="*" ForeColor="Red" ErrorMessage="Bir Firma Adı Yazmalısınız!"></asp:RequiredFieldValidator></td>
                                         <td><asp:FileUpload ID="fuKatResimEkle" runat="server" ToolTip="Referansınız için bir logo yükleyebilirsiniz." />
                                            <asp:RequiredFieldValidator ID="reqImage" ValidationGroup="NewInsert" ControlToValidate="fuKatResimEkle" runat="server" Text="*" ForeColor="Red" ErrorMessage="Referans için bir logo yüklemelisiniz!"></asp:RequiredFieldValidator></td>
                                         <td><asp:CheckBox ID="chkDurumEkle" runat="server" Enabled="true" ToolTip="Referansı yayından kaldırır, istediğiniz zaman tekrar yayınlayabilirsiniz."  /></td>
                                         <td><asp:TextBox ID="txtSiraNoEkle" runat="server" ToolTip="Sitedeki sıralamayı belirtir. Bu sayede menü yada sayfa içeriğinde belirttiğiniz sırada gözükür." ></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqInsertOrderNo" ValidationGroup="NewInsert" ControlToValidate="txtSiraNoEkle" runat="server" Text="*" ForeColor="Red" ErrorMessage="Bir Sıra numarası Girmelisiniz!"></asp:RequiredFieldValidator></td>
                                        
                                         <td>
                                         
                                        <asp:TextBox ID="txtRefURL" runat="server" ToolTip="Dergi için bir sayfa veya PDF linki verin!. Örn: http://google.com/dergi.pdf "></asp:TextBox>
                                
                                         </td>
                                         
                                         <td>
                                             
                                              <asp:UpdatePanel ID="upEmptyEkle" runat="server">
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkEmtyEkle" />
                                            </Triggers>
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lnkEmtyEkle" runat="server" OnClick="lnkEkle_Click" ValidationGroup="NewInsert" CausesValidation="true" Text="Ekle" CssClass="btn btn-success btn-primary"></asp:LinkButton>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                         </td>
                                    </tr>
                                </table>
                            </EmptyDataTemplate>
                            <EditRowStyle BackColor="SlateGray" />
                            <FooterStyle CssClass="Ekle" />
                            <PagerSettings Mode="NumericFirstLast" />
                            <PagerStyle CssClass="pagination" />
                        </asp:GridView>

                                        </ContentTemplate>
                        </asp:UpdatePanel>

                        <asp:SqlDataSource ID="OnceKaliteDergisiDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:KalderConnectionString %>"
                            DeleteCommand="DELETE FROM [OnceKaliteDergisi] WHERE [DergiId] = @original_DergiId"
                            InsertCommand="INSERT INTO [OnceKaliteDergisi] ([Yil],[Aciklama], [Resim], [Status], [OrderNo], [Link]) VALUES (@Yil, @Aciklama, @Resim, @Status, @OrderNo, @Link)" OldValuesParameterFormatString="original_{0}"
                            SelectCommand="SELECT * FROM [OnceKaliteDergisi] ORDER BY [OrderNo]"
                            UpdateCommand="UPDATE [OnceKaliteDergisi] SET [Yil] = @Yil, [Aciklama] = @Aciklama, [Resim] = @Resim, [Status] = @Status, [OrderNo] = @OrderNo, [Link] = @Link WHERE [DergiId] = @original_DergiId">
                            <DeleteParameters>
                                <asp:Parameter Name="original_DergiId" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="Yil" Type="String" />
                                <asp:Parameter Name="Aciklama" Type="String" />
                                <asp:Parameter Name="Resim" Type="String" />
                                <asp:Parameter Name="Status" Type="Boolean" />
                                <asp:Parameter Name="OrderNo" Type="Int32" />
                                <asp:Parameter Name="Link" Type="String" />
                            </InsertParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="Yil" Type="String" />
                                <asp:Parameter Name="Aciklama" Type="String" />
                                <asp:Parameter Name="Resim" Type="String" />
                                <asp:Parameter Name="Status" Type="Boolean" />
                                <asp:Parameter Name="OrderNo" Type="Int32" />
                                <asp:Parameter Name="Link" Type="String" />
                                <asp:Parameter Name="original_DergiId" Type="Int32" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                        <asp:ValidationSummary ID="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" ValidationGroup="Insert" HeaderText="Kayıt eklerken şu hataları yaptınız:" DisplayMode="BulletList" EnableClientScript="true" runat="server" />
                        <asp:ValidationSummary ID="ValidationSummary3" ShowMessageBox="true" ShowSummary="false" ValidationGroup="NewInsert" HeaderText="Kayıt eklerken şu hataları yaptınız:" DisplayMode="BulletList" EnableClientScript="true" runat="server" />
                        <asp:ValidationSummary ID="ValidationSummary2" ShowMessageBox="true" ShowSummary="false" ValidationGroup="Edit" HeaderText="Kayıt güncellerken şu hataları yaptınız:" DisplayMode="BulletList" EnableClientScript="true" runat="server" />


                    </div>
                </div>
            </div>
        </div>


    </div>


</asp:Content>

