﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="faq.aspx.cs" Inherits="Kalder.Adminv2.faq" %>


<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <link href="./js/plugins/faq/faq.css" rel="stylesheet" />
    <script src="./js/plugins/faq/faq.js"></script>
    <script src="./js/Application.js"></script>
    <script src="./js/demo/faq.js"></script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="Server">
    <div class="row">

        <div class="span8">

            <div class="widget stacked">

                <div class="widget-header">
                    <i class="icon-pushpin"></i>
                    <h3>Sık Sorulan Sorular</h3>
                </div>
                <!-- /widget-header -->

                <div class="widget-content">

                    <h3>Sorularda Ara</h3>

                    <br />

                    <ol class="faq-list">

                        <li>
                            <h4>Sitem yeni ticaret kanununa uygun olacak mı?</h4>
                            <p>Kesinlikle en önemli soru bu! Şuan piyasada hazır sistem üretip satan meslektaşlarımız sadece site yaptırmanın zorunlu olduğunu sanıyorlar. Kanuna uygunluk da bir o kadar önemli unsur. Şirket bünyemizde faaliyet gösteren avukatlarımız bizzat denetim yaparak sizi yönlendiriyorlar. Sistemi satın aldıktan sonra bizimle iletişime geçmeniz durumunda tarafınıza yeterli bilgi verilecektir.</p>

                        </li>

                        <li>
                            <h4>Sistemi hangi yollarla satın alabilirim? Ödeme yaptıktan sonra bir sorunla karşılaşır mıyım?</h4>
                            <p>Kredi Kartı, Havale ve EFT yöntemlerini kullanarak satın alabilirsiniz. Banka hesap bilgilerimiz sipariş sayfasında karşınıza çıkacaktır. Havale ve EFT ödemelerinizi siparişinizi takiben 24 saat içerisinde belirtilen banka hesaplarımızdan birine yatırdıktan sonra bize bildirmeniz yeterli.</p>

                        </li>

                        <li>

                            <h4>Sistemi satın aldıktan sonraki süreç nedir? Sitem ne zaman açılır?</h4>
                            <p>Hazır web sistemini satın alan kullanıcılarımız sipariş formunda alan adı müsaitliğini sorgulamalı ve kendileri için belirledikleri alan adlarını bize bildirmeliler. İlk olarak mevcut bir alan adınız yoksa (com,net,org,vs.) tescili yapılır, mevcut bir alan adınız var ise NameServer değişikliği tarafınızca alan adını tescil ettiğiniz şirket üzerinden yapılır. Hizmetiniz 1 işgünü içerisinde aktif edilir ve sistemin yönetim paneli bilgileri mail yoluyla iletilir.</p>
                        </li>

                        <li>
                            <h4>Yönetim paneli için kullanıcı adı ve parolamı aldım. Web sitemi nasıl yöneteceğim?</h4>
                            <p>Öncelikle şunu belirtmeliyiz ki Cportal yönetim paneli Joomla, Wordpress, Drupal gibi hazır sistemlerden derlenmemiştir. Siz değerli kullanıcılarımız için Sertifikalı Uzmanlar tarafından Türkçe ve kolay, anlaşılır bir arayüzle tasarlanmıştır. Değişiklik yapmak istediğiniz her sayfa için sistem size küçük notlar ile ne yapmanız gerektiğini söyleyecek ve yönlendirecektir. Şundan emin olabilirsiniz, görebileceğiniz en kolay ve efektif yönetim paneline sahip olacaksınız. Ayrıca kullanım videolarımızı da izleyebilir, takıldığınız noktada ekibimizden destek alabilirsiniz.</p>
                        </li>

                        <li>

                            <h4>Ben içerik girmek istemiyorum, bu işlemi bizim yerimize siz yaparmısınız?</h4>
                            <p>Haklı olabilirsiniz, hazır web sistemlerinin en güzel yanı tüm içerikleri kendinizin yönetebilmesidir. Ancak sitenizin içeriğini ne kadar düzgün girerseniz siteniz o kadar profesyonel gözükür. Bu nedenle böyle bir karar almanız durumunda "İçerik Yerleşimi" için lütfen bizden teklif isteyin. İçerik yerleşimi ücretleri eklenecek sayfa sayısına, veriye göre değişkenlik göstereceğinden bir paket ile sınırlayamıyoruz.</p>
                        </li>

                        <li>
                            <h4>Sistemi ne kadar süre kullanabilirim?</h4>
                            <p>
                                Cportal ürünlerinin kullanım süresi 1 yıl'dır. 1 yılın sonunda yetkili arkadaşlarımız sizinle iletişime geçecek ve sözleşme yenilemesi konusunda fikrinizi alacaktır. Şayet sözleşmenizi uzatmak istemezseniz sistem durur ve içeriklerinizi kaybedersiniz. Şayet sözleşme yenilerseniz sistemi yeniden satın almaz, sadece yenileme bedeli ödersiniz. Yenileme bedelleri Paket ücretinin %30'u olarak yansıtılacaktır. Örn; 600TL satın aldığınız paket için yenileme bedeli olarak sadece 180TL ödersiniz. Bu ücret içerisinde alan adınız ve hosting ücretleriniz de dahildir.
                            </p>

                        </li>

                        <li>

                            <h4>Fatura istememe hakkımız var mı? faturamızı nasıl alırız?</h4>
                            <p>Maalesef fatura kanuni bir zorunluluktur. Ayrıca Ticaret Kanununa uygunluk açısından da mecburidir. Şayet denetime tabii tutulursanız; bir site sahibi olduğunuzun en önemli ispat aracıdır. Faturanız ödemenizi yaptıktan sonra takip eden ilk işgünü kesilerek tarafınıza kargo yoluyla karşı ödemeli olarak gönderilecektir.</p>

                        </li>

                        <li>

                            <h4>Sistemi satın aldık ama sorun yaşıyoruz, ne yapmamız gerekli?</h4>
                            <p>Sitenizde oluşabilecek teknik sorunlarınızın tamamı Cportal güvencesi altındadır. Böyle bir durumla karşılaşmanız durumunda bizimle iletişime geçmeniz yeterli olacaktır. Ayrıca kendiniz de kullanımı bilmediğiniz için yanlış bişey yapmış ve sitenizin görünümüne zarar vermiş olsanız dahi bizi aramaktan çekinmeyin!. Memnuniyetiniz bizim için çok önemli.</p>

                        </li>

                        <li>

                            <h4>Özel tasarım yada modül ihtiyacım var? Aradığım bu siteden fazlası yardımcı olabilir misiniz?</h4>
                            <p>Elbette, Cportal hazır web sistemleri Cplus Interactive Media markası olan hazır web sistemidir. Cplus ihtiyaçlarınıza özel kurumsal çözümler sunan profesyonel bir ajanstır. Kurumsal Kimlik, Logo Tasarımı, Web Site Tasarımı, Karakter Tasarımı, Animasyon, Özel Yazılım, Sosyal Medya Planlaması, Google Reklamları ve daha birçok alanda ihtiyaçlarınıza çözüm getilebiliriz. Detaylı Bilgi için: www.cplus.com.tr</p>

                        </li>

                        <li>
                            <h4>Sistemi satın aldıktan sonra tasarım değiştirebilir miyim?</h4>
                            <p>Sistem kurulumu yapılmadan tasarımınıza karar vermeniz gerekli. Maalesef sistem kurulumu yapıldıktan sonra tasarımınızı değiştiremezsiniz. Ancak yinede talebinizi bizi arayarak bildirin, memnuniyetiniz bizim için önemli!</p>

                        </li>

                    </ol>


                </div>
                <!-- /widget-content -->

            </div>
            <!-- /widget -->

        </div>
        <!-- /span8 -->



        <div class="span4">

            <div class="widget widget-plain">

                <div class="widget-content">
                    <a href="javascript:;" class="btn btn-large btn-warning btn-support-ask">Soru Sorun</a>
                    <a href="javascript:;" class="btn btn-large btn-support-contact">Destek Masası</a>
                </div>
                <!-- /widget-content -->
            </div>
            <!-- /widget -->



            <div class="widget stacked widget-box">

                <div class="widget-header">
                    <h3>Video Galeri</h3>
                </div>
                <!-- /widget-header -->

                <div class="widget-content">

                    <p>Video galeri buraya gelecek!</p>

                </div>
                <!-- /widget-content -->

            </div>
            <!-- /widget -->

        </div>
        <!-- /span4 -->



    </div>
    <!-- /row -->
</asp:Content>


