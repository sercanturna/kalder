﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="product_categories.aspx.cs" Inherits="Kalder.Adminv2.product_categories" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    

    <script type="text/javascript">
        $(function () {
            var $allCheckbox = $('.allCheckbox :checkbox');
            var $checkboxes = $('.singleCheckbox :checkbox');
            $allCheckbox.change(function () {
                if ($allCheckbox.is(':checked')) {
                    $checkboxes.attr('checked', 'checked');
                }
                else {
                    $checkboxes.removeAttr('checked');
                }
            });
            $checkboxes.change(function () {
                if ($checkboxes.not(':checked').length) {
                    $allCheckbox.removeAttr('checked');
                }
                else {
                    $allCheckbox.attr('checked', 'checked');
                }
            });
        });

        
    $("#urunler").addClass("active");

    </script>
    <style>
        th.allCheckbox {width:20px;        }
        td.singleCheckbox input, th.allCheckbox input{
          width:20px !important;
        }
            td.singleCheckbox label {
            display:none;
            }
    </style>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    
    <div class="row">

        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-picture"></i>
                    <h3>Ürün Kategorileri</h3>
                    <asp:LinkButton ID="btnDeleteAll" runat="server" OnClick="btnDeleteAll_Onclick" CssClass="btn right" BorderStyle="Solid" BorderColor="#606060" BorderWidth="1" ToolTip="Sil" CausesValidation="False" CommandName="Delete" Text="x Sil" OnClientClick="return confirm('Bu Kategorileri silmek istediğinize emin misiniz? \nEğer Kategoriyi silerseniz bu kategoriye ait ürünler listelenmeyecektir.');"></asp:LinkButton>
                    <a href="add_product_categories.aspx" class="btn btn-success btn-primary right"> + Ekle </a>
                    
                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                    <div class="EU_TableScroll">
                        <asp:UpdatePanel ID="upGrid" runat="server">
                            <ContentTemplate>
                                
                                <table id="ProductCat" class="table table-bordered table-striped blank" cellspacing="0" border="1" style="border-collapse: collapse;">
                                   <asp:Repeater ID="rpt_ProductCat" runat="server" OnItemDataBound="rpt_ProductCat_OnItemDataBound" >
                                       <HeaderTemplate>
                                     <tr >
                                        <th scope="col" class="allCheckbox"><asp:CheckBox ID="allCheckbox1"  runat="server" />
                                            
                                        </th>
                                       <th scope="col">ID</th>
                                        <th scope="col">Kategori Adı</th>
                                        <th scope="col">Kategori Resmi</th>
                                        <th scope="col">Sıra No</th>
                                        <th scope="col">Yayın Durumu</th>
                                        <th scope="col">Menude Göster</th>
                                        <th scope="col">Eklenme Tarihi</th>
                                        <th scope="col">İşlem</th>
                                    </tr>
                                    </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="singleCheckbox">
                                                    <asp:CheckBox ID="chkContainer" runat="server" Text='<%# Eval("UrunKatId") %>' />
                                                    <asp:HiddenField ID="hbItem" runat="server" Value='<%# Eval("UrunKatId") %>' />
                                                </td>
                                               <td><%#Eval ("UrunKatId") %></td>
                                                 <td><a href="edit_product_categories.aspx?cId=<%#Eval ("UrunKatId") %>"><%#Eval ("KategoriAdi") %></a></td>
                                                 <td><%#Eval ("Resim") %></td>
                                                 <td><%#Eval ("SiraNo") %></td>
                                                 <td class="center"><%# Eval("YayinDurumu") == "True" ? Eval("YayinDurumu") : Eval("YayinDurumu").ToString().Replace("True", "<img src=\"img/play.png\" />").Replace("False", "<img src=\"img/pause.png\" />")%></td>
                                                 <td class="center"><%# Eval("MenudeGoster") == "True" ? Eval("MenudeGoster") : Eval("MenudeGoster").ToString().Replace("True", "<img src=\"img/play.png\" />").Replace("False", "<img src=\"img/pause.png\" />")%></td>
                                                 <td><%#Eval ("EklenmeTarihi") %></td>
                                                 <td><a href="edit_product_categories.aspx?cId=<%#Eval ("UrunKatId") %>" class="btn btn-small"><i class="btn-icon-only icon-pencil"></i></a></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <!--
                                    <tr>
                                        <td colspan="9" style="text-align:center; padding:20px;" ><asp:Label CssClass="alert" ID="ltrlSonuc" runat="server" Text="Henüz bir ürün kategorisi oluşturulmamış."></asp:Label></td>
                                    </tr>
                                    -->
                                     
                                </table>
                         
                                        </ContentTemplate>
                        </asp:UpdatePanel>

                        
                        
                    </div>
                </div>
            </div>
        </div>


    </div>


</asp:Content>



