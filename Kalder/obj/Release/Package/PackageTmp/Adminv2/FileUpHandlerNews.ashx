﻿<%@ WebHandler Language="C#" Class="FileUpHandler" %>

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.IO;
using Microsoft.SqlServer;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.Web.Routing;


    public class FileUpHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            Kalder.DbConnection db = new Kalder.DbConnection();

            string hId = context.Request.QueryString["HaberId"];

            try
            {

                context.Response.Write(hId);
            }
            catch (Exception ex)
            {
                context.Response.Write(ex.Message);
            }

          //  DbConnection db = new DbConnection();

            string sayfa = HttpContext.Current.Request.Url.AbsolutePath;

            // context.Response.ContentType = "text/plain";
            // context.Response.Write(sayfa + " d" + pId);

            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;




                for (int i = 0; i < files.Count; i++)
                {
                    
                    HttpPostedFile file = files[i];
                    string resimAdi ;

                    Random r = new Random();
                   int RandomSayi = r.Next(0, 100000);


                   int dbControl = db.cmd("Select Count(Resim) from HaberResimleri where Resim ='" + file.FileName + "' and HaberId="+hId);

                   if (dbControl == 0)
                       resimAdi = file.FileName;
                   else
                       resimAdi = RandomSayi.ToString() + file.FileName;
                    


                    string sahteResim = context.Server.MapPath("~/upload/sahte/" + resimAdi);
                    file.SaveAs(sahteResim);


                    Bitmap resim = new Bitmap(context.Server.MapPath("~/upload/sahte/" + resimAdi));
                    resim = Ayarlar.ResimBoyutlandir(resim, 800);

                    resim.Save(context.Server.MapPath("~/upload/HaberResimleri/FotoGaleri/big/" + resimAdi), ImageFormat.Jpeg);
                    // Resmi önce 280 klasörüne kayıt ediyoruz.

                    resim = Ayarlar.ResimBoyutlandir(resim, 283);
                    resim.Save(context.Server.MapPath("~/upload/HaberResimleri/FotoGaleri/thumb/" + resimAdi), ImageFormat.Jpeg);
                    // Resmi sonrada 220 klasörüne kayıt ediyoruz.

                    FileInfo temp = new FileInfo(context.Server.MapPath("~/upload/sahte/" + resimAdi));
                    temp.Delete();
                    

                    //  ****************** Veri tabanına kaydet ***************  //

                   SqlConnection resimbaglanti = db.baglan();
                    SqlCommand resimcmd = new SqlCommand("Insert into HaberResimleri(" +
                    "Resim,HaberId,IsPrimary) values (" +
                    "@Resim,@HaberId,@IsPrimary)", resimbaglanti);

                    resimcmd.Parameters.AddWithValue("Resim", resimAdi);
                    resimcmd.Parameters.AddWithValue("HaberId", hId);
                    resimcmd.Parameters.AddWithValue("IsPrimary", 0);

                    int resimsonuc = resimcmd.ExecuteNonQuery();

                }
               
                //context.Response.ContentType = "text/javascript";
                //string script = String.Format("Javascript:SuccessWithMsg('Tebrikler');");
                //context.Response.Write(script);
            }
        }
 

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
