﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="yonetim_kurulu.aspx.cs" Inherits="Kalder.yonetim_kurulu" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <style>
    span.row {
        display: table-row;
    }

    span.cell {
        display: table-cell;
        vertical-align: top;
    }

    .ykContainer {
        display: table;
        height: 106px;
        width: 100%;
    }

    ul.ykList {
        margin: 0;
        padding: 0;
        list-style: none;
        text-align: center;
    }

        ul.ykList > li {
            display: inline-block;
            width: 32%;
            margin-right: calc(2% - 4px);
            margin-bottom: 2%;
            vertical-align: top;
            transition: all 0.5s ease;
        }

           ul.ykList > li:hover {
          
-webkit-box-shadow: 0px 0px 25px -5px rgba(11,61,53,1);
-moz-box-shadow: 0px 0px 25px -5px rgba(11,61,53,1);
box-shadow: 0px 0px 25px -5px rgba(11,61,53,1);
           }

            ul.ykList > li:nth-of-type(3n+1) {
                margin-right: 0;
            }

            ul.ykList > li > span {
                display: block;
                padding: 10px;
                background: #fff;
            }

            ul.ykList > li img {
                display: block;
                max-width: 100%;
            }

    span.adsoyad {
        display: block;
        text-align: center;
        font-size: 14px;
        font-weight: bold;
        padding: 5px;
        border-bottom: 1px solid #cecece;
    }

    span.unvan1 {
        display: block;
        text-align: center;
        font-weight: bold;
        padding: 5px;
        color:rgba(0, 172, 168, 1);
    }

    span.unvan2 p{
        display: block;
        text-align: center;
        padding: 5px;
        border-top: 1px solid #cecece;
        font-size: 12px;
        color:#f68a1e;
    }

    .ykContainer {
    }

    ul.ykList > li:first-of-type {
        display: block;
        margin: 0 auto 2% auto;
    }

    ul.ykList > li:nth-of-type(4) {
        display: block;
        margin: 0 auto 2% auto;
    }

     .overlay {
            background: rgba(0, 172, 168, .7) !important;
        }
</style>

     <script>
        $(function () {
                var v = document.getElementById('<%= hdnPid.ClientID %>');
                var val = v.value;
               
                $("#cssmenu a#" + val).addClass("active");

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

      <div id="screeno"></div>
    <asp:HiddenField ID="hdnPid" runat="server" />
    <!--======= CONTENT =========-->
    <div class="content">
        <section class="sub-banner" style="margin-top: 178px;">
            <div class="overlay">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="<%= Page.ResolveUrl("~")%>Anasayfa" title="Ana Sayfa">
                            <%--<img src="<%= Page.ResolveUrl("~/App_Themes/")%><%= Page.Theme %>/images/home-icon.png" alt="Home" class="home" /></a></li>--%>
                            <%--<img src="<%= Page.ResolveUrl("~/")%>images/home-icon.png" alt="Home" class="home" />--%>Anasayfa</a></li>
                        
                        <asp:Repeater ID="rptBreadcrumb" runat="server" OnItemDataBound="rptBreadcrumb_OnItemDataBound">
                    <ItemTemplate>
                        <asp:PlaceHolder ID="treeMenu" runat="server"></asp:PlaceHolder>
                    </ItemTemplate>
                </asp:Repeater>
                    </ol>
                </div>
            </div>
        </section>





        <section class="innerpages">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <i id="mobile-sidemenu-btn" class="ion-chevron-down"></i>
                            <div class="col-md-3 col-sm-3 sidemenus" id="sideBar" runat="server">
                                <asp:Repeater ID="rptSidebarModules" runat="server" OnItemDataBound="rptSidebarModules_ItemDataBound">
                                    <ItemTemplate>
                                        <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>


                            <div class="col-md-9 col-sm-9">
                                <div class="contentbox">

                                    <div class="tittle tittle-2">
										
										<h3><asp:Literal ID="ltrlBaslik" runat="server" Text="KalDer Yönetim Kurulu"></asp:Literal></h3>
										<hr />
									</div>
 
<ul class="ykList">
    <asp:Repeater ID="rptYk" runat="server">
        <ItemTemplate>
            <li>
                <span>
                    <img src="<%= ResolveUrl("~/") %>Upload/YonetimKurulu/<%#Eval("Image") %>" alt="">
                    <span class="ykContainer">
                        <span class="row">
                            <span class="cell">
                                <span class="adsoyad"><%#Eval("Name") %></span>
                                <span class="unvan1"><%#Eval("Title") %></span>
                                <span class="unvan2"><%#Eval("Content") %></span>
                            </span>
                        </span>
                    </span>
                </span>
            </li>
        </ItemTemplate>
    </asp:Repeater>

</ul>


                                     </div>
                            </div>
                         </div>
                    </div>
                </div>
            </div>
        </section>


    </div>
</asp:Content>
