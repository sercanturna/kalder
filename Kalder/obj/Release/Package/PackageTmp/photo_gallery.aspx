﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="photo_gallery.aspx.cs" Inherits="Kalder.photo_gallery" %>
 
    <%@ Register Assembly="CollectionPager" Namespace="SiteUtils" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
<%--    <link href="<%= ResolveUrl("~/Adminv2") %>/js/plugins/lightbox/themes/evolution-dark/jquery.lightbox.css" rel="stylesheet" />	
    <script type="text/javascript"src="<%= ResolveUrl("~/Adminv2") %>/js/plugins/lightbox/jquery.lightbox.min.js"></script>
     

 
    <script type="text/javascript" src="<%= ResolveUrl("~/Adminv2") %>/js/plugins/hoverIntent/jquery.hoverIntent.minified.js"></script>
    <script type="text/javascript" src="<%= ResolveUrl("~/Adminv2") %>/js/demo/gallery.js"></script>--%>
       <script src="<%= ResolveUrl("~/") %>include/sliderengine/amazingslider.js"></script>
    <script src="<%= ResolveUrl("~/") %>include/sliderengine/initslider-2.js"></script>
    
<style>
 
.gallery-container figcaption {
    position:absolute;
    border:0px solid #ff0000;
  z-index:10;
  bottom:5px;
  width:93%;
  background-color:#000000; color:#ffffff;
}
 
                div.aaa{
                    position:absolute; display:block; left:0; z-index:100; width:100%; bottom:0px; height:25px;   line-height:1.7; display:block; color:#fff;

                        transition-property: all;
	transition-duration: .5s;
	transition-timing-function: cubic-bezier(0, 1, 0.5, 1);
                }

                    li.cat a:hover > div.aaa{
                      color:#fff;
                       

                    height:50%;

                    transition-property: all;
	transition-duration: .5s;
	transition-timing-function: cubic-bezier(0, 1, 0.5, 1);

                    }

                   
            </style>
    <!--[if lt IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
<![endif]-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <asp:Literal ID="ltrlCollectionPagerSayfaSayisi" runat="server"></asp:Literal>
    <asp:Literal ID="ltrlCollectionNesneSayisi" runat="server"></asp:Literal>
    
     <div class="clear page_container">
           <div class="breadcumb">
            <ul id="breadcrumb" class="breadcumb">
                <li><a href="<%= Page.ResolveUrl("~")%>Anasayfa" title="Ana Sayfa">
                    <img src="<%= Page.ResolveUrl("~/")%>images/home-icon.png" alt="Home" class="home" /></a></li>

                <li><a href="<%= Page.ResolveUrl("~")%>fotogaleri" title="Foto Galeri">Foto Galeri</a></li>
                <li><asp:Literal ID="ltrlphotoCat" runat="server"></asp:Literal></li>
            </ul>
            
            <div class="social_share">
        <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style " style="float:right; width:150px;">
        <a class="addthis_button_preferred_1"></a>
        <a class="addthis_button_preferred_2"></a>
        <a class="addthis_button_preferred_3"></a>
        <a class="addthis_button_preferred_4"></a>
        <a class="addthis_button_compact"></a>
        <a class="addthis_counter addthis_bubble_style"></a>
        </div>
        <script type="text/javascript">var addthis_config = { "data_track_addressbar": false };</script>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f34f2f752ee3f86"></script>
        <!-- AddThis Button END -->
            </div>

        </div>

         

     

        <div id="pageContent" class="page_content" runat="server" >
             <asp:Repeater ID="rptContentModules" runat="server" OnItemDataBound="rptContentModules_ItemDataBound">
                <ItemTemplate>
                    <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:Repeater>
           
            <div class="slider" style="overflow:hidden; width:100%; padding-left:6px; padding-bottom:20px;">
            
               <!-- Insert to your webpage where you want to display the slider -->
    <div id="amazingslider-1">
        <ul class="amazingslider-slides photogallery" style="display:none;">
            <asp:Repeater ID="rptGallerySlider" runat="server">
                        <ItemTemplate>
                            <li class="photogallery"><a href="<%= ResolveUrl("~/fotogaleri/") %><%#Eval("KategoriAdi") %>"><img src="<%=Page.ResolveUrl("~/upload/Galeri/") %>/<%#Eval("KategoriAdi") %>/<%#Eval("KategoriResmi") %>" alt="<%#Eval("KategoriAdi") %>" data-description="<%#Eval("KategoriAdi") %> albümüne ait görseller için tıklayın..." /></a></li>
                        </ItemTemplate>
                    </asp:Repeater>
        </ul>

        <ul class="amazingslider-thumbnails" style="display:none;">
                <asp:Repeater ID="rptGalleryThumb" runat="server">
                        <ItemTemplate>
                            <li><a href="<%= ResolveUrl("~/fotogaleri/") %><%#Eval("KategoriAdi") %>"><img src="<%=Page.ResolveUrl("~/upload/Galeri/") %><%#Eval("KategoriAdi") %>/<%#Eval("KategoriResmi") %>" alt="<%#Eval("KategoriAdi") %>" /></a></li>
                        </ItemTemplate>
                    </asp:Repeater>
        </ul>
        
    </div>
    <!-- End of body section HTML codes -->
            </div>

            <div class="clear"></div>
          
        

            
            
            <div class="gallery-containers">
                 <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Always">
              <ContentTemplate>
                 <h1 class="title"><span class="left"><asp:Literal ID="ltrlBaslik" runat="server"></asp:Literal></span> </h1>
                 
                    
                                <asp:Repeater ID="rptImages" runat="server" OnItemDataBound="rptImages_ItemDataBound">
                                    <HeaderTemplate>
                                        <div class="gallery-container" style="float:left; margin-bottom:0;">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Literal ID="ltrlFigure" runat="server"></asp:Literal>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </div>
                                    </FooterTemplate>
                                </asp:Repeater>

                     <!-- Eğer kategori seçilmemişse doğrudan bu reperater devreye girecek üsttekini kapatacak. -->
                     <asp:Repeater ID="rptPhotoCat" runat="server">
                      <HeaderTemplate>
                          <ul class="categorycontainer" style="float:left; margin-bottom:0;">
                      </HeaderTemplate>
                         <ItemTemplate>
                                <li class="cat"><a href="<%= ResolveUrl("~/fotogaleri/") %><%#Eval("KategoriAdi") %>"><img src="<%=Page.ResolveUrl("~/upload/Galeri/") %>/<%#Eval("KategoriAdi") %>/<%#Eval("KategoriResmi") %>" alt="<%#Eval("KategoriAdi") %>"  />
                               <div class="aaa"><%#Eval("KategoriAdi") %> <br />albümüne ait görseller</div></a></li>
                         </ItemTemplate>
                         <FooterTemplate>
                            </ul>
                         </FooterTemplate>
                     </asp:Repeater>
                         
                     </div>

    <section id="paginations" style="border-top:1px dashed #d3d3d3 !important; float:left; width:100% !important; padding-top:10px;">
        <div class="sayfalama">
            <cc1:CollectionPager ID="CollectionPager1" runat="server" BackText=" « Önceki" 
            FirstText="İlk" LabelText="" LastText="Son" NextText="Sonraki »" 
            PageNumbersDisplay="Numbers" ResultsFormat="Resimler {0}-{1} (Toplam:{2})" ResultsStyle="font-weight:normal; font-size:12px !important; float:left; line-height:2.4; padding-right:10px;"
           
              EnableViewState="true" SectionPadding="5" BackNextDisplay="Buttons" BackNextLocation="Split" 
             PageNumbersSeparator=""></cc1:CollectionPager>
            </div>
    </section>


 

<!-- Root element of PhotoSwipe. Must have class pswp. -->
<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

    <!-- Background of PhotoSwipe. 
         It's a separate element, as animating opacity is faster than rgba(). -->
    <div class="pswp__bg"></div>

    <!-- Slides wrapper with overflow:hidden. -->
    <div class="pswp__scroll-wrap">

        <!-- Container that holds slides. PhotoSwipe keeps only 3 slides in DOM to save memory. -->
        <!-- don't modify these 3 pswp__item elements, data is added later on. -->
        <div class="pswp__container">
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
            <div class="pswp__item"></div>
        </div>

        <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
        <div class="pswp__ui pswp__ui--hidden">

            <div class="pswp__top-bar">

                <!--  Controls are self-explanatory. Order can be changed. -->

                <div class="pswp__counter"></div>

                <button type="button" class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                <button type="button" class="pswp__button pswp__button--share" title="Share"></button>

                <button type="button" class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                <button type="button" class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                <!-- element will get class pswp__preloader--active when preloader is running -->
                <div class="pswp__preloader">
                    <div class="pswp__preloader__icn">
                      <div class="pswp__preloader__cut">
                        <div class="pswp__preloader__donut"></div>
                      </div>
                    </div>
                </div>
            </div>

            <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                <div class="pswp__share-tooltip"></div> 
            </div>

            <button type="button" id="prev" class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
            </button>

            <button type="button" id="next" class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
            </button>

            <div class="pswp__caption">
                <div class="pswp__caption__center"></div>
            </div>

          </div>

        </div>

</div>

              </ContentTemplate>
          </asp:UpdatePanel>
            </div>

                   
             

            <div id="sideBar" class="sidebar left" runat="server">

            <asp:Repeater ID="rptSidebarModules" runat="server" OnItemDataBound="rptSidebarModules_ItemDataBound">
                <ItemTemplate>
                    <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <!-- / Sidebar-->

         </div>
 

        <asp:Repeater ID="rptFooterModules" runat="server" OnItemDataBound="rptFooterModules_ItemDataBound">
            <ItemTemplate>
                <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
            </ItemTemplate>
    </asp:Repeater>




    <link href="<%= ResolveUrl("~/") %>js/photoswipe/dist/photoswipe.css" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/") %>js/photoswipe/dist/default-skin/default-skin.css" rel="stylesheet" />
    <script src="<%= ResolveUrl("~/") %>js/photoswipe/dist/photoswipe.min.js"></script>
    <script src="<%= ResolveUrl("~/") %>js/photoswipe/dist/photoswipe-ui-default.min.js"></script>


    <script>

        // CollectionPager nesnesinin sayfa ve nesne sayısını c# tarafından hidden field ataması yaparak jquery ile yakalıyorum.
        var CollectionPagerPageCount = document.getElementById('ColPageNum').innerHTML;
        var CollectionPagerItemCount = document.getElementById('ColPageItems').innerHTML;

        console.log("Sayfa sayısı : " + CollectionPagerPageCount + ", Nesne sayısı : " + CollectionPagerItemCount);



        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }


        var pid;
        var sayfaNo = getParameterByName('Page');

        if (sayfaNo == null) {
            sayfaNo = 1;
        }

        //$("#next").click(function () {
        ////    var pid = getParameterByName('pid');



        //    if (pid == 11)
        //    {
        //        sayfaNo++;


        //        GoNextPage(sayfaNo);


        //    }

        //});



        $("#prev").click(function () {
            //  var pid = getParameterByName('pid');
            console.log(pid--);
        });



        function GoNextPage(sayfaNo) {
            if (history.pushState) {
                var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?Page=' + sayfaNo + '#&gid=1&pid=1';
                window.location.href = newurl;
            }
        }





        //    if (history.pushState) {
        //        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?myNewUrlQuery=1';
        //        window.history.pushState({ path: newurl }, '', newurl);
        //    }




        var initPhotoSwipeFromDOM = function (gallerySelector) {

            // parse slide data (url, title, size ...) from DOM elements 
            // (children of gallerySelector)
            var parseThumbnailElements = function (el) {
                var thumbElements = el.childNodes,
                    numNodes = thumbElements.length,
                    items = [],
                    figureEl,
                    linkEl,
                    size,
                    item;

                for (var i = 0; i < numNodes; i++) {


                    figureEl = thumbElements[i]; // <figure> element

                    // include only element nodes 
                    if (figureEl.nodeType !== 1) {
                        continue;
                    }

                    linkEl = figureEl.children[0]; // <a> element

                    size = linkEl.getAttribute('data-size').split('x');

                    // create slide object
                    item = {
                        src: linkEl.getAttribute('href'),
                        w: parseInt(size[0], 10),
                        h: parseInt(size[1], 10)
                    };



                    if (figureEl.children.length > 1) {
                        // <figcaption> content
                        item.title = figureEl.children[1].innerHTML;
                    }

                    if (linkEl.children.length > 0) {
                        // <img> thumbnail element, retrieving thumbnail url
                        item.msrc = linkEl.children[0].getAttribute('src');
                    }

                    item.el = figureEl; // save link to element for getThumbBoundsFn
                    items.push(item);
                }

                return items;
            };

            // find nearest parent element
            var closest = function closest(el, fn) {
                return el && (fn(el) ? el : closest(el.parentNode, fn));
            };

            // triggers when user clicks on thumbnail
            var onThumbnailsClick = function (e) {
                e = e || window.event;
                e.preventDefault ? e.preventDefault() : e.returnValue = false;

                var eTarget = e.target || e.srcElement;

                // find root element of slide
                var clickedListItem = closest(eTarget, function (el) {
                    return (el.tagName && el.tagName.toUpperCase() === 'FIGURE');
                });

                if (!clickedListItem) {
                    return;
                }

                // find index of clicked item by looping through all child nodes
                // alternatively, you may define index via data- attribute
                var clickedGallery = clickedListItem.parentNode,
                    childNodes = clickedListItem.parentNode.childNodes,
                    numChildNodes = childNodes.length,
                    nodeIndex = 0,
                    index;

                for (var i = 0; i < numChildNodes; i++) {
                    if (childNodes[i].nodeType !== 1) {


                        continue;
                    }

                    if (childNodes[i] === clickedListItem) {
                        index = nodeIndex;
                        break;
                    }
                    nodeIndex++;


                }



                if (index >= 0) {
                    // open PhotoSwipe if valid index found
                    openPhotoSwipe(index, clickedGallery);

                }
                return false;
            };

            // parse picture index and gallery index from URL (#&pid=1&gid=2)
            var photoswipeParseHash = function () {
                var hash = window.location.hash.substring(1),
                params = {};

                if (hash.length < 5) {
                    return params;
                }

                var vars = hash.split('&');
                for (var i = 0; i < vars.length; i++) {
                    if (!vars[i]) {
                        continue;
                    }
                    var pair = vars[i].split('=');
                    if (pair.length < 2) {
                        continue;
                    }
                    params[pair[0]] = pair[1];
                }

                if (params.gid) {

                    params.gid = parseInt(params.gid, 10);
                }


                return params;
            };

            var openPhotoSwipe = function (index, galleryElement, disableAnimation, fromURL) {
                var pswpElement = document.querySelectorAll('.pswp')[0],
                    gallery,
                    options,
                    items;

                items = parseThumbnailElements(galleryElement);




                // define options (if needed)
                options = {
                    loop: false,
                    // define gallery index (for URL)
                    index: 0,
                    galleryUID: galleryElement.getAttribute('data-pswp-uid'),

                    getThumbBoundsFn: function (index) {
                        // See Options -> getThumbBoundsFn section of documentation for more info
                        var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                            pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                            rect = thumbnail.getBoundingClientRect();

                        return { x: rect.left, y: rect.top + pageYScroll, w: rect.width };
                    }

                };

                // PhotoSwipe opened from URL
                if (fromURL) {
                    if (options.galleryPIDs) {
                        // parse real index when custom PIDs are used 
                        // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
                        for (var j = 0; j < items.length; j++) {
                            if (items[j].pid == index) {
                                options.index = j;
                                break;
                            }
                        }
                    } else {
                        // in URL indexes start from 1
                        options.index = parseInt(index, 10) - 1;
                    }
                } else {
                    options.index = parseInt(index, 10);
                }

                // exit if index not found
                if (isNaN(options.index)) {
                    return;
                }

                if (disableAnimation) {
                    options.showAnimationDuration = 0;
                }

                // Pass data to PhotoSwipe and initialize it
                gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);


                gallery.listen('beforeChange', function () {

                    var pid = parseInt(getParameterByName('pid'));

                    console.log(pid);


                    if (pid + 1 == 12) {
                        sayfaNo++;
                        GoNextPage(sayfaNo);
                    }

                });


                ////Sayfalamayı otomatik yapsın diye galeriyi listen yani dinliyorum böylelikle collectionpager nesnesinde kaç sayfa geliyorsa o sayfa kadar eleman sayısını arttıracak.
                gallery.listen('imageLoadComplete', function (index, item) {

                    //console.log(index);

                    //pid = index;

                    //if (pid == 10) {
                    //    sayfaNo++;
                    //    GoNextPage(sayfaNo);
                    //}



                    //var currentPage = 1
                    //var loop = 0;

                    //   console.log("Sayfa sayısı : " + CollectionPagerPageCount + ", Nesne sayısı : " + CollectionPagerItemCount);

                    //for (var i = 0; i < CollectionPagerPageCount; i++)
                    //{
                    //if (index == 1) {

                    //    if (history.pushState) {
                    //        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?Page=' + i + '#&gid=1&pid=1';
                    //        window.location.href = newurl;
                    //    }

                    //}
                    //   }



                });


                gallery.init();
            };

            // loop through all gallery elements and bind events
            var galleryElements = document.querySelectorAll(gallerySelector);

            for (var i = 0, l = galleryElements.length; i < l; i++) {
                galleryElements[i].setAttribute('data-pswp-uid', i + 1);
                galleryElements[i].onclick = onThumbnailsClick;
            }

            // Parse URL and open gallery if it contains #&pid=3&gid=1
            var hashData = photoswipeParseHash();
            if (hashData.pid && hashData.gid) {

                openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
            }



        };

        // execute above function
        initPhotoSwipeFromDOM('.gallery-container');

    </script>
        <script>
            if (document.location.search.match(/type=embed/gi)) {
                window.parent.postMessage("resize", "*");
            }
</script>
</asp:Content>

