﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="breadcumb.ascx.cs" Inherits="Kalder.include.breadcumb" %>
  <div class="breadcumb">
            <ul id="breadcrumb" class="breadcumb">
                <li><a href="<%= Page.ResolveUrl("~")%>Anasayfa" title="Ana Sayfa">
                    <img src="<%= Page.ResolveUrl("~/")%>images/home-icon.png" alt="Home" class="home" /></a></li>

                <asp:Repeater ID="rptBreadcrumb" runat="server" OnItemDataBound="rptBreadcrumb_OnItemDataBound">
                    <ItemTemplate>
                        <asp:PlaceHolder ID="treeMenu" runat="server"></asp:PlaceHolder>
                    </ItemTemplate>
                </asp:Repeater>
              
            </ul>
            
            <div class="social_share">
        <!-- AddThis Button BEGIN -->
        <div class="addthis_toolbox addthis_default_style " style="float:right; width:150px;">
        <a class="addthis_button_preferred_1"></a>
        <a class="addthis_button_preferred_2"></a>
        <a class="addthis_button_preferred_3"></a>
        <a class="addthis_button_preferred_4"></a>
        <a class="addthis_button_compact"></a>
        <a class="addthis_counter addthis_bubble_style"></a>
        </div>
        <script type="text/javascript">var addthis_config = { "data_track_addressbar": true };</script>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f34f2f752ee3f86"></script>
        <!-- AddThis Button END -->
            </div>

        </div>