﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsSidebarMenu.ascx.cs" Inherits="Kalder.include.NewsSidebarMenu" %>
<aside>  
<div id='cssmenu'>
    <ul>
        <asp:Repeater ID="rpt_leftmenu" runat="server" OnItemDataBound="rpt_leftmenu_OnItemDataBound" >
            <HeaderTemplate>
                    <asp:Literal ID="ltrlLeftMenuHeader" runat="server"></asp:Literal>
            </HeaderTemplate>
            <ItemTemplate>
                <li><asp:HyperLink ID="hplLink" runat="server"><span><%#Eval("KategoriAdi") %></span></asp:HyperLink></li>
                <%--<li><a href='<%= ResolveUrl("~/") %><%#Eval("newsHeader") %>/<%#Ayarlar.UrlSeo(Eval("KategoriAdi").ToString())%>'></a></li>--%>
            </ItemTemplate>
            <FooterTemplate>
                </ul>
            </FooterTemplate>
        </asp:Repeater>
    </ul>
</div>
<!-- / cssmenu-->
    </aside>