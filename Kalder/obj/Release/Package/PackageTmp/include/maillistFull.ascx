﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="maillistFull.ascx.cs" Inherits="Kalder.include.maillistFull" %>

 
<form id="MailingForm">
<style>
    .closeBtn {
    position:absolute; top:30px; right:10px; text-decoration:none; color:#fff;
    }

   div.CurrencyModule h1.h_bg {
    margin-left:0 !important; width:208px;
    }
</style>


          <!-- Bootstrap Modal Dialog -->
<div class="modal fade" id="MailingModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <asp:UpdatePanel ID="upModal" runat="server" ChildrenAsTriggers="false" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" style="color:#00aca8; padding:5px;"><asp:Label ID="lblModalTitle" runat="server" Text=""></asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <asp:Label ID="lblModalBody" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-info" data-dismiss="modal" aria-hidden="true">Kapat</button>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>

<asp:Panel ID="weather_widget" runat="server">



<asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
    <ContentTemplate>
   
        

       
            <asp:Panel ID="pnlFormContent" runat="server">
           <!--======= EMAILLIST SUBSCRIBE =========-->
			 
            <section class="ebulten" style="margin-top:60px; background:#22282f url(images/yildiz.jpg) left top no-repeat;">
				<div class="container subscribe">				
                    <div class="col-lg-8">
                    	<img src="images/kalder-eabone.png" class="img img-responsive pull-left"/>
                        <h2>e-bülten listemize kayıt olabilirsiniz.</h2>
                        <p class="text">KalDer tarafından bilgilendirmeler almak için...</p>
                    </div>
                    <div class="col-lg-4 padding-t-20">
                    <table class="table table-responsive">
                    <tr>
	                    <td><asp:TextBox runat="server" id="txtName" placeholder="Adınız ve Soyadınız"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtName" ErrorMessage="* Bu alan zorunludur" ValidationGroup="maillist" Display="Dynamic"></asp:RequiredFieldValidator></td>
                        <td><asp:Button ID="contact_submit1" class="btn-warning" Text="LİSTEYE EKLE" runat="server" OnClick="contact_submit1_Click" ValidationGroup="maillist" CausesValidation="true" /></td>
                    </tr>
                    <tr>
	                    <td> <asp:TextBox  runat="server" id="txtEmail" tabindex="2" placeholder="Eposta adresiniz" ></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="reqMail" ControlToValidate="txtEmail" ErrorMessage="* Bu alan zorunludur" ValidationGroup="maillist" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator Display="Dynamic"  ValidationGroup="maillist" runat="server" ID="reqMailv" ControlToValidate="txtEmail" ErrorMessage="*Geçerli bir mail adresi yazın" ValidationExpression="^([a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]){1,70}$"></asp:RegularExpressionValidator></td>
                        <td><asp:Button ID="btn_remove" class="btn-info" Text="LİSTEDEN ÇIK" runat="server" OnClick="btn_remove_Click" ValidationGroup="maillist" CausesValidation="true" /></td>
                    </tr>
                    </table>
                    </div>
                    
				</div>
            </section>
		 <!--======= EMAILLIST SUBSCRIBE =========-->


        </asp:Panel>
           
      
     
</ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="contact_submit1" />
        <asp:PostBackTrigger ControlID="btn_remove" />
    </Triggers>
</asp:UpdatePanel>
</asp:Panel>
</form>