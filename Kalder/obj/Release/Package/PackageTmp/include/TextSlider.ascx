﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TextSlider.ascx.cs" Inherits="Kalder.include.TextSlider" %>

 
 
  <!--======= ÖNCE KALİTE MAGAZINE =========-->
    <section id="yayinlar">
			<div class="subcategory-list margin-b-20">
				<div class="container"> 
                <div class="tittle tittle-2">
  						<p>KalDer Yayınları </p>
						<h3>Önce Kalite Dergisi</h3>
						<hr>
					</div>
					<div class="col-lg-12" style="margin:0; border:0px solid #26b8b5; border-top:none; border-bottom:none;">
				  <ul class="kalDer-magazine">
                      <asp:Repeater ID="rptDergi" runat="server">
                          <ItemTemplate>
                              <li class="magazine_item"><a href="<%#Eval("Link") %>"><img src="upload/Dergi/<%#Eval("Resim") %>"><h6><%#Eval("Yil") %> <%#Eval("Aciklama") %></h6></a></li>
                          </ItemTemplate>
                      </asp:Repeater>
				  </ul>
				  </div>
				</div>
			</div>
    </section>