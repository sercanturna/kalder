﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PhotoGallerySidebarMenu.ascx.cs" Inherits="Kalder.include.PhotoGallerySidebarMenu" %>
<div id='cssmenu'>
                <ul>
                    <asp:Repeater ID="rpt_leftmenu" runat="server">
                        <HeaderTemplate>
                             <li class="has-sub active"><a href="fotogaleri" class="selected"><span><asp:Literal ID="ltrlLeftMenuHeader" runat="server" Text="Foto Galeri"></asp:Literal></span></a>
                            <ul>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li><a href='<%= ResolveUrl("~/") %>fotogaleri/<%#Eval("KategoriAdi").ToString()%>'><span><%#Eval("KategoriAdi") %></span></a></li>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul></li>
                        </FooterTemplate>
                    </asp:Repeater>
                </ul>
            </div>
            <!-- / cssmenu-->