﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="maillist.ascx.cs" Inherits="Kalder.include.maillist" %>

 

<style>
    .closeBtn {
    position:absolute; top:30px; right:10px; text-decoration:none; color:#fff;
    }

   div.CurrencyModule h1.h_bg {
    margin-left:0 !important; width:208px;
    }
</style>


<asp:Panel ID="weather_widget" runat="server">
<asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false">
    <ContentTemplate>
    <div class="CurrencyModule">
        <h1 class="h_bg">E-Bülten</h1>

        <div class="form left">
            <asp:Panel ID="pnlFormContent" runat="server">
            <div class="wrapper">
                <div id="main">
                   
                    <!-- Form -->
                    <div id="contact-form">
                     <%-- <p>Newsletter'a üye olun ilk sizin haberiniz olsun.</p>--%>
                        <div>
                            <label>
                                <asp:TextBox runat="server" id="txtName" placeholder="Adınız ve Soyadınız"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqName" runat="server" ControlToValidate="txtName" ErrorMessage="* Bu alan zorunludur" ValidationGroup="maillist" Display="Dynamic"></asp:RequiredFieldValidator>
                            </label>
                        </div>
                        <div>
                            <label>
                              
                                <asp:TextBox  runat="server" id="txtEmail" tabindex="2" placeholder="Eposta adresiniz" ></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ID="reqMail" ControlToValidate="txtEmail" ErrorMessage="* Bu alan zorunludur" ValidationGroup="maillist" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator Display="Dynamic"  ValidationGroup="maillist" runat="server" ID="reqMailv" ControlToValidate="txtEmail" ErrorMessage="*Geçerli bir mail adresi yazın" ValidationExpression="^([a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]){1,70}$"></asp:RegularExpressionValidator>
                            </label>
                        </div>
                        	<div>
                <asp:Button ID="contact_submit1" Text="Kayıt Ol" runat="server" OnClick="contact_submit1_Click" ValidationGroup="maillist" CausesValidation="true" />
                
			</div>
                        <div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
            <asp:Panel ID="PnlSonuc" runat="server" Visible="false">
                <asp:Literal ID="ltrlSonuc" runat="server"></asp:Literal>
                <img src="<%Page.ResolveClientUrl("~/"); %>images/success.gif" style="width:100%; height:auto;" />
                <asp:LinkButton ID="lnkClose" runat="server" Text="X" CssClass="closeBtn" OnClick="lnkClose_Click"></asp:LinkButton>
            </asp:Panel>
        </div>
    </div>
</ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="contact_submit1" />
    </Triggers>
</asp:UpdatePanel>
</asp:Panel>
