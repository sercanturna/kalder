﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="style_switcher.ascx.cs" Inherits="Kalder.include.style_switcher" %> 

<link href="<%=Page.ResolveUrl("~/") %>swtch/font-awesome.min.css" rel="stylesheet" />
<link href="<%=Page.ResolveUrl("~/") %>swtch/style-switcher.css" rel="stylesheet" />
<%--<script src="<%=Page.ResolveUrl("~/") %>swtch/jquery.cookie.js"></script>--%>
<script src="<%=Page.ResolveUrl("~/") %>swtch/main.js"></script>


<!-- jQuery/jQueryUI (hosted) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.js"></script>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="<%= ResolveUrl("~/") %>Scripts/colorpicker/jquery.colorpicker.js"></script>
<link href="<%= ResolveUrl("~/") %>Scripts/colorpicker/jquery.colorpicker.css" rel="stylesheet" type="text/css"/>

<script>
    

    $(function () {
        $('#style_switcher1_Renk1ColorPicker').colorpicker({
            parts: 'full',
            alpha: true,
            showOn: 'both',
            buttonColorize: true,
            showNoneButton: true
        });
    });

    $(function () {
        $('#style_switcher1_Renk2ColorPicker').colorpicker({
            parts: 'full',
            alpha: true,
            showOn: 'both',
            buttonColorize: true,
            showNoneButton: true
        });
    });

</script>


<style>
    .ul-colors .ui-button .ui-button-text {
        display: block;
        line-height: 1.0; 
    }
   .ul-colors .ui-button-text-only .ui-button-text {
    margin:0; padding:0; margin:0px; 
    }
</style>


<div id="style-switcher" class="swopen">
	<div class="swtch-opener"><a href="#"><i class="icon-magic"></i></a></div>
	<div class="swtch-header">
		<h4>KİŞİSEL SEÇENEKLER</h4>
	</div>
	<div class="styleswtch">
		<%--<h4>LAYOUT STYLE</h4>
		<ul class="ul-layout margint10 clearfix">
			<li><a href="http://www.2035themes.com/boxme/wide">WIDE</a></li>
			<li><a href="http://www.2035themes.com/boxme/boxed" class="active">BOXED</a></li>
		</ul>--%>
        <div class="clearfix"></div>
        <br />
		<h4>RENKLER</h4>
		<ul class="ul-colors margint10 clearfix">
			<li>Renk 1 : <asp:TextBox ID="Renk1ColorPicker" runat="server"></asp:TextBox><br />
			Renk 2 : <asp:TextBox ID="Renk2ColorPicker" runat="server"></asp:TextBox></li>
            <li class="clearfix"><asp:LinkButton ID="btnColors" runat="server" CssClass="stylereset" OnClick="lnkColors_Click" Text="Renkleri Uygula"></asp:LinkButton></li>
			
		</ul>
         <div class="clearfix"></div>
        <br />
           


		<h4 class="margint20">DOKULAR</h4>
		<ul class="ul-pattern margint10 clearfix">
			<li id="pattern1"></li>
			<li id="pattern2"></li>
			<li id="pattern3"></li>
			<li id="pattern4"></li>
			<li id="pattern5"></li>
			<li id="pattern6"></li>
			<li id="pattern7"></li>
			<li id="pattern8"></li>
			<li id="pattern9"></li>
			<li id="pattern10"></li>
			<li id="pattern11"></li>
			<li id="pattern12"></li>
		</ul>
		<h4 class="margint20">ARKAPLAN</h4>
		<ul class="ul-background margint10 clearfix">
			<li id="background1"></li>
			<li id="background2"></li>
			<li id="background3"></li>
			<li id="background4"></li>
			<li id="background5"></li>
			<li id="background6"></li>
			<li id="background7"></li>
			<li id="background8"></li>
		</ul>
		<p class="margint20"><a class="stylereset" href="#">SIFIRLA</a></p>
	</div>
</div>