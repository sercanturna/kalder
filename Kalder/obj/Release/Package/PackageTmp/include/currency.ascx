﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="currency.ascx.cs" Inherits="Kalder.include.currency" %>

<style>
    div.CurrencyModule {
        width: 100%;
    
        margin:4% 0;
        position:relative;
         
        border: 1px solid #cacaca;
        overflow:hidden;
        
    }
        div.CurrencyModule table {
         width: 100%;
         
        }
            div.CurrencyModule table thead tr th {
                 font-size:13px; padding:5px; color:#fff; border-bottom-right-radius:9px; border-bottom-left-radius:69px;
            }

            div.CurrencyModule table thead tr th.alis {
                background-color:#cacaca;  position:absolute; width:75px; right:65px;
            }

            div.CurrencyModule table thead tr th.satis {
                background-color:#3686c6; position:absolute; width:75px; right:0px;
            }

            div.CurrencyModule table tr td {
                padding:10px;
                background:#fbfbfb;
                font-size:14px;
                border-bottom:1px solid #e1e1e1;
            }

                div.CurrencyModule table tr td.tl {
                font-weight:normal;
                color:#777;
                }

                div.CurrencyModule table tr td img {
                vertical-align:middle;
                }
            div.CurrencyModule table tr td span.value {
            font-weight:800;
            }
        div.CurrencyModule h1.h_bg {
        margin:0; background-repeat:repeat; width:100%;
        }
</style>
<div class="CurrencyModule">
    <h1 class="h_bg">Döviz Kurları</h1>
<table>
    <thead>
        <tr style="position:relative; height:25px;">
         <th></th>
        <th class="alis">Alış</th>
        <th class="satis">Satış</th>
            </tr>
    </thead>

    <tbody>
        <asp:Repeater ID="rptCurrency" runat="server" OnItemDataBound="rptCurrency_ItemDataBound">
            <ItemTemplate>
                <tr>
                <td><div class="flag flag-<%#Eval("Currency") %>">&nbsp;</div> <span class="value"><asp:Literal ID="ltrlCurrency" runat="server" Text=""></asp:Literal></span></td>
                <td class="tl"><asp:Label ID="lblAlis" runat="server" Text=""></asp:Label></td>
                <td class="tl"><asp:Label ID="lblSatis" runat="server" Text=""></asp:Label></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        </tbody>
</table>
    </div>