﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="boxes.ascx.cs" Inherits="Kalder.include.boxes" %>

 


<section>

       <!--======= BOXES LIST =========-->
			<div class="subcategory-list">
				<div class="container"> 
                <div class="tittle tittle-2">
						<p>KalDer-Kısayollar</p>
						<h3><asp:Literal ID="ltrlBaslik" runat="server"></asp:Literal></h3>
						<hr>
					</div>
					<div class="col-lg-12" style="margin:0; border:5px solid #26b8b5; border-top:none; border-bottom:none;  ">
				  <div class="doct-list-style">

                      <asp:Repeater ID="rpt_Box" runat="server">
                            <ItemTemplate>
                                <div class="item">
                                    <h4><%#Eval ("Title") %></h4>
                                    <p><%#Eval ("Content") %></p>
                                        <a href="<%#Eval ("Link") %>" target="<%#Eval ("LinkTarget") %>" class="no_textdecoration">Devamı</a>
                                    </div>
                            </ItemTemplate>
                        </asp:Repeater>

 
				  </div>
				  </div>
				</div>
			</div>

     <div class="row margin-t-20">
            <div class="col-lg-12 text-center">
                <asp:HyperLink ID="hplBanner" runat="server">
                    <asp:Image ID="imgBanner1" runat="server" CssClass="img img-responsive banner" /></asp:HyperLink>
            </div>
        </div>
			</section>

 

  