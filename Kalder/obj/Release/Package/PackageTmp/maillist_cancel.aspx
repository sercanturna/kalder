﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="maillist_cancel.aspx.cs" Inherits="Kalder.maillist_cancel" %>

<%@ Register src="include/maillistFull.ascx" tagname="maillistFull" tagprefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>E-bülten Talep/İptal Formu </title>

    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>--%>
    <!-- Cache Süresini 1 yıl veriyoruz, http header -->
    <meta http-equiv="Expires" content="30" />
        <!-- STYLES-->
    <link href="<%= ResolveUrl("~/") %>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("~/") %>css/main.min.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("~/") %>css/style.min.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("~/") %>css/responsive.min.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("~/") %>css/animate.min.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("~/") %>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<%= ResolveUrl("~/") %>css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<%= ResolveUrl("~/") %>css/input.min.css" />
    <link href="<%= ResolveUrl("~/") %>css/kalder.min.css" rel="stylesheet" type="text/css" />

      <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

    <!-- Bu sitenin kodlamasına 10.02.2017 tarihinde başlanmıştır. -->

    <!-- Bootstrap Table için plug-in -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.2/b-colvis-1.2.2/b-flash-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.1.3/r-2.1.0/rr-1.1.2/sc-1.4.2/se-1.2.0/datatables.min.css"/>

    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script>
     

        jQuery(document).ready(function ($) {

            jQuery.fn.htmlClean = function () {
                this.contents().filter(function () {
                    if (this.nodeType != 3) {
                        $(this).htmlClean();
                        return false;
                    }
                    else {
                        this.textContent = $.trim(this.textContent);
                        return !/\S/.test(this.nodeValue);
                    }
                }).remove();
                return this;
            }
             
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
     <div class="top-bar light" >
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-xs-12">

                                <!--=======  KALDER & EFQM LOGO =========-->
                                <div class="logo" style="width:100%; text-align:center;">
                                    <img src="<%= ResolveUrl("~/") %>upload/kalder_logo.jpg" class="img-responsive" />
                                </div>

                            </div>
                           
                        </div>
                    </div>
                </div>
        
        <div style="margin-top:-60px;">
        <uc1:maillistFull ID="maillistFull1" runat="server" />
    </div>
        <p style="padding:5px; text-align:center;">Not: KalDer e-bülten bildirim formu doldurulduktan sonra e-bülten göndermeyi veya göndermemeyi garanti etmez. Formu doldurmanıza rağmen posta alamıyor veya
             listeden çıkmanıza rağmen halen posta almaya devam ediyorsanız bu formu tekrar doldurmalı veya <a href="mailto:kalder@kalder.org">kalder@kalder.org</a> adresine talebinizi yazılı olarak belirtmeniz gerekmektedir.</p>



        <div id="responsivebox"></div>
         <script type="text/javascript" src="<%= ResolveUrl("~/") %>js/jquery.min.js"></script>
        <%--<script src="<%= ResolveUrl("~/") %>js/jquery-1.11.0.min.js"></script>--%>
         <script src="http://code.jquery.com/jquery-2.2.4.min.js"></script>
        <script src="<%= ResolveUrl("~/") %>js/bootstrap.min.js"></script>
        <script src="<%= ResolveUrl("~/") %>js/own-menu.min.js"></script>
        <script src="<%= ResolveUrl("~/") %>js/owl.carousel.min.js"></script>
        <script src="<%= ResolveUrl("~/") %>js/jquery.superslides.js"></script>
        <script src="<%= ResolveUrl("~/") %>js/masonry.pkgd.min.js"></script>
        <script src="<%= ResolveUrl("~/") %>js/jquery.stellar.min.js"></script>
        <script src="<%= ResolveUrl("~/") %>js/jquery-ui-1.10.3.custom.js"></script>
        <script src="<%= ResolveUrl("~/") %>js/jquery.magnific-popup.min.js"></script>
        <script src="<%= ResolveUrl("~/") %>js/jquery.isotope.min.js"></script>
        <script src="<%= ResolveUrl("~/") %>js/jquery.flexslider-min.js"></script>
        <script src="<%= ResolveUrl("~/") %>js/appointment.min.js"></script>
        <%--<script src="<%= ResolveUrl("~/") %>js/jquery.downCount.js"></script>--%>
        <%--<script src="<%= ResolveUrl("~/") %>js/counter.js"></script>--%>
        <script src="<%= ResolveUrl("~/") %>js/custom-file-input.min.js"></script>
        <script src="<%= ResolveUrl("~/") %>js/backtotop.min.js"></script>
        <script src="<%= ResolveUrl("~/") %>js/main.min.js"></script>

        <script src="<%= ResolveUrl("~/") %>js/jquery.effects.core.min.js" type="text/javascript"></script>
        <script src="<%= ResolveUrl("~/") %>js/jquery.slicknav.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
        <script type="text/javascript" src="<%= ResolveUrl("~/") %>js/jquery-carousel.min.js"></script>


        <script src="<%= ResolveUrl("~/") %>js/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
        <script src="<%= ResolveUrl("~/") %>js/perfect-scrollbar/js/min/perfect-scrollbar.min.js"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/Adminv2") %>/js/Application.js"></script>


        <%--Arkaplan resmini devre dışı bıraktık. İhtiyaç duyulması halinde bu js dosyası yeniden eklenebilir.<script type="text/javascript" src="<%= ResolveUrl("~/") %>js/supersized.3.2.7.min.js"></script>--%>
        <script type="text/javascript" src="<%= ResolveUrl("~/") %>js/datatables.js"></script>
        <%--<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.2/b-colvis-1.2.2/b-flash-1.2.2/b-html5-1.2.2/b-print-1.2.2/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.1.3/r-2.1.0/rr-1.1.2/sc-1.4.2/se-1.2.0/datatables.min.js"></script>--%>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
        <script type="text/javascript" src="<%= ResolveUrl("~/") %>js/datetime.min.js"></script>
        <%--<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.15/dataRender/datetime.js"></script>--%>
      
        <!--[if lt IE 8]>
        <script src ="http://ie7-js.googlecode.com/svn/version/2.1(beta2)/IE8.js"></script>
    <![endif]-->
        <script type="text/javascript" src="<%= ResolveUrl("~/") %>js/modernizr-2.6.2.min.js"></script>



        <!-- Social-feed css -->
        <link href="<%= Page.ResolveUrl("~/")%>js/socialFeed/jquery.socialfeed.css" rel="stylesheet" type="text/css" />
        <!-- font-awesome for social network icons -->
        <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet" />

        <script src="<%= ResolveUrl("~/") %>js/socialFeed/codebird.js"></script>
        <script src="<%= ResolveUrl("~/") %>js/socialFeed/doT.min.js"></script>
        <script src="<%= ResolveUrl("~/") %>js/socialFeed/moment.min.js"></script>
        <script src="<%= ResolveUrl("~/") %>js/socialFeed/jquery.socialfeed.js"></script>


<script>
    $(window).load(function () {
        $('.flex-banner').flexslider({
            touch: true,
            slideshow: true,
            controlNav: true,
            slideshowSpeed: 7000,
            animationSpeed: 600,
            initDelay: 0,
            start: function (slider) { // Fires when the slider loads the first slide
                var slide_count = slider.count - 1;

                $(slider)
                  .find('img.lazy:eq(0)')
                  .each(function () {
                      var src = $(this).attr('data-src');
                      $(this).attr('src', src).removeAttr('data-src');
                  });
            },
            before: function (slider) { // Fires asynchronously with each slider animation
                var slides = slider.slides,
                    index = slider.animatingTo,
                    $slide = $(slides[index]),
                    $img = $slide.find('img[data-src]'),
                    current = index,
                    nxt_slide = current + 1,
                    prev_slide = current - 1;

                $slide
                  .parent()
                  .find('img.lazy:eq(' + current + '), img.lazy:eq(' + prev_slide + '), img.lazy:eq(' + nxt_slide + ')')
                  .each(function () {
                      var src = $(this).attr('data-src');
                      $(this).attr('src', src).removeAttr('data-src');
                  });
            }
        });
    });


</script>
    </form>
   
</body>
</html>
