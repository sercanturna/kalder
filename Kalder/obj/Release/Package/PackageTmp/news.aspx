﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="news.aspx.cs" Inherits="Kalder.news" %>
 
<%@ Register Assembly="CollectionPager" Namespace="SiteUtils" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
   <%-- <script src="<%= ResolveUrl("~/") %>include/sliderengine/amazingslider.js"></script>
    <script src="<%= ResolveUrl("~/") %>include/sliderengine/initslider-2.js"></script>--%>


        <style>
    

        .contentbox {
        overflow:hidden;
        }

        .overlay {
    background: rgba(0, 172, 168, .7) !important;
}

        .breadcrumb {
        margin: 63px 0px !important;
}
            input, select, textarea {
            width:auto !important; margin:0 10px;
            }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="screeno"></div>

      <div class="clear page_container">
       <!--======= CONTENT =========-->
        <div class="content" >
                 <section class="sub-banner" style="margin-top: 178px;">
            <div class="overlay">
                <div class="container">
                    <ol class="breadcrumb">
                        <li><a href="<%= Page.ResolveUrl("~")%>Anasayfa" title="Ana Sayfa">Ana Sayfa</a></li>
                        <li><a href="<%= Page.ResolveUrl("~")%>Haberler">
                            <asp:Literal ID="ltrlBlog" runat="server"></asp:Literal></a></li>
                        <li>
                            <asp:Literal ID="ltrlnewsCat" runat="server"></asp:Literal></li>
                        <li>
                            <asp:Literal ID="ltrlNewsTitle" runat="server"></asp:Literal></li>
                    </ol>
                </div>
            </div>
        </section>

               
                    <section class="innerpages">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <i id="mobile-sidemenu-btn" class="ion-chevron-down"></i>
                            <div class="col-md-3 col-sm-3 sidemenus" id="Div1" runat="server">
                                <asp:Repeater ID="rptSidebarModules" runat="server" OnItemDataBound="rptSidebarModules_ItemDataBound">
                <ItemTemplate>
                    <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:Repeater>
                                </div>





                                <div class="col-md-9 col-sm-9">
                                    <div class="tittle tittle-2">
										<h3><asp:Literal ID="ltrlBaslik" runat="server"></asp:Literal> Kategorisine ait haberler</h3>
										<hr>
									</div>
                   <section class="blog" style="padding-top:0px;">
    <div class="container">
        
        <ul class="row">

            <asp:Repeater ID="rptNews" runat="server" OnItemDataBound="rpt_News_ItemDataBound">
                <ItemTemplate>
                    <li class="col-md-3 col-sm-6" style="padding-bottom:10px;">
                        <div class="blog-item">
                            <div class="overlay">
                                <h2><%#Eval("Baslik") %></h2>
                                <p>
                                    <%#Eval("Ozet") %>
                                    <a href="<%= ResolveUrl("~/") %><%#Ayarlar.UrlSeo(Eval("KategoriAdi").ToString()) %>/<%#Eval("HaberId") %>/<%#Ayarlar.UrlSeo(Eval("Baslik").ToString()) %>" class="more">Devamını oku »</a>
                                </p>
                            </div>
                            <asp:Image ID="img" runat="server" /><p class="news">
                                <%#Ayarlar.OzetCek(Eval("Ozet").ToString(),80) %>
                        </div>

                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
</section>
























         <section id="paginations" style="border-top:1px dashed #d3d3d3 !important; float:left; width:100% !important; padding-top:10px;">
                                <div class="sayfalama">
                                  <cc1:CollectionPager ID="CollectionPager1" runat="server" BackText=" « Önceki" 
                                    FirstText="İlk" LabelText="" LastText="Son" NextText="Sonraki »" 
                                    PageNumbersDisplay="Numbers" ResultsFormat="Haberler {0}-{1} (Toplam:{2})" ResultsStyle="font-weight:normal; font-size:12px !important; float:left; line-height:2.4; padding-right:10px;"
                                    PageSize="6" SectionPadding="5" BackNextDisplay="Buttons" BackNextLocation="Split" 
                                    MaxPages="5" PageNumbersSeparator=""></cc1:CollectionPager>
                         </div>
                                  </section>
        </div>

           
    
        <!-- / Sidebar-->

    </div>
                        </div>
                    </div>
                </div>
                        </section>
             
          
     <asp:Repeater ID="rptFooterModules" runat="server" OnItemDataBound="rptFooterModules_ItemDataBound">
            <ItemTemplate>
                <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
            </ItemTemplate>
    </asp:Repeater>

    </div>
</asp:Content>

