﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="product_list.aspx.cs" Inherits="Kalder.product_list" %>
 
<%@ Register src="include/references.ascx" tagname="references" tagprefix="uc1" %>
<%@ Register src="include/breadcumb.ascx" tagname="breadcumb" tagprefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script src="<%= Page.ResolveUrl("~")%>js/font-size.js"></script>
 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <div class="clear page_container">
  <uc2:breadcumb ID="breadcumb1" runat="server" />

    
 

         <div id="pageContent" class="page_content" runat="server">
            
            <asp:Image ID="imgPageHeader" runat="server" CssClass="pageImage"  />
            
            <h1 class="title" style="margin:0;"><span class="left"><asp:Literal ID="ltrlBaslik" runat="server"></asp:Literal></span> 
                
                <div id="controls" class="right" style="display:none;">
                    <a href="#" id="small">A</a>
                    <a href="#" id="medium" class="selected">A</a>
                    <a href="#" id="large">A</a>
                </div>
               
            </h1>
            <div class="clear"></div>
            <div class="text_content product_list">
                <ul class="products">
                    <asp:Repeater ID="rpt_Products" runat="server" OnItemDataBound="rpt_Products_ItemDataBound">
                        <ItemTemplate>
                            <li><asp:Image ID="imgNew" runat="server" ImageUrl="~/images/yeni_urun_icon.png" CssClass="new_icon" /><a href="<%#Ayarlar.UrlSeo(Eval("KatAdi").ToString()) %>/<%#Eval("SeoUrl") %>"><img src="<%=Page.ResolveUrl("~") %>upload/urunler/<%#Eval("ImageURL") %>" />
                                <p class="product_Name"><span><%#Ayarlar.OzetCek(Eval("ProductName").ToString(),40) %></span></p></a>
                                <div class="price"><%# String.Format("{0:0.00}", Eval("UnitPrice")) %> <%#Eval("Currency") %> <span style="font-size:10px;"><asp:Literal ID="ltrlKDV" runat="server"></asp:Literal></span></div>
                               <!-- <a href="<%#Ayarlar.UrlSeo(Eval("KatAdi").ToString()) %>/<%#Eval("SeoUrl") %>" class="button">Ürünü İncele</a> -->
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </div>
          
        </div>

        <div id="sideBar" class="sidebar" runat="server">
           

         
            <asp:Repeater ID="rptSidebarModules" runat="server" OnItemDataBound="rptSidebarModules_ItemDataBound">
                <ItemTemplate>
                    <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
                </ItemTemplate>
            </asp:Repeater>

           

        </div>
        <!-- / Sidebar-->

    </div>
         <asp:Repeater ID="rptFooterModules" runat="server" OnItemDataBound="rptFooterModules_ItemDataBound">
            <ItemTemplate>
                <asp:PlaceHolder ID="phModules" runat="server"></asp:PlaceHolder>
            </ItemTemplate>
    </asp:Repeater>
 
</asp:Content>

