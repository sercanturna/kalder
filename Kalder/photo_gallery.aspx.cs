﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder
{
    public partial class photo_gallery : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string GalleryCat, GalleryCatId, GalleryId;
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                GalleryCat = RouteData.Values["GalleryCat"].ToString().Replace("-","_").Replace("%20","_");
                ltrlphotoCat.Text = Ayarlar.BasHarfBuyuk(GalleryCat);

                
                
            }
            catch (Exception)
            {

            }

            GetGalleryCat();

            if (GalleryCat == null)
            {
                rptImages.Visible = false;
                GetPhotos();
                ltrlBaslik.Text = "Foto Galeri";
            }
            else
            {
                GalleryCatId = db.GetDataCell("Select GaleriId from GaleriKategori where KategoriAdi like '%"+GalleryCat+"%'");
                GetPhotosById(GalleryCatId);

                ltrlBaslik.Text = GalleryCat + " Resimleri";
            }

            GetSidebarModules();
            GetContentModules();
            GetFooterModules();

            //ltrlTel.Text = db.GetDataCell("Select Tel from GenelAyarlar");

            ////GalleryId = RouteData.Values["HaberId"].ToString();

            ///* Facebook Likebox*/
            //string fblink = db.GetDataCell("Select Facebook from GenelAyarlar");

            //if (fblink == "" || fblink == null)
            //    pnl_facebook_like.Visible = false;
            //else
            //{
            //    LtrlFbox.Text = "<div class=\"fb-like-box\" data-href=\"" + fblink + "\" data-width=\"218\" data-colorscheme=\"light\" data-show-faces=\"true\" data-header=\"true\" data-stream=\"false\" data-show-border=\"true\"></div>";
            //    // ltrlFbComments.Text = "<div class=\"fb-comments yorum\"  data-href=\"" + Request.Url.Authority + Request.Url.AbsolutePath + "\" data-numposts=\"5\" data-colorscheme=\"light\"></div>";
            //}

        }

        public void GetPhotosById(string Id)
        {
            try
            {
                DataTable dt = db.GetDataTable("Select * from GaleriResim as gr inner join GaleriKategori as gk on gr.GaleriId=gk.GaleriId where gr.GaleriId=" + Id + " and gr.YayinDurumu=1 order by gr.ResimSiraNo");

                string ResimSayisi = db.GetDataCell("select ResimSayisi from GaleriKategori where GaleriId=" + Id);

              
                if (ResimSayisi == "")
                {
                    ResimSayisi = "0";
                }

                CollectionPager1.PageSize = Convert.ToInt32(ResimSayisi);
                CollectionPager1.DataSource = dt.DefaultView;
                CollectionPager1.BindToControl = rptImages;
                rptImages.DataSource = CollectionPager1.DataSourcePaged;
                rptImages.DataBind();

                ltrlCollectionPagerSayfaSayisi.Text = "<div style=\"display:none;\" id=\"ColPageNum\">" + CollectionPager1.PageCount.ToString() + "</div>";

                ltrlCollectionNesneSayisi.Text = "<div style=\"display:none;\" id=\"ColPageItems\">" + CollectionPager1.PageSize.ToString() + "</div>";

            }
            catch (Exception )
            {
                
              //  throw;
            }
           
        }

        public void GetPhotos()
        {
            //DataTable dt = db.GetDataTable("Select * from GaleriResim as gr inner join GaleriKategori as gk on gr.GaleriId=gk.GaleriId where gr.YayinDurumu=1 and gk.YayinDurumu=1 order by ResimSiraNo");

            //CollectionPager1.DataSource = dt.DefaultView;
            //CollectionPager1.BindToControl = rptImages;
            //rptImages.DataSource = CollectionPager1.DataSourcePaged;
            //rptImages.DataBind();


             DataTable dt = db.GetDataTable("Select * from GaleriKategori where YayinDurumu=1 order by KategoriSiraNo");
             CollectionPager1.PageSize = 12;
             CollectionPager1.DataSource = dt.DefaultView;
             CollectionPager1.BindToControl = rptPhotoCat;
             rptPhotoCat.DataSource = CollectionPager1.DataSourcePaged;
             rptPhotoCat.DataBind();

             ltrlCollectionPagerSayfaSayisi.Text = "<div style=\"display:none;\" id=\"ColPageNum\">" + CollectionPager1.PageCount.ToString() + "</div>";

             ltrlCollectionNesneSayisi.Text = "<div style=\"display:none;\" id=\"ColPageItems\">" + CollectionPager1.PageSize.ToString() + "</div>";
             
        }

        public void GetGalleryCat()
        {
             DataTable dt = db.GetDataTable("Select * from GaleriKategori where YayinDurumu=1 order by KategoriSiraNo");
            //rpt_leftmenu.DataSource = dt;
            //rpt_leftmenu.DataBind();

            rptGallerySlider.DataSource = dt;
            rptGallerySlider.DataBind();


            rptGalleryThumb.DataSource = dt;
            rptGalleryThumb.DataBind();
        }

        public void GetSidebarModules()
        {
            DataTable dt = db.GetDataTable("select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=6 and mss.IsActive=1 and m.SectionId=2 order by mss.OrderNo");
            rptSidebarModules.DataSource = dt;
            rptSidebarModules.DataBind();

            if (dt.Rows.Count == 0)
            {
                sideBar.Visible = false;
                pageContent.Style.Add("width", "100%");
            }
        }

        public void GetContentModules()
        {
            DataTable dt = db.GetDataTable("select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=6 and mss.IsActive=1 and m.SectionId=1 order by mss.OrderNo");
            rptContentModules.DataSource = dt;
            rptContentModules.DataBind();
        }

        public void GetFooterModules()
        {
            DataTable dt = db.GetDataTable("select * from Modules as m inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId where mss.SegmentId=6 and mss.IsActive=1 and m.SectionId=5 order by mss.OrderNo");
            rptFooterModules.DataSource = dt;
            rptFooterModules.DataBind();
        }



        protected void rptSidebarModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);


                    if (drv["ModuleId"].ToString() == "11")
                    {
                        PropertyInfo[] info = uc.GetType().GetProperties();


                        foreach (PropertyInfo item in info)
                        {
                            if (item.CanWrite)
                            {
                                if (item.Name == "PageIDwithURL")
                                    item.SetValue(uc, GalleryCat, null);

                            }
                        }

                    }
                }
            }




        }

        protected void rptContentModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);


                    PropertyInfo[] info = uc.GetType().GetProperties();


                    foreach (PropertyInfo item in info)
                    {
                        if (item.CanWrite)
                        {
                            if (item.Name == "PageIDwithURL")
                                item.SetValue(uc, GalleryCat, null);

                        }
                    }

                }
            }




        }

        protected void rptFooterModules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                PlaceHolder ph = (PlaceHolder)e.Item.FindControl("phModules");


                if (drv["ModuleContent"].ToString() != null && drv["ModuleContent"].ToString() != "")
                {
                    UserControl uc = (UserControl)Page.LoadControl("~/include/" + drv["ModuleContent"].ToString());
                    uc.ID = drv["ModuleId"].ToString();
                    ph.Controls.Add(uc);
                }
            }




        }
        public void Page_PreInit()
        {
            // Set the Theme for the page. Post-data is not currently loaded
            // Use trace="true" to see the Form collection
            //this.Theme = db.GetDataCell("Select Tema from GenelAyarlar");
            //if (Request.Form != null && Request.Form.Count > 0)
            //    this.Theme = this.Request.Form[4].Trim();
        }

        protected void rptImages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                

                string resimAdi = DataBinder.Eval(e.Item.DataItem, "ResimAdi").ToString();
                string KategoriAdi = DataBinder.Eval(e.Item.DataItem, "KategoriAdi").ToString();
                string ResimAciklama = DataBinder.Eval(e.Item.DataItem, "Aciklama").ToString();


                string resimYolu = Server.MapPath("~/upload/Galeri/" + KategoriAdi +"/"+resimAdi);


                Literal ltrlFigure = (Literal)e.Item.FindControl("ltrlFigure");

                Bitmap bitmap = new Bitmap(resimYolu);

                int iWidth = bitmap.Width;
                int iHeight = bitmap.Height;


              //  System.Drawing.Image img = System.Drawing.Image.FromFile(resimYolu);
                string dataSize = iWidth.ToString() +"x"+iHeight.ToString();

                string display = "";

                    if(ResimAciklama == "")
                    {
                        display = "display:none !important;";
                    }
                    else
                        display = "display:visible !important;";

string figure ="<figure itemprop=\"associatedMedia\" itemscope itemtype=\"http://schema.org/ImageObject\">"+
      "<a href=\""+Page.ResolveClientUrl("~")+"/upload/Galeri/"+KategoriAdi+"/"+resimAdi+" \" itemprop=\"contentUrl\" data-size=\""+dataSize+"\">"+
          "<img src=\"" + Page.ResolveClientUrl("~") + "/upload/Galeri/" + KategoriAdi + "/" + resimAdi + " \" itemprop=\"thumbnail\" alt=\"" + ResimAciklama + "\" />" +
      "</a>"+
        "<figcaption style=\""+display+"\" itemprop=\"caption description\">" + ResimAciklama + "</figcaption>" +
    "</figure>";


ltrlFigure.Text = figure;
               
            }
        }
    }
}