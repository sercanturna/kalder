﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;

namespace Kalder.Adminv2
{
    public partial class OnceKalite : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!Page.IsPostBack)
            { 
               // txtKutuBaslik.Text = db.GetDataCell("Select Baslik from OnceKaliteDergisi");

             
            }
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.UseAccessibleHeader = true;
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Cancel();", true);
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            string resim = db.GetDataCell("SET language turkish; Select Resim from OnceKaliteDergisi where DergiId=" + GridView1.DataKeys[e.RowIndex].Value);

            if (resim != null && resim != "")
                File.Delete(Server.MapPath("~/upload/Dergi/" + resim));

            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);


        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridView1.Rows[e.RowIndex];
            FileUpload FileUpload1 = row.FindControl("fuKatResim") as FileUpload;

            if (FileUpload1 != null && FileUpload1.HasFile)
            {
                // ScriptManager.GetCurrent(this).RegisterPostBackControl(FileUpload1);
                string ResimAdi = DateTime.Now.Millisecond + FileUpload1.FileName;

                ResimAdi = Ayarlar.ResimAdi(DateTime.Now.Millisecond + FileUpload1.PostedFile.FileName);
                FileUpload1.SaveAs(Server.MapPath("~/upload/sahte/" + ResimAdi));
                Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + ResimAdi));
                resim = Ayarlar.ResimBoyutlandir(resim, 263);
                resim.Save(Server.MapPath("~/upload/Dergi/" + ResimAdi), ImageFormat.Png);
                // Orjinal resmi bu kod satırıyla siliyorum.
                FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + ResimAdi));
                temp.Delete();



                string EskiResim = db.GetDataCell("SET language turkish; Select Resim from OnceKaliteDergisi where DergiId=" + GridView1.DataKeys[e.RowIndex].Value);
                File.Delete(Server.MapPath("~/upload/Dergi/" + EskiResim));

                OnceKaliteDergisiDataSource.UpdateParameters["Resim"].DefaultValue = ResimAdi;




                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
            }
            else
            {
                string resim = db.GetDataCell("SET language turkish; Select Resim from OnceKaliteDergisi where DergiId=" + GridView1.DataKeys[e.RowIndex].Value);
                OnceKaliteDergisiDataSource.UpdateParameters["Resim"].DefaultValue = resim;
            }



        }




        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }




        protected void lnkEkle_Click(object sender, EventArgs e)
        {
            string Yil,KatAdi, ResimAdi, SiraNo, refURL;
            ResimAdi = "";
            bool YayinDurumu;
            if (GridView1.Rows.Count > 0)
            {
                KatAdi = ((TextBox)GridView1.FooterRow.FindControl("txtKatAdiEkle")).Text;
                Yil = ((TextBox)GridView1.FooterRow.FindControl("txtYilEkle")).Text;
                refURL = ((TextBox)GridView1.FooterRow.FindControl("txtRefURL")).Text;
                FileUpload FileUpload1 = (FileUpload)GridView1.FooterRow.FindControl("fuKatResimEkle");

                if (FileUpload1.HasFile)
                {
                    ResimAdi = Ayarlar.ResimAdi(DateTime.Now.Millisecond + FileUpload1.PostedFile.FileName);

                    FileUpload1.SaveAs(Server.MapPath("~/upload/sahte/" + ResimAdi));

                    Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + ResimAdi));
                    resim = Ayarlar.ResimBoyutlandir(resim, 263);
                    resim.Save(Server.MapPath("~/upload/Dergi/" + ResimAdi), ImageFormat.Png);

                    // Orjinal resmi bu kod satırıyla siliyorum.
                    FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + ResimAdi));
                    temp.Delete();
                }

                SiraNo = ((TextBox)GridView1.FooterRow.FindControl("txtSiraNoEkle")).Text;
                CheckBox Yayin = ((CheckBox)GridView1.FooterRow.FindControl("chkDurumEkle"));



                if (Yayin.Checked == true)
                    YayinDurumu = true;
                else
                    YayinDurumu = false;
            }

            else
            {
                KatAdi = ((TextBox)GridView1.Controls[0].Controls[0].FindControl("txtKatAdiEkle")).Text;
                Yil = ((TextBox)GridView1.Controls[0].Controls[0].FindControl("txtYilEkle")).Text;
                refURL = ((TextBox)GridView1.Controls[0].Controls[0].FindControl("txtRefURL")).Text;

                FileUpload FileUpload1 = (FileUpload)GridView1.Controls[0].Controls[0].FindControl("fuKatResimEkle");

                if (FileUpload1.HasFile)
                {
                    ResimAdi = Ayarlar.ResimAdi(DateTime.Now.Millisecond + FileUpload1.PostedFile.FileName);
                    FileUpload1.SaveAs(Server.MapPath("~/upload/sahte/" + ResimAdi));

                    Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + ResimAdi));
                    resim = Ayarlar.ResimBoyutlandir(resim, 235);
                    resim.Save(Server.MapPath("~/upload/Dergi/" + ResimAdi), ImageFormat.Png);
                    // Orjinal resmi bu kod satırıyla siliyorum.
                    FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + ResimAdi));
                    temp.Delete();
                }

                SiraNo = ((TextBox)GridView1.Controls[0].Controls[0].FindControl("txtSiraNoEkle")).Text;

                CheckBox Yayin = ((CheckBox)GridView1.Controls[0].Controls[0].FindControl("chkDurumEkle"));


                if (Yayin.Checked == true)
                    YayinDurumu = true;
                else
                    YayinDurumu = false;


            }


            OnceKaliteDergisiDataSource.InsertParameters["Yil"].DefaultValue = Yil; ;
            OnceKaliteDergisiDataSource.InsertParameters["Aciklama"].DefaultValue = KatAdi; ;
            OnceKaliteDergisiDataSource.InsertParameters["Link"].DefaultValue = refURL;
            OnceKaliteDergisiDataSource.InsertParameters["Resim"].DefaultValue = ResimAdi;
            OnceKaliteDergisiDataSource.InsertParameters["OrderNo"].DefaultValue = SiraNo;
            OnceKaliteDergisiDataSource.InsertParameters["Status"].DefaultValue = YayinDurumu.ToString();


            int sonuc = OnceKaliteDergisiDataSource.Insert();

            if (sonuc > 0)
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);

        }


        protected void btnDeleteAll_Onclick(object sender, EventArgs e)
        {
            List<string> IstGalleryIdsToDelete = new List<string>();

            foreach (GridViewRow gwRow in GridView1.Rows)
            {
                if (((CheckBox)gwRow.FindControl("cbDelete")).Checked)
                {
                    int DergiId = Convert.ToInt32(GridView1.DataKeys[gwRow.RowIndex]["DergiId"]);
                    IstGalleryIdsToDelete.Add(DergiId.ToString());
                }

            }
            if (IstGalleryIdsToDelete.Count > 0)
            {
                foreach (string strDergiId in IstGalleryIdsToDelete)
                {
                    string resim = db.GetDataCell("Select Resim from OnceKaliteDergisi where DergiId=" + strDergiId);

                    if (resim != null && resim != "")
                        File.Delete(Server.MapPath("~/upload/Dergi/" + resim));

                    int sonuc = db.cmd("Delete from OnceKaliteDergisi where DergiId=" + strDergiId);

                    if (sonuc > 0)
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
                }
                GridView1.DataBind();
            }



        }


        protected void btnBaslik_Click(object sender, EventArgs e)
        {
            string RefHeader = txtKutuBaslik.Text;
            int sonuc = db.cmd("Update OnceKaliteDergisi set Baslik='" + RefHeader + "'");
            if (sonuc > 0)
                ClientScript.RegisterStartupScript(typeof(Page), "ScriptDescription", "<script type=\"text/javascript\"> alert('İşlem Başarılı!');</script>");
            else
                ClientScript.RegisterStartupScript(typeof(Page), "ScriptDescription", "<script type=\"text/javascript\"> alert('Bir Hata Oluştu, Lütfen tekrar deneyin!');</script>");
            //if (sonuc > 0)
            //    ClientScript.RegisterStartupScript(typeof(Page), "ScriptDescription", "<script type=\"text/javascript\">Success();</script>");
            //    //ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
            //else
            //    ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
        }

     
    }
}