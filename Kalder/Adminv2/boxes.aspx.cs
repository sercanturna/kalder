﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;

namespace Kalder.Adminv2
{
    public partial class boxes : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                getBox();
            }
        }


        public void getBox()
        {
            DataTable dt = db.GetDataTable("Select * from Boxes");

            CollectionPager1.DataSource = dt.DefaultView;

            CollectionPager1.BindToControl = rpt_Boxes;
            rpt_Boxes.DataSource = CollectionPager1.DataSourcePaged;
            rpt_Boxes.DataBind();

            txtKutuBaslik.Text = db.GetDataCell("Select boxHeader from GenelAyarlar");
        }


        protected void btnDeleteAll_Onclick(object sender, EventArgs e)
        {
            foreach (RepeaterItem ri in rpt_Boxes.Items)
            {
                CheckBox chk = (CheckBox)ri.FindControl("chkContainer");
                HiddenField hd = (HiddenField)ri.FindControl("hbItem");

                if (chk.Checked)
                {
                    DataTable dt = db.GetDataTable("Select * from Boxes where BoxId=" + hd.Value);

                    foreach (DataRow dr in dt.Rows)
                    {
                        File.Delete(Server.MapPath("~/upload/Box/" + dr["ImageURL"].ToString()));
                        db.guncelle("delete from Boxes where BoxId=" + hd.Value, up1);
                    }
                }
            }


           // Response.Redirect("boxes.aspx");
            getBox();


        }


        protected void btnBaslik_Click(object sender, EventArgs e)
        {
            string BoxHeader = txtKutuBaslik.Text;
            int sonuc = db.cmd("Update GenelAyarlar set BoxHeader='" + BoxHeader + "'");
            if (sonuc > 0)
                ClientScript.RegisterStartupScript(typeof(Page), "ScriptDescription", "<script type=\"text/javascript\"> alert('İşlem Başarılı!');</script>");
            else
                ClientScript.RegisterStartupScript(typeof(Page), "ScriptDescription", "<script type=\"text/javascript\"> alert('Bir Hata Oluştu, Lütfen tekrar deneyin!');</script>");
        }
    }
}