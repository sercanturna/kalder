﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class admin_permissions : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();

        protected void Page_Load(object sender, EventArgs e)
        {
            //Userinfo Coockie'sinin olup olmadığını denetliyoruz.
            HttpCookie CportalUserCookie = Request.Cookies["UserInfo"];

            //Sisteme giriş yapmış olan admin.
            string CurrentId;

            if (CportalUserCookie != null)
            {
                CurrentId = CportalUserCookie["AdminId"];
            }
            else
                CurrentId = Session["AdminId"].ToString();




            if (!this.Page.IsPostBack)
            {
                DataRow drCurrentAdmin = db.GetDataRow("Select * from Admin where AdminId=" + CurrentId);

                if (drCurrentAdmin["YetkiId"].ToString() != "1")
                {
                    pnlContent.Visible = false;
                    pnlAlert.Visible = true;
                }
                else
                {
                    pnlContent.Visible = true;
                    pnlAlert.Visible = false;

                    string Id = Request.QueryString["AId"].ToString();

                    DataRow dr = db.GetDataRow("Select * from Admin where AdminId=" + Id);
                    ltrlName.Text = '"' + dr["AdiSoyadi"].ToString() + '"' + " yetkilerini düzenle";

                    DataRow drPermissions = db.GetDataRow("Select * from AdminPermission where AdminId=" + Id);
                    //Genel Ayarlar
                    chkListGenelAyarlar.Items.FindByValue("Modül Ekleme/Silme").Selected = Convert.ToBoolean(drPermissions["General_Module_Manage"]);
                    chkListGenelAyarlar.Items.FindByValue("Mesaj İzleme/Silme").Selected = Convert.ToBoolean(drPermissions["General_Message_Manage"]);
                    chkListGenelAyarlar.Items.FindByValue("Düzenleme").Selected = Convert.ToBoolean(drPermissions["General_IsEdit"]);
                    chkListGenelAyarlar.Items.FindByValue("Silme").Selected = Convert.ToBoolean(drPermissions["General_IsDelete"]);

                    //Sayfa Yönetimi
                    chkListSayfaYonetimi.Items.FindByValue("Sayfa Ekleme").Selected = Convert.ToBoolean(drPermissions["Page_IsInsert"]);
                    chkListSayfaYonetimi.Items.FindByValue("Görüntüleme").Selected = Convert.ToBoolean(drPermissions["Page_IsView"]);
                    chkListSayfaYonetimi.Items.FindByValue("Düzenleme").Selected = Convert.ToBoolean(drPermissions["Page_IsEdit"]);
                    chkListSayfaYonetimi.Items.FindByValue("Silme").Selected = Convert.ToBoolean(drPermissions["Page_IsDelete"]);

                    //Haber Yönetimi
                    chkListHaberYonetimi.Items.FindByValue("Kategori Yönetimi").Selected = Convert.ToBoolean(drPermissions["News_Cat_Manage"]);
                    chkListHaberYonetimi.Items.FindByValue("Haber Ekleme").Selected = Convert.ToBoolean(drPermissions["News_IsInsert"]);
                    chkListHaberYonetimi.Items.FindByValue("Görüntüleme").Selected = Convert.ToBoolean(drPermissions["News_IsView"]);
                    chkListHaberYonetimi.Items.FindByValue("Düzenleme").Selected = Convert.ToBoolean(drPermissions["News_IsEdit"]);
                    chkListHaberYonetimi.Items.FindByValue("Silme").Selected = Convert.ToBoolean(drPermissions["News_IsDelete"]);

                    //Ürün Yönetimi
                    chkListUrunYonetimi.Items.FindByValue("Kategori Yönetimi").Selected = Convert.ToBoolean(drPermissions["Product_Cat_Manage"]);
                    chkListUrunYonetimi.Items.FindByValue("Marka Yönetimi").Selected = Convert.ToBoolean(drPermissions["Product_Brand_Manage"]);
                    chkListUrunYonetimi.Items.FindByValue("Ürün Ekleme").Selected = Convert.ToBoolean(drPermissions["Product_IsInsert"]);
                    chkListUrunYonetimi.Items.FindByValue("Görüntüleme").Selected = Convert.ToBoolean(drPermissions["Product_IsView"]);
                    chkListUrunYonetimi.Items.FindByValue("Düzenleme").Selected = Convert.ToBoolean(drPermissions["Product_IsEdit"]);
                    chkListUrunYonetimi.Items.FindByValue("Silme").Selected = Convert.ToBoolean(drPermissions["Product_IsDelete"]);

                    //Albüm Yönetimi
                    chkListAlbumYonetimi.Items.FindByValue("Album Yönetimi").Selected = Convert.ToBoolean(drPermissions["Album_Cat_Manage"]);
                    chkListAlbumYonetimi.Items.FindByValue("Resim Ekleme").Selected = Convert.ToBoolean(drPermissions["Album_Image_Insert"]);
                    chkListAlbumYonetimi.Items.FindByValue("Resim Silme").Selected = Convert.ToBoolean(drPermissions["Album_Image_Delete"]);

                    //Albüm Yönetimi
                    chkListReferenceManagement.Items.FindByValue("Referans Yönetimi").Selected = Convert.ToBoolean(drPermissions["Reference_Management"]);


                }
            }


        }

        //private void chkSelectGenelAyarlar(string valueToSelect)
        //{
        //    ListItem listItem = this.chkListGenelAyarlar.Items.FindByText(valueToSelect);

        //    if (listItem != null) listItem.Selected = true;
        //}


        protected void lnkYetkiGuncelle_Click(object sender, EventArgs e)
        {
            //lnkYetkiGuncelle.Attributes.Add("data-dismiss", "modal");


            bool General_Module_Manage = chkListGenelAyarlar.Items[0].Selected;
            bool General_Message_Manage = chkListGenelAyarlar.Items[1].Selected;
            bool General_IsEdit = chkListGenelAyarlar.Items[2].Selected;
            bool General_IsDelete = chkListGenelAyarlar.Items[3].Selected;

            bool Page_IsInsert = chkListSayfaYonetimi.Items[0].Selected;
            bool Page_IsView = chkListSayfaYonetimi.Items[1].Selected;
            bool Page_IsEdit = chkListSayfaYonetimi.Items[2].Selected;
            bool Page_IsDelete = chkListSayfaYonetimi.Items[3].Selected;

            bool News_Cat_Manage = chkListHaberYonetimi.Items[0].Selected;
            bool News_IsInsert = chkListHaberYonetimi.Items[1].Selected;
            bool News_IsView = chkListHaberYonetimi.Items[2].Selected;
            bool News_IsEdit = chkListHaberYonetimi.Items[3].Selected;
            bool News_IsDelete = chkListHaberYonetimi.Items[4].Selected;

            bool Product_Cat_Manage = chkListUrunYonetimi.Items[0].Selected;
            bool Product_Brand_Manage = chkListUrunYonetimi.Items[1].Selected;
            bool Product_IsInsert = chkListUrunYonetimi.Items[2].Selected;
            bool Product_IsView = chkListUrunYonetimi.Items[3].Selected;
            bool Product_IsEdit = chkListUrunYonetimi.Items[4].Selected;
            bool Product_IsDelete = chkListUrunYonetimi.Items[5].Selected;

            bool Album_Cat_Manage = chkListAlbumYonetimi.Items[0].Selected;
            bool Album_Image_Insert = chkListAlbumYonetimi.Items[1].Selected;
            bool Album_Image_Delete = chkListAlbumYonetimi.Items[2].Selected;

            bool Reference_Management = chkListReferenceManagement.Items[0].Selected;


            string Id = Request.QueryString["AId"].ToString();

            SqlConnection conn = db.baglan();

            SqlCommand cmdGuncelle = new SqlCommand("Update AdminPermission set " +
             "General_Module_Manage=@General_Module_Manage," +
             "General_Message_Manage=@General_Message_Manage," +
             "General_IsEdit=@General_IsEdit," +
             "General_IsDelete=@General_IsDelete," +
             "Page_IsInsert=@Page_IsInsert," +
             "Page_IsView = @Page_IsView," +
             "Page_IsEdit= @Page_IsEdit," +
             "Page_IsDelete= @Page_IsDelete," +
             "News_Cat_Manage = @News_Cat_Manage," +
             "News_IsInsert= @News_IsInsert," +
             "News_IsView = @News_IsView," +
             "News_IsEdit = @News_IsEdit," +
             "News_IsDelete= @News_IsDelete," +
             "Product_Cat_Manage= @Product_Cat_Manage," +
             "Product_Brand_Manage= @Product_Brand_Manage," +
             "Product_IsInsert= @Product_IsInsert," +
             "Product_IsView= @Product_IsView," +
             "Product_IsEdit= @Product_IsEdit," +
             "Product_IsDelete= @Product_IsDelete," +
             "Album_Cat_Manage= @Album_Cat_Manage," +
             "Album_Image_Insert= @Album_Image_Insert," +
             "Album_Image_Delete= @Album_Image_Delete," +
             "Reference_Management= @Reference_Management" +
             " where AdminId=" + Id, conn);

            cmdGuncelle.Parameters.AddWithValue("General_Module_Manage", General_Module_Manage);
            cmdGuncelle.Parameters.AddWithValue("General_Message_Manage", General_Message_Manage);
            cmdGuncelle.Parameters.AddWithValue("General_IsEdit", General_IsEdit);
            cmdGuncelle.Parameters.AddWithValue("General_IsDelete", General_IsDelete);
            cmdGuncelle.Parameters.AddWithValue("Page_IsInsert", Page_IsInsert);
            cmdGuncelle.Parameters.AddWithValue("Page_IsView", Page_IsView);
            cmdGuncelle.Parameters.AddWithValue("Page_IsEdit", Page_IsEdit);
            cmdGuncelle.Parameters.AddWithValue("Page_IsDelete", Page_IsDelete);
            cmdGuncelle.Parameters.AddWithValue("News_Cat_Manage", News_Cat_Manage);
            cmdGuncelle.Parameters.AddWithValue("News_IsInsert", News_IsInsert);
            cmdGuncelle.Parameters.AddWithValue("News_IsView", News_IsView);
            cmdGuncelle.Parameters.AddWithValue("News_IsEdit", News_IsEdit);
            cmdGuncelle.Parameters.AddWithValue("News_IsDelete", News_IsDelete);
            cmdGuncelle.Parameters.AddWithValue("Product_Cat_Manage", Product_Cat_Manage);
            cmdGuncelle.Parameters.AddWithValue("Product_Brand_Manage", Product_Brand_Manage);
            cmdGuncelle.Parameters.AddWithValue("Product_IsInsert", Product_IsInsert);
            cmdGuncelle.Parameters.AddWithValue("Product_IsView", Product_IsView);
            cmdGuncelle.Parameters.AddWithValue("Product_IsEdit", Product_IsEdit);
            cmdGuncelle.Parameters.AddWithValue("Product_IsDelete", Product_IsDelete);
            cmdGuncelle.Parameters.AddWithValue("Album_Cat_Manage", Album_Cat_Manage);
            cmdGuncelle.Parameters.AddWithValue("Album_Image_Insert", Album_Image_Insert);
            cmdGuncelle.Parameters.AddWithValue("Album_Image_Delete", Album_Image_Delete);
            cmdGuncelle.Parameters.AddWithValue("Reference_Management", Reference_Management);
            int sonuc = 0;
            try
            {
                sonuc = cmdGuncelle.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Response.Write(ex.Message);
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:ErrorWithMsg('" + ex.Message + "');", true);
            }


            if (sonuc > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:CloseModal();", true);


            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:ErrorWithMsg('İşlem Sırasında Bir Hata Oluştu!');", true);

        }



        void CheckUncheckAll(bool tf, ControlCollection controls)
        {
            foreach (Control ctl in controls)
            {
                if (ctl is CheckBoxList)
                {
                    for (int index = 0; index < ((CheckBoxList)ctl).Items.Count; ++index)
                    {
                        ((CheckBoxList)ctl).Items[index].Selected = tf;
                    }
                }
                if (ctl.Controls.Count > 0)
                    CheckUncheckAll(tf, ctl.Controls);
            }
        }

    }
}