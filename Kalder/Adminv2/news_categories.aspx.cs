﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;

namespace Kalder.Adminv2
{
    public partial class news_categories : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                txtKutuBaslik.Text = db.GetDataCell("Select NewsHeader from GenelAyarlar");

                bool ShowMenu = Convert.ToBoolean(db.GetDataCell("Select ShowNews from GenelAyarlar"));
                if (ShowMenu)
                {
                    btnShowMenu.Text = "Menüden Kaldır";
                    btnShowMenu.CssClass = "btn btn-danger";
                }
                else
                {
                    btnShowMenu.Text = "Menüde Göster";
                    btnShowMenu.CssClass = "btn btn-success btn-danger";
                }
            }
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.UseAccessibleHeader = true;
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Cancel();", true);
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            string resim = db.GetDataCell("select KategoriResmi from HaberKategori where KategoriId=" + GridView1.DataKeys[e.RowIndex].Value);
            File.Delete(Server.MapPath("~/upload/" + resim));




            db.cmd("Delete HaberResimleri from HaberResimleri as hr inner join haberler as h on h.HaberId=hr.HaberId inner join HaberKategori as hk on h.KategoriId = hk.KategoriId  where h.KategoriId=" + GridView1.DataKeys[e.RowIndex].Value);
            db.cmd("Delete from Haberler where KategoriId=" + GridView1.DataKeys[e.RowIndex].Value);

            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridView1.Rows[e.RowIndex];
            FileUpload FileUpload1 = row.FindControl("fuKatResim") as FileUpload;
            TextBox txtKatAdi = row.FindControl("txtKatAdi") as TextBox;

            if (FileUpload1 != null && FileUpload1.HasFile)
            {
                // ScriptManager.GetCurrent(this).RegisterPostBackControl(FileUpload1);
                string ResimAdi = DateTime.Now.Millisecond + FileUpload1.FileName;

                ResimAdi = Ayarlar.ResimAdi(DateTime.Now.Millisecond + FileUpload1.PostedFile.FileName);
                FileUpload1.SaveAs(Server.MapPath("~/upload/sahte/" + ResimAdi));
                Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + ResimAdi));
                resim = Ayarlar.ResimBoyutlandir(resim, 500);
                resim.Save(Server.MapPath("~/upload/" + ResimAdi), ImageFormat.Jpeg);
                // Orjinal resmi bu kod satırıyla siliyorum.
                FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + ResimAdi));
                temp.Delete();



                string EskiResim = db.GetDataCell("Select KategoriResmi from HaberKategori where KategoriId=" + GridView1.DataKeys[e.RowIndex].Value);
                File.Delete(Server.MapPath("~/upload/" + EskiResim));

                SqlDataSource1.UpdateParameters["KategoriResmi"].DefaultValue = ResimAdi;





                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
            }
            else
            {
                string resim = db.GetDataCell("select KategoriResmi from HaberKategori where KategoriId=" + GridView1.DataKeys[e.RowIndex].Value);
                SqlDataSource1.UpdateParameters["KategoriResmi"].DefaultValue = resim;
            }


            SqlDataSource1.UpdateParameters["SeoURL"].DefaultValue = Ayarlar.UrlSeo(txtKatAdi.Text);

        }




        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }




        protected void lnkEkle_Click(object sender, EventArgs e)
        {
            string KatAdi, ResimAdi, SiraNo;
            bool YayinDurumu, MenuDurumu;
            if (GridView1.Rows.Count > 0)
            {
                KatAdi = ((TextBox)GridView1.FooterRow.FindControl("txtKatAdiEkle")).Text;
                FileUpload FileUpload1 = (FileUpload)GridView1.FooterRow.FindControl("fuKatResimEkle");

                if (FileUpload1.HasFile)
                {
                    ResimAdi = Ayarlar.ResimAdi(DateTime.Now.Millisecond + FileUpload1.PostedFile.FileName);

                    FileUpload1.SaveAs(Server.MapPath("~/upload/sahte/" + ResimAdi));

                    Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + ResimAdi));
                    resim = Ayarlar.ResimBoyutlandir(resim, 500);
                    resim.Save(Server.MapPath("~/upload/" + ResimAdi), ImageFormat.Jpeg);

                    // Orjinal resmi bu kod satırıyla siliyorum.
                    FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + ResimAdi));
                    temp.Delete();
                }
                else
                    ResimAdi = "resim_yok.jpg";

                SiraNo = ((TextBox)GridView1.FooterRow.FindControl("txtSiraNoEkle")).Text;
                CheckBox Yayin = ((CheckBox)GridView1.FooterRow.FindControl("chkDurumEkle"));
                CheckBox Menu = ((CheckBox)GridView1.FooterRow.FindControl("chkMenuEkle"));


                if (Yayin.Checked == true)
                    YayinDurumu = true;
                else
                    YayinDurumu = false;

                if (Menu.Checked == true)
                    MenuDurumu = true;
                else
                    MenuDurumu = false;
            }

            else
            {
                KatAdi = ((TextBox)GridView1.Controls[0].Controls[0].FindControl("txtKatAdiEkle")).Text;
                FileUpload FileUpload1 = (FileUpload)GridView1.Controls[0].Controls[0].FindControl("fuKatResimEkle");
                 if (FileUpload1.HasFile)
                {
                ResimAdi = Ayarlar.ResimAdi(DateTime.Now.Millisecond + FileUpload1.PostedFile.FileName);
                FileUpload1.SaveAs(Server.MapPath("~/upload/sahte/" + ResimAdi));
                Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + ResimAdi));
                resim = Ayarlar.ResimBoyutlandir(resim, 500);
                resim.Save(Server.MapPath("~/upload/" + ResimAdi), ImageFormat.Jpeg);
                // Orjinal resmi bu kod satırıyla siliyorum.
                FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + ResimAdi));
                temp.Delete();
                }
                 else
                     ResimAdi = "resim_yok.jpg";

                SiraNo = ((TextBox)GridView1.Controls[0].Controls[0].FindControl("txtSiraNoEkle")).Text;

                CheckBox Yayin = ((CheckBox)GridView1.Controls[0].Controls[0].FindControl("chkDurumEkle"));
                CheckBox Menu = ((CheckBox)GridView1.Controls[0].Controls[0].FindControl("chkMenuEkle"));

                if (Yayin.Checked == true)
                    YayinDurumu = true;
                else
                    YayinDurumu = false;

                if (Menu.Checked == true)
                    MenuDurumu = true;
                else
                    MenuDurumu = false;
            }



            SqlDataSource1.InsertParameters["KategoriAdi"].DefaultValue = KatAdi;
            SqlDataSource1.InsertParameters["KategoriResmi"].DefaultValue = ResimAdi;
            SqlDataSource1.InsertParameters["KategoriSiraNo"].DefaultValue = SiraNo;
            SqlDataSource1.InsertParameters["YayinDurumu"].DefaultValue = YayinDurumu.ToString();
            SqlDataSource1.InsertParameters["MenuGorunumu"].DefaultValue = MenuDurumu.ToString();
            SqlDataSource1.InsertParameters["SeoURL"].DefaultValue = Ayarlar.UrlSeo(KatAdi);
            SqlDataSource1.InsertParameters["EklenmeTarihi"].DefaultValue = DateTime.Now.ToShortDateString();

            int sonuc = SqlDataSource1.Insert();

            if (sonuc > 0)
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);

        }


        protected void btnDeleteAll_Onclick(object sender, EventArgs e)
        {
            List<string> IstGalleryIdsToDelete = new List<string>();

            foreach (GridViewRow gwRow in GridView1.Rows)
            {
                if (((CheckBox)gwRow.FindControl("cbDelete")).Checked)
                {
                    int KategoriId = Convert.ToInt32(GridView1.DataKeys[gwRow.RowIndex]["KategoriId"]);
                    IstGalleryIdsToDelete.Add(KategoriId.ToString());
                }

            }
            if (IstGalleryIdsToDelete.Count > 0)
            {
                foreach (string strGaleriId in IstGalleryIdsToDelete)
                {
                    string resim = db.GetDataCell("Select KategoriResmi from HaberKategori where KategoriId=" + strGaleriId);
                    if(resim !="resim_yok.jpg")
                    File.Delete(Server.MapPath("~/upload/" + resim));

                    int sonuc = db.cmd("Delete from HaberKategori where KategoriId=" + strGaleriId);

                    if (sonuc > 0)
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
                }
                GridView1.DataBind();
            }



        }

        protected void btnShowMenu_Click(object sender, EventArgs e)
        {
            bool ShowMenu = Convert.ToBoolean(db.GetDataCell("Select ShowNews from GenelAyarlar"));
            int sonuc;
            if (ShowMenu)
                sonuc = db.cmd("Update GenelAyarlar set ShowNews=0");
            else
                sonuc = db.cmd("Update GenelAyarlar set ShowNews=1");
            if (sonuc > 0)
                Response.Redirect("news_categories.aspx");
            else
                ClientScript.RegisterStartupScript(typeof(Page), "ScriptDescription", "<script type=\"text/javascript\"> alert('Bir Hata Oluştu, Lütfen tekrar deneyin!');</script>");
        }

        protected void btnBaslik_Click(object sender, EventArgs e)
        {
            string NewsHeader = txtKutuBaslik.Text;
            int sonuc = db.cmd("Update GenelAyarlar set NewsHeader='" + NewsHeader + "'");
            if (sonuc > 0)
                ClientScript.RegisterStartupScript(typeof(Page), "ScriptDescription", "<script type=\"text/javascript\"> alert('İşlem Başarılı!');</script>");
            else
                ClientScript.RegisterStartupScript(typeof(Page), "ScriptDescription", "<script type=\"text/javascript\"> alert('Bir Hata Oluştu, Lütfen tekrar deneyin!');</script>");
        }
    }
}