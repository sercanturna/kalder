﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class edit_product_categories : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string CatId;
        DataRow dr;

        protected void Page_Load(object sender, EventArgs e)
        {
         

            CatId = Request.QueryString["cId"].ToString();
            dr = db.GetDataRow("Select * from UrunKategori where UrunKatId=" + CatId);

            if (!IsPostBack)
            {

                GetData(CatId);

            }
        }

        public void GetCat()
        {
            //  DataTable dtKat= db.GetDataTable("select * from UrunKategori");

            DataTable dtKat = db.GetProductCat();

            dropKat.DataSource = dtKat;
            dropKat.DataValueField = dtKat.Columns["UrunKatId"].ColumnName.ToString();
            dropKat.DataTextField = dtKat.Columns["KategoriAdi"].ColumnName.ToString();
            dropKat.DataBind();
            dropKat.Items.Insert(0, new ListItem("– Yok –", "0"));

        }


        public void GetData(string CatId)
        {

            GetCat();

            if (dr["UstKatId"].ToString() == "0")
                chkMenudeGoster.Enabled = true;
            else
            {
                chkMenudeGoster.Checked = false;
                chkMenudeGoster.Enabled = false;
            }

            dropKat.SelectedValue = dr["UstKatId"].ToString();
            // dropKat.Items.RemoveAt(Convert.ToInt32(dr["UrunKatId"].ToString()));

            //  dropKat.Items.Remove(dropKat.Items.FindByValue("UrunKatId"));

            ListItem removeItem = dropKat.Items.FindByValue(dr["UrunKatId"].ToString());
            dropKat.Items.Remove(removeItem);



            txtKategoriAdi.Text = dr["KatAdi"].ToString();
            txtDetay.Text = dr["Aciklama"].ToString();
            imgResim.ImageUrl = "~/upload/UrunKategoriResimleri/340/" + dr["Resim"].ToString();
            txtSiraNo.Text = dr["SiraNo"].ToString();
            txtSutun.Text = dr["Sutun"].ToString();

            bool durum = Convert.ToBoolean(dr["YayinDurumu"]);
            if (durum == false)
                drpDurum.SelectedValue = "0";
            else
                drpDurum.SelectedValue = "1";

            bool showmenu = Convert.ToBoolean(dr["MenudeGoster"]);
            if (showmenu == false)
                chkMenudeGoster.Checked = false;
            else
                chkMenudeGoster.Checked = true;

            txtSeoUrl.Text = dr["Seo_url"].ToString();
            txtTitle.Text = dr["Meta_Title"].ToString();
            txtDescription.Text = dr["Meta_Desc"].ToString();
            tags.Text = dr["Meta_Keyword"].ToString();


        }

        protected void btn_KategoriGuncelle_Click(object sender, EventArgs e)
        {

            string resimadi = db.GetDataCell("select Resim from UrunKategori where UrunKatId=" + CatId);
            string uzanti = "";
            bool ShowMenu;
            if (chkMenudeGoster.Checked)
                ShowMenu = true;
            else
                ShowMenu = false;



            if (fuResim.HasFile)
            {
                uzanti = Path.GetExtension(fuResim.PostedFile.FileName);
                resimadi = Ayarlar.OzetCek(Ayarlar.UrlSeo(txtKategoriAdi.Text), 10).Trim() + "_kategoriresim_" + DateTime.Now.Day + uzanti;
                fuResim.SaveAs(Server.MapPath("~/upload/sahte/" + resimadi));

                Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + resimadi));
                resim = Ayarlar.ResimBoyutlandir(resim, 770);

                resim.Save(Server.MapPath("~/upload/UrunKategoriResimleri/770/" + resimadi), ImageFormat.Jpeg);
                // Resmi önce 280 klasörüne kayıt ediyoruz.

                resim = Ayarlar.ResimBoyutlandir(resim, 340);
                resim.Save(Server.MapPath("~/upload/UrunKategoriResimleri/340/" + resimadi), ImageFormat.Jpeg);
                // Resmi sonrada 220 klasörüne kayıt ediyoruz.

                FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + resimadi));
                temp.Delete();
            }

            SqlConnection baglanti = db.baglan();
            SqlCommand cmdGuncelle = new SqlCommand("Update UrunKategori set " +
            "UstKatId=@UstKatId," +
            "KatAdi=@KatAdi," +
            "MenudeGoster=@MenudeGoster," +
            "Meta_Desc=@Meta_Desc," +
            "Meta_Title=@Meta_Title," +
            "Meta_Keyword=@Meta_Keyword," +
            "Aciklama=@Aciklama," +
            "Seo_url=@Seo_url," +
            "YayinDurumu=@YayinDurumu," +
            "Sutun=@Sutun," +
            "Resim=@Resim," +
            "EklenmeTarihi=@EklenmeTarihi," +
            "SiraNo=@SiraNo where UrunKatId=" + CatId,
            baglanti);



            cmdGuncelle.Parameters.AddWithValue("UstKatId", dropKat.SelectedValue);
            cmdGuncelle.Parameters.AddWithValue("KatAdi", txtKategoriAdi.Text);
            cmdGuncelle.Parameters.AddWithValue("MenudeGoster", ShowMenu);
            cmdGuncelle.Parameters.AddWithValue("Meta_Desc", txtDescription.Text);
            cmdGuncelle.Parameters.AddWithValue("Meta_Title", txtTitle.Text);
            cmdGuncelle.Parameters.AddWithValue("Meta_Keyword", tags.Text);
            cmdGuncelle.Parameters.AddWithValue("Aciklama", txtDetay.Text);
            cmdGuncelle.Parameters.AddWithValue("Seo_url", txtSeoUrl.Text);
            cmdGuncelle.Parameters.AddWithValue("YayinDurumu", drpDurum.SelectedValue);
            cmdGuncelle.Parameters.AddWithValue("Sutun", txtSutun.Text);
            cmdGuncelle.Parameters.AddWithValue("Resim", resimadi);
            cmdGuncelle.Parameters.AddWithValue("EklenmeTarihi", DateTime.Now.ToString("MM.dd.yyyy hh:mm:ss"));
            cmdGuncelle.Parameters.AddWithValue("SiraNo", txtSiraNo.Text);


            string PatentControl = db.GetDataCell("select UstKatId from UrunKategori where UrunKatId=" + CatId);


            db.cmd("update UrunKategori set YayinDurumu=" + drpDurum.SelectedValue + " where UstKatId=" + CatId);

            int sonuc = cmdGuncelle.ExecuteNonQuery();



            if (sonuc > 0)
            {
                // Ayarlar.Alert.result();
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                Ayarlar.Alert.Show("İşlem Başarı ile gerçekleşti");
                //Response.Redirect("edit_product_categories.aspx?cId="+CatId);
                Response.Redirect("product_categories.aspx");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
            }

        }

        protected void dropKat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dropKat.SelectedValue != "0")
            {
                chkMenudeGoster.Checked = false;
                chkMenudeGoster.Enabled = false;
                txtSutun.Enabled = false;
            }
            else
            {
                chkMenudeGoster.Enabled = true;
                txtSutun.Enabled = true;
            }
        }
    }
}