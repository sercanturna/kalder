﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class add_trainer : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        int SonEklenenUrunId;
        string KatId = "";
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
               
            }
        }

       



        protected void btn_UrunEkle_Click(object sender, EventArgs e)
        {



            #region Eğitmen Tablosu Veritabanı Insert

            string resimadi = "resim_yok.jpg";
            string uzanti = "";

            if (fuResim.HasFile)
            {
                uzanti = Path.GetExtension(fuResim.PostedFile.FileName);
                resimadi = Ayarlar.UrlSeo((txtUrunAdi.Text) + DateTime.Now.Millisecond).Trim() + uzanti;
                fuResim.SaveAs(Server.MapPath("~/upload/sahte/" + resimadi));

                Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + resimadi));
                resim = Ayarlar.ResimBoyutlandir(resim, 300);

                resim.Save(Server.MapPath("~/upload/YonetimKurulu/" + resimadi), ImageFormat.Jpeg);
                // Resmi önce 760 klasörüne kayıt ediyoruz.

                //resim = Ayarlar.ResimBoyutlandirYukseklik(resim, 205);
                //resim.Save(Server.MapPath("~/upload/YonetimKurulu/sahte/" + resimadi), ImageFormat.Jpeg);
                //// Resmi sonrada 340 klasörüne kayıt ediyoruz.



                //System.Drawing.Image img = System.Drawing.Image.FromFile(Server.MapPath("~/upload/YonetimKurulu/sahte/" + resimadi));

                //System.Drawing.Image ResizedImg = CropImage(img, img.Height, 270, 60, 0);

                //ResizedImg.Save(Server.MapPath("~/upload/YonetimKurulu/" + resimadi));


                //FileInfo Thumbtemp = new FileInfo(Server.MapPath("~/upload/sahte/" + resimadi));
                //Thumbtemp.Delete();


                FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + resimadi));
                temp.Delete();
            }


            SqlConnection baglanti = db.baglan();
            SqlCommand cmd = new SqlCommand("SET language turkish; INSERT INTO BoardofDirectors(" +

            "Name, " +
            "Title, " +
            "Content, " +
            "Image, " +
            "IsActive, " +
            "OrderNo, " +
            "SeoUrl, " +
            "Meta_Title, " +
            "Meta_Keywords, " +
            "Meta_Desc) values (" +

            "@Name, " +
            "@Title, " +
                "@Content, " +
                "@Image, " +
                "@IsActive, " +
                "@OrderNo, " +
                "@SeoUrl, " +
                "@Meta_Title, " +
                "@Meta_Keywords, " +
                "@Meta_Desc) SELECT SCOPE_IDENTITY() AS YkId", baglanti);


            SqlParameter insertPrm = new SqlParameter();
            insertPrm.Direction = ParameterDirection.Output;
            // ID değerini istediğimiz alan
            insertPrm.ParameterName = "YkId";
            insertPrm.Size = 10;

            string seoURL = txtSeoUrl.Text;

            if (seoURL == "")
                seoURL = Ayarlar.UrlSeo(txtUrunAdi.Text);
            else
                seoURL = Ayarlar.UrlSeo(txtSeoUrl.Text);

            cmd.Parameters.Add(insertPrm);

            bool durum = false;
            if (drpDurum.SelectedValue != "NULL")
            {
                durum = true;
            }

            cmd.Parameters.AddWithValue("Name", txtUrunAdi.Text);
            cmd.Parameters.AddWithValue("Title", txtUnvan.Text);
            cmd.Parameters.AddWithValue("Image", resimadi);
            cmd.Parameters.AddWithValue("Content", txtDetay.Text);
            cmd.Parameters.AddWithValue("OrderNo", txtSiraNo.Text);
            cmd.Parameters.AddWithValue("IsActive", durum);
            cmd.Parameters.AddWithValue("SeoUrl", seoURL);
            cmd.Parameters.AddWithValue("Meta_Title", txtTitle.Text);
            cmd.Parameters.AddWithValue("Meta_Keywords", tags.Text);
            cmd.Parameters.AddWithValue("Meta_Desc", txtDescription.Text);

            cmd.Parameters.AddWithValue("CreatedDate", DateTime.Now.ToString());


            int sonuc = int.Parse(cmd.ExecuteScalar().ToString());
            SonEklenenUrunId = sonuc;

            // int sonuc = cmd.ExecuteNonQuery();

            // SonEklenenUrunId = (int)insertPrm.Value;

            if (sonuc > 0)
            {
                // Ayarlar.Alert.result();
                Session["SonGirilenKatId"] = KatId;
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                //GetCat();
                Response.Redirect("BoardofDirectors.aspx");
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);





            #endregion

            //#region Kategori İlişki Tablosu Veritabanı
            //ArrayList SecilenKatId = new ArrayList();


            //foreach (ListItem secilenler in chkKat.Items)
            //{
            //    //Ayarlar.Alert.Show(secilenler.Text + " - " + secilenler.Selected.ToString());

            //    if (secilenler.Selected == true)
            //    {
            //        //SecilenKatId.Add((string)secilenler.Value);
            //        SqlConnection Katbaglanti = db.baglan();
            //        SqlCommand Katcmd = new SqlCommand("Insert into TrainersBranchesReleations(" +
            //        "BranchId, TrainerId) values (" +
            //        "@BranchId,@TrainerId) SELECT SCOPE_IDENTITY() AS TrainerId",
            //        Katbaglanti);

            //        Katcmd.Parameters.AddWithValue("@BranchId", (string)secilenler.Value);
            //        Katcmd.Parameters.AddWithValue("@TrainerId", SonEklenenUrunId);
            //        Katcmd.ExecuteNonQuery();
            //    }
            //}

            //#endregion

            //#region Resim Tablosu

            //HttpFileCollection files = Request.Files;
            //string resimadi = "resim_yok.jpg";
            //string uzanti = "";
            //for (int i = 0; i < files.Count; i++)
            //{
            //    HttpPostedFile file = files[i];
            //    if (file.ContentLength > 0)
            //    {
            //        //resimin adı
            //        string fileName = Path.GetFileName(file.FileName);
            //        //resim uzantısı
            //        uzanti = Path.GetExtension(file.FileName);
            //        //resime atanacak yeni ad
            //        resimadi = Ayarlar.UrlSeo(txtUrunAdi.Text) + DateTime.Now.Millisecond + uzanti;
            //        // Orjinal resmi kaydet
            //        file.SaveAs(Server.MapPath("../upload/sahte" + resimadi));


            //        //****************** Yeni boyutlara göre resim oluştur ***************/

            //        // Orjinal resim
            //        Bitmap resim = new Bitmap(Server.MapPath("../upload/sahte" + resimadi));
            //        // 1920px genişlikte yeni resim oluştur


            //        resim = Ayarlar.ResimBoyutlandir(resim, 800);
            //        // oluşturulan resmi kaydet
            //        resim.Save(Server.MapPath("../upload/urunler/" + resimadi), ImageFormat.Jpeg);

            //        //****************** Veri tabanına kaydet ***************//




            //        SqlConnection resimbaglanti = db.baglan();
            //        SqlCommand resimcmd = new SqlCommand("Insert into ProductImages(" +
            //        "ImageURL," +
            //        "IsPrimary," +
            //        "ProductId) values (" +
            //        "@ImageURL," +
            //        "@IsPrimary," +
            //        "@ProductId)", resimbaglanti);

            //        resimcmd.Parameters.AddWithValue("ImageURL", resimadi);
            //        resimcmd.Parameters.AddWithValue("IsPrimary", "1");
            //        resimcmd.Parameters.AddWithValue("ProductId", SonEklenenUrunId);

            //        int resimsonuc = resimcmd.ExecuteNonQuery();

            //        if (resimsonuc > 0)
            //        {
            //            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
            //             Response.Redirect("products.aspx");
            //            //lblMesaj.Text += "Dosya : <b>" + fileName + "</b> başarıyla yüklendi !<br />";
            //        }
            //        else
            //        {
            //            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
            //        }


            //    }
            //    // Orjinal resmi isterseniz bu kod satırıyla silebilirsiniz
            //    FileInfo temp = new FileInfo(Server.MapPath("../upload/sahte" + resimadi));
            //    temp.Delete();
            //}

            //#endregion

        }

        public static System.Drawing.Image CropImage(System.Drawing.Image Image, int Height, int Width, int StartAtX, int StartAtY)
        {
            System.Drawing.Image outimage;
            MemoryStream mm = null;
            try
            {
                //check the image height against our desired image height
                if (Image.Height < Height)
                {
                    Height = Image.Height;
                }

                if (Image.Width < Width)
                {
                    Width = Image.Width;
                }

                //create a bitmap window for cropping
                Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
                bmPhoto.SetResolution(72, 72);

                //create a new graphics object from our image and set properties
                Graphics grPhoto = Graphics.FromImage(bmPhoto);
                grPhoto.SmoothingMode = SmoothingMode.AntiAlias;
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;

                //now do the crop
                grPhoto.DrawImage(Image, new Rectangle(0, 0, Width, Height), StartAtX, StartAtY, Width, Height, GraphicsUnit.Pixel);

                // Save out to memory and get an image from it to send back out the method.
                mm = new MemoryStream();
                bmPhoto.Save(mm, System.Drawing.Imaging.ImageFormat.Jpeg);
                Image.Dispose();
                bmPhoto.Dispose();
                grPhoto.Dispose();
                outimage = System.Drawing.Image.FromStream(mm);

                return outimage;
            }
            catch (Exception ex)
            {
                throw new Exception("Error cropping image, the error was: " + ex.Message);
            }
        }


        protected void dropKat_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }


}