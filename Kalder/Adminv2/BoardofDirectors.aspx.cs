﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;

namespace Kalder.Adminv2
{
    public partial class BoardofDirectors : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                // DataTable dt = db.GetDataTable("SELECT u.*,(SELECT Top 1 ImageURL FROM ProductImages WHERE ProductImages.ProductId = u.ProductId) AS resim from Products as u  where u.ProductId =(select top 1 ku.ProductId from KatUrunIliski as ku where u.ProductId = ku.ProductId)");
                //("Select * from Products as p inner join KatUrunIliski as ku on p.ProductId=ku.ProductId inner join Brands as b on p.BrandId=b.BrandId");
                DataTable dt = db.GetDataTable("Select * from BoardofDirectors order by OrderNo");
                CollectionPager1.DataSource = dt.DefaultView;
                CollectionPager1.BindToControl = rpt_Products;
                rpt_Products.DataSource = CollectionPager1.DataSourcePaged;
                rpt_Products.DataBind();
            }
        }





        protected void btnDeleteAll_Onclick(object sender, EventArgs e)
        {
            foreach (RepeaterItem ri in rpt_Products.Items)
            {
                CheckBox chk = (CheckBox)ri.FindControl("chkContainer");
                HiddenField hd = (HiddenField)ri.FindControl("hbItem");

                if (chk.Checked)
                {
                    db.cmd("delete from BoardofDirectors where YkId=" + hd.Value);
                 
                }
            }
            Response.Redirect("BoardofDirectors.aspx");
        }


        protected void rpt_Products_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
           

        }



    }

}