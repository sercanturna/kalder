﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="edit_product.aspx.cs" Inherits="Kalder.Adminv2.edit_product" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="CKFinder" Namespace="CKFinder" TagPrefix="CKFinder" %>



<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
 <script type="text/javascript" src="./js/demo/gallery.js"></script>
    <link href="css/custom.css" rel="stylesheet" />

    <script src="js/multi.js" type="text/javascript"></script>
    <link href="./js/tags/jquery.tagsinput.css" rel="stylesheet" />
    <script src="./js/tags/jquery.tagsinput.js"></script>


    <script src="js/validation/jquery.validationEngine-tr.js"></script>
    <script src="js/validation/jquery.validationEngine.js"></script>
    <link href="js/validation/validationEngine.jquery.css" rel="stylesheet" />

    <script src="../ckeditor/ckeditor.js" type="text/javascript"></script>

    <script src="../ckfinder/ckfinder.js" type="text/javascript"></script>


    <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

    <link href="css/bootstrap.fd.css" rel="stylesheet" />
    <script src="js/bootstrap.fd.js"></script>

    
    <script type="text/javascript">
        window.onload = function () {

            $("#urunler").addClass("active");

            $("#form1").validationEngine('attach', { promptPosition: "topRight", scroll: true });


            var editor = CKEDITOR.replace('<% = txtDetay.ClientID %>');
            // CKFinder.setupCKEditor(editor, '../ckfinder');

            function fn_init() {

                $("#form1").validationEngine('attach', { promptPosition: "topRight", scroll: true });


                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_initializeRequest(onEachRequest);


                function onEachRequest(sender, args) {
                    if ($("#form1").validationEngine() == false) {
                        args.set_cancel(true);
                    }
                }

            }
            // $("#form1").validationEngine('attach', { promptPosition: "topRight", scroll: true });
            function DescChar(val) {
                var len = val.value.length;
                if (len >= 201) {
                    val.value = val.value.substring(0, 200);
                } else {
                    $('#desc').text(200 - len + " Karakter");
                }
            };
            function TitleChar(val) {
                var len = val.value.length;
                if (len >= 70) {
                    val.value = val.value.substring(0, 70);
                } else {
                    $('#title').text(70 - len + " Karakter");
                }
            };
            $(function () {
                $('#content_tags').tagsInput({
                    width: 'auto',
                });
            });
        }
    </script>


    <script>

        $(function pageLoad() {
            $("#open_btn").click(function () {

                $.FileDialog({ multiple: true }).on('files.bs.filedialog', function (ev) {
                    var files = ev.files;

                    var data = new FormData();
                    for (var i = 0; i < files.length; i++) {
                        data.append(files[i].name, files[i]);
                    }

                    $.ajax({
                        url: "FileUpHandler.ashx?PageId=<%= HttpContext.Current.Request.RequestContext.RouteData.Values["pId"] %>",
                        type: "POST",
                        data: data,
                        contentType: false,
                        processData: false,
                        success: function (result) {
                            // alert(result);

                            var UpdatePanel1 = '<%=upResim.ClientID%>';

                            function ShowItems() {
                                if (UpdatePanel1 != null) {
                                    __doPostBack(UpdatePanel1, '');
                                }
                            }
                            SuccessWithMsg("Resim(ler) başarıyla yüklendi. Şayet görüntülenmiyorsa sayfayı yenileyin.")
                            ShowItems();
                        },
                        error: function (err) {
                            alert(err.statusText)
                        }
                    });
                }).on('cancel.bs.filedialog', function (ev) {
                    CancelWithMsg("Yükleme işlemini iptal ettiniz! \r Ürün resimlerinde herhangi bir değişiklik yapılmadı.");
                });
             });
        });
    </script>


</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <%--<asp:ScriptManager ID="ScriptManager1" EnablePageMethods="true" runat="server"></asp:ScriptManager>
     <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
         <ContentTemplate> --%>



    <div class="row">
        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-globe"></i>
                    <h3>Ürün Güncelle</h3>
                </div>
                <!-- /widget-header -->


                <div class="widget-content">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#genel" data-toggle="tab">Genel Bilgiler</a></li>
                            <li class=""><a href="#veriler" data-toggle="tab">Veriler</a></li>
                            <li class=""><a href="#seo" data-toggle="tab">SEO</a></li>

                        </ul>

                        <br />

                        <div class="tab-content">


                            <div class="tab-pane active" id="genel">
                                <asp:UpdatePanel ID="upGenel" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div id="edit-genel" class="form-horizontal">

                                            <div class="control-group">
                                                <label class="control-label" for="dropKat">Kategori Seçin</label>
                                                <div class="controls cats">
                                                    <asp:CheckBoxList ID="chkKat" runat="server" DataTextField="KatAdi" DataValueField="UrunKatId" RepeatLayout="UnorderedList" OnDataBound="chkKat_DataBound"></asp:CheckBoxList>
                                                    <p class="help-block">* Ekleyeceğiniz ürün için lütfen en az bir kategoriyi seçin.</p>
                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->

                                            <div class="control-group">
                                                <label class="control-label" for="dropKat">Marka Seçin</label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="DropBrand" runat="server" AutoPostBack="true" CssClass="validate[required]"></asp:DropDownList>
                                                    <p class="help-block">* Ekleyeceğiniz ürün için lütfen bir marka seçin.</p>
                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->


                                            <div class="control-group">
                                                <label class="control-label" for="txtKategoriAdi">Ürün Adı</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtUrunAdi" runat="server" Width="300px" CssClass="validate[required]"></asp:TextBox>
                                                </div>

                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->

                                            <div class="control-group">
                                                <label class="control-label" for="txtUrunKodu">Ürün Kodu</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtUrunKodu" runat="server" Width="300px" CssClass="validate[required]"></asp:TextBox>
                                                    <p class="help-block">Örn; UA7242-3.</p>
                                                </div>

                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->

                                            <div class="control-group">
                                                <label class="control-label" for="txtUrunKodu">Ürün Fiyatı</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtFiyat" runat="server" Width="100px" CssClass="validate[required]"></asp:TextBox>
                                                    <asp:DropDownList ID="dropCurrency" runat="server" Width="100px"></asp:DropDownList>

                                                    <span style="margin-left: 2%;">KDV</span>
                                                    <asp:DropDownList ID="dropTax" runat="server">
                                                        <asp:ListItem Value="0">KDV Yok</asp:ListItem>
                                                        <asp:ListItem Value="8">%8</asp:ListItem>
                                                        <asp:ListItem Value="18">%18</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->


                                            <div class="control-group">
                                                <label class="control-label" for="txtDetay">Açıklama</label>
                                                <div class="controls">
                                                    <CKEditor:CKEditorControl ID="txtDetay" runat="server">
                                                    </CKEditor:CKEditorControl>
                                                    <p class="help-block">* Ürün ile ilgili açıklamaları bu alanda belirtin.</p>
                                                </div>

                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->





                                            <div class="control-group">
                                                <label class="control-label" for="chkOnay">Durum</label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="drpDurum" runat="server" CssClass="validate[required]">
                                                        <asp:ListItem Value="">-Seçiniz-</asp:ListItem>
                                                        <asp:ListItem Value="1">Aktif</asp:ListItem>
                                                        <asp:ListItem Value="0">Pasif</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <p class="help-block">* Durumu Pasif yaparsanız ürün sitede gözükmez.</p>
                                                </div>

                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->


                                            <div class="control-group">
                                                <label class="control-label" for="txtSiraNo">Sıra No</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtSiraNo" runat="server"></asp:TextBox>
                                                    <p class="help-block">* Ürünlerin sitedeki sırasını belirtin. Eğer istemiyorsanız boş bırakın</p>
                                                </div>

                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->

                                            <div class="control-group">
                                                <label class="control-label" for="chkOnay">Yeni Ürün</label>
                                                <div class="controls">
                                                    <asp:CheckBox ID="chkYeni" runat="server" />
                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->
                                            <div class="control-group">
                                                <label class="control-label" for="chkOnay">Popüler Ürün</label>
                                                <div class="controls">
                                                    <asp:CheckBox ID="chkPopuler" runat="server" />
                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->


                                        </div>


                                        <div class="form-actions">

                                            <asp:Button ID="btn_UrunDuzenle" runat="server" CssClass="btn btn-primary" OnClick="btn_UrunDuzenle_Click" Text="Ürünü Güncelle" />
                                            <asp:Button ID="btn_Temizle" runat="server" CssClass="btn" Text="İptal" />
                                        </div>
                                        <!-- /form-actions -->


                                    </ContentTemplate>


                                </asp:UpdatePanel>






                            </div>
                            <!-- /genel -->


                            <div class="tab-pane" id="veriler">
                                <asp:UpdatePanel ID="upResim" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>

                                        <div id="edit-veriler" class="form-horizontal">

                                            <div class="control-group">
                                                <label class="control-label" for="fuResim">Ürün Resimleri</label>
                                                <div class="controls">

                                                    <input type="button" id="open_btn" class="btn btn-primary" value="upload files" />

                                                    <%--<asp:AsyncFileUpload ID="fuResim" runat="server" />--%>
                                                    <p class="help-block">* Burada seçilecek resmin 4x3 formatında olması gerekli.</p>
                                                </div>

                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->



                                            <ul class="gallery-container">
                                                <asp:Repeater ID="rptResimler" runat="server" OnItemCommand="rptResimler_OnItemCommand">
                                                    <ItemTemplate>
                                                        <li>
                                                            <a href="../upload/urunler/<%#Eval("ImageURL") %>" class="ui-lightbox">
                                                                <img src="../upload/urunler/<%#Eval("ImageURL") %>" />
                                                            </a>
                                                            <a href="../upload/urunler/<%#Eval("ImageURL") %>" class="preview"></a>

                                                            <asp:UpdatePanel ID="uplnkUpdate" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:LinkButton ID="lnkUpdate" CommandName="setPrimary" CommandArgument='<%#Eval ("ImgId") %>' runat="server" CssClass="btn btn-small btn-primary" Text="Varsayılan Yap"></asp:LinkButton></span>
                                                        <asp:LinkButton ID="lnkDelete" CommandName="del" CommandArgument='<%#Eval ("ImgId") %>' runat="server" CssClass="btn btn-small" Text="Sil" OnClientClick="return confirm('Bu resmi silmek istediğinize emin misiniz?');"></asp:LinkButton></span>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="lnkUpdate" EventName="Click" />
                                                                    <asp:AsyncPostBackTrigger ControlID="lnkDelete" EventName="Click" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>


                                                        </li>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </ul>



                                            <%--<asp:LinkButton ID="btnUploadImage" OnClick="btnUploadImage_Click" CssClass="btn btn-primary" runat="server" Text="Resim(leri) yükle"></asp:LinkButton>--%>
                                        </div>
                                    </ContentTemplate>

                                </asp:UpdatePanel>



                            </div>


                            <div class="tab-pane" id="seo">
                                <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                    <ContentTemplate>--%>
                                <div id="edit-profile2" class="form-horizontal">

                                    <div class="control-group">
                                        <label class="control-label" for="vergidaire">Seo URL</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtSeoUrl" TextMode="SingleLine" CssClass="validate[required] input-xlarge" runat="server"></asp:TextBox><div class="char" id="Div1"></div>
                                            <p class="help-block">* Google ve diğer arama motorları için adres çubuğunda gözükecek kategoriniz için bir URL belirleyin.</p>
                                        </div>
                                        <!-- /controls -->
                                    </div>
                                    <!-- /control-group -->


                                    <div class="control-group">
                                        <label class="control-label" for="vergidaire">Meta Title</label>
                                        <div class="controls">

                                            <asp:TextBox ID="txtTitle" TextMode="SingleLine" CssClass="input-xlarge" onkeyup="TitleChar(this)" runat="server"></asp:TextBox><div class="char" id="title"></div>
                                            <p class="help-block">* Google ve diğer arama motorları için bir başlık yazın.</p>
                                        </div>
                                        <!-- /controls -->
                                    </div>
                                    <!-- /control-group -->

                                    <div class="control-group">
                                        <label class="control-label" for="vergidaire">Meta Description</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtDescription" TextMode="MultiLine" CssClass="input-xlarge" onkeyup="DescChar(this)" runat="server"></asp:TextBox><div class="char" id="desc"></div>
                                            <p class="help-block">* Google ve diğer arama motorları için bir açıklama yazın.</p>
                                        </div>
                                        <!-- /controls -->
                                    </div>
                                    <!-- /control-group -->

                                    <div class="control-group">
                                        <label class="control-label" for="vergidaire">Meta Keywords</label>
                                        <div class="controls">
                                            <asp:TextBox ID="tags" CssClass="input-medium" runat="server"></asp:TextBox>
                                            <p class="help-block">* Google ve diğer arama motorları için anahtar kelimeler yazın.
                                                <br />
                                                Kelimeler arasına virgül koyarak yada Enter tuşuna basarak geçiş yapabilirsiniz.</p>
                                        </div>
                                        <!-- /controls -->
                                    </div>
                                    <!-- /control-group -->


                                </div>
                                <%--      </ContentTemplate>
                                </asp:UpdatePanel>--%>
                            </div>





                        </div>

                        <!--form action kodları buradaydı -->

                    </div>
                    <!-- /tabbable -->
                </div>
                <!-- /widget-content -->
            </div>
            <!-- /widget stacked -->
        </div>
        <!-- /span12 -->
    </div>
    <!-- /row -->

    <%--   </ContentTemplate>
 
    </asp:UpdatePanel>   --%>
</asp:Content>
