﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="add_news.aspx.cs" Inherits="Kalder.Adminv2.add_news" %>
 <%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">

    <script src="js/multi.js" type="text/javascript"></script>
     <link href="./js/tags/jquery.tagsinput.css" rel="stylesheet" />
    <script src="./js/tags/jquery.tagsinput.js"></script>
   
      <script>
          $("#haberler").addClass("active");
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 183px;
            font-family:Verdana;
            font-size:13px;
            font-weight:bold;
            color:#646464;
        }
          ul.resimler
        {
          
        }

            ul.resimler li
            {
                height:280px;
            }
    </style> 

    
    <script src="../ckeditor/ckeditor.js" type="text/javascript"></script>
 
    <script src="../ckfinder/ckfinder.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.onload = function () {
            var editor = CKEDITOR.replace('<% = txtDetay.ClientID %>');
            CKFinder.setupCKEditor(editor, '../ckfinder');
        };

        function SumChar(val) {
            var len = val.value.length;
            if (len >= 201) {
                val.value = val.value.substring(0, 200);
            } else {
                $('#desc').text(200 - len + " Karakter");
            }
        };

</script>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate></ContentTemplate>
    </asp:UpdatePanel>

            <asp:Panel ID="pnlAlert" runat="server" Visible="false">
            <div class="row">
                <div class="span12">
                    <div class="widget stacked">
                <div class="widget-header" style="background:none; background-color:#ffd800">
                    <i class="icon-exclamation-sign"></i>
                    <h3>Uyarı</h3>
                </div>
                <!-- /widget-header -->
                        <div class="widget-content">
                        Henüz bir kategori oluşturmamışsınız, lütfen <a href="news_categories.aspx">Kategori Yönetimi</a> sayfasına gidin ve bir kategori oluşturun.
                        </div>
                    </div>
                </div>
            </div>
                </asp:Panel>


    <div class="row">
        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-globe"></i>
                    <h3>Haber Ekle</h3>
                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                    <div id="edit-profile" class="form-horizontal">
    <div class="control-group">
        <label class="control-label" for="dropKat">Haber Kategorisi</label>
        <div class="controls">
            <asp:DropDownList ID="dropKat" runat="server"></asp:DropDownList>
            <p class="help-block">* Haberi ekleyeceğiniz kategoriyi seçin.</p>
        </div>
        
        <!-- /controls -->
    </div>
    <!-- /control-group -->

    <div class="control-group">
        <label class="control-label" for="txtBaslik">Başlık</label>
        <div class="controls">
            <asp:TextBox ID="txtBaslik" runat="server" Width="300px"></asp:TextBox>
        </div>
        
        <!-- /controls -->
    </div>
    <!-- /control-group -->

    <div class="control-group">
        <label class="control-label" for="txtOzet">Özet</label>
        <div class="controls">
            <asp:TextBox ID="txtOzet" runat="server" Height="60px" TextMode="MultiLine" Width="400px" onkeyup="SumChar(this)"></asp:TextBox><div class="char" id="desc"></div>
            <p class="help-block">* Haber içeriğinin bir kısmını anlatın..</p>
        </div>
        
        <!-- /controls -->
    </div>
    <!-- /control-group -->

    <div class="control-group">
        <label class="control-label" for="txtDetay">Detay</label>
        <div class="controls">
           
            <CKEditor:CKEditorControl ID="txtDetay" runat="server"></CKEditor:CKEditorControl>
            <p class="help-block">* Haber içeriğinin tamamını bu alanda yapılandırın.</p>
        </div>
        
        <!-- /controls -->
    </div>
    <!-- /control-group -->

<%--    <div class="control-group">
        <label class="control-label" for="fuResim">Manşet Resmi</label>
        <div class="controls">
            <asp:FileUpload ID="fuResim" runat="server" />
            <p class="help-block">* Burada seçilecek resmin 4x3 formatında olması gerekli.</p>
        </div>
        
        <!-- /controls -->
    </div>--%>
    <!-- /control-group -->

    <div class="control-group">
        <label class="control-label" for="fuDetayResim">Resim galerisi</label>
        <div class="controls">
             <asp:FileUpload ID="fuDetayResim" runat="server" CssClass="multi" />
            <p class="help-block">* Habere ait fotoğraf galerisi oluşturabilirsiniz.</p>
        </div>
        
        <!-- /controls -->
    </div>
    <!-- /control-group -->

    <div class="control-group">
        <label class="control-label" for="fuResim">Video</label>
        <div class="controls">
             <asp:TextBox ID="txtVideo" runat="server" Height="50px" TextMode="MultiLine" Width="400px"></asp:TextBox>
            <p class="help-block">* Youtube dan alınan embed kodunu yapıştırın eğer yoksa boş bırakın.</p>
        </div>
        
        <!-- /controls -->
    </div>
    <!-- /control-group -->

    <div class="control-group">
        <label class="control-label" for="chkOnay">Durum</label>
        <div class="controls">
             <asp:DropDownList ID="drpDurum" runat="server">
                 <asp:ListItem Value="">-Seçiniz-</asp:ListItem>
                 <asp:ListItem Value="1">Aktif</asp:ListItem>
                 <asp:ListItem Value="0">Pasif</asp:ListItem>
             </asp:DropDownList>
            <p class="help-block">* Durumu Pasif yaparsanız haber sitede gözükmez.</p>
        </div>
        
        <!-- /controls -->
    </div>
    <!-- /control-group -->

    <div class="control-group">
        <label class="control-label" for="chkManset">Manşet Haberi</label>
        <div class="controls">
          <asp:DropDownList ID="dropManset" runat="server">
              <asp:ListItem Value="">-Seçiniz-</asp:ListItem>
              <asp:ListItem Value="1">Evet</asp:ListItem>
              <asp:ListItem Value="0">Hayır</asp:ListItem>
          </asp:DropDownList>
            <p class="help-block">* Seçili olursa haber manşete taşınacak.</p>
        </div>
        
        <!-- /controls -->
    </div>
    <!-- /control-group -->

    <div class="control-group">
        <label class="control-label" for="chkManset">Sıra No</label>
        <div class="controls">
        <asp:TextBox ID="txtSiraNo" runat="server"></asp:TextBox>
            <p class="help-block">* Haberin sitedeki sırasını belirtin. Eğer istemiyorsanız boş bırakın</p>
        </div>
        
        <!-- /controls -->
    </div>
    <!-- /control-group -->

    <div class="form-actions">
           <asp:Button ID="btn_HaberEkle" runat="server" CssClass="btn btn-primary" onclick="btn_HaberEkle_Click" Text="Haber Ekle" />
            <asp:Button ID="btn_Temizle" runat="server" CssClass="btn"  Text="Temizle" />
        </div>
    <!-- /form-actions -->
   
                        </div>
                </div>
            </div>
         </div>
    </div>
</asp:Content>