﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="profile.aspx.cs" Inherits="Kalder.Adminv2.profile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        
        <ContentTemplate>
            <div class="row">

                <!--
      	<div class="span8" style="border:1px solid #ccebac; background-color:#e0f2cb; color:#6da827; font-weight:bold;line-height:40px; margin-bottom:10px; border-radius:5px;">
             <span class="span7 left"><img src="img/notifications/success-32.png" style="height:16px;" /> İşlem Başarı ile tamamlandı.</span>
             <asp:HyperLink ID="hplClose" runat="server" OnClick="hplClose_Click"><img src="img/notifications/success-32.png" /></asp:HyperLink>
      	</div>
        -->
                <div class="span8">

                    <div class="widget stacked ">

                        <div class="widget-header">
                            <i class="icon-cog"></i>
                            <h3>Hesabım</h3>
                        </div>
                        <!-- /widget-header -->



                        <div class="widget-content">

                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#profile" data-toggle="tab">Profil</a>
                                    </li>


                                </ul>

                                <br />

                                <div class="tab-content">
                                    <div class="tab-pane active" id="profile">
                                        <div id="edit-profile" class="form-horizontal">

                                            <div class="control-group">
                                                <label class="control-label" for="txtusername">Kullanıcı Adı</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtusername" TextMode="SingleLine" CssClass="input-medium" runat="server"></asp:TextBox>
                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->

                                            <div class="control-group">
                                                <label class="control-label" for="username">Yetkiniz</label>
                                                <div class="controls">
                                                    <asp:DropDownList ID="dropYetki" TextMode="SingleLine" CssClass="input-medium" runat="server"></asp:DropDownList>

                                                    <p class="help-block">Yetkinizi siz değiştiremezsiniz, sadece "Üst Yönetici" değiştirebilir.</p>
                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->

                                            <div class="control-group">
                                                <label class="control-label" for="lastname">Ad Soyad</label>
                                                <div class="controls">

                                                    <asp:TextBox ID="txtName" TextMode="SingleLine" CssClass="input-medium" runat="server"></asp:TextBox>
                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->


                                            <div class="control-group">
                                                <label class="control-label" for="email">Eposta Adresiniz</label>
                                                <div class="controls">

                                                    <asp:TextBox ID="txtemail" CssClass="input-large" runat="server"></asp:TextBox>
                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->


                                            <br />
                                            <br />

                                            <div class="control-group">
                                                <label class="control-label" for="Password">Parola</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtpassword1" TextMode="Password" CssClass="input-medium" runat="server" ></asp:TextBox>
                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->


                                            <div class="control-group">
                                                <label class="control-label" for="Password">Parola Tekrar</label>
                                                <div class="controls">
                                                    <asp:TextBox ID="txtpassword2" TextMode="Password" CssClass="input-medium" runat="server"></asp:TextBox>

                                                </div>
                                                <!-- /controls -->
                                            </div>
                                            <!-- /control-group -->



                                            <br />


                                            <div class="form-actions">
                                                <asp:Button ID="btnKaydet" runat="server" Text="Kaydet" CssClass="btn btn-primary" OnClick="btnKaydet_Click" />
                                                <asp:Button ID="btnIptal" runat="server" Text="İptal" CssClass="btn" OnClick="btnIptal_Click" />
                                            </div>
                                            <!-- /form-actions -->

                                        </div>
                                    </div>

                                </div>


                            </div>





                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget -->

                </div>
                <!-- /span8 -->


                <div class="span4">


                    <div class="widget stacked widget-box">

                        <div class="widget-header">
                            <i class="icon-info-sign"></i>
                            <h3>Genel Bilgi</h3>
                        </div>
                        <!-- /widget-header -->

                        <div class="widget-content">
                            <p>Bu bölümden hesabınız ile ilgili bilgileri görüntüleyebilir, dilerseniz bilgilerinizi güncelleyebilirsiniz.</p>
                            <p>Parolanızı değiştirmeniz durumunda bir sonraki giriş sırasında yeni parolanızı girmeniz gerekecektir. Parola oluştururken lütfen ', < ,  > ,  &  ,  [ , ]  gibi karakterler kullanmayınız.</p>
                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget-box -->

                </div>
                <!-- /span4 -->



            </div>
        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
        
</asp:Content>
