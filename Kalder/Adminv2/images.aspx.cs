﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Kalder.Adminv2
{
    public partial class images : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string yayindurumu;

        protected void Page_Load(object sender, EventArgs e)
        {
            

            string islem = Request.QueryString["act"];
            string GId = Request.QueryString["GId"];
            string rId = Request.QueryString["rId"];





            if (!IsPostBack)
            {
                GetCat(GId);
            }



        }



        public void GetCat(string GId)
        {
            DataTable dtKat = db.GetDataTable("select * from GaleriKategori");
            dropKat.DataSource = dtKat;
            dropKat.DataValueField = dtKat.Columns["GaleriId"].ColumnName.ToString();
            dropKat.DataTextField = dtKat.Columns["KategoriAdi"].ColumnName.ToString();
            dropKat.DataBind();
            dropKat.Items.Insert(0, new ListItem("– Albüm Seçiniz –", "0"));

            

            if (GId != null)
            {
                dropKat.SelectedValue = GId;
                ltrlAlbumAdi.Text = db.GetDataCell("select KategoriAdi from GaleriKategori where GaleriId=" + GId) + " Albüm Resimleri";
                GetImages(GId);

            }
            else
            {
                ltrlAlbumAdi.Text = "Foto Galeri";
                ltrlUyari.Text = "<p>Lütfen yanda gördüğünüz resim ekleme kutusundan albüm seçiniz.<br/> Eğer albüm gözükmüyorsa lütfen <a href=\"gallery_categories.aspx\">Yeni Galeri</a> oluşturun.</p>";
                ltrlUyari.Visible = true;
            }


        }

        public void GetImages(string gID)
        {
            try
            {
                DataTable dtImages = db.GetDataTable("select * from GaleriResim as gr inner join GaleriKategori as gk on gr.GaleriId=gk.GaleriId where gr.GaleriId=" + gID);
                rptResimler.DataSource = dtImages;
                rptResimler.DataBind();
                ltrlUyari.Visible = false;

                txtResimSayisi.Text = db.GetDataCell("Select ResimSayisi from GaleriKategori where GaleriId=" + gID);
            }
            catch (Exception)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
            }

        }

        protected void dropKat_OnSelectedIndexChanged(object sender, EventArgs e)
        {

            GetImages(dropKat.SelectedValue);
            ltrlAlbumAdi.Text = db.GetDataCell("select KategoriAdi from GaleriKategori where GaleriId=" + dropKat.SelectedValue) + " Albüm Resimleri";
        }




        protected void lnkResimEkle_Onclick(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(5000);
            string uzanti = "";
            string resimadi = "resim_yok.jpg";
            string gID = dropKat.SelectedValue;
            string gAdi = db.GetDataCell("select KategoriAdi from GaleriKategori where GaleriId=" + gID);
            #region Resim Tablosu

            HttpFileCollection files = Request.Files;

            int SiraNo = Convert.ToInt32(db.GetDataCell("SELECT TOP 1 ResimSiraNo FROM GaleriResim where GaleriId=" + gID + " ORDER BY ResimId DESC"));
            if (SiraNo == null)
            {
                SiraNo = 1;
            }


            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                if (file.ContentLength > 0)
                {
                    //resimin adı
                    string fileName = Path.GetFileName(file.FileName);
                    //resim uzantısı
                    uzanti = Path.GetExtension(file.FileName);
                    //resime atanacak yeni ad
                    resimadi = Ayarlar.ResimAdi(DateTime.Now.Millisecond + fileName);
                    // Orjinal resmi kaydet
                    file.SaveAs(Server.MapPath("~/upload/sahte/" + resimadi));


                    //****************** Yeni boyutlara göre resim oluştur ***************/

                    // Orjinal resim
                    Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + resimadi));
                    // 1920px genişlikte yeni resim oluştur


                    resim = Ayarlar.ResimBoyutlandir(resim, 800);
                    // oluşturulan resmi kaydet
                    resim.Save(Server.MapPath("~/upload/Galeri/" + gAdi + "/" + resimadi), ImageFormat.Jpeg);



                    //Sıra Numarasına satır numarasını ekleyelim.
                    SiraNo++;

                    //****************** Veri tabanına kaydet ***************//

                    SqlConnection resimbaglanti = db.baglan();
                    SqlCommand resimcmd = new SqlCommand("SET language turkish; Insert into GaleriResim(" +
                    "GaleriId," +
                    "ResimAdi," +
                    "YayinDurumu," +
                    "ResimSiraNo," +
                    "EklenmeTarihi) values (" +
                    "@GaleriId," +
                    "@ResimAdi," +
                    "@YayinDurumu," +
                    "@ResimSiraNo," +
                    "@EklenmeTarihi)", resimbaglanti);

                    resimcmd.Parameters.AddWithValue("GaleriID", dropKat.SelectedValue);
                    resimcmd.Parameters.AddWithValue("ResimAdi", resimadi);
                    resimcmd.Parameters.AddWithValue("ResimSiraNo", SiraNo);
                    resimcmd.Parameters.AddWithValue("YayinDurumu", true);
                    resimcmd.Parameters.AddWithValue("EklenmeTarihi", DateTime.Now.ToString());

                    try
                    {
                        int resimsonuc = resimcmd.ExecuteNonQuery();

                        if (resimsonuc > 0)
                        {
                            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);

                        }
                        else
                            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);

                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
                        Response.Write(ex.Message);
                    }


                }
                // Orjinal resmi isterseniz bu kod satırıyla silebilirsiniz
                FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + resimadi));
                temp.Delete();
            }
            GetImages(dropKat.SelectedValue);


            #endregion
        }

        protected void rptResimler_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            // System.Web.UI.WebControls.Image imgAction = (System.Web.UI.WebControls.Image)e.Item.FindControl("imgAction");
            ImageButton lnkAction = (ImageButton)e.Item.FindControl("lnkAction");

            yayindurumu = db.GetDataCell("select YayinDurumu from GaleriResim where ResimId=" + DataBinder.Eval(e.Item.DataItem, "ResimId"));

            if (yayindurumu == "True")
            {
                lnkAction.ImageUrl = "img/play.png";
                // lnkAction.PostBackUrl = "images.aspx?act=suspend&GId=" + DataBinder.Eval(e.Item.DataItem, "GaleriId") + "&rId=" + DataBinder.Eval(e.Item.DataItem, "ResimId");
                lnkAction.ToolTip = "Yayından Kaldır";
                lnkAction.CommandName = "suspend";

            }
            else
            {
                lnkAction.ImageUrl = "img/pause.png";
                // lnkAction.PostBackUrl = "images.aspx?act=unsuspend&GId=" + DataBinder.Eval(e.Item.DataItem, "GaleriId") + "&rId=" + DataBinder.Eval(e.Item.DataItem, "ResimId");
                lnkAction.ToolTip = "Yayınla";
                lnkAction.CommandName = "unsuspend";
            }


            string aciklama = db.GetDataCell("Select Aciklama from GaleriResim where ResimId=" + DataBinder.Eval(e.Item.DataItem, "ResimId"));
            TextBox txtAciklama = (TextBox)e.Item.FindControl("txtDesc");

            txtAciklama.Text = aciklama;
        }



        protected void rptResimler_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            string GId = db.GetDataCell("select GaleriId from GaleriResim where ResimId=" + e.CommandArgument.ToString());
            string gAdi = db.GetDataCell("select KategoriAdi from GaleriKategori where GaleriId=" + GId);

            if (e.CommandName == "update")
            {
                LinkButton button = e.CommandSource as LinkButton;

                TextBox txtAciklama = (TextBox)e.Item.FindControl("txtDesc");
                db.guncelle("Update GaleriResim set Aciklama='" + txtAciklama.Text + "' where ResimId=" + e.CommandArgument.ToString(), UpdatePanel1);
            }
            if (e.CommandName == "suspend")
            {
                LinkButton button = e.CommandSource as LinkButton;
                db.guncelle("Update GaleriResim set YayinDurumu=0 where ResimId=" + e.CommandArgument.ToString(), UpdatePanel1);
                GetImages(GId);
            }

            if (e.CommandName == "unsuspend")
            {
                LinkButton button = e.CommandSource as LinkButton;
                db.guncelle("Update GaleriResim set YayinDurumu=1 where ResimId=" + e.CommandArgument.ToString(), UpdatePanel1);
                GetImages(GId);
            }

            if (e.CommandName == "del")
            {
                LinkButton button = e.CommandSource as LinkButton;

                string ResimAdi = db.GetDataCell("Select ResimAdi from GaleriResim where ResimId=" + e.CommandArgument.ToString());
                FileInfo temp = new FileInfo(Server.MapPath("../upload/Galeri/" + gAdi + "/" + ResimAdi));
                temp.Delete();
                int sonuc = db.cmd("Delete from GaleriResim where ResimId=" + e.CommandArgument.ToString());

                if (sonuc > 0)
                    ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);

                GetImages(GId);

            }

        }

        protected void btnResimSayisiGuncelle_Click(object sender, EventArgs e)
        {
            string GaleriId = dropKat.SelectedValue;

            SqlConnection baglanti = db.baglan();
            SqlCommand cmdGuncelle = new SqlCommand("SET language turkish; Update GaleriKategori set ResimSayisi=@ResimSayisi Where GaleriId=" + GaleriId, baglanti);

            cmdGuncelle.Parameters.AddWithValue("ResimSayisi", txtResimSayisi.Text);
           


            int sonuc = cmdGuncelle.ExecuteNonQuery();

            if (sonuc > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
               // Response.Redirect("banners.aspx");
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);


        }
    }
}