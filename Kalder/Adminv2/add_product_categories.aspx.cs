﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class add_product_categories : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();

        protected void Page_Load(object sender, EventArgs e)
        {
          
            if (!IsPostBack)
                GetCat();

        }

        public void GetCat()
        {
            DataTable dtKat = db.GetDataTable("select * from UrunKategori");


            dropKat.DataSource = dtKat;
            dropKat.DataValueField = dtKat.Columns["UrunKatId"].ColumnName.ToString();
            dropKat.DataTextField = dtKat.Columns["KatAdi"].ColumnName.ToString();
            dropKat.DataBind();
            dropKat.Items.Insert(0, new ListItem("– Kategori Seçiniz –", "0"));
        }


        protected void btn_HaberEkle_Click(object sender, EventArgs e)
        {

            string s = txtKategoriAdi.Text;
            if (dropKat.Items.FindByText(s) != null)
            {
                Ayarlar.Alert.Show("Bu kategoriyi zaten eklediniz!");
            }
            else
            {
                //save it here...


                if (dropKat.Items.Count > 0 && dropKat.Items.ToString().Contains(s))
                {
                    Ayarlar.Alert.Show("Bu kategoriyi zaten eklediniz!");
                }
                else
                {
                    string resimadi = "resim_yok.jpg";
                    string uzanti = "";
                    bool ShowMenu;
                    if (chkMenudeGoster.Checked)
                        ShowMenu = true;
                    else
                        ShowMenu = false;



                    if (fuResim.HasFile)
                    {
                        uzanti = Path.GetExtension(fuResim.PostedFile.FileName);
                        resimadi = Ayarlar.OzetCek(Ayarlar.UrlSeo(txtKategoriAdi.Text), 10).Trim() + "_kategoriresim_" + DateTime.Now.Day + uzanti;
                        fuResim.SaveAs(Server.MapPath("~/upload/sahte/" + resimadi));

                        Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + resimadi));
                        resim = Ayarlar.ResimBoyutlandir(resim, 770);

                        resim.Save(Server.MapPath("~/upload/UrunKategoriResimleri/770/" + resimadi), ImageFormat.Jpeg);
                        // Resmi önce 280 klasörüne kayıt ediyoruz.

                        resim = Ayarlar.ResimBoyutlandir(resim, 340);
                        resim.Save(Server.MapPath("~/upload/UrunKategoriResimleri/340/" + resimadi), ImageFormat.Jpeg);
                        // Resmi sonrada 220 klasörüne kayıt ediyoruz.

                        FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + resimadi));
                        temp.Delete();
                    }

                    SqlConnection baglanti = db.baglan();
                    SqlCommand cmdKaydet = new SqlCommand("insert into UrunKategori(" +
                    "UstKatId," +
                    "KatAdi," +
                    "MenudeGoster," +
                    "Meta_Desc," +
                    "Meta_Title," +
                    "Meta_Keyword," +
                    "Aciklama," +
                    "Seo_url," +
                    "YayinDurumu," +
                    "Sutun," +
                    "Resim," +
                    "EklenmeTarihi," +
                    "Hit," +
                    "SiraNo) values (" +
                    "@UstKatId," +
                    "@KatAdi," +
                    "@MenudeGoster," +
                    "@Meta_Desc," +
                    "@Meta_Title," +
                    "@Meta_Keyword," +
                    "@Aciklama," +
                    "@Seo_url," +
                    "@YayinDurumu," +
                    "@Sutun," +
                    "@Resim," +
                    "@EklenmeTarihi," +
                    "@Hit," +
                    "@SiraNo) Select Scope_Identity() AS UrunKatId",
                    baglanti);

                    string seoURL = txtSeoUrl.Text;
                    if (seoURL == "")
                        seoURL = Ayarlar.UrlSeo(txtKategoriAdi.Text);
                    else
                        seoURL = Ayarlar.UrlSeo(txtSeoUrl.Text);


                    bool durum = false;
                    if (drpDurum.SelectedValue != "NULL")
                    {
                        durum = true;
                    }

                    cmdKaydet.Parameters.AddWithValue("UstKatId", dropKat.SelectedValue);
                    cmdKaydet.Parameters.AddWithValue("KatAdi", txtKategoriAdi.Text);
                    cmdKaydet.Parameters.AddWithValue("MenudeGoster", ShowMenu);
                    cmdKaydet.Parameters.AddWithValue("Meta_Desc", txtDescription.Text);
                    cmdKaydet.Parameters.AddWithValue("Meta_Title", txtTitle.Text);
                    cmdKaydet.Parameters.AddWithValue("Meta_Keyword", tags.Text);
                    cmdKaydet.Parameters.AddWithValue("Aciklama", txtDetay.Text);
                    cmdKaydet.Parameters.AddWithValue("Seo_url", seoURL);
                    cmdKaydet.Parameters.AddWithValue("YayinDurumu", durum);
                    cmdKaydet.Parameters.AddWithValue("Sutun", txtSutun.Text);
                    cmdKaydet.Parameters.AddWithValue("Resim", resimadi);
                    cmdKaydet.Parameters.AddWithValue("EklenmeTarihi", DateTime.Now.ToString("MM.dd.yyyy hh:mm:ss"));
                    cmdKaydet.Parameters.AddWithValue("SiraNo", txtSiraNo.Text);
                    cmdKaydet.Parameters.AddWithValue("Hit", 0);

                    SqlParameter insertPrm = new SqlParameter();
                    insertPrm.Direction = ParameterDirection.Output;
                    // ID değerini istediğimiz alan
                    insertPrm.ParameterName = "UrunKatId";
                    insertPrm.Size = 10;

                    cmdKaydet.Parameters.Add(insertPrm);

                    int sonuc = int.Parse(cmdKaydet.ExecuteScalar().ToString());



                    if (sonuc > 0)
                    {
                        // Ayarlar.Alert.result();
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                        GetCat();
                        Response.Redirect("product_categories.aspx");
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);




                }
            }

        }

        protected void dropKat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dropKat.SelectedValue != "0")
            {
                chkMenudeGoster.Enabled = false;
                txtSutun.Enabled = false;
            }
            else
            {
                chkMenudeGoster.Enabled = true;
                txtSutun.Enabled = true;
            }
        }
    }
}