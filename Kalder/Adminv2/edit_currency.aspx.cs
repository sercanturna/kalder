﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Kalder.Adminv2
{
    public partial class edit_currency : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            //Userinfo Coockie'sinin olup olmadığını denetliyoruz.
            HttpCookie CportalUserCookie = Request.Cookies["UserInfo"];

            //Sisteme giriş yapmış olan admin.
            string CurrentId;

            if (CportalUserCookie != null)
            {
                CurrentId = CportalUserCookie["AdminId"];
            }
            else
                CurrentId = Session["AdminId"].ToString();

            if(!Page.IsPostBack)
            GetCurrency();
        }
        public void GetCurrency() {

            DataTable dt = db.GetDataTable("Select * from Currency");
            rptCurrency.DataSource = dt;
            rptCurrency.DataBind();
        }
        protected void rptCurrency_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            string CurrencyId = DataBinder.Eval(e.Item.DataItem, "CurrencyId").ToString();
            string Image = DataBinder.Eval(e.Item.DataItem, "Currency").ToString();
            string isActive = DataBinder.Eval(e.Item.DataItem, "IsActive").ToString();
            

            Literal ltrlImg = (Literal)e.Item.FindControl("ltrlImage");
            CheckBox chk = (CheckBox)e.Item.FindControl("chk");
            chk.InputAttributes.Add("value", isActive);
            chk.InputAttributes.Add("data-name", CurrencyId);

            if (isActive == "True")
                chk.Checked = true;
            else
                chk.Checked = false;



            if(CurrencyId == "1")
                ltrlImg.Text = "<img class=\"flag\" src=\"img/" + Image.ToLower() + ".gif\" />";
            else
                ltrlImg.Text = "<img class=\"flag\" src=\"http://www.tcmb.gov.tr/kurlar/kurlar_tr_dosyalar/images/" + Image + ".gif\" />";
        }



        protected void chk_CheckedChanged(object sender, EventArgs e)
        {
           //System.Threading.Thread.Sleep(5000);
            bool value = Convert.ToBoolean(((CheckBox)sender).InputAttributes["value"]);
            var id = ((CheckBox)sender).InputAttributes["data-name"];

            string cmd = "Update Currency set IsActive='"+!value+"' where CurrencyId="+id;
            db.guncelle(cmd,up1);
            
        }
    }
}