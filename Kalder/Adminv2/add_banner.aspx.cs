﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class add_banners : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();

        protected void Page_Load(object sender, EventArgs e)
        {
         

            if (!IsPostBack)
            {
                string sId = Request.QueryString["sId"];

            }
        }




        protected void btn_SlaytEkle_Click(object sender, EventArgs e)
        {
            string resimadi = "resim_yok.jpg";
            string uzanti = "";

            if (fuResim.HasFile)
            {
                uzanti = Path.GetExtension(fuResim.PostedFile.FileName);
                resimadi = Ayarlar.OzetCek(Ayarlar.UrlSeo(txtTitle.Text), 10).Trim().Replace(".","") + DateTime.Now.Millisecond + uzanti;
                fuResim.SaveAs(Server.MapPath("~/upload/sahte/" + resimadi));

                Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + resimadi));
                resim = Ayarlar.ResimBoyutlandir(resim, 970);

                resim.Save(Server.MapPath("~/upload/BannerResimleri/" + resimadi), resim.RawFormat);
                // Resmi önce SlaytResimleri klasörüne kayıt ediyoruz.

                //resim = Ayarlar.ResimBoyutlandir(resim, 220);
                //resim.Save(Server.MapPath("~/upload/SlaytResimleri/220/" + resimadi), ImageFormat.Jpeg);
                //// Resmi sonrada 220 klasörüne kayıt ediyoruz.

                FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + resimadi));
                temp.Delete();
            }

            SqlConnection baglanti = db.baglan();
            SqlCommand cmdKaydet = new SqlCommand("SET language turkish; insert into Banners(" +
            "ImageURL," +
            "Title," +
            "IsActive," +
            "OrderNo," +
            "Link," +
            "LinkTarget," +
            "Date) values (" +
            "@ImageURL," +
            "@Title," +
            "@IsActive," +
            "@OrderNo," +
            "@Link," +
            "@LinkTarget," +
            "@Date)", baglanti);


            cmdKaydet.Parameters.AddWithValue("ImageURL", resimadi);
            cmdKaydet.Parameters.AddWithValue("Title", txtTitle.Text);
            cmdKaydet.Parameters.AddWithValue("IsActive", drpDurum.SelectedValue);
            cmdKaydet.Parameters.AddWithValue("OrderNo", txtSiraNo.Text);
            cmdKaydet.Parameters.AddWithValue("Link", txtLink.Text);
            cmdKaydet.Parameters.AddWithValue("LinkTarget", dropTarget.SelectedValue);
            cmdKaydet.Parameters.AddWithValue("Date", DateTime.Now.ToString());


            int sonuc = cmdKaydet.ExecuteNonQuery();

            if (sonuc > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                Response.Redirect("banners.aspx");
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);

        }
    }
}