﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="stats.aspx.cs" Inherits="Kalder.Adminv2.stats" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
      <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>

    <div class="row">

        <div class="span12">
            <div class="widget big-stats-container stacked">
                <div class="widget-header">
                        <i class="icon-filter"></i>
                        <h3>Kayıtları Filtrele</h3>
                    </div>
                <div class="widget-content">
                    

                    <div class="form-horizontal">

                        <div class="control-group">
                            <label class="control-label" for="unvan">Tarih Aralığı</label>
                            <div class="controls">
                                <asp:TextBox ID="txtBeginDate" runat="server" TextMode="DateTime" Width="100"></asp:TextBox>
                                -
                                <asp:TextBox ID="txtEndDate" runat="server" TextMode="DateTime" Width="100" ></asp:TextBox>
                                <asp:Button ID="btnKaydet1" runat="server" Text="Filtrele" CssClass="btn btn-primary left" OnClick="btnKaydet1_Click" />
                            </div>
                           
                            <!-- /controls -->
                        </div>
                        <!-- /control-group -->
                    </div>
                    <!-- /form-horizontal -->
                </div>
                <!-- /widget-content -->
            </div>
            <!-- /widget -->
        </div>
        <!-- /span12 -->
    </div>
    <!-- /row -->

    <div class="row">

        <div class="span12">
            <div class="widget big-stats-container stacked">
                <div class="widget-header">
                    <i class="icon-star"></i>
                    <h3>Genel İstatistik</h3>
                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                    <div id="big_stats" class="cf">
                        <div class="stat">
                            <h4>Oturum Sayısı</h4>
                            <span class="value">
                                <asp:Literal ID="ltrlOturum" runat="server" Text="0"></asp:Literal></span>
                        </div>
                        <!-- .stat -->

                        <div class="stat">
                            <h4>Tekil Ziyaret</h4>
                            <span class="value">
                                <asp:Literal ID="ltrlTekil" runat="server" Text="0"></asp:Literal></span>
                        </div>
                        <!-- .stat -->

                        <div class="stat">
                            <h4>Sayfa Çevrimi</h4>
                            <span class="value">
                                <asp:Literal ID="ltrlHit" runat="server" Text="0"></asp:Literal></span>
                        </div>
                        <!-- .stat -->

                        <div class="stat">
                            <h4>Yeni Zirayet Yüzdesi</h4>
                            <span class="value">
                                <asp:Literal ID="ltrlYuzde" runat="server" Text="0"></asp:Literal></span>
                        </div>
                        <!-- .stat -->
                         <div class="stat">
                            <h4>Ortalama Süre</h4>
                            <span class="value stat-value" style="font-size:3em; color: #F90;">
                                <asp:Literal ID="ltrlOrtalamaSure" runat="server" Text="0"></asp:Literal></span>
                        </div>
                        <!-- .stat -->
                    </div>

                </div>
                <!-- /widget-content -->

            </div>
            <!-- /widget -->

        </div>
        <!-- /span12 -->
    </div>
    <!-- /row -->

    <div class="row">

        <div class="span6">

            <div class="widget stacked">

                <div class="widget-header">
                    <i class="icon-star"></i>
                    <h3>Toplam / Tekilçi Ziyaret Diyagramı</h3>
                    <%-- <span class="right"><asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="true" Width="90">
                 
                          </asp:DropDownList></span>--%>
                </div>
                <!-- /widget-header -->

                <div class="widget-content">


                    <%--<div id="pie-chart" class="chart-holder"></div>--%>
                    <asp:Chart ID="Chart1" runat="server" Width="550px" BackSecondaryColor="255, 128, 0">
                        <Series>
                            <asp:Series Name="Series1" YValuesPerPoint="5" ChartType="Spline"></asp:Series>
                            <asp:Series Name="Series2" YValuesPerPoint="5" ChartType="Spline"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                        </ChartAreas>
                        <BorderSkin BorderColor="Coral" />
                    </asp:Chart>
                </div>
                <!-- /widget-content -->

            </div>
            <!-- /widget -->




        </div>
        <!-- /span6 -->


        <div class="span6">

            <div class="widget stacked">

                <div class="widget-header">
                    <i class="icon-list-alt"></i>
                    <h3>Tarayıcı Oranı</h3>
                </div>
                <!-- /widget-header -->

                <div class="widget-content">

                    <asp:Chart ID="Chart2" runat="server" Width="550" Palette="None" PaletteCustomColors="Gainsboro; Orange; Crimson; DodgerBlue; Silver">

                        <Legends>
                            <asp:Legend Alignment="Center" Docking="Right" IsTextAutoFit="true" Name="Default" LegendStyle="Table" />
                        </Legends>
                        <Series>
                            <asp:Series Name="Series1" ChartType="Doughnut"></asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1"></asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>

                </div>
                <!-- /widget-content -->

            </div>
            <!-- /widget -->

        </div>
        <!-- /span6 -->

    </div>
    <!-- /row -->
    <div class="row">

        <div class="span4">

            <div class="widget stacked widget-table">

                <div class="widget-header">
                    <span class="icon-list-alt"></span>
                    <h3>Popüler Sayfalar</h3>
                </div>
                <!-- .widget-header -->

                <div class="widget-content">
                    <table class="table table-bordered table-striped">

                        <thead>
                            <tr>
                                <th>Sayfa Adı</th>
                                <th>Ziyaret</th>
                            </tr>
                        </thead>

                        <tbody>
                            <asp:Repeater ID="rptSayfalar" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td class="description"><a target="_blank" href="<%= ResolveUrl("~/") %><%#Eval("PageUrl") %>"><%#Eval("PageName") %></a></td>
                                        <td class="value"><span><%#Eval("Hit") %></span></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>

                        </tbody>
                    </table>

                </div>
                <!-- .widget-content -->

            </div>
            <!-- /widget -->

        </div>
        <!-- /span4 -->



        <div class="span4">

            <div class="widget stacked widget-table">

                <div class="widget-header">
                    <span class="icon-file"></span>
                    <h3>Popüler Haberler</h3>
                </div>
                <!-- .widget-header -->

                <div class="widget-content">
                    <table class="table table-bordered table-striped">

                        <thead>
                            <tr>
                                <th>Haber Başlığı</th>
                                <th>Ziyaret</th>
                            </tr>
                        </thead>

                        <tbody>
                            <asp:Repeater ID="rptHaber" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td class="description"><a target="_blank" href="<%= ResolveUrl("~/") %><%#Ayarlar.UrlSeo(Eval("KategoriAdi").ToString()) %>/<%#Eval("HaberId") %>/<%#Ayarlar.UrlSeo(Eval("Baslik").ToString()) %>"><%#Ayarlar.OzetCek(Eval ("Baslik").ToString(),45) %></a></td>
                                        <td class="value"><span><%#Eval("Hit") %></span></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>


                        </tbody>
                    </table>

                </div>
                <!-- .widget-content -->

            </div>

        </div>
        <!-- /span4 -->



        <div class="span4">

            <div class="widget stacked widget-table">

                <div class="widget-header">
                    <span class="icon-external-link"></span>
                    <h3>Lokasyon</h3>
                </div>
                <!-- .widget-header -->

                <div class="widget-content">
                    <table class="table table-bordered table-striped">

                        <thead>
                            <tr>
                                <th>Ülke</th>
                                <th>Ziyaretçi</th>
                            </tr>
                        </thead>

                        <tbody>
                            <asp:Repeater ID="rptLocation" runat="server" OnItemDataBound="rptLocation_ItemDataBound">
                                <ItemTemplate>
                                    <tr>
                                        <td class="description"><a href="#"><asp:Literal ID="ltrlLoc" runat="server"></asp:Literal></a></td>
                                        <td class="value"><span><%#Eval("Tekil") %></span></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>

                        </tbody>
                    </table>

                </div>
                <!-- .widget-content -->

            </div>

        </div>
        <!-- /span4 -->

    </div>
    <!-- /row -->
          </ContentTemplate>
         <%-- <Triggers>
              <asp:PostBackTrigger ControlID="btnKaydet1" />
          </Triggers>--%>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <link href="css/pages/reports.css" rel="stylesheet" />


    <script type="text/javascript">
        $(function () {

            var d = new Date();

            var month = d.getMonth() + 1;
            var day = d.getDate();

            var output = d.getFullYear() + '/' +
                (month < 10 ? '0' : '') + month + '/' +
                (day < 10 ? '0' : '') + day;


            $("[id$=txtEndDate]").datepicker({

                buttonImageOnly: false,
                showButtonPanel: false,
                dateFormat: 'yy/mm/dd',
                closeText: 'Kapat',
                gotoCurrent: true,
                maxDate: new Date(output),
                changeMonth: true,//ayı elle seçmeyi aktif eder
                changeYear: true,//yılı elee seçime izin verir
                dayNames: ["pazar", "pazartesi", "salı", "çarşamba", "perşembe", "cuma", "cumartesi"],//günlerin adı
                dayNamesMin: ["pa", "pzt", "sa", "çar", "per", "cum", "cmt"],//kısaltmalar
                defaultDate: +5,//takvim açılınca seçili olanı bu günden 10 gün sonra olsun dedik
                monthNamesShort: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],//ay seçim alanın düzenledik
                nextText: "ileri",//ileri butonun türkçeleştirdik

                prevText: "geri",//buda geri butonu için

            });

            $("[id$=txtBeginDate]").datepicker({

                buttonImageOnly: false,
                showButtonPanel: false,
                dateFormat: 'yy/mm/dd',
                closeText: 'Kapat',
                gotoCurrent: true,
                maxDate: new Date(output),
                changeMonth: true,//ayı elle seçmeyi aktif eder
                changeYear: true,//yılı elee seçime izin verir
                dayNames: ["pazar", "pazartesi", "salı", "çarşamba", "perşembe", "cuma", "cumartesi"],//günlerin adı
                dayNamesMin: ["pa", "pzt", "sa", "çar", "per", "cum", "cmt"],//kısaltmalar
                defaultDate: +5,//takvim açılınca seçili olanı bu günden 10 gün sonra olsun dedik
                monthNamesShort: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],//ay seçim alanın düzenledik
                nextText: "ileri",//ileri butonun türkçeleştirdik

                prevText: "geri",//buda geri butonu için

            });

            var prm = Sys.WebForms.PageRequestManager.getInstance();

            prm.add_endRequest(function () {
                var d = new Date();

                var month = d.getMonth() + 1;
                var day = d.getDate();

                var output = d.getFullYear() + '/' +
                    (month < 10 ? '0' : '') + month + '/' +
                    (day < 10 ? '0' : '') + day;


                $("[id$=txtEndDate]").datepicker({

                    buttonImageOnly: false,
                    showButtonPanel: false,
                    dateFormat: 'yy/mm/dd',
                    closeText: 'Kapat',
                    gotoCurrent: true,
                    maxDate: new Date(output),
                    changeMonth: true,//ayı elle seçmeyi aktif eder
                    changeYear: true,//yılı elee seçime izin verir
                    dayNames: ["pazar", "pazartesi", "salı", "çarşamba", "perşembe", "cuma", "cumartesi"],//günlerin adı
                    dayNamesMin: ["pa", "pzt", "sa", "çar", "per", "cum", "cmt"],//kısaltmalar
                    defaultDate: +5,//takvim açılınca seçili olanı bu günden 10 gün sonra olsun dedik
                    monthNamesShort: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],//ay seçim alanın düzenledik
                    nextText: "ileri",//ileri butonun türkçeleştirdik

                    prevText: "geri",//buda geri butonu için

                });

                $("[id$=txtBeginDate]").datepicker({

                    buttonImageOnly: false,
                    showButtonPanel: false,
                    dateFormat: 'yy/mm/dd',
                    closeText: 'Kapat',
                    gotoCurrent: true,
                    maxDate: new Date(output),
                    changeMonth: true,//ayı elle seçmeyi aktif eder
                    changeYear: true,//yılı elee seçime izin verir
                    dayNames: ["pazar", "pazartesi", "salı", "çarşamba", "perşembe", "cuma", "cumartesi"],//günlerin adı
                    dayNamesMin: ["pa", "pzt", "sa", "çar", "per", "cum", "cmt"],//kısaltmalar
                    defaultDate: +5,//takvim açılınca seçili olanı bu günden 10 gün sonra olsun dedik
                    monthNamesShort: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],//ay seçim alanın düzenledik
                    nextText: "ileri",//ileri butonun türkçeleştirdik

                    prevText: "geri",//buda geri butonu için

                });
            });


            //$.datepicker._gotoToday = function (id) {
            //    $(id).datepicker('setDate', new Date()).datepicker('hide').blur();
            //};
        });
    </script>
</asp:Content>
