﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;

namespace Kalder
{
    public partial class brands : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
          
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.UseAccessibleHeader = true;
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Cancel();", true);
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            string resim = db.GetDataCell("select BrandLogo from Brands where BrandId=" + GridView1.DataKeys[e.RowIndex].Value);

            if (resim != "" && resim != null)
                File.Delete(Server.MapPath("~/upload/" + resim));

            db.cmd("Update Products set IsActive=0 where BrandId=" + GridView1.DataKeys[e.RowIndex].Value);

            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridView1.Rows[e.RowIndex];
            FileUpload FileUpload1 = row.FindControl("fuKatResim") as FileUpload;




            if (FileUpload1 != null && FileUpload1.HasFile)
            {
                // ScriptManager.GetCurrent(this).RegisterPostBackControl(FileUpload1);
                string ResimAdi = DateTime.Now.Millisecond + FileUpload1.FileName;

                ResimAdi = Ayarlar.ResimAdi(DateTime.Now.Millisecond + FileUpload1.PostedFile.FileName);
                FileUpload1.SaveAs(Server.MapPath("~/upload/sahte/" + ResimAdi));
                Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + ResimAdi));
                resim = Ayarlar.ResimBoyutlandir(resim, 800);
                resim.Save(Server.MapPath("~/upload/" + ResimAdi), ImageFormat.Jpeg);
                // Orjinal resmi bu kod satırıyla siliyorum.
                FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + ResimAdi));
                temp.Delete();



                string EskiResim = db.GetDataCell("Select BrandLogo from Brands where BrandId=" + GridView1.DataKeys[e.RowIndex].Value);

                if (EskiResim != "")
                {
                    File.Delete(Server.MapPath("~/upload/" + EskiResim));
                }

                SqlDataSource1.UpdateParameters["BrandLogo"].DefaultValue = ResimAdi;




                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
            }
            else
            {
                string resim = db.GetDataCell("select BrandLogo from Brands where BrandId=" + GridView1.DataKeys[e.RowIndex].Value);
                SqlDataSource1.UpdateParameters["BrandLogo"].DefaultValue = resim;
            }



        }




        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }




        protected void lnkEkle_Click(object sender, EventArgs e)
        {
            string KatAdi, SiraNo;
            string ResimAdi = "";
            bool YayinDurumu;
            if (GridView1.Rows.Count > 0)
            {
                KatAdi = ((TextBox)GridView1.FooterRow.FindControl("txtKatAdiEkle")).Text;
                FileUpload FileUpload1 = (FileUpload)GridView1.FooterRow.FindControl("fuKatResimEkle");

                if (FileUpload1.HasFile)
                {

                    ResimAdi = Ayarlar.ResimAdi(DateTime.Now.Millisecond + FileUpload1.PostedFile.FileName);

                    FileUpload1.SaveAs(Server.MapPath("~/upload/sahte/" + ResimAdi));

                    Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + ResimAdi));
                    resim = Ayarlar.ResimBoyutlandir(resim, 800);
                    resim.Save(Server.MapPath("~/upload/" + ResimAdi), ImageFormat.Jpeg);

                    // Orjinal resmi bu kod satırıyla siliyorum.
                    FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + ResimAdi));
                    temp.Delete();
                }

                SiraNo = ((TextBox)GridView1.FooterRow.FindControl("txtSiraNoEkle")).Text;
                CheckBox Yayin = ((CheckBox)GridView1.FooterRow.FindControl("chkDurumEkle"));



                if (Yayin.Checked == true)
                    YayinDurumu = true;
                else
                    YayinDurumu = false;
            }

            else
            {
                KatAdi = ((TextBox)GridView1.Controls[0].Controls[0].FindControl("txtKatAdiEkle")).Text;
                FileUpload FileUpload1 = (FileUpload)GridView1.Controls[0].Controls[0].FindControl("fuKatResimEkle");

                if (FileUpload1.HasFile)
                {

                    ResimAdi = Ayarlar.ResimAdi(DateTime.Now.Millisecond + FileUpload1.PostedFile.FileName);
                    FileUpload1.SaveAs(Server.MapPath("~/upload/sahte/" + ResimAdi));
                    Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + ResimAdi));
                    resim = Ayarlar.ResimBoyutlandir(resim, 800);
                    resim.Save(Server.MapPath("~/upload/" + ResimAdi), ImageFormat.Jpeg);
                    // Orjinal resmi bu kod satırıyla siliyorum.
                    FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + ResimAdi));
                    temp.Delete();
                }

                SiraNo = ((TextBox)GridView1.Controls[0].Controls[0].FindControl("txtSiraNoEkle")).Text;

                CheckBox Yayin = ((CheckBox)GridView1.Controls[0].Controls[0].FindControl("chkDurumEkle"));


                if (Yayin.Checked == true)
                    YayinDurumu = true;
                else
                    YayinDurumu = false;


            }



            SqlDataSource1.InsertParameters["BrandName"].DefaultValue = KatAdi; ;
            SqlDataSource1.InsertParameters["BrandLogo"].DefaultValue = ResimAdi;
            SqlDataSource1.InsertParameters["OrderNo"].DefaultValue = SiraNo;
            SqlDataSource1.InsertParameters["Status"].DefaultValue = YayinDurumu.ToString();

            int sonuc = SqlDataSource1.Insert();

            if (sonuc > 0)
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
            else
            {
                Ayarlar.Alert.Show("bir sorun oluştu!");
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
            }
        }


        protected void btnDeleteAll_Onclick(object sender, EventArgs e)
        {
            List<string> IstGalleryIdsToDelete = new List<string>();

            foreach (GridViewRow gwRow in GridView1.Rows)
            {
                if (((CheckBox)gwRow.FindControl("cbDelete")).Checked)
                {
                    int BrandId = Convert.ToInt32(GridView1.DataKeys[gwRow.RowIndex]["BrandId"]);
                    IstGalleryIdsToDelete.Add(BrandId.ToString());
                }

            }
            if (IstGalleryIdsToDelete.Count > 0)
            {
                foreach (string strBrandId in IstGalleryIdsToDelete)
                {
                    string resim = db.GetDataCell("Select BrandLogo from Brands where BrandId=" + strBrandId);

                    if (resim != null && resim != "")
                        File.Delete(Server.MapPath("~/upload/" + resim));

                    int sonuc = db.cmd("Delete from Brands where BrandId=" + strBrandId);

                    if (sonuc > 0)
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
                }
                GridView1.DataBind();
            }



        }
    }
}