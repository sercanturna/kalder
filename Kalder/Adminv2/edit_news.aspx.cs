﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class edit_news : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string hId;
        string process;
        string ImgId;
        string kId = null;
        DataRow dr;
        protected void Page_Load(object sender, EventArgs e)
        {


            hId = Request.QueryString["hId"];
            process = Request.QueryString["process"];

            dr = db.GetDataRow("Select * from Haberler where HaberId=" + hId);


            if (process == "success")
                Ayarlar.Alert.Show("İşlem Başarılı");

            if (dr != null)
            {
                if (!IsPostBack)
                {

                    //if (dr["Resim"].ToString() == "resim_yok.jpg")
                    //{
                    //    lnkDeleteCuffImage.Visible = false;
                    //}
                    //else
                    //    lnkDeleteCuffImage.Visible = true;
                    
                    GetCat();


                    dropKat.SelectedValue = kId;
                    txtBaslik.Text = dr["Baslik"].ToString();
                    txtOzet.Text = dr["Ozet"].ToString();
                    txtDetay.Text = dr["Detay"].ToString();
                   // imgManset.ImageUrl = "~/upload/HaberResimleri/220/" + dr["Resim"].ToString();
                    txtVideo.Text = dr["Video"].ToString();

                    //SEO
                    txtHit.Text = dr["Hit"].ToString();
                   // txtSeoUrl.Text = dr["SeoURL"].ToString();
                    txtTitle.Text = dr["MetaTitle"].ToString();
                    txtDescription.Text = dr["MetaDesc"].ToString();
                    tags.Text = dr["MetaKeywords"].ToString();


                    string durum = dr["Durum"].ToString();
                    if (durum == "False")
                        drpDurum.SelectedValue = "0";
                    else
                        drpDurum.SelectedValue = "1";

                    string manset = dr["Manset"].ToString();
                    if (manset == "False")
                        dropManset.SelectedValue = "0";
                    else
                        dropManset.SelectedValue = "1";

                    txtSiraNo.Text = dr["SiraNo"].ToString();
                }
            }
           GetPhotoGallery(hId);
        }

        public void GetCat()
        {
            try
            {
                DataTable dtKat = db.GetDataTable("select * from HaberKategori");

                if (kId == null || kId == "")
                {
                    kId = dr["KategoriId"].ToString();
                    dropKat.SelectedValue = kId;
                }
                else
                {
                    kId = Request.QueryString["kId"];
                    dropKat.SelectedValue = kId;
                }
                dropKat.DataSource = dtKat;
                dropKat.DataValueField = dtKat.Columns["KategoriId"].ColumnName.ToString();
                dropKat.DataTextField = dtKat.Columns["KategoriAdi"].ColumnName.ToString();
                dropKat.DataBind();
                dropKat.Items.Insert(0, new ListItem("– Kategori Seçiniz –", "0"));
            }
            catch (Exception)
            {

            }

        }

        public void GetPhotoGallery(string hId)
        {
            DataTable dt = db.GetDataTable("Select * from HaberResimleri where HaberId=" + hId);
            rptResimler.DataSource = dt;
            rptResimler.DataBind();
           // if (dt.Rows.Count < 1)
             //   ltrlAlert.Visible = true;
          //  ltrlAlert.Text = "Bu haber için henüz fotoğraf galerisi oluşturmamışsınız.";


        }

        protected void rptResimler_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            string hId = Request.QueryString["hId"];

            if (e.CommandName == "setPrimary")
            {
                LinkButton button = e.CommandSource as LinkButton;
                 
                ScriptManager sm = ScriptManager.GetCurrent(this.Page);
                sm.RegisterAsyncPostBackControl(button);

                 db.guncelle("Update HaberResimleri set IsPrimary=0 where HaberId=" + hId, upResim);
                 db.guncelle("Update HaberResimleri set IsPrimary=1 where ResimId=" + e.CommandArgument.ToString() + " and HaberId=" + hId, upResim);

                 string defaultImage = db.GetDataCell("Select Resim from HaberResimleri where IsPrimary=1 and HaberId=" + hId);

               int sonuc = db.guncelle("Update Haberler set Resim ='"+defaultImage+"' where HaberId="+hId ,upResim);

                upResim.Update();
                 GetPhotoGallery(hId);
            }

            if (e.CommandName == "del")
            {
                LinkButton button = e.CommandSource as LinkButton;

                string ResimAdi = db.GetDataCell("Select Resim from HaberResimleri where ResimId=" + e.CommandArgument.ToString());
                FileInfo thumb = new FileInfo(Server.MapPath("../upload/HaberResimleri/FotoGaleri/thumb/" + ResimAdi));
                FileInfo big = new FileInfo(Server.MapPath("../upload/HaberResimleri/FotoGaleri/big/" + ResimAdi));
                thumb.Delete();
                big.Delete();
                int sonuc = db.cmd("Delete from HaberResimleri where ResimId=" + e.CommandArgument.ToString());

                if (sonuc > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
                }

                 upResim.Update();
                    GetPhotoGallery(hId);
            }
        }

        protected void btn_HaberGuncelle_Click(object sender, EventArgs e)
        {
            //string resimadi = db.GetDataCell("select Resim from Haberler where HaberId=" + hId);

            //if (resimadi == null || resimadi == "")
            //{
            //    resimadi = "resim_yok.jpg";
            //}

            //string uzanti = "";


            //if (fuResim.HasFile)
            //{
            //    uzanti = Path.GetExtension(fuResim.PostedFile.FileName);
            //    resimadi = Ayarlar.OzetCek(Ayarlar.UrlSeo(txtBaslik.Text), 10).Trim() + "_haberresim_" + DateTime.Now.Day + uzanti;
            //    fuResim.SaveAs(Server.MapPath("~/upload/sahte/" + resimadi));

            //    Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + resimadi));
            //    resim = Ayarlar.ResimBoyutlandir(resim, 512);

            //    resim.Save(Server.MapPath("~/upload/HaberResimleri/500/" + resimadi), ImageFormat.Jpeg);
            //    // Resmi önce 280 klasörüne kayıt ediyoruz.

            //    resim = Ayarlar.ResimBoyutlandir(resim, 280);
            //    resim.Save(Server.MapPath("~/upload/HaberResimleri/280/" + resimadi), ImageFormat.Jpeg);
            //    // Resmi sonrada 220 klasörüne kayıt ediyoruz.

            //    resim = Ayarlar.ResimBoyutlandir(resim, 220);
            //    resim.Save(Server.MapPath("~/upload/HaberResimleri/220/" + resimadi), ImageFormat.Jpeg);
            //    // Resmi sonrada 220 klasörüne kayıt ediyoruz.

            //    FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + resimadi));
            //    temp.Delete();
            //}

            SqlConnection baglanti = db.baglan();
            SqlCommand cmdGuncelle = new SqlCommand("SET language turkish; Update Haberler set KategoriId=@KategoriId,Baslik=@Baslik,Ozet=@Ozet,Detay=@Detay,Resim=@Resim,Video=@Video," +
           "Durum=@Durum,Manset=@Manset,Hit=@Hit,EklenmeTarihi=@EklenmeTarihi,SiraNo=@SiraNo,MetaTitle=@MetaTitle,MetaDesc=@MetaDesc,MetaKeywords=@MetaKeywords Where HaberId=" + hId, baglanti);

            cmdGuncelle.Parameters.AddWithValue("KategoriId", dropKat.SelectedValue);
            cmdGuncelle.Parameters.AddWithValue("Baslik", txtBaslik.Text);
            cmdGuncelle.Parameters.AddWithValue("Ozet", txtOzet.Text);
            cmdGuncelle.Parameters.AddWithValue("Detay", txtDetay.Text);
          //  cmdGuncelle.Parameters.AddWithValue("Resim", resimadi);
            cmdGuncelle.Parameters.AddWithValue("Resim", "");
            cmdGuncelle.Parameters.AddWithValue("Video", txtVideo.Text);
            cmdGuncelle.Parameters.AddWithValue("Durum", drpDurum.SelectedValue);
            cmdGuncelle.Parameters.AddWithValue("Manset", dropManset.SelectedValue);
            cmdGuncelle.Parameters.AddWithValue("Hit", "1");
            cmdGuncelle.Parameters.AddWithValue("EklenmeTarihi", DateTime.Now.ToString());
            cmdGuncelle.Parameters.AddWithValue("SiraNo", txtSiraNo.Text);
            cmdGuncelle.Parameters.AddWithValue("MetaTitle", txtTitle.Text);
            cmdGuncelle.Parameters.AddWithValue("MetaDesc", txtDescription.Text);
            cmdGuncelle.Parameters.AddWithValue("MetaKeywords", tags.Text);
            

            int sonuc = cmdGuncelle.ExecuteNonQuery();

            if (sonuc > 0)
            {

                kId = dr["KategoriId"].ToString();
                Response.Redirect("edit_news.aspx?hId=" + hId + "&kId=" + kId + "&process=success");
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);

        }

        protected void lnkDeleteCuffImage_Click(object sender, EventArgs e)
        {


            string Resim = db.GetDataCell("Select Resim from Haberler where HaberId=" + hId);

            if (Resim != "resim_yok.jpg")
            {

                FileInfo img220 = new FileInfo(Server.MapPath("../upload/HaberResimleri/220/" + Resim));
                FileInfo img280 = new FileInfo(Server.MapPath("../upload/HaberResimleri/280/" + Resim));
                FileInfo img500 = new FileInfo(Server.MapPath("../upload/HaberResimleri/500/" + Resim));

                img220.Delete();
                img280.Delete();
                img500.Delete();

                int sonuc = db.cmd("Update Haberler set Resim ='resim_yok.jpg' where HaberId=" + hId);

                if (sonuc > 0)
                    Response.Redirect("edit_news.aspx?hId=" + hId + "&kId=" + kId + "&process=success");
            }


            
          

        }

        protected void rptResimler_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            string resimId = DataBinder.Eval(e.Item.DataItem, "ResimId").ToString();

            string isPrimary = db.GetDataCell("Select IsPrimary from HaberResimleri where ResimId="+resimId);


            if (isPrimary=="True")
            {
                LinkButton lnkSetPrimary = (LinkButton)e.Item.FindControl("lnkUpdate");
                lnkSetPrimary.CssClass = "btn btn-small btn-success";
                lnkSetPrimary.Text = "Varsayılan Resim";

            }

            
        }
    }
}