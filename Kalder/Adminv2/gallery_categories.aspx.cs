﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;

namespace Kalder.Adminv2
{
    public partial class gallery_categories : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.UseAccessibleHeader = true;
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Cancel();", true);
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            string KatAdi = db.GetDataCell("select KategoriAdi from GaleriKategori where GaleriId=" + GridView1.DataKeys[e.RowIndex].Value);


            string path = Server.MapPath("~/upload/Galeri/" + KatAdi);
            //Klasörün olup olmadığını kontrol ediyorum.
            var directoryInfo = new DirectoryInfo(path);

            if (directoryInfo.Exists)
                Directory.Delete(path, true);


            db.cmd("Delete from GaleriResim where GaleriId=" + GridView1.DataKeys[e.RowIndex].Value);
            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);


            //string resim = db.GetDataCell("select KategoriResmi from GaleriKategori where GaleriId=" + GridView1.DataKeys[e.RowIndex].Value);
            //File.Delete(Server.MapPath("~/upload/" + resim));


            //db.cmd("Delete from GaleriResim where GaleriId=" + GridView1.DataKeys[e.RowIndex].Value);


            //DataTable dtKategoriyeAitResimler = db.GetDataTable("Select * from GaleriResim where GaleriId=" + GridView1.DataKeys[e.RowIndex].Value);

            //string KatAdi = db.GetDataCell("select KategoriAdi from GaleriKategori where GaleriId=" + GridView1.DataKeys[e.RowIndex].Value);


            //foreach (DataRow drResim in dtKategoriyeAitResimler.Rows)
            //{
            //    File.Delete(Server.MapPath("~/upload/Galeri/" + KatAdi +"/"+ drResim["ResimAdi"].ToString()));
            //    db.cmd("Delete from GaleriResim where GaleriId=" + drResim["GaleriId"].ToString());
            //}


        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = GridView1.Rows[e.RowIndex];
            FileUpload FileUpload1 = row.FindControl("fuKatResim") as FileUpload;


            string KatAdi = ((TextBox)row.FindControl("txtKatAdi")).Text;
            string OrginalKatAdi = db.GetDataCell("Select KategoriAdi from GaleriKategori where GaleriId=" + GridView1.DataKeys[e.RowIndex].Value);


            string path = Server.MapPath("~/upload/Galeri/");
            string Fromfol = OrginalKatAdi + "/";
            string Tofol = KatAdi + "/";

            //Yeni galeri klasörü oluşturuyorum..
            var directoryInfo = new DirectoryInfo(path);

            var directoryInfo2 = new DirectoryInfo(path + OrginalKatAdi);


            if (directoryInfo2.Exists)
            {
                if (Ayarlar.BasHarfBuyuk(KatAdi) != Ayarlar.BasHarfBuyuk(OrginalKatAdi))
                    Directory.Move(path + Fromfol, path + Tofol);
            }
            else
            {
                directoryInfo.CreateSubdirectory(KatAdi);
            }




            if (FileUpload1 != null && FileUpload1.HasFile)
            {
                // ScriptManager.GetCurrent(this).RegisterPostBackControl(FileUpload1);
                string ResimAdi = DateTime.Now.Millisecond + FileUpload1.FileName;

                ResimAdi = Ayarlar.ResimAdi(DateTime.Now.Millisecond + FileUpload1.PostedFile.FileName);
                FileUpload1.SaveAs(Server.MapPath("~/upload/sahte/" + ResimAdi));
                Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + ResimAdi));
                resim = Ayarlar.ResimBoyutlandir(resim, 755);

                resim.Save(Server.MapPath("~/upload/Galeri/" + KatAdi + "/" + ResimAdi), ImageFormat.Jpeg);
                // Orjinal resmi bu kod satırıyla siliyorum.
                FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + ResimAdi));
                temp.Delete();


                string EskiResim = db.GetDataCell("Select KategoriResmi from GaleriKategori where GaleriId=" + GridView1.DataKeys[e.RowIndex].Value);
                File.Delete(Server.MapPath("~/upload/Galeri/" + KatAdi + "/" + EskiResim));

                SqlDataSource1.UpdateParameters["KategoriResmi"].DefaultValue = ResimAdi;




                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
            }
            else
            {
                string resim = db.GetDataCell("select KategoriResmi from GaleriKategori where GaleriId=" + GridView1.DataKeys[e.RowIndex].Value);
                SqlDataSource1.UpdateParameters["KategoriResmi"].DefaultValue = resim;
            }



        }




        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }




        protected void lnkEkle_Click(object sender, EventArgs e)
        {
            string KatAdi;
            string SiraNo = "";
            string ResimAdi = "resim_yok.jpg";
            bool YayinDurumu = false;
            bool MenuDurumu = false;




            if (GridView1.Rows.Count > 0)
            {
                KatAdi = ((TextBox)GridView1.FooterRow.FindControl("txtKatAdiEkle")).Text;
                FileUpload FileUpload1 = (FileUpload)GridView1.FooterRow.FindControl("fuKatResimEkle");


                DataTable dt = db.GetDataTable("Select * from GaleriKategori where KategoriAdi='" + KatAdi + "'");

                if (dt.Rows.Count > 0)
                    ClientScript.RegisterStartupScript(typeof(Page), "ScriptDescription", "<script type=\"text/javascript\"> alert('Bu Albümü zaten Eklediniz'); return false;</script>");
                else
                {
                    if (FileUpload1.HasFile)
                    {
                        ResimAdi = Ayarlar.ResimAdi(DateTime.Now.Millisecond + FileUpload1.PostedFile.FileName);

                        FileUpload1.SaveAs(Server.MapPath("~/upload/sahte/" + ResimAdi));


                        //Yeni galeri klasörü oluşturuyorum..
                        var directoryInfo = new DirectoryInfo(Server.MapPath("~/upload/Galeri/"));

                        if (directoryInfo.Exists)
                        {
                            directoryInfo.CreateSubdirectory(KatAdi);
                        }

                        Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + ResimAdi));
                        resim = Ayarlar.ResimBoyutlandir(resim, 755);
                        resim.Save(Server.MapPath("~/upload/Galeri/" + KatAdi + "/" + ResimAdi), ImageFormat.Jpeg);


                        // Orjinal resmi bu kod satırıyla siliyorum.
                        FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + ResimAdi));
                        temp.Delete();

                    }

                    SiraNo = ((TextBox)GridView1.FooterRow.FindControl("txtSiraNoEkle")).Text;
                    CheckBox Yayin = ((CheckBox)GridView1.FooterRow.FindControl("chkDurumEkle"));
                    CheckBox Menu = ((CheckBox)GridView1.FooterRow.FindControl("chkMenuEkle"));


                    if (Yayin.Checked == true)
                        YayinDurumu = true;
                    else
                        YayinDurumu = false;

                    if (Menu.Checked == true)
                        MenuDurumu = true;
                    else
                        MenuDurumu = false;

                    SqlDataSource1.InsertParameters["KategoriAdi"].DefaultValue = KatAdi; ;
                    SqlDataSource1.InsertParameters["KategoriResmi"].DefaultValue = ResimAdi;
                    SqlDataSource1.InsertParameters["KategoriSiraNo"].DefaultValue = SiraNo;
                    SqlDataSource1.InsertParameters["YayinDurumu"].DefaultValue = YayinDurumu.ToString();
                    SqlDataSource1.InsertParameters["MenuGorunumu"].DefaultValue = MenuDurumu.ToString();
                    SqlDataSource1.InsertParameters["EklenmeTarihi"].DefaultValue = DateTime.Now.ToString();

                    int sonuc = SqlDataSource1.Insert();

                    if (sonuc > 0)
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
                }
            }

            else
            {
                KatAdi = ((TextBox)GridView1.Controls[0].Controls[0].FindControl("txtKatAdiEkle")).Text;

                DataTable dt = db.GetDataTable("Select * from GaleriKategori where KategoriAdi='" + KatAdi + "'");

                if (dt.Rows.Count > 0)
                {
                    ClientScript.RegisterStartupScript(typeof(Page), "ScriptDescription", "<script type=\"text/javascript\"> alert('Bu Albümü zaten Eklediniz'); return false;</script>");

                }
                else
                {
                    //Yeni galeri klasörü oluşturuyorum..
                    var directoryInfo = new DirectoryInfo(Server.MapPath("~/upload/Galeri/"));

                    if (directoryInfo.Exists)
                    {
                        directoryInfo.CreateSubdirectory(KatAdi);
                    }

                    FileUpload FileUpload1 = (FileUpload)GridView1.Controls[0].Controls[0].FindControl("fuKatResimEkle");
                    if (FileUpload1.HasFile)
                    {
                        ResimAdi = Ayarlar.ResimAdi(DateTime.Now.Millisecond + FileUpload1.PostedFile.FileName);
                        FileUpload1.SaveAs(Server.MapPath("~/upload/sahte/" + ResimAdi));
                        Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + ResimAdi));
                        resim = Ayarlar.ResimBoyutlandir(resim, 755);
                        resim.Save(Server.MapPath("~/upload/Galeri/" + KatAdi + "/" + ResimAdi), ImageFormat.Jpeg);
                        // Orjinal resmi bu kod satırıyla siliyorum.
                        FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + ResimAdi));
                        temp.Delete();
                    }

                    SiraNo = ((TextBox)GridView1.Controls[0].Controls[0].FindControl("txtSiraNoEkle")).Text;

                    CheckBox Yayin = ((CheckBox)GridView1.Controls[0].Controls[0].FindControl("chkDurumEkle"));
                    CheckBox Menu = ((CheckBox)GridView1.Controls[0].Controls[0].FindControl("chkMenuEkle"));

                    if (Yayin.Checked == true)
                        YayinDurumu = true;
                    else
                        YayinDurumu = false;

                    if (Menu.Checked == true)
                        MenuDurumu = true;
                    else
                        MenuDurumu = false;


                    SqlDataSource1.InsertParameters["KategoriAdi"].DefaultValue = KatAdi; ;
                    SqlDataSource1.InsertParameters["KategoriResmi"].DefaultValue = ResimAdi;
                    SqlDataSource1.InsertParameters["KategoriSiraNo"].DefaultValue = SiraNo;
                    SqlDataSource1.InsertParameters["YayinDurumu"].DefaultValue = YayinDurumu.ToString();
                    SqlDataSource1.InsertParameters["MenuGorunumu"].DefaultValue = MenuDurumu.ToString();
                    SqlDataSource1.InsertParameters["EklenmeTarihi"].DefaultValue = DateTime.Now.ToString();

                    int sonuc = SqlDataSource1.Insert();

                    if (sonuc > 0)
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
                }
            }






        }



        protected void btnDeleteAll_Onclick(object sender, EventArgs e)
        {
            List<string> IstGalleryIdsToDelete = new List<string>();

            foreach (GridViewRow gwRow in GridView1.Rows)
            {
                if (((CheckBox)gwRow.FindControl("cbDelete")).Checked)
                {
                    int GaleriId = Convert.ToInt32(GridView1.DataKeys[gwRow.RowIndex]["GaleriId"]);
                    IstGalleryIdsToDelete.Add(GaleriId.ToString());
                }

            }
            if (IstGalleryIdsToDelete.Count > 0)
            {
                foreach (string strGaleriId in IstGalleryIdsToDelete)
                {
                    string KatAdi = db.GetDataCell("select KategoriAdi from GaleriKategori where GaleriId=" + strGaleriId);


                    string path = Server.MapPath("~/upload/Galeri/" + KatAdi);
                    //Klasörün olup olmadığını kontrol ediyorum.
                    var directoryInfo = new DirectoryInfo(path);

                    if (directoryInfo.Exists)
                        Directory.Delete(path, true);


                    db.cmd("Delete from GaleriResim where GaleriId=" + strGaleriId);
                    int sonuc = db.cmd("Delete from GaleriKategori where GaleriId=" + strGaleriId);

                    //ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);




                    //string resim = db.GetDataCell("Select KategoriResmi from GaleriKategori where GaleriId=" + strGaleriId);

                    //if (resim != null && resim != "")
                    //    File.Delete(Server.MapPath("~/upload/" + resim));

                    //int sonuc = db.cmd("Delete from GaleriKategori where GaleriId=" + strGaleriId);





                    if (sonuc > 0)
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
                }
                GridView1.DataBind();
            }



        }

    }
}