﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;

namespace Kalder.Adminv2
{
    public partial class pages : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
          
            if (!IsPostBack)
            {
                DataTable dt = db.GetPages();

                //CollectionPager1.DataSource = dt.DefaultView;

                //CollectionPager1.BindToControl = rpt_News;
                //rpt_News.DataSource = CollectionPager1.DataSourcePaged;
                //rpt_News.DataBind();


                //Collecrion pages'ı devre dışı bıraktım ki bootstrap filter table çalışsın diye ;)

                rpt_News.DataSource = dt.DefaultView;
                rpt_News.DataBind();

            }
        }





        protected void btnDeleteAll_Onclick(object sender, EventArgs e)
        {
            int sonuc;
            foreach (RepeaterItem ri in rpt_News.Items)
            {
                CheckBox chk = (CheckBox)ri.FindControl("chkContainer");
                HiddenField hd = (HiddenField)ri.FindControl("hbItem");

                if (chk.Checked)
                {
                    //if(hd.Value.ToString()=="0")
                    sonuc = db.cmd("delete from Pages where PageId=" + hd.Value);
                    //else
                    //    sonuc = db.cmd("delete from Pages where PageId=" + hd.Value);

                    if (sonuc > 0)
                        Ayarlar.Alert.Show("Silme İşlemi Başarıyla Tamamlandı!");
                    else
                        Ayarlar.Alert.Show("Silmek için herhangi bir kayıt seçmediniz.");
                }


            }

            Response.Redirect("pages.aspx");



        }
    }
}