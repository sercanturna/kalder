﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class edit_slide : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string sId;
        string resimadi;
        protected void Page_Load(object sender, EventArgs e)
        {
          
            sId = Request.QueryString["sId"];

            if (!IsPostBack)
            {
                DataRow dr = db.GetDataRow("select * from Sliders where SlideId=" + sId);
                txtLink.Text = dr["Link"].ToString();
                txtSiraNo.Text = dr["OrderNo"].ToString();
                txtTitle.Text = dr["Title"].ToString();
                txtPublishDate.Text = dr["Date"].ToString();
                txtSuspendDate.Text = dr["EndDate"].ToString();

                dropTarget.Items.FindByValue(dr["LinkTarget"].ToString()).Selected = true;

                string durum = dr["IsActive"].ToString();
                if (durum == "True")
                    drpDurum.Items.FindByValue("1").Selected = true;
                else
                    drpDurum.Items.FindByValue("0").Selected = true;


                imgURL.ImageUrl = "../upload/SlaytResimleri/" + dr["ImageURL"].ToString();

            }
        }




        protected void btn_SlaytGuncelle_Click(object sender, EventArgs e)
        {

            string uzanti = "";
            resimadi = db.GetDataCell("Select ImageURL from Sliders where SlideId=" + sId);

            if (fuResim.HasFile)
            {
                FileInfo eskiresim = new FileInfo(Server.MapPath("~/upload/SlaytResimleri/" + resimadi));
                eskiresim.Delete();

                uzanti = Path.GetExtension(fuResim.PostedFile.FileName);
                resimadi = Ayarlar.OzetCek(Ayarlar.UrlSeo(txtTitle.Text), 10).Trim().Replace(".","") + DateTime.Now.Millisecond + uzanti;
                fuResim.SaveAs(Server.MapPath("~/upload/sahte/" + resimadi));

                Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + resimadi));
                resim = Ayarlar.ResimBoyutlandir(resim, 1600);

                resim.Save(Server.MapPath("~/upload/SlaytResimleri/" + resimadi), resim.RawFormat);
                // Resmi önce SlaytResimleri klasörüne kayıt ediyoruz.

                //resim = Ayarlar.ResimBoyutlandir(resim, 220);
                //resim.Save(Server.MapPath("~/upload/SlaytResimleri/220/" + resimadi), ImageFormat.Jpeg);
                //// Resmi sonrada 220 klasörüne kayıt ediyoruz.

                FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + resimadi));
                temp.Delete();
            }


            SqlConnection baglanti = db.baglan();
            SqlCommand cmdGuncelle = new SqlCommand("set language turkish; Update Sliders set ImageURL=@ImageURL,Title=@Title,IsActive=@IsActive,OrderNo=@OrderNo,Link=@Link,LinkTarget=@LinkTarget, EndDate=@EndDate, " +
            "Date=@Date Where SlideId=" + sId, baglanti);

            cmdGuncelle.Parameters.AddWithValue("ImageURL", resimadi);
            cmdGuncelle.Parameters.AddWithValue("Title", txtTitle.Text);
            cmdGuncelle.Parameters.AddWithValue("IsActive", drpDurum.SelectedValue);
            cmdGuncelle.Parameters.AddWithValue("OrderNo", txtSiraNo.Text);
            cmdGuncelle.Parameters.AddWithValue("Link", txtLink.Text);
            cmdGuncelle.Parameters.AddWithValue("LinkTarget", dropTarget.SelectedValue);

            //string dateString = txtPublishDate.Text;

            //DateTime DT;
            //DateTimeFormatInfo DateInfo = CultureInfo.CurrentCulture.DateTimeFormat;
            //DT = Convert.ToDateTime(String.Format("{0:yyyy-mm-dd hh:ii:ss}", dateString.Trim()), CultureInfo.CurrentCulture);
          
            //cmdGuncelle.Parameters.AddWithValue("Date", DT);


            string BeginDateString = txtPublishDate.Text;
            string EndDateString = txtSuspendDate.Text;

            DateTime DTBegin, DtEnd;
            //DateTimeFormatInfo DateInfo = CultureInfo.CurrentCulture.DateTimeFormat;

            DTBegin = Convert.ToDateTime(String.Format("{0:yyyy-mm-dd hh:ii:ss}", BeginDateString.Trim()), CultureInfo.CurrentCulture);
            DtEnd = Convert.ToDateTime(String.Format("{0:yyyy-mm-dd hh:ii:ss}", EndDateString.Trim()), CultureInfo.CurrentCulture);

            cmdGuncelle.Parameters.AddWithValue("Date", DTBegin);
            cmdGuncelle.Parameters.AddWithValue("EndDate", DtEnd);


            int sonuc = cmdGuncelle.ExecuteNonQuery();

            if (sonuc > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                Response.Redirect("sliders.aspx");
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);

        }
    }
}