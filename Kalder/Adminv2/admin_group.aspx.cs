﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class admin_group : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        int YetkiKontrol;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Userinfo Coockie'sinin olup olmadığını denetliyoruz.
            HttpCookie CportalUserCookie = Request.Cookies["UserInfo"];

            //Yetki kontrolünü hem session hemde cookie den aldığımız değerlerle kontrol ettiriyoruz.
            if (CportalUserCookie != null)
            {
                YetkiKontrol = Convert.ToInt32(db.GetAdminYetki(CportalUserCookie["AdminId"]));
                Session["AdminId"] = CportalUserCookie["AdminId"];
            }
            else
                YetkiKontrol = Convert.ToInt32(db.GetAdminYetki(Session["AdminId"].ToString()));

             


            //GridView1.UseAccessibleHeader = true;
            //GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;

        }



        protected void lnkEkle_Click(object sender, EventArgs e)
        {
            string txtAdiSoyadi, txtKullaniciAdi, txtParola, txtEposta;
            int ddlYetki;

            if (GridView1.Rows.Count > 0)
            {
                txtAdiSoyadi = ((TextBox)GridView1.FooterRow.FindControl("txtAdiSoyadiEkle")).Text;
                txtKullaniciAdi = ((TextBox)GridView1.FooterRow.FindControl("txtKullaniciAdiEkle")).Text;
                txtParola = ((TextBox)GridView1.FooterRow.FindControl("txtParolaEkle")).Text;
                txtEposta = ((TextBox)GridView1.FooterRow.FindControl("txtEpostaEkle")).Text;
                ddlYetki = Convert.ToInt32(((DropDownList)GridView1.FooterRow.FindControl("ddlYetkiEkle")).SelectedValue);
            }
            else
            {
                txtAdiSoyadi = ((TextBox)GridView1.Controls[0].Controls[0].FindControl("txtAdiSoyadiEkle")).Text;
                txtKullaniciAdi = ((TextBox)GridView1.Controls[0].Controls[0].FindControl("txtKullaniciAdiEkle")).Text;
                txtParola = ((TextBox)GridView1.Controls[0].Controls[0].FindControl("txtParolaEkle")).Text;
                txtEposta = ((TextBox)GridView1.Controls[0].Controls[0].FindControl("txtEpostaEkle")).Text;
                ddlYetki = Convert.ToInt32(((DropDownList)GridView1.Controls[0].Controls[0].FindControl("ddlYetkiEkle")).SelectedValue);
            }



            DataTable dt = db.GetDataTable("Select * from Admin as a inner join AdminYetkileri as ay on a.YetkiId=ay.YetkiId where KullaniciAdi='" + txtKullaniciAdi + "'");

            if (dt.Rows.Count > 0)
            {

                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:ErrorWithMsg('Bu kayıt daha önce eklenmiş ve aynı kullanıcı adına sahip başka bir kullanıcı mevcut!');", true);
            }
            else
            {

                SqlDataSource1.InsertParameters["AdiSoyadi"].DefaultValue = txtAdiSoyadi; ;
                SqlDataSource1.InsertParameters["KullaniciAdi"].DefaultValue = txtKullaniciAdi;
                SqlDataSource1.InsertParameters["Parola"].DefaultValue = txtParola;
                SqlDataSource1.InsertParameters["Eposta"].DefaultValue = txtEposta;
                SqlDataSource1.InsertParameters["YetkiId"].DefaultValue = ddlYetki.ToString();

                int sonuc = SqlDataSource1.Insert();

                if (sonuc > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:SuccessWithMsg('Yeni kullanıcı kaydı başarı ile gerçekleşti.');", true);
                }
                else
                    ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
            }



        }



        protected void GridView1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            GridView1.UseAccessibleHeader = true;
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Cancel();", true);
        }

        protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            GridView1.UseAccessibleHeader = true;
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
        }

        protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridView1.UseAccessibleHeader = true;
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:SuccessWithMsg('Kullanıcı kaydını başarıyla güncellediniz! Ekranda göremiyorsanız lütfen sayfayı yenileyin');", true);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {

        }

        protected void lnklPermission_Click(object sender, EventArgs e)
        {

        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string Id = GridView1.DataKeys[e.Row.RowIndex].Values[0].ToString();
                LinkButton lnkPermissionBtn = e.Row.FindControl("lnklPermission") as LinkButton;
               // lnkPermissionBtn.Attributes.Add("href","admin_permissions.aspx?AId=" + Id);
                lnkPermissionBtn.Attributes.Add("href", "javascript:$('#myModal .modal-body').load('admin_permissions.aspx?AId=" + Id + "',function(e){$('#myModal').modal('show');})");
            }


        }

        protected void GridView1_DataBinding(object sender, EventArgs e)
        {
            if (YetkiKontrol == 1)
            {
                pnlKontrolResult.Visible = false;

            }
            else
            {
                GridView1.ShowFooter = false;
                GridView1.Visible = false;
            }


        }
        protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

            }
        }
        protected void SqlDataSource1_Inserted(object sender, SqlDataSourceStatusEventArgs e)
        {

            int LastID = (int)e.Command.Parameters["@LastID"].Value;

            //string lastID = e.Command.Parameters["@LastID"].Value.ToString();

            //ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:SuccessWithMsg('"+Convert.ToString(LastID)+"');", true);

            int sonuc = db.cmd("INSERT INTO AdminPermission (" +
              "AdminId," +
              "General_Module_Manage," +
              "General_Message_Manage," +
              "General_IsEdit," +
              "General_IsDelete," +
              "Page_IsInsert," +
              "Page_IsView," +
              "Page_IsEdit," +
              "Page_IsDelete," +
              "News_Cat_Manage," +
              "News_IsInsert," +
              "News_IsView," +
              "News_IsEdit," +
              "News_IsDelete," +
              "Product_Cat_Manage," +
              "Product_Brand_Manage," +
              "Product_IsInsert," +
              "Product_IsView," +
              "Product_IsEdit," +
              "Product_IsDelete," +
              "Album_Cat_Manage," +
              "Album_Image_Insert," +
              "Album_Image_Delete," +
              "Reference_Management)" +
       " VALUES (" +
       Convert.ToString(LastID) + "," +
    "0," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0'," +
    "'0')");

        }
        protected void SqlDataSource1_Deleted(object sender, SqlDataSourceStatusEventArgs e)
        {
            int LastID = (int)e.Command.Parameters["@original_AdminId"].Value;

            db.cmd("Delete from AdminPermission where AdminId=" + LastID);

            //ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:SuccessWithMsg('" + Convert.ToString(LastID) + " silindi!');", true);
        }


        protected void SqlDataSource1_Updating(object sender, SqlDataSourceCommandEventArgs e)
        {


            // string  txtParola = ((TextBox)GridView1.Controls[0].Controls[0].FindControl("txtParolaDuzenle")).Text;

            string txtParola2 = ((TextBox)GridView1.Rows[GridView1.EditIndex].FindControl("txtParolaDuzenle")).Text;

            //   string str = ((TextBox)(GridView1.Rows(GridView1.EditIndex).FindControl("txtApprover")), TextBox).Text;


            // SqlDataSource1.UpdateParameters["Parola"].DefaultValue = Ayarlar.md5(txtParola);


            e.Command.Parameters["@Parola"].Value = Ayarlar.md5(txtParola2);
        }
    }
}