﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class general_settings : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        StyleColor st = new StyleColor();
        string _logo, logo, _financeTable, financeTable, _bg, bg;

        protected void Page_Load(object sender, EventArgs e)
        {
            

            //dropRenk.Enabled = true;
            imgBg.Height = 100;


            try
            {
                if (!Page.IsPostBack)
                {
                    DataTable dt = db.GetDataTable("Select * from Sehirler order by SehirAdi");
                    dropSehir.DataSource = dt;
                    dropSehir.DataValueField = dt.Columns["SehirId"].ColumnName.ToString();
                    dropSehir.DataTextField = dt.Columns["SehirAdi"].ColumnName.ToString();
                    dropSehir.DataBind();
                    dropSehir.Items.Insert(0, new ListItem("– Seçiniz –", "0"));



                    DataRow drAyarlar = db.GetDataRow("Select * from GenelAyarlar");


                    txtcompanyshort.Text = drAyarlar["FirmaUnvanKisa"].ToString();
                    txtcompanylong.Text = drAyarlar["FirmaUnvanUzun"].ToString();

                    dropSehir.SelectedValue = drAyarlar["Sehir"].ToString();
                    dropRenk.SelectedValue = drAyarlar["Tema"].ToString();

                    txtVergiDairesi.Text = drAyarlar["VergiDairesi"].ToString();
                    txtVergino.Text = drAyarlar["VergiNo"].ToString();
                    txtNace.Text = drAyarlar["NaceKodu"].ToString();
                    txtMersis.Text = drAyarlar["MersisNo"].ToString();

                    txtortak1.Text = drAyarlar["Ortak1Adi"].ToString();
                    txtortak2.Text = drAyarlar["Ortak2Adi"].ToString();
                    txtortak3.Text = drAyarlar["Ortak3Adi"].ToString();
                    txtortak4.Text = drAyarlar["Ortak4Adi"].ToString();
                    txtortak5.Text = drAyarlar["Ortak5Adi"].ToString();

                    txtSermaye1.Text = drAyarlar["Ortak1Sermaye"].ToString();
                    txtSermaye2.Text = drAyarlar["Ortak2Sermaye"].ToString();
                    txtSermaye3.Text = drAyarlar["Ortak3Sermaye"].ToString();
                    txtSermaye4.Text = drAyarlar["Ortak4Sermaye"].ToString();
                    txtSermaye5.Text = drAyarlar["Ortak5Sermaye"].ToString();

                    txtAdres.Text = drAyarlar["Adres"].ToString();
                    txtTel.Text = drAyarlar["Tel"].ToString();
                    txtTelPbx.Text = drAyarlar["TelPbx"].ToString();
                    txtFax.Text = drAyarlar["Fax"].ToString();
                    txtMail.Text = drAyarlar["Eposta"].ToString();
                    txtFacebook.Text = drAyarlar["Facebook"].ToString();
                    txtTwitter.Text = drAyarlar["Twitter"].ToString();
                    txtInstagram.Text = drAyarlar["Instagram"].ToString();
                    txtGoogle.Text = drAyarlar["Google"].ToString();
                    txtYoutube.Text = drAyarlar["Youtube"].ToString();
                    txtPinterest.Text = drAyarlar["Pinterest"].ToString();
                    txtfoursquare.Text = drAyarlar["foursquare"].ToString();



                    txtTitle.Text = drAyarlar["MetaTitle"].ToString();
                    txtDescription.Text = drAyarlar["MetaDescription"].ToString();
                    tags.Text = drAyarlar["MetaKeywords"].ToString();
                    txtAnalytics.Text = drAyarlar["Analytics"].ToString();
                    txtHarita.Text = drAyarlar["GoogleMap"].ToString();


                    txtHaftaici.Text = drAyarlar["HaftaiciCs"].ToString();
                    txtHaftasonu.Text = drAyarlar["HaftasonuCs"].ToString();


                    Renk1ColorPicker.Text = Server.HtmlEncode(drAyarlar["Color1"].ToString().Trim());
                    Renk2ColorPicker.Text = Server.HtmlEncode(drAyarlar["Color2"].ToString().Trim());







                }

                _logo = db.GetDataCell("Select LogoUrl from GenelAyarlar");
                _bg = db.GetDataCell("Select Bg from GenelAyarlar");

                if (_logo != null && _logo != "")
                    imgLogo.ImageUrl = "~/upload/" + _logo;
                else
                    imgLogo.ImageUrl = "~/upload/cportal_logo.png";


                if (_bg != null && _bg != "")
                    imgBg.ImageUrl = "~/upload/" + _bg;
                else
                    imgBg.ImageUrl = "~/upload/defaultbg.jpg";
            }
            catch (Exception)
            {

            }



        }
        protected void btnKaydet_Click(object sender, EventArgs e)
        {
            //Renkleri değişitirmek için st adında bir class yazdım.
            st.ChangeStyle(Renk1ColorPicker, Renk2ColorPicker);




            logo = Ayarlar.ResimAdi(fuLogo.FileName);
            _logo = db.GetDataCell("Select LogoUrl from GenelAyarlar");

            if (fuLogo.HasFile)
            {
                if (logo != null && logo != "")
                {
                    FileInfo fiEskiResim = new FileInfo(Server.MapPath("~/upload/" + _logo));
                    fiEskiResim.Delete();
                }

                HttpPostedFile logo_file = fuLogo.PostedFile;
                logo_file.SaveAs(Server.MapPath("~/upload/" + Ayarlar.ResimAdi(logo)));


            }
            else
            {
                logo = _logo;
            }

            //if (logo != null && logo != "")
            //{
            //    if (_logo != null && _logo != "")
            //    {
            //        FileInfo fiEskiResim = new FileInfo(Server.MapPath("~/upload/" + _logo));
            //        fiEskiResim.Delete();
            //    }
            //    logo = FileUploadWithoutResize.SaveFile(fuLogo, "upload", logo);
            //}
            //else
            //{
            //    logo = _logo;
            //}

            bg = "bg.jpg";
            _bg = db.GetDataCell("Select Bg from GenelAyarlar");
            if (fuBg.HasFile)
            {
                if (_bg != null && _bg != "")
                {
                    FileInfo fiEskiBgResim = new FileInfo(Server.MapPath("~/upload/" + _bg));
                    fiEskiBgResim.Delete();
                }


                HttpPostedFile bg_file = fuBg.PostedFile;
                bg_file.SaveAs(Server.MapPath("~/upload/" + bg));


            }
            else
            {
                bg = _bg;
            }

            financeTable = fuFinansalTablolar.FileName;
            _financeTable = db.GetDataCell("Select FinansalTablo from GenelAyarlar");

            if (financeTable != null && financeTable != "")
            {
                FileInfo fiEskiResim = new FileInfo(Server.MapPath("~/upload/" + _financeTable));

                fiEskiResim.Delete();
                // financeTable = FileUploadWithoutResize.SaveFile(fuFinansalTablolar, "upload", financeTable);
            }
            else
            {
                financeTable = _financeTable;
            }


            #region DbAction

            SqlConnection conn = db.baglan();
            SqlCommand cmd = new SqlCommand("Update GenelAyarlar set " +


            "FirmaUnvanKisa=@FirmaUnvanKisa," +
            "FirmaUnvanUzun=@FirmaUnvanUzun," +
            "Sehir=@Sehir," +
             "Tema=@Tema," +
              "Bg=@Bg," +
            "VergiDairesi=@VergiDairesi," +
            "VergiNo=@VergiNo," +
            "NaceKodu=@NaceKodu," +
            "MersisNo=@MersisNo," +
            "Ortak1Adi=@Ortak1Adi," +
            "Ortak2Adi=@Ortak2Adi," +
            "Ortak3Adi=@Ortak3Adi," +
            "Ortak4Adi=@Ortak4Adi," +
            "Ortak5Adi=@Ortak5Adi," +
            "Ortak1Sermaye=@Ortak1Sermaye," +
            "Ortak2Sermaye=@Ortak2Sermaye," +
            "Ortak3Sermaye=@Ortak3Sermaye," +
            "Ortak4Sermaye=@Ortak4Sermaye," +
            "Ortak5Sermaye=@Ortak5Sermaye," +
            "FinansalTablo=@FinansalTablo," +
            "LogoUrl=@LogoUrl," +
            "MetaTitle=@MetaTitle," +
            "MetaDescription=@MetaDescription," +
            "MetaKeywords=@MetaKeywords," +
            "Analytics=@Analytics," +
            "GoogleMap=@GoogleMap," +
            "Adres=@Adres," +
            "Tel=@Tel," +
            "TelPbx=@TelPbx," +
            "Fax=@Fax," +
            "Eposta=@Eposta," +
            "Facebook=@Facebook," +
            "Twitter=@Twitter," +
            "Instagram=@Instagram," +
            "Google=@Google," +
            "Pinterest=@Pinterest," +
            "Foursquare=@Foursquare," +
            "HaftaiciCs=@HaftaiciCs," +
            "HaftasonuCs=@HaftasonuCs," +
            "Color1=@Color1," +
            "Color2=@Color2," +
            "Youtube=@Youtube"
                    , db.baglan());

            cmd.Parameters.AddWithValue("FirmaUnvanKisa", txtcompanyshort.Text);
            cmd.Parameters.AddWithValue("FirmaUnvanUzun", txtcompanylong.Text);
            cmd.Parameters.AddWithValue("Sehir", dropSehir.SelectedValue);
            cmd.Parameters.AddWithValue("Tema", dropRenk.SelectedValue);
            cmd.Parameters.AddWithValue("VergiDairesi", txtVergiDairesi.Text);
            cmd.Parameters.AddWithValue("VergiNo", txtVergino.Text);
            cmd.Parameters.AddWithValue("NaceKodu", txtNace.Text);
            cmd.Parameters.AddWithValue("MersisNo", txtMersis.Text);
            cmd.Parameters.AddWithValue("Ortak1Adi", txtortak1.Text);
            cmd.Parameters.AddWithValue("Ortak2Adi", txtortak2.Text);
            cmd.Parameters.AddWithValue("Ortak3Adi", txtortak3.Text);
            cmd.Parameters.AddWithValue("Ortak4Adi", txtortak4.Text);
            cmd.Parameters.AddWithValue("Ortak5Adi", txtortak5.Text);
            cmd.Parameters.AddWithValue("Ortak1Sermaye", txtSermaye1.Text);
            cmd.Parameters.AddWithValue("Ortak2Sermaye", txtSermaye2.Text);
            cmd.Parameters.AddWithValue("Ortak3Sermaye", txtSermaye3.Text);
            cmd.Parameters.AddWithValue("Ortak4Sermaye", txtSermaye4.Text);
            cmd.Parameters.AddWithValue("Ortak5Sermaye", txtSermaye5.Text);

            cmd.Parameters.AddWithValue("HaftaiciCs", txtHaftaici.Text);
            cmd.Parameters.AddWithValue("HaftasonuCs", txtHaftasonu.Text);

            cmd.Parameters.AddWithValue("FinansalTablo", Ayarlar.FileName(financeTable));


            cmd.Parameters.AddWithValue("LogoUrl", Ayarlar.FileName(logo));
            cmd.Parameters.AddWithValue("Bg", Ayarlar.FileName(bg));
            cmd.Parameters.AddWithValue("MetaTitle", txtTitle.Text);
            cmd.Parameters.AddWithValue("MetaDescription", txtDescription.Text);
            cmd.Parameters.AddWithValue("MetaKeywords", tags.Text);
            cmd.Parameters.AddWithValue("Analytics", txtAnalytics.Text);
            cmd.Parameters.AddWithValue("GoogleMap", txtHarita.Text);
            cmd.Parameters.AddWithValue("Adres", txtAdres.Text);
            cmd.Parameters.AddWithValue("Tel", txtTel.Text);
            cmd.Parameters.AddWithValue("TelPbx", txtTelPbx.Text);
            cmd.Parameters.AddWithValue("Fax", txtFax.Text);
            cmd.Parameters.AddWithValue("Eposta", txtMail.Text);
            cmd.Parameters.AddWithValue("Facebook", txtFacebook.Text);
            cmd.Parameters.AddWithValue("Twitter", txtTwitter.Text);
            cmd.Parameters.AddWithValue("Instagram", txtInstagram.Text);
            cmd.Parameters.AddWithValue("Google", txtGoogle.Text);
            cmd.Parameters.AddWithValue("Youtube", txtYoutube.Text);
            cmd.Parameters.AddWithValue("Pinterest", txtPinterest.Text);
            cmd.Parameters.AddWithValue("Foursquare", txtfoursquare.Text);

            cmd.Parameters.AddWithValue("Color1", Renk1ColorPicker.Text);
            cmd.Parameters.AddWithValue("Color2", Renk2ColorPicker.Text);

            int sonuc = cmd.ExecuteNonQuery();

            if (sonuc > 0)
                Response.Redirect(Path.GetFileName(Page.AppRelativeVirtualPath) + "?process=success");
            else
                Response.Redirect(Path.GetFileName(Page.AppRelativeVirtualPath) + "?process=error");





            #endregion
        }




        protected void btnIptal_Click(object sender, EventArgs e)
        {
            Ayarlar.Alert.Show("İptal Edildi");
        }

        protected void btnBgTemizle_Click(object sender, ImageClickEventArgs e)
        {

            try
            {
                File.Delete(Server.MapPath("~/upload/bg.jpg"));
            }
            catch (Exception)
            {
                //throw;
            }

            //File.Copy("c:\\galatasaray.txt", "e:\\galatasaray.txt");

            File.Copy(Server.MapPath("~/upload/defaultbg.jpg"), Server.MapPath("~/upload/bg.jpg"));
            imgBg.ImageUrl = "bg.jpg";
            Response.Redirect(Path.GetFileName(Page.AppRelativeVirtualPath));
        }
    }
}