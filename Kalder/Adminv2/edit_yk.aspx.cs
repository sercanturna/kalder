﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{

    public partial class edit_trainer : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        int SonEklenenUrunId;
        string KatId = "";
        string pId;
        DataRow dr;
        protected void Page_Load(object sender, EventArgs e)
        {
           
            pId = RouteData.Values["pId"].ToString();


            if (!IsPostBack)
            {
               
                dr = db.GetDataRow("Select * from BoardofDirectors where YkId=" + pId);


                txtUrunAdi.Text = dr["Name"].ToString();
                txtUnvan.Text = dr["Title"].ToString();
                txtDetay.Text = dr["Content"].ToString();

                string durum = dr["IsActive"].ToString();
                if (durum == "True")
                    drpDurum.Items.FindByValue("1").Selected = true;
                else
                    drpDurum.Items.FindByValue("0").Selected = true;

                txtSiraNo.Text = dr["OrderNo"].ToString();
                txtDescription.Text = dr["Meta_Desc"].ToString();
                tags.Text = dr["Meta_Keywords"].ToString();
                txtTitle.Text = dr["Meta_Title"].ToString();
                txtSeoUrl.Text = dr["SeoUrl"].ToString();
            }
        }

       



        protected void btn_UrunEkle_Click(object sender, EventArgs e)
        {
            // Önce kaç adet seçilen kategori var onu tespit ediyoruz.
           
           

            #region Eğitmen Tablosu Veritabanı Update

            string resimadi = db.GetDataCell("Select Image from BoardofDirectors where YkId=" + pId);
            //string uzanti = "";

            if (fuResim.HasFile)
            {
                //Küçük resmi sil
                FileInfo tempThumb = new FileInfo(Server.MapPath("~/upload/YonetimKurulu/" + resimadi));
                tempThumb.Delete();

                string uzanti = Path.GetExtension(fuResim.PostedFile.FileName);
                resimadi = Ayarlar.UrlSeo((txtUrunAdi.Text) + DateTime.Now.Millisecond).Trim() + uzanti;
                fuResim.SaveAs(Server.MapPath("~/upload/sahte/" + resimadi));

                Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + resimadi));
                resim = Ayarlar.ResimBoyutlandir(resim, 300);

                resim.Save(Server.MapPath("~/upload/YonetimKurulu/" + resimadi), ImageFormat.Jpeg);
                // Resmi önce 760 klasörüne kayıt ediyoruz.

                //resim = Ayarlar.ResimBoyutlandirYukseklik(resim, 205);
                //resim.Save(Server.MapPath("~/upload/YonetimKurulu/sahte" + resimadi), ImageFormat.Jpeg);
                //// Resmi sonrada 340 klasörüne kayıt ediyoruz.



                //System.Drawing.Image img = System.Drawing.Image.FromFile(Server.MapPath("~/upload/YonetimKurulu/sahte" + resimadi));

                //System.Drawing.Image ResizedImg = CropImage(img, img.Height, 270, 60, 0);

                //ResizedImg.Save(Server.MapPath("~/upload/YonetimKurulu/" + resimadi));


                //FileInfo Thumbtemp = new FileInfo(Server.MapPath("~/upload/sahte/" + resimadi));
                //Thumbtemp.Delete();


                FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + resimadi));
                temp.Delete();
            }
            #endregion

            #region Db

            SqlConnection baglanti = db.baglan();
            SqlCommand cmd = new SqlCommand("SET language turkish; Update BoardofDirectors set " +
            "Name=@Name, " +
            "Title=@Title, " +
            "Content=@Content, " +
            "Image=@Image, " +
            "IsActive=@IsActive, " +
            "OrderNo=@OrderNo, " +
            "SeoUrl=@SeoUrl, " +
            "Meta_Title=@Meta_Title, " +
            "Meta_Keywords=@Meta_Keywords, " +
            "Meta_Desc=@Meta_Desc" +
            " where YkId=" + pId, baglanti);


            SqlParameter insertPrm = new SqlParameter();
            insertPrm.Direction = ParameterDirection.Output;
            // ID değerini istediğimiz alan
            insertPrm.ParameterName = "YkId";
            insertPrm.Size = 10;

            string seoURL = txtSeoUrl.Text;

            if (seoURL == "")
                seoURL = Ayarlar.UrlSeo(txtUrunAdi.Text);
            else
                seoURL = Ayarlar.UrlSeo(txtSeoUrl.Text);

            cmd.Parameters.Add(insertPrm);

            string durum = drpDurum.SelectedValue;



            cmd.Parameters.AddWithValue("Name", txtUrunAdi.Text);
            cmd.Parameters.AddWithValue("Title", txtUnvan.Text);
            cmd.Parameters.AddWithValue("Image", resimadi);
            cmd.Parameters.AddWithValue("Content", txtDetay.Text);
            cmd.Parameters.AddWithValue("OrderNo", txtSiraNo.Text);
            cmd.Parameters.AddWithValue("IsActive", durum);
            cmd.Parameters.AddWithValue("SeoUrl", seoURL);
            cmd.Parameters.AddWithValue("Meta_Title", txtTitle.Text);
            cmd.Parameters.AddWithValue("Meta_Keywords", tags.Text);
            cmd.Parameters.AddWithValue("Meta_Desc", txtDescription.Text);

            //int sonuc = int.Parse(cmd.ExecuteScalar().ToString());
            //SonEklenenUrunId = sonuc;

            int sonuc = cmd.ExecuteNonQuery();

            // SonEklenenUrunId = (int)insertPrm.Value;

            if (sonuc > 0)
            {
                // Ayarlar.Alert.result();
                Session["SonGirilenKatId"] = KatId;
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
               
                Response.Redirect("BoardofDirectors.aspx");
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);





        

            #endregion



        }

        //Overload for crop that default starts top left of the image.
        //public static System.Drawing.Image CropImage(System.Drawing.Image Image, int Height, int Width)
        //{
        //    return CropImage(Image, Height, Width, 0, 0);
        //}

        //The crop image sub
        public static System.Drawing.Image CropImage(System.Drawing.Image Image, int Height, int Width, int StartAtX, int StartAtY)
        {
            System.Drawing.Image outimage;
            MemoryStream mm = null;
            try
            {
                //check the image height against our desired image height
                if (Image.Height < Height)
                {
                    Height = Image.Height;
                }

                if (Image.Width < Width)
                {
                    Width = Image.Width;
                }

                //create a bitmap window for cropping
                Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
                bmPhoto.SetResolution(72, 72);

                //create a new graphics object from our image and set properties
                Graphics grPhoto = Graphics.FromImage(bmPhoto);
                grPhoto.SmoothingMode = SmoothingMode.AntiAlias;
                grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                grPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;

                //now do the crop
                grPhoto.DrawImage(Image, new Rectangle(0, 0, Width, Height), StartAtX, StartAtY, Width, Height, GraphicsUnit.Pixel);

                // Save out to memory and get an image from it to send back out the method.
                mm = new MemoryStream();
                bmPhoto.Save(mm, System.Drawing.Imaging.ImageFormat.Jpeg);
                Image.Dispose();
                bmPhoto.Dispose();
                grPhoto.Dispose();
                outimage = System.Drawing.Image.FromStream(mm);

                return outimage;
            }
            catch (Exception ex)
            {
                throw new Exception("Error cropping image, the error was: " + ex.Message);
            }
        }

        ////Hard resize attempts to resize as close as it can to the desired size and then crops the excess
        //public static System.Drawing.Image HardResizeImage(int Width, int Height, System.Drawing.Image Image)
        //{
        //    int width = Image.Width;
        //    int height = Image.Height;
        //    System.Drawing.Image resized = null;
        //    if (Width > Height)
        //    {
        //        resized = ResizeImage(Width, Width, Image);
        //    }
        //    else
        //    {
        //        resized = ResizeImage(Height, Height, Image);
        //    }
        //    System.Drawing.Image output = CropImage(resized, Height, Width);
        //    //return the original resized image
        //    return output;
        //}

        ////Image resizing
        //public static System.Drawing.Image ResizeImage(int maxWidth, int maxHeight, System.Drawing.Image Image)
        //{
        //    int width = Image.Width;
        //    int height = Image.Height;
        //    if (width > maxWidth || height > maxHeight)
        //    {
        //        //The flips are in here to prevent any embedded image thumbnails -- usually from cameras
        //        //from displaying as the thumbnail image later, in other words, we want a clean
        //        //resize, not a grainy one.
        //        Image.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipX);
        //        Image.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipX);

        //        float ratio = 0;
        //        if (width > height)
        //        {
        //            ratio = (float)width / (float)height;
        //            width = maxWidth;
        //            height = Convert.ToInt32(Math.Round((float)width / ratio));
        //        }
        //        else
        //        {
        //            ratio = (float)height / (float)width;
        //            height = maxHeight;
        //            width = Convert.ToInt32(Math.Round((float)height / ratio));
        //        }

        //        //return the resized image
        //        return Image.GetThumbnailImage(width, height, null, IntPtr.Zero);
        //    }
        //    //return the original resized image
        //    return Image;
        //}


        //public System.Drawing.Image Crop(string img, int width, int height, int x, int y)
        //{
        //    try
        //    {
        //        System.Drawing.Image image = System.Drawing.Image.FromFile(img);
        //        Bitmap bmp = new Bitmap(width, height, PixelFormat.Format24bppRgb);
        //        bmp.SetResolution(270, 205);

        //        Graphics gfx = Graphics.FromImage(bmp);
        //        gfx.SmoothingMode = SmoothingMode.AntiAlias;
        //        gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;
        //        gfx.PixelOffsetMode = PixelOffsetMode.HighQuality;
        //        gfx.DrawImage(image, new Rectangle(0, 0, width, height), x, y, width, height, GraphicsUnit.Pixel);
        //        // Dispose to free up resources
        //        image.Dispose();
        //        bmp.Dispose();
        //        gfx.Dispose();

        //        return bmp;
        //    }
        //    catch (Exception ex)
        //    {
        //       Ayarlar.Alert.Show(ex.Message);
        //        return null;
        //    }
        //}


        protected void dropKat_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

      

        protected void btn_Temizle_Click(object sender, EventArgs e)
        {
            Response.Redirect("BoardofDirectors.aspx");
        }
    }


}