﻿<%@ Page Title="" Language="C#" MasterPageFile="MasterPage.master" AutoEventWireup="true" CodeFile="sanalkutuphane.aspx.cs" Inherits="sanalkutuphane" %>
<%@ Register assembly="FredCK.FCKeditorV2" namespace="FredCK.FCKeditorV2" tagprefix="FCKeditorV2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 545px;
        }
        .style2
        {
            width: 1106px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <table ID="table64" border="0" cellspacing="1" 
                style="font-family: arial; font-size: 8pt" width="750">
                <tr>
                    <td background="http://localhost:1501/deyyap%20web/Admin/images/table_bg.gif" 
                        bgcolor="#333333" colspan="2" height="27">
                        <font color="#FFFFFF" face="Tahoma" style="font-size: 8pt">&nbsp;<b>Yeni Kayıt Ekle</b></font></td>
                </tr>
               
                <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;Raf No</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:TextBox ID="txtRafNo_E" runat="server" maxlength="1000" size="50" 
                            style="border-style:groove;font-size:10;font-family:verdana"></asp:TextBox>
                    </td>
                </tr> 
                
                <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;Dolap No</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:TextBox ID="txtDolapNo_E" runat="server" maxlength="255" size="50" 
                            style="border-style:groove;font-size:10;font-family:verdana"></asp:TextBox>
                    </td>
                </tr> 
                <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;Kitap Adı</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:TextBox ID="txtKitapAdi_E" runat="server" maxlength="1000" size="50" 
                            style="border-style:groove;font-size:10;font-family:verdana"></asp:TextBox>
                    </td>
                </tr> 
                <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;Yazar</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:TextBox ID="txtYazar_E" runat="server" maxlength="1000" size="50" 
                            style="border-style:groove;font-size:10;font-family:verdana"></asp:TextBox>
                    </td>
                </tr> 
                  <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;Yayıncı</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:TextBox ID="txtYayinci_E" runat="server" maxlength="1000" size="50" 
                            style="border-style:groove;font-size:10;font-family:verdana"></asp:TextBox>
                    </td>
                </tr> 
                  <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;Telif Hakkı Yılı</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:TextBox ID="txtTelif_E" runat="server" maxlength="1000" size="50" 
                            style="border-style:groove;font-size:10;font-family:verdana"></asp:TextBox>
                    </td>
                </tr> 
                  <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;Sayfa Sayısı</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:TextBox ID="txtSayfa_E" runat="server" maxlength="1000" size="50" 
                            style="border-style:groove;font-size:10;font-family:verdana"></asp:TextBox>
                    </td>
                </tr> 
                  <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;ISBN NO</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:TextBox ID="txtISBN_E" runat="server" maxlength="1000" size="50" 
                            style="border-style:groove;font-size:10;font-family:verdana"></asp:TextBox>
                    </td>
                </tr> 
                <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp; Açıklama</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <fckeditor id="FCKeditor1" runat="server" basepath="fckeditor/" height="400px"></fckeditor>
                        <FCKeditorV2:FCKeditor ID="FCKAciklama_E" runat="server" BasePath="fckeditor/" 
                            Height="400px"></FCKeditorV2:FCKeditor>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        Anahtar Kelime:</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <font face="Verdana" size="1">
                        <asp:TextBox ID="txtAnahtarKelime_E" runat="server" Height="84px" TextMode="MultiLine" 
                            Width="450px"></asp:TextBox>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;Resim</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:FileUpload ID="fuResim_E" runat="server" />
                        <font face="Verdana" size="1">&nbsp;</font></td>
                </tr>
                <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;
                    </td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:Button ID="btnKaydet" runat="server" onclick="btnKaydet_Click" 
                            Text="Kaydet" />
                        &nbsp;
                        <input name="B3" onclick="javascript:history.back()" type="button" 
                            value="Geri Dön"></input></td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <table ID="table65" border="0" cellspacing="1" 
                style="font-family: arial; font-size: 8pt" width="750">
                <tr>
                    <td background="http://localhost:1501/deyyap%20web/Admin/images/table_bg.gif" 
                        bgcolor="#333333" colspan="2" height="27">
                        <font color="#FFFFFF" face="Tahoma" style="font-size: 8pt">&nbsp;<b>Kayıt Bilgileri</b></font></td>
                </tr>
               
                <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;Raf No</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:TextBox ID="txtRafNo_G" runat="server" maxlength="200" size="50" 
                            style="border-style:groove;font-size:10;font-family:verdana"></asp:TextBox>
                    </td>
                </tr>
               
                 <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;Dolap No:</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:TextBox ID="txtDolapNo_G" runat="server" maxlength="200" size="50" 
                            style="border-style:groove;font-size:10;font-family:verdana"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;Kitap Adı</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:TextBox ID="txtKitapAdi_G" runat="server" maxlength="200" size="50" 
                            style="border-style:groove;font-size:10;font-family:verdana"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;Yazar</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:TextBox ID="txtYazar_G" runat="server" maxlength="200" size="50" 
                            style="border-style:groove;font-size:10;font-family:verdana"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;Yayıncı</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:TextBox ID="txtYayinci_G" runat="server" maxlength="200" size="50" 
                            style="border-style:groove;font-size:10;font-family:verdana"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;Telif Hakkı Yılı</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:TextBox ID="txtTelif_G" runat="server" maxlength="200" size="50" 
                            style="border-style:groove;font-size:10;font-family:verdana"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;Sayfa Sayısı</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:TextBox ID="txtSayfa_G" runat="server" maxlength="200" size="50" 
                            style="border-style:groove;font-size:10;font-family:verdana"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;ISBN No</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:TextBox ID="txtISBN_G" runat="server" maxlength="200" size="50" 
                            style="border-style:groove;font-size:10;font-family:verdana"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        Açıklama:</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <fckeditor id="FCKeditor2" runat="server" basepath="fckeditor/" height="400px">
                            <FCKeditorV2:FCKeditor ID="FCKAciklama_G" runat="server" BasePath="fckeditor/" 
                            Height="400px">
                            </FCKeditorV2:FCKeditor>
                        </fckeditor>
                    </td>
                </tr>
                 <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;Anahtar Kelimeler</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:TextBox ID="txtAnahtar_G" runat="server" maxlength="200" size="50" 
                            style="border-style:groove;font-size:10;font-family:verdana"></asp:TextBox>
                    </td>
                </tr>
                <tr valign="bottom">
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;Resim</td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        <asp:FileUpload ID="fuResim_G" runat="server" />
                        &nbsp;<asp:Image ID="imgResim_G" runat="server" width="80" height="60"
                            ImageUrl="" /><asp:Label ID="lblresimAd" runat="server"></asp:Label>
                    </td>
                </tr>
                
               
              
               
               
                <tr>
                    <td bgcolor="#EFEFEF" height="25" width="115">
                        &nbsp;
                    </td>
                    <td bgcolor="#EFEFEF" height="25" width="630">
                        &nbsp;<asp:Button ID="btnGuncelle" runat="server" onclick="btnGuncelle_Click" 
                            Text="Güncelle" />
                        &nbsp;
                        <input name="B4" onclick="javascript:history.back()" type="button" 
                            value="Geri Dön"></input></td>
                </tr>
            </table>
        </asp:View>
        <asp:View ID="View3" runat="server">
            <table width="750px">
                <tr>
                    <td style="width: 2%; text-align: right">
                        <img src="images/ekle.gif" alt="Arti" />
                    </td>
                    <td class="style2" style=" text-align: left">
                        <strong><a href="#">
                        <asp:LinkButton ID="LinkButton3" runat="server" Font-Bold="True" 
                            Font-Names="Tahoma" Font-Size="9pt" ForeColor="#FF6600" 
                            onclick="LinkButton3_Click">Kayıt Filtrelemeyi Aç</asp:LinkButton>
                        </a>&nbsp;</strong></td>
                    <td align="right" style="width: 3%; ">
                        <img src="images/ekle.gif" alt="Arti" />
                    </td>
                    <td align="right" style=" height:30px;" width="153">
                        <asp:LinkButton ID="LinkButton4" runat="server" Font-Bold="True" 
                            Font-Names="Tahoma" Font-Size="9pt" ForeColor="#FF6600" 
                            onclick="LinkButton2_Click">Yeni Kayıt Ekle</asp:LinkButton>
                    </td>
                </tr>
            </table>
            <asp:DataList ID="DataList1" runat="server" DataKeyField="Id" 
                onitemcommand="DataList1_ItemCommand">
                <HeaderTemplate>
                    <table border="0" cellspacing="1" width="750"  bgcolor="#333333">
                        <tr>
                            <td class="style1" colspan="3" 
                                style="background-image: url('http://localhost:1501/deyyap%20web/Admin/images/table_bg.gif');">
                                <span style="font-size: 9pt; color: #ffffff; font-family: Tahoma"><strong>&nbsp;Kayıt 
                                Listesi</strong></span></td>
                        </tr>
                        <tr>
                            <td bgcolor="#E5E5E5" height="25" style="border-bottom: 2px solid #666666" 
                                width="600">
                                <font face="Tahoma" style="font-size: 8pt">&nbsp;Kayıt</font></td>
                            <td bgcolor="#E5E5E5" height="25" style="border-bottom: 2px solid #666666" 
                                width="69">
                                <font face="Tahoma" style="font-size: 8pt">&nbsp;Sıra</font></td>
                            <td align="center" bgcolor="#E5E5E5" height="25" 
                                style="border-bottom: 2px solid #666666" width="70">
                                <font face="Tahoma" style="font-size: 8pt">&nbsp;İşlem</font></td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemStyle BackColor="#EFEFEF" />
                <ItemTemplate>
                    <table border="0" cellspacing="1" width="750">
                        <tr bgcolor="" onmouseout="bgColor=''" onmouseover="bgColor='#ffda75'">
                            <td width="600">
                                <asp:Label ID="Label1" runat="server" Font-Names="Tahoma" Font-Size="8pt" 
                                    Text='<%# Eval("KitapAdi") %>'></asp:Label>
                            </td>
                            <td width="69">
                                &nbsp;
                            </td>
                            <td align="center" width="70">
                                <table border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td style="width:24px">
                                            <asp:ImageButton ID="ImageButton1" runat="server" CommandName="edit" 
                                                ImageUrl="images/edit.gif" />
                                        </td>
                                        <td style="width:24px">
                                            <asp:ImageButton ID="ImageButton2" runat="server" CommandName="delete" 
                                                ImageUrl="images/delete.gif" 
                                                onclientclick="return window.confirm('Silmek istediğinize emin misiniz?');" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:DataList>
        </asp:View>
        <asp:View ID="View4" runat="server">
            <table ID="Table2" border="0" cellpadding="0" cellspacing="0" 
                style="display:block" width="750px">
                <tr>
                    <td colspan="3"  bgcolor="#333333"
                        style="background-image: url('http://localhost:1501/deyyap%20web/Admin/images/table_bg.gif'); height: 25px">
                        <span style="font-size: 9pt; color: #ffffff; font-family: Tahoma"><strong>&nbsp;Kayıt 
                        Filtreleme</strong></span></td>
                </tr>
                <tr>
                    <td style="width: 87px; height: 33px">
                        <font face="Tahoma" style="font-size: 8pt">&nbsp;Kayıt</font></td>
                    <td style="width: 8px; height: 33px">
                        <font face="Tahoma" style="font-size: 8pt">:</font></td>
                    <td style="width: 655px; height: 33px">
                        <font face="Verdana" size="1">
                        <asp:TextBox ID="TextBox13" runat="server" maxlength="200" size="30" 
                            style="border-style:groove;font-size:10;font-family:verdana"></asp:TextBox>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td height="32" style="width: 87px">
                        &nbsp;</td>
                    <td height="32" style="width: 8px">
                        &nbsp;</td>
                    <td height="32" style="width: 655px">
                        <asp:Button ID="Button1" runat="server" onclick="Button1_Click" 
                            Text="  Bul  " />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" height="32">
                        <asp:DataList ID="DataList2" runat="server" DataKeyField="KatId" 
                            onitemcommand="DataList2_ItemCommand">
                            <HeaderTemplate>
                                <table border="0" cellspacing="1" width="750"  bgcolor="#333333">
                                    <tr>
                                        <td class="style1" colspan="3" 
                                            style="background-image: url('http://localhost:1501/deyyap%20web/Admin/images/table_bg.gif');">
                                            <span style="font-size: 9pt; color: #ffffff; font-family: Tahoma"><strong>&nbsp;Kayıt 
                                            Listesi</strong></span></td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#E5E5E5" height="25" style="border-bottom: 2px solid #666666" 
                                            width="600">
                                            <font face="Tahoma" style="font-size: 8pt">&nbsp;Kayıt</font></td>
                                        <td bgcolor="#E5E5E5" height="25" style="border-bottom: 2px solid #666666" 
                                            width="69">
                                            <font face="Tahoma" style="font-size: 8pt">&nbsp;</font></td>
                                        <td align="center" bgcolor="#E5E5E5" height="25" 
                                            style="border-bottom: 2px solid #666666" width="70">
                                            <font face="Tahoma" style="font-size: 8pt">&nbsp;İşlem</font></td>
                                    </tr>
                                </table>
                            </HeaderTemplate>
                            <ItemStyle BackColor="#EFEFEF" />
                            <ItemTemplate>
                                <table border="0" cellspacing="1" width="750">
                                    <tr bgcolor="" onmouseout="bgColor=''" onmouseover="bgColor='#ffda75'">
                                        <td width="600">
                                            <asp:Label ID="Label2" runat="server" Font-Names="Tahoma" Font-Size="8pt" 
                                                Text='<%# Eval("KitapAdi") %>'></asp:Label>
                                        </td>
                                        <td width="69">
                                            &nbsp;
                                           </asp:TextBox>
                                        </td>
                                        <td align="center" width="70">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td style="width:24px">
                                                        <asp:ImageButton ID="ImageButton6" runat="server" CommandName="edit" 
                                                            ImageUrl="images/edit.gif" />
                                                    </td>
                                                    <td style="width:24px">
                                                        <asp:ImageButton ID="ImageButton7" runat="server" CommandName="delete" 
                                                            ImageUrl="images/delete.gif" 
                                                            onclientclick="return window.confirm('Silmek istediğinize emin misiniz?');" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:DataList>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" height="10">
                    </td>
                </tr>
            </table>
        </asp:View>
    </asp:MultiView>
</asp:Content>

