$(function () {
	
	
	/*--------------------------------------------------
	Plugin: Lightbox
	--------------------------------------------------*/	
	
	$('.lightbox-type').lightbox ();
	
	/*--------------------------------------------------
	Plugin: Msg Growl
	--------------------------------------------------*/	
	$('.growl-type').live ('click', function (e) {
		$.msgGrowl ({
			type: $(this).attr ('data-type')
			, title: 'Header'
			, text: 'Lorem ipsum dolor sit amet, consectetur ipsum dolor sit amet, consectetur.'
		});
	});	
	/*--------------------------------------------------
	Plugin: Msg Box
	--------------------------------------------------*/
	
	$('.msgbox-alert').live ('click', function (e) {
		$.msgbox("The selection includes process white objects. Overprinting such objects is only useful in combination with transparency effects.");
	});
	
	$('.msgbox-info').live ('click', function (e) {
		$.msgbox("jQuery is a fast and concise JavaScript Library that simplifies HTML document traversing, event handling, animating, and Ajax interactions for rapid web development.", {type: "info"});
	});

	$('.msgbox-nace').live('click', function (e) {
	    $.msgbox("Nace kodunuzu bilmiyorsanız Ticaret Odasından şirket ünvanınızın bir kısmını yazarak sorgulayabilirsiniz..", { type: "info" });
	});
	
	$('.msgbox-error').live ('click', function (e) {
		$.msgbox("An error 1053 ocurred while perfoming this service operation on the MySql Server service.", {type: "error"});
	});
	
	$('.msgbox-confirm').live ('click', function (e) {
		$.msgbox("Are you sure that you want to permanently delete the selected element?", {
		  type: "confirm",
		  buttons : [
		    {type: "submit", value: "Yes"},
		    {type: "submit", value: "No"},
		    {type: "cancel", value: "Cancel"}
		  ]
		}, function(result) {
		  	$("#result2").text(result);
			});
	});
	
	$('.msgbox-prompt').live ('click', function (e) {
		$.msgbox("Insert your name below:", {
		  type: "prompt"
		}, function(result) {
		  if (result) {
		    alert("Hello " + result);
		  }
		});
	});
	
});


function SuccessWithMsg(msg) {
    $.msgGrowl({
        type: 'success'
             , title: 'Tebrikler!'
             , text: msg
    });
}

function CancelWithMsg(msg) {
    $.msgGrowl({
        type: 'warning'
             , title: 'Uyarı!'
             , text: msg
    });
}
function ErrorWithMsg(msg) {
    $.msgGrowl({
        type: 'warning'
             , title: 'Üzgünüz!'
             , text: msg
    });
}

function Success() {
	    $.msgGrowl({
	        type: 'success'
                 , title: 'Tebrikler!'
                 , text: 'İşleminiz başarı ile gerçekleşti, ekranda göremiyorsanız lütfen sayfayı yenileyin.'
	    });
	}



	function Hata() {
	    $.msgGrowl({
	        type: 'warning'
                 , title: 'Üzgünüz!'
                 , text: 'İşlemi sırasında bir hata oluştu, lütfen daha sonra tekrar deneyin.'
	    });
	}

	function Cancel() {
	    $.msgGrowl({
	        type: 'warning'
                 , title: 'Uyarı!'
                 , text: 'İşlemi iptal ettiniz, yaptığınız işlemlerin hiçbiri dikkate alınmadı.'
	    });
	}