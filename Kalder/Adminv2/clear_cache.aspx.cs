﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class clear_cache : System.Web.UI.Page
    {
        CacheProvider cp = new CacheProvider();
        protected void Page_Load(object sender, EventArgs e)
        {
            Dictionary<string, object> s = cp.RemoveAll();

            foreach (KeyValuePair<string, object> kv in s)
            {
                Response.Write("K: " + kv.Key + " - V:" + kv.Value + "<br>");
            }

            ClearCache();
             
        }



        public void ClearCache()
        {
            try
            {
                List<string> keyList = new List<string>();
                IDictionaryEnumerator CacheEnum = HttpContext.Current.Cache.GetEnumerator();

                string cacheKey;

                //Read all the keys from cache and store them in a list

                while (CacheEnum.MoveNext())
                {
                    cacheKey = CacheEnum.Key.ToString();
                    keyList.Add(cacheKey);
                }


                //Remove entries from cache

                foreach (string key in keyList)
                {
                    HttpContext.Current.Cache.Remove(key);
                }

                keyList.Clear();
                Response.Write("Cache Cleared");

            }

            catch
            {
                Response.Write("Cache NOT Cleared");
            }

        }

    }
}