﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class stats : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            //try
            //{

            //}
            //catch (Exception ex)
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:ErrorWithMsg('" + ex.Message + "');", true);
            //}

            if (!Page.IsPostBack)
            {
                DateTime dT = DateTime.Now;
                string tarih = dT.ToString("yyyy.MM.dd");


                txtBeginDate.Text = tarih;
                txtEndDate.Text = tarih;

                getStats(tarih, tarih);
                Browser(tarih, tarih);
                Sayfalar();
                Haberler();
                Location();
            }


        }

        protected void btnKaydet1_Click(object sender, EventArgs e)
        {
            string beginDate, EndDate;
            beginDate = txtBeginDate.Text;
            EndDate = txtEndDate.Text;

            if (Convert.ToDateTime(EndDate) < Convert.ToDateTime(beginDate))
            {
                // Ayarlar.Alert.Show("Bitiş Hat");
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:ErrorWithMsg('Bitiş Tarihi, Başlangıç tarihinden önce olamaz.Lütfen ileri bir tarih seçin.');", true);
                txtEndDate.BackColor = System.Drawing.Color.Red;
            }
            else
            {
                Browser(beginDate, EndDate);
                getStats(beginDate, EndDate);

                txtEndDate.BackColor = System.Drawing.Color.White;
            }



        }


        public void getStats(string beginDate, string endDate)
        {
            //try
            //{
            DataTable dt = db.GetDataTable("select * from Stats where Date between '" + beginDate + "' and '" + endDate + "'");
            Chart1.DataSource = dt;

            DataRow drTekil = db.GetDataRow("Select SUM(UniqeVisitors) as Tekil from Stats where Date between '" + beginDate + "' and '" + endDate + "'");
            DataRow drCogul = db.GetDataRow("Select SUM(TotalVisitors) as Cogul from Stats where Date between '" + beginDate + "' and '" + endDate + "'");
            DataRow drHit = db.GetDataRow("Select SUM(Hit) as Hit from Stats where Date between '" + beginDate + "' and '" + endDate + "'");

            DataRow drUniqeIpToplami = db.GetDataRow("select count(distinct Ip) as Ip_count, count(distinct (case when StatId > 0 then Ip end)) as positive_tag_count from Stats where Date between '" + beginDate + "' and '" + endDate + "'");


            int uniqe=0;
            int total = 0;
            int hit = 0;
            int Ipkontrol = 0;

            try
            {


               

                if (drTekil != null)
                {
                    uniqe = (int)drTekil["Tekil"];
                }
                else
                    uniqe = 0;


                if (drCogul != null)
                {
                    total = (int)drCogul["Cogul"];
                }
                else
                    total = 0;

                if (drHit != null)
                {
                    hit = (int)drHit["Hit"];
                }
                else
                    hit = 0;

                if (drUniqeIpToplami != null)
                {
                    Ipkontrol = (int)drUniqeIpToplami["Ip_count"];
                }
                else
                    Ipkontrol = 0;

                
                float yuzde = (uniqe - Ipkontrol) * 100 / total;

                ltrlYuzde.Text = "%" + yuzde.ToString();

                // set series members names for the X and Y values 
                Chart1.Series["Series1"].XValueMember = "Date";
                Chart1.Series["Series1"].YValueMembers = "TotalVisitors";
                Chart1.Series["Series2"].XValueMember = "Date";
                Chart1.Series["Series2"].YValueMembers = "UniqeVisitors";

                // data bind to the selected data source
                Chart1.DataBind();


                ltrlOturum.Text = total.ToString("N0");
                ltrlTekil.Text = uniqe.ToString("N0");

                ltrlHit.Text = hit.ToString("N0");




                DataRow drOrtalamaSure = db.GetDataRow("Select SUM(AvarageTime) as OrtalamaSure from Stats where Date between '" + beginDate + "' and '" + endDate + "'");

                int seconds = (int)drOrtalamaSure["OrtalamaSure"] / uniqe;

                string toplamsure = ConvertToTimeFormat(seconds);
                ltrlOrtalamaSure.Text = toplamsure.ToString();

            }
            catch (Exception)
            { 
            }










            


       

            //}
            //catch (Exception ex)
            //{
            //    ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:ErrorWithMsg('"+ex.Message+"');", true);
            //}







        }

        public string ConvertToTimeFormat(int seconds)
        {
            TimeSpan ts = new TimeSpan(0, 0, seconds);
            return ts.ToString(@"hh\:mm\:ss");
        }


        void TextControl()
        {
 
        }


        public void Sayfalar()
        {
            DataTable dt = db.GetDataTable("Select Top 6 * from Pages order by Hit desc");
            rptSayfalar.DataSource = dt;
            rptSayfalar.DataBind();
        }

        public void Haberler()
        {
            DataTable dt = db.GetDataTable("Select Top 6 * from Haberler as h inner join HaberKategori as hk on h.KategoriId=hk.KategoriId order by Hit desc");
            rptHaber.DataSource = dt;
            rptHaber.DataBind();
        }

        public void Browser(string beginDate, string endDate)
        {
            DataTable dtBrowser = db.GetDataTable("Select SUM(UniqeVisitors) as Tekil, Browser from Stats where Date between '" + beginDate + "' and '" + endDate + "' group by Browser");
            Chart2.DataSource = dtBrowser;

            Chart2.Series["Series1"].XValueMember = "Browser";
            Chart2.Series["Series1"].YValueMembers = "Tekil";

            //  Chart2.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
            Chart2.DataBind();
        }


        public void Location()
        {
            DataTable dt = db.GetDataTable("Select top 6 SUM(UniqeVisitors) as Tekil, Location from Stats group by Location order by Tekil desc");
            rptLocation.DataSource = dt;
            rptLocation.DataBind();
        }

        protected void rptLocation_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {

                if (DataBinder.Eval(e.Item.DataItem, "Location").ToString() == "")
                {
                    ((Literal)e.Item.FindControl("ltrlLoc")).Text = "<b>***Bilinmiyor***</b>";
                }
                else
                    ((Literal)e.Item.FindControl("ltrlLoc")).Text = DataBinder.Eval(e.Item.DataItem, "Location").ToString();
            }
        }

 



    }
}