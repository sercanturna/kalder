﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class edit_event : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string Id;

        protected void Page_Load(object sender, EventArgs e)
        {
            Id = Request.QueryString["Id"];


            if (!Page.IsPostBack)
            {
                GetData(Id);
            }


        }


        public void GetData(string Id)
        {
            getGalleryCategories();

            DataRow dr = db.GetDataRow("Select * From Events where EventId=" + Id);

            dropOrg.Items.FindByText(dr["Organizator"].ToString()).Selected = true;

            string BaslangicTarihi = String.Format("{0:dd.MM.yyyy}", Convert.ToDateTime(dr["BeginDate"]));
            string BitisTarihi = String.Format("{0:dd.MM.yyyy}", Convert.ToDateTime(dr["EndDate"]));

            txt_Etkinlik_Tarihi.Text = BaslangicTarihi;
            txt_Etkinlik_Bitis_Tarihi.Text = BitisTarihi;

            txt_Etkinlik_Yeri.Text = dr["EventPlace"].ToString();
            txtEtkinlikAdi.Text = dr["EventName"].ToString();
            txtDetay.Text = dr["EventDetail"].ToString();

            bool durum = Convert.ToBoolean(dr["IsActive"]);
            if (durum == false)
                drpDurum.SelectedValue = "0";
            else
                drpDurum.SelectedValue = "1";

            txtSeoUrl.Text = dr["SeoUrl"].ToString();
            txtTitle.Text = dr["Meta_Title"].ToString();
            txtDescription.Text = dr["Meta_Desc"].ToString();
            tags.Text = dr["Meta_Keywords"].ToString();
            txtHit.Text = dr["Hit"].ToString();
            txtVideo.Text = dr["Video"].ToString();

            txtSiraNo.Text = dr["OrderNo"].ToString();

            string GaleriId = dr["GalleryId"].ToString();

            if (GaleriId != null && GaleriId != "")
            {
                drpGaleri.Items.FindByValue(GaleriId).Selected = true;
            }

            imgBrosur.ImageUrl = Page.ResolveUrl("~/upload/Etkinlikler/") + dr["BrochureImg"].ToString();
            imgBrosur.Height = 250;


        }

        public void getGalleryCategories()
        {
            DataTable dt = db.GetDataTable("Select * from GaleriKategori");

            drpGaleri.DataSource = dt;
            drpGaleri.DataValueField = dt.Columns["GaleriId"].ColumnName.ToString();
            drpGaleri.DataTextField = dt.Columns["KategoriAdi"].ColumnName.ToString();
            drpGaleri.DataBind();
            drpGaleri.Items.Insert(0, new ListItem("– Yok –", "0"));
        }





        protected void btnGonder_Click(object sender, EventArgs e)
        {

            string Organizator, EtkinlikAdi, yer, detay, BaslangicTarihi, BitisTarihi, durum, SeoURL, Meta_Title, Meta_Keywords, Meta_Desc, Hit, OlusturmaTarihi, BrosurResmi, uzanti;

            BrosurResmi = db.GetDataCell("select BrochureImg from Events where EventId=" + Id);


            Organizator = dropOrg.SelectedItem.Text;
            EtkinlikAdi = txtEtkinlikAdi.Text;
            yer = txt_Etkinlik_Yeri.Text;
            detay = txtDetay.Text;

            BaslangicTarihi = String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(txt_Etkinlik_Tarihi.Text));
            BitisTarihi = String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(txt_Etkinlik_Bitis_Tarihi.Text));

            durum = drpDurum.SelectedValue;


            SeoURL = txtSeoUrl.Text;
            if (SeoURL == "" || SeoURL == null)
                SeoURL = Ayarlar.UrlSeo(txtEtkinlikAdi.Text);

            Meta_Title = txtTitle.Text;
            Meta_Desc = txtDescription.Text;
            Meta_Keywords = tags.Text;
            Hit = txtHit.Text;
            OlusturmaTarihi = DateTime.Now.ToString();


            if (fuBrosur.HasFile)
            {
                string ExResim = db.GetDataCell("select BrochureImg from Events where EventId=" + Id);
                if (ExResim != null && ExResim != "")
                {
                    FileInfo eskiResim = new FileInfo(Server.MapPath("~/upload/sahte/" + ExResim));
                    eskiResim.Delete();
                }

                uzanti = Path.GetExtension(fuBrosur.PostedFile.FileName);
                BrosurResmi = Ayarlar.OzetCek(Ayarlar.UrlSeo(txtEtkinlikAdi.Text), 20).Trim().Replace(".", "") + "_brosur_" + DateTime.Now.Day + uzanti;
                fuBrosur.SaveAs(Server.MapPath("~/upload/sahte/" + BrosurResmi));

                Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + BrosurResmi));
                resim = Ayarlar.ResimBoyutlandir(resim, 700);

                resim.Save(Server.MapPath("~/upload/Etkinlikler/" + BrosurResmi), ImageFormat.Png);
                // Resmi önce 760 klasörüne kayıt ediyoruz.

                FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + BrosurResmi));
                temp.Delete();
            }



            SqlConnection baglanti = db.baglan();
            SqlCommand cmd = new SqlCommand("SET language turkish; Update Events set " +
               "Organizator=@Organizator," +
                "EventName=@EventName," +
               "BeginDate=@BeginDate," +
               "EndDate=@EndDate," +
               "EventPlace=@EventPlace," +
               "CreatedDate=@CreatedDate," +
               "EventDetail=@EventDetail," +
               "IsActive=@IsActive," +
               "Hit=@Hit," +
               "SeoUrl=@SeoUrl," +
               "Meta_Title=@Meta_Title," +
               "Meta_Keywords=@Meta_Keywords," +
               "Meta_Desc=@Meta_Desc," +
               "BrochureImg=@BrochureImg," +
               "Video=@Video," +
               "GalleryId=@GalleryId" +
               " where EventId=" + Id,
               baglanti);

            cmd.Parameters.AddWithValue("Organizator", Organizator);
            cmd.Parameters.AddWithValue("EventName", EtkinlikAdi);
            cmd.Parameters.AddWithValue("BeginDate", BaslangicTarihi);
            cmd.Parameters.AddWithValue("EndDate", BitisTarihi);
            cmd.Parameters.AddWithValue("EventPlace", yer);
            cmd.Parameters.AddWithValue("CreatedDate", OlusturmaTarihi);
            cmd.Parameters.AddWithValue("EventDetail", detay);
            cmd.Parameters.AddWithValue("IsActive", durum);
            cmd.Parameters.AddWithValue("Hit", Hit);
            cmd.Parameters.AddWithValue("SeoUrl", SeoURL);
            cmd.Parameters.AddWithValue("Meta_Title", Meta_Title);
            cmd.Parameters.AddWithValue("Meta_Keywords", Meta_Keywords);
            cmd.Parameters.AddWithValue("BrochureImg", BrosurResmi);
            cmd.Parameters.AddWithValue("GalleryId", drpGaleri.SelectedValue);
            cmd.Parameters.AddWithValue("Meta_Desc", Meta_Desc);
            cmd.Parameters.AddWithValue("Video", txtVideo.Text);

            int sonuc = cmd.ExecuteNonQuery();
            if (sonuc > 0)
            {
                Ayarlar.Alert.Show("İşlem Başarılı");
                // Response.Redirect("event_management.aspx");
            }
            else
                Ayarlar.Alert.Show("Bir Hata oluştu, Lütfen daha sonra tekrar deneyin");


        }

        protected void btnTemizle_Click(object sender, EventArgs e)
        {
            Response.Redirect("event_management.aspx");
        }


        protected void btnResimTemizle_Click(object sender, ImageClickEventArgs e)
        {
            string ResimAdi = imgBrosur.ImageUrl;

            Response.Write(ResimAdi);
            if (ResimAdi.Contains("NoBrochure.jpg") || ResimAdi =="")
            {
                Ayarlar.Alert.Show("Broşür Resmi zaten yok!");
            }
            else
            {
                string Id = Request.QueryString["Id"];
                //Response.Write(ResimAdi);
                File.Delete(Server.MapPath(ResimAdi));
                int sonuc = db.cmd("Update Events set BrochureImg ='NoBrochure.jpg' where EventId=" + Id);

                if (sonuc > 0)
                    Ayarlar.Alert.Show("İşlem Başarılı");
                else
                    Ayarlar.Alert.Show("Bir Hata Oluştu\nLütfen tekrar deneyin.");
            }
        }
    }
}