﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class edit_pages : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string pId;
        DataRow dr;

        protected void Page_Load(object sender, EventArgs e)
        {
            

            pId = Request.QueryString["pId"].ToString();
            dr = db.GetDataRow("Select * from Pages where PageId=" + pId);

            if (!IsPostBack)
            {
                GetData(pId);

                ViewState["ReferrerUrl"] = Request.UrlReferrer.ToString();
            }
        }

        void returnToSender()
        {
            object referrer = ViewState["ReferrerUrl"];
            if (referrer != null)
                Response.Redirect((string)referrer);
            else
                Response.Redirect("pages.aspx");
        }

        public void GetCat()
        {
            DataTable dtKat = db.GetPages();


            dropKat.DataSource = dtKat;
            dropKat.DataValueField = dtKat.Columns["PageId"].ColumnName.ToString();
            dropKat.DataTextField = dtKat.Columns["SayfaAdi"].ColumnName.ToString();
            dropKat.DataBind();
            dropKat.Items.Insert(0, new ListItem("– yok –", "0"));
        }


        public void GetData(string pId)
        {
            GetCat();
            getGalleryCategories();

            dropKat.SelectedValue = dr["ParentPageId"].ToString();
            // dropKat.Items.RemoveAt(Convert.ToInt32(dr["UrunKatId"].ToString()));

            //  dropKat.Items.Remove(dropKat.Items.FindByValue("UrunKatId"));

            ListItem removeItem = dropKat.Items.FindByValue(dr["PageId"].ToString());
            dropKat.Items.Remove(removeItem);



            txtKategoriAdi.Text = dr["PageName"].ToString();
            txtDetay.Text = dr["PageContent"].ToString();
            imgResim.ImageUrl = "~/upload/SayfaResimleri/thumb/" + dr["PageImage"].ToString();
            txtSiraNo.Text = dr["OrderNumber"].ToString();
            //txtSutun.Text = dr["Cols"].ToString();

            bool durum = Convert.ToBoolean(dr["PageIsActive"]);
            if (durum == false)
                drpDurum.SelectedValue = "0";
            else
                drpDurum.SelectedValue = "1";

            bool showmenu = Convert.ToBoolean(dr["ShowMenu"]);
            if (showmenu == false)
                chkMenudeGoster.Checked = false;
            else
                chkMenudeGoster.Checked = true;



            txtSeoUrl.Text = dr["PageUrl"].ToString();
            txtTitle.Text = dr["Meta_Title"].ToString();
            txtDescription.Text = dr["Meta_Desc"].ToString();
            tags.Text = dr["Meta_Keywords"].ToString();
            txtHit.Text = dr["Hit"].ToString();

            try
            {
                string GaleriId = dr["GalleryId"].ToString();

                if (GaleriId != null && GaleriId != "")
                {
                    drpGaleri.Items.FindByValue(GaleriId).Selected = true;
                }
            }
            catch (Exception)
            {
                 
            }

        


        }

        public void getGalleryCategories()
        {
            DataTable dt = db.GetDataTable("Select * from GaleriKategori");

            drpGaleri.DataSource = dt;
            drpGaleri.DataValueField = dt.Columns["GaleriId"].ColumnName.ToString();
            drpGaleri.DataTextField = dt.Columns["KategoriAdi"].ColumnName.ToString();
            drpGaleri.DataBind();
            drpGaleri.Items.Insert(0, new ListItem("– Yok –", "0"));
        }


        protected void btn_SayfaGuncelle_Click(object sender, EventArgs e)
        {

            string s = txtKategoriAdi.Text;
            if (dropKat.Items.FindByText(s) != null)
            {
                Ayarlar.Alert.Show("Bu sayfayı zaten eklediniz!");
            }
            else
            {
                //save it here...


                if (dropKat.Items.ToString().Contains(s))
                {
                    Ayarlar.Alert.Show("Bu sayfayı zaten eklediniz!");
                }
                else
                {
                    string resimadi = db.GetDataCell("select PageImage from Pages where PageId=" + pId);
                    string uzanti = "";
                    bool ShowMenu;
                    if (chkMenudeGoster.Checked)
                        ShowMenu = true;
                    else
                        ShowMenu = false;



                    if (fuResim.HasFile)
                    {
                        uzanti = Path.GetExtension(fuResim.PostedFile.FileName);
                        resimadi = Ayarlar.OzetCek(Ayarlar.UrlSeo(txtKategoriAdi.Text), 10).Trim() + "_kategoriresim_" + DateTime.Now.Day + uzanti;
                        fuResim.SaveAs(Server.MapPath("~/upload/sahte/" + resimadi));

                        Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + resimadi));
                        resim = Ayarlar.ResimBoyutlandir(resim, 770);

                        resim.Save(Server.MapPath("~/upload/SayfaResimleri/big/" + resimadi), ImageFormat.Jpeg);
                        // Resmi önce 280 klasörüne kayıt ediyoruz.

                        resim = Ayarlar.ResimBoyutlandir(resim, 340);
                        resim.Save(Server.MapPath("~/upload/SayfaResimleri/thumb/" + resimadi), ImageFormat.Jpeg);
                        // Resmi sonrada 220 klasörüne kayıt ediyoruz.

                        FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + resimadi));
                        temp.Delete();
                    }

                    SqlConnection baglanti = db.baglan();
                    SqlCommand cmdKaydet = new SqlCommand("SET language turkish; Update Pages set " +

                    "ParentPageId=@ParentPageId," +
                    "PageName=@PageName," +
                    "PageUrl=@PageUrl," +
                    "PageImage=@PageImage," +
                    "Meta_Title=@Meta_Title," +
                    "Meta_Desc=@Meta_Desc," +
                    "Meta_Keywords=@Meta_Keywords," +
                    "PageContent=@PageContent," +
                    "PageIsActive=@PageIsActive," +
                    "ShowMenu=@ShowMenu," +
                    "Cols=@Cols," +
                    "Hit=@Hit," +
                     "GalleryId=@GalleryId," +
                    "OrderNumber=@OrderNumber" +
                        //"CreatedDate=@CreatedDate"+ 
                    " where PageId=" + pId,
                    baglanti);


                    cmdKaydet.Parameters.AddWithValue("ParentPageId", dropKat.SelectedValue);
                    cmdKaydet.Parameters.AddWithValue("PageName", txtKategoriAdi.Text);
                    cmdKaydet.Parameters.AddWithValue("PageUrl", txtSeoUrl.Text);
                    cmdKaydet.Parameters.AddWithValue("PageImage", resimadi);
                    cmdKaydet.Parameters.AddWithValue("Meta_Title", txtTitle.Text);
                    cmdKaydet.Parameters.AddWithValue("Meta_Keywords", tags.Text);
                    cmdKaydet.Parameters.AddWithValue("Meta_Desc", txtDescription.Text);
                    cmdKaydet.Parameters.AddWithValue("PageContent", txtDetay.Text);
                    cmdKaydet.Parameters.AddWithValue("PageIsActive", drpDurum.SelectedValue);
                    cmdKaydet.Parameters.AddWithValue("ShowMenu", ShowMenu);
                    cmdKaydet.Parameters.AddWithValue("Cols", "0");
                    cmdKaydet.Parameters.AddWithValue("GalleryId", drpGaleri.SelectedValue);
                    cmdKaydet.Parameters.AddWithValue("Hit", txtHit.Text);
                    // cmdKaydet.Parameters.AddWithValue("CreatedDate", DateTime.Now.ToString("MM.dd.yyyy hh:mm:ss"));
                    cmdKaydet.Parameters.AddWithValue("OrderNumber", txtSiraNo.Text);



                    int sonuc = cmdKaydet.ExecuteNonQuery();



                    if (sonuc > 0)
                    {
                        // Ayarlar.Alert.result();
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                        Ayarlar.Alert.Show("İşlem Başarı ile gerçekleşti");
                       // returnToSender();
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);




                }
            }

        }






        protected void dropKat_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dropKat.SelectedValue != "0")
            {
                chkMenudeGoster.Enabled = false;
                //txtSutun.Enabled = false;
            }
            else
            {
                chkMenudeGoster.Enabled = true;
                //txtSutun.Enabled = true;
            }
        }
        protected void btnResimTemizle_Click(object sender, ImageClickEventArgs e)
        {
            string ResimAdi = imgResim.ImageUrl;
            string pageId = Request.QueryString["pId"];
            //Response.Write(ResimAdi);
            File.Delete(Server.MapPath(ResimAdi));
            int sonuc = db.cmd("Update Pages set PageImage ='resim_yok.jpg' where PageId=" + pageId);

            if (sonuc > 0)
                Ayarlar.Alert.Show("İşlem Başarılı");
            else
                Ayarlar.Alert.Show("Bir Hata Oluştu\nLütfen tekrar deneyin.");


            //File.Delete(Server.MapPath("~/upload/bg.jpg"));

            //File.Copy(Server.MapPath("~/upload/defaultbg.jpg"), Server.MapPath("~/upload/bg.jpg"));
            ////imgResim.ImageUrl = "bg.jpg";
            //Response.Redirect(Path.GetFileName(Page.AppRelativeVirtualPath));
        }
    }
}