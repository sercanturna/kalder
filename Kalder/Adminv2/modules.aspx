﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="modules.aspx.cs" Inherits="Kalder.Adminv2.modules" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script src="js/jquery-sortable.js"></script>

    <style>
    .sorted_table tr {
        cursor:move;
    }
    .sorted_table thead tr {
    cursor:no-drop;
    }

    .sorted_table tr.placeholder {
        display: block;
        background: red;
        position: relative;
        margin: 0;
        padding: 0;
        border: none;
                
    }

    .sorted_table tr.placeholder:before {
        content: "";
        position: absolute;
        width: 0;
        height: 0;
        border: 5px solid transparent;
        border-left-color: red;
        margin-top: -5px;
        left: -5px;
        border-right: none;
    }

    span.islem {
        position: absolute;
        z-index: 10000;
        margin-top: -10px;
        width:100px;
    }
    </style>
     <script type="text/javascript">
         function pageLoad() {
             $(function () {

                 // Sortable rows
                $('.sorted_table').sortable({
                   
                     containerSelector: 'table',
                     itemPath: '> tbody',
                     itemSelector: 'tr',
                     placeholder: '<tr class="placeholder"/>',
                  

                     onDrop: function (item, container, _super) {

                         var data = $('.sorted_table').sortable("serialize").get();

                         var jsonString = JSON.stringify(data, null, ' ');
                        // document.write(data);
                         // $.post("kaydet.aspx", data);
                         $.ajax({
                             url: 'kaydet.aspx',
                             data: "data=" + data,
                             type: 'POST'
                         })
                             .fail(function () {
                                 Error();
                             })
                         .done(function (msg) {
                             Success();
                         });
                     },

                    serialize: function (parent, children, isContainer) {
                    return isContainer ? children.join() : parent.text()
                }
                 })
             });
         };



    </script>


</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="Server">
    <asp:ScriptManager ID="criptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
   
            <div class="row">

                <div class="span4">


                    <div class="widget stacked widget-box">

                        <div class="widget-header ">
                            <i class="icon-pushpin"></i>
                            <h3>Klavuz</h3>
                        </div>
                        <!-- /widget-header -->

                        <div class="widget-content">

                            <div style="border: 2px solid #000; width: 99%; height: 206px; text-align: center;">
                                <div style="width: 100%; height: 30px; border: 1px solid #808080; margin-bottom:1px;">Gövde</div>
                                <%--<div style="width:278px; height:30px; border:1px solid #108ca8;">Gövde</div>--%>
                                <div style="width: 27%; border: 1px solid #808080; float: left; height: 136px;">Kenar</div>
                                <div style="width: 71%; border: 1px solid #808080; float: right; height: 136px; text-align: center;">İçerik<div id="anasayfakonum" runat="server" visible="false" style="width: 190px; border: 1px solid red; height: 50px; margin: 0px auto; margin-top: 55px;">Anasayfa içerik</div>
                                </div>
                                <div style="width: 99%; border: 1px solid #808080; height: 30px; float: left; margin-top:1px;">Sayfasonu</div>
                            </div>

                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget -->

                </div>
                <!-- /span4 -->


                <div class="span8">

                    <div class="widget stacked">

                        <div class="widget-header">
                            <i class="icon-check"></i>
                            <h3>Modül Yönetimi</h3>
                        </div>
                        <!-- /widget-header -->

                        <div class="widget-content">

                            <asp:DropDownList ID="dropSegment" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dropSegment_SelectedIndexChanged"></asp:DropDownList>
                            <asp:DropDownList ID="dropSections" runat="server" AutoPostBack="true"></asp:DropDownList>
                            <asp:LinkButton ID="lnkGetModules" OnClick="lnkGetModules_Click" runat="server" CssClass="btn btn-success btn-primary" Text="Modülleri Getir" Style="margin-top: 0; display: inline-block; float: right;"></asp:LinkButton>


                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget -->

                </div>
                <!-- /span8 -->






                <div class="span8">

                    <div class="widget stacked widget-table action-table">

                        <div class="widget-header">
                            <i class="icon-th-list"></i>
                            <h3>Modüller</h3>
                        </div>
                        <!-- /widget-header -->

                        <div class="widget-content">
                            <div class="promosyonlar">

                                <div style="clear: both"></div>
                                <div id="icerik">
                                    <asp:Label ID="lblSonucText" runat="server" Visible="false"></asp:Label>
                                    <table class="table table-bordered table-striped sorted_table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Modül Adı</th>
                                                <th>Bölüm</th>
                                                <th>SıraNo</th>

                                                <th>Yayın Durumu</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <asp:Repeater ID="rptListe" runat="server" OnItemDataBound="rptListe_OnItemDataBound" OnItemCommand="rptListe_ItemCommand">
                                                <ItemTemplate>
                                                    
                                                    <tr id='SiraNo_<%#Eval("ModuleId") %>'>
                                                        <td id="gonder" style="display:none;"><%#Eval("ModuleId") %>-<%#Eval("SegmentId") %>-<%#Eval("SectionId") %>-</td>
                                                        <td><%#Eval("ModuleId") %></td>
                                                        <td><%#Eval("ModuleName") %></td>
                                                        <td><%#Eval("SectionName") %></td>
                                                        <td><%#Eval("OrderNo") %></td>
                                                        <td><span class="islem">
                                                            <asp:UpdatePanel ID="upRep" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:LinkButton ID="lnkAction" CommandArgument='<%# Eval("ModuleId") + "," + Eval("SectionId") + "," + Eval("SegmentId")  %>' runat="server"> </asp:LinkButton></span>
                                                                    <asp:LinkButton ID="lnkEditModule" CommandName="Edit" CommandArgument='<%# Eval("ModuleId") %>' runat="server"></asp:LinkButton>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="lnkAction" EventName="Click" />
                                                                    <asp:AsyncPostBackTrigger ControlID="lnkEditModule" EventName="Click" />
                                                                </Triggers>
                                                            </asp:UpdatePanel> </td>
                                                    </tr>

                                                </ItemTemplate>
                                            </asp:Repeater>

                                        </tbody>
                                    </table>
                                </div>
                            </div>





                        </div>
                    </div>
                </div>
            </div>
            <!-- /row -->
        </ContentTemplate>

    </asp:UpdatePanel>
</asp:Content>
