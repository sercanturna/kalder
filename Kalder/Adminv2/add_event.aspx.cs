﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class add_event : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                getGalleryCategories();
            }

            // pnl_Etkinlikler.Visible = false;

        }


        public void getGalleryCategories()
        {
            DataTable dt = db.GetDataTable("Select * from GaleriKategori");

            drpGaleri.DataSource = dt;
            drpGaleri.DataValueField = dt.Columns["GaleriId"].ColumnName.ToString();
            drpGaleri.DataTextField = dt.Columns["KategoriAdi"].ColumnName.ToString();
            drpGaleri.DataBind();
            drpGaleri.Items.Insert(0, new ListItem("– Yok –", "0"));
        }

        protected void btnGonder_Click(object sender, EventArgs e)
        {

            string EtkinlikAdi, yer, detay, BaslangicTarihi, BitisTarihi, durum, SeoURL, Meta_Title, Meta_Keywords, Meta_Desc, Hit, OlusturmaTarihi, BrosurResmi, uzanti;

            EtkinlikAdi = txtEtkinlikAdi.Text;
            yer = txt_Etkinlik_Yeri.Text;
            detay = txtDetay.Text;

            BaslangicTarihi = String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(txt_Etkinlik_Tarihi.Text));
            BitisTarihi = String.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(txt_Etkinlik_Bitis_Tarihi.Text));

            durum = drpDurum.SelectedValue;


            SeoURL = txtSeoUrl.Text;
            if (SeoURL == "" || SeoURL == null)
                SeoURL = Ayarlar.UrlSeo(txtEtkinlikAdi.Text);

            Meta_Title = txtTitle.Text;
            Meta_Desc = txtDescription.Text;
            Meta_Keywords = tags.Text;
            Hit = txtHit.Text;
            OlusturmaTarihi = DateTime.Now.ToString();

            BrosurResmi = "";

            if (fuBrosur.HasFile)
            {
                uzanti = Path.GetExtension(fuBrosur.PostedFile.FileName);
                BrosurResmi = Ayarlar.OzetCek(Ayarlar.UrlSeo(txtEtkinlikAdi.Text), 10).Trim().Replace(".", "") + "_brochure_" + DateTime.Now.Day + uzanti;
                fuBrosur.SaveAs(Server.MapPath("~/upload/sahte/" + BrosurResmi));

                Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + BrosurResmi));
                resim = Ayarlar.ResimBoyutlandir(resim, 700);

                resim.Save(Server.MapPath("~/upload/Etkinlikler/" + BrosurResmi), ImageFormat.Jpeg);
                // Resmi önce 760 klasörüne kayıt ediyoruz.

                FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + BrosurResmi));
                temp.Delete();
            }

            SqlConnection baglanti = db.baglan();
            SqlCommand cmd = new SqlCommand("SET language turkish; insert into Events (" +
               "Organizator," +
               "EventName," +
               "BeginDate," +
               "EndDate," +
               "EventPlace," +
               "CreatedDate," +
               "EventDetail," +
               "IsActive," +
               "Hit," +
               "SeoUrl," +
               "Meta_Title," +
               "Meta_Keywords," +
               "Meta_Desc," +
               "GalleryId," +
               "BrochureImg," +
               "Video," +
               "OrderNo) values (" +
               "@Organizator," +
               "@EventName," +
               "@BeginDate," +
               "@EndDate," +
               "@EventPlace," +
               "@CreatedDate," +
               "@EventDetail," +
               "@IsActive," +
               "@Hit," +
               "@SeoUrl," +
               "@Meta_Title," +
               "@Meta_Keywords," +
               "@Meta_Desc," +
               "@GalleryId," +
               "@BrochureImg," +
                   "@Video," +
               "@OrderNo)",

               baglanti);

            cmd.Parameters.AddWithValue("Organizator", dropOrg.SelectedItem.Text);
            cmd.Parameters.AddWithValue("EventName", EtkinlikAdi);
            cmd.Parameters.AddWithValue("BeginDate", BaslangicTarihi);
            cmd.Parameters.AddWithValue("EndDate", BitisTarihi);
            cmd.Parameters.AddWithValue("EventPlace", yer);
            cmd.Parameters.AddWithValue("CreatedDate", OlusturmaTarihi);
            cmd.Parameters.AddWithValue("EventDetail", detay);
            cmd.Parameters.AddWithValue("IsActive", durum);
            cmd.Parameters.AddWithValue("Hit", Hit);
            cmd.Parameters.AddWithValue("SeoUrl", SeoURL);
            cmd.Parameters.AddWithValue("Meta_Title", Meta_Title);
            cmd.Parameters.AddWithValue("Meta_Keywords", Meta_Keywords);
            cmd.Parameters.AddWithValue("Meta_Desc", Meta_Desc);
            cmd.Parameters.AddWithValue("GalleryId", drpGaleri.SelectedValue);
            cmd.Parameters.AddWithValue("BrochureImg", BrosurResmi);
            cmd.Parameters.AddWithValue("Video", txtVideo.Text);
            cmd.Parameters.AddWithValue("OrderNo", txtSiraNo.Text);

            int sonuc = cmd.ExecuteNonQuery();
            if (sonuc > 0)

                db.MailGonder("web@kalder.org", "KalDer | Etkinlik Takvimi", "etkinliktakvimi@kalder.org", "Etkinlik Takvimi Grubu", EtkinlikAdi + "Etkinliği Eklendi", BaslangicTarihi + " ile " + BitisTarihi + " tarihleri arasında " + yer + " adlı mekanda " + EtkinlikAdi + " adlı etkinlik düzenlenecektir.", "web@kalder.org", "!KalDer2017!", "smtp.office365.com");
               // db.MailGonder("kalderweb@hotmail.com", "Etkinlik Takvimi", "web@kalder.org", "Etkinlik Takvimi Grubu", "Etkinlik Oluşturuldu", BaslangicTarihi + " ile " + BitisTarihi + " tarihleri arasında " + yer + " adlı mekanda " + EtkinlikAdi + " adlı etkinlik düzenlenecektir.", "info@studyexpo.com", "Inf0123", "mail.studyexpo.com");
            //Response.Redirect("event_management.aspx");
            else
                Ayarlar.Alert.Show("Bir Hata Oluştu!");



        }

        protected void btnTemizle_Click(object sender, EventArgs e)
        {
            Response.Redirect("event_management.aspx");
        }

    }
}