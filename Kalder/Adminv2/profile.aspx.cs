﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class profile : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string AdminId, parola;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Userinfo Coockie'sinin olup olmadığını denetliyoruz.
            HttpCookie CportalUserCookie = Request.Cookies["UserInfo"];

            if (CportalUserCookie != null)
            {
                AdminId = CportalUserCookie["AdminId"];
            }
            else
                AdminId = Session["AdminId"].ToString();

            if (!Page.IsPostBack)
            {

                DataRow drAyarlar = db.GetDataRow("Select * from Admin where AdminId=" + AdminId);

                txtusername.Text = drAyarlar["KullaniciAdi"].ToString();
                txtName.Text = drAyarlar["AdiSoyadi"].ToString();
                txtemail.Text = drAyarlar["Eposta"].ToString();

                DataTable dtYetki = db.GetDataTable("select * from AdminYetkileri");
                dropYetki.DataSource = dtYetki;
                dropYetki.DataValueField = dtYetki.Columns["YetkiId"].ColumnName.ToString();
                dropYetki.DataTextField = dtYetki.Columns["Yetki"].ColumnName.ToString();
                dropYetki.DataBind();

                string Yetki = db.GetDataCell("select b.YetkiId from Admin as a inner join AdminYetkileri as b on a.YetkiId = b.YetkiId where AdminId=" + AdminId);

                dropYetki.SelectedValue = Yetki;
                dropYetki.Enabled = false;

            }

        }
        protected void btnKaydet_Click(object sender, EventArgs e)
        {
            if (txtpassword1.Text == "" || txtpassword2.Text == "")
                parola = db.GetDataCell("select Parola from Admin where AdminId=" + AdminId);
            else
            {
                if(txtpassword1.Text != txtpassword2.Text)
                    ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:ErrorWithMsg('Girmiş olduğunuz parolalar birbiri ile eşleşmiyor!');", true);
                else
                parola = Ayarlar.md5(txtpassword1.Text);
            }
               


            #region DbAction

            SqlConnection conn = db.baglan();
            SqlCommand cmd = new SqlCommand("Update Admin set " +
            "AdiSoyadi=@AdiSoyadi," +
            "YetkiId=@YetkiId," +
            "Eposta=@Eposta," +
            "KullaniciAdi=@KullaniciAdi," +
            "Parola=@Parola" +
                    " where AdminId=" + AdminId, conn);

            cmd.Parameters.AddWithValue("AdiSoyadi", txtName.Text);
            cmd.Parameters.AddWithValue("YetkiId", "1");
            cmd.Parameters.AddWithValue("Eposta", txtemail.Text);
            cmd.Parameters.AddWithValue("KullaniciAdi", txtusername.Text);
            cmd.Parameters.AddWithValue("Parola", parola);

            int sonuc = cmd.ExecuteNonQuery();

            if (sonuc > 0)
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);







            #endregion
        }
        protected void btnIptal_Click(object sender, EventArgs e)
        {
            //  ClientScript.RegisterClientScriptBlock(this.GetType(), "Javascript", "Javascript:Cancel();",true);


            //Page.ClientScript.RegisterClientScriptBlock(typeof(Page), "Javascript", "Javascript:Cancel();", true);

            // ClientScript.RegisterStartupScript(this.GetType(), "Javascript", "Javascript:Cancel();", true);
            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Cancel();", true);
        }
    }
}