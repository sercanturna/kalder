﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="admin_group.aspx.cs" Inherits="Kalder.Adminv2.admin_group" %>

 

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style>
        div.row div.span12 div.widget section#tables div.EU_TableScroll table tr td input, table.blank tr td input, div.row div.span12 div.widget section#tables div.EU_TableScroll table tr td select {
            width: 80%;
            margin: 0;
        }



        div.row div.span12 div.widget section#tables div.EU_TableScroll table {
            border-radius: 4px;
        }

            div.row div.span12 div.widget section#tables div.EU_TableScroll table tr.Ekle td {
                position: relative;
                height: 40px;
                line-height: 40px;
                background: #E9E9E9;
                background: -moz-linear-gradient(top, #fafafa 0%, #e9e9e9 100%);
                /* FF3.6+ */
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #fafafa), color-stop(100%, #e9e9e9));
                /* Chrome,Safari4+ */
                background: -webkit-linear-gradient(top, #fafafa 0%, #e9e9e9 100%);
                /* Chrome10+,Safari5.1+ */
                background: -o-linear-gradient(top, #fafafa 0%, #e9e9e9 100%);
                /* Opera11.10+ */
                background: -ms-linear-gradient(top, #fafafa 0%, #e9e9e9 100%);
                /* IE10+ */
                background: linear-gradient(top, #fafafa 0%, #e9e9e9 100%);
                /* W3C */
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#FAFAFA', endColorstr='#E9E9E9');
                -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr='#FAFAFA', endColorstr='#E9E9E9')";
                border: 1px solid #D5D5D5;
                -webkit-border-top-left-radius: 4px;
                -webkit-border-top-right-radius: 4px;
                -moz-border-radius-topleft: 4px;
                -moz-border-radius-topright: 4px;
                border-top-left-radius: 4px;
                border-top-right-radius: 4px;
                -webkit-background-clip: padding-box;
            }

        .modal-body {
            max-height: 70%;
        }

        .modal {
            height: auto;
            width: 64.5%;
            max-width: 1240px;
            margin-left: -32.4%;
        }

        @media (max-width: 768px) {
            .modal {
                width: 90%;
                margin-left: 2%;
            }
        }

        @media (min-width: 1921px) {
            .modal {
                width: 64.5%;
                max-width: 1240px;
                margin-left: -24.4%;
            }
        }
    </style>

     

    <script>
        $(document).ready(function () {

            $('#myModal').on('hidden', function () {
               location.reload();

                var UpdatePanel1 = '<%=UpdatePanel5.ClientID%>';

                function ShowItems() {
                    if (UpdatePanel5 != null) {
                        __doPostBack(UpdatePanel5, '');
                    }
                }
            })

        });

    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="Server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"></asp:ScriptManager>
      

    <div class="row">

        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-user-md"></i>
                    <h3>Yöneticilerim</h3>
                </div>
                <!-- /widget-header -->
                <div class="widget-content">

                   <%-- <a href="admin_permissions.aspx?AId=135" class="btn btn-small" data-toggle="modal" data-target="#myModal"><i class="btn-icon-only icon-bell"></i>Test</a>--%>
                    

                    <asp:Panel ID="pnlKontrolResult" runat="server">
                        <div>
                            <p>
                                <img src="img/notifications/error-48.png" class="left" style="padding-right: 10px;" />Üzgünüz Bu bölümü görüntüleme yetkiniz yok! Lütfen site yöneticiniz ile görüşün.</p>
                        </div>
                    </asp:Panel>
                    <section id="tables">
                                
                        <div class="EU_TableScroll">
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                <ContentTemplate>
                            <!--Modal içeriği buraya yüklenecek-->
                            <div class="modal fade hide" id="myModal">
                                <div class="modal-body">
                                    <p>dsfdsd</p>
                                </div>
                            </div>  

                                    <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-striped" AllowPaging="True" 
                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="AdminId" DataSourceID="SqlDataSource1" ShowFooter="True" 
                             OnRowDeleting="GridView1_RowDeleting" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowUpdating="GridView1_RowUpdating" OnDataBinding="GridView1_DataBinding"
                              FooterStyle-CssClass="Ekle" EditRowStyle-BackColor="SlateGray" ValidateRequestMode="Enabled">
                            <Columns>
                                
                                <asp:BoundField DataField="AdminId" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="AdminId" />
                                <asp:TemplateField HeaderText="Yetki" SortExpression="YetkiId">
                                    <EditItemTemplate>
                                         <asp:DropDownList ID="ddlAuth" runat="server" CausesValidation="true" SelectedValue='<%# Bind("YetkiId") %>'>
                                            <asp:ListItem Value="1">Üst Yönetici</asp:ListItem>
                                            <asp:ListItem Value="2">Yönetici</asp:ListItem>
                                        </asp:DropDownList>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("Yetki") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:DropDownList ID="ddlYetkiEkle" runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0">-Seçiniz-</asp:ListItem>
                                            <asp:ListItem Value="1">Üst Yönetici</asp:ListItem>
                                            <asp:ListItem Value="2">Yönetici</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="reqAut" runat="server" ErrorMessage="Yetki Seçimi Yapmadınız" Text="*" ForeColor="Red" ValidationGroup="Insert" ControlToValidate="ddlYetkiEkle" InitialValue="0"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Adı Soyadı" InsertVisible="False" SortExpression="AdiSoyadi">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtEditName" runat="server" Text='<%# Bind("AdiSoyadi") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqName" runat="server" ErrorMessage="Adı Soyadı zorunludur" Text="*" ForeColor="Red" ValidationGroup="Edit" ControlToValidate="txtEditName"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Bind("AdiSoyadi") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtAdiSoyadiEkle" runat="server" ></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqInsertName" runat="server" ErrorMessage="Adı Soyadı zorunludur" Text="*" ForeColor="Red" ValidationGroup="Insert" ControlToValidate="txtAdiSoyadiEkle"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Kullanıcı Adı" SortExpression="KullaniciAdi">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtKullaniciAdi" runat="server" Text='<%# Bind("KullaniciAdi") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqUserName" runat="server" ErrorMessage="Kullanıcı Adı zorunludur" Text="*" ForeColor="Red" ValidationGroup="Edit" ControlToValidate="txtKullaniciAdi"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("KullaniciAdi") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtKullaniciAdiEkle" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqUserName" runat="server" ErrorMessage="Kullanıcı Adı zorunludur" Text="*" ForeColor="Red" ValidationGroup="Insert" ControlToValidate="txtKullaniciAdiEkle"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Parola" SortExpression="Parola">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtParolaDuzenle" runat="server" TextMode="Password" Text='<%# Bind("Parola") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqPassw" runat="server" ErrorMessage="Parola zorunludur" Text="*" ForeColor="Red" ValidationGroup="Edit" ControlToValidate="txtParolaDuzenle"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label4"  runat="server" Text='*****'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtParolaEkle" TextMode="Password" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqInsertPassword" runat="server" ErrorMessage="Parola Adı zorunludur" Text="*" ForeColor="Red" ValidationGroup="Insert" ControlToValidate="txtParolaEkle"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Eposta" SortExpression="Eposta">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtEmail" runat="server" Text='<%# Bind("Eposta") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqEmail" runat="server" ErrorMessage="Eposta adresi zorunludur" Text="*" ForeColor="Red" ValidationGroup="Edit" ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidatorEmail"  Text="*" ForeColor="Red" ControlToValidate="txtEmail" runat="server" ErrorMessage="Eposta adresi geçerli değil!" ValidationGroup="Edit" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("Eposta") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtEpostaEkle" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqInsertEmail" runat="server" ErrorMessage="Eposta Adresi zorunludur" Text="*" ForeColor="Red" ValidationGroup="Insert" ControlToValidate="txtEpostaEkle"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1"  Text="*" ForeColor="Red" ControlToValidate="txtEpostaEkle" runat="server" ErrorMessage="Eposta adresi geçerli değil!" ValidationGroup="Insert" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="İşlem" ConvertEmptyStringToNull="False" ValidateRequestMode="Enabled">
                                    <EditItemTemplate>
                                       <asp:UpdatePanel ID="upOnayla" runat="server">
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="LinkDuzenle" />
                                            </Triggers>
                                            <ContentTemplate>
                                        <asp:LinkButton ID="LinkDuzenle" runat="server"  ValidationGroup="Edit"  CssClass="btn btn-small btn-warning" ToolTip="Onayla" CausesValidation="True" CommandName="Update"  Text="">
                                            <i class="btn-icon-only icon-ok"></i>	
                                        </asp:LinkButton>
                                        &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-small" ToolTip="İptal"  CausesValidation="False" CommandName="Cancel" Text="İptal">
                                             <i class="btn-icon-only icon-circle-arrow-left"></i>
                                              </asp:LinkButton>
                                                    </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-small" ToolTip="Düzenle" CausesValidation="true" CommandName="Edit" Text="">
                                             <i class="btn-icon-only icon-pencil"></i>	
                                        </asp:LinkButton>
                                        &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-small" ToolTip="Sil" CausesValidation="False" CommandName="Delete" Text="" OnClientClick="return confirm('Bu kişiyi silmek istediğinize emin misiniz?');">
                                            <i class="btn-icon-only icon-remove"></i>
                                              </asp:LinkButton>

<%--<a href="admin_permissions.aspx?AId=135" class="btn btn-small" data-toggle="modal" data-target="#myModal"><i class="btn-icon-only icon-bell"></i></a>--%>
                                            &nbsp;<asp:LinkButton ID="lnklPermission" runat="server" CssClass="btn btn-small modal-link" data-target="#myModal" ToolTip="İzinleri Yönet" CausesValidation="False" Text="İzinleri Yönet">
                                             <i class="btn-icon-only icon-bookmark"></i>
                                                    </asp:LinkButton>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:LinkButton ID="lnkEkle" runat="server" OnClick="lnkEkle_Click" ValidationGroup="Insert" CausesValidation="true"  Text="Ekle" CssClass="btn btn-success btn-primary"></asp:LinkButton>
                                    </FooterTemplate>
                                </asp:TemplateField>


                            </Columns>

                            <EmptyDataTemplate>
                                 
                                
                                <table class="table table-bordered table-striped blank" cellspacing="0" border="1" style="border-collapse: collapse;">
                                    <tr>
                                        <th scope="col"> - </th>
                                        <th scope="col">ID</th>
                                        <th scope="col">Yetki Düzeyi</th>
                                        <th scope="col">Adı Soyadı</th>
                                        <th scope="col">Kullanıcı Adı</th>
                                        <th scope="col">Parola</th>
                                        <th scope="col">E-posta Adresi</th>
                                    </tr>
                                    <tr>
                                        <td colspan="9" style="text-align:center; padding:20px;" ><asp:Label CssClass="alert" ID="ltrlSonuc" runat="server" Text="Henüz bir admin yetkilisi eklenmemiş. Lütfen aşağıdaki formu kullanarak bir tane ekleyin."></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                         <td>&nbsp;</td>
                                         <td>
                                            <asp:DropDownList ID="ddlYetkiEkle" runat="server" AppendDataBoundItems="true">
                                            <asp:ListItem Value="0">-Seçiniz-</asp:ListItem>
                                            <asp:ListItem Value="1">Üst Yönetici</asp:ListItem>
                                            <asp:ListItem Value="2">Yönetici</asp:ListItem>
                                            </asp:DropDownList>
                                         </td>
                                         
                                        <td>
                                            <asp:TextBox ID="txtAdiSoyadiEkle" runat="server" ></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqInsertName" runat="server" ErrorMessage="Adı Soyadı zorunludur" Text="*" ForeColor="Red" ValidationGroup="NewInsert" ControlToValidate="txtAdiSoyadiEkle"></asp:RequiredFieldValidator>
                                        </td>

                                       
                                         <td>
                                             <asp:TextBox ID="txtKullaniciAdiEkle" runat="server"></asp:TextBox>
                                             <asp:RequiredFieldValidator ID="reqUserName" runat="server" ErrorMessage="Kullanıcı Adı zorunludur" Text="*" ForeColor="Red" ValidationGroup="NewInsert" ControlToValidate="txtKullaniciAdiEkle"></asp:RequiredFieldValidator>
                                         </td>

                                          <td>
                                              <asp:TextBox ID="txtParolaEkle" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqInsertPassword" runat="server" ErrorMessage="Parola Adı zorunludur" Text="*" ForeColor="Red" ValidationGroup="NewInsert" ControlToValidate="txtParolaEkle"></asp:RequiredFieldValidator>

                                         </td>
                                         
                                        
                                        <td> <asp:TextBox ID="txtEpostaEkle" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="reqInsertEmail" runat="server" ErrorMessage="Eposta Adresi zorunludur" Text="*" ForeColor="Red" ValidationGroup="NewInsert" ControlToValidate="txtEpostaEkle"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1"  Text="*" ForeColor="Red" ControlToValidate="txtEpostaEkle" runat="server" ErrorMessage="Eposta adresi geçerli değil!" ValidationGroup="Insert" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                        </td>
                                         


<td>
                                          <asp:UpdatePanel ID="upEmptyEkle" runat="server">
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkEmtyEkle" />
                                            </Triggers>
                                            <ContentTemplate>
                                              <asp:LinkButton ID="lnkEmtyEkle" runat="server" OnClick="lnkEkle_Click" ValidationGroup="NewInsert" CausesValidation="true" Text="Ekle" CssClass="btn btn-success btn-primary"></asp:LinkButton>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>


                                         
                                             
                                              
                                                
                                       
                                         </td>
                                    </tr>
                                </table>
                             
                            </EmptyDataTemplate>



                            <EditRowStyle BackColor="SlateGray" />
                            <FooterStyle CssClass="Ekle" />
                            <PagerSettings Mode="NumericFirstLast" />
                            <PagerStyle CssClass="pagination" />
                        </asp:GridView>
                                    

                                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" OnInserted="SqlDataSource1_Inserted" OnUpdating="SqlDataSource1_Updating"  OnDeleted="SqlDataSource1_Deleted" ConnectionString="<%$ ConnectionStrings:KalderConnectionString %>"
                                        DeleteCommand="DELETE FROM [Admin] WHERE [AdminId] = @original_AdminId "
                                        InsertCommand="INSERT INTO [Admin] ([YetkiId], [AdiSoyadi], [KullaniciAdi], [Parola], [Eposta]) VALUES (@YetkiId, @AdiSoyadi, @KullaniciAdi, @Parola, @Eposta) 
                            SET @LastID = Scope_Identity()"
                                        OldValuesParameterFormatString="original_{0}"
                                        SelectCommand="SELECT * FROM [Admin] as a inner join AdminYetkileri as ay on a.YetkiId=ay.YetkiId where AdminId != @AdminId"
                                        UpdateCommand="UPDATE [Admin] SET [YetkiId] = @YetkiId, [AdiSoyadi] = @AdiSoyadi, [KullaniciAdi] = @KullaniciAdi, [Parola] = @Parola, [Eposta] = @Eposta WHERE [AdminId] = @original_AdminId AND (([YetkiId] = @original_YetkiId) OR ([YetkiId] IS NULL AND @original_YetkiId IS NULL)) AND (([AdiSoyadi] = @original_AdiSoyadi) OR ([AdiSoyadi] IS NULL AND @original_AdiSoyadi IS NULL)) AND (([KullaniciAdi] = @original_KullaniciAdi) OR ([KullaniciAdi] IS NULL AND @original_KullaniciAdi IS NULL)) AND (([Parola] = @original_Parola) OR ([Parola] IS NULL AND @original_Parola IS NULL)) AND (([Eposta] = @original_Eposta) OR ([Eposta] IS NULL AND @original_Eposta IS NULL))">
                                        <SelectParameters>
                                            <asp:SessionParameter Name="AdminId" SessionField="AdminId" Type="String" />
                                        </SelectParameters>
                                        <DeleteParameters>
                                            <asp:Parameter Name="original_AdminId" Type="Int32" />
                                            <asp:Parameter Name="original_YetkiId" Type="Int32" />
                                            <asp:Parameter Name="original_AdiSoyadi" Type="String" />
                                            <asp:Parameter Name="original_KullaniciAdi" Type="String" />
                                            <asp:Parameter Name="original_Parola" Type="String" />
                                            <asp:Parameter Name="original_Eposta" Type="String" />
                                        </DeleteParameters>
                                        <InsertParameters>
                                            <asp:Parameter Name="YetkiId" Type="Int32" />
                                            <asp:Parameter Name="AdiSoyadi" Type="String" />
                                            <asp:Parameter Name="KullaniciAdi" Type="String" />
                                            <asp:Parameter Name="Parola" Type="String" />
                                            <asp:Parameter Name="Eposta" Type="String" />
                                            <asp:Parameter Direction="Output" Name="LastID" Type="Int32" />
                                        </InsertParameters>
                                        <UpdateParameters>
                                            <asp:Parameter Name="YetkiId" Type="Int32" />
                                            <asp:Parameter Name="AdiSoyadi" Type="String" />
                                            <asp:Parameter Name="KullaniciAdi" Type="String" />
                                            <asp:Parameter Name="Parola" Type="String" />
                                            <asp:Parameter Name="Eposta" Type="String" />
                                            <asp:Parameter Name="original_AdminId" Type="Int32" />
                                            <asp:Parameter Name="original_YetkiId" Type="Int32" />
                                            <asp:Parameter Name="original_AdiSoyadi" Type="String" />
                                            <asp:Parameter Name="original_KullaniciAdi" Type="String" />
                                            <asp:Parameter Name="original_Parola" Type="String" />
                                            <asp:Parameter Name="original_Eposta" Type="String" />
                                        </UpdateParameters>
                                    </asp:SqlDataSource>
                                    <asp:ValidationSummary ID="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" ValidationGroup="Insert" HeaderText="Kayıt eklerken şu hataları yaptınız:" DisplayMode="BulletList" EnableClientScript="true" runat="server" />
                                    <asp:ValidationSummary ID="ValidationSummary2" ShowMessageBox="true" ShowSummary="false" ValidationGroup="Edit" HeaderText="Kayıt güncellerken şu hataları yaptınız:" DisplayMode="BulletList" EnableClientScript="true" runat="server" />



                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="GridView1" />
                                      
                
          
                                </Triggers>
                            </asp:UpdatePanel>
                          



                        </div>

                    </section>
                </div>

            </div>

                                 
        </div>

    </div>
           
</asp:Content>
