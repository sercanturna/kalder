﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        { 
            if (Request.Cookies["UserInfo"] != null)
            {
                HttpCookie CportalUserCookie = new HttpCookie("UserInfo");
                CportalUserCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(CportalUserCookie);
            }
            else
            Session.Abandon();


            Response.Redirect("default.aspx");
        }
    }
}