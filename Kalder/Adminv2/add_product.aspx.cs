﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class add_product : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        int SonEklenenUrunId;
        string KatId = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetCat();
                GetBrand();
                GetCurrency();
            }
        }

        public void GetCat()
        {
            DataTable dt = db.GetDataTable("select * from UrunKategori where YayinDurumu=1");

            chkKat.DataSource = dt;
            chkKat.DataValueField = dt.Columns["UrunKatId"].ColumnName.ToString();
            chkKat.DataTextField = dt.Columns["KatAdi"].ColumnName.ToString();
            chkKat.DataBind();

            if (dt.Rows.Count <= 0)
                pnlAlert.Visible = true;

        }

        public void GetCurrency()
        {
            DataTable dt = db.GetDataTable("select * from Currency where IsActive=1");

            dropCurrency.DataSource = dt;
            dropCurrency.DataValueField = dt.Columns["CurrencyId"].ColumnName.ToString();
            dropCurrency.DataTextField = dt.Columns["Currency"].ColumnName.ToString();
            dropCurrency.DataBind();
        }

        public void GetBrand()
        {
            DataTable dt = db.GetDataTable("select * from Brands where Status=1");

            DropBrand.DataSource = dt;
            DropBrand.DataValueField = dt.Columns["BrandId"].ColumnName.ToString();
            DropBrand.DataTextField = dt.Columns["BrandName"].ColumnName.ToString();
            DropBrand.DataBind();
            DropBrand.Items.Insert(0, new ListItem("– Marka Seçiniz –", "0"));
        }


        protected void btn_UrunEkle_Click(object sender, EventArgs e)
        {



            #region Ürün Tablosu Veritabanı Insert



            SqlConnection baglanti = db.baglan();
            SqlCommand cmd = new SqlCommand("INSERT INTO Products(" +
            "BrandId, " +
            "ProductName, " +
            "ProductCode, " +
            "Description, " +
            "OrderNo, " +
            "IsActive, " +
            "IsPopuler, " +
            "IsNew, " +
            "SeoUrl, " +
            "Meta_Title, " +
            "Meta_Keywords, " +
            "Meta_Desc, " +
            "UnitPrice, " +
            "CurrencyId," +
            "Tax, " +
            "CreatedDate) values (" +
                "@BrandId, " +
                "@ProductName, " +
                "@ProductCode, " +
                "@Description, " +
                "@OrderNo, " +
                "@IsActive, " +
                "@IsPopuler, " +
                "@IsNew, " +
                "@SeoUrl, " +
                "@Meta_Title, " +
                "@Meta_Keywords, " +
                "@Meta_Desc, " +
                "@UnitPrice, " +
                "@CurrencyId," +
                "@Tax," +
                "@CreatedDate) SELECT SCOPE_IDENTITY() AS ProductId", baglanti);



            bool IsPopuler, IsNew;

            if (chkPopuler.Checked == true)
                IsPopuler = true;
            else
                IsPopuler = false;

            if (chkYeni.Checked == true)
                IsNew = true;
            else
                IsNew = false;


            SqlParameter insertPrm = new SqlParameter();
            insertPrm.Direction = ParameterDirection.Output;
            // ID değerini istediğimiz alan
            insertPrm.ParameterName = "ProductId";
            insertPrm.Size = 10;

            string seoURL = txtSeoUrl.Text;

            if (seoURL == "")
                seoURL = Ayarlar.UrlSeo(txtUrunAdi.Text);
            else
                seoURL = Ayarlar.UrlSeo(txtSeoUrl.Text);


            cmd.Parameters.Add(insertPrm);

            bool durum = false;
            if (drpDurum.SelectedValue != "NULL")
            {
                durum = true;
            }


            cmd.Parameters.AddWithValue("BrandId", DropBrand.SelectedValue);
            cmd.Parameters.AddWithValue("ProductName", txtUrunAdi.Text);
            cmd.Parameters.AddWithValue("ProductCode", txtUrunKodu.Text);
            cmd.Parameters.AddWithValue("Description", txtDetay.Text);
            cmd.Parameters.AddWithValue("OrderNo", txtSiraNo.Text);
            cmd.Parameters.AddWithValue("IsActive", durum);
            cmd.Parameters.AddWithValue("IsPopuler", IsPopuler);
            cmd.Parameters.AddWithValue("IsNew", IsNew);
            cmd.Parameters.AddWithValue("SeoUrl", seoURL);
            cmd.Parameters.AddWithValue("Meta_Title", txtTitle.Text);
            cmd.Parameters.AddWithValue("Meta_Keywords", tags.Text);
            cmd.Parameters.AddWithValue("Meta_Desc", txtDescription.Text);
            cmd.Parameters.AddWithValue("UnitPrice", Convert.ToDouble(txtFiyat.Text));
            cmd.Parameters.AddWithValue("Tax", dropTax.SelectedValue);
            cmd.Parameters.AddWithValue("CurrencyId", dropCurrency.SelectedValue);
            cmd.Parameters.AddWithValue("CreatedDate", DateTime.Now.ToString());


            int sonuc = int.Parse(cmd.ExecuteScalar().ToString());

            SonEklenenUrunId = sonuc;

            // int sonuc = cmd.ExecuteNonQuery();

            // SonEklenenUrunId = (int)insertPrm.Value;

            if (sonuc > 0)
            {
                // Ayarlar.Alert.result();
                Session["SonGirilenKatId"] = KatId;
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                //GetCat();
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);





            #endregion

            #region Kategori İlişki Tablosu Veritabanı
            ArrayList SecilenKatId = new ArrayList();


            foreach (ListItem secilenler in chkKat.Items)
            {
                //Ayarlar.Alert.Show(secilenler.Text + " - " + secilenler.Selected.ToString());

                if (secilenler.Selected == true)
                {
                    //SecilenKatId.Add((string)secilenler.Value);
                    SqlConnection Katbaglanti = db.baglan();
                    SqlCommand Katcmd = new SqlCommand("Insert into KatUrunIliski(" +
                    "UrunKatId, ProductId) values (" +
                    "@UrunKatId,@ProductId) SELECT SCOPE_IDENTITY() AS ProductId",
                    Katbaglanti);

                    Katcmd.Parameters.AddWithValue("@UrunKatId", (string)secilenler.Value);
                    Katcmd.Parameters.AddWithValue("@ProductId", SonEklenenUrunId);
                    Katcmd.ExecuteNonQuery();
                }
            }

            #endregion

            #region Resim Tablosu

            HttpFileCollection files = Request.Files;
            string resimadi = "resim_yok.jpg";
            string uzanti = "";
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                if (file.ContentLength > 0)
                {
                    //resimin adı
                    string fileName = Path.GetFileName(file.FileName);
                    //resim uzantısı
                    uzanti = Path.GetExtension(file.FileName);
                    //resime atanacak yeni ad
                    resimadi = Ayarlar.UrlSeo(txtUrunAdi.Text) + DateTime.Now.Millisecond + uzanti;
                    // Orjinal resmi kaydet
                    file.SaveAs(Server.MapPath("../upload/sahte" + resimadi));


                    //****************** Yeni boyutlara göre resim oluştur ***************/

                    // Orjinal resim
                    Bitmap resim = new Bitmap(Server.MapPath("../upload/sahte" + resimadi));
                    // 1920px genişlikte yeni resim oluştur


                    resim = Ayarlar.ResimBoyutlandir(resim, 800);
                    // oluşturulan resmi kaydet
                    resim.Save(Server.MapPath("../upload/urunler/" + resimadi), ImageFormat.Jpeg);

                    //****************** Veri tabanına kaydet ***************//




                    SqlConnection resimbaglanti = db.baglan();
                    SqlCommand resimcmd = new SqlCommand("Insert into ProductImages(" +
                    "ImageURL," +
                    "IsPrimary," +
                    "ProductId) values (" +
                    "@ImageURL," +
                    "@IsPrimary," +
                    "@ProductId)", resimbaglanti);

                    resimcmd.Parameters.AddWithValue("ImageURL", resimadi);
                    resimcmd.Parameters.AddWithValue("IsPrimary", "1");
                    resimcmd.Parameters.AddWithValue("ProductId", SonEklenenUrunId);

                    int resimsonuc = resimcmd.ExecuteNonQuery();

                    if (resimsonuc > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                        Response.Redirect("products.aspx");
                        //lblMesaj.Text += "Dosya : <b>" + fileName + "</b> başarıyla yüklendi !<br />";
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
                    }


                }
                // Orjinal resmi isterseniz bu kod satırıyla silebilirsiniz
                FileInfo temp = new FileInfo(Server.MapPath("../upload/sahte" + resimadi));
                temp.Delete();
            }

            #endregion

        }

        protected void dropKat_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btn_Temizle_Click(object sender, EventArgs e)
        {
            Server.Transfer("default.aspx");
        }
    }
}