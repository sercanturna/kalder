﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;

namespace Kalder.Adminv2
{
    public partial class news : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {

            ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            sm.RegisterAsyncPostBackControl(btnDeleteAll);

            //this.Page.Form.Enctype = "multipart/form-data";
            Page.Form.Attributes.Add("enctype", "multipart/form-data");


            if (!Page.IsPostBack)
            {
                getData();
            }

            

        }


        public void getData()
        {
            DataTable dt = db.GetDataTable("Select * from Haberler as h left join HaberKategori as hk on h.KategoriId=hk.KategoriId order by h.SiraNo asc");

            CollectionPager1.DataSource = dt.DefaultView;

            CollectionPager1.BindToControl = rpt_News;
            rpt_News.DataSource = CollectionPager1.DataSourcePaged;
            rpt_News.DataBind();
        }



        protected void btnDeleteAll_Onclick(object sender, EventArgs e)
        {
            foreach (RepeaterItem ri in rpt_News.Items)
            {
                CheckBox chk = (CheckBox)ri.FindControl("chkContainer");
                HiddenField hd = (HiddenField)ri.FindControl("hbItem");

                if (chk.Checked)
                {
                    db.guncelle("delete from Haberler where HaberId=" + hd.Value,up1);

                    DataTable dt = db.GetDataTable("Select * from HaberResimleri where HaberId=" + hd.Value);

                    foreach (DataRow dr in dt.Rows)
                    {
                        File.Delete(Server.MapPath("~/upload/HaberResimleri/FotoGaleri/big/" + dr["Resim"].ToString()));
                        File.Delete(Server.MapPath("~/upload/HaberResimleri/FotoGaleri/thumb/" + dr["Resim"].ToString()));
                        db.cmd("delete from HaberResimleri where HaberId=" + dr["HaberId"].ToString());
                    }
                   
                }

            }

           // Response.Redirect("news.aspx");
 getData();


        }

        protected void rpt_News_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string id = DataBinder.Eval(e.Item.DataItem, "HaberId").ToString();
                string imageURL = db.GetDataCell("Select Resim from HaberResimleri where IsPrimary=1 and HaberId=" + id);

                System.Web.UI.WebControls.Image img = (System.Web.UI.WebControls.Image)e.Item.FindControl("img");
                img.ImageUrl = Page.ResolveUrl("~/upload/HaberResimleri/FotoGaleri/thumb/") + imageURL;


                TextBox txtBox = e.Item.FindControl("txtSiraNo") as TextBox;
                txtBox.Text = DataBinder.Eval(e.Item.DataItem, "SiraNo").ToString();

            }

        }


        protected void rpt_News_ItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "updateRow")
            {
                string hId = e.CommandArgument.ToString();
                string rowNumber;


                TextBox txtBox = e.Item.FindControl("txtSiraNo") as TextBox;
                rowNumber = txtBox.Text;

                LinkButton button = e.CommandSource as LinkButton;

                ScriptManager sm = ScriptManager.GetCurrent(this.Page);
                sm.RegisterAsyncPostBackControl(button);

                string sorgu = "Update Haberler set SiraNo=" + rowNumber + " where HaberId=" + hId;

                int sonuc = db.guncelle(sorgu, upGrid);

              
                if (sonuc > 0)
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
                }

                upGrid.Update();
                getData();
            }

          
        }
    }
}