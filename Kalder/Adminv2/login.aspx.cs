﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class login : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string username, password, CompanyName;
        protected void Page_Load(object sender, EventArgs e)
        {
            //HttpCookie CportalUserCookie = Request.Cookies["UserInfo"];

            //if (CportalUserCookie != null)
            //{
            //    //DataRow drAdmin = db.GetDataRow("Select * from Admin where AdminId=" + CportalUserCookie["AdminId"]);
            //    //txtPassword.Text = drAdmin["Parola"].ToString();
            //    //txtUsername.Text = drAdmin["KullaniciAdi"].ToString();
            //   Response.Redirect("default.aspx");
            //     //returnToSender();
            //}


            if (!Page.IsPostBack)
            {
                try
                {
                    ViewState["ReferrerUrl"] = Request.UrlReferrer.ToString();
                }
                catch (Exception)
                {
                    //  throw;
                }


            }

            CompanyName = db.GetDataCell("Select FirmaUnvanKisa from GenelAyarlar where GenelAyarId=1");

            if (CompanyName == null || CompanyName == "")
                LtrlCompanyName.Text = "Cportal Admin";
            else
                LtrlCompanyName.Text = CompanyName + " Admin";

        }

        void returnToSender()
        {
            object referrer = ViewState["ReferrerUrl"];
            if (referrer != null)
                Response.Redirect((string)referrer);
            else
                Response.Redirect("default.aspx");
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            username = txtUsername.Text;
            password = Ayarlar.md5(txtPassword.Text);


            DataRow drAdminLogin = db.GetDataRow("select * from Admin where KullaniciAdi='" + Ayarlar.Temizle(username) + "' and Parola='" + Ayarlar.Temizle(password) + "' ");

            if (drAdminLogin != null)
            {
                if (chkRemember.Checked)
                {
                    HttpCookie CportalUserCookie; // Bir Cookie nesnesi oluşturuyorum.
                    CportalUserCookie = Request.Cookies["UserInfo"];
                    if (CportalUserCookie == null)
                    {
                            CportalUserCookie = new HttpCookie("UserInfo");
                            CportalUserCookie["AdminId"] = drAdminLogin["AdminId"].ToString(); //nesneme değer ataması yaptım.
                            CportalUserCookie["YetkiId"] = drAdminLogin["YetkiId"].ToString(); //nesneme değer ataması yaptım.
                            CportalUserCookie["AdiSoyadi"] = drAdminLogin["AdiSoyadi"].ToString(); //nesneme değer ataması yaptım.

                            CportalUserCookie.Expires = DateTime.Now.AddDays(30);
                        Response.Cookies.Add(CportalUserCookie);
                    }

                }
                else
                { 
                        Session["AdiSoyadi"] = drAdminLogin["AdiSoyadi"].ToString();
                        Session["AdminId"] = drAdminLogin["AdminId"].ToString();
                        Session["YetkiId"] = drAdminLogin["YetkiId"].ToString();
                }
                returnToSender();
            }
            else
            {
                ltrlAlert.Text = "<span class=\"color-orange-text\"><img src='./img/notifications/alert-32.png' /> * Kullanıcı Adı ve/ya Parolanızı hatalı girdiniz!</span>";
            }




        }
    }
}