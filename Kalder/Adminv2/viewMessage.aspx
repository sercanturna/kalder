﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="viewMessage.aspx.cs" Inherits="Kalder.Adminv2.viewMessage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
        <style type="text/css">
        body
        {
            font-family: Arial;
        }
        
        table.iletisim
        {
            text-align: left;
            margin-left: auto;
            margin-right: auto;
            width:500px;
            border:1px solid #ebebeb;
        }
        
        td.iletisim_etiket
        {
            padding: 10px 5px 10px 10px;
            
            color: #000;
            font-size: 10pt;
            font-weight: bold;
            width:200px;
            background-color:#ebebeb;
            border-bottom:1px solid #fff;
            }
        
        td.iletisim_deger
        {
            padding: 10px 50px 10px 10px;
            font-size: 11pt;
            
        }
        
        td.nokta
        {
            padding-left: 5px;
            padding-right: 5px;
            
        }
           
        h2.baslik
        {
        font-family: Arial, Helvetica, sans-serif;
	    font-weight: bold;
	    font-size: 13pt;
	    color: #073856;
	    padding:5px;
	    text-align:center
        }
        
         
        
        table a
        {
            /*text-decoration: none;
            color: #b31a1a;
            font-weight: bold;*/
        }
        
        table a:hover
        {
            text-decoration: underline;
        }

            .center {
            text-align:center; padding:10px;
            }
    </style>



</head>
<body>
    <form id="form1" runat="server">

     <div style="width:500px;">
     
         <h2 class="baslik">
             Kişi/Firma Bilgileri</h2>
    <table class="iletisim">
        <tr>
            <td class="iletisim_etiket">
                Adı Soyadı</td>
            <td class="nokta">
                :
            </td>
            <td class="iletisim_deger">
                <asp:Label ID="lblAdi_Soyadi" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="iletisim_etiket">
                Firma Adı</td>
            <td class="nokta">
                :</td>
            <td class="iletisim_deger">
                <asp:Label ID="lblFirmaAdi" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="iletisim_etiket">
                Telefon
                Numarası</td>
            <td class="nokta">
                :
            </td>
            <td class="iletisim_deger">
                <asp:Label ID="lblTelefon" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="iletisim_etiket">
                E-Posta Adresi
            </td>
            <td class="nokta">
                :
            </td>
            <td class="iletisim_deger">
                <asp:Label ID="lblEposta" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <h2 class="baslik">
                    Mesaj İçeriği</h2>
            </td>
        </tr>
        <tr>
            <td class="iletisim_etiket">
                Konu
            </td>
            <td class="nokta">
                :
            </td>
            <td class="iletisim_deger">
                <asp:Label ID="lblKonu" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="iletisim_etiket">
                Eklemek İstediği Konular
            </td>
            <td class="nokta">
                :
            </td>
            <td class="iletisim_deger">
                <asp:Label ID="lblEklemekIstedikleri" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="iletisim_etiket">
                Mesajın Gönderilme
                Tarihi
            </td>
            <td class="nokta">
                :
            </td>
            <td class="iletisim_deger">
                <asp:Label ID="lblTarih" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="center" colspan="4">
                <asp:Button ID="btnDurum" runat="server" Text="Okunmadı Olarak İşaretle" OnClick="btnDurum_Click"  class="btn btn-primary" Width="200" />
            </td>
        </tr>
    </table>
        </div>
    </form>

</body>
</html>