﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        DbConnection db = new DbConnection();
        string AdminId = "";
        int YetkiKontrol;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Userinfo Coockie'sinin olup olmadığını denetliyoruz.
            HttpCookie CportalUserCookie = Request.Cookies["UserInfo"];

            //Coockie veye Session'da AdminId nesnesini kontrol ediyoruz, yoksa giriş sayfasına yönlenecek.
            if (Session["AdminId"] != null || CportalUserCookie != null)
            {

                //Yetki kontrolünü hem session hemde cookie den aldığımız değerlerle kontrol ettiriyoruz.
                if (CportalUserCookie != null)
                {
                    YetkiKontrol = Convert.ToInt32(db.GetAdminYetki(CportalUserCookie["AdminId"]));
                }
                else
                    YetkiKontrol = Convert.ToInt32(db.GetAdminYetki(Session["AdminId"].ToString()));





                //Eğer Yetki düzeyi Üst Yönetici (1) ise Yöneticilerim menüsü görünür hale gelecek.
                if (YetkiKontrol == 1)
                {
                    ltrlAdminGroup.Text = "<a href=\"admin_group.aspx\">Yöneticilerim</a>";
                }



                //  string process = Request.QueryString["process"];

                // Admin Master'da duran üst alanları dolduruyorum.
                //ltrlAdminAdi.Text = db.GetDataCell("select AdiSoyadi from Adminler where AdminId='" + AdminId + "'");
                //ltrlSaat.Text = DateTime.Now.ToShortTimeString();
                //ltrlIP.Text = Request.ServerVariables["REMOTE_ADDR"];




                if (!Page.IsPostBack)
                {
                    if (CportalUserCookie != null)
                    {
                        AdminId = CportalUserCookie["AdminId"];
                    }
                    else
                        AdminId = Session["AdminId"].ToString();


                    LtrlUser.Text = db.GetDataCell("select AdiSoyadi from Admin where AdminId='" + AdminId + "'");

                    string CompanyName = db.GetDataCell("Select FirmaUnvanKisa from GenelAyarlar where GenelAyarId=1");

                    if (CompanyName == null || CompanyName == "")
                        LtrlCompanyName.Text = "Cportal Admin";
                    else
                        LtrlCompanyName.Text = CompanyName + " Admin";

                }

            }
            else
                Response.Redirect("login.aspx");


        }
    }
}