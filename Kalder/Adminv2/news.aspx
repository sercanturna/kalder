﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="news.aspx.cs" Inherits="Kalder.Adminv2.news" %> 

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    
    <%@ Register Assembly="CollectionPager" Namespace="SiteUtils" TagPrefix="cc1" %>

    <script type="text/javascript">
        $(function () {
            var $allCheckbox = $('.allCheckbox :checkbox');
            var $checkboxes = $('.singleCheckbox :checkbox');
            $allCheckbox.change(function () {
                if ($allCheckbox.is(':checked')) {
                    $checkboxes.attr('checked', 'checked');
                }
                else {
                    $checkboxes.removeAttr('checked');
                }
            });
            $checkboxes.change(function () {
                if ($checkboxes.not(':checked').length) {
                    $allCheckbox.removeAttr('checked');
                }
                else {
                    $allCheckbox.attr('checked', 'checked');
                }
            });
        });

        $("#haberler").addClass("active");

    </script>
    <style>
        th.allCheckbox {
            width: 20px;
        }

            td.singleCheckbox input, th.allCheckbox input {
                width: 20px !important;
            }
    </style>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>


    <div class="row">

        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-picture"></i>
                    <h3>Haberler</h3>
                    <asp:LinkButton ID="btnDeleteAll" OnClick="btnDeleteAll_Onclick" runat="server" CssClass="btn right" BorderStyle="Solid" BorderColor="#606060" BorderWidth="1" ToolTip="Sil" CausesValidation="False" Text="x Sil" OnClientClick="return confirm('Bu Haberi silmek istediğinize emin misiniz?');"></asp:LinkButton>
                    <a href="add_news.aspx" class="btn btn-success btn-primary right"> + Ekle </a>
                    
                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                    <div class="EU_TableScroll">
                        <asp:UpdatePanel ID="upGrid" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>

                                <table id="ProductCat" class="table table-bordered table-striped blank"  border="1" style="border-collapse: collapse;">
                                   <asp:Repeater ID="rpt_News" runat="server" OnItemDataBound="rpt_News_ItemDataBound" OnItemCommand="rpt_News_ItemCommand">
                                       <HeaderTemplate>
                                     <tr >
                                        <th scope="col" class="allCheckbox"><asp:CheckBox ID="allCheckbox1"  runat="server" /></th>
                                        <th scope="col">ID</th>
                                        <th scope="col">Kategori</th>
                                        <th scope="col">Başlık</th>
                                        <th scope="col">Resim</th>
                                        <th scope="col">Durum</th>
                                        <th scope="col">Manşet</th>
                                        <th scope="col">Sıra No</th>
                                        <th scope="col">Eklenme Tarihi</th>
                                        <th scope="col">İşlem</th>
                                    </tr>
                                    </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="singleCheckbox">
                                                    <asp:CheckBox ID="chkContainer" runat="server"  />
                                                    <asp:HiddenField ID="hbItem" runat="server" Value='<%# Eval("HaberId") %>' />
                                                </td>

                                                 <td><%#Eval ("HaberId") %></td>
                                                 <td><%#Eval ("KategoriAdi") %></td>
                                                 <td><%#Eval ("Baslik") %></td>
                                                <td><asp:Image ID="img" runat="server" Height="50" /></td>
                                                 <td class="center"><%# Eval("Durum") == "True" ? Eval("Durum") : Eval("Durum").ToString().Replace("True", "<img src=\"img/play.png\" />").Replace("False", "<img src=\"img/pause.png\" />")%></td>
                                                 <td class="center"><%# Eval("Manset") == "True" ? Eval("Manset") : Eval("Manset").ToString().Replace("True", "<img src=\"img/play.png\" />").Replace("False", "<img src=\"img/pause.png\" />")%></td>
                                                <td class="center"><asp:TextBox ID="txtSiraNo" runat="server" MaxLength="3" Width="30" style="width:30px !important; text-align:center"></asp:TextBox> </td>
                                                 <td><%#Eval ("EklenmeTarihi") %></td>
                                                 <td><a href="edit_news.aspx?hId=<%#Eval("HaberId") %>&kId=<%#Eval("KategoriId") %>" class="btn btn-small"><i class="btn-icon-only icon-pencil"></i></a>
                                                     <asp:LinkButton ID="lnkId" CommandName="updateRow" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "HaberId") %>' runat="server" CssClass="btn btn-small btn-warning"><i class="btn-icon-only icon-ok"></i></asp:LinkButton>
                                                     <%--<a href="news.aspx?hId=<%#Eval("HaberId") %>&kId=<%#Eval("KategoriId") %>&RowNumber=<%#Eval("SiraNo") %>" class="btn btn-small btn-warning"><i class="btn-icon-only icon-ok"></i></a>--%>
                                                 </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                  
                                    <!--
                                    <tr>
                                        <td colspan="9" style="text-align:center; padding:20px;" ><asp:Label CssClass="alert" ID="ltrlSonuc" runat="server" Text="Henüz bir ürün kategorisi oluşturulmamış."></asp:Label></td>
                                    </tr>
                                    -->
                                     
                                </table>
                              <section id="paginations">
                                <div class="sayfalama">
                                  <cc1:CollectionPager ID="CollectionPager1" runat="server" BackText=" « Önceki" 
                                    FirstText="İlk" LabelText="" LastText="Son" NextText="Sonraki »" 
                                    PageNumbersDisplay="Numbers" ResultsFormat="Sayfalar {0} {1} (Toplam:{2})"
                                    PageSize="10" SectionPadding="10" BackNextDisplay="Buttons" BackNextLocation="Split" 
                                    MaxPages="5000000" PageNumbersSeparator=""></cc1:CollectionPager>
                         </div>
                                  </section>
                                        </ContentTemplate>
                        </asp:UpdatePanel>

                        
                        
                    </div>
                </div>
            </div>
        </div>


    </div>
        </ContentTemplate>
    </asp:UpdatePanel>
        <asp:UpdateProgress AssociatedUpdatePanelID="up1" runat="server">
        <ProgressTemplate>
            <div class="progress_template">
                <img src="img/loader.gif" /><br /><h1 class="help-block">İŞLEM GERÇEKLEŞTİRİLİYOR!</h1>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>


