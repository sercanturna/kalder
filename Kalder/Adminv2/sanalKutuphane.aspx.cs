﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Configuration;
using System.Data;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data.SqlClient;

public partial class sanalkutuphane : System.Web.UI.Page
{
    // SqlConnection oldbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["Kalderconn"].ConnectionString);
    SqlConnection oldbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["Kalderconn"].ConnectionString);
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!Page.IsPostBack)
        {
            MultiView1.ActiveViewIndex = 2;
            SayfaKategori_listesi();
            DropDoldur();
        }
    }
    protected void DropDoldur()
    {
        

    }
    void SayfaKategori_listesi()
    {
        SqlDataAdapter oldbDa = new SqlDataAdapter("select * from SanalKutuphane order by KitapAdi", oldbConn);
        DataTable dt = new DataTable();
        oldbDa.Fill(dt);
        DataList1.DataSource = dt;
        DataList1.DataBind();
    }
    protected void btnKaydet_Click(object sender, EventArgs e)
    {
        SqlCommand oldbCmd = new SqlCommand("Insert into SanalKutuphane(RafNo, DolapNo, KitapAdi, Yazar, Yayinci, TelifYil, SayfaSayi, ISBNNo, Aciklama, AnahtarKelime,Resim) values (@RafNo, @DolapNo, @KitapAdi, @Yazar, @Yayinci, @TelifYil, @SayfaSayi, @ISBNNo, @Aciklama, @AnahtarKelime, @Resim)", oldbConn);
            oldbConn.Open();

            if (txtRafNo_E.Text != "")
                oldbCmd.Parameters.AddWithValue("@RafNo", txtRafNo_E.Text);
            else
                oldbCmd.Parameters.AddWithValue("@RafNo", DBNull.Value);

            if (txtDolapNo_E.Text != "")
                oldbCmd.Parameters.AddWithValue("@DolapNo", txtDolapNo_E.Text);
            else
                oldbCmd.Parameters.AddWithValue("@DolapNo", DBNull.Value);
            oldbCmd.Parameters.AddWithValue("@KitapAdi", txtKitapAdi_E.Text);
            oldbCmd.Parameters.AddWithValue("@Yazar", txtYazar_E.Text);
            oldbCmd.Parameters.AddWithValue("@Yayinci", txtYayinci_E.Text);
            if (txtTelif_E.Text != "")
                oldbCmd.Parameters.AddWithValue("@TelifYil", txtTelif_E.Text);
            else
                oldbCmd.Parameters.AddWithValue("@TelifYil", DBNull.Value);
            if (txtSayfa_E.Text != "")
                oldbCmd.Parameters.AddWithValue("@SayfaSayi", txtSayfa_E.Text);
            else
                oldbCmd.Parameters.AddWithValue("@SayfaSayi", DBNull.Value);
            oldbCmd.Parameters.AddWithValue("@ISBNNo", txtISBN_E.Text);
            oldbCmd.Parameters.AddWithValue("@Aciklama", FCKAciklama_E.Value);
            oldbCmd.Parameters.AddWithValue("@AnahtarKelime", txtAnahtarKelime_E.Text);
            string resimAd = "";
            if (fuResim_E.HasFile)
            {
                if (fuResim_E.PostedFile.ContentType == "image/jpg" ||
                        fuResim_E.PostedFile.ContentType == "image/jpeg" ||
                        fuResim_E.PostedFile.ContentType == "image/bmp" ||
                        fuResim_E.PostedFile.ContentType == "image/png" ||
                        fuResim_E.PostedFile.ContentType == "image/gif")
                {
                    string ext = System.IO.Path.GetExtension(fuResim_E.FileName).TrimStart(".".ToCharArray()).ToLower();
                    if ((ext != "jpeg") && (ext != "jpg") && (ext != "png") && (ext != "gif") && (ext != "bmp"))
                        return;


                    Bitmap uploadedImage = new Bitmap(fuResim_E.FileContent);

                    int maxWidth = 600;
                    int maxHeight = 800;

                    Bitmap resizedImage = GetScaledPicture(uploadedImage, maxWidth, maxHeight);

                    resimAd = ResimAdIsle.ResimAdOlustur(fuResim_E.PostedFile.FileName);
                    string fotoYolu = "/images/uploads/" + resimAd;
                    resizedImage.Save(Server.MapPath(fotoYolu), uploadedImage.RawFormat);
                }
            }
            oldbCmd.Parameters.AddWithValue("@Resim", resimAd);
           
           

            oldbCmd.ExecuteNonQuery();
            oldbConn.Close();
            MultiView1.ActiveViewIndex = 2;
            SayfaKategori_listesi();
            txtRafNo_E.Text = ""; txtDolapNo_E.Text = ""; txtKitapAdi_E.Text = ""; txtYazar_E.Text = ""; txtYayinci_E.Text = ""; txtTelif_E.Text = ""; txtSayfa_E.Text = ""; txtISBN_E.Text = ""; FCKAciklama_E.Value = ""; txtAnahtarKelime_E.Text = "";
        
  }     
    protected void btnGuncelle_Click(object sender, EventArgs e)
    {

        SqlCommand oldbCmd1 = new SqlCommand("Update SanalKutuphane set RafNo=@RafNo, DolapNo=@DolapNo, KitapAdi=@KitapAdi, Yazar=@Yazar, Yayinci=@Yayinci, TelifYil=@TelifYil, SayfaSayi=@SayfaSayi, ISBNNo=@ISBNNo, Aciklama=@Aciklama, AnahtarKelime=@AnahtarKelime, Resim=@Resim  where Id=@Id", oldbConn);
            oldbConn.Open();


            if (txtRafNo_G.Text != "")
                oldbCmd1.Parameters.AddWithValue("@RafNo", txtRafNo_G.Text);
            else
                oldbCmd1.Parameters.AddWithValue("@RafNo", DBNull.Value);
            if (txtDolapNo_G.Text != "")
                oldbCmd1.Parameters.AddWithValue("@DolapNo", txtDolapNo_G.Text);
            else
                oldbCmd1.Parameters.AddWithValue("@DolapNo", DBNull.Value);
            oldbCmd1.Parameters.AddWithValue("@KitapAdi", txtKitapAdi_G.Text);
            oldbCmd1.Parameters.AddWithValue("@Yazar", txtYazar_G.Text);
            oldbCmd1.Parameters.AddWithValue("@Yayinci", txtYayinci_G.Text);
            if (txtTelif_G.Text != "")
                oldbCmd1.Parameters.AddWithValue("@TelifYil", txtTelif_G.Text);
            else
                oldbCmd1.Parameters.AddWithValue("@TelifYil", DBNull.Value);
            if (txtSayfa_G.Text != "")
                oldbCmd1.Parameters.AddWithValue("@SayfaSayi", txtSayfa_G.Text);
            else
                oldbCmd1.Parameters.AddWithValue("@SayfaSayi", DBNull.Value);
            oldbCmd1.Parameters.AddWithValue("@ISBNNo", txtISBN_G.Text);
            oldbCmd1.Parameters.AddWithValue("@Aciklama", FCKAciklama_G.Value);
            oldbCmd1.Parameters.AddWithValue("@AnahtarKelime", txtAnahtar_G.Text);

            string resimAd = lblresimAd.Text;
            if (fuResim_G.HasFile)
            {
                if (fuResim_G.PostedFile.ContentType == "image/jpg" ||
                        fuResim_G.PostedFile.ContentType == "image/jpeg" ||
                        fuResim_G.PostedFile.ContentType == "image/bmp" ||
                        fuResim_G.PostedFile.ContentType == "image/png" ||
                        fuResim_G.PostedFile.ContentType == "image/gif")
                {
                    string ext = System.IO.Path.GetExtension(fuResim_G.FileName).TrimStart(".".ToCharArray()).ToLower();
                    if ((ext != "jpeg") && (ext != "jpg") && (ext != "png") && (ext != "gif") && (ext != "bmp"))
                        return;
                    Bitmap uploadedImage = new Bitmap(fuResim_G.FileContent);

                    int maxWidth = 600;
                    int maxHeight = 800;

                    Bitmap resizedImage = GetScaledPicture(uploadedImage, maxWidth, maxHeight);

                    resimAd = ResimAdIsle.ResimAdOlustur(fuResim_G.PostedFile.FileName);
                    string fotoYolu = "/images/uploads/" + resimAd;
                    resizedImage.Save(Server.MapPath(fotoYolu), uploadedImage.RawFormat);
                }
            }
            oldbCmd1.Parameters.AddWithValue("@Resim", resimAd);
         
           
           
            oldbCmd1.Parameters.AddWithValue("@Id", Session["id"]);
            oldbCmd1.ExecuteNonQuery();
            oldbConn.Close();
            MultiView1.ActiveViewIndex = 2;
            SayfaKategori_listesi();
            txtRafNo_G.Text = ""; txtDolapNo_G.Text = ""; txtKitapAdi_G.Text = ""; txtYazar_G.Text = ""; txtYayinci_G.Text = ""; txtTelif_G.Text = ""; txtSayfa_G.Text = ""; txtISBN_G.Text = ""; FCKAciklama_G.Value = ""; txtAnahtar_G.Text = "";
        
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        search();
        MultiView1.ActiveViewIndex = 3;
    }
    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "delete")
        {
            string id = DataList1.DataKeys[e.Item.ItemIndex].ToString();
            SqlCommand cmd = new SqlCommand("Delete from SanalKutuphane where Id=@Id", oldbConn);
            oldbConn.Open();
            cmd.Parameters.AddWithValue("@Id", id);
            cmd.ExecuteNonQuery();
            oldbConn.Close();
            
            SayfaKategori_listesi();
        }
        if (e.CommandName == "edit")
        {
            MultiView1.ActiveViewIndex = 1;
            //btnGuncelle.Visible = true;
            string id = DataList1.DataKeys[e.Item.ItemIndex].ToString();
            Session["id"] = id.ToString();
            SqlDataAdapter oldbDa = new SqlDataAdapter("Select * from SanalKutuphane where Id=@Id", oldbConn);
            oldbDa.SelectCommand.Parameters.AddWithValue("@Id", id);
            DataTable dt = new DataTable();
            oldbDa.Fill(dt);
            DataList1.DataSource = dt;
            DataList1.DataBind();

            txtRafNo_G.Text = dt.Rows[0]["RafNo"].ToString();
            txtDolapNo_G.Text = dt.Rows[0]["DolapNo"].ToString();
            txtKitapAdi_G.Text = dt.Rows[0]["KitapAdi"].ToString();
            txtYazar_G.Text = dt.Rows[0]["Yazar"].ToString();
            txtYayinci_G.Text = dt.Rows[0]["Yayinci"].ToString();
            txtTelif_G.Text = dt.Rows[0]["TelifYil"].ToString();
            txtSayfa_G.Text = dt.Rows[0]["SayfaSayi"].ToString();
            txtISBN_G.Text = dt.Rows[0]["ISBNNo"].ToString();
            FCKAciklama_G.Value = dt.Rows[0]["Aciklama"].ToString();
            txtAnahtar_G.Text = dt.Rows[0]["AnahtarKelime"].ToString();
            lblresimAd.Text = dt.Rows[0]["Resim"].ToString();
            if (lblresimAd.Text.Length > 1)
                imgResim_G.ImageUrl = "/images/uploads/" + lblresimAd.Text;
            else
                imgResim_G.Visible = false;

                   
            SayfaKategori_listesi();
        }
    }
    protected void DataList2_ItemCommand(object source, DataListCommandEventArgs e)
    {
        //if (e.CommandName == "delete")
        //{
        //    string id = DataList1.DataKeys[e.Item.ItemIndex].ToString();
        //    SqlDataAdapter sqlDa = new SqlDataAdapter("Delete from OrganizasyonYapisi where Hakkimizda_ID=@Hakkimizda_ID", oldbConn);
        //    sqlDa.SelectCommand.Parameters.AddWithValue("@Hakkimizda_ID", id);
        //    DataTable dt = new DataTable();
        //    sqlDa.Fill(dt);
        //    DataList1.DataSource = dt;
        //    DataList1.DataBind();
        //    SayfaKategori_listesi();
        //}
        //if (e.CommandName == "edit")
        //{
        //    MultiView1.ActiveViewIndex = 1;
        //    //btnGuncelle.Visible = true;
        //    string id = DataList1.DataKeys[e.Item.ItemIndex].ToString();
        //    Session["id"] = id.ToString();
        //    SqlDataAdapter oldbDa = new SqlDataAdapter("Select Hakkimizda_ID,Baslik,Kisa_Icerik,Aciklama,Resim,Dosya,Video,Sira_No from OrganizasyonYapisi" +
        //   " where Hakkimizda_ID=@Hakkimizda_ID", oldbConn);
        //    oldbDa.SelectCommand.Parameters.AddWithValue("@Hakkimizda_ID", id);
        //    DataTable dt = new DataTable();
        //    oldbDa.Fill(dt);
        //    DataList1.DataSource = dt;
        //    DataList1.DataBind();
        //    TextBox7.Text = dt.Rows[0][1].ToString();
        //    TextBox8.Text = dt.Rows[0][7].ToString();
        //    SayfaKategori_listesi();
        //}

    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 3;
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
    public void search()
    {
        //string Sorqu = "SELECT * FROM OrganizasyonYapisi WHERE Baslik like '%" + TextBox13.Text + "%'";
        //SqlDataAdapter adp = new SqlDataAdapter(Sorqu, oldbConn);
        //DataTable dt = new DataTable();
        //adp.Fill(dt);
        //DataList2.DataSource = dt;
        //DataList2.DataBind();
    }
    protected Bitmap GetScaledPicture(Bitmap source, int maxWidth, int maxHeight)
    {
        int width, height;
        float aspectRatio = (float)source.Width / (float)source.Height;

        if ((maxHeight > 0) && (maxWidth > 0))
        {
            if ((source.Width < maxWidth) && (source.Height < maxHeight))
            {
                //Return unchanged image
                return source;
            }
            else if (aspectRatio > 1)
            {
                // Calculated width and height,
                // and recalcuate if the height exceeds maxHeight
                width = maxWidth;
                height = (int)(width / aspectRatio);
                if (height > maxHeight)
                {
                    height = maxHeight;
                    width = (int)(height * aspectRatio);
                }
            }
            else
            {
                // Calculated width and height,
                // and recalcuate if the width exceeds maxWidth
                height = maxHeight;
                width = (int)(height * aspectRatio);
                if (width > maxWidth)
                {
                    width = maxWidth;
                    height = (int)(width / aspectRatio);
                }
            }
        }
        else if ((maxHeight == 0) && (source.Width > maxWidth))
        {
            // If MaxHeight is not provided (unlimited), and
            // the source width exceeds maxWidth,
            // then recalculate height
            width = maxWidth;
            height = (int)(width / aspectRatio);
        }
        else if ((maxWidth == 0) && (source.Height > maxHeight))
        {
            // If MaxWidth is not provided (unlimited), and the
            // source height exceeds maxHeight, then
            // recalculate width
            height = maxHeight;
            width = (int)(height * aspectRatio);
        }
        else
        {
            //Return unchanged image
            return source;
        }

        Bitmap newImage = GetResizedImage(source, width, height);
        return newImage;
    }
    protected Bitmap GetResizedImage(Bitmap source, int width, int height)
    {
        //This function creates the thumbnail image.
        //The logic is to create a blank image and to
        // draw the source image onto it

        Bitmap thumb = new Bitmap(width, height);
        Graphics gr = Graphics.FromImage(thumb);

        // gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
        gr.SmoothingMode = SmoothingMode.HighQuality;
        gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
        gr.CompositingQuality = CompositingQuality.HighQuality;


        gr.DrawImage(source, 0, 0, width, height);
        return thumb;
    }
}