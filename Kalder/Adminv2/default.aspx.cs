﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class _default : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        { 

            //DataTable dt = db.GetDataTable("Select * from Stats where Date like '%" + DropDownList1.SelectedItem.Text + "%'");
            //    Chart1.DataSource = dt;

            //    DataRow drTekil = db.GetDataRow("Select SUM(UniqeVisitors) as Tekil from Stats where Date like '%" + DropDownList1.SelectedItem.Text + "%'");
            //    DataRow drCogul = db.GetDataRow("Select SUM(TotalVisitors) as Cogul from Stats where Date like '%" + DropDownList1.SelectedItem.Text + "%'");
            //    DataRow drHit = db.GetDataRow("Select SUM(Hit) as Hit from Stats where Date like '%" + DropDownList1.SelectedItem.Text + "%'");

            //    int uniqe = (int)drTekil["Tekil"];
            //    int total = (int)drCogul["Cogul"];
            //    int hit = (int)drHit["Hit"];

            //    float yuzde = uniqe * 100 / total; 


            //    // set series members names for the X and Y values 
            //    Chart1.Series["Series1"].XValueMember = "Date";
            //    Chart1.Series["Series1"].YValueMembers = "TotalVisitors";

            //    Chart1.Series["Series2"].XValueMember = "Date";
            //    Chart1.Series["Series2"].YValueMembers = "UniqeVisitors";

            //   // Chart1.Series["Series2"].Color = System.Drawing.Color.Red;

            //    // data bind to the selected data source
            //    Chart1.DataBind();


            DataTable dtBrowser = db.GetDataTable("Select SUM(UniqeVisitors) as Tekil, Browser from Stats group by Browser; ");
            Chart1.DataSource = dtBrowser;

            Chart1.Series["Series1"].XValueMember = "Browser";
            Chart1.Series["Series1"].YValueMembers = "Tekil";

            Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;   
            Chart1.DataBind();


            DataTable dt = db.GetDataTable("Select top 3 HaberId, Baslik, Ozet, EklenmeTarihi from Haberler order by Hit");
            rpt_News.DataSource = dt;
            rpt_News.DataBind();

            DataTable dtmessage = db.GetDataTable("Select top 5 NameSurname, Status, CompanyName, Subject, IsRead, MessageId from ContactForm order by SentDate desc");
            rpt_Message.DataSource = dtmessage;
            rpt_Message.DataBind();

            try
            {
                DataRow drTekil = db.GetDataRow("Select SUM(UniqeVisitors) as Tekil from Stats");
                DataRow drCogul = db.GetDataRow("Select SUM(TotalVisitors) as Cogul from Stats");

                double tekil, cogul;
                tekil = Convert.ToDouble(drTekil["Tekil"]);
                cogul = Convert.ToDouble(drCogul["Cogul"]);

                ltrlOnline.Text = Application["OnlineUsers"].ToString();//Online ziyaretçi
                ltrlUniqeVisit.Text = tekil.ToString("N0");
                ltrlTotalVisit.Text = cogul.ToString("N0");



                string s = db.GetDataCell("Select SUM(AvarageTime) as OrtalamaSure from Stats");


                string toplamsure = ConvertToTimeFormat(int.Parse(s) / int.Parse(drTekil["Tekil"].ToString()));
                ltrlOrtalamaSure.Text = toplamsure;
            }
            catch (Exception)
            {
                 
            }

          
        }

        public string ConvertToTimeFormat(int seconds)
        {
            TimeSpan ts = new TimeSpan(0, 0, seconds);
            return ts.ToString(@"hh\:mm\:ss");
        }



                   


        protected void rpt_News_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Literal ltrlAy = (Literal)e.Item.FindControl("ltrlAy");
                Literal ltrlGun = (Literal)e.Item.FindControl("ltrlGun");

                DataRowView drv = e.Item.DataItem as DataRowView;
                string tarih = db.GetDataCell("select EklenmeTarihi from Haberler where HaberId=" + drv.Row["HaberId"].ToString());

                DateTime dt = Convert.ToDateTime(tarih);

                ltrlAy.Text = dt.ToString("MMM");
                ltrlGun.Text = dt.ToString("dd");

            }
        }
    

    }
}