﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class maillist : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        private bool _IsRenderForScreen = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.btnExportExel);


            if (!IsPostBack)
            {
                DataTable dt = db.GetDataTable("Select * from Maillist order by SentDate desc");

                //CollectionPager1.DataSource = dt.DefaultView;

                //CollectionPager1.BindToControl = rpt_maillist;
                //rpt_maillist.DataSource = CollectionPager1.DataSourcePaged;

                rpt_maillist.DataSource = dt.DefaultView;
                rpt_maillist.DataBind();
            }
        }

        protected void btnDeleteAll_Onclick(object sender, EventArgs e)
        {
            foreach (RepeaterItem ri in rpt_maillist.Items)
            {
                CheckBox chk = (CheckBox)ri.FindControl("chkContainer");
                HiddenField hd = (HiddenField)ri.FindControl("hbItem");

                if (chk.Checked)
                {
                    db.cmd("delete from Maillist where MaillistId=" + hd.Value);
                }

            }

            Response.Redirect("maillist.aspx");
        }


        protected void btnExportExel_Click(object sender, EventArgs e)
        {
            ExportToExcel();
        }


        protected void ExportToExcel()
        {
            //if (CollectionPager1.PageCount > 0)
            //{
            //    Ayarlar.Alert.Show("Sadece mevcut sayfa excel dosyasına aktarılır, Tüm kayıtları göndermek için Kayıt sayısını arttırmalısınız.");
            //}
            //else
            //{
            //    report();
            //}
        }

        public void report()
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=maillist.xls");
             Response.ContentEncoding = System.Text.Encoding.GetEncoding("ISO-8859-9");
            Response.Charset = "ISO-8859-9";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            rpt_maillist.RenderControl(hw);
            Response.Write("<table>");
            Response.Write(sw.ToString());
            Response.Write("</table>");
            Response.End();

        }


        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
               server control at run time. */
        }
    }
}