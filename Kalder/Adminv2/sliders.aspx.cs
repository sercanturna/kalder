﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;

namespace Kalder.Adminv2
{
    public partial class sliders : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
          
            if (!IsPostBack)
            {
                DataTable dt = db.GetDataTable("Select * from Sliders");

                CollectionPager1.DataSource = dt.DefaultView;

                CollectionPager1.BindToControl = rpt_Slider;
                rpt_Slider.DataSource = CollectionPager1.DataSourcePaged;
                rpt_Slider.DataBind();
            }
        }





        protected void btnDeleteAll_Onclick(object sender, EventArgs e)
        {
            foreach (RepeaterItem ri in rpt_Slider.Items)
            {
                CheckBox chk = (CheckBox)ri.FindControl("chkContainer");
                HiddenField hd = (HiddenField)ri.FindControl("hbItem");

                if (chk.Checked)
                {
                    string resimadi = db.GetDataCell("Select ImageURL from Sliders where SlideId=" + hd.Value);
                    FileInfo temp = new FileInfo(Server.MapPath("~/upload/SlaytResimleri/" + resimadi));
                    temp.Delete();

                    db.cmd("delete from Sliders where SlideId=" + hd.Value);

                }
            }

            Response.Redirect("sliders.aspx");



        }
    }
}