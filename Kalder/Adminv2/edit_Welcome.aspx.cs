﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class edit_Welcome : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        DataRow dr;

        protected void Page_Load(object sender, EventArgs e)
        {
            //ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            //sm.RegisterAsyncPostBackControl(btn_SayfaGuncelle);

            //Page.Form.Attributes.Add("enctype", "multipart/form-data");



            if (!IsPostBack)
            {
                dr = db.GetDataRow("Select * from ModuleContent where ModuleContentId=15");
                try
                {
                    ViewState["ReferrerUrl"] = Request.UrlReferrer.ToString();
                }
                catch (Exception)
                {
                }

                GetData();
            }

        }

        void returnToSender()
        {
            object referrer = ViewState["ReferrerUrl"];
            if (referrer != null)
                Response.Redirect((string)referrer);
            else
                Response.Redirect("edit_Welcome.aspx");
        }




        public void GetData()
        {
            dr = db.GetDataRow("Select * from ModuleContent where ModuleId=15");
            txtDetay.Text = dr["ModuleContent"].ToString();
            txtSeoUrl.Text = dr["ModuleLink"].ToString();
            txtBaslik.Text = dr["ModuleTitle"].ToString();

            if (dr["ModuleImage"] == null || dr["ModuleImage"].ToString() == "")
            {
                ltrlSil.Text = "null";
                imgResim.Visible = false;
                btnResimTemizle.Visible = false;
                ltrlSil.Visible = false;
            }
            else
            {
                imgResim.ImageUrl = "~/upload/" + dr["ModuleImage"].ToString();
            }


            //if (dr["ModuleImage"].ToString() != null || dr["ModuleImage"].ToString() != "")
            //    imgResim.ImageUrl = "~/upload/" + dr["ModuleImage"].ToString();
            //else
            //{
            //    ltrlSil.Text = "null";
            //    imgResim.Visible = false;
            //    btnResimTemizle.Visible = false;
            //    ltrlSil.Visible = false;
            //}


        }

        protected void btnResimTemizle_Click(object sender, ImageClickEventArgs e)
        {
            dr = db.GetDataRow("Select * from ModuleContent where ModuleId=15");

            if (dr["ModuleImage"] != null)
            {
                string ResimAdi = dr["ModuleImage"].ToString();

                File.Delete(Server.MapPath("~/upload/") + ResimAdi);
                int sonuc = db.cmd("Update ModuleContent set ModuleImage = CAST(NULL As nvarchar(100)) where ModuleId=15");

                if (sonuc > 0)
                {
                    GetData();
                }
                else
                {

                    Ayarlar.Alert.Show("Bir Hata Oluştu\nLütfen tekrar deneyin.");
                }
            }

            else
            {
                btnResimTemizle.Visible = false;
            }



        }

        protected void btn_SayfaGuncelle_Click(object sender, EventArgs e)
        {
            string resimadi = db.GetDataCell("select ModuleImage from ModuleContent where ModuleId=15");
            string uzanti = "";


            if (fuResim.HasFile)
            {

                FileInfo eskiResim = new FileInfo(Server.MapPath("~/upload/" + resimadi));
                if (eskiResim.Exists)
                    eskiResim.Delete();
               

                    uzanti = Path.GetExtension(fuResim.PostedFile.FileName);
                    resimadi = Ayarlar.ResimAdi(fuResim.FileName).Trim();
                    fuResim.SaveAs(Server.MapPath("~/upload/sahte" + resimadi));

                    Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte" + resimadi));

                    resim = Ayarlar.ResimBoyutlandir(resim, 292);
                    resim.Save(Server.MapPath("~/upload/" + resimadi), ImageFormat.Jpeg);

                    FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte" + resimadi));
                    temp.Delete();
                
            }

            SqlConnection conn = db.baglan();
            SqlCommand cmdUpdate = new SqlCommand("Update ModuleContent set " +

                "ModuleContent=@ModuleContent," +
                "ModuleLink=@ModuleLink," +
                "ModuleTitle=@ModuleTitle," +
                "ModuleImage=@ModuleImage" +
                " where ModuleId=15", conn);

            cmdUpdate.Parameters.AddWithValue("ModuleContent", txtDetay.Text);
            cmdUpdate.Parameters.AddWithValue("ModuleLink", txtSeoUrl.Text);
            cmdUpdate.Parameters.AddWithValue("ModuleTitle", txtBaslik.Text);
            cmdUpdate.Parameters.AddWithValue("ModuleImage", resimadi);

            int result = cmdUpdate.ExecuteNonQuery();

            if (result > 0)
            {
                // Ayarlar.Alert.result();
                Response.Redirect("Edit_Welcome.aspx?P=Success");
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                //  Ayarlar.Alert.Show("İşlem Başarı ile gerçekleşti");

            }
            else
                Response.Redirect("Edit_Welcome.aspx?P=Error");
            //ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);

        }
    }
}