﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Collections;

namespace Kalder.Adminv2
{
    public partial class product_categories : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();


        protected void Page_Load(object sender, EventArgs e)
        {
        

            if (!Page.IsPostBack)
            {
                DataTable dt = db.GetProductCat();
                rpt_ProductCat.DataSource = dt;
                rpt_ProductCat.DataBind();

            }

        }




        protected void rpt_ProductCat_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    DataRowView drv = e.Item.DataItem as DataRowView;


            //    PlaceHolder treeMenu = (PlaceHolder)e.Item.FindControl("treeMenu");
            //    treeMenu.Controls.Clear();
            //    DataTable dt = db.GetDataTable( ";with CTE As" +
            //                                    "(" +
            //                                    "	select UrunKatId,UstKatId,KatAdi,0 as [Level]," +
            //                                    "	Cast(UrunKatId as varchar(max)) As KatId,Cast(KatAdi as varchar(max)) as KategoriAdi" +
            //                                    "	from UrunKategori " +
            //                                    "	where UstKatId is null " +

            //                                    "	Union All" +

            //                                    "	select T.UrunKatId,T.UstKatId,T.KatAdi,[Level]+1," +
            //                                    "	KatId+'->'+Cast(T.UrunKatId as varchar(max)),KategoriAdi+'->'+Cast(T.KatAdi as varchar(max))" +
            //                                    "	from UrunKategori As T" +
            //                                    "	Inner Join CTE On T.UstKatId=CTE.UrunKatId " +
            //                                    ")" +
            //                                    "Select * from CTE where UrunKatId=" + drv.Row["UrunKatId"].ToString() +""
            //                                  );



            //    treeMenu.Controls.Add(new LiteralControl("<a href='#'>" + dt.Rows[0]["KategoriAdi"].ToString() + "</a>"));
            //}
        }



        protected void btnDeleteAll_Onclick(object sender, EventArgs e)
        {
            foreach (RepeaterItem ri in rpt_ProductCat.Items)
            {
                CheckBox chk = (CheckBox)ri.FindControl("chkContainer");
                HiddenField hd = (HiddenField)ri.FindControl("UrunKatId");

                if (chk.Checked)
                {
                    db.cmd("delete from UrunKategori where UstKatId=" + chk.Text + " or UrunKatId=" + chk.Text);
                }
            }

            Response.Redirect("product_categories.aspx");

        }
    }
}