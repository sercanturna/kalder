﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Kalder.Adminv2
{
    public partial class viewMessage : System.Web.UI.Page
    {
        string mId = "";


        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
           
            mId = Request.QueryString["mId"];

            db.cmd("update ContactForm set Status=1, IsRead=1 where MessageId=" + mId);

            if (Page.IsPostBack == false)
            {
                DataRow drForm = db.GetDataRow("Select * from ContactForm where MessageId=" + mId);

                string Email,Konu,Mesaj;

                Email=drForm["Email"].ToString();
                Konu =drForm["Subject"].ToString();
                Mesaj=drForm["Content"].ToString();

                lblAdi_Soyadi.Text = drForm["NameSurname"].ToString();
                lblFirmaAdi.Text = drForm["CompanyName"].ToString();
                lblTelefon.Text = drForm["PhoneNumber"].ToString();
                lblEposta.Text = "<a href=\"mailto:" + Email + "&amp;Subject=" + Konu + "&amp;body=" + Mesaj + "\">" + Email + "</a>";
                lblKonu.Text = drForm["Subject"].ToString();
                lblEklemekIstedikleri.Text = drForm["Content"].ToString();
                lblTarih.Text = drForm["SentDate"].ToString();
            }

        }
        protected void btnDurum_Click(object sender, EventArgs e)
        {
            db.cmd("update ContactForm set Status=0, IsRead=0 where MessageId=" + mId);
            Response.Redirect("Messages.aspx");
        }
    }
}