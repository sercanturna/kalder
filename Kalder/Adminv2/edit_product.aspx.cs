﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class edit_product : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        int SonEklenenUrunId;
        string KatId = "";
        string pId;
        DataRow dr;
        protected void Page_Load(object sender, EventArgs e)
        {
             
            ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            sm.RegisterAsyncPostBackControl(btn_UrunDuzenle);

            //this.Page.Form.Enctype = "multipart/form-data";
            Page.Form.Attributes.Add("enctype", "multipart/form-data");

            //ScriptManager sm = ScriptManager.GetCurrent(this.Page);
            //sm.RegisterAsyncPostBackControl(AjaxFileUpload1);

            //ScriptManager.RegisterStartupScript(this.Page, GetType(), "Javascript", "Success();", true);

           

            pId = RouteData.Values["pId"].ToString();


            GetImages();

            if (!IsPostBack)
            {
                
                

                GetCat();
                GetBrand();
                GetCurrency();
                GetImages();

                dr = db.GetDataRow("Select * from Products where ProductId=" + pId);


                //Böyle bir marka markalar tablosunda duruyormu diye kontrol et!
                string Brand = db.GetDataCell("Select * from Brands where BrandId=" + dr["BrandId"].ToString());

                if (Brand != null)
                {
                    DropBrand.Items.FindByValue(dr["BrandId"].ToString()).Selected = true;
                }
     

                txtUrunAdi.Text = dr["ProductName"].ToString();
                txtUrunKodu.Text = dr["ProductCode"].ToString();

                double price = Convert.ToDouble(dr["UnitPrice"]);

                txtFiyat.Text = price.ToString();
                dropCurrency.Items.FindByValue(dr["CurrencyId"].ToString()).Selected = true;
                dropTax.Items.FindByValue(dr["Tax"].ToString()).Selected = true;
                txtDetay.Text = dr["Description"].ToString();

                string durum = dr["IsActive"].ToString();
                if (durum == "True")
                    drpDurum.Items.FindByValue("1").Selected = true;
                else
                    drpDurum.Items.FindByValue("0").Selected = true;

                txtSiraNo.Text = dr["OrderNo"].ToString();



                chkPopuler.Checked = Convert.ToBoolean(dr["IsPopuler"].ToString());
                chkYeni.Checked = Convert.ToBoolean(dr["IsNew"].ToString());

                txtDescription.Text = dr["Meta_Desc"].ToString();
                tags.Text = dr["Meta_Keywords"].ToString();
                txtTitle.Text = dr["Meta_Title"].ToString();
                txtSeoUrl.Text = dr["SeoUrl"].ToString();
            }





        }


        public void GetImages()
        {
            DataTable dt = db.GetDataTable("select * from ProductImages where ProductId=" + pId);
            rptResimler.DataSource = dt;
            rptResimler.DataBind();
        }




        protected void chkKat_DataBound(object sender, EventArgs e)
        {

            DataSet ds = db.GetDataSet("select UrunKatId from KatUrunIliski where ProductId=" + pId);

            List<string> strDetailIDList = new List<string>();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                strDetailIDList.Add(row["UrunKatId"].ToString());
            }

            string[] strDetailID = strDetailIDList.ToArray();


            StringBuilder builder = new StringBuilder();
            foreach (string value in strDetailID)
            {
                try
                {
                    chkKat.Items.FindByValue(value).Selected = true;
                }
                catch (Exception)
                {

                }

            }

        }

        public void GetCat()
        {
            DataTable dt = db.GetDataTable("select * from UrunKategori where YayinDurumu=1");
            chkKat.DataSource = dt;
            chkKat.DataValueField = dt.Columns["UrunKatId"].ColumnName.ToString();
            chkKat.DataTextField = dt.Columns["KatAdi"].ColumnName.ToString();
            chkKat.DataBind();

        }

        public void GetCurrency()
        {
            DataTable dt = db.GetDataTable("select * from Currency where IsActive=1");

            dropCurrency.DataSource = dt;
            dropCurrency.DataValueField = dt.Columns["CurrencyId"].ColumnName.ToString();
            dropCurrency.DataTextField = dt.Columns["Currency"].ColumnName.ToString();
            dropCurrency.DataBind();
        }

        public void GetBrand()
        {
            DataTable dt = db.GetDataTable("select * from Brands where Status=1");

            DropBrand.DataSource = dt;
            DropBrand.DataValueField = dt.Columns["BrandId"].ColumnName.ToString();
            DropBrand.DataTextField = dt.Columns["BrandName"].ColumnName.ToString();
            DropBrand.DataBind();
            DropBrand.Items.Insert(0, new ListItem("– Marka Seçiniz –", ""));
        }



        protected void rptResimler_OnItemCommand(object sender, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "setPrimary")
            {
                LinkButton button = e.CommandSource as LinkButton;

                //UpdatePanel upLinks = (UpdatePanel)e.Item.FindControl("uplnkUpdate");

                ScriptManager sm = ScriptManager.GetCurrent(this.Page);
                sm.RegisterAsyncPostBackControl(button);

                db.guncelle("SET language turkish; Update ProductImages set IsPrimary=0 where ProductId=" + pId, upResim);
                db.guncelle("SET language turkish; Update ProductImages set IsPrimary=1 where ImgId=" + e.CommandArgument.ToString() + " and ProductId=" + pId, upResim);


                upResim.Update();
                //upLinks.Update();


                //GetImages();

                //upResim.Update();
            }


            if (e.CommandName == "del")
            {
                LinkButton button = e.CommandSource as LinkButton;

                // UpdatePanel upLinks = (UpdatePanel)e.Item.FindControl("uplnkUpdate");

                ScriptManager sm = ScriptManager.GetCurrent(this.Page);
                sm.RegisterAsyncPostBackControl(button);

                string ResimAdi = db.GetDataCell("Select ImageURL from ProductImages where ImgId=" + e.CommandArgument.ToString());
                FileInfo temp = new FileInfo(Server.MapPath("../upload/urunler/" + ResimAdi));
                temp.Delete();
                int sonuc = db.cmd("Delete from ProductImages where ImgId=" + e.CommandArgument.ToString());

                if (sonuc > 0)
                {

                    ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                    upResim.Update();
                    //upLinks.Update();
                    GetImages();
                }
                else
                {

                    ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
                    upResim.Update();
                    //upLinks.Update();
                    GetImages();
                }



            }
        }

        protected void btn_UrunDuzenle_Click(object sender, EventArgs e)
        {
            
            // Önce kaç adet seçilen kategori var onu tespit ediyoruz.
            int numSelected = 0;
            foreach (ListItem li in chkKat.Items)
            {
                if (li.Selected)
                {
                    numSelected = numSelected + 1;
                }
            }

            #region Kategori İlişki Tablosu Veritabanı
            if (numSelected > 0)
            {
                ArrayList SecilenKatId = new ArrayList();
                //Önce tüm kategori ilişkilerini temizliyoruz, yeniden insert yaptıracağız.
                db.cmd("delete from KatUrunIliski where ProductId=" + pId);


                foreach (ListItem secilenler in chkKat.Items)
                {
                    if (secilenler.Selected == true)
                    {
                        SqlConnection Katbaglanti = db.baglan();
                        SqlCommand Katcmd = new SqlCommand("SET language turkish; Insert into KatUrunIliski(" +
                        "UrunKatId, ProductId) values (" +
                        "@UrunKatId,@ProductId)",
                        Katbaglanti);

                        Katcmd.Parameters.AddWithValue("@UrunKatId", (string)secilenler.Value);
                        Katcmd.Parameters.AddWithValue("@ProductId", pId);
                        Katcmd.ExecuteNonQuery();
                    }
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(typeof(Page), "ScriptDescription", "<script type=\"text/javascript\"> alert('Lütfen en az 1(bir) kategori seçin!');</script>");
            }
            #endregion

            #region Ürün Tablosu Veritabanı Insert

            SqlConnection baglanti = db.baglan();
            SqlCommand cmd = new SqlCommand("SET language turkish; Update Products set " +
            "BrandId=@BrandId, " +
            "ProductName=@ProductName, " +
            "ProductCode=@ProductCode, " +
            "Description=@Description, " +
            "OrderNo=@OrderNo, " +
            "IsActive=@IsActive, " +
            "IsPopuler=@IsPopuler, " +
            "IsNew=@IsNew, " +
            "SeoUrl=@SeoUrl, " +
            "Meta_Title=@Meta_Title, " +
            "Meta_Keywords=@Meta_Keywords, " +
            "Meta_Desc=@Meta_Desc, " +
            "UnitPrice=@UnitPrice, " +
            "CurrencyId=@CurrencyId," +
            "Tax=@Tax," +
            "CreatedDate=@CreatedDate" +
            " where ProductId=" + pId, baglanti);

            bool IsPopuler, IsNew;

            if (chkPopuler.Checked == true)
                IsPopuler = true;
            else
                IsPopuler = false;

            if (chkYeni.Checked == true)
                IsNew = true;
            else
                IsNew = false;


            SqlParameter insertPrm = new SqlParameter();
            insertPrm.Direction = ParameterDirection.Output;
            // ID değerini istediğimiz alan
            insertPrm.ParameterName = "ProductId";
            insertPrm.Size = 10;


            cmd.Parameters.Add(insertPrm);

            cmd.Parameters.AddWithValue("BrandId", DropBrand.SelectedValue);
            cmd.Parameters.AddWithValue("ProductName", txtUrunAdi.Text);
            cmd.Parameters.AddWithValue("ProductCode", txtUrunKodu.Text);
            cmd.Parameters.AddWithValue("Description", txtDetay.Text);
            cmd.Parameters.AddWithValue("OrderNo", txtSiraNo.Text);
            cmd.Parameters.AddWithValue("IsActive", drpDurum.SelectedValue);
            cmd.Parameters.AddWithValue("IsPopuler", IsPopuler);
            cmd.Parameters.AddWithValue("IsNew", IsNew);
            cmd.Parameters.AddWithValue("SeoUrl", txtSeoUrl.Text);
            cmd.Parameters.AddWithValue("Meta_Title", txtTitle.Text);
            cmd.Parameters.AddWithValue("Meta_Keywords", tags.Text);
            cmd.Parameters.AddWithValue("Meta_Desc", txtDescription.Text);
            cmd.Parameters.AddWithValue("UnitPrice", Convert.ToDouble(txtFiyat.Text));
            cmd.Parameters.AddWithValue("Tax", dropTax.SelectedValue);
            cmd.Parameters.AddWithValue("CreatedDate", DateTime.Now.ToString());
            cmd.Parameters.AddWithValue("CurrencyId", dropCurrency.SelectedValue);





            int sonuc = cmd.ExecuteNonQuery();



            if (sonuc > 0)
            {
                // Ayarlar.Alert.result();
                Session["SonGirilenKatId"] = KatId;
                //ClientScript.RegisterStartupScript(typeof(Page), "ScriptDescription", "<script type=\"text/javascript\"> Javascript:Success();</script>");
                // ClientScript.RegisterStartupScript(typeof(Page), "ScriptDescription", "<script type=\"text/javascript\"> alert('Lütfen en az 1(bir) kategori seçin!');</script>");
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                GetCat();
                 // Response.Redirect("products.aspx");
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);





            #endregion

        }

        protected void dropKat_SelectedIndexChanged(object sender, EventArgs e)
        {

        }



        protected void btnUploadImage_Click(object sender, EventArgs e)
        {
            //#region Resim Tablosu

            //HttpFileCollection files = Request.Files;
            //string resimadi = "resim_yok.jpg";
            //string uzanti = "";
            //for (int i = 0; i < files.Count; i++)
            //{
            //    HttpPostedFile file = files[i];
            //    if (file.ContentLength > 0)
            //    {
            //        //resimin adı
            //        string fileName = Path.GetFileName(file.FileName);
            //        //resim uzantısı
            //        uzanti = Path.GetExtension(file.FileName);
            //        //resime atanacak yeni ad
            //        resimadi = Ayarlar.UrlSeo(txtUrunAdi.Text) + DateTime.Now.Millisecond + uzanti;
            //        // Orjinal resmi kaydet
            //        file.SaveAs(Server.MapPath("../upload/sahte" + resimadi));


            //        //****************** Yeni boyutlara göre resim oluştur ***************/

            //        // Orjinal resim
            //        Bitmap resim = new Bitmap(Server.MapPath("../upload/sahte" + resimadi));
            //        // 1920px genişlikte yeni resim oluştur


            //        resim = Ayarlar.ResimBoyutlandir(resim, 800);
            //        // oluşturulan resmi kaydet
            //        resim.Save(Server.MapPath("../upload/urunler/" + resimadi), ImageFormat.Jpeg);

            //        //****************** Veri tabanına kaydet ***************//




            //        SqlConnection resimbaglanti = db.baglan();
            //        SqlCommand resimcmd = new SqlCommand("SET language turkish; Insert into ProductImages(" +
            //        "ImageURL," +
            //        "ProductId) values (" +
            //        "@ImageURL," +
            //        "@ProductId)", resimbaglanti);

            //        resimcmd.Parameters.AddWithValue("ImageURL", resimadi);
            //        resimcmd.Parameters.AddWithValue("ProductId", pId);

            //        int resimsonuc = resimcmd.ExecuteNonQuery();

            //        if (resimsonuc > 0)
            //        {
            //            GetImages();
            //            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
            //            // Response.Redirect("urun_ekle.aspx?islemsonuc=1");
            //            //lblMesaj.Text += "Dosya : <b>" + fileName + "</b> başarıyla yüklendi !<br />";
            //        }
            //        else
            //        {
            //            ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);
            //        }


            //    }
            //    // Orjinal resmi isterseniz bu kod satırıyla silebilirsiniz
            //    FileInfo temp = new FileInfo(Server.MapPath("../upload/sahte" + resimadi));
            //    temp.Delete();
            //}


            //#endregion
        }




        //protected void UploadComplete(object sender, AjaxControlToolkit.AjaxFileUploadEventArgs e)
        //{

        //    //string filePath = Server.MapPath("../upload/urunler/" + e.FileName);
        //    //AjaxFileUpload1.SaveAs(filePath);

        //    //ScriptManager act = ScriptManager.GetCurrent(this.Page);
        //    //act.RegisterAsyncPostBackControl(this.AjaxFileUpload1);

        //    string resimadi = e.FileName;
        //    string uzanti = Path.GetExtension(e.FileName);



        //    //resime atanacak yeni ad
        //    resimadi = Ayarlar.UrlSeo(txtUrunAdi.Text) + DateTime.Now.Millisecond + uzanti;

        //            // Orjinal resmi kaydet
        //            //AjaxFileUpload1.SaveAs(Server.MapPath("../upload/sahte/" + resimadi));


        //            //****************** Yeni boyutlara göre resim oluştur ***************/

        //            // Orjinal resim
        //            Bitmap resim = new Bitmap(Server.MapPath("../upload/sahte/" + resimadi));
        //            // 1920px genişlikte yeni resim oluştur


        //            resim = Ayarlar.ResimBoyutlandir(resim, 800);
        //            // oluşturulan resmi kaydet
        //            resim.Save(Server.MapPath("../upload/urunler/" + resimadi), ImageFormat.Jpeg);

        //            //****************** Veri tabanına kaydet ***************//




        //            SqlConnection resimbaglanti = db.baglan();
        //            SqlCommand resimcmd = new SqlCommand("SET language turkish; Insert into ProductImages(" +
        //            "ImageURL," +
        //            "ProductId) values (" +
        //            "@ImageURL," +
        //            "@ProductId)", resimbaglanti);

        //            resimcmd.Parameters.AddWithValue("ImageURL", resimadi);
        //            resimcmd.Parameters.AddWithValue("ProductId", pId);

        //            int resimsonuc = resimcmd.ExecuteNonQuery();

        //            if (resimsonuc > 0)
        //            {

        //                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);



        //                upResim.Update();
        //            }
        //            else
        //            {

        //                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);

        //                upResim.Update();
        //            }




        //        // Orjinal resmi isterseniz bu kod satırıyla silebilirsiniz
        //        FileInfo temp = new FileInfo(Server.MapPath("../upload/sahte/" + resimadi));
        //        temp.Delete();

        //        e.DeleteTemporaryData();




        //}



        //protected void AjaxFileUpload1_PreRender(object sender, EventArgs e)
        //{
        //    GetImages();
        //    upResim.Update();
        //}


        public void Yukle()
        {
            HttpFileCollection files = Request.Files;
            //string resimadi = "resim_yok.jpg";
            //string uzanti = "";
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                string fname = Server.MapPath("~/upload/" + file.FileName);
                file.SaveAs(fname);
            }
        }



       
    }
}