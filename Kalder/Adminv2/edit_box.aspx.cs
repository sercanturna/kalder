﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class edit_box : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string bId;
        string resimadi;
        protected void Page_Load(object sender, EventArgs e)
        {
           

            bId = Request.QueryString["bId"];

            if (!IsPostBack)
            {
                DataRow dr = db.GetDataRow("select * from Boxes where BoxId=" + bId);
                txtLink.Text = dr["Link"].ToString();
                txtSiraNo.Text = dr["OrderNo"].ToString();
                txtTitle.Text = dr["Title"].ToString();
                txtIcerik.Text = dr["Content"].ToString();

                dropTarget.Items.FindByValue(dr["LinkTarget"].ToString()).Selected = true;

                string durum = dr["IsActive"].ToString();
                if (durum == "True")
                    drpDurum.Items.FindByValue("1").Selected = true;
                else
                    drpDurum.Items.FindByValue("0").Selected = true;


                imgURL.ImageUrl = "../upload/Box/" + dr["ImageURL"].ToString();

            }
        }




        protected void btn_SlaytGuncelle_Click(object sender, EventArgs e)
        {

            string uzanti = "";
            resimadi = db.GetDataCell("Select ImageURL from Boxes where BoxId=" + bId);

            if (fuResim.HasFile)
            {
                uzanti = Path.GetExtension(fuResim.PostedFile.FileName);
                resimadi = Ayarlar.OzetCek(Ayarlar.UrlSeo(txtTitle.Text), 10).Trim() + DateTime.Now.Day + uzanti;
                fuResim.SaveAs(Server.MapPath("~/upload/sahte/" + resimadi));

                Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + resimadi));
                resim = Ayarlar.ResimBoyutlandir(resim, 150);

                resim.Save(Server.MapPath("~/upload/Box/" + resimadi), ImageFormat.Jpeg);
                // Resmi önce SlaytResimleri klasörüne kayıt ediyoruz.

                //resim = Ayarlar.ResimBoyutlandir(resim, 220);
                //resim.Save(Server.MapPath("~/upload/SlaytResimleri/220/" + resimadi), ImageFormat.Jpeg);
                //// Resmi sonrada 220 klasörüne kayıt ediyoruz.

                FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + resimadi));
                temp.Delete();
            }


            SqlConnection baglanti = db.baglan();
            SqlCommand cmdGuncelle = new SqlCommand("SET language turkish; Update Boxes set ImageURL=@ImageURL,Title=@Title,Content=@Content,IsActive=@IsActive,OrderNo=@OrderNo," +
            "Link=@Link,LinkTarget=@LinkTarget," +
           "Date=@Date Where BoxId=" + bId, baglanti);

            cmdGuncelle.Parameters.AddWithValue("ImageURL", resimadi);
            cmdGuncelle.Parameters.AddWithValue("Title", txtTitle.Text);
            cmdGuncelle.Parameters.AddWithValue("Content", txtIcerik.Text);
            cmdGuncelle.Parameters.AddWithValue("IsActive", drpDurum.SelectedValue);
            cmdGuncelle.Parameters.AddWithValue("OrderNo", txtSiraNo.Text);
            cmdGuncelle.Parameters.AddWithValue("Link", txtLink.Text);
            cmdGuncelle.Parameters.AddWithValue("LinkTarget", dropTarget.SelectedValue);
            cmdGuncelle.Parameters.AddWithValue("Date", DateTime.Now.ToString());


            int sonuc = cmdGuncelle.ExecuteNonQuery();

            if (sonuc > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                Response.Redirect("Boxes.aspx");
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);

        }
    }
}