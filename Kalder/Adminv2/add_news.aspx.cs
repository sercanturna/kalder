﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class add_news : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string kId = Request.QueryString["kId"];
                GetCat(kId);
            }
        }

        public void GetCat(string kId)
        {
            DataTable dtKat = db.GetDataTable("select * from HaberKategori");
            if (kId != null)
                dropKat.SelectedValue = kId;

            dropKat.DataSource = dtKat;
            dropKat.DataValueField = dtKat.Columns["KategoriId"].ColumnName.ToString();
            dropKat.DataTextField = dtKat.Columns["KategoriAdi"].ColumnName.ToString();
            dropKat.DataBind();
            dropKat.Items.Insert(0, new ListItem("– Kategori Seçiniz –", "0"));

            if (dtKat.Rows.Count <= 0)
                pnlAlert.Visible = true;
        }


        protected void btn_HaberEkle_Click(object sender, EventArgs e)
        {
            string resimadi = "resim_yok.jpg";
            string uzanti = "";

            //if (fuResim.HasFile)
            //{
            //    uzanti = Path.GetExtension(fuResim.PostedFile.FileName);
            //    resimadi = Ayarlar.ResimAdi(DateTime.Now.Millisecond + uzanti);
            //    //resimadi = Ayarlar.OzetCek(Ayarlar.ResimAdi(txtBaslik.Text), 10).Trim() + "_" + DateTime.Now.Millisecond + uzanti;
            //    //fuResim.SaveAs(Server.MapPath("~/upload/sahte/" + resimadi));

            //    //Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + resimadi));
            //    //resim = Ayarlar.ResimBoyutlandir(resim, 420);

            //    //resim.Save(Server.MapPath("~/upload/HaberResimleri/FotoGaleri/big/" + resimadi), ImageFormat.Jpeg);
            //    //// Resmi önce 280 klasörüne kayıt ediyoruz.

            //    ////resim = Ayarlar.ResimBoyutlandir(resim, 280);
            //    ////resim.Save(Server.MapPath("~/upload/HaberResimleri/280/" + resimadi), ImageFormat.Jpeg);
            //    //// Resmi sonrada 220 klasörüne kayıt ediyoruz.

            //    //resim = Ayarlar.ResimBoyutlandir(resim, 220);
            //    //resim.Save(Server.MapPath("~/upload/HaberResimleri/FotoGaleri/thumb/" + resimadi), ImageFormat.Jpeg);
            //    //// Resmi sonrada 220 klasörüne kayıt ediyoruz.

            //    //FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + resimadi));
            //    //temp.Delete();
            //}

            SqlConnection baglanti = db.baglan();
            SqlCommand cmdKaydet = new SqlCommand("SET language turkish; insert into Haberler(" +
            "KategoriId," +
            "Baslik," +
            "Ozet," +
            "Detay," +
            "Resim," +
            "Video," +
            "Durum," +
            "Manset," +
            "Hit," +
            "EklenmeTarihi," +
            "SiraNo) values (" +
            "@KategoriId," +
            "@Baslik," +
            "@Ozet," +
            "@Detay," +
            "@Resim," +
            "@Video," +
            "@Durum," +
            "@Manset," +
            "@Hit," +
            "@EklenmeTarihi," +
            "@SiraNo) Select Scope_Identity() AS HaberId",
            baglanti);

            cmdKaydet.Parameters.AddWithValue("KategoriId", dropKat.SelectedValue);
            cmdKaydet.Parameters.AddWithValue("Baslik", txtBaslik.Text);
            cmdKaydet.Parameters.AddWithValue("Ozet", txtOzet.Text);
            cmdKaydet.Parameters.AddWithValue("Detay", txtDetay.Text);
            cmdKaydet.Parameters.AddWithValue("Resim", resimadi);
            cmdKaydet.Parameters.AddWithValue("Video", txtVideo.Text);
            cmdKaydet.Parameters.AddWithValue("Durum", drpDurum.SelectedValue);
            cmdKaydet.Parameters.AddWithValue("Manset", dropManset.SelectedValue);
            cmdKaydet.Parameters.AddWithValue("Hit", "1");
            cmdKaydet.Parameters.AddWithValue("EklenmeTarihi", DateTime.Now.ToString());
            cmdKaydet.Parameters.AddWithValue("SiraNo", txtSiraNo.Text);

            SqlParameter insertPrm = new SqlParameter();
            insertPrm.Direction = ParameterDirection.Output;
            // ID değerini istediğimiz alan
            insertPrm.ParameterName = "HaberId";
            insertPrm.Size = 10;

            cmdKaydet.Parameters.Add(insertPrm);

            int sonuc = int.Parse(cmdKaydet.ExecuteScalar().ToString());
            int SonEklenenHaberId = sonuc;

            if (sonuc > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);

            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);



            #region Resim Tablosu

            HttpFileCollection files = Request.Files;

            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFile file = files[i];
                if (file.ContentLength > 0)
                {
                    //resimin adı
                    string fileName = Path.GetFileName(file.FileName);
                    //resim uzantısı
                    uzanti = Path.GetExtension(file.FileName);
                    //resime atanacak yeni ad
                    resimadi = Ayarlar.ResimAdi(DateTime.Now.Millisecond + fileName);
                    // Orjinal resmi kaydet
                    file.SaveAs(Server.MapPath("~/upload/sahte" + resimadi));


                    //****************** Yeni boyutlara göre resim oluştur ***************/

                    // Orjinal resim
                    Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte" + resimadi));
                    // 1920px genişlikte yeni resim oluştur


                    resim = Ayarlar.ResimBoyutlandir(resim, 800);
                    // oluşturulan resmin büyük halini kaydet
                    resim.Save(Server.MapPath("~/upload/HaberResimleri/FotoGaleri/big/" + resimadi), ImageFormat.Jpeg);

                    resim = Ayarlar.ResimBoyutlandir(resim, 283);
                    // oluşturulan resmin küçük halini kaydet
                    resim.Save(Server.MapPath("~/upload/HaberResimleri/FotoGaleri/thumb/" + resimadi), ImageFormat.Jpeg);



                    //****************** Veri tabanına kaydet ***************//

                    SqlConnection resimbaglanti = db.baglan();
                    SqlCommand resimcmd = new SqlCommand("SET language turkish; Insert into HaberResimleri(" +
                    "Resim,HaberId,IsPrimary) values (" +
                    "@Resim,@HaberId,@IsPrimary)", resimbaglanti);

                    resimcmd.Parameters.AddWithValue("Resim", resimadi);
                    resimcmd.Parameters.AddWithValue("HaberId", SonEklenenHaberId);
                    if(i==0)
                        resimcmd.Parameters.AddWithValue("IsPrimary", 1);
                    else
                        resimcmd.Parameters.AddWithValue("IsPrimary", 0);

                    int resimsonuc = resimcmd.ExecuteNonQuery();

                    // Orjinal resmi bu kod satırıyla siliyorum.
                    FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte" + resimadi));
                    temp.Delete();

                    if (resimsonuc > 0)
                    {
                       //  Response.Redirect("news.aspx");
                        //lblMesaj.Text += "Dosya : <b>" + fileName + "</b> başarıyla yüklendi !<br />";
                    }


                }
               
            }

            #endregion


            Response.Redirect("news.aspx");

        }
    }
}