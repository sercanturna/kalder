﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class Messages : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            

            if (!IsPostBack)
            {
                DataTable dt = db.GetDataTable("Select * from ContactForm order by SentDate desc");

                CollectionPager1.DataSource = dt.DefaultView;

                CollectionPager1.BindToControl = rpt_News;
                rpt_News.DataSource = CollectionPager1.DataSourcePaged;
                rpt_News.DataBind();

                try
                {
                    GetMailSettings();
                }
                catch (Exception)
                { 

                }
              
            }
        }



        public void GetMailSettings()
        {
            DataRow dr = db.GetDataRow("Select * from MailSettings where FormId=1");

            bool FormIsActive = Convert.ToBoolean(dr["IsActive"]);

            IsActive.Checked = FormIsActive;
            this.Controls.Add(new LiteralControl("<script type='text/javascript'>CheckFormState('" + FormIsActive + "');</script>"));


            string To = dr["Alan"].ToString();
            string From = dr["Gonderen"].ToString();
            string SmtpUser = dr["SmtpKullaniciAdi"].ToString();
            string SmtpPass = dr["SmtpParola"].ToString();
            string SmtpServer = dr["SmtpSunucu"].ToString();

            TxtAlici.Text = To;
            TxtEmailServer.Text = SmtpServer;
            TxtSmtpUser.Text = SmtpUser;
           

            hdnPass.Value = SmtpPass;
            txtSmtpPass.Text = hdnPass.Value;
        }


        protected void btnDeleteAll_Onclick(object sender, EventArgs e)
        {
            foreach (RepeaterItem ri in rpt_News.Items)
            {
                CheckBox chk = (CheckBox)ri.FindControl("chkContainer");
                HiddenField hd = (HiddenField)ri.FindControl("hbItem");

                if (chk.Checked)
                {
                    db.cmd("delete from ContactForm where MessageId=" + hd.Value);
                }

            }

            Response.Redirect("messages.aspx");
        }

        protected void btnKaydet_Click(object sender, EventArgs e)
        {
            string To = TxtAlici.Text;
            string From = TxtSmtpUser.Text;
            string SmtpUser = TxtSmtpUser.Text;

            string SmtpPass = txtSmtpPass.Text;
            if (SmtpPass == "")
                SmtpPass = hdnPass.Value;

            string SmtpServer = TxtEmailServer.Text;
            string FormKonusu = "İletişim Formu Geldi";

            bool FormDurumu = IsActive.Checked; 


            string cmd = "Update MailSettings set Gonderen='"+From+"', Alan='"+To+"', FormKonusu='"+FormKonusu+"', SmtpSunucu='"+SmtpServer+"', SmtpKullaniciAdi='"+SmtpUser+"', " +
            "SmtpParola='"+SmtpPass+"', IsActive='" + FormDurumu + "' where FormId=1";

            db.guncelle(cmd, up1);


        }
    }
}