﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class add_slide : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();

        protected void Page_Load(object sender, EventArgs e)
        {
         

            if (!IsPostBack)
            {
                string sId = Request.QueryString["sId"];

            }
        }




        protected void btn_SlaytEkle_Click(object sender, EventArgs e)
        {
            string resimadi = "resim_yok.jpg";
            string uzanti = "";

            if (fuResim.HasFile)
            {
                uzanti = Path.GetExtension(fuResim.PostedFile.FileName);
                resimadi = Ayarlar.OzetCek(Ayarlar.UrlSeo(txtTitle.Text), 10).Trim().Replace(".","") + DateTime.Now.Millisecond + uzanti;
                fuResim.SaveAs(Server.MapPath("~/upload/sahte/" + resimadi));

                Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + resimadi));
                resim = Ayarlar.ResimBoyutlandir(resim, 1600);

                resim.Save(Server.MapPath("~/upload/SlaytResimleri/" + resimadi), resim.RawFormat);
                // Resmi önce SlaytResimleri klasörüne kayıt ediyoruz.

                //resim = Ayarlar.ResimBoyutlandir(resim, 220);
                //resim.Save(Server.MapPath("~/upload/SlaytResimleri/220/" + resimadi), ImageFormat.Jpeg);
                //// Resmi sonrada 220 klasörüne kayıt ediyoruz.

                FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + resimadi));
                temp.Delete();
            }

            SqlConnection baglanti = db.baglan();
            SqlCommand cmdKaydet = new SqlCommand("SET language turkish; insert into Sliders(" +
            "ImageURL," +
            "Title," +
            "IsActive," +
            "OrderNo," +
            "Link," +
            "LinkTarget," +
            "Date," +
            "EndDate) values (" +
            "@ImageURL," +
            "@Title," +
            "@IsActive," +
            "@OrderNo," +
            "@Link," +
            "@LinkTarget," +
            "@Date," +
            "@EndDate)", baglanti);


            cmdKaydet.Parameters.AddWithValue("ImageURL", resimadi);
            cmdKaydet.Parameters.AddWithValue("Title", txtTitle.Text);
            cmdKaydet.Parameters.AddWithValue("IsActive", drpDurum.SelectedValue);
            cmdKaydet.Parameters.AddWithValue("OrderNo", txtSiraNo.Text);
            cmdKaydet.Parameters.AddWithValue("Link", txtLink.Text);
            cmdKaydet.Parameters.AddWithValue("LinkTarget", dropTarget.SelectedValue);


            string BeginDateString = txtPublishDate.Text;
            string EndDateString = txtSuspendDate.Text;

            DateTime DTBegin, DtEnd;
            //DateTimeFormatInfo DateInfo = CultureInfo.CurrentCulture.DateTimeFormat;
            
            DTBegin = Convert.ToDateTime(String.Format("{0:yyyy-mm-dd hh:ii:ss}", BeginDateString.Trim()), CultureInfo.CurrentCulture);
            DtEnd = Convert.ToDateTime(String.Format("{0:yyyy-mm-dd hh:ii:ss}", EndDateString.Trim()), CultureInfo.CurrentCulture);

            cmdKaydet.Parameters.AddWithValue("Date", DTBegin);
            cmdKaydet.Parameters.AddWithValue("EndDate", DtEnd);




            int sonuc = cmdKaydet.ExecuteNonQuery();

            if (sonuc > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                Response.Redirect("sliders.aspx");
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);

        }
    }
}