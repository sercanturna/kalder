﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="edit_yk.aspx.cs" Inherits="Kalder.Adminv2.edit_trainer" %>


<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="CKFinder" Namespace="CKFinder" TagPrefix="CKFinder" %>


<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">

    <script src="js/multi.js" type="text/javascript"></script>
     <link href="./js/tags/jquery.tagsinput.css" rel="stylesheet" />
    <script src="./js/tags/jquery.tagsinput.js"></script>
     
    
    <script src="js/validation/jquery.validationEngine-tr.js"></script>
    <script src="js/validation/jquery.validationEngine.js"></script>
   <link href="js/validation/validationEngine.jquery.css" rel="stylesheet" /> 
   
     
      <script>
        //  $("#urunler").addClass("active");
    </script>

    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 183px;
            font-family:Verdana;
            font-size:13px;
            font-weight:bold;
            color:#646464;
        }
          ul.resimler
        {
          
        }

            ul.resimler li
            {
                height:280px;
            }

        .cats {
        padding:10px; max-height:340px; width:35%; overflow-y:scroll; border:1px solid #f3f3f3;
        }
        .cats input {
        float:right;
        }
        .cats label {
        display:block;
        }

            .cats ul {
            list-style-type:none; margin:0; 
            }
            .cats ul li {
            padding:0px; padding:3px; padding-bottom:1px;  
            }
            .cats ul li:nth-child(2n+1) {
            background-color:#f3f3f3;
            }

            .cats ul li:hover {
            background-color:#e2ebf9; border:1px dashed #b7b7b7;
            }


        .txtDetay {
        width: 100% !important;
        height:300px;
        }
    </style> 

    
    <script src="../ckeditor/ckeditor.js" type="text/javascript"></script>
 
    <script src="../ckfinder/ckfinder.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.onload = function () {
            var editor = CKEDITOR.replace('<% = txtDetay.ClientID %>');
            CKFinder.setupCKEditor(editor, '../ckfinder');
        };

        function DescChar(val) {
            var len = val.value.length;
            if (len >= 201) {
                val.value = val.value.substring(0, 200);
            } else {
                $('#desc').text(200 - len + " Karakter");
            }
        };

        function TitleChar(val) {
            var len = val.value.length;
            if (len >= 70) {
                val.value = val.value.substring(0, 70);
            } else {
                $('#title').text(70 - len + " Karakter");
            }
        };

      
      

        $(function () {
            $('#content_tags').tagsInput({
                width: 'auto',
            });
        });

</script>
     
       
 
 

</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="content" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server">
        <ContentTemplate>   
        
           

    <div class="row">
        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-globe"></i>
                    <h3>Yk Üyesi Düzenle</h3>
                </div>
                <!-- /widget-header -->


                <div class="widget-content">
                    <div class="tabbable">
					<ul class="nav nav-tabs">
					  <li class="active"><a href="#genel" data-toggle="tab">Genel Bilgiler</a></li>
                      <li class=""><a href="#seo" data-toggle="tab">SEO</a></li>
                     
					</ul>
					
					<br/>
					
						<div class="tab-content">
						
							
                            <div class="tab-pane active" id="genel">
                    <div id="edit-genel" class="form-horizontal">

                       


                        <div class="control-group">
                            <label class="control-label" for="txtKategoriAdi">Adı Soyadı</label>
                            <div class="controls">
                                <asp:TextBox ID="txtUrunAdi" runat="server" Width="300px"></asp:TextBox>
                            </div>
        
                            <!-- /controls -->
                        </div>
                        <!-- /control-group -->

                        <div class="control-group">
                            <label class="control-label" for="txtUnvan">Ünvanı</label>
                            <div class="controls">
                                <asp:TextBox ID="txtUnvan" runat="server" Width="300px"></asp:TextBox>
                            </div>
        
                            <!-- /controls -->
                        </div>
                        <!-- /control-group -->

                               <div class="control-group">
                            <label class="control-label" for="fuResim">Resim</label>
                            <div class="controls">
                                <asp:FileUpload ID="fuResim" runat="server" AllowMultiple="true" CssClass="multi" />
                                <p class="help-block">* Burada seçilecek resmin 3x4 formatında olması gerekli.(Önerilen Boyut : 300px x 400px)</p>
                            </div>
        
                            <!-- /controls -->
                        </div>
                        <!-- /control-group -->


                        <div class="control-group">
                            <label class="control-label" for="txtDetay">Açıklama</label>
                            <div class="controls">
                                <CKEditor:CKEditorControl ID="txtDetay" runat="server">
                                </CKEditor:CKEditorControl>
                                <p class="help-block">* Detaylı açıklamaları veya CV bilgilerini bu alanda belirtin.</p>
                            </div>
        
                            <!-- /controls -->
                        </div>
                        <!-- /control-group -->

                        

                               

                                <div class="control-group">
                                    <label class="control-label" for="chkOnay">Durum</label>
                                    <div class="controls">
                                         <asp:DropDownList ID="drpDurum" runat="server">
                                             <asp:ListItem Value="0">-Seçiniz-</asp:ListItem>
                                             <asp:ListItem Value="1">Aktif</asp:ListItem>
                                             <asp:ListItem Value="0">Pasif</asp:ListItem>
                                         </asp:DropDownList>
                                        <p class="help-block">* Durumu Pasif yaparsanız Üye sitede gözükmez.</p>
                                    </div>
        
                                    <!-- /controls -->
                                </div>
                                <!-- /control-group -->
 

                                <div class="control-group">
                                    <label class="control-label" for="txtSiraNo">Sıra No</label>
                                    <div class="controls">
                                    <asp:TextBox ID="txtSiraNo" runat="server"></asp:TextBox>
                                        <p class="help-block">* Üyelerin sitedeki sırasını belirtin. Eğer istemiyorsanız boş bırakın</p>
                                    </div>
        
                                    <!-- /controls -->
                                </div>
                                <!-- /control-group -->

                              

                        		 
                                </div>
                             </div>
 

                 

                                  


							<div class="tab-pane" id="seo">
                                <div id="edit-profile2" class="form-horizontal">	
                                  
                                      <div class="control-group">											
										<label class="control-label" for="vergidaire">Seo URL</label>
										<div class="controls">
                                            <asp:TextBox ID="txtSeoUrl" TextMode="SingleLine" CssClass="input-xlarge" runat="server"></asp:TextBox><div class="char" id="Div1"></div>
										 <p class="help-block">* Google ve diğer arama motorları için adres çubuğunda gözükecek kategoriniz için bir URL belirleyin.</p>
                                        </div> <!-- /controls -->				
									</div> <!-- /control-group -->


                                       <div class="control-group">											
										<label class="control-label" for="vergidaire">Meta Title</label>
										<div class="controls">
									 
                                            <asp:TextBox ID="txtTitle" TextMode="SingleLine" CssClass="input-xlarge" onkeyup="TitleChar(this)" runat="server"></asp:TextBox><div class="char" id="title"></div>
										 <p class="help-block">* Google ve diğer arama motorları için bir başlık yazın.</p>
                                        </div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    <div class="control-group">											
										<label class="control-label" for="vergidaire">Meta Description</label>
										<div class="controls">
                                            <asp:TextBox ID="txtDescription" TextMode="MultiLine" CssClass="input-xlarge" onkeyup="DescChar(this)" runat="server"></asp:TextBox><div class="char" id="desc"></div>
                                            <p class="help-block">* Google ve diğer arama motorları için bir açıklama yazın.</p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->

                                    <div class="control-group">											
										<label class="control-label" for="vergidaire">Meta Keywords</label>
										<div class="controls">                                            
                                            <asp:TextBox ID="tags"  CssClass="input-medium" runat="server"></asp:TextBox>
                                            <p class="help-block">* Google ve diğer arama motorları için anahtar kelimeler yazın. <br />Kelimeler arasına virgül koyarak yada Enter tuşuna basarak geçiş yapabilirsiniz.</p>
										</div> <!-- /controls -->				
									</div> <!-- /control-group -->


                                    </div>
							 </div>





						</div>
    <div class="form-actions">
        <asp:UpdatePanel ID="upInsert" runat="server">
            <Triggers>
                <asp:PostBackTrigger ControlID="btn_UrunEkle" />
                <asp:PostBackTrigger ControlID="btn_Temizle" />
            </Triggers>
            <ContentTemplate>
                <asp:Button ID="btn_UrunEkle" runat="server" CssClass="btn btn-primary" onclick="btn_UrunEkle_Click" Text="Güncelle" />
                <asp:Button ID="btn_Temizle" runat="server" CssClass="btn"  Text="İptal" OnClick="btn_Temizle_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
        </div>
    <!-- /form-actions -->
   
                        </div>
                </div>
            </div>
         </div>
    </div>
       
         </ContentTemplate>
       
    </asp:UpdatePanel>
     <script src="./js/demo/validation.js"></script>
</asp:Content>