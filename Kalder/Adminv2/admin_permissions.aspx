﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="admin_permissions.aspx.cs" Inherits="Kalder.Adminv2.admin_permissions" %>
 
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cportal İçerik Yönetim Sistemi</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />

    <link href="./css/bootstrap.min.css" rel="stylesheet" />
    <link href="./css/bootstrap-responsive.min.css" rel="stylesheet" />

    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet" />
    <link href="./css/font-awesome.min.css" rel="stylesheet" />

    <link href="./css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet" />
    <link href="./js/plugins/msgGrowl/css/msgGrowl.css" rel="stylesheet" />
    <link href="./js/plugins/lightbox/themes/evolution-dark/jquery.lightbox.css" rel="stylesheet" />
    <link href="./js/plugins/msgbox/jquery.msgbox.css" rel="stylesheet" />

    <link href="./js/plugins/cirque/cirque.css" rel="stylesheet" />
    <link href="./css/pages/plans.css" rel="stylesheet" />



    <link href="./css/base-admin-2.css" rel="stylesheet" />
    <link href="./css/base-admin-2-responsive.css" rel="stylesheet" />



    <link href="./css/custom.css" rel="stylesheet" /> 



    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
        


        ul.liste {
            list-style: none;
            margin: 0;
            padding: 0;
        }
        /*ul.liste li input, ul.liste li label {
            margin:0; padding:0; line-height:1;  
            }*/

        .array input {
            float: left;
            margin-right: 3px;
        }

        .array label {
            display: block;
        }

        .control-label {
            float: none;
            display: block;
            color: #d88304;
            font-weight: 600;
        }


        div.column {
            border-right: 1px solid #dddddd; min-height:200px; padding-top:30px;
        }

        div.column:last-child {
            border-right: 0px solid #dddddd; min-height:200px; padding-top:30px;
        }

        .form-inline {
            border: 1px solid #dddddd;
            border-bottom: none;
            overflow: hidden;
            display: block;
        }
    </style>

    <script>
        $(document).ready(function () {

             
            

            $('#selectAll').click(function (event) {  //on click 
               
                if (this.checked) { // check select status
                    $(this).siblings('label').html('Seçimleri Kaldır');

                    $('.liste input').each(function () { //loop through each checkbox
                        this.checked = true;  //select all checkboxes with class "checkbox1"               
                    });
                } else {
                    $(this).siblings('label').html('Tümünü Seç');
                    $('.liste input').each(function () { //loop through each checkbox
                        this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                    });
                }
            });

            function CloseModal() {
                $('#myModal').modal.close();
            }
            

        });
    </script>
</head>
<body>
    <form id="form2" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel11" runat="server">
            

              <Triggers>
                 <asp:AsyncPostBackTrigger ControlID="lnkYetkiGuncelle" EventName="Click" />
            </Triggers> 
        <ContentTemplate>
        <asp:Panel ID="pnlContent" runat="server">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h3>
                <asp:Literal runat="server" ID="ltrlName"></asp:Literal></h3>
        </div>
        <div>
            <asp:Literal ID="ltrlText" runat="server" Visible="false"></asp:Literal>

            <div class="form-inline">
                <div class="column span2">
                    <div class="control-group">
                        <label class="control-label" for="dropKat">Genel Ayarlar</label>
                        <div class="controls array">
                            <%--<asp:CheckBoxList ID="chkKat" runat="server" DataTextField="KatAdi"  DataValueField="UrunKatId" RepeatLayout="UnorderedList" OnDataBound="chkKat_DataBound" ></asp:CheckBoxList>--%>
                            <asp:CheckBoxList ID="chkListGenelAyarlar" runat="server" RepeatDirection="Vertical" RepeatLayout="UnorderedList" CssClass="liste">
                                <asp:ListItem>Modül Ekleme/Silme</asp:ListItem>
                                <asp:ListItem>Mesaj İzleme/Silme</asp:ListItem>
                                <asp:ListItem>Düzenleme</asp:ListItem>
                                <asp:ListItem>Silme</asp:ListItem>
                            </asp:CheckBoxList>
                        </div>
                        <!-- /controls -->
                    </div>
                    <!-- /control-group -->
                </div>

                <div class="column span2">
                    <div class="control-group">
                        <label class="control-label" for="dropKat">Sayfa Yönetimi</label>
                        <div class="controls array">
                            <%--<asp:CheckBoxList ID="chkKat" runat="server" DataTextField="KatAdi"  DataValueField="UrunKatId" RepeatLayout="UnorderedList" OnDataBound="chkKat_DataBound" ></asp:CheckBoxList>--%>
                            <asp:CheckBoxList ID="chkListSayfaYonetimi" runat="server" RepeatDirection="Vertical" RepeatLayout="UnorderedList" CssClass="liste">
                                <asp:ListItem>Sayfa Ekleme</asp:ListItem>
                                <asp:ListItem>Görüntüleme</asp:ListItem>
                                <asp:ListItem>Düzenleme</asp:ListItem>
                                <asp:ListItem>Silme</asp:ListItem>
                            </asp:CheckBoxList>
                        </div>
                        <!-- /controls -->
                    </div>
                    <!-- /control-group -->
                </div>

                <div class="column span2">
                    <div class="control-group">
                        <label class="control-label" for="dropKat">Haber Yönetimi</label>
                        <div class="controls array">
                            <%--<asp:CheckBoxList ID="chkKat" runat="server" DataTextField="KatAdi"  DataValueField="UrunKatId" RepeatLayout="UnorderedList" OnDataBound="chkKat_DataBound" ></asp:CheckBoxList>--%>
                            <asp:CheckBoxList ID="chkListHaberYonetimi" runat="server" RepeatDirection="Vertical" RepeatLayout="UnorderedList" CssClass="liste">
                                <asp:ListItem>Kategori Yönetimi</asp:ListItem>
                                <asp:ListItem>Haber Ekleme</asp:ListItem>
                                <asp:ListItem>Görüntüleme</asp:ListItem>
                                <asp:ListItem>Düzenleme</asp:ListItem>
                                <asp:ListItem>Silme</asp:ListItem>
                            </asp:CheckBoxList>
                        </div>
                        <!-- /controls -->
                    </div>
                    <!-- /control-group -->
                </div>

                <div class="column span2">
                    <div class="control-group">
                        <label class="control-label" for="dropKat">Ürün Yönetimi</label>
                        <div class="controls array">
                            <%--<asp:CheckBoxList ID="chkKat" runat="server" DataTextField="KatAdi"  DataValueField="UrunKatId" RepeatLayout="UnorderedList" OnDataBound="chkKat_DataBound" ></asp:CheckBoxList>--%>
                            <asp:CheckBoxList ID="chkListUrunYonetimi" runat="server" RepeatDirection="Vertical" RepeatLayout="UnorderedList" CssClass="liste">
                                <asp:ListItem>Kategori Yönetimi</asp:ListItem>
                                <asp:ListItem>Marka Yönetimi</asp:ListItem>
                                <asp:ListItem>Ürün Ekleme</asp:ListItem>
                                <asp:ListItem>Görüntüleme</asp:ListItem>
                                <asp:ListItem>Düzenleme</asp:ListItem>
                                <asp:ListItem>Silme</asp:ListItem>
                            </asp:CheckBoxList>
                        </div>
                        <!-- /controls -->
                    </div>
                    <!-- /control-group -->
                </div>

                <div class="column span2">
                    <div class="control-group">
                        <label class="control-label" for="dropKat">Albüm Yönetimi</label>
                        <div class="controls array">
                            <%--<asp:CheckBoxList ID="chkKat" runat="server" DataTextField="KatAdi"  DataValueField="UrunKatId" RepeatLayout="UnorderedList" OnDataBound="chkKat_DataBound" ></asp:CheckBoxList>--%>
                            <asp:CheckBoxList ID="chkListAlbumYonetimi" runat="server" RepeatDirection="Vertical" RepeatLayout="UnorderedList" CssClass="liste">
                                <asp:ListItem>Album Yönetimi</asp:ListItem>
                                <asp:ListItem>Resim Ekleme</asp:ListItem>
                                <asp:ListItem>Resim Silme</asp:ListItem>
                            </asp:CheckBoxList>
                        </div>
                        <!-- /controls -->
                    </div>
                    <!-- /control-group -->
                </div>

                <div class="column span2">
                    <div class="control-group">
                        <label class="control-label" for="dropKat">Referans Yönetimi</label>
                        <div class="controls array">
                            <%--<asp:CheckBoxList ID="chkKat" runat="server" DataTextField="KatAdi"  DataValueField="UrunKatId" RepeatLayout="UnorderedList" OnDataBound="chkKat_DataBound" ></asp:CheckBoxList>--%>
                            <asp:CheckBoxList ID="chkListReferenceManagement" runat="server" RepeatDirection="Vertical" RepeatLayout="UnorderedList" CssClass="liste">
                                <asp:ListItem>Referans Yönetimi</asp:ListItem>
                            </asp:CheckBoxList>
                        </div>
                        <!-- /controls -->
                    </div>
                    <!-- /control-group -->
                </div>

                 


            </div>

        </div>
            </asp:Panel>
        <asp:Panel ID="pnlAlert" runat="server">
            <img src="img/notifications/error-48.png" /> Üzgünüz Bu bölümü görüntüleme yetkiniz yok! Lütfen site yöneticiniz ile görüşün.
        </asp:Panel>

              
                        <div class="modal-footer">
                            <input type="checkbox" id="selectAll" class="checkbox pull-left" /><label for="selectAll" class="pull-left">Tümünü Seç</label>
                            <%--<asp:LinkButton ID="lnkSelAll" runat="server" OnClick="lnkSelAll_Click" CssClass="btn btn-primary pull-left" Text="Tümünü Seç"></asp:LinkButton>--%>
                            <a href="javascript:;" class="btn" data-dismiss="modal">Kapat</a>
                            <%--<a href="javascript:;" class="btn btn-primary">Yetkileri Güncelle</a>--%>
                            <asp:LinkButton ID="lnkYetkiGuncelle" runat="server" OnClick="lnkYetkiGuncelle_Click" CssClass="btn btn-primary" Text="Yetkileri Güncelle"></asp:LinkButton>
                        </div>
                   
        
            </ContentTemplate>
        </asp:UpdatePanel>
         

         
    </form>
</body>
</html>
