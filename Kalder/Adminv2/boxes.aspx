﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="boxes.aspx.cs" Inherits="Kalder.Adminv2.boxes" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    
    <%@ Register Assembly="CollectionPager" Namespace="SiteUtils" TagPrefix="cc1" %>

    <script type="text/javascript">
        $(function () {
            var $allCheckbox = $('.allCheckbox :checkbox');
            var $checkboxes = $('.singleCheckbox :checkbox');
            $allCheckbox.change(function () {
                if ($allCheckbox.is(':checked')) {
                    $checkboxes.attr('checked', 'checked');
                }
                else {
                    $checkboxes.removeAttr('checked');
                }
            });
            $checkboxes.change(function () {
                if ($checkboxes.not(':checked').length) {
                    $allCheckbox.removeAttr('checked');
                }
                else {
                    $allCheckbox.attr('checked', 'checked');
                }
            });
        });

        
        $("#eklentiler").addClass("active");

    </script>
    <style>
        th.allCheckbox {
            width: 20px;
        }

            td.singleCheckbox input, th.allCheckbox input {
                width: 20px !important;
            }

        .sercan {
        vertical-align:top; margin-top:0px;
        }
    </style>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="up1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
    <div class="row">

        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-picture"></i>
                    <h3>Kutu Yönetimi</h3>
                    <asp:LinkButton ID="btnDeleteAll" OnClick="btnDeleteAll_Onclick" runat="server" CssClass="btn right" BorderStyle="Solid" BorderColor="#606060" BorderWidth="1" ToolTip="Sil" CausesValidation="False" Text="x Sil" OnClientClick="return confirm('Bu Kutuyu silmek istediğinize emin misiniz?');"></asp:LinkButton>
                    <a href="add_box.aspx" class="btn btn-success btn-primary right"> + Ekle </a>
                    
                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                    <div style="border-bottom:1px dashed #808080; margin-bottom:10px;">
                        <label style="float:left; line-height:2; padding-right:5px;">Başlık :</label><asp:TextBox ID="txtKutuBaslik" runat="server" CssClass="left"></asp:TextBox> <asp:Button ID="btnBaslik" runat="server" Text="Güncelle" CssClass="btn btn-success btn-primary left sercan" OnClick="btnBaslik_Click" />
                    </div>
                    <div class="EU_TableScroll">
                        <asp:UpdatePanel ID="upGrid" runat="server">
                            <ContentTemplate>

                                <table id="ProductCat" class="table table-bordered table-striped blank"  border="1" style="border-collapse: collapse;">
                                   <asp:Repeater ID="rpt_Boxes" runat="server">
                                       <HeaderTemplate>
                                     <tr >
                                        <th scope="col" class="allCheckbox"><asp:CheckBox ID="allCheckbox1"  runat="server" /></th>
                                        <th scope="col">ID</th>
                                       <th scope="col">Resim</th>
                                        <th scope="col">Başlık</th>
                                        <th scope="col">Durum</th>
                                        <th scope="col">Link</th>
                                        <th scope="col">Target</th>
                                         <th scope="col">Sıra No</th>
                                        <th scope="col">Eklenme Tarihi</th>
                                        <th scope="col">İşlem</th>
                                    </tr>
                                    </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="singleCheckbox">
                                                    <asp:CheckBox ID="chkContainer" runat="server"  />
                                                    <asp:HiddenField ID="hbItem" runat="server" Value='<%# Eval("BoxId") %>' />
                                                </td>
                                                 <td><%#Eval ("BoxId") %></td>
                                                 <td><img src="../upload/Box/<%#Eval ("ImageURL") %>" style="height:50px" /></td>
                                                 <td><%#Eval ("Title") %></td>
                                                 <td class="center"><%# Eval("IsActive") == "True" ? Eval("IsActive") : Eval("IsActive").ToString().Replace("True", "<img src=\"img/play.png\" />").Replace("False", "<img src=\"img/pause.png\" />")%></td>
                                                 <td><a href="<%#Eval ("Link") %>" target="_blank"><%#Eval ("Link") %></a></td>
                                                
                                                <td><%# Eval("LinkTarget") == "_blank" ? Eval("LinkTarget") : Eval("LinkTarget").ToString().Replace("_blank", "Yeni Pencere").Replace("_self", "Aynı Pencere")%></td>
                                                <td><%#Eval ("OrderNo") %></td>
                                                 <td><%#Eval ("Date") %></td>
                                                 <td><a href="edit_box.aspx?bId=<%#Eval("BoxId") %>" class="btn btn-small"><i class="btn-icon-only icon-pencil"></i></a></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                  
                                    <!--
                                    <tr>
                                        <td colspan="9" style="text-align:center; padding:20px;" ><asp:Label CssClass="alert" ID="ltrlSonuc" runat="server" Text="Henüz bir ürün kategorisi oluşturulmamış."></asp:Label></td>
                                    </tr>
                                    -->
                                     
                                </table>
                              <section id="paginations">
                                <div class="sayfalama">
                                  <cc1:CollectionPager ID="CollectionPager1" runat="server" BackText=" « Önceki" 
                                    FirstText="İlk" LabelText="" LastText="Son" NextText="Sonraki »" 
                                    PageNumbersDisplay="Numbers" ResultsFormat="Sayfalar {0} {1} (Toplam:{2})"
                                    PageSize="5" SectionPadding="5" BackNextDisplay="Buttons" BackNextLocation="Split" 
                                    MaxPages="5" PageNumbersSeparator=""></cc1:CollectionPager>
                         </div>
                                  </section>
                                        </ContentTemplate>
                        </asp:UpdatePanel>

                        
                        
                    </div>
                </div>
            </div>
        </div>


    </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress AssociatedUpdatePanelID="up1" runat="server">
        <ProgressTemplate>
            <div class="progress_template">
                <img src="img/loader.gif" /><br /><h1 class="help-block">İŞLEM GERÇEKLEŞTİRİLİYOR!</h1>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>