﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="news_categories.aspx.cs" Inherits="Kalder.Adminv2.news_categories" %>
 
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    
    <script type="text/javascript">
        function toogleSelectionUsingHeaderCheckbox(source) {
            $("#content_GridView1 input[name$='cbDelete']").each(function () {
                $(this).attr('checked', source.checked);
            });
        }

        function toogleSelectionOfHeaderCheckbox() {
            var Checkboxs = $("#content_GridView1 input[name$='cbDelete']").length;
            var CheckSelected = $("#content_GridView1 input[name$='cbDelete']:checked").length;
            if (Checkboxs == CheckSelected) {
                $("#content_GridView1 input[name$='cbDeleteHeader']").attr('checked', true);
            }
            else {
                $("#content_GridView1 input[name$='cbDeleteHeader']").attr('checked', false);
            }
        }
        
                      $(document).load(function () {
                          $("#content_GridView1_btnDeleteAll").click(function () {
                              var RowSelected = $("#content_GridView1 input[name$='cbDelete']:checked").length;
                              if (RowSelected == 0) {
                                  alert('Hiçbir kayıt seçmediniz');
                                  return false;
                              }
                              else
                                  return confirm("Seçtiğiniz " + RowSelected + ' kayıt silinecek, onaylıyor musunuz?');
                          });
                      });

                      $("#haberler").addClass("active");

    </script>
       <style>
          .sercan {
        vertical-align:top; margin-top:0px;
        }

        .showmenu label, .showmenu input{
        float:left; margin-right:5px;
        }
    </style>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <div class="row">

        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-globe"></i>
                    <h3>Haber Kategorileri</h3>
                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                     <div style="border-bottom:1px dashed #808080; margin-bottom:10px;">
                        <label style="float:left; line-height:2; padding-right:5px;">Başlık :</label><asp:TextBox ID="txtKutuBaslik" runat="server" CssClass="left"></asp:TextBox> <asp:Button ID="btnBaslik" runat="server" Text="Güncelle" CssClass="btn btn-success btn-primary left sercan" OnClick="btnBaslik_Click" />
                    <div class="right showmenu sercan"><asp:Button ID="btnShowMenu" runat="server" OnClick="btnShowMenu_Click" /></div>
                    </div>

                    <div class="EU_TableScroll">
                        <asp:UpdatePanel ID="upGrid" runat="server">
                            <ContentTemplate>

                    
                        <asp:GridView ID="GridView1" runat="server" CssClass="table table-bordered table-striped" AllowPaging="True"
                            AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="KategoriId" DataSourceID="SqlDataSource1" ShowFooter="True"
                            OnRowDeleting="GridView1_RowDeleting" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" OnRowCancelingEdit="GridView1_RowCancelingEdit" OnRowUpdating="GridView1_RowUpdating" FooterStyle-CssClass="Ekle" EditRowStyle-BackColor="SlateGray">

                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="cbDeleteHeader" onclick="toogleSelectionUsingHeaderCheckbox(this);" runat="server" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbDelete" onclick="toogleSelectionOfHeaderCheckbox();" runat="server" />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:LinkButton ID="btnDeleteAll" OnClick="btnDeleteAll_Onclick" ToolTip="Seçilen Kayıtları Sil" runat="server" Text="Sil"></asp:LinkButton>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="KategoriId" HeaderText="ID" InsertVisible="False" ReadOnly="True" SortExpression="KategoriId" />

                                <asp:TemplateField HeaderText="Kategori Adı" SortExpression="KategoriAdi">

                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtKatAdi" runat="server" Text='<%# Bind("KategoriAdi") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqCatName" ValidationGroup="Edit" ControlToValidate="txtKatAdi" runat="server" Text="*" ForeColor="Red" ErrorMessage="Bir kategori Adı Yazmalısınız!"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Bind("KategoriAdi") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtKatAdiEkle" runat="server" ToolTip="Kategori oluşturup, haberlerinizi kolayca gruplayabilirsiniz."></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqName" ValidationGroup="Insert" ControlToValidate="txtKatAdiEkle" runat="server" Text="*" ForeColor="Red" ErrorMessage="Bir kategori Adı Yazmalısınız!"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Kategori Resmi" SortExpression="KategoriResmi">
                                    <EditItemTemplate>
                                        <!-- <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("KategoriResmi") %>'></asp:TextBox>-->
                                        <asp:FileUpload ID="fuKatResim" runat="server" />

                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Image ID="img1" runat="server" CssClass="img-polaroid img-small" ImageUrl='<%# "~/upload/" + Eval("KategoriResmi") %>' />
                                        <!-- <asp:Label ID="Label2" runat="server" Text='<%# Bind("KategoriResmi") %>'></asp:Label>-->
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:FileUpload ID="fuKatResimEkle" runat="server" ToolTip="Haber Kategorisi için temsili bir kapak fotoğrafı seçin." />
                                        <%--<asp:RequiredFieldValidator ID="reqImage" ValidationGroup="Insert" ControlToValidate="fuKatResimEkle" runat="server" Text="*" ForeColor="Red" ErrorMessage="Kategori için bir kapak resmi yüklemelisiniz!"></asp:RequiredFieldValidator>--%>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sıra No" SortExpression="KategoriSiraNo">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtSiraNo" runat="server" Text='<%# Bind("KategoriSiraNo") %>'></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqOrderNo" ValidationGroup="Edit" ControlToValidate="txtSiraNo" runat="server" Text="*" ForeColor="Red" ErrorMessage="Bir Sıra numarası Girmelisiniz!"></asp:RequiredFieldValidator>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("KategoriSiraNo") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:TextBox ID="txtSiraNoEkle" runat="server" ToolTip="Sitedeki sıralamayı belirtir. Bu sayede menü yada sayfa içeriğinde belirttiğiniz sırada gözükür."></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqInsertOrderNo" ValidationGroup="Insert" ControlToValidate="txtSiraNoEkle" runat="server" Text="*" ForeColor="Red" ErrorMessage="Bir Sıra numarası Girmelisiniz!"></asp:RequiredFieldValidator>
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Yayınla" SortExpression="YayinDurumu">
                                    <EditItemTemplate>
                                        <asp:CheckBox ID="chkYayin" runat="server" Checked='<%# Bind("YayinDurumu") %>' />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkYayin" runat="server" Checked='<%# Bind("YayinDurumu") %>' Enabled="false" />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:CheckBox ID="chkDurumEkle" runat="server" Enabled="true" ToolTip="Bu Kategoriyi yayından kaldırır, istediğiniz zaman tekrar yayınlayabilirsiniz." />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Menude Göster" SortExpression="MenuGorunumu">
                                    <EditItemTemplate>
                                        <asp:CheckBox ID="chkMenu" runat="server" Checked='<%# Bind("MenuGorunumu") %>' />
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkMenu" runat="server" Checked='<%# Bind("MenuGorunumu") %>' Enabled="false" />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:CheckBox ID="chkMenuEkle" runat="server" Enabled="true" ToolTip="Kategorinin site menüsünde gözükmesini sağlar." />
                                    </FooterTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Eklenme Tarihi" SortExpression="EklenmeTarihi">
                                    <EditItemTemplate>
                                        <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("EklenmeTarihi") %>'></asp:TextBox>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="Label4" runat="server" Text='<%# Bind("EklenmeTarihi") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="İşlem" ConvertEmptyStringToNull="False">
                                    <EditItemTemplate>
                                        <asp:UpdatePanel ID="upOnayla" runat="server">
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="LinkButton1" />
                                            </Triggers>
                                            <ContentTemplate>
                                                <asp:LinkButton ID="LinkButton1" runat="server" ValidationGroup="Edit" CssClass="btn btn-small btn-warning" ToolTip="Onayla" CausesValidation="True" CommandName="Update" Text="">
                                            <i class="btn-icon-only icon-ok"></i>	
                                                </asp:LinkButton>
                                                &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-small" ToolTip="İptal" CausesValidation="False" CommandName="Cancel" Text="İptal">
                                             <i class="btn-icon-only icon-circle-arrow-left"></i>
                                                </asp:LinkButton>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </EditItemTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-small" ToolTip="Düzenle" CausesValidation="true" CommandName="Edit" Text="">
                                             <i class="btn-icon-only icon-pencil"></i>	
                                        </asp:LinkButton>
                                        &nbsp;
                                        <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-small" ToolTip="Sil" CausesValidation="False" CommandName="Delete" Text="" OnClientClick="return confirm('Bu kategoriyi silmek istediğinize emin misiniz? \nEğer Kategoriyi silerseniz bu kategoriye ait tüm haberler silinecektir.');">
                                            <i class="btn-icon-only icon-remove"></i>
                                        </asp:LinkButton>
                                        &nbsp;
                                        <asp:LinkButton ID="HyperLink1" runat="server" CssClass="btn btn-small btn-warning" PostBackUrl='<%# Eval("KategoriId", "add_news.aspx?kId={0}") %>' ToolTip="Haber Ekle">
                                            <i class="btn-icon-only icon-plus"></i>
                                        </asp:LinkButton>
                                        
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:UpdatePanel ID="upEkle" runat="server">
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkEkle" />
                                            </Triggers>
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lnkEkle" runat="server" OnClick="lnkEkle_Click" ValidationGroup="Insert" CausesValidation="true" Text="Ekle" CssClass="btn btn-success btn-primary"></asp:LinkButton>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </FooterTemplate>
                                </asp:TemplateField>



                            </Columns>

                            <EmptyDataTemplate>
                                
                                <table class="table table-bordered table-striped blank" cellspacing="0" border="1" style="border-collapse: collapse;">
                                    <tr>
                                        <th scope="col"> - </th>
                                        <th scope="col">ID</th>
                                        <th scope="col">Kategori Adı</th>
                                        <th scope="col">Kategori Resmi</th>
                                        <th scope="col">Sıra No</th>
                                        <th scope="col">Yayınla</th>
                                        <th scope="col">Menude Göster</th>
                                        <th scope="col">Eklenme Tarihi</th>
                                        <th scope="col">İşlem</th>
                                    </tr>
                                    <tr>
                                        <td colspan="9" style="text-align:center; padding:20px;" ><asp:Label CssClass="alert" ID="ltrlSonuc" runat="server" Text="Henüz bir kategori oluşturulmamış. Lütfen aşağıdaki formu kullanarak bir tane oluşturun."></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                         <td>&nbsp;</td>
                                         <td><asp:TextBox ID="txtKatAdiEkle" runat="server" ToolTip="Kategori isim verip, haberlerinizi kolayca gruplayabilirsiniz." ></asp:TextBox>
                                             <asp:RequiredFieldValidator ID="reqName" ValidationGroup="NewInsert" ControlToValidate="txtKatAdiEkle" runat="server" Text="*" ForeColor="Red" ErrorMessage="Bir kategori Adı Yazmalısınız!"></asp:RequiredFieldValidator></td>
                                         <td><asp:FileUpload ID="fuKatResimEkle" runat="server" ToolTip="Kategoriniz için temsili bir kapak fotoğrafı seçin." />
                                            <%--<asp:RequiredFieldValidator ID="reqImage" ValidationGroup="NewInsert" ControlToValidate="fuKatResimEkle" runat="server" Text="*" ForeColor="Red" ErrorMessage="Kategori için bir kapak resmi yüklemelisiniz!"></asp:RequiredFieldValidator>--%></td>
                                         <td><asp:TextBox ID="txtSiraNoEkle" runat="server" ToolTip="Sitedeki sıralamayı belirtir. Bu sayede menü yada sayfa içeriğinde belirttiğiniz sırada gözükür." ></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="reqInsertOrderNo" ValidationGroup="NewInsert" ControlToValidate="txtSiraNoEkle" runat="server" Text="*" ForeColor="Red" ErrorMessage="Bir Sıra numarası Girmelisiniz!"></asp:RequiredFieldValidator></td>
                                         <td><asp:CheckBox ID="chkDurumEkle" runat="server" Enabled="true" ToolTip="Kategoriyi yayından kaldırır, istediğiniz zaman tekrar yayınlayabilirsiniz."  /></td>
                                         <td><asp:CheckBox ID="chkMenuEkle" runat="server" Enabled="true" ToolTip="Kategorinin site menüsünde gözükmesini sağlar." /></td>
                                         <td>&nbsp;</td>
                                         <td>
                                             
                                              <asp:UpdatePanel ID="upEmptyEkle" runat="server">
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkEmtyEkle" />
                                            </Triggers>
                                            <ContentTemplate>
                                                <asp:LinkButton ID="lnkEmtyEkle" runat="server" OnClick="lnkEkle_Click" ValidationGroup="NewInsert" CausesValidation="true" Text="Ekle" CssClass="btn btn-success btn-primary"></asp:LinkButton>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                         </td>
                                    </tr>
                                </table>
                            </EmptyDataTemplate>
                            <EditRowStyle BackColor="SlateGray" />
                            <FooterStyle CssClass="Ekle" />
                            <PagerSettings Mode="NumericFirstLast" />
                            <PagerStyle CssClass="pagination" />
                        </asp:GridView>

                                        </ContentTemplate>
                        </asp:UpdatePanel>

                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:KalderConnectionString %>"
                            DeleteCommand="DELETE FROM [HaberKategori] WHERE [KategoriId] = @original_KategoriId AND (([KategoriAdi] = @original_KategoriAdi) OR ([KategoriAdi] IS NULL AND @original_KategoriAdi IS NULL)) AND (([KategoriResmi] = @original_KategoriResmi) OR ([KategoriResmi] IS NULL AND @original_KategoriResmi IS NULL)) AND (([KategoriSiraNo] = @original_KategoriSiraNo) OR ([KategoriSiraNo] IS NULL AND @original_KategoriSiraNo IS NULL)) AND (([YayinDurumu] = @original_YayinDurumu) OR ([YayinDurumu] IS NULL AND @original_YayinDurumu IS NULL)) AND (([MenuGorunumu] = @original_MenuGorunumu) OR ([MenuGorunumu] IS NULL AND @original_MenuGorunumu IS NULL)) AND (([EklenmeTarihi] = @original_EklenmeTarihi) OR ([EklenmeTarihi] IS NULL AND @original_EklenmeTarihi IS NULL))"
                            InsertCommand="INSERT INTO [HaberKategori] ([KategoriAdi],[SeoURL], [KategoriResmi], [KategoriSiraNo], [YayinDurumu], [MenuGorunumu], [EklenmeTarihi]) VALUES (@KategoriAdi,@SeoURL, @KategoriResmi, @KategoriSiraNo, @YayinDurumu, @MenuGorunumu, @EklenmeTarihi)" OldValuesParameterFormatString="original_{0}"
                            SelectCommand="SELECT * FROM [HaberKategori]"
                            UpdateCommand="UPDATE [HaberKategori] SET [KategoriAdi] = @KategoriAdi, [SeoURL] = @SeoURL, [KategoriResmi] = @KategoriResmi, [KategoriSiraNo] = @KategoriSiraNo, [YayinDurumu] = @YayinDurumu, [MenuGorunumu] = @MenuGorunumu, [EklenmeTarihi] = @EklenmeTarihi 
                            WHERE [KategoriId] = @original_KategoriId AND 
                            (([KategoriAdi] = @original_KategoriAdi) OR ([KategoriAdi] IS NULL AND @original_KategoriAdi IS NULL)) AND 
                           
                            (([KategoriResmi] = @original_KategoriResmi) OR ([KategoriResmi] IS NULL AND @original_KategoriResmi IS NULL)) AND 
                            (([KategoriSiraNo] = @original_KategoriSiraNo) OR ([KategoriSiraNo] IS NULL AND @original_KategoriSiraNo IS NULL)) AND 
                            (([YayinDurumu] = @original_YayinDurumu) OR ([YayinDurumu] IS NULL AND @original_YayinDurumu IS NULL)) AND 
                            (([MenuGorunumu] = @original_MenuGorunumu) OR ([MenuGorunumu] IS NULL AND @original_MenuGorunumu IS NULL)) AND 
                            (([EklenmeTarihi] = @original_EklenmeTarihi) OR ([EklenmeTarihi] IS NULL AND @original_EklenmeTarihi IS NULL))">
                            <DeleteParameters>
                                <asp:Parameter Name="original_KategoriId" Type="Int32" />
                                <asp:Parameter Name="original_KategoriAdi" Type="String" />
                                <asp:Parameter Name="original_SeoURL" Type="String" />
                                <asp:Parameter Name="original_KategoriResmi" Type="String" />
                                <asp:Parameter Name="original_KategoriSiraNo" Type="Int32" />
                                <asp:Parameter Name="original_YayinDurumu" Type="Boolean" />
                                <asp:Parameter Name="original_MenuGorunumu" Type="Boolean" />
                                <asp:Parameter Name="original_EklenmeTarihi" Type="DateTime" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="KategoriAdi" Type="String" />
                                <asp:Parameter Name="SeoURL" Type="String" />
                                <asp:Parameter Name="KategoriResmi" Type="String" />
                                <asp:Parameter Name="KategoriSiraNo" Type="Int32" />
                                <asp:Parameter Name="YayinDurumu" Type="Boolean" />
                                <asp:Parameter Name="MenuGorunumu" Type="Boolean" />
                                <asp:Parameter Name="EklenmeTarihi" Type="DateTime" />
                            </InsertParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="KategoriAdi" Type="String" />
                                <asp:Parameter Name="SeoURL" Type="String" />
                                <asp:Parameter Name="KategoriResmi" Type="String" />
                                <asp:Parameter Name="KategoriSiraNo" Type="Int32" />
                                <asp:Parameter Name="YayinDurumu" Type="Boolean" />
                                <asp:Parameter Name="MenuGorunumu" Type="Boolean" />
                                <asp:Parameter Name="EklenmeTarihi" Type="DateTime" />
                                <asp:Parameter Name="original_KategoriId" Type="Int32" />
                                <asp:Parameter Name="original_KategoriAdi" Type="String" />
                                <asp:Parameter Name="original_SeoURL" Type="String" />
                                <asp:Parameter Name="original_KategoriResmi" Type="String" />
                                <asp:Parameter Name="original_KategoriSiraNo" Type="Int32" />
                                <asp:Parameter Name="original_YayinDurumu" Type="Boolean" />
                                <asp:Parameter Name="original_MenuGorunumu" Type="Boolean" />
                                <asp:Parameter Name="original_EklenmeTarihi" Type="DateTime" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                        <asp:ValidationSummary ID="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" ValidationGroup="Insert" HeaderText="Kayıt eklerken şu hataları yaptınız:" DisplayMode="BulletList" EnableClientScript="true" runat="server" />
                        <asp:ValidationSummary ID="ValidationSummary3" ShowMessageBox="true" ShowSummary="false" ValidationGroup="NewInsert" HeaderText="Kayıt eklerken şu hataları yaptınız:" DisplayMode="BulletList" EnableClientScript="true" runat="server" />
                        <asp:ValidationSummary ID="ValidationSummary2" ShowMessageBox="true" ShowSummary="false" ValidationGroup="Edit" HeaderText="Kayıt güncellerken şu hataları yaptınız:" DisplayMode="BulletList" EnableClientScript="true" runat="server" />


                    </div>
                </div>
            </div>
        </div>


    </div>


</asp:Content>


