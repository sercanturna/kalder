﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class modules : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();

        string IsActive;


        protected void Page_Load(object sender, EventArgs e)
        {
          

            string islem = Request.QueryString["action"];



            //if (islem == "suspend")
            //{
            //    db.guncelle("Update ModuleSegmentSectionReleation set IsActive=0 where ModuleId=" + act_ModuleId + " and SectionId=" + act_SectionId + " and SegmentId=" + act_SegmentId, UpdatePanel1);
            //    Response.Redirect("modules.aspx");


            //}
            //else if (islem == "unsuspend")
            //{
            //    db.guncelle("Update ModuleSegmentSectionReleation set IsActive=1 where ModuleId=" + act_ModuleId + " and SectionId=" + act_SectionId + " and SegmentId=" + act_SegmentId, UpdatePanel1);
            //    Response.Redirect("modules.aspx");
            //}


            if (!IsPostBack)
            {
                getSegment();
                getSections();
            }

            //ListItem li = dropSegment.Items.FindByText("Anasayfa");
            //if(li !=null)
            //li.Selected = true;

            //if (act_SectionId == null && act_SegmentId == null)
            //{
            //   // getModules(dropSegment.SelectedValue, dropSections.SelectedValue);
            //}
            //else
            //{
            //    // getModules(act_SegmentId, act_SectionId);
            //}

        }



        protected void rptListe_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            LinkButton lnkAction = (LinkButton)e.Item.FindControl("lnkAction");
            LinkButton lnkEditModule = (LinkButton)e.Item.FindControl("lnkEditModule");
            lnkEditModule.Visible = false;

            string ModuleId = DataBinder.Eval(e.Item.DataItem, "ModuleId").ToString();
            string SectionId = DataBinder.Eval(e.Item.DataItem, "SectionId").ToString();
            string SegmentId = DataBinder.Eval(e.Item.DataItem, "SegmentId").ToString();

            IsActive = db.GetDataCell("select IsActive from ModuleSegmentSectionReleation where ModuleId=" + ModuleId + " and SegmentId=" + SegmentId + " and SectionId=" + SectionId);

            if (IsActive == "True")
            {
                lnkAction.CommandName = "Suspend";
                // lnkAction.PostBackUrl = "modules.aspx?action=suspend&ModuleId=" + ModuleId +"&SegmentId="+SegmentId+"&SectionId="+SectionId;
                lnkAction.Text = "<i class=\"btn-icon-only icon-pause\"></i>";
                lnkAction.CssClass = "btn btn-small";
                lnkAction.ToolTip = "Yayından Kaldır";
            }

            else
            {
                lnkAction.CommandName = "Unsuspend";
                
                lnkAction.Text = "<i class=\"btn-icon-only icon-ok \"></i>";
                lnkAction.CssClass = "btn btn-small btn-warning";
                lnkAction.ToolTip = "Yayınla";
            }

            
            
            if (ModuleId == "15")
            {
                lnkEditModule.Visible = true;
                lnkEditModule.Text = "<i class=\"btn-icon-only icon-pencil \"></i>";
                lnkEditModule.CssClass = "btn btn-small";
                lnkEditModule.ToolTip = "Düzenle";
                lnkEditModule.PostBackUrl = "Edit_Welcome.aspx";
            }

            if (ModuleId == "3")
            {
                lnkEditModule.Visible = true;
                lnkEditModule.Text = "<i class=\"btn-icon-only icon-pencil \"></i>";
                lnkEditModule.CssClass = "btn btn-small";
                lnkEditModule.ToolTip = "Düzenle";
                lnkEditModule.PostBackUrl = "edit_currency.aspx";
            }

            if (ModuleId == "7")
            {
                lnkEditModule.Visible = true;
                lnkEditModule.Text = "<i class=\"btn-icon-only icon-pencil \"></i>";
                lnkEditModule.CssClass = "btn btn-small";
                lnkEditModule.ToolTip = "Düzenle";
                lnkEditModule.PostBackUrl = "boxes.aspx";
            }
            if (ModuleId == "9")
            {
                lnkEditModule.Visible = true;
                lnkEditModule.Text = "<i class=\"btn-icon-only icon-pencil \"></i>";
                lnkEditModule.CssClass = "btn btn-small";
                lnkEditModule.ToolTip = "Düzenle";
                lnkEditModule.PostBackUrl = "references.aspx";
            }
            if (ModuleId == "6")
            {
                lnkEditModule.Visible = true;
                lnkEditModule.Text = "<i class=\"btn-icon-only icon-pencil \"></i>";
                lnkEditModule.CssClass = "btn btn-small";
                lnkEditModule.ToolTip = "Düzenle";
                lnkEditModule.PostBackUrl = "sliders.aspx";
            }
          

        }


        public void getSegment()
        {
            DataTable dt = db.GetDataTable("select * from ModuleSegments");
            dropSegment.DataSource = dt;
            dropSegment.DataValueField = dt.Columns["SegmentId"].ColumnName.ToString();
            dropSegment.DataTextField = dt.Columns["SegmentName"].ColumnName.ToString();
            dropSegment.DataBind();

        }

        public void getSections()
        {
            DataTable dt = db.GetDataTable("select * from ModuleSections");
            dropSections.DataSource = dt;
            dropSections.DataValueField = dt.Columns["SectionId"].ColumnName.ToString();
            dropSections.DataTextField = dt.Columns["SectionName"].ColumnName.ToString();
            dropSections.DataBind();

        }

        public void getModules(string SectionId, string SegmentId)
        {
            string sorgu = "select m.ModuleId,m.ModuleName,m.ModuleContent,m.IsActive, mss.IsActive, ms.SectionName, mss.OrderNo, mss.SectionId, mss.SegmentId, msg.SegmentName from Modules as m " +
                "inner join ModuleSegmentSectionReleation as mss on m.ModuleId=mss.ModuleId " +
                "inner join ModuleSections as ms on mss.SectionId=ms.SectionId " +
                "inner join ModuleSegments as msg on mss.SegmentId=msg.SegmentId " +
                "where mss.SegmentId=" + SegmentId + " and mss.SectionId=" + SectionId + " and m.IsActive=1 order by mss.OrderNo";
            DataTable dt = db.GetDataTable(sorgu);
            rptListe.DataSource = dt;
            rptListe.DataBind();

            // Response.Write(sorgu);

        }


        protected void dropSegment_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dropSegment.SelectedItem.Text == "Anasayfa")
                anasayfakonum.Visible = true;
            else
                anasayfakonum.Visible = false;

            // getSections();
        }


        protected void lnkGetModules_Click(object sender, EventArgs e)
        {
            getModules(dropSections.SelectedValue, dropSegment.SelectedValue);
        }


        protected void rptListe_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

            string[] arguments = e.CommandArgument.ToString().Split(new char[] { ',' });
            string act_ModuleId = arguments[0];
            string act_SectionId = arguments[1];
            string act_SegmentId = arguments[2];

            getModules(act_SectionId, act_SegmentId);

            if (e.CommandName == "Suspend")
            {
                LinkButton button = e.CommandSource as LinkButton;

                //UpdatePanel upLinks = (UpdatePanel)e.Item.FindControl("uplnkUpdate");

                ScriptManager sm = ScriptManager.GetCurrent(this.Page);
                sm.RegisterAsyncPostBackControl(button);

                string sorgu = "Update ModuleSegmentSectionReleation set IsActive=0 where ModuleId=" + act_ModuleId + " and SectionId=" + act_SectionId + " and SegmentId=" + act_SegmentId;
                db.guncelle(sorgu, UpdatePanel1);

                getModules(act_SectionId, act_SegmentId);

                UpdatePanel1.Update();
            }

            if (e.CommandName == "Unsuspend")
            {
                LinkButton button = e.CommandSource as LinkButton;

                //UpdatePanel upLinks = (UpdatePanel)e.Item.FindControl("uplnkUpdate");

                ScriptManager sm = ScriptManager.GetCurrent(this.Page);
                sm.RegisterAsyncPostBackControl(button);

                string sorgu = "Update ModuleSegmentSectionReleation set IsActive=1 where ModuleId=" + act_ModuleId + " and SectionId=" + act_SectionId + " and SegmentId=" + act_SegmentId;
                db.guncelle(sorgu, UpdatePanel1);


                getModules(act_SectionId, act_SegmentId);
                UpdatePanel1.Update();
            }


         

        }
    }
}