﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="pages.aspx.cs" Inherits="Kalder.Adminv2.pages" %>
 
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    
    <%@ Register Assembly="CollectionPager" Namespace="SiteUtils" TagPrefix="cc1" %>

    <script type="text/javascript">
        $(function () {
            var $allCheckbox = $('.allCheckbox :checkbox');
            var $checkboxes = $('.singleCheckbox :checkbox');
            $allCheckbox.change(function () {
                if ($allCheckbox.is(':checked')) {
                    $checkboxes.attr('checked', 'checked');
                }
                else {
                    $checkboxes.removeAttr('checked');
                }
            });
            $checkboxes.change(function () {
                if ($checkboxes.not(':checked').length) {
                    $allCheckbox.removeAttr('checked');
                }
                else {
                    $allCheckbox.attr('checked', 'checked');
                }
            });
        });

        
        $("#sayfalar").addClass("active");


  $(function () {
      $('#ProductCat').DataTable({

          
          dom: 'Bfrtip',
          buttons: [
              'copyHtml5'
              
          ],

          
          "language": {
              "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Turkish.json"
          }
      });
  });


    </script>
    <style>
        th.allCheckbox {
            width: 20px;
        }

            td.singleCheckbox input, th.allCheckbox input {
                width: 20px !important;
            }
    </style>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

    <div class="row">

        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-paper-clip"></i>
                    <h3>Sayfalar</h3>
                    <asp:LinkButton ID="btnDeleteAll" OnClick="btnDeleteAll_Onclick" runat="server" CssClass="btn right" BorderStyle="Solid" BorderColor="#606060" BorderWidth="1" ToolTip="Sil" CausesValidation="False" Text="x Sil" OnClientClick="return confirm('Bu sayfayı silmek istediğinize emin misiniz? \nEğer bir üst sayfa ise tüm alt sayfalar silinecek!');"></asp:LinkButton>
                    <a href="add_static_pages.aspx" class="btn btn-success btn-primary right"> + Ekle </a>
                    
                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                    <div class="EU_TableScroll">
                        <asp:UpdatePanel ID="upGrid" runat="server">
                            <ContentTemplate>

                                <table id="ProductCat" class="table table-bordered table-striped blank" cellspacing="0" border="1" style="border-collapse: collapse;">
                                   <asp:Repeater ID="rpt_News" runat="server">
                                       <HeaderTemplate>
                                           <thead>
                                     <tr >
                                        <th scope="col" class="allCheckbox"><asp:CheckBox ID="allCheckbox1"  runat="server" /></th>
                                        <th scope="col">ID</th>
                                        <th scope="col">Kategori</th>
                                        <th scope="col">Başlık</th>
                                      
                                        <th scope="col" class="center">Durum</th>
                                        <th scope="col" class="center">SıraNo</th>
                                        <th scope="col" class="center">Okunma</th>
                                        <th scope="col">Eklenme Tarihi</th>
                                        <th scope="col">İşlem</th>
                                    </tr>
                                           </thead>
                                           <tbody>
                                    </HeaderTemplate>
                                        <ItemTemplate>
                                            
                                            <tr>
                                                <td class="singleCheckbox">
                                                    <asp:CheckBox ID="chkContainer" runat="server"  />
                                                    <asp:HiddenField ID="hbItem" runat="server" Value='<%# Eval("PageId") %>' />
                                                </td>

                                                 <td><%#Eval ("PageId") %></td>
                                                 <td><%#Eval ("SayfaAdi") %></td>
                                                 <td><%#Eval ("Meta_Title") %></td>
                                               
                                                 <td class="center"><%# Eval("PageIsActive") == "True" ? Eval("PageIsActive") : Eval("PageIsActive").ToString().Replace("True", "<img src=\"img/play.png\" />").Replace("False", "<img src=\"img/pause.png\" />")%></td>
                                                 <td class="center"><%#Eval("OrderNumber") %></td>
                                                <td class="center"><%#Eval ("Hit") %></td>
                                                 <td><%#Eval ("CreatedDate") %></td>
                                                 <td><a href="edit_pages.aspx?pId=<%#Eval("PageId") %>" class="btn btn-small"><i class="btn-icon-only icon-pencil"></i></a></td>
                                            </tr>
                                        </ItemTemplate>
                                       <FooterTemplate>
                                           </tbody>
                                           
                                       </FooterTemplate>
                                    </asp:Repeater>
                                  
                                    <!--
                                    <tr>
                                        <td colspan="9" style="text-align:center; padding:20px;" ><asp:Label CssClass="alert" ID="ltrlSonuc" runat="server" Text="Henüz bir ürün kategorisi oluşturulmamış."></asp:Label></td>
                                    </tr>
                                    -->
                                     
                                </table>
                              <section id="paginations">
                                <div class="sayfalama">
                                  <cc1:CollectionPager ID="CollectionPager1" runat="server" BackText=" « Önceki" 
                                    FirstText="İlk" LabelText="" LastText="Son" NextText="Sonraki »" 
                                    PageNumbersDisplay="Numbers" ResultsFormat="Sayfalar {0} {1} (Toplam:{2})"
                                    PageSize="10" SectionPadding="5" BackNextDisplay="Buttons" BackNextLocation="Split" 
                                    MaxPages="500" PageNumbersSeparator="" Visible="false"></cc1:CollectionPager>
                         </div>
                                  </section>
                                        </ContentTemplate>
                        </asp:UpdatePanel>

                        
                        
                    </div>
                </div>
            </div>
        </div>


    </div>


</asp:Content>


