﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="edit_event.aspx.cs" Inherits="Kalder.Adminv2.edit_event" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<%@ Register Assembly="CKFinder" Namespace="CKFinder" TagPrefix="CKFinder" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
      <style type="text/css">
        .style1 {
            width: 100%;
        }

        .style2 {
            width: 183px;
            font-family: Verdana;
            font-size: 13px;
            font-weight: bold;
            color: #646464;
        }

        td.label {
            font-size: 12px;
            line-height: 1.4;
            padding: 10px;
            border: 1px dotted #e7e0d9;
        }

        .uyari {
            clear: both;
            border: 5px solid #2a4066;
            font-size: large;
            width: 380px;
            line-height: 2.0;
            background-color: #677690;
            color: White;
        }

            .uyari td {
                clear: both;
                vertical-align: middle;
                margin: 0px;
                padding: 0px;
            }

            .uyari img {
                display: block;
            }

        .cal {
            width: 180px;
        }

        input:focus,
        textarea:focus,
        select:focus {
            outline: none;
        }

        .drop {
            padding: 5px;
            font-size: 14px;
            font-weight: bold;
            color: #2a4066;
            cursor: pointer;
        }
    </style>


         <link href="./js/tags/jquery.tagsinput.css" rel="stylesheet" />
    <script src="./js/tags/jquery.tagsinput.js"></script>
     
    
    <script src="js/validation/jquery.validationEngine-tr.js"></script>
    <script src="js/validation/jquery.validationEngine.js"></script>
   <link href="js/validation/validationEngine.jquery.css" rel="stylesheet" /> 


    <script src="../ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="../ckfinder/ckfinder.js" type="text/javascript"></script>
    <script type="text/javascript">
        window.onload = function () {
            var editor = CKEDITOR.replace('<% = txtDetay.ClientID %>');
            CKFinder.setupCKEditor(editor, '../ckfinder');
        };

        function DescChar(val) {
            var len = val.value.length;
            if (len >= 201) {
                val.value = val.value.substring(0, 200);
            } else {
                $('#desc').text(200 - len + " Karakter");
            }
        };

        function TitleChar(val) {
            var len = val.value.length;
            if (len >= 70) {
                val.value = val.value.substring(0, 70);
            } else {
                $('#title').text(70 - len + " Karakter");
            }
        };




        $(function () {
            $('#content_tags').tagsInput({
                width: 'auto',
            });
        });

</script>
    <script type="text/javascript">
        $(document).ready(function () {
           // $("#sayfalar").addClass("active");
            $("#form1").validationEngine('attach', { promptPosition: "topRight", scroll: true });
        });
</script>
</asp:Content>


<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="server">
    <asp:ScriptManager ID="ToolkitScriptManager1" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true"></asp:ScriptManager>
    <div class="row">

        <div class="span12">
            <div class="widget stacked">
                <div class="widget-header">
                    <i class="icon-pencil"></i>
                    <h3>Etkinlik Düzenleme</h3>

                </div>
                <!-- /widget-header -->
                <div class="widget-content">
                    <div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#genel" data-toggle="tab">Genel Bilgiler</a></li>
                                  <li><a href="#galeri" data-toggle="tab">Foto Galeri</a></li>  
                            <li class=""><a href="#veriler" data-toggle="tab">SEO</a></li>

                        </ul>
                        <br />

                        <div class="tab-content">
                            <div class="tab-pane active" id="genel">
                                <div id="edit-genel" class="form-horizontal">
                                    <p>
                                        Bu bölümden sadece mevcut Etkinliği düzenleyebilirsiniz, etkinlik eklemek veya silmek için lütfen <a href="event_management.aspx">Etkinlik Yönetimi</a> sayfasına gidin.
                                    </p>
                                    <hr />

                                    <div class="control-group">
                                        <label class="control-label" for="dropOrg">Etkinlik Sahibi</label>
                                        <div class="controls">
                                            <asp:DropDownList ID="dropOrg" runat="server" CssClass="validate[required]">
                                                <asp:ListItem>Genel Merkez</asp:ListItem>
                                                <asp:ListItem>KalDer Ankara Şubesi</asp:ListItem>
                                                <asp:ListItem>KalDer Bursa Şubesi</asp:ListItem>
                                                <asp:ListItem>KalDer Eskişehir Şubesi</asp:ListItem>
                                                <asp:ListItem>KalDer İzmir Şubesi</asp:ListItem>
                                                <asp:ListItem>KalDer Kayseri Temsilciliği</asp:ListItem>
                                                <asp:ListItem>KalDer Trakya Temsilciliği</asp:ListItem>
                                                <asp:ListItem>KalDer Doğu Marmara Gönüllü Temsilciliği</asp:ListItem>
                                                <asp:ListItem>KalDer Batı Akdeniz Gönüllü Temsilciliği</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <!-- /controls -->
                                    </div>

                                    <div class="control-group">
                                        <label class="control-label" for="txtKategoriAdi">Başlangıç Tarihi</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txt_Etkinlik_Tarihi" runat="server" CssClass="validate[required]" ></asp:TextBox>
                                            <asp:CalendarExtender ID="txt_Etkinlik_Tarihi_CalendarExtender" runat="server" TargetControlID="txt_Etkinlik_Tarihi" CssClass="uyari cal"></asp:CalendarExtender>
                                        </div>
                                        <!-- /controls -->
                                    </div>
                                    <!-- /control-group -->


                                    <div class="control-group">
                                        <label class="control-label" for="txt_Etkinlik_Bitis_Tarihi">Bitiş Tarihi</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txt_Etkinlik_Bitis_Tarihi" runat="server" CssClass="validate[required]"></asp:TextBox>
                                            <asp:CalendarExtender ID="txt_Etkinlik_Bitis_Tarihi_CalendarExtender" runat="server" TargetControlID="txt_Etkinlik_Bitis_Tarihi" CssClass="uyari cal"></asp:CalendarExtender>
                                        </div>
                                        <!-- /controls -->
                                    </div>
                                    <!-- /control-group -->


                                 

                                    <div class="control-group">
                                        <label class="control-label" for="txt_Etkinlik_Yeri">Yer / Mekan</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txt_Etkinlik_Yeri" runat="server"></asp:TextBox>
                                        </div>
                                        <!-- /controls -->
                                    </div>
                                    <!-- /control-group -->


                                    <div class="control-group">
                                        <label class="control-label" for="txtEtkinlikAdi">Etkinlik Adı</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtEtkinlikAdi" runat="server" CssClass="validate[required]"></asp:TextBox>
                                        </div>
                                        <!-- /controls -->
                                    </div>
                                    <!-- /control-group -->

                                    <div class="control-group">
                                        <label class="control-label" for="txtBrosur">Broşür Resmi</label>
                                        <div class="controls">
                                            
                                            <asp:FileUpload ID="fuBrosur" runat="server"></asp:FileUpload>
                                        </div>
                                        <!-- /controls -->
                                        <div style="position:absolute; right:60px; top:200px;">
                                        <span style="border:0px solid #f00; position:absolute; z-index:1000; right:-20px; margin-top:-10px;"><asp:ImageButton ID="btnResimTemizle" runat="server" OnClick="btnResimTemizle_Click" ImageUrl="~/images/icon_close.png" /></span>
                                        <asp:Image ID="imgBrosur" runat="server" ImageUrl="~/upload/Etkinlikler/NoBrochure.jpg" />
                                            </div>
                                    </div>
                                    <!-- /control-group -->

                                    <div class="control-group">
                                        <label class="control-label" for="txtVideo">Video Embed (Youtube)</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtVideo" TextMode="MultiLine" runat="server"></asp:TextBox>
                                        </div>
                                        <!-- /controls -->
                                    </div>
                                    <!-- /control-group -->

                                    <div class="control-group">
                                        <label class="control-label" for="txtDetay">Etkinlik İçeriği</label>
                                        <div class="controls">
                                            <CKEditor:CKEditorControl ID="txtDetay" runat="server" Width="630px" Height="300px" CssClass="validate[required]">
                                            </CKEditor:CKEditorControl>
                                            <p class="help-block">* Sayfa içeriğinin tamamını bu alanda yapılandırın.</p>
                                        </div>

                                        <!-- /controls -->
                                    </div>
                                    <!-- /control-group -->


                                <div class="control-group">
                                    <label class="control-label" for="chkOnay" >Durum</label>
                                    <div class="controls">
                                         <asp:DropDownList ID="drpDurum" runat="server" CssClass="validate[required]">
                                             <asp:ListItem Value="0">-Seçiniz-</asp:ListItem>
                                             <asp:ListItem Value="1">Aktif</asp:ListItem>
                                             <asp:ListItem Value="0">Pasif</asp:ListItem>
                                         </asp:DropDownList>
                                        <p class="help-block">* Durumu Pasif yaparsanız haber sitede gözükmez.</p>
                                    </div>
        
                                    <!-- /controls -->
                                </div>
                                <!-- /control-group -->
 

                                <div class="control-group">
                                    <label class="control-label" for="txtSiraNo">Sıra No</label>
                                    <div class="controls">
                                    <asp:TextBox ID="txtSiraNo" runat="server"></asp:TextBox>
                                        <p class="help-block">* Haberin sitedeki sırasını belirtin. Eğer istemiyorsanız boş bırakın</p>
                                    </div>
        
                                    <!-- /controls -->
                                </div>
                                <!-- /control-group -->

                                </div>
                            </div>

                            <div class="tab-pane" id="galeri">
                                <div id="Div2" class="form-horizontal">	
                                <div class="control-group">
                                    <label class="control-label" for="drpGaleri">Foto Galeri</label>
                                    <div class="controls">
                                         <asp:DropDownList ID="drpGaleri" runat="server"></asp:DropDownList>
                                        <p class="help-block">* Sayfaya ait fotoğraf albümü seçin. Yeni bir tane oluşturmak için lütfen <a href="gallery_categories.aspx" target="_blank"> Albüm Yönetimi </a>sayfasına gidin.</p>
                                    </div>
        
                                    <!-- /controls -->
                                </div>
                                <!-- /control-group -->
                            </div>
                            </div>

                            <div class="tab-pane" id="veriler">
                                <div id="edit-profile2" class="form-horizontal">

                                    <div class="control-group">
                                        <label class="control-label" for="vergidaire">Seo URL</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtSeoUrl" TextMode="SingleLine" CssClass="input-xlarge" runat="server"></asp:TextBox><div class="char" id="Div1"></div>
                                            <p class="help-block">* Google ve diğer arama motorları için adres çubuğunda gözükecek kategoriniz için bir URL belirleyin.</p>
                                        </div>
                                        <!-- /controls -->
                                    </div>
                                    <!-- /control-group -->


                                    <div class="control-group">
                                        <label class="control-label" for="vergidaire">Meta Title</label>
                                        <div class="controls">

                                            <asp:TextBox ID="txtTitle" TextMode="SingleLine" CssClass="input-xlarge" onkeyup="TitleChar(this)" runat="server"></asp:TextBox><div class="char" id="title"></div>
                                            <p class="help-block">* Google ve diğer arama motorları için bir başlık yazın.</p>
                                        </div>
                                        <!-- /controls -->
                                    </div>
                                    <!-- /control-group -->

                                    <div class="control-group">
                                        <label class="control-label" for="vergidaire">Meta Description</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtDescription" TextMode="MultiLine" CssClass="input-xlarge" onkeyup="DescChar(this)" runat="server"></asp:TextBox><div class="char" id="desc"></div>
                                            <p class="help-block">* Google ve diğer arama motorları için bir açıklama yazın.</p>
                                        </div>
                                        <!-- /controls -->
                                    </div>
                                    <!-- /control-group -->

                                    <div class="control-group">
                                        <label class="control-label" for="vergidaire">Meta Keywords</label>
                                        <div class="controls">
                                            <asp:TextBox ID="tags" CssClass="input-medium" runat="server"></asp:TextBox>
                                            <p class="help-block">* Google ve diğer arama motorları için anahtar kelimeler yazın.
                                                <br />
                                                Kelimeler arasına virgül koyarak yada Enter tuşuna basarak geçiş yapabilirsiniz.</p>
                                        </div>
                                        <!-- /controls -->
                                    </div>
                                    <!-- /control-group -->


                                    <div class="control-group">
                                        <label class="control-label" for="vergidaire">Okunma Sayısı</label>
                                        <div class="controls">
                                            <asp:TextBox ID="txtHit" CssClass="input-medium" runat="server"></asp:TextBox>
                                            <p class="help-block">* Sayfanın sitenizde kaç kere okunduğunu gösteren değerdir.</p>
                                        </div>
                                        <!-- /controls -->
                                    </div>
                                    <!-- /control-group -->


                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <asp:Button ID="btnGonder" runat="server" Text="Güncelle" CssClass="btn btn-primary" OnClick="btnGonder_Click" />
                            <asp:Button ID="btnTemizle" runat="server" CssClass="btn" OnClick="btnTemizle_Click" Text="İptal" />

                        </div>
                        <!-- /form-actions -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

