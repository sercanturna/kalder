﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class edit_banner : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string sId;
        string resimadi;
        protected void Page_Load(object sender, EventArgs e)
        {
          
            sId = Request.QueryString["sId"];

            if (!IsPostBack)
            {
                DataRow dr = db.GetDataRow("select * from Banners where BannerId=" + sId);
                txtLink.Text = dr["Link"].ToString();
                txtSiraNo.Text = dr["OrderNo"].ToString();
                txtTitle.Text = dr["Title"].ToString();

                dropTarget.Items.FindByValue(dr["LinkTarget"].ToString()).Selected = true;

                string durum = dr["IsActive"].ToString();
                if (durum == "True")
                    drpDurum.Items.FindByValue("1").Selected = true;
                else
                    drpDurum.Items.FindByValue("0").Selected = true;


                imgURL.ImageUrl = "../upload/BannerResimleri/" + dr["ImageURL"].ToString();

            }
        }




        protected void btn_SlaytGuncelle_Click(object sender, EventArgs e)
        {

            string uzanti = "";
            resimadi = db.GetDataCell("Select ImageURL from Banners where BannerId=" + sId);

            if (fuResim.HasFile)
            {
                FileInfo eskiresim = new FileInfo(Server.MapPath("~/upload/BannerResimleri/" + resimadi));
                eskiresim.Delete();

                uzanti = Path.GetExtension(fuResim.PostedFile.FileName);
                resimadi = Ayarlar.OzetCek(Ayarlar.UrlSeo(txtTitle.Text), 10).Trim().Replace(".","") + DateTime.Now.Millisecond + uzanti;
                fuResim.SaveAs(Server.MapPath("~/upload/sahte/" + resimadi));

                Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + resimadi));
                resim = Ayarlar.ResimBoyutlandir(resim, 970);

                resim.Save(Server.MapPath("~/upload/BannerResimleri/" + resimadi), resim.RawFormat);
                // Resmi önce SlaytResimleri klasörüne kayıt ediyoruz.

                //resim = Ayarlar.ResimBoyutlandir(resim, 220);
                //resim.Save(Server.MapPath("~/upload/SlaytResimleri/220/" + resimadi), ImageFormat.Jpeg);
                //// Resmi sonrada 220 klasörüne kayıt ediyoruz.

                FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + resimadi));
                temp.Delete();
            }


            SqlConnection baglanti = db.baglan();
            SqlCommand cmdGuncelle = new SqlCommand("SET language turkish; Update Banners set ImageURL=@ImageURL,Title=@Title,IsActive=@IsActive,OrderNo=@OrderNo,Link=@Link,LinkTarget=@LinkTarget," +
           "Date=@Date Where BannerId=" + sId, baglanti);

            cmdGuncelle.Parameters.AddWithValue("ImageURL", resimadi);
            cmdGuncelle.Parameters.AddWithValue("Title", txtTitle.Text);
            cmdGuncelle.Parameters.AddWithValue("IsActive", drpDurum.SelectedValue);
            cmdGuncelle.Parameters.AddWithValue("OrderNo", txtSiraNo.Text);
            cmdGuncelle.Parameters.AddWithValue("Link", txtLink.Text);
            cmdGuncelle.Parameters.AddWithValue("LinkTarget", dropTarget.SelectedValue);
            cmdGuncelle.Parameters.AddWithValue("Date", DateTime.Now.ToString());


            int sonuc = cmdGuncelle.ExecuteNonQuery();

            if (sonuc > 0)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                Response.Redirect("banners.aspx");
            }
            else
                ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);

        }
    }
}