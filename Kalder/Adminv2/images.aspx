﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Adminv2/Admin.Master" AutoEventWireup="true" CodeBehind="images.aspx.cs" Inherits="Kalder.Adminv2.images" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
       <script type="text/javascript" src="./js/demo/gallery.js"></script>
    <style>
        .gallery-container li img {
            height: 116px;
            
        }

        .gallery-container li {
            text-align: center;
        }

        input.imgText {
            width: 80%;
            display: block;
            margin: 0;
            margin: 5px 0 5px 14px;
            padding: 3px;
            font-size: 12px;
        }

        .update-icon {
            width: 20px !important;
            height: 20px !important;
        }


    </style>
    <script src="js/multi.js"></script>

    <script type="text/javascript">
        $("#fotogaleri").addClass("active");
    </script>
 
 

</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="content" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
    <script type="text/javascript">
        var updateProgress = null;

        function postbackButtonClick() {

            updateProgress = $find("<%= UpProg.ClientID %>");
            window.setTimeout("updateProgress.set_visible(true)", updateProgress.get_displayAfter());
            return true;
        }


    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="row">

                <div class="span8">
                    <div class="widget stacked">
                        <div class="widget-header">
                            <i class="icon-picture"></i>
                            <h3>
                                <asp:Literal ID="ltrlAlbumAdi" runat="server"></asp:Literal></h3>
                        </div>
                        <!-- /widget-header -->
                        <div id="icerik" class="widget-content">
                            <asp:Literal ID="ltrlUyari" runat="server" Visible="false"></asp:Literal>
                            <ul class="gallery-container">

                                <asp:Repeater ID="rptResimler" runat="server" OnItemCommand="rptResimler_ItemCommand" OnItemDataBound="rptResimler_OnItemDataBound">
                                    <ItemTemplate>
                                        <li>
                                            <a href="../upload/Galeri/<%#Eval("KategoriAdi") %>/<%#Eval("ResimAdi") %>" class="ui-lightbox">
                                                <img src="../upload/Galeri/<%#Eval("KategoriAdi") %>/<%#Eval("ResimAdi") %>" />
                                            </a>
                                            <a href="../upload/Galeri/<%#Eval("KategoriAdi") %>/<%#Eval("ResimAdi") %>" class="preview"></a>
                                            <div>
                                                <asp:TextBox ID="txtDesc" runat="server" ToolTip="Resim Açıklaması" CssClass="imgText"></asp:TextBox></div>
                                            <span>
                                                <asp:ImageButton ID="lnkAction" runat="server" CommandArgument='<%#Eval ("ResimId") %>' CssClass="update-icon"></asp:ImageButton></span>
                                            <span>
                                                <asp:LinkButton ID="lnkUpdate" CommandName="update" CommandArgument='<%#Eval ("ResimId") %>' runat="server" CssClass="btn btn-small btn-primary" Text="Güncelle"></asp:LinkButton></span>
                                            <asp:LinkButton ID="lnkDelete" CommandName="del" CommandArgument='<%#Eval ("ResimId") %>' runat="server" CssClass="btn btn-small" Text="Sil" OnClientClick="return confirm('Bu resmi silmek istediğinize emin misiniz?');"></asp:LinkButton></span>
                               
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>



                        </div>
                    </div>
                </div>

                <div class="span4">
                    <div class="widget stacked widget-box">

                        <div class="widget-header">
                            <i class="icon-plus"></i>
                            <h3>Resim Yükleme</h3>
                        </div>
                        <!-- /widget-header -->

                        <div class="widget-content">
                            <ul class="insertImage">
                                <li>
                                    <asp:DropDownList ID="dropKat" runat="server" OnSelectedIndexChanged="dropKat_OnSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></li>
                                <li>
                                    <asp:FileUpload ID="fuResim" CssClass="multi" runat="server" /></li>
                                <li>&nbsp</li>
                                <li>
                                    <asp:LinkButton UseSubmitBehavior="True" OnClientClick="return postbackButtonClick();" ClientIDMode="Static" ID="lnkResimEkle" OnClick="lnkResimEkle_Onclick" CssClass="btn btn-primary" runat="server" Text="Resimleri Yükle"></asp:LinkButton></li>



                            </ul>
                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget-box -->


                      <div class="widget stacked widget-box">

                        <div class="widget-header">
                            <i class="icon-adjust"></i>
                            <h3>Sayfa Resim Sayısı</h3>
                        </div>
                        <!-- /widget-header -->

                        <div class="widget-content">
                            <p>Bir sayfada kaç resim gözüksün?</p>
                            <asp:TextBox ID="txtResimSayisi" runat="server" CssClass="left" Text="16"></asp:TextBox>
                            <asp:Button ID="btnResimSayisiGuncelle" OnClick="btnResimSayisiGuncelle_Click" runat="server" Text ="Uygula" CssClass="btn btn-primary left" style="vertical-align:top" /><br />
                            <i>Değer girmezseniz tüm resimler aynı sayfada gözükür.</i>
                        </div>
                        <!-- /widget-content -->

                    </div>
                    <!-- /widget-box -->

                </div>
                <!-- /span4 -->

                <div class="span4">
                    <div id="progressbar"></div>
                    <!-- /.bar -->
                </div>

            </div>


        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="lnkResimEkle" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress runat="server" ID="UpProg" DynamicLayout="false" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div class="widget stacked widget-box">
                <div class="widget-header">
                    <i class="icon-plus"></i>
                    <h3>Yükleniyor...</h3>
                </div>
                <!-- /widget-header -->

                <div class="widget-content">

                    <div class="progress progress-primary progress-striped active">
                        <div class="bar" style="width: 100%"></div>
                        <!-- /.bar -->
                    </div>
                    <!-- /.progress -->


                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>


