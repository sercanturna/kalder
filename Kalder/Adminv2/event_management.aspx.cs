﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class event_management : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataTable dt = db.GetDataTable("Select * from Events order by BeginDate desc");

                CollectionPager1.DataSource = dt.DefaultView;

                CollectionPager1.BindToControl = rpt_News;
                rpt_News.DataSource = CollectionPager1.DataSourcePaged;
                rpt_News.DataBind();
            }
        }


        protected void btnDeleteAll_Onclick(object sender, EventArgs e)
        {
            foreach (RepeaterItem ri in rpt_News.Items)
            {
                CheckBox chk = (CheckBox)ri.FindControl("chkContainer");
                HiddenField hd = (HiddenField)ri.FindControl("hbItem");

                if (chk.Checked)
                {
                    db.cmd("delete from Events where EventId=" + hd.Value);
                }

            }

            Response.Redirect("event_management.aspx");



        }
    }
}