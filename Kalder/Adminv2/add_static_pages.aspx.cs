﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.Adminv2
{
    public partial class add_static_pages : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();

        protected void Page_Load(object sender, EventArgs e)
        {
         
            if (!IsPostBack)
            {
                GetCat();
                getGalleryCategories();
            }
        }

        public void GetCat()
        {
            DataTable dtKat = db.GetPages();


            dropKat.DataSource = dtKat;
            dropKat.DataValueField = dtKat.Columns["PageId"].ColumnName.ToString();
            dropKat.DataTextField = dtKat.Columns["SayfaAdi"].ColumnName.ToString();
            dropKat.DataBind();
            dropKat.Items.Insert(0, new ListItem("– yok –", "0"));
        }


        public void getGalleryCategories()
        {
            DataTable dt = db.GetDataTable("Select * from GaleriKategori");

            drpGaleri.DataSource = dt;
            drpGaleri.DataValueField = dt.Columns["GaleriId"].ColumnName.ToString();
            drpGaleri.DataTextField = dt.Columns["KategoriAdi"].ColumnName.ToString();
            drpGaleri.DataBind();
            drpGaleri.Items.Insert(0, new ListItem("– Yok –", "0"));
        }


        protected void btn_SayfaEkle_Click(object sender, EventArgs e)
        {
            string SeoURL = txtSeoUrl.Text;
            if (SeoURL == "" || SeoURL == null)
                SeoURL = Ayarlar.UrlSeo(txtKategoriAdi.Text);

            string s = txtKategoriAdi.Text;
            if (dropKat.Items.FindByText(s) != null)
            {
                Ayarlar.Alert.Show("Bu sayfayı zaten eklediniz!");
            }
            else
            {
                //save it here...


                if (dropKat.Items.ToString().Contains(s))
                {
                    Ayarlar.Alert.Show("Bu sayfayı zaten eklediniz!");
                }
                else
                {
                    string resimadi = "resim_yok.jpg";
                    string uzanti = "";
                    bool ShowMenu;
                    if (chkMenudeGoster.Checked)
                        ShowMenu = true;
                    else
                        ShowMenu = false;



                    if (fuResim.HasFile)
                    {
                        uzanti = Path.GetExtension(fuResim.PostedFile.FileName);
                        resimadi = Ayarlar.OzetCek(Ayarlar.UrlSeo(txtKategoriAdi.Text), 10).Trim() + "_kategoriresim_" + DateTime.Now.Day + uzanti;
                        fuResim.SaveAs(Server.MapPath("~/upload/sahte/" + resimadi));

                        Bitmap resim = new Bitmap(Server.MapPath("~/upload/sahte/" + resimadi));
                        resim = Ayarlar.ResimBoyutlandir(resim, 770);

                        resim.Save(Server.MapPath("~/upload/SayfaResimleri/big/" + resimadi), ImageFormat.Jpeg);
                        // Resmi önce 760 klasörüne kayıt ediyoruz.

                        resim = Ayarlar.ResimBoyutlandir(resim, 340);
                        resim.Save(Server.MapPath("~/upload/SayfaResimleri/thumb/" + resimadi), ImageFormat.Jpeg);
                        // Resmi sonrada 340 klasörüne kayıt ediyoruz.

                        FileInfo temp = new FileInfo(Server.MapPath("~/upload/sahte/" + resimadi));
                        temp.Delete();
                    }

                    SqlConnection baglanti = db.baglan();
                    SqlCommand cmdKaydet = new SqlCommand("SET language turkish; insert into Pages(" +

                    "ParentPageId," +
                    "PageName," +
                    "PageUrl," +
                    "PageImage," +
                    "Meta_Title," +
                    "Meta_Desc," +
                    "Meta_Keywords," +
                    "PageContent," +
                    "PageIsActive," +
                    "ShowMenu," +
                    "Cols," +
                    "Hit," +
                    "OrderNumber," +
                    "GalleryId," +
                    "CreatedDate) values (" +


                    "@ParentPageId," +
                    "@PageName," +
                    "@PageUrl," +
                    "@PageImage," +
                    "@Meta_Title," +
                    "@Meta_Desc," +
                    "@Meta_Keywords," +
                    "@PageContent," +
                    "@PageIsActive," +
                    "@ShowMenu," +
                    "@Cols," +
                    "@Hit," +
                    "@OrderNumber," +
                    "@GalleryId," +
                    "@CreatedDate) Select Scope_Identity() AS PageId",
                    baglanti);


                    cmdKaydet.Parameters.AddWithValue("ParentPageId", dropKat.SelectedValue);
                    cmdKaydet.Parameters.AddWithValue("PageName", txtKategoriAdi.Text);
                    cmdKaydet.Parameters.AddWithValue("PageUrl", SeoURL);
                    cmdKaydet.Parameters.AddWithValue("PageImage", resimadi);
                    cmdKaydet.Parameters.AddWithValue("Meta_Title", txtTitle.Text);
                    cmdKaydet.Parameters.AddWithValue("Meta_Keywords", tags.Text);
                    cmdKaydet.Parameters.AddWithValue("Meta_Desc", txtDescription.Text);
                    cmdKaydet.Parameters.AddWithValue("PageContent", txtDetay.Text);
                    cmdKaydet.Parameters.AddWithValue("PageIsActive", drpDurum.SelectedValue);
                    cmdKaydet.Parameters.AddWithValue("ShowMenu", ShowMenu);
                    cmdKaydet.Parameters.AddWithValue("Cols", "0");
                    cmdKaydet.Parameters.AddWithValue("Hit", txtHit.Text);
                    cmdKaydet.Parameters.AddWithValue("GalleryId", drpGaleri.SelectedValue);
                    cmdKaydet.Parameters.AddWithValue("CreatedDate", DateTime.Now.ToString());
                    cmdKaydet.Parameters.AddWithValue("OrderNumber", txtSiraNo.Text);

                    SqlParameter insertPrm = new SqlParameter();
                    insertPrm.Direction = ParameterDirection.Output;
                    // ID değerini istediğimiz alan
                    insertPrm.ParameterName = "UrunKatId";
                    insertPrm.Size = 10;

                    cmdKaydet.Parameters.Add(insertPrm);

                    int sonuc = int.Parse(cmdKaydet.ExecuteScalar().ToString());



                    if (sonuc > 0)
                    {
                        // Ayarlar.Alert.result();
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Success();", true);
                        GetCat();
                        Response.Redirect("pages.aspx");
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, GetType(), "Javascript", "Javascript:Error();", true);




                }
            }

        }

        protected void dropKat_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (dropKat.SelectedValue != "0")
            //{
            //    chkMenudeGoster.Enabled = false;
            //    txtSutun.Enabled = false;
            //}
            //else
            //{
            //    chkMenudeGoster.Enabled = true;
            //    txtSutun.Enabled = true;
            //}
        }
    }
}