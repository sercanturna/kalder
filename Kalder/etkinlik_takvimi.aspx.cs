﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder
{
    public partial class etkinlik_takvimi : System.Web.UI.Page
    {
        Takvim Data = new Takvim();
        DateTime[] dates;
        Takvim clsData;
        DataTable dt;
        DataList dl = new DataList();

        DbConnection db = new DbConnection();

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                TakvimLoad();

                GetPolulerEvents();

                rptEventsItems.DataSource = db.GetDataTable("Set language Turkish; Select * from Events where IsActive=1 and GalleryId=0 order by BeginDate asc");
                rptEventsItems.DataBind();
            }
            catch (Exception)
            {
                
                throw;
            }

         
           
        }



        public void GetPolulerEvents()
        {
            rptPopulerEvents.DataSource = db.GetDataTable("Set language Turkish; Select * from Events where IsActive=1 order by BeginDate asc");
            rptPopulerEvents.DataBind();
        }


        #region takvimload
        private void TakvimLoad()
        {
            if (!IsPostBack)
            {
                Calendar1.Attributes.Add("class", "labelLogin");
                Calendar1.Attributes.Add("alt", "");
                clsData = new Takvim();
                dt = clsData.NotesDate();
                Session.Add("dtdata", dt);
            }
            dt = Session["dtdata"] as DataTable;
            dates = new DateTime[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
               // Response.Write(dt.Rows[i][4].ToString());
                //dates[i] = DateTime.Parse(dt.Rows[i][4].ToString(), System.Globalization.CultureInfo.InvariantCulture);
                 dates[i] = Convert.ToDateTime(dt.Rows[i][5].ToString());
            }
        }
        #endregion

        #region Calendar1_DayRender
        protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
        {

            TakvimAyar(e);
            e.Day.IsSelectable = false;
        }
        #endregion

        #region TakvimAyar
        private void TakvimAyar(DayRenderEventArgs e)
        {


            for (int i = 0; i <= dates.GetUpperBound(0); i++)
            {
                /* Diziye eklenen tarih formatı saatli ve dakikalı olduğu için
                 * takvim karşılaştırmaları string ile short date e çevirip yapmak gerekiyor.
                 * Yoksa saatli girilen takvim olaylarını gün ile eşleştirip göstermiyor.
                */
                if (e.Day.Date.ToShortDateString() == dates[i].ToShortDateString())
                {

                    e.Day.IsSelectable = true;
                    // e.Cell.BackColor = System.Drawing.Color.FromName("#eea404");
                    e.Cell.ForeColor = System.Drawing.Color.White;
                    e.Cell.CssClass = "qtip";

                    //Takvim hücresindeki geçerli tarihi bir değişken alıp, SQL tarih formatına çeviriyoruz
                    //SQL sorgusunda bu tarihteki olayları listeliyoruz
                    string sqlTarih = String.Format("{0:yyyy-MM-dd}", e.Day.Date);
                    string sorgu = "Set language Turkish; Select * from Events Where IsActive=1 and BeginDate like '%" + sqlTarih + "%' ";

                    SqlConnection baglan = new SqlConnection(ConfigurationManager.ConnectionStrings["KalderConnectionString"].ConnectionString);
                    baglan.Open();
                    SqlCommand komut = new SqlCommand(sorgu, baglan);
                    SqlDataReader oku;
                    oku = komut.ExecuteReader();
                    string takvimOlay = "";

                    while (oku.Read())
                    {
                        takvimOlay += @"<ul class=""list-unstyled eventlist"">";
                        takvimOlay += "     <li><b>Etkinlik</b>: <a href=\"etkinlikler/" + Ayarlar.UrlSeo(oku["SeoUrl"].ToString()) + "\" " + oku["EventName"].ToString() + ">" + oku["EventName"].ToString() + "</a></li>";
                        takvimOlay += "     <li><b>Etkinlik Sahibi</b>: " + oku["Organizator"].ToString() + "</li>";
                        takvimOlay += "     <li><b>Başlangıç Tarihi</b>: " + oku["BeginDate"].ToString().Substring(0, 10) + "</li>";
                        takvimOlay += "     <li><b>Bitiş Tarihi</b>: " + oku["EndDate"].ToString().Substring(0, 10) + "</li>";
                        // takvimOlay += "   <br/><b>Saat</b>: " + oku["Saat"].ToString().Substring(0, 5);


                        if (String.IsNullOrEmpty(oku["EventPlace"].ToString()))
                            takvimOlay += "<li><b>Yer</b>: Belirtilmemiş </li>";
                        else                       
takvimOlay += "     <li><b>Yer</b>: " + oku["EventPlace"].ToString() + "</li>";

                        takvimOlay += "     <li><hr style=color:#ED9D54; /></li>";
                        takvimOlay += "     <li>" + oku["EventDetail"].ToString() + "</li>";
                        takvimOlay += "</ul>";
                    }
                    e.Cell.Attributes.Add("alt", takvimOlay);
                    baglan.Close();
                    oku.Dispose();
                }

            }


            //burada sayfamiz olusturulurken takvim gününüe ait etkinlik varsa o günü isaretledim
        }
        #endregion

        protected void rptEventsItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {



        }

        protected void rptBreadcrumb_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }
    }
}