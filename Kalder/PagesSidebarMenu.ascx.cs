﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder
{
    public partial class PagesSidebarMenu : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
        string PageId, ParentPageId;

        string PageURL = string.Empty;



        public string PageIDwithURL
        {
            set { PageURL = value; }
            get { return PageURL; }
        }



        protected void Page_Load(object sender, EventArgs e)
        {


            // PageURL = RouteData.Values["PageURL"].ToString();
            PageId = db.GetDataCell("Select PageId from Pages where PageURL='" + PageURL + "'");
            ParentPageId = db.GetDataCell("Select ParentPageId from Pages where PageId='" + PageId + "'");

            GetPageleftMenu(ParentPageId, PageId);
        }

        public void GetPageleftMenu(string ParentPageId, string PageId)
        {
            if (ParentPageId == "0" || ParentPageId == null || ParentPageId == "")
            {
                rpt_leftmenu.DataSource = db.GetDataTable("SELECT aa.PageId, aa.ParentPageId, aa.PageUrl as PageUrl, bb.PageName AS ParentPage, aa.PageName FROM Pages aa LEFT OUTER JOIN Pages bb ON aa.ParentPageId=bb.PageId where aa.ParentPageId=" + PageId + " or bb.PageId=" + ParentPageId + " and aa.PageIsActive=1 order by aa.OrderNumber");
                //rpt_leftmenu.DataSource = db.GetDataTable("SELECT aa.PageId, aa.ParentPageId, aa.PageUrl as PageUrl, bb.PageName AS ParentPage, aa.PageName FROM Pages aa LEFT OUTER JOIN Pages bb ON aa.ParentPageId=bb.PageId where aa.ParentPageId=" + PageId + " and aa.PageIsActive=1 order by aa.OrderNumber");
                rpt_leftmenu.DataBind();
            }
            else
            {
                rpt_leftmenu.DataSource = db.GetDataTable("SELECT aa.PageId, aa.ParentPageId, aa.PageUrl as PageUrl, bb.PageName AS ParentPage, aa.PageName FROM Pages aa LEFT OUTER JOIN Pages bb ON aa.ParentPageId=bb.PageId where bb.ParentPageId=" + ParentPageId + " or bb.PageId=" + ParentPageId + " and aa.PageIsActive=1 order by aa.OrderNumber");
                rpt_leftmenu.DataBind();
            }
        }






        protected void rpt_leftmenu_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                Literal ltrlLeftMenuHeader = (Literal)e.Item.FindControl("ltrlLeftMenuHeader");

                DataSet ds = db.GetDataSet("SELECT aa.PageId, aa.ParentPageId, aa.PageUrl as PageUrl, bb.PageName AS ParentPage, aa.PageName FROM Pages aa LEFT OUTER JOIN Pages bb ON aa.ParentPageId=bb.PageId where aa.ParentPageId=" + PageId + "or bb.PageId=" + ParentPageId);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ltrlLeftMenuHeader.Text = "<li class='has-sub active'><a href='" + dr["ParentPage"].ToString() + "'><span>" + dr["ParentPage"].ToString() + "</span></a><ul>";
                }

            }

        }

    }
}