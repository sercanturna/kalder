﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactDetails.ascx.cs" Inherits="Kalder.include.ContactDetails" %>
<div class="colums">
            	<h2 class="h2_bg"><span class="icon"></span> BİZE ULAŞIN</h2>
            	<div class="image" style=""><img src="images/contact2.png" /></div>
            	<div class="contact_container">
                <div class="company_name"><h3><asp:Literal ID="ltrlCompany" runat="server"></asp:Literal></h3></div>
                <table>
                <tr>
                <td>Adres</td>
                <td class="sep">:</td>
                <td><asp:Literal ID="ltrlAdress" runat="server"></asp:Literal></td>
                </tr>
                
                <tr>
                <td>Telefon</td>
                <td class="sep">:</td>
                <td><asp:Literal ID="ltrlTel" runat="server"></asp:Literal></td>
                </tr>
                
                <tr>
                <td>Faks</td>
                <td class="sep">:</td>
                <td><asp:Literal ID="ltrlFax" runat="server"></asp:Literal></td>
                </tr>
                
                <tr>
                <td>E-Posta</td>
                <td class="sep">:</td>
                <td><asp:LinkButton ID="lnkMail" runat="server"></asp:LinkButton></td>
                </tr>
                
                <tr>
                <td>Facebook</td>
                <td class="sep">:</td>
                <td><asp:HyperLink ID="lnkFacebook" runat="server"></asp:HyperLink></td>
                </tr>
                
                <tr>
                <td>Twitter</td>
                <td class="sep">:</td>
                <td><asp:LinkButton ID="lnkTwitter" runat="server"></asp:LinkButton></td>
                </tr>
                
                </table>
            	</div>
            <a href="#" class="more">daha fazla >></a>
            </div><!--/colums-->