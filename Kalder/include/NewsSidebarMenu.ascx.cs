﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class NewsSidebarMenu : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
        string HaberMenuBaslik = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            GetNewsCat();
        }

        public void GetNewsCat()
        {
            rpt_leftmenu.DataSource = db.GetDataTable("Select * from HaberKategori where YayinDurumu=1 and MenuGorunumu=1 order by KategoriSiraNo");
            rpt_leftmenu.DataBind();
        }

        protected void rpt_leftmenu_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;

                //string KategoriAdi = drv["KategoriAdi"].ToString();

                Literal ltrlLeftMenuHeader = (Literal)e.Item.FindControl("ltrlLeftMenuHeader");
               
                    

                HaberMenuBaslik = db.GetDataCell("Select newsHeader from GenelAyarlar");
                string url = new System.Uri(Page.Request.Url, ResolveClientUrl("~/" + HaberMenuBaslik)).AbsoluteUri;

                ltrlLeftMenuHeader.Text = "<a href='" + url + "'><h5>" + HaberMenuBaslik + "<h5></a>";
                //ltrlLeftMenuHeader.Text = link; 

                 
            }

            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                HyperLink hplLink = (HyperLink)e.Item.FindControl("hplLink");

                DataRowView drv = e.Item.DataItem as DataRowView;

                hplLink.NavigateUrl = Page.ResolveUrl(HaberMenuBaslik+"/" + DataBinder.Eval(drv, "KategoriAdi").ToString());


            }



        }
    }
}