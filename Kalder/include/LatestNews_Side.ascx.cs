﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class LatestNews_Side : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();

        protected void Page_Load(object sender, EventArgs e)
        {
            GetNews();
        }

        public void GetNews()
        {
            //DataTable dt = db.GetDataTable("select top 5 * from Haberler Where Durum=1 order by SiraNo");
            DataTable dt = db.GetDataTable("Select top 4 * from Haberler as h inner join HaberKategori as hk on h.KategoriId=hk.KategoriId where Durum=1 order by h.SiraNo");
            rptNews.DataSource = dt;
            rptNews.DataBind();

            //ltrlBaslik.Text = db.GetDataCell("Select newsHeader from GenelAyarlar").ToUpper();
        }

        protected void rptNews_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string id = DataBinder.Eval(e.Item.DataItem, "HaberId").ToString();
                string baslik = DataBinder.Eval(e.Item.DataItem, "Baslik").ToString();
                string ozet = DataBinder.Eval(e.Item.DataItem, "Ozet").ToString();

                string imageURL = db.GetDataCell("Select Resim from HaberResimleri where IsPrimary=1 and HaberId=" + id);

                System.Web.UI.WebControls.Image img = (System.Web.UI.WebControls.Image)e.Item.FindControl("img");
                img.ImageUrl = Page.ResolveUrl("~/upload/HaberResimleri/FotoGaleri/thumb/") + imageURL;
                //Literal ltrlImage = (Literal)e.Item.FindControl("ltrlImage");
                //ltrlImage.Text = "<img src=\"" + Page.ResolveUrl("~/upload/HaberResimleri/FotoGaleri/big/") + imageURL + "\" alt=\"" + baslik + "\" data-description=\"" + ozet + "\" />";

            }
        }


    }
}