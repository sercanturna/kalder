﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class boxes : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
        CacheProvider cp = new CacheProvider();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (cp.IsExist("Boxes"))
            {
                rpt_Box.DataSource = cp.Get<DataTable>("Boxes");
                ltrlBaslik.Text = cp.Get<string>("BoxHeader");
                rpt_Box.DataBind();
            }
            else
            {
                GetBoxes();
            }


            DataRow dr = db.GetDataRow("Select top 1 * from Banners where IsActive = 1 order by OrderNo desc");

            if (dr != null)
            {
                imgBanner1.ImageUrl = Page.ResolveUrl("~/upload/BannerResimleri/") + dr["ImageUrl"].ToString();
                hplBanner.NavigateUrl = dr["Link"].ToString();
            }

            
        }

        public void GetBoxes()
        {
            DataTable dt = db.GetDataTable("select * from Boxes Where IsActive=1 order by OrderNo");

            cp.Remove("Boxes");
            cp.Remove("BoxHeader");
            cp.Set("Boxes", dt, 60 * 24 * 365);
            rpt_Box.DataSource = cp.Get<DataTable>("Boxes"); ;
            rpt_Box.DataBind();


            string BoxHeader = db.GetDataCell("Select BoxHeader from GenelAyarlar").ToUpper();

            cp.Set("BoxHeader", BoxHeader, 60 * 24 * 10);

            ltrlBaslik.Text = cp.Get<string>("BoxHeader");

        }
    }
}