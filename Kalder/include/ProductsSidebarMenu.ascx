﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductsSidebarMenu.ascx.cs" Inherits="Kalder.include.ProductsSidebarMenu" %>
 <div id='cssmenu'>
                <ul>
                    <asp:Repeater ID="rpt_leftmenu" runat="server" OnItemDataBound="rpt_leftmenu_OnItemDataBound">
                        <HeaderTemplate>
                            <asp:Literal ID="ltrlLeftMenuHeader" runat="server"></asp:Literal>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li><a href='<%#Page.ResolveUrl("~/urunler/") %><%#Eval("Seo_url") %>'><span><%#Eval("KatAdi") %></span></a>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>
                </ul>
            </div>
            <!-- / cssmenu-->