﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class LatestNews : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
        CacheProvider cp = new CacheProvider();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (cp.IsExist("news"))
            {
                rptNews.DataSource = cp.Get<DataTable>("news");
                ltrlBaslik.Text = cp.Get<string>("newsHeader");
                rptNews.DataBind();
            }
            else
            {
                GetNews();
            }


            DataRow dr = db.GetDataRow("Select top 1 * from Banners where IsActive = 1 order by OrderNo asc");

            if (dr != null)
            {
                imgBanner1.ImageUrl = Page.ResolveUrl("~/upload/BannerResimleri/") + dr["ImageUrl"].ToString();
                hplBanner.NavigateUrl = dr["Link"].ToString();
            }

        }

        public void GetNews()
        {
            //DataTable dt = db.GetDataTable("select top 5 * from Haberler Where Durum=1 order by SiraNo");
            DataTable dt = db.GetDataTable("Select top 8 * from Haberler as h inner join HaberKategori as hk on h.KategoriId=hk.KategoriId where Durum=1 and Manset=1 order by h.SiraNo");

            cp.Remove("news");
            cp.Remove("newsHeader");
            cp.Set("news", dt, 60 * 24 * 365);

            rptNews.DataSource = cp.Get<DataTable>("news");
            rptNews.DataBind();

            string newsHeader = db.GetDataCell("Select newsHeader from GenelAyarlar").ToUpper();
            cp.Set("newsHeader", newsHeader, 60 * 24 * 10);

            ltrlBaslik.Text = cp.Get<string>("newsHeader");
        }

        protected void rptNews_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string id = DataBinder.Eval(e.Item.DataItem, "HaberId").ToString();
                string baslik = DataBinder.Eval(e.Item.DataItem, "Baslik").ToString();
                string ozet = DataBinder.Eval(e.Item.DataItem, "Ozet").ToString();

                string imageURL = db.GetDataCell("Select Resim from HaberResimleri where IsPrimary=1 and HaberId=" + id);

                System.Web.UI.WebControls.Image img = (System.Web.UI.WebControls.Image)e.Item.FindControl("img");
                img.ImageUrl = Page.ResolveUrl("~/upload/HaberResimleri/FotoGaleri/big/") + imageURL;
                //Literal ltrlImage = (Literal)e.Item.FindControl("ltrlImage");
                //ltrlImage.Text = "<img src=\"" + Page.ResolveUrl("~/upload/HaberResimleri/FotoGaleri/big/") + imageURL + "\" alt=\"" + baslik + "\" data-description=\"" + ozet + "\" />";

            }
        }


    }
}