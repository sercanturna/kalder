﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PagesSidebarMenu.ascx.cs" Inherits="Kalder.include.PagesSidebarMenu" %>
<aside>
 <div id='cssmenu'>
    <ul>
        <asp:Repeater ID="rpt_leftmenu" runat="server" OnItemDataBound="rpt_leftmenu_OnItemDataBound">
            <HeaderTemplate>
            <asp:Literal ID="ltrlLeftMenuHeader" runat="server"></asp:Literal>
            </HeaderTemplate>
            <ItemTemplate>
                <li><a id="<%#Eval("PageId") %>" href='<%#Eval("PageURL") %>'><span><%#Eval("PageName") %></span></a></li>
            </ItemTemplate>
         <%--   <FooterTemplate>
                </ul>
            </FooterTemplate>--%>
        </asp:Repeater>
    </ul>
</div>
</aside>