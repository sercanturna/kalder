﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class breadcumb : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
        string ProductCatId, ParentProductCatId;

        string ProductCat = string.Empty;

        public string getKategoriID
        {
            set { ProductCat = value; }
            get { return ProductCat; }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            // ProductCat = RouteData.Values["ProductCat"].ToString();
            ProductCatId = db.GetDataCell("Select UrunKatId from UrunKategori where Seo_url='" + ProductCat + "'");
            ParentProductCatId = db.GetDataCell("Select UstKatId from UrunKategori where UrunKatId='" + ProductCatId + "'");
            GetBreadcrumb(ProductCatId);
        }



        public void GetBreadcrumb(string ProductCatId)
        {
            //rptBreadcrumb.DataSource = db.GetDataTable("select * from Pages where PageId=" + PageId +" and PageIsActive=1 and ShowMenu=1 order by OrderNumber"); bu şekilde breadcumb da sayfa gözükmüyor
            rptBreadcrumb.DataSource = db.GetDataTable("select * from UrunKategori where UrunKatId=" + ProductCatId + " order by SiraNo");
            rptBreadcrumb.DataBind();
        }

        protected void rptBreadcrumb_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;

                PlaceHolder treeMenu = (PlaceHolder)e.Item.FindControl("treeMenu");
                treeMenu.Controls.Clear();

                //DataSet ds = db.GetDataSet("Select * From Pages where PageId=" + PageId + "and PageIsActive=1 and ShowMenu=1"); bu şekilde breadcumb da sayfa gözükmüyor
                DataSet ds = db.GetDataSet("Select * From UrunKategori where UrunKatId=" + ProductCatId);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if ((int)dr["UstKatId"] != 0)
                    {
                        int Id = (int)dr["UrunKatId"];
                        getParent(Convert.ToInt32(ProductCatId), treeMenu);
                    }

                    treeMenu.Controls.Add(new LiteralControl("<li>" + dr["KatAdi"].ToString() + "</li>"));
                }
            }
        }

        private int getParent(int ProductCatId, PlaceHolder treeMenu)
        {
            string ParentProductCatId = db.GetDataCell("select UstKatId from UrunKategori where UrunKatId=" + ProductCatId + "order by SiraNo asc");

            // DataSet ds = db.GetDataSet("select * from Pages where PageId=" + parentPageId + "and PageIsActive=1 and ShowMenu=1");bu şekilde breadcumb da sayfa gözükmüyor
            DataSet ds = db.GetDataSet("select * from UrunKategori where UrunKatId=" + ParentProductCatId);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string ParentPageName = db.GetDataCell("Select KatAdi from UrunKategori where UrunKatId=" + ParentProductCatId);

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (ParentProductCatId != "0")
                    {
                        int id = Convert.ToInt32(ParentProductCatId);
                        getParent(id, treeMenu);
                    }
                    treeMenu.Controls.Add(new LiteralControl("<li><a href=" + Page.ResolveUrl("~/urunler/") + dr["Seo_url"].ToString() + ">" + dr["KatAdi"].ToString() + "</a></li>"));
                }
                return 0;
            }
            return 0;
        }
    }
}