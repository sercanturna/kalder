﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class style_switcher : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
        StyleColor st = new StyleColor();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DataRow drAyarlar = db.GetDataRow("Select * from GenelAyarlar");
                Renk1ColorPicker.Text = Server.HtmlEncode(drAyarlar["Color1"].ToString().Trim());
                Renk2ColorPicker.Text = Server.HtmlEncode(drAyarlar["Color2"].ToString().Trim());

                // Renkleri değişitirmek için st adında bir class yazdım.
               // st.ChangeStyle(Renk1ColorPicker, Renk2ColorPicker);
            }
           
        }

        
        protected void lnkColors_Click(object sender, EventArgs e)
        {
            //Renkleri değişitirmek için st adında bir class yazdım.
            st.ChangeStyle(Renk1ColorPicker, Renk2ColorPicker);

            #region ChangeColorDbAction

            SqlConnection conn = db.baglan();
            string command = "Update GenelAyarlar SET Color1='" + Renk1ColorPicker.Text + "', Color2='" + Renk2ColorPicker.Text + "'";
            SqlCommand cmd = new SqlCommand(command,conn);

            int sonuc = cmd.ExecuteNonQuery();

            #endregion

        }
    }
}