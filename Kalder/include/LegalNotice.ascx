﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LegalNotice.ascx.cs" Inherits="Kalder.include.LegalNotice" %>

<div class="colums">
            	<h2 class="h2_bg"><span class="icon"></span> YASAL BİLDİRİM</h2>
            		<div class="image" style=""><img src="images/welcome.png" /></div>
            		<div class="about-text" style="padding:10px;">
                    <table class="yasal">
                        <tbody>
                            <tr>
                                <td><h2>Firma Ünvanı</h2></td>
                                <td class="sep">:</td>
                                <td><p><asp:Literal ID="ltrlCompanyTitle" runat="server"></asp:Literal></p></td>
                            </tr>
                            <tr>
                                <td><h2>Vergi Dairesi</h2></td>
                                <td class="sep">:</td>
                                <td><p><asp:Literal ID="ltrlVergiD" runat="server"></asp:Literal></p></td>
                            </tr>
                            <tr>
                                <td><h2>Vergi No</h2></td>
                                <td class="sep">:</td>
                                <td><p><asp:Literal ID="ltrlVergiNo" runat="server"></asp:Literal></p></td>
                            </tr> 
                            <tr>
                                <td><h2>Mersis No</h2></td>
                                <td class="sep">:</td>
                                <td><p><asp:Literal ID="ltrlMersis" runat="server"></asp:Literal></p></td>
                            </tr>
                            <tr>
                                <td><h2>Nace Kodu</h2></td>
                                <td class="sep">:</td>
                                <td><p><asp:Literal ID="LtrlNace" runat="server"></asp:Literal></p></td>
                            </tr>
                           
                        </tbody>
                    </table>
                        
                    

                        

                <p class="normal" style="display:none;">
                   
                   <span class="color-text"> Cplus Interactive Media </span> olarak 2005 yılında kurulmuş olan firmamız günümüz teknolojilerini yakından takip ederek her geçen gün hızla gelişmiş ve siz değerli müşterilerine verdiği hizmet alanlarını da geliştirmiştir. 
     
                    2008 yılı Dünyanın en büyük <span class="color-text">Register Firması olan Reseller Club </span> Türkiye Bayiiliğini Domain Tescil alanında da profesyonel hizmet vermeye başlamıştır. 
                  
                    2009 yılı itibarı ile <span class="color-text">Google Adwords Türkiye İnternet Reklamcılığı </span>programına üye olan firmamız Facebook Reklam Çözümlerinde kurumsal bazda siz değerli müşterilerine hizmet vermektedir. 
                    
                    2012 yılında mevcut sunucu altyapısını genişleterek maximum hız ve garantili altyapı için Türkiye'nin En büyük DataCenter(Hosting) firmalarından biri olan <span class="color-text">VARGONEN TECHNOLOGY ile anlaşarak Hosting </span> ve Sunucu Kiralama hizmeti vermeye başlamıştır. 
                   
                    2013 yılının son çeyreğinde Fransa'nın önde gelen internet firmalarından biri olan <span class="color-text"> Central Web ile iş ve çözüm ortaklığı </span> anlaşmasını imzalayan firmamız; Centralweb platformlarının tasarım ve koordinasyon hizmetlerini sağlamaya başlamış ve bu platformların Türkiye pazarında satış ortaklığını almış bulunmaktadır. 
                    Aynı zamanda Microsoft Bizspark programını 3 yıllık süre ile tamamlamış olup, <span class="color-text"> Microsoft Certified Partner </span> olma hakkı kazanmış bulunan firmamız;
                    <br><br>
                    Kaliteli hizmet için hergeçen gün dünya devleri ile ortak çalışma sahaları araştırıp, geliştiren firmamız aynı zamanda Web Tasarım, Portal Tasarımı, Web Tabanlı Yazılım ve Uygulamalar, Kurumsal Web Siteleri, Hosting Hizmetleri, E-Bülten Tasarımları, İnternet Reklamları alanında faaliyet Göstermektedir.
                </p>
                <a href="legal_notice.aspx" class="more">daha fazla >></a>
              </div>
          </div>