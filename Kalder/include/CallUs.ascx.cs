﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class CallUs : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            ltrlTel.Text = db.GetDataCell("Select Tel from GenelAyarlar");
        }
    }
}