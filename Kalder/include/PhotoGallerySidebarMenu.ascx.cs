﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class PhotoGallerySidebarMenu : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            GetGalleryCat();
        }


        public void GetGalleryCat()
        {
            DataTable dt = db.GetDataTable("Select * from GaleriKategori where YayinDurumu=1 order by KategoriSiraNo");
            rpt_leftmenu.DataSource = dt;
            rpt_leftmenu.DataBind();

            //rptGallerySlider.DataSource = dt;
            //rptGallerySlider.DataBind();


            //rptGalleryThumb.DataSource = dt;
            //rptGalleryThumb.DataBind();
        }
    }
}