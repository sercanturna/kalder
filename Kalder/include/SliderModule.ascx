﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SliderModule.ascx.cs" Inherits="Kalder.include.SliderModule" %>
 


	<!--======= BANNER =========-->
		<div class="slider">
			<div id="banner" class="bnr-2">
				<div class="flex-banner">
					<ul class="slides">
                        <asp:Repeater ID="rptSlider" runat="server">
                            <ItemTemplate>
                                <li><a href="<%#Eval("Link") %>" target="<%#Eval("LinkTarget") %>">
                                    <!-- Overlay for Slider -->
                                    <span style="width:100%; height:100%; background-color:#026d6b; display:block; position:absolute; opacity:0"></span>
                                    <img class="lazy" src="upload/SlaytResimleri/<%#Eval("ImageURL") %>" alt="<%#Eval("Title") %>" />
                                    <div class="container">
								        <div class="meta">
									        <%--<p>2017 TÜRKİYE KALİTE KONGRESİ YAPILDI;</p>--%>
									        <h2><%#Eval("Title") %></h2>
								        </div>
							        </div>
                                    </a>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>


						<%--<li> 
                        	<a href="#1">
                            <span style="width:100%; height:100%; background-color:#026d6b; display:block; position:absolute; opacity:0.2;"></span>
							<img src="images/slider-images/slide-1.jpg" alt="" >
							<div class="container">
								<div class="meta">
									<p>2016 TÜRKİYE MÜKEMMELLİK ÖDÜLLERİ SAHİPLERİNİ BULDU</p>
									<h2>MÜKEMMELLİK ÖDÜLLERİ</h2>
								</div>
							</div>
                            </a>
						</li>
                        <li> 
							<a href="#2">
                            <span style="width:100%; height:100%; background-color:#026d6b; display:block; position:absolute; opacity:0.2;"></span>
							<img src="images/slider-images/slide-2.jpg" alt="" >
							<div class="container">
								<div class="meta">
									<p>2017 TÜRKİYE KALİTE KONGRESİ YAPILDI;</p>
									<h2>KALİTE KONGRESİ</h2>
								</div>
							</div>
                            </a>
						</li>
                         <li> 
							<a href="#3">
                            <span style="width:100%; height:100%; background-color:#026d6b; display:block; position:absolute; opacity:0.2;"></span>
							<img src="images/slider-images/slide-3.jpg" alt="" >
							<div class="container">
								<div class="meta">
									<p></p>
									<h2></h2>
								</div>
							</div>
                            </a>
						</li>--%>
					</ul>
				</div>
			</div>
		</div>