﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class Welcome : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
           DataRow dr = db.GetDataRow("Select * from ModuleContent where ModuleId=15");
            ltrlContent.Text = dr["ModuleContent"].ToString();
            lnkBtn.PostBackUrl =  Page.ResolveUrl("~/")+dr["ModuleLink"].ToString();
            ltrlBaslik.Text = dr["ModuleTitle"].ToString();


            if (dr["ModuleImage"] == null || dr["ModuleImage"].ToString() == "")
            {
                imgResim.Visible = false;
            }
            else
            {
                imgResim.ImageUrl = "~/upload/" + dr["ModuleImage"].ToString();
            }

             
        }
    }
}