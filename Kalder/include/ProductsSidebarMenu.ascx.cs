﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class ProductsSidebarMenu : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
        string ProductCatId, ParentProductCatId;

        string ProductCat = string.Empty;



        public string ProductCatProp
        {
            set { ProductCat = value; }
            get { return ProductCat; }
        }





        protected void Page_Load(object sender, EventArgs e)
        {

            ProductCatId = db.GetDataCell("Select UrunKatId from UrunKategori where Seo_url='" + ProductCat + "'");
            ParentProductCatId = db.GetDataCell("Select UstKatId from UrunKategori where UrunKatId='" + ProductCatId + "'");



            if (ParentProductCatId == "0")
            {
                GetPageleftMenu(ParentProductCatId, ProductCatId);
            }
            else
            {
                GetPageleftMenu(ParentProductCatId, ProductCatId);
            }
        }

        public void GetPageleftMenu(string ParentProductCatId, string ProductCatId)
        {
            DataSet ds = db.GetDataSet("SELECT aa.UrunKatId, aa.UstKatId, aa.Seo_url as Seo_url, bb.KatAdi AS ParentCat, aa.KatAdi FROM UrunKategori aa LEFT OUTER JOIN UrunKategori bb ON aa.UstKatId=bb.UrunKatId where aa.UstKatId=" + ProductCatId + " and aa.YayinDurumu=1 order by aa.SiraNo");

            if (ds.Tables[0].Rows.Count == 0)
            {
                ds = db.GetDataSet("SELECT aa.UrunKatId, aa.UstKatId, aa.Seo_url as Seo_url, bb.KatAdi AS ParentCat, aa.KatAdi FROM UrunKategori aa LEFT OUTER JOIN UrunKategori bb ON aa.UstKatId=bb.UrunKatId where aa.UstKatId=" + ParentProductCatId + " and aa.YayinDurumu=1 order by aa.SiraNo");
            }

            rpt_leftmenu.DataSource = ds;
            rpt_leftmenu.DataBind();
        }




        protected void rpt_leftmenu_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            DataRowView drv = e.Item.DataItem as DataRowView;
            Literal ltrlLeftMenuHeader = (Literal)e.Item.FindControl("ltrlLeftMenuHeader");

            if (e.Item.ItemType == ListItemType.Header)
            {
                DataSet ds = db.GetDataSet("SELECT aa.UrunKatId, aa.UstKatId, aa.Seo_url as Seo_url, bb.KatAdi AS ParentCatName, aa.KatAdi FROM UrunKategori aa LEFT OUTER JOIN UrunKategori bb ON aa.UstKatId=bb.UrunKatId where aa.UstKatId=" + ProductCatId);


                if (ds.Tables[0].Rows.Count == 0)
                {
                    ds = db.GetDataSet("SELECT aa.UrunKatId, aa.UstKatId, aa.Seo_url as Seo_url, bb.KatAdi AS ParentCatName, aa.KatAdi FROM UrunKategori aa LEFT OUTER JOIN UrunKategori bb ON aa.UstKatId=bb.UrunKatId where aa.UstKatId=" + ParentProductCatId);
                }

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ltrlLeftMenuHeader.Text = "<li class='has-sub active'><a href='" + dr["ParentCatName"].ToString() + "'><span>" + dr["ParentCatName"].ToString() + "</span></a><ul>";
                }
            }

        }
    }
}