﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LatestNews.ascx.cs" Inherits="Kalder.include.LatestNews" %>

<section class="blog">
    <div class="container">
        <div class="tittle tittle-2">
            <p>Bizden Haberiniz Olsun</p>
            <h3><span style="text-transform: none;">KalDer</span>-
                <asp:Literal ID="ltrlBaslik" runat="server"></asp:Literal></h3>
            <hr>
        </div>
        <ul class="row">

            <asp:Repeater ID="rptNews" runat="server" OnItemDataBound="rptNews_ItemDataBound">
                <ItemTemplate>
                    <li class="col-md-3 col-sm-6" style="margin-top: 10px;">
                        <div class="blog-item">
                            <div class="overlay">
                                <h2><%#Eval("Baslik") %></h2>
                                <p>
                                    <%#Eval("Ozet") %>
                                    <a href="<%= ResolveUrl("~/") %><%#Ayarlar.UrlSeo(Eval("KategoriAdi").ToString()) %>/<%#Eval("HaberId") %>/<%#Ayarlar.UrlSeo(Eval("Baslik").ToString()) %>" class="more">Devamını oku »</a>
                                </p>
                            </div>
                            <asp:Image ID="img" runat="server" CssClass="img img-responsive" /><p class="news">
                                <%#Ayarlar.OzetCek(Eval("Ozet").ToString(),80) %>
                        </div>

                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>


        <div class="row margin-t-20">
            <div class="col-lg-12 text-center">
                <asp:HyperLink ID="hplBanner" runat="server">
                    <asp:Image ID="imgBanner1" runat="server" CssClass="img img-responsive banner" /></asp:HyperLink>
            </div>
        </div>
    </div>
</section>
