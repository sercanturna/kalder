﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class FacebookLikeBox : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();

        protected void Page_Load(object sender, EventArgs e)
        {



            /* Facebook Likebox*/
            string fblink = db.GetDataCell("Select Facebook from GenelAyarlar");

            if (fblink == "" || fblink == null)
                pnl_facebook_like.Visible = false;
            else
            {
                LtrlFbox.Text = "<div class=\"fb-like-box\" data-href=\"" + fblink + "\" data-width=\"218\" data-colorscheme=\"light\" data-show-faces=\"true\" data-header=\"true\" data-stream=\"false\" data-show-border=\"true\"></div>";
               // ltrlFbComments.Text = "<div class=\"fb-comments yorum\"  data-href=\"" + Request.Url.Authority + Request.Url.AbsolutePath + "\" data-numposts=\"5\" data-colorscheme=\"light\"></div>";
            }


        }
    }
}