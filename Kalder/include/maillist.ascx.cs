﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class maillist : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            PnlSonuc.Visible = false;
        }

        protected void contact_submit1_Click(object sender, EventArgs e)
        {
            string adSoyad = Ayarlar.Temizle(Ayarlar.SQLInjectionControl(txtName.Text));
            string Email = Ayarlar.Temizle(Ayarlar.SQLInjectionControl(txtEmail.Text));

            SqlConnection baglanti = db.baglan();
            SqlCommand cmdKaydet = new SqlCommand("SET language turkish; insert into Maillist(" +
            "AdSoyad," +
            "Email," +
            "SentDate) values (" +
            "@AdSoyad," +
            "@Email," +
            "@SentDate)",
            baglanti);

            cmdKaydet.Parameters.AddWithValue("AdSoyad", adSoyad);
            cmdKaydet.Parameters.AddWithValue("Email", Email);
            cmdKaydet.Parameters.AddWithValue("SentDate", DateTime.Now.ToString());

            int sonuc = cmdKaydet.ExecuteNonQuery();

            if (sonuc > 0)
            {
                pnlFormContent.Visible = false;
                PnlSonuc.Visible = true;
                //Ayarlar.Alert.Show("Mesajınız tarafımıza ulaşmıştır, En kısa sürede size geri dönüş sağlanacaktır.");
            }
            else
                Ayarlar.Alert.Show("Form Gönderilemedi, Lütfen Tekrar Deneyin.");

        }

        protected void lnkClose_Click(object sender, EventArgs e)
        {
            pnlFormContent.Visible = true;
            PnlSonuc.Visible = false;
        }
    }
}