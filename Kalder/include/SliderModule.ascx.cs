﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class SliderModule : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
        CacheProvider cp = new CacheProvider();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (cp.IsExist("slide"))
            {
                rptSlider.DataSource = cp.Get<DataTable>("slide");
                rptSlider.DataBind();
            }
            else
            {
                GetSlides();
            }
            
        }

        public void GetSlides()
        {
            string sorgu = "set language turkish; select * from Sliders as s Where s.IsActive=1 and '" + DateTime.Now.ToString() + "' >= s.Date and s.EndDate >= '" + DateTime.Now.ToString() + "' order by OrderNo";
            DataTable dt = db.GetDataTable(sorgu);
            
            cp.Remove("slide");
            cp.Set("slide", dt, 30);
            rptSlider.DataSource = cp.Get<DataTable>("slide"); ;


            rptSlider.DataBind();
        }
    }
}