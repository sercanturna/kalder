﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class YonetimKurulu : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = db.GetDataTable("Select * from BoardofDirectors");
            rptYk.DataSource = dt;
            rptYk.DataBind();
        }
    }
}