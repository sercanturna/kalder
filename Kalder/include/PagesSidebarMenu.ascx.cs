﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class PagesSidebarMenu : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();


        string PageId = string.Empty;
        string ParentPageId= string.Empty;
        string PageURL = string.Empty;
        



        public string PageIDwithURL
        {
            set { PageURL = value; }
            get { return PageURL; }
        }



        protected void Page_Load(object sender, EventArgs e)
        {


            // PageURL = RouteData.Values["PageURL"].ToString();
            PageId = db.GetDataCell("Select top 1 PageId from Pages where PageURL='" + PageURL + "' order by PageId desc");

            ParentPageId = db.GetDataCell("Select ParentPageId from Pages where PageId='" + PageId + "'");
             

            if (ParentPageId == "0")
            {
                GetPageleftMenu(ParentPageId, PageId);
            }
            else 
            {
                GetPageleftMenu(ParentPageId, PageId);
            }

            
            
        }

        public void getSubMenuVertical(string PageId)
        {
 
        }

        public void GetPageleftMenu(string ParentPageId, string PageId)
        {

            //DataSet ds = db.GetDataSet("SELECT aa.PageId, aa.ParentPageId, aa.PageUrl as PageUrl, bb.PageName AS ParentPage, aa.PageName FROM Pages aa " +
            //"LEFT OUTER JOIN Pages bb ON aa.ParentPageId=bb.PageId where aa.ParentPageId=" + PageId + " and aa.PageIsActive=1 order by aa.OrderNumber");

            //rpt_leftmenu.DataSource = ds;
            //rpt_leftmenu.DataBind();

            

            // Bu şekilde yaparsak tüm alt sayfaları yan menüde gösteriyor.

            if (ParentPageId == "0" || ParentPageId == null || ParentPageId == "")
            {
                //rpt_leftmenu.DataSource = db.GetDataTable("SELECT aa.PageId, aa.ParentPageId, aa.PageUrl as PageUrl, bb.PageName AS ParentPage, aa.PageName FROM Pages aa LEFT OUTER JOIN Pages bb ON aa.ParentPageId=bb.PageId where aa.ParentPageId=" + PageId + " or bb.PageId=" + ParentPageId + " and aa.PageIsActive=1 order by aa.OrderNumber");
                rpt_leftmenu.DataSource = db.GetDataTable("SELECT aa.PageId, aa.ParentPageId, aa.PageUrl as PageUrl, bb.PageName AS ParentPage, aa.PageName FROM Pages aa LEFT OUTER JOIN Pages bb ON aa.ParentPageId=bb.PageId where aa.ParentPageId=" + PageId + " and aa.PageIsActive=1 order by aa.OrderNumber");
                rpt_leftmenu.DataBind();
            }
            else
            {
              
                DataTable dt = db.GetDataTable("SELECT aa.PageId, aa.ParentPageId, aa.PageUrl as PageUrl, bb.PageName AS ParentPage, aa.PageName FROM Pages aa LEFT OUTER JOIN Pages bb ON aa.ParentPageId=bb.PageId where aa.ParentPageId=" + PageId + " and aa.PageIsActive=1 order by aa.OrderNumber");

                if (dt.Rows.Count > 0)
                {
                    rpt_leftmenu.DataSource = dt;
                    rpt_leftmenu.DataBind();
                }
                else
                {
                    int id = GetTopPageId(Convert.ToInt32(PageId));

                    //DataTable dt2 = db.GetDataTable("SELECT aa.PageId, aa.ParentPageId, aa.PageUrl as PageUrl, bb.PageName AS ParentPage, aa.PageName FROM Pages aa LEFT OUTER JOIN Pages bb ON aa.ParentPageId=bb.PageId where bb.PageId=" + id + " and aa.PageIsActive=1 order by aa.OrderNumber");

                    DataTable dt2 = db.GetDataTable("SELECT aa.PageId, aa.ParentPageId, aa.PageUrl as PageUrl, bb.PageName AS ParentPage, aa.PageName FROM Pages aa LEFT OUTER JOIN Pages bb ON aa.ParentPageId=bb.PageId where aa.ParentPageId= "+id+" and aa.PageIsActive=1 order by aa.OrderNumber");
                    rpt_leftmenu.DataSource = dt2;
                    rpt_leftmenu.DataBind();

                  // Response.Write(id.ToString());
                }
                
            }

            //Orjinal Ydek
            //if (ParentPageId == "0" || ParentPageId == null || ParentPageId == "")
            //{
            //    //rpt_leftmenu.DataSource = db.GetDataTable("SELECT aa.PageId, aa.ParentPageId, aa.PageUrl as PageUrl, bb.PageName AS ParentPage, aa.PageName FROM Pages aa LEFT OUTER JOIN Pages bb ON aa.ParentPageId=bb.PageId where aa.ParentPageId=" + PageId + " or bb.PageId=" + ParentPageId + " and aa.PageIsActive=1 order by aa.OrderNumber");
            //    rpt_leftmenu.DataSource = db.GetDataTable("SELECT aa.PageId, aa.ParentPageId, aa.PageUrl as PageUrl, bb.PageName AS ParentPage, aa.PageName FROM Pages aa LEFT OUTER JOIN Pages bb ON aa.ParentPageId=bb.PageId where aa.ParentPageId=" + PageId + " and aa.PageIsActive=1 order by aa.OrderNumber");
            //    rpt_leftmenu.DataBind();
            //}
            //else
            //{
            //    //rpt_leftmenu.DataSource = db.GetDataTable("SELECT aa.PageId, aa.ParentPageId, aa.PageUrl as PageUrl, bb.PageName AS ParentPage, aa.PageName FROM Pages aa LEFT OUTER JOIN Pages bb ON aa.ParentPageId=bb.PageId where bb.ParentPageId=" + ParentPageId + " or bb.PageId=" + ParentPageId + " and aa.PageIsActive=1 order by aa.OrderNumber");
            //    rpt_leftmenu.DataSource = db.GetDataTable("SELECT aa.PageId, aa.ParentPageId, aa.PageUrl as PageUrl, bb.PageName AS ParentPage, aa.PageName FROM Pages aa LEFT OUTER JOIN Pages bb ON aa.ParentPageId=bb.PageId where bb.ParentPageId=" + ParentPageId + " and aa.PageIsActive=1 order by aa.OrderNumber");
            //    rpt_leftmenu.DataBind();
            //}
        }


        private int GetTopPageId(int pageId)
        {
            int rpageId = 0;
            int ParentId = 0;

            DataSet ds = db.GetDataSet("select * from Pages where PageId='" + pageId + "' and PageIsActive=1 order by OrderNumber");

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ParentId = (int)dr["ParentPageId"];
                    rpageId = (int)dr["PageId"];
                    GetTopPageId(ParentId);
                    
                }

                return ParentId;
            }

            return ParentId;
        }



        protected void rpt_leftmenu_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                Literal ltrlLeftMenuHeader = (Literal)e.Item.FindControl("ltrlLeftMenuHeader");

                

                DataSet ds = db.GetDataSet("SELECT aa.PageId, aa.ParentPageId, aa.PageUrl as PageUrl, bb.PageName AS ParentPage, aa.PageName FROM Pages aa LEFT OUTER JOIN Pages bb ON aa.ParentPageId=bb.PageId where aa.PageId='" + PageId + "' or aa.ParentPageId=" + PageId);

             
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                   //ltrlLeftMenuHeader.Text = "<li class='has-sub active'><a href='" + Ayarlar.UrlSeo(dr["ParentPage"].ToString()) + "'><span>" + dr["ParentPage"].ToString() + "</span></a><ul>";
                    ltrlLeftMenuHeader.Text = "<a href='" + Ayarlar.UrlSeo(dr["ParentPage"].ToString()) + "'><h5>" + dr["ParentPage"].ToString() + "<h5></a>";
                }

            }

            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    DataRowView drv = e.Item.DataItem as DataRowView;

            //    DataSet ds = db.GetDataSet("Select * From Pages where PageId=" + drv.Row["PageId"].ToString() + "and PageIsActive=1 and ShowMenu=1  order by OrderNumber");

            //    foreach (DataRow dr in ds.Tables[0].Rows)
            //    {
            //        if ((int)dr["ParentPageId"] != 0)
            //        {

            //            int Id = (int)dr["PageId"];
            //            GetTopPageId(Id);
            //        }
            //    }
            //    treeMenu.Controls.Add(new LiteralControl("</ul>"));
            //}

        }
    }
}