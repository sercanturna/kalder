﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class TextSlider : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
        CacheProvider cp = new CacheProvider();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (cp.IsExist("OnceKaliteDergisi"))
            {
                rptDergi.DataSource = cp.Get<DataTable>("OnceKaliteDergisi");
                rptDergi.DataBind();
            }
            else
            {
                GetMagazine();
            }
        }


        public void GetMagazine()
        {
            DataTable dt = db.GetDataTable("Select * from OnceKaliteDergisi order by OrderNo desc");

            cp.Remove("OnceKaliteDergisi");
            cp.Set("OnceKaliteDergisi", dt, 60 * 24 * 365);

            rptDergi.DataSource = cp.Get<DataTable>("OnceKaliteDergisi"); ;
            rptDergi.DataBind();
        }
    }
}