﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class references : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
        CacheProvider cp = new CacheProvider();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (cp.IsExist("Reference"))
            {
                rptRef.DataSource = cp.Get<DataTable>("Reference");
                ltrlBaslik.Text = cp.Get<string>("refHeader");
                rptRef.DataBind();
            }
            else
            {
                GetReferences();
            }

            
        }

        public void GetReferences()
        {
            DataTable dt = db.GetDataTable("select * from Reference Where Status=1 order by OrderNo");
            cp.Remove("Reference");
            cp.Remove("refHeader");
            cp.Set("Reference", dt, 60 * 24 * 365);

            rptRef.DataSource = cp.Get<DataTable>("Reference"); 
            rptRef.DataBind();


            string refHeader = db.GetDataCell("Select refHeader from GenelAyarlar").ToUpper();
            cp.Set("refHeader", refHeader, 60 * 24 * 365);
            ltrlBaslik.Text = cp.Get<string>("refHeader");
        }
    }
}