﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class ContactDetails : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            DataRow dr = db.GetDataRow("Select * from GenelAyarlar where GenelAyarId=1");
            ltrlAdress.Text = dr["Adres"].ToString();
            ltrlTel.Text = dr["Tel"].ToString();
            ltrlFax.Text = dr["Fax"].ToString();
            lnkMail.Text = dr["Eposta"].ToString();



            lnkMail.OnClientClick = "window.open('mailto:" + dr["Eposta"].ToString() + "', 'email'); return false;";


            lnkFacebook.Text = dr["Facebook"].ToString();
            lnkFacebook.NavigateUrl = "http://" + dr["Facebook"].ToString();
            lnkFacebook.Target = "_blank";
            //lnkFacebook.OnClientClick = "window.document.forms[0].target='_blank';";


            lnkTwitter.Text = dr["Twitter"].ToString();
            lnkTwitter.PostBackUrl = dr["Twitter"].ToString();
            lnkTwitter.OnClientClick = "window.document.forms[0].target='_blank';";
        }
    }
}