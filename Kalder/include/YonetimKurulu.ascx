﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="YonetimKurulu.ascx.cs" Inherits="Kalder.include.YonetimKurulu" %>

<style>
    span.row {
        display: table-row;
    }

    span.cell {
        display: table-cell;
        vertical-align: bottom;
    }

    .ykContainer {
        display: table;
        height: 106px;
        width: 100%;
    }

    ul.ykList {
        margin: 0;
        padding: 0;
        list-style: none;
        text-align: center;
    }

        ul.ykList > li {
            display: inline-block;
            width: 32%;
            margin-right: calc(2% - 4px);
            margin-bottom: 2%;
            vertical-align: bottom;
        }

            ul.ykList > li:nth-of-type(3n+1) {
                margin-right: 0;
            }

            ul.ykList > li > span {
                display: block;
                padding: 10px;
                background: #efefef;
            }

            ul.ykList > li img {
                display: block;
                max-width: 100%;
            }

    span.adsoyad {
        display: block;
        text-align: center;
        font-size: 14px;
        font-weight: bold;
        padding: 5px;
        border-bottom: 1px solid #cecece;
    }

    span.unvan1 {
        display: block;
        text-align: center;
        font-weight: bold;
        padding: 5px;
    }

    span.unvan2 {
        display: block;
        text-align: center;
        padding: 5px;
        border-top: 1px solid #cecece;
        font-size: 12px;
    }

    .ykContainer {
    }

    ul.ykList > li:first-of-type {
        display: block;
        margin: 0 auto 2% auto;
    }

    ul.ykList > li:nth-of-type(4) {
        display: block;
        margin: 0 auto 2% auto;
    }
</style>
<ul class="ykList">
    <asp:Repeater ID="rptYk" runat="server">
        <ItemTemplate>
            <li>
                <span>
                    <img src="<%= ResolveUrl("~/") %>Upload/YonetimKurulu/<%#Eval("Image") %>" alt="">
                    <span class="ykContainer">
                        <span class="row">
                            <span class="cell">
                                <span class="adsoyad"><%#Eval("Name") %></span>
                                <span class="unvan1"><%#Eval("Title") %></span>
                                <span class="unvan2"><%#Eval("Content") %></span>
                            </span>
                        </span>
                    </span>
                </span>
            </li>
        </ItemTemplate>
    </asp:Repeater>

</ul>
