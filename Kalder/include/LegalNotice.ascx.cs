﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class LegalNotice : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
       protected void Page_Load(object sender, EventArgs e)
        {
            DataRow dr = db.GetDataRow("Select * from GenelAyarlar where GenelAyarId=1");

            ltrlCompanyTitle.Text = dr["FirmaUnvanUzun"].ToString();
            ltrlVergiD.Text = dr["VergiDairesi"].ToString();
            ltrlVergiNo.Text = dr["VergiNo"].ToString();

            ltrlMersis.Text = dr["MersisNo"].ToString();
            LtrlNace.Text = Ayarlar.OzetCek(dr["NaceKodu"].ToString(), 160);
        }
    }
}