﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace Kalder.include
{
    public partial class currency : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                getData();
        }


        public void getData()
        {
            DataTable dt = db.GetDataTable("Select * from Currency where IsActive=1 and CurrencyId>1");
            rptCurrency.DataSource = dt;
            rptCurrency.DataBind();
        }

        protected void rptCurrency_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            XmlDocument xmlVerisi = new XmlDocument();
            xmlVerisi.Load("http://www.tcmb.gov.tr/kurlar/today.xml");

            string CurrencyId = DataBinder.Eval(e.Item.DataItem, "CurrencyId").ToString();
            string currency = DataBinder.Eval(e.Item.DataItem, "Currency").ToString();
            string CurrencyName = DataBinder.Eval(e.Item.DataItem, "CurrencyName").ToString();

            Literal ltrlCurrency = (Literal)e.Item.FindControl("ltrlCurrency");
            ltrlCurrency.Text = currency;
            

            Label lblAlis = (Label)e.Item.FindControl("lblAlis");
            Label lblSatis = (Label)e.Item.FindControl("lblSatis");

            lblAlis.Text = Convert.ToDecimal(xmlVerisi.SelectSingleNode(string.Format("Tarih_Date/Currency[@Kod='{0}']/ForexBuying", ""+currency+"")).InnerText.Replace('.', ',')).ToString();
            lblSatis.Text = Convert.ToDecimal(xmlVerisi.SelectSingleNode(string.Format("Tarih_Date/Currency[@Kod='{0}']/ForexSelling", "" + currency + "")).InnerText.Replace('.', ',')).ToString();

        }
    }
}