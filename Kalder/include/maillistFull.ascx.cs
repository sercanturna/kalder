﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder.include
{
    public partial class maillistFull : System.Web.UI.UserControl
    {
        DbConnection db = new DbConnection();
        string SubDomainSubeAdi = Ayarlar.GetSubDomain();
        protected void Page_Load(object sender, EventArgs e)
        {
           // PnlSonuc.Visible = false;
        }

        protected void contact_submit1_Click(object sender, EventArgs e)
        {
            string adSoyad = Ayarlar.Temizle(Ayarlar.SQLInjectionControl(txtName.Text));
            string Email = Ayarlar.Temizle(Ayarlar.SQLInjectionControl(txtEmail.Text));

            

           // int kayitKontrol = db.cmd("Select count(Email) from Maillist where AdSoyad='" + adSoyad + "' and Email='" + Email + "'");

            DataRow dr = db.GetDataRow("Select top 1 * from Maillist where Email='" + Email + "'");


            if (dr !=null)
            {
                lblModalTitle.Text = "Uyarı! ";
                lblModalBody.Text = "Bu kullanıcı zaten veritabanımızda mevcut!";

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "MailingModal", "$('#MailingModal').modal();", true);
                upModal.Update();
            }
            else
            {
           

                SqlConnection baglanti = db.baglan();
                SqlCommand cmdKaydet = new SqlCommand("SET language turkish; insert into Maillist(" +
                "AdSoyad," +
                "Email," +
                "SentDate) values (" +
                "@AdSoyad," +
                "@Email," +
                "@SentDate)",
                baglanti);

                cmdKaydet.Parameters.AddWithValue("AdSoyad", adSoyad);
                cmdKaydet.Parameters.AddWithValue("Email", Email);
                cmdKaydet.Parameters.AddWithValue("SentDate", DateTime.Now.ToString());

                int sonuc = cmdKaydet.ExecuteNonQuery();

                if (sonuc > 0)
                {
                   // pnlFormContent.Visible = false;
                   // PnlSonuc.Visible = true;

                    //Ayarlar.Alert.Show("Mesajınız tarafımıza ulaşmıştır, En kısa sürede size geri dönüş sağlanacaktır.");

                    lblModalTitle.Text = "Tebrikler";
                    lblModalBody.Text = "E-bülten listemize başarıyla kayıt oldunuz.";
                    if (SubDomainSubeAdi == "kalder")
                        db.MailGonder("web@kalder.org", "KalDer " + SubDomainSubeAdi + " | Ebülten Ekleme Talebi", "" + SubDomainSubeAdi + "@kalder.org", "Ebülten Ekleme Talebi", "Ebülten Ekleme Talebi", adSoyad + " adlı kişi " + Email + " e-posta adresine bülten gelmesini istiyor.", "web@kalder.org", "!KalDer2017!", "smtp.office365.com");
                    else
                    {
                        db.MailGonder("web@kalder.org", "KalDer " + SubDomainSubeAdi + " | Ebülten Ekleme Talebi", "" + SubDomainSubeAdi + "@kalder.org", "Ebülten Ekleme Talebi", "Ebülten Ekleme Talebi", adSoyad + " adlı kişi " + Email + " e-posta adresine bülten gelmesini istiyor.", "web@kalder.org", "!KalDer2017!", "smtp.office365.com");
                        db.MailGonder("web@kalder.org", "KalDer " + SubDomainSubeAdi + "  | Ebülten Ekleme Talebi", "kalder@kalder.org", "" + SubDomainSubeAdi + " Ebülten Ekleme Talebi", "" + SubDomainSubeAdi + " Ebülten Ekleme Talebi", adSoyad + " adlı kişi " + Email + " e-posta adresine bülten gelmesini istiyor.", "web@kalder.org", "!KalDer2017!", "smtp.office365.com");
                    }

                    

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "MailingModal", "$('#MailingModal').modal();", true);
                    upModal.Update();
                }
                else
                {
                    //Ayarlar.Alert.Show("Form Gönderilemedi, Lütfen Tekrar Deneyin.");


                    lblModalTitle.Text = "Üzgünüm";
                    lblModalBody.Text = "Kayıt işlemi sırasında bir sorun oluştu. Lütfen daha sonra tekrar deneyin.";

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "MailingModal", "$('#MailingModal').modal();", true);
                    upModal.Update();
                }

            }
                

        }

        protected void lnkClose_Click(object sender, EventArgs e)
        {
            pnlFormContent.Visible = true;
           // PnlSonuc.Visible = false;
        }

        protected void btn_remove_Click(object sender, EventArgs e)
        {
            string adSoyad = Ayarlar.Temizle(Ayarlar.SQLInjectionControl(txtName.Text));
            string Email = Ayarlar.Temizle(Ayarlar.SQLInjectionControl(txtEmail.Text));


            int sonuc = db.cmd("Delete from Maillist where Email='" + Email + "'");

            if (sonuc < 1)
            {
                lblModalTitle.Text = "Üzgünüm";
                lblModalBody.Text = "Girmiş olduğunuz bilgilere ait bir kayıt bulunamamıştır!";

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "MailingModal", "$('#MailingModal').modal();", true);
                upModal.Update();
            }
            else
            {
                //int silmesonuc = db.cmd("Delete from Maillist where AdSoyad='" + adSoyad + "' and Email='" + Email + "'");

                //if (silmesonuc > 0)
                //{
                    lblModalTitle.Text = "Tebrikler";
                    lblModalBody.Text = "Girmiş olduğunuz bilgilere ait kayıt veritabanımızdan başarıyla silinmiştir.";

                    if (SubDomainSubeAdi == "kalder")
                db.MailGonder("web@kalder.org", "KalDer | Ebülten İptal Talebi", "" + SubDomainSubeAdi + "@kalder.org", "Ebülten İptal Talebi", "Ebülten İptal Talebi", adSoyad + " adlı kişi " + Email + " e-posta adresine artık bülten gelmesini istemiyor", "web@kalder.org", "!KalDer2017!", "smtp.office365.com");
                    else
                    {
                        db.MailGonder("web@kalder.org", "KalDer | Ebülten İptal Talebi", "" + SubDomainSubeAdi + "@kalder.org", "Ebülten İptal Talebi", "Ebülten İptal Talebi", adSoyad + " adlı kişi " + Email + " e-posta adresine artık bülten gelmesini istemiyor", "web@kalder.org", "!KalDer2017!", "smtp.office365.com");
                        db.MailGonder("web@kalder.org", "KalDer " + SubDomainSubeAdi + " | Ebülten İptal Talebi", "kalder@kalder.org", "" + SubDomainSubeAdi + " Ebülten İptal Talebi", "" + SubDomainSubeAdi + " Ebülten İptal Talebi", adSoyad + " adlı kişi " + Email + " e-posta adresine artık bülten gelmesini istemiyor", "web@kalder.org", "!KalDer2017!", "smtp.office365.com");

                    }
                
                


                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "MailingModal", "$('#MailingModal').modal();", true);
                    upModal.Update();
                //}
                //else
                //{
                //    lblModalTitle.Text = "Üzgünüm";
                //    lblModalBody.Text = "Bu kayıt zaten sistemimizde bulunmuyor!";

                //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "MailingModal", "$('#MailingModal').modal();", true);
                //    upModal.Update();
                //}
            }


        }
    }
}