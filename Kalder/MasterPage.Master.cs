﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Timers;


namespace Kalder
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {

        DbConnection db = new DbConnection();
        System.Timers.Timer OrtalamaSure = new System.Timers.Timer();
       
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Header.DataBind();
             
            //  Önbellekte verilerimizi tutuyoruz.
            //  Response.Cache.SetExpires(DateTime.Now.AddMinutes(1));
            //  Response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);
            //  Response.Cache.SetValidUntilExpires(true);

         //   slicknavCss.Text = "  <link href=\"" + Page.ResolveUrl("~/js/slicknav.css") + "\" rel=\"stylesheet\" />";

           // colorCSS.Text = "  <link href=\"" + Page.ResolveUrl("~/style/color.css") + "\" rel=\"stylesheet\" />";

            //if (Session["AdminId"] != null)
            //{
            //    ltrlAdminBar.Text =
            //    "<link href=\"" + Page.ResolveUrl("~/") + "Adminv2/css/bootstrap.min.css\" rel=\"stylesheet\" />" +
            //    "<link href=\"" + Page.ResolveUrl("~/") + "Adminv2/css/bootstrap-responsive.min.css\" rel=\"stylesheet\" />" +
            //    "<link href=\"" + Page.ResolveUrl("~/") + "Adminv2/css/font-awesome.min.css\" rel=\"stylesheet\" />" +
            //    "<link href=\"" + Page.ResolveUrl("~/") + "Adminv2/css/base-admin-2.css\" rel=\"stylesheet\" />" +
            //    "<link href=\"" + Page.ResolveUrl("~/") + "Adminv2/css/base-admin-2-responsive.css\" rel=\"stylesheet\" />" +
            //    "<script type=\"text/javascript\" src=\"" + Page.ResolveUrl("~/") + "Adminv2/js/libs/bootstrap.min.js\"></script>";
             
            //    pnlAdminMenu.Visible = true;

            //    //var ctl = LoadControl(Page.ResolveUrl("~/include/style_switcher.ascx"));
            //    //ph1.Controls.Add(ctl);

            //}

            try
            {
                //Önce Menu cache'e eklenmişmi diye bakıyoruz, yoksa metodu çağırıyor ve method içerisinden cache'e ekletiyoruz.

                //if (Cache["MenuCache"] == null)
                //{
                //    GetPage();
                //}
                //// Arkasından ilgili repeater arkadaşımız datalarını dolduruyor.
                //rpt_mainmenu.DataSource = Cache["MenuCache"] as DataTable;
                //rpt_mainmenu.DataBind();

                Menu menu = new Menu();
                List<MenuItem> menus = menu.GetAll();
                ltrlmenu.Text = menu.WriteMenuItem(menus, 0);

                //Sayfa Menüleri çağırıldı


                newsControl();
                PhotoControl();
                ReferanceControl();
                
                GetProducts();
                GetNewsCategories();
                GetPhotoCategories();
                GetSocialNetworks();

                DataRow dr = db.GetDataRow("select * from GenelAyarlar");
   

                //ltrlCompanyName.Text = dr["FirmaUnvanUzun"].ToString();

                bool showReferences = Convert.ToBoolean(dr["ShowRef"]);

                if (showReferences)
                    liRef.Visible = true;
                else
                    liRef.Visible = false;

                bool showNews = Convert.ToBoolean(dr["ShowNews"]);
                if (showNews)
                    liNews.Visible = true;
                else
                    liNews.Visible = false;



                /* Facebook Likebox*/
                string fblink = dr["Facebook"].ToString();

                string logo = dr["LogoUrl"].ToString();
                imgLogo.ImageUrl = "~/upload/" + logo;

                LtrlAdres.Text = dr["Adres"].ToString();
                LtrlTel.Text = dr["Tel"].ToString();
                LtrlFax.Text = dr["Fax"].ToString();
                ltrlTelUst.Text = "<a href=\"tel:" + dr["Tel"].ToString() + "\"> Tel: " + dr["Tel"].ToString() + "</a>";
                

                ltrlMail.Text = "<a href=\"mailto:"+dr["Eposta"].ToString()+"\">"+dr["Eposta"].ToString()+"</a>";


                ltrlGoogleAnalytics.Text = dr["Analytics"].ToString();

              

                Istatistik();
            }
            catch (Exception)
            {
                throw;
            }

            //string IpAdres = Request.ServerVariables["REMOTE_ADDR"].ToString();//Ip Adresini alıyoruz.
            //string Tarih = DateTime.Now.ToShortDateString();

            //DataRow drStats = db.GetDataRow("Select * from Stats");
            //int hit = (int)drStats["TotalVisitors"] + 1;
            //db.cmd("update Stats set TotalVisitors=" + hit.ToString() + "and Ip");


            //if (!Page.IsPostBack)
            //{
            //    getAdress();
            //}

        }


        public void GetBanners()
        {
            DataTable dt = db.GetDataTable("Select * from Banners where IsActive=1");

            if (dt.Rows.Count > 0)
            {
                Random r = new Random();
                int randomRow = r.Next(0, dt.Rows.Count);


                string Link = dt.Rows[randomRow]["Link"].ToString();
                string LinkTarget = dt.Rows[randomRow]["LinkTarget"].ToString();
                string ImageUrl = dt.Rows[randomRow]["ImageUrl"].ToString();
                string Title = dt.Rows[randomRow]["Title"].ToString();

             //   ltrlBanner.Text = "<a href=\"" + Link + "\" target=\"" + LinkTarget + "\"><img src=\"" + Page.ResolveClientUrl("~/") + "upload/BannerResimleri/" + ImageUrl + "\" alt=\"" + Title + "\" /></a>";

            }
            else
            {
              //  ltrlBanner.Visible = false;
            }

        }

        public void Istatistik()
        {
           

            string IpAdres = Request.ServerVariables["REMOTE_ADDR"].ToString(); //Ip Adresini alıyoruz.
            DateTime dT = DateTime.Now;

            string Tarih = dT.ToString("yyyy-MM-dd");
            
            string browser = Request.Browser.Browser;
           // HttpBrowserCapabilities browser = Request.Browser;


            // Bu doğrulama api için çok bekliyordu o yüzden iptal ettim. Ülke parametresini önemsemiyorum.
            //DataSet dset = new DataSet();
            //dset.ReadXml("http://freegeoip.net/xml/"+IpAdres);            
            //string ulke = dset.Tables[0].Rows[0]["CountryName"].ToString();

            string ulke = "Turkey";

            // O güne Ait Hit Bilgi Güncelleme
            DataRow drHit = db.GetDataRow("Select * from Stats Where Ip='" + IpAdres + "' and Date='" + Tarih + "'");
            if (drHit == null)
            {
                // Bugüne ait kayıt yoksa bugunün ilk siftahını yap
                db.cmd("Insert into Stats(Date,TotalVisitors,UniqeVisitors,AvarageTime,Ip,Browser,Location,Hit) values('" + Tarih + "',1,1,0,'" + IpAdres + "','" + browser + "','" + ulke + "',1)");
            }
            else
            {
                //Session kontrolü yapıyorum, eğer yeni oturum açılmışsa arkadaş gidip bu ip ve bu tarihteki Oturum sayısını 1 arttıracak.
                if (Session.IsNewSession)
                {
                    db.cmd("Update Stats set TotalVisitors=TotalVisitors+1 where Ip='" + IpAdres + "' and Browser='" + browser + "'");
                }

                //Bugüne ait kayıt varsa Hit 1 artırıyoruz.
                db.cmd("Update Stats set Hit=Hit+1 Where Date='" + Tarih + "'");
                db.cmd("Update Stats set Browser='" + browser + "' Where Date='" + Tarih + "' and Ip='" + IpAdres + "'");
                

                //Tekil artımı için önce Ip kontrolü yapıyoruz.
                DataRow drIpKontrol = db.GetDataRow("select * from Stats Where Ip='" + IpAdres + "'");
                if (drIpKontrol == null)
                { //Eğer ip yoksa tekilide artırabiliriz. Ip kayıtlı ise artırma işlemi yapmıyoruz.

                    db.cmd("Update Stats set UniqeVisitors=UniqeVisitors+1 Where Date='" + Tarih + "'");
                }
                

            }
          
        }

        void OrtalamaSureHesapla()
        {
            // In this case, we'll run a task every minute
            OrtalamaSure.Interval = 5000;
            OrtalamaSure.Enabled = true;
            // Add handler for Elapsed event
            OrtalamaSure.Elapsed +=
            new System.Timers.ElapsedEventHandler(OrtalamaSure_Elapsed);
            OrtalamaSure.Stop();
            OrtalamaSure.Start();

        }

        void OrtalamaSure_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //  db.cmd("update Stats set AvarageTime=AvarageTime+5 where Ip='" + ip + "'");

            //ortalamaSure += 5;
            ////Application["AverageTime"] = (int)Application["AverageTime"] + 5;
            //// ip = System.Web.HttpContext.Current.Request.UserHostAddress;
            //int sure = int.Parse(db.GetDataCell("Select AvarageTime from Stats"));
            //int yeniSure = sure + ortalamaSure;
            //db.cmd("update Stats set AvarageTime='" + yeniSure.ToString() + "'"); 
            // 
          //  db.cmd("update Stats set AvarageTime=AvarageTime+5");

           

            db.guncelle("update Stats set AvarageTime=AvarageTime+5",null);
            OrtalamaSure.Stop();
            
        }
         
        public void GetSocialNetworks()
        {
            try
            {
                DataRow dr = db.GetDataRow("Select facebook,google,twitter,pinterest,youtube,Instagram,foursquare from GenelAyarlar");

                if (dr["facebook"] != null && dr["facebook"].ToString() != "")
                {
                    hplFb.Visible = true;
                    hplFb.NavigateUrl = "http://" + dr["facebook"].ToString();
                    hplFb.Target = "_blank";
                }

                if (dr["twitter"] != null && dr["twitter"].ToString() != "")
                {
                    hplTwit.Visible = true;
                    hplTwit.NavigateUrl = "http://" + dr["twitter"].ToString();
                    hplTwit.Target = "_blank";
                }

                if (dr["google"].ToString() != null && dr["google"].ToString() != "")
                {
                    hplgoogle.Visible = true;
                    hplgoogle.NavigateUrl = "http://" + dr["google"].ToString();
                    hplgoogle.Target = "_blank";
                }


                if (dr["pinterest"] != null && dr["pinterest"].ToString() != "")
                {
                    hplpinterest.Visible = true;
                    hplpinterest.NavigateUrl = "http://" + dr["pinterest"].ToString();
                    hplpinterest.Target = "_blank";
                }

                if (dr["youtube"] != null && dr["youtube"].ToString() != "")
                {
                    hplyoutube.Visible = true;
                    hplyoutube.NavigateUrl = "http://" + dr["youtube"].ToString();
                    hplyoutube.Target = "_blank";
                }

                if (dr["Instagram"] != null && dr["Instagram"].ToString() != "")
                {
                    hplinstagram.Visible = true;
                    hplinstagram.NavigateUrl = "http://" + dr["Instagram"].ToString();
                    hplinstagram.Target = "_blank";
                }

                if (dr["foursquare"] != null && dr["foursquare"].ToString() != "")
                {
                    hplfoursquare.Visible = true;
                    hplfoursquare.NavigateUrl = "http://" + dr["foursquare"].ToString();
                    hplfoursquare.Target = "_blank";
                }
            }
            catch (Exception)
            {

            }
        }
        public void newsControl()
        {
            DataTable dtHaberler = db.GetDataTable("Select * from HaberKategori where YayinDurumu=1");
            if (dtHaberler.Rows.Count > 0)
            {
                hplNews.Visible = true;

                string link = db.GetDataCell("Select newsHeader from GenelAyarlar");

                hplNews.Text = link;
                hplNews.NavigateUrl = Page.ResolveUrl("~/blog");
            }

            else
            {
                hplNews.Visible = false;
            }
        }
        public void PhotoControl()
        {
            DataTable dtphotogallery = db.GetDataTable("Select * from GaleriKategori where YayinDurumu=1");
            int MenudeGoster = int.Parse(db.GetDataCell("SELECT COUNT(MenuGorunumu) AS sayi FROM GaleriKategori where MenuGorunumu >0"));



            //if (dtphotogallery.Rows.Count > 0)
            //{
            //    hplPhotoGallery.Visible = true;
            //    hplPhotoGallery.NavigateUrl = Page.ResolveUrl("~/fotogaleri");
            //}


            if (MenudeGoster > 0 && dtphotogallery.Rows.Count > 0)
            {
                hplPhotoGallery.Visible = true;
                hplPhotoGallery.NavigateUrl = Page.ResolveUrl("~/fotogaleri");
            }

            if (MenudeGoster == 0 && dtphotogallery.Rows.Count < 1)
            {
                hplPhotoGallery.Visible = false;
                liPhoto.Visible = false;
            }

            if (MenudeGoster > 0)
            {
                hplPhotoGallery.Visible = true;
                liPhoto.Visible = true;

            }
            else
            {
                hplPhotoGallery.Visible = false;
                liPhoto.Visible = false;

            }



        }
        public void ReferanceControl()
        {
            DataTable dtref = db.GetDataTable("Select * from Reference where Status=1");
            if (dtref.Rows.Count > 0)
            {
                hplReferences.Visible = true;
                hplReferences.Text = db.GetDataCell("Select refHeader from GenelAyarlar");
                hplReferences.NavigateUrl = Page.ResolveUrl("~/referanslar");
            }

            else
            {
                hplReferences.Visible = false;
            }
        }



        protected void Imgbtn_Search_Onclick(object sender, EventArgs e)
        {
            string keyword = txtSearch.Text;

            if (keyword != null && keyword != "" && keyword != "Ne aradınız?")
                Response.Redirect(Page.ResolveUrl("~/page-results/") + Ayarlar.Temizle(keyword));
            else
                txtSearch.Text = "Ne aradınız?";

        }


        //protected void rpt_mainmenu_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        //{

        //    Repeater rptSub = (Repeater)e.Item.FindControl("RptsubMenu");
        //    rptSub.DataSource = db.GetDataTable("select * from Pages where ParentPageId='"+e.Item.DataItem+"' order by OrderNumber");

        //}
         

        public void GetNewsCategories()
        {
            rpt_News.DataSource = db.GetDataTable("select * from HaberKategori where YayinDurumu=1 order by KategoriSiraNo");
            rpt_News.DataBind();
        }

        public void GetPhotoCategories()
        {
            DataTable dt = db.GetDataTable("Select * from GaleriKategori where YayinDurumu=1 and MenuGorunumu=1 order by KategoriSiraNo");
            rptPhotoCat.DataSource = dt;
            rptPhotoCat.DataBind();
        }


        public void GetProducts()
        {
            rpt_Products.DataSource = db.GetDataTable("select * from UrunKategori where YayinDurumu=1 order by SiraNo");
            rpt_Products.DataBind();
        }


        public void GetPage()
        {
            DataTable dt = new DataTable();

           // DataSet dt = new DataSet();
            //// DataSet i almak için önbeleğe gidiliyor
            //dt = (DataSet)Cache["MenuCache"];
            // DataSet önbellekte yok ise xml dosyası okunuyor.


            //if (Cache["MenuCache"] == null)
            //{
                dt = db.GetDataTable("select * from Pages where ParentPageId=0 and PageIsActive=1 and ShowMenu=1 order by OrderNumber");
                Cache.Insert("MenuCache", dt, null, DateTime.Now.AddMinutes(60 * 24 * 365), System.Web.Caching.Cache.NoSlidingExpiration);
            //}


        
        }


        private int altDoldur(int parentPageId, PlaceHolder treeMenu)
        {
             
                DataSet ds = db.GetDataSet("select * from Pages where ParentPageId=" + parentPageId + "and PageIsActive=1 and ShowMenu=1  order by OrderNumber");

                if (ds.Tables[0].Rows.Count > 0)
                {
                    //string ParentPageName = db.GetDataCell("Select PageName from Pages where PageId=" + parentPageId);
                    treeMenu.Controls.Add(new LiteralControl("<ul class=\"dropdown animated-3s fadeIn\">"));
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        if (dr["PageURL"].ToString().Contains("http"))
                            treeMenu.Controls.Add(new LiteralControl("<li><a href=" + dr["PageURL"].ToString() + " target=\"_blank\"><span>" + dr["PageName"].ToString() + "</span></a>"));
                        else
                            treeMenu.Controls.Add(new LiteralControl("<li><a href=" + Page.ResolveUrl("~") + dr["PageURL"].ToString() + "><span>" + dr["PageName"].ToString() + "</span></a>"));


                        int id = (int)dr["PageId"];
                        altDoldur(id, treeMenu);
                    }
                    treeMenu.Controls.Add(new LiteralControl("</ul>"));
                    treeMenu.Controls.Add(new LiteralControl("</li>"));
                    return 0;
                }
                treeMenu.Controls.Add(new LiteralControl("</li>"));
                return 0;
             
        }




        protected void rpt_mainmenu_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
              
                    DataRowView drv = e.Item.DataItem as DataRowView;

                    PlaceHolder treeMenu = (PlaceHolder)e.Item.FindControl("treeMenu");
                    treeMenu.Controls.Clear();
                    DataSet ds = db.GetDataSet("Select * From Pages where PageId=" + drv.Row["PageId"].ToString() + "and PageIsActive=1 and ShowMenu=1  order by OrderNumber");
                    // treeMenu.Controls.Add(new LiteralControl("<ul id='treeMenu'>"));
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        if ((int)dr["ParentPageId"] == 0)
                        {

                            if (dr["PageURL"].ToString().Contains("http"))
                                treeMenu.Controls.Add(new LiteralControl("<li><a href=" + dr["PageURL"].ToString() + ">" + dr["PageName"].ToString() + "</a>"));
                            else
                                treeMenu.Controls.Add(new LiteralControl("<li><a href=" + Page.ResolveUrl("~") + dr["PageURL"].ToString() + ">" + dr["PageName"].ToString() + "</a>"));

                            //  treeMenu.Controls.Add(new LiteralControl("<li><a href=" + Page.ResolveUrl("~") + dr["PageURL"].ToString() + ">" + dr["PageName"].ToString() + "</a>"));
                            int Id = (int)dr["PageId"];
                            altDoldur(Id, treeMenu);
                        }
                    }
                    //   treeMenu.Controls.Add(new LiteralControl("</ul>"));
                }
            
        }



        private int altKategoriDoldur(int parentPageId, PlaceHolder treeMenu)
        {
            DataSet ds = db.GetDataSet("select * from UrunKategori where UstKatId=" + parentPageId + "and YayinDurumu=1  order by SiraNo");

            if (ds.Tables[0].Rows.Count > 0)
            {
                //string ParentPageName = db.GetDataCell("Select PageName from Pages where PageId=" + parentPageId);
                treeMenu.Controls.Add(new LiteralControl("<ul>"));
                foreach (DataRow dr in ds.Tables[0].Rows)
                {

                    treeMenu.Controls.Add(new LiteralControl("<li><a href=" + Page.ResolveUrl("~/urunler/") + dr["Seo_url"].ToString() + "><span>" + dr["KatAdi"].ToString() + "</span></a>"));
                    int id = (int)dr["UrunKatId"];
                    altKategoriDoldur(id, treeMenu);
                }
                treeMenu.Controls.Add(new LiteralControl("</ul>"));
                treeMenu.Controls.Add(new LiteralControl("</li>"));
                return 0;
            }
            treeMenu.Controls.Add(new LiteralControl("</li>"));
            return 0;
        }

        protected void rpt_Products_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;

                PlaceHolder treeMenu = (PlaceHolder)e.Item.FindControl("productMenu");
                treeMenu.Controls.Clear();
                DataSet ds = db.GetDataSet("Select * From UrunKategori where UrunKatId=" + drv.Row["UrunKatId"].ToString() + "and YayinDurumu=1 and MenudeGoster=1  order by SiraNo");
                // treeMenu.Controls.Add(new LiteralControl("<ul id='treeMenu'>"));
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if ((int)dr["UstKatId"] == 0)
                    {
                        treeMenu.Controls.Add(new LiteralControl("<li><a href=" + Page.ResolveUrl("~/urunler/") + dr["Seo_url"].ToString() + ">" + dr["KatAdi"].ToString() + "</a>"));
                        int Id = (int)dr["UrunKatId"];
                        altKategoriDoldur(Id, treeMenu);
                    }
                }
                // treeMenu.Controls.Add(new LiteralControl("</ul>"));
            }
        }


        protected void rpt_News_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    DataRowView drv = e.Item.DataItem as DataRowView;

            //    PlaceHolder treeMenu = (PlaceHolder)e.Item.FindControl("newsMenu");
            //    treeMenu.Controls.Clear();
            //    DataSet ds = db.GetDataSet("Select Distinct KategoriAdi,KategoriSiraNo,MenuGorunumu From HaberKategori where YayinDurumu=1 order by KategoriSiraNo");

            //    treeMenu.Controls.Add(new LiteralControl("<ul id='treeMenu'>"));
            //    foreach (DataRow dr in ds.Tables[0].Rows)
            //    {
            //        if ((bool)dr["MenuGorunumu"] == true)
            //        {
            //            treeMenu.Controls.Add(new LiteralControl("<li><a href=" + dr["KategoriAdi"].ToString() + ">" + dr["KategoriAdi"].ToString() + "</a>"));
            //            //int Id = (int)dr["UrunKatId"];
            //            //altKategoriDoldur(Id, treeMenu);
            //        }
            //    }
            //    treeMenu.Controls.Add(new LiteralControl("</ul>"));
            //}
        }


        protected void rptPhotoCat_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {

        }

        protected void dropCins_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void dropPlaces_SelectedIndexChanged(object sender, EventArgs e)
        {

          


        }

        //public void getAdress() {

        //    dropCins.Items.FindByText("GENEL MERKEZ").Selected = true;

        //    string[] kurumtipi = { "GENEL MERKEZ", "ŞUBE", "TEMSİLCİLİK" };
        //    string[] sehirler = { "İstanbul", "Ankara", "Bursa", "Eskişehir", "İzmir", "Kayseri", "Trakya", "Doğu Marmara", "Batı Akdeniz" };
        //    string[] adresler = { "İstanbul Adresi", "Ankara Adresi", "Bursa Adresi", "Eskişehir Adresi", "İzmir Adresi", "Kayseri Adresi", "Trakya Adresi", "Doğu Marmara Adresi", "Batı Akdeniz Adresi" };

        //    dropCins.DataSource = kurumtipi;
        //    dropCins.DataBind();
        //    dropCins.Items[0].Selected = true;


        //    if (dropCins.SelectedIndex == 0)
        //    {
        //        try
        //        {
        //            dropPlaces.DataSource = sehirler;
        //            dropPlaces.Items.FindByValue(sehirler[0]).Selected = true;
        //            txtAdressContent.Text = adresler[0].ToString();
        //        }
        //        catch (Exception ex)
        //        {
                    
        //        }
        
        //    }

        //}




    }
}