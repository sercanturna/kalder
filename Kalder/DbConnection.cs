﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Collections.Generic;
using System.Web;
using System.Configuration;
using System.Web.UI;
using System.Net.Mail;
using System.IO;
using System.Net;

/// <summary>
/// Sercan
/// </summary>
/// 

namespace Kalder
{ 
public class DbConnection
{
    public DbConnection()
    {

    }
   

    #region SqlClass

    public SqlConnection baglan()
    {
        SqlConnection.ClearAllPools();
        SqlConnection baglanti = new SqlConnection(ConfigurationManager.ConnectionStrings["KalderConnectionString"].ConnectionString);
        baglanti.Open();
        return (baglanti);
    }

    public int cmd(string sqlcumle)
    {
        SqlConnection baglan = this.baglan();
        SqlCommand sorgu = new SqlCommand(Ayarlar.SQLInjectionControl(sqlcumle), baglan);
        int sonuc = 0;

        try
        {
            sonuc = sorgu.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            throw new Exception(ex.Message + " (" + sqlcumle + ")");
        }
        sorgu.Dispose();
        baglan.Close();
        baglan.Dispose();
        return (sonuc);
    }

    public DataTable GetDataTable(string sql)
    {
        SqlConnection baglanti = this.baglan();
        SqlDataAdapter adapter = new SqlDataAdapter(Ayarlar.SQLInjectionControl(sql), baglanti);
        DataTable dt = new DataTable();
        try
        {
            adapter.Fill(dt);
        }
        catch (SqlException ex)
        {
            throw new Exception(ex.Message + " (" + sql + ")");
        }
        adapter.Dispose();
        baglanti.Close();
        baglanti.Dispose();
        return dt;
    }

    public DataSet GetDataSet(string sql)
    {
        SqlConnection baglanti = this.baglan();
        SqlDataAdapter adapter = new SqlDataAdapter(Ayarlar.SQLInjectionControl(sql), baglanti);
        DataSet ds = new DataSet();
        try
        {
            adapter.Fill(ds);
        }
        catch (SqlException ex)
        {
            throw new Exception(ex.Message + " (" + sql + ")");
        }
        adapter.Dispose();
        baglanti.Close();
        baglanti.Dispose();
        return ds;
    }

    public DataRow GetDataRow(string sql)
    {
        DataTable table = GetDataTable(Ayarlar.SQLInjectionControl(sql));
        if (table.Rows.Count == 0) return null;
        return table.Rows[0];
    }

    public string GetDataCell(string sql)
    {
        DataTable table = GetDataTable(Ayarlar.SQLInjectionControl(sql));
        if (table.Rows.Count == 0) return null;
        return table.Rows[0][0].ToString();
    }


    public int guncelle(string cumle, UpdatePanel up1)
    {
        SqlConnection conn = this.baglan();
        SqlCommand sql = new SqlCommand(Ayarlar.SQLInjectionControl(cumle), conn);
        int sonuc = 0;
        try
        {
            sonuc = sql.ExecuteNonQuery();

        }
        catch (Exception)
        {

        }

        if (sonuc > 0)
            ScriptManager.RegisterStartupScript(up1, GetType(), "Javascript", "Javascript:Success();", true);
        else
            ScriptManager.RegisterStartupScript(up1, GetType(), "Javascript", "Javascript:Success();", true);

        conn.Close();
        conn.Dispose();
        sql.Dispose();
        return (sonuc);
    }

    public DataTable GetProductCat()
    {
        DataTable dtProductCats = this.GetDataTable(";with CTE As" +
              " (" +
              " SELECT UrunKatId ,UstKatId,KatAdi,MenudeGoster,Meta_Desc,Meta_Title,Meta_Keyword,Aciklama,Seo_url" +
               "      ,YayinDurumu,Sutun,SiraNo,Resim,EklenmeTarihi, 0 as [Level]," +
               "    Cast(UrunKatId as varchar(max)) As KatId," +
              "     Cast(KatAdi as varchar(max)) as KategoriAdi" +

               "    from UrunKategori" +
               "    where UstKatId =0 " +

               "    Union All" +

                "   select T.UrunKatId,T.UstKatId,T.KatAdi,T.MenudeGoster,T.Meta_Desc,T.Meta_Title,T.Meta_Keyword,T.Aciklama" +
                "     ,T.Seo_url,T.YayinDurumu,T.Sutun,T.SiraNo,T.Resim,T.EklenmeTarihi,[Level]+1," +

                "   KatId+'->'+Cast(T.UrunKatId as varchar(max)),KategoriAdi+'->'+Cast(T.KatAdi as varchar(max))" +
                 "  from UrunKategori As T" +
                "   Inner Join CTE On T.UstKatId=CTE.UrunKatId " +
              " )" +
              " Select * from CTE order by KategoriAdi asc,SiraNo asc");

        return dtProductCats;
    }

    public string GetAdminYetki(string AdminId)
    {
        string yetki = GetDataCell("Select YetkiId from Admin where AdminId="+AdminId);
        return yetki;
    }


    public DataTable GetPages()
        {
        DataTable dtPages = this.GetDataTable("with CTE As"+
        "("+
        "SELECT PageId ,ParentPageId,PageName,PageUrl,Meta_Desc,Meta_Title,Meta_Keywords,PageContent"+
        "        ,Hit,PageIsActive,OrderNumber,CreatedDate, 0 as [Level],"+
        "    Cast(PageId as varchar(max)) As SayfaId,"+
        "    Cast(PageName as varchar(max)) as SayfaAdi"+

        "    from Pages"+
        "    where ParentPageId = 0"+

        "    Union All"+

        "    select T.PageId,T.ParentPageId,T.PageName,T.PageUrl,T.Meta_Desc,T.Meta_Title,T.Meta_Keywords,T.PageContent"+
        "        ,T.Hit,T.PageIsActive,T.OrderNumber,T.CreatedDate,[Level]+1," +

        "    SayfaId+'->'+Cast(T.PageId as varchar(max)),SayfaAdi+'->'+Cast(T.PageName as varchar(max))"+
        "    from Pages As T"+
        "    inner Join CTE On T.ParentPageId=CTE.PageId"+
        ")"+
        "Select * from CTE order by SayfaAdi asc");

 return dtPages;
        }


    public DataTable GetPageBreadcump(string id)
    {
        DataTable dtPages = this.GetDataTable("with CTE As" +
        "(" +
        "SELECT PageId ,ParentPageId,PageName,PageUrl,Meta_Desc,Meta_Title,Meta_Keywords,PageContent" +
        "        ,Hit,PageIsActive,OrderNumber,CreatedDate, 0 as [Level]," +
        "    Cast(PageId as varchar(max)) As SayfaId," +
        "    Cast(PageName as varchar(max)) as SayfaAdi" +

        "    from Pages" +
        "    where ParentPageId =0" +

        "    Union All" +

        "    select T.PageId,T.ParentPageId,T.PageName,T.PageUrl,T.Meta_Desc,T.Meta_Title,T.Meta_Keywords,T.PageContent" +
        "        ,T.Hit,T.PageIsActive,T.OrderNumber,T.CreatedDate,[Level]+1," +

        "    SayfaId+' » '+Cast(T.PageId as varchar(max)),SayfaAdi+' » '+Cast(T.PageName as varchar(max))" +
        "    from Pages As T" +
        "    Inner Join CTE On T.ParentPageId=CTE.PageId" +
        ")" +
        "Select * from CTE where PageId="+id+" order by SayfaAdi asc");

        return dtPages;
    }

    public string GetIp()
    {
       
         string ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        
        return ip;
    }


    public void MailGonder(string gonderen, string gonderen_baslik, string alan, string alan_baslik, string konu, string icerik, string username, string pass, string SMTP_Adress)
    {
        MailAddress _gonderen = new MailAddress(gonderen, gonderen_baslik);
        MailAddress _alan = new MailAddress(alan, alan_baslik);
        MailMessage eposta = new MailMessage(_gonderen, _alan);
        eposta.IsBodyHtml = true;
        eposta.Subject = konu;
        eposta.Body = icerik;
       System.Net.NetworkCredential auth = new System.Net.NetworkCredential(username, pass);
        SmtpClient SMTP = new SmtpClient();
        SMTP.Host = SMTP_Adress;
       // SMTP.Host = "relay-hosting.secureserver.net";
        SMTP.UseDefaultCredentials = false;
        SMTP.Credentials = auth;
        SMTP.DeliveryMethod = SmtpDeliveryMethod.Network;
        SMTP.Timeout = 20000;
        SMTP.Port = 587;
        SMTP.EnableSsl = true;
        try
        {
            SMTP.Send(eposta);
          //  Ayarlar.Alert.Show("Mail Gönderildi!");

        }
        catch (Exception ex)
        {
            Ayarlar.Alert.Show("Mail Gönderilemedi, Sebebi: " + ex.Message);
        }
    }

    public string HttpGet(string uri)
    {
        Stream stream;
        StreamReader reader;
        String response = null;
        WebClient webClient = new WebClient();

        using (webClient)
        {
            try
            {
                // open and read from the supplied URI
                stream = webClient.OpenRead(uri);
                reader = new StreamReader(stream);
                response = reader.ReadToEnd();
            }
            catch (WebException ex)
            {
                if (ex.Response is HttpWebResponse)
                {
                    // Add you own error handling as required
                    switch (((HttpWebResponse)ex.Response).StatusCode)
                    {
                        case HttpStatusCode.NotFound:
                        case HttpStatusCode.Unauthorized:
                            response = null;
                            break;

                        default:
                            throw ex;
                    }
                }
            }
        }

        return response;
    }



    public DataTable getCalenderNotesbyDate() {
         DataTable dt = GetDataTable(Ayarlar.SQLInjectionControl("select * from Events WHERE IsActive=1 and ISNULL(BeginDate,'')<>''"));
        return dt;
}


    #endregion
}


}