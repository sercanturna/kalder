﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder
{
    public class StyleColor
    {
        public void ChangeStyle(TextBox txt1, TextBox txt2)
        {
            
            //Style dosyasına veritabanındaki renk kodlarını buradan çekiyor ve ilgili classları yeniden oluşturarak dosyayı yüklüyorum.
            string path = HttpContext.Current.Server.MapPath("../style/color.css");
           
            try
            {
                string color1, color2, gradient;

                color1 = "#" + txt1.Text;
                color2 = "#" + txt2.Text;

                gradient = "background: linear-gradient(" + color2 + "," + color1 + ");";

                string colorStyle =
                ".aaa, #menu, #menu li a, a.more, h1.h_bg, .h2_bg, #cssmenu > ul > li, div.page_content h1.title, div.page_content h1, #cssmenu > ul > li:hover, div.CurrencyModule table thead tr th.satis," +
                "#contact-form input[type=\"submit\"], .search_button, ul.search_box li.search, ul.search_box li, #show_menu .slicknav_menu { background:" + color1 + "; " + gradient + " }" +
                "div.header { border-top: 5px solid " + color1 + "; }" +
                "#cssmenu { border-bottom: 4px solid " + color1 + "; }" +
                "#cssmenu ul ul a:hover { color: " + color1 + "; background-color: #d4d4d4; opacity:0.8; } " +
                "a.more:hover { background: " + color1 + "; opacity:0.7;}" +

                "div.carousel-normal ul li h1.box { color: " + color2 + "; }" +
                "div.news_container ul.news li h3, div#contact-form h3, div#contact-form div label span, div.headline ul li h2.headline { color: " + color2 + " !important; } " +
                "h3{ color: " + color1 + " !important;  } " +
                "#breadcrumb a:hover { color: " + color1 + "; }" +
                ".r-tabs .r-tabs-nav .r-tabs-anchor { background: " + color1 + "; }" +
                ".r-tabs .r-tabs-nav .r-tabs-state-active .r-tabs-anchor { color: " + color1 + "; background-color: #fff; }" +

                "#menu li .hasChildren{border-top: 4px solid " + color1 + ";}" +
                "#menu li ul .hasChildren{border-left: 4px solid " + color1 + ";}" +
                "#menu li a{border-left: 1px solid " + color1 + "; }" +
                "#menu li:hover > a{background: " + color2 + "} " +
                "#selectionSharerPopover-inner{background-image:none; background: " + color1 + "; border-color: " + color2 + " }" +
                "#selectionSharerPopover span.selectionSharerPopover-arrow{background-color: " + color1 + " !important;  border: 2px solid " + color2 + ";}" +
                "#selectionSharerPopover:after{background: " + color1 + ";}" +
                "#back-to-top:hover {background-color: #F90; background: " + color1 + " !important;}" +
                "div.CurrencyModule table thead tr th.alis{background: " + color2 + " !important;}" +
                "div.CurrencyModule table thead tr th.satis{background: " + color1 + " !important;}";

                // Eğer css dosyası varsa sil. 
                if (File.Exists(path))
                {
                    File.Delete(path);
                }

                // yoksa oluştur. 
                using (FileStream fs = File.Create(path))
                {
                    Byte[] info = new UTF8Encoding(true).GetBytes(colorStyle);
                    // içerikleri ekle.
                    fs.Write(info, 0, info.Length);
                }
            }

            catch (Exception ex)
            {
                Ayarlar.Alert.Show(ex.ToString());
                
            }
            // style dosyası işlem sonu
        }
    }
}