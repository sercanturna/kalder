﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Kalder
{
    public partial class search : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();
        string Keyword;
        string SearchCategory;
        int sonucSayisi;
        protected void Page_Load(object sender, EventArgs e)
        {
        
                txtDetailSearch.Attributes.Add("onKeyPress",
   "doClick('" + ImgBtnSearch.ClientID + "',event)");
            
            
            Keyword = RouteData.Values["ArananKelime"].ToString();
            ltrlKeyword.Text = Keyword;

            SearchCategory = RouteData.Values["Kategori"].ToString();

            lnkPages.PostBackUrl = Page.ResolveUrl("~/page-results/") + Ayarlar.Temizle(Keyword);
            lnkNews.PostBackUrl = Page.ResolveUrl("~/news-results/") + Ayarlar.Temizle(Keyword);
            lnkProduct.PostBackUrl = Page.ResolveUrl("~/product-results/") + Ayarlar.Temizle(Keyword);
            lnkImages.PostBackUrl = Page.ResolveUrl("~/image-results/") + Ayarlar.Temizle(Keyword);


            switch (SearchCategory)
            {
                case "page":
                    {
                        sonucSayisi = GetPageResult();
                        lnkPages.CssClass = "active";
                        break;
                    }

                case "news":
                    {
                        sonucSayisi = GetNewsResult();
                        lnkNews.CssClass = "active";
                        break;
                    }
                case "product":
                    {
                        sonucSayisi = GetProductResult();
                        lnkProduct.CssClass = "active";
                        break;
                    }
                case "image":
                    {
                        // sonucSayisi = GetNewsResult();
                        lnkImages.CssClass = "active";
                        break;
                    }
                default:
                    sonucSayisi = GetPageResult();
                    break;
            }



           // ImgBtnSearch.ImageUrl = Page.ResolveUrl("~") + "images/search.jpg";


            // sonucSayisi = GetPageResult() + GetNewsResult();

            ltrlKeyword.Text = "Sitede \"<strong>" + Keyword + "</strong>\" ile ilgili yaklaşık " + sonucSayisi + " " + SearchCategory.Replace("image", "resim").Replace("product", "ürün").Replace("news", "haber").Replace("page", "sayfa") + " bulundu!";

            ltrlnewsCat.Text = Keyword;

          
        }

        public int GetPageResult()
        {
            DataTable dtPages = db.GetDataTable("Select * from Pages Where (PageName like '%" + Keyword + "%' or " +
    "PageUrl like '%" + Keyword + "%' or " +
    "PageImage like '%" + Keyword + "%' or " +
    "Meta_Title like '%" + Keyword + "%' or " +
    "Meta_Keywords like '%" + Keyword + "%' or " +
    "Meta_Desc like '%" + Keyword + "%' or " +
    "PageContent like '%" + Keyword + "%')");

            if (dtPages.Rows.Count > 0)
            {

                //ltrlKeyword.Text = "\"<strong>" + Keyword + "</strong>\" ile ilgili yaklaşık " + dtPages.Rows.Count + " sonuç bulundu!";


                CollectionPager1.DataSource = dtPages.DefaultView;
                CollectionPager1.BindToControl = rptPages;
                rptPages.DataSource = CollectionPager1.DataSourcePaged;
                rptPages.DataBind();

                lnkPages.Text = "Sayfalar (" + dtPages.Rows.Count.ToString() + ")";
            }
            else
            {
                ltrlKeyword.Text = Keyword + " ile ilgili sonuç bulunamadı!";
            }

            return dtPages.Rows.Count;
        }

        public int GetNewsResult()
        {
            DataTable dtNews = db.GetDataTable("Select * from Haberler as h inner join HaberKategori as hk on h.KategoriId=hk.KategoriId Where (Baslik like '%" + Keyword + "%' or " +
    "Ozet like '%" + Keyword + "%' or " +
    "Detay like '%" + Keyword + "%' or " +
                //"Meta_Title like '%" + Keyword + "%' or " +
                //"Meta_Keywords like '%" + Keyword + "%' or " +
                //"Meta_Desc like '%" + Keyword + "%' or " +
    "Resim like '%" + Keyword + "%')");

            if (dtNews.Rows.Count > 0)
            {
                //ltrlKeyword.Text = "\"<strong>" + Keyword + "</strong>\" ile ilgili yaklaşık " + dtPages.Rows.Count + " sonuç bulundu!";



                CollectionPager1.DataSource = dtNews.DefaultView;
                CollectionPager1.BindToControl = rptNews;
                rptNews.DataSource = CollectionPager1.DataSourcePaged;
                rptNews.DataBind();

                lnkNews.Text = "Haberler (" + dtNews.Rows.Count.ToString() + ")";
            }
            else
            {
                ltrlKeyword.Text = Keyword + " ile ilgili sonuç bulunamadı!";
            }
            return dtNews.Rows.Count;
        }


        public int GetProductResult()
        {
            DataTable dtProduct = db.GetDataTable("SELECT p.*,kui.*, uk.*,c.*,(" +
            "SELECT top 1 ImageURL FROM ProductImages WHERE ProductImages.ProductId = p.ProductId and IsPrimary=1) as ImageURL from Products as p inner join KatUrunIliski as kui on p.ProductId=kui.ProductId inner join UrunKategori as uk on uk.UrunKatId=kui.UrunKatId inner join Currency as c on c.CurrencyId=p.CurrencyId where" +
                "(ProductName like '%" + Keyword + "%' or " +
    "p.Description like '%" + Keyword + "%' or " +
    "p.SeoUrl like '%" + Keyword + "%' or " +
    "p.Meta_Title like '%" + Keyword + "%' or " +
    "p.Meta_Keywords like '%" + Keyword + "%' or " +
    "p.Meta_Desc like '%" + Keyword + "%')");

            if (dtProduct.Rows.Count > 0)
            {

                //ltrlKeyword.Text = "\"<strong>" + Keyword + "</strong>\" ile ilgili yaklaşık " + dtPages.Rows.Count + " sonuç bulundu!";


                CollectionPager1.DataSource = dtProduct.DefaultView;
                CollectionPager1.BindToControl = rptProducts;
                rptProducts.DataSource = CollectionPager1.DataSourcePaged;
                rptProducts.DataBind();

                lnkPages.Text = "Ürünler (" + dtProduct.Rows.Count.ToString() + ")";
            }
            else
            {
                ltrlKeyword.Text = Keyword + " ile ilgili sonuç bulunamadı!";
            }

            return dtProduct.Rows.Count;
        }



        protected void rpt_Products_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                DataRowView drv = e.Item.DataItem as DataRowView;
                Image yeni = (Image)e.Item.FindControl("imgNew");

                Literal KDV = (Literal)e.Item.FindControl("ltrlKDV");

                if ((double)drv.Row["Tax"] == 0)
                    KDV.Visible = false;
                else
                    KDV.Visible = true;
                KDV.Text = "+ KDV";

                string IsNew = db.GetDataCell("Select IsNew from Products where ProductId=" + drv.Row["ProductId"].ToString());

                if (IsNew != "True")
                {
                    yeni.Visible = false;
                }

            }

        }




        public void Page_PreInit()
        {
            // Set the Theme for the page. Post-data is not currently loaded
            // Use trace="true" to see the Form collection
            //this.Theme = db.GetDataCell("Select Tema from GenelAyarlar");
            //if (Request.Form != null && Request.Form.Count > 0)
            //    this.Theme = this.Request.Form[3].Trim();
        }



       

        protected void ImgBtnSearch_Click1(object sender, EventArgs e)
        {
            string keyword = txtDetailSearch.Text;

            if (keyword != null && keyword != "" && keyword != "Ne aradınız?")
                Response.Redirect(Page.ResolveUrl("~/page-results/") + Ayarlar.Temizle(keyword));
            else
                txtDetailSearch.Text = "Ne aradınız?";
        }
    }
}