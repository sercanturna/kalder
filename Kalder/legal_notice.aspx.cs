﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Kalder
{
    public partial class legal_notice : System.Web.UI.Page
    {  
        DbConnection db = new DbConnection();
        protected void Page_Load(object sender, EventArgs e)
        {

        DataRow dr = db.GetDataRow("Select * from GenelAyarlar");
         
        ltrlCompany.Text = dr["FirmaUnvanUzun"].ToString();
        ltrlMersis.Text = dr["MersisNo"].ToString();
        ltrlNaceKodu.Text = dr["NaceKodu"].ToString();
        ltrlOrtak1.Text = dr["Ortak1Adi"].ToString();
        ltrlOrtak1Sermaye.Text = dr["Ortak1Sermaye"].ToString();
        ltrlOrtak2.Text = dr["Ortak2Adi"].ToString();
        ltrlOrtak2Sermaye.Text = dr["Ortak2Sermaye"].ToString();
        ltrlOrtak3.Text = dr["Ortak3Adi"].ToString();
        ltrlOrtak3Sermaye.Text = dr["Ortak3Sermaye"].ToString();
        ltrlOrtak4.Text = dr["Ortak4Adi"].ToString();
        ltrlOrtak4Sermaye.Text = dr["Ortak4Sermaye"].ToString();
        ltrlVergiDairesi.Text = dr["VergiDairesi"].ToString();
        ltrlVergiNo.Text = dr["VergiNo"].ToString();
    }

    public void Page_PreInit()
    {
        // Set the Theme for the page. Post-data is not currently loaded
        // Use trace="true" to see the Form collection
        //this.Theme = db.GetDataCell("Select Tema from GenelAyarlar");
        //if (Request.Form != null && Request.Form.Count > 0)
        //    this.Theme = this.Request.Form[4].Trim();
    }
        }
    }
