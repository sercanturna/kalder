﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Kalder
{
    public partial class etik_bildirim_formu : System.Web.UI.Page
    {
        DbConnection db = new DbConnection();

        string AdSoyad, Tel, Email, Aciklama;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGonder_Click(object sender, EventArgs e)
        {
            

            AdSoyad = Ayarlar.SQLInjectionControl(txtName.Text);
            Tel = Ayarlar.SQLInjectionControl(txtPhone.Text);
            Email = Ayarlar.SQLInjectionControl(txtEmail.Text);
            Aciklama = Ayarlar.SQLInjectionControl(txtMsg.Text);


            MailYolla(AdSoyad, Email, Tel, Aciklama);
        }



        public void MailYolla(string name, string email, string phone, string message)
        {
            //MailAddress gonderen = new MailAddress("bilgi@seyahatterzisi.com.tr", "Seyahat Terzisi WebForm");
            //MailAddress alan = new MailAddress("bilgi@seyahatterzisi.com.tr", "StudyExpo");
            //MailMessage eposta = new MailMessage(gonderen, alan);
            //eposta.IsBodyHtml = true;
            //eposta.Subject = "Seyahat Terzisi WebForm";
            //eposta.Body = "<strong>Firma Adı: </strong>" + companyName + "<br/><strong>Adı Soyadı: </strong>" + name + "<br/><strong>E-mail Adresi: </strong>" + email + "<br/><strong>Telefon: </strong>" + phone + "<br/><strong>Mesaj: </strong>" + message + "<br/><br/><br/> " + "Yönetim Panelinden görüntülemek için; <br/> <a href='http://www.seyahatterzisi.com.tr/adminv2'>Site Yönetimine Git!</a> ";


            //System.Net.NetworkCredential auth = new System.Net.NetworkCredential("bilgi@seyahatterzisi.com.tr", "Blg0123");
            //SmtpClient SMTP = new SmtpClient();
            //SMTP.Host = "smtp.seyahatterzisi.com.tr";
            //SMTP.UseDefaultCredentials = false;
            //SMTP.Credentials = auth;
            //SMTP.DeliveryMethod = SmtpDeliveryMethod.Network;

            DataRow dr = db.GetDataRow("Select * from MailSettings where FormId=1 and IsActive = 1");

            if (!dr.IsNull("Id") && !dr.IsNull("SmtpSunucu"))
            {
                string GonderenEposta = dr["Gonderen"].ToString();
                //string AlanEposta = dr["Alan"].ToString();
                string AlanEposta = "etikkurulu@kalder.org";
                string Konu = dr["FormKonusu"].ToString();
                string SmtpSunucu = dr["SmtpSunucu"].ToString();
                string SmtpKullaniciAdi = dr["SmtpKullaniciAdi"].ToString();
                string SmtpParola = dr["SmtpParola"].ToString();


                MailAddress gonderen = new MailAddress(GonderenEposta, Konu);
                MailAddress alan = new MailAddress(AlanEposta, Konu);
                MailMessage eposta = new MailMessage(gonderen, alan);
                eposta.IsBodyHtml = true;
                eposta.Subject = name + " form gönderdi";
                eposta.Body = "<strong>Adı Soyadı: </strong>" + name + "<br/><strong>E-mail Adresi: </strong>" + email + "<br/><strong>Telefon: </strong>" + phone + "<br/><strong>Mesaj: </strong>" + message + "<br/> ";


                System.Net.NetworkCredential auth = new System.Net.NetworkCredential(SmtpKullaniciAdi, SmtpParola);
                SmtpClient SMTP = new SmtpClient();
                SMTP.Host = SmtpSunucu;
                SMTP.UseDefaultCredentials = false;
                SMTP.Credentials = auth;
                SMTP.DeliveryMethod = SmtpDeliveryMethod.Network;
                SMTP.Port = 587;
                SMTP.EnableSsl = true;


                try
                {
                    SMTP.Send(eposta);
                    ltrlSonuc.Text = "<div class=\"alert alert-success\"><strong>Teşekkürler!</strong> Bildiriminiz başarıyla gönderildi.</div>";

                }
                catch (Exception ex)
                {
                    // Response.Write("Mail Gönderilemedi, Sebebi: " + ex.Message);

                    ltrlSonuc.Text = "<div class=\"alert alert-danger\"><strong>Üzgünüm!</strong> Form gönderilirken bir hata oluştu. Hata: " + ex.Message + "</div>";
                    btnGonder.Text = "Tekrar Gönder";
                }
            }
            else
            {
                ltrlSonuc.Text = "<div class=\"alert alert-warning\"><strong>Uyarı!</strong> SMTP ayarları yapılmamış. Ayarlamak için <a href=\"adminv2/Messages.aspx\" target=\"_blank\">buraya tıklayın...</a></div>";

            }

        }

        protected void btnTemizle_Click(object sender, EventArgs e)
        {
            System.Threading.Thread.Sleep(5000);

            txtName.Text = "";
            txtEmail.Text = "";
            txtMsg.Text = "";
            txtPhone.Text = ""; 
            
            ltrlSonuc.Text = "";



            //ScriptManager.RegisterClientScriptBlock(this, GetType(), "script", "$(\"a[data-target=#myModal]\").click(function(ev) { ev.preventDefault();    var target = $(this).attr(\"href\");    $(\"#myModal .modal-body\").load(target, function() {$(\"#myModal\").modal(\"show\");     }); })", true);
        }
    }
}